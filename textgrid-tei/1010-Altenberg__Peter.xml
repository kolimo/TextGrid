<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg45" n="/Literatur/M/Altenberg, Peter/Prosa/Wie ich es sehe/Revolutionär/Gesellschaft">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Gesellschaft</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Altenberg, Peter: Element 00052 [2011/07/11 at 20:31:54]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Peter Altenberg: Wie ich es sehe. 8.–9. Auflage, Berlin: S. Fischer, 1914.</title>
                        <author key="pnd:118502255">Altenberg, Peter</author>
                    </titleStmt>
                    <extent>98-</extent>
                    <publicationStmt>
                        <date>1914</date>
                        <pubPlace>Berlin</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1859" notAfter="1919"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg45.2">
                <div type="h4">
                    <head type="h4" xml:id="tg45.2.1">Gesellschaft</head>
                    <p xml:id="tg45.2.2">Die gelblich-weisse fette aufgedunsene Langweile kroch umher auf dem dunkelrothen weichen Teppich des Salons – – –.</p>
                    <p rend="zenoPLm4n0" xml:id="tg45.2.3">Dann kroch sie auf den Schooss des jungen wunderschönen Haustöchterchens und küsste sie breit auf den Mund – – –.</p>
                    <p rend="zenoPLm4n0" xml:id="tg45.2.4">Da begann das Haustöchterchen zu gähnen –.</p>
                    <p rend="zenoPLm4n0" xml:id="tg45.2.5">Aber Niemand merkte es.</p>
                    <p rend="zenoPLm4n0" xml:id="tg45.2.6">Die junge Frau im braunen seidenen Moirékleide sass neben einem dicken jungen gemüthlichen Schweine.</p>
                    <p rend="zenoPLm4n0" xml:id="tg45.2.7">Sie dachte: »Wieso machen 4 Säcke Kohlen 3 Gulden aus?! Dieser Kerl hat sich das Trinkgeld mit eingerechnet heute Vormittag – – –!?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg45.2.8">Das gemüthliche Schwein grunzte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg45.2.9">Aber weil es reich war und aus guter Familie, sagte man später: »Dieser T. ist fein, so zurückhaltend, bescheiden – – – er hat so gute Manieren.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg45.2.10">Die junge Frau, der die Rechnung nicht stimmte, sagte mit einem Lächeln wie »l'homme qui rit«: <pb n="98" xml:id="tg45.2.10.1"/>
»Sie, Herr T., Ihr Fräulein Schwester ist so lieb – –.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg45.2.11">»L'homme qui rit« lachte nämlich gar nicht – – im Gegentheil! Aber er sah so aus, weil man ihm die Nerven durchschnitten hatte. So lächeln Gesellschaft-Menschen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg45.2.12">»Oh« grunzte das gemüthliche Schwein.</p>
                    <p rend="zenoPLm4n0" xml:id="tg45.2.13">Es wollte sagen: »Zu gütig, Gnädige – – –.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg45.2.14">»Ja, Ihre Schwester hat etwas so Liebes – –« sagte die Dame und starrte auf das Muster der weissen Stores, wie wenn sie es dort ablesen würde. »Was sind das eigentlich für Blumen?!« dachte sie.</p>
                    <p rend="zenoPLm4n0" xml:id="tg45.2.15">Die Schwester, welche »etwas so Liebes« hatte, sass da und dachte: »Wird Er kommen – – –?!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg45.2.16">Aber er kam nicht.</p>
                    <p rend="zenoPLm4n0" xml:id="tg45.2.17">So Etwas hatte man noch nie gesehen! Sie hatte nur ein paar Augen und alles Andere war Plunder –. Aber noch nie hat man laut jammernde Augen gesehen – –. Diese Augen jammerten laut: »Warum kommt Er nicht – –?!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg45.2.18">Plötzlich kroch die gelblich-weisse, fette, aufgedunsene Langweile an ihr empor, setzte sich auf ihren Schooss und küsste sie breit auf den Mund.</p>
                    <p rend="zenoPLm4n0" xml:id="tg45.2.19">Da begann sie zu gähnen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg45.2.20">Aber Niemand merkte es. Sie gähnte direkt mit den Augen, eigentlich mit dem Herzen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg45.2.21">Der junge Lieutenant dachte: »Heute ist Fiakerball! Wenn diese Gisela – – –. Ich gehe nach dem Souper weg. Ich kann um 11 Uhr dort sein; Gisela – – –.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg45.2.22">
                        <pb n="99" xml:id="tg45.2.22.1"/>
Die Langweile zwängte ihre dicke Faust in seinen Mund und sperrte ihn auf.</p>
                    <p rend="zenoPLm4n0" xml:id="tg45.2.23">Das merkten aber Alle – – –.</p>
                    <p rend="zenoPLm4n0" xml:id="tg45.2.24">Beim Souper sagte der Haussohn: »Diese Gabriele P. habe ich benannt ›Letzte Bacchantin des Wienerwaldes‹! Alles jauchzt in ihr! Dieses Leben, diese herrliche Bewegung – – –! Jawohl!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg45.2.25">Die Damen fanden diese Bemerkung ziemlich taktlos – –. »Wie kommt Gabriele hierher, bitte?!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg45.2.26">Alle hassten den Haussohn – –.</p>
                    <p rend="zenoPLm4n0" xml:id="tg45.2.27">»Weil er immer originell sein will – – –!«, dachte Fräulein Dasy.</p>
                    <p rend="zenoPLm4n0" xml:id="tg45.2.28">Die fette aufgedunsene Langweile kroch dem Haussohn auf den Schooss.</p>
                    <p rend="zenoPLm4n0" xml:id="tg45.2.29">Dieser aber gähnte nicht, nicht einmal innerlich!</p>
                    <p rend="zenoPLm4n0" xml:id="tg45.2.30">Er nahm sich die besten Stücke aus der Schüssel, zwei weisse Bruststücke vom Kapaun und schüttete Natursaft darüber wie einen Platzregen. Das amüsirte ihn.</p>
                    <p rend="zenoPLm4n0" xml:id="tg45.2.31">Er dachte: »Was für eine Torte wird kommen?! Sie ist doch die ›letzte Bacchantin des Wienerwaldes‹ –! Und Ihr seid die Gesitteten!? Hollahó!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg45.2.32">Nach dem Souper sagte das Haustöchterchen: »Herr v.S., spielen Sie – – –!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg45.2.33">v.S. spielte das Intermezzo aus den »Rantzau«, wirklich wunderbar – – –.</p>
                    <p rend="zenoPLm4n0" xml:id="tg45.2.34">Die Schwester des Schweines sass in einem Fauteuil und trank die süssen Töne – –.</p>
                    <p rend="zenoPLm4n0" xml:id="tg45.2.35">Der Lieutenant sagte: »Kann man danach tanzen – – –?!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg45.2.36">
                        <pb n="100" xml:id="tg45.2.36.1"/>
Die Hausfrau fand, dass es sehr animirt sei und sans gêne.</p>
                    <p rend="zenoPLm4n0" xml:id="tg45.2.37">Die Herren rauchten und lagen in Fauteuils – –.</p>
                    <p rend="zenoPLm4n0" xml:id="tg45.2.38">Die Langweile kroch hinaus zu dem goldblonden Stubenmädchen, welches im Speizezimmer den Tisch abdeckte – – –.</p>
                    <p rend="zenoPLm4n0" xml:id="tg45.2.39">Da kam der Entdecker der »letzten Bacchantin des Wienerwaldes« und küsste die Goldblonde auf den Mund – – –.</p>
                    <p rend="zenoPLm4n0" xml:id="tg45.2.40">Da kroch die gelblich-weisse, fette, aufgedunsene Langweile, schon ziemlich piquirt, weiter, in das Vorzimmer, wo alle Mäntel und Spitzentücher hingen und diese begannen sich tüchtig zu langweilen, obzwar sie nach Eau de Cologne und Essbouquet dufteten. Aber auf die Dauer ist auch das Duften reizlos, besonders wenn Niemand sagt: »hapzi – – –!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg45.2.41">Und dann kroch die Langweile weiter in das finstere Stiegenhaus und hinaus auf die schwarze Strasse und schleppte sich auf den Fiakerball – – –.</p>
                    <p rend="zenoPLm4n0" xml:id="tg45.2.42">Dort kroch sie dem Fräulein Giesela, die auf den Lieutenant wartete, auf den Schooss und küsste sie breit auf den Mund – – –.</p>
                    <p rend="zenoPLm4n0" xml:id="tg45.2.43">Diese begann zu gähnen und sperrte ihr Mäulchen weit auf – – –.</p>
                    <p rend="zenoPLm4n0" xml:id="tg45.2.44">Aber Niemand merkte es – – –.</p>
                    <p rend="zenoPLm4n0" xml:id="tg45.2.45">Denn Alle tanzten den »Gestrampften« und waren ganz toll!</p>
                    <p rend="zenoPLm4n0" xml:id="tg45.2.46">Der Lieutenant kam nicht.</p>
                    <p rend="zenoPLm4n0" xml:id="tg45.2.47">Er machte dem Haustöchterchen den Hof, bei den Klängen der »Rantzau« und war ganz weg – –!</p>
                    <p rend="zenoPLm4n0" xml:id="tg45.2.48">
                        <pb n="101" xml:id="tg45.2.48.1"/>
Am nächsten Tage sagte das Haustöchterchen: »Es war doch sehr gemüthlich – –!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg45.2.49">»Nach dem Souper!« sagte der Haussohn und dachte an goldblonde Haare und an Anderes – –.</p>
                </div>
            </div>
        </body>
    </text>
</TEI>