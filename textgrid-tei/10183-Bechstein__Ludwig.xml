<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg91" n="/Literatur/M/Bechstein, Ludwig/Märchen/Deutsches Märchenbuch/Der Zauber-Wettkampf">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Der Zauber-Wettkampf</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Bechstein, Ludwig: Element 00095 [2011/07/11 at 20:31:17]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Ludwig Bechstein: Sämtliche Märchen. Mit Anmerkungen und einem Nachwort von Walter Scherf, München: Winkler, 1971.</title>
                        <author key="pnd:118654292">Bechstein, Ludwig</author>
                    </titleStmt>
                    <extent>164-</extent>
                    <publicationStmt>
                        <date when="1971"/>
                        <pubPlace>München</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1801" notAfter="1860"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg91.3">
                <div type="h4">
                    <head type="h4" xml:id="tg91.3.1">Der Zauber-Wettkampf</head>
                    <p xml:id="tg91.3.2">Einstmals ging ein junger Buchbindergeselle in die Fremde, und wanderte, bis kein Kreuzerlein mehr in seiner Tasche klimperte. Da endlich nötigte ihn sein gespanntes Verhältnis mit dem schlaff gewordenen Geldbeutel ernstlich der Arbeit nachzufragen, und bald ward er auch von einem Meister angenommen, und bekam es sehr, sehr gut. Sein Meister sprach zu ihm: »Gesell, du wirst es gut bei mir haben; die Arbeit, die du täglich zu tun hast, ist ganz geringe. Du kehrst nur die Bücher hier alle Tage recht säuberlich ab, und stellst sie dann nach der Ordnung wieder auf. Aber dieses eine Büchlein, welches hier apart steht, darfst du nicht anrühren, viel weniger hineinsehen, sonst ergeht dir's schlimm, Bursche, merk dir's. Dagegen kannst du in den andern Büchern lesen, so viel du nur magst.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg91.3.3">Der Geselle beherzigte die Worte seines Meisters sehr wohl und hatte zwei Jahre lang die besten Tage, indem er täglich nur die Bücher säuberte, dann in manchem derselben las, und dabei die vortrefflichste Kost hatte – jenes verbotene Büchlein ließ er gänzlich unangerührt. Dadurch erwarb er sich das volle Vertrauen seines Herrn, so daß dieser öfters tagelang vom Hause entfernt blieb, und auch zuweilen eine Reise unternahm. Aber wie stets dem Menschen nach Verbotenem gelüstet, so regte sich einstmals, als der Meister auf mehrere Tage verreist war, in dem Gesellen eine mächtige Begierde, endlich doch zu wissen, was in dem Büchlein stehe, das immer ganz heilig an seinem bestimmten Orte lag. – Denn alle andern Bücher hatte er bereits durchgelesen. Zwar sträubte sich sein Gewissen, das Verbotene zu tun, aber die Neugierde war mächtiger; er nahm das Büchlein, schlug es auf und fing an darinnen zu lesen. In dem Büchlein standen die größesten kostbarsten Geheimnisse, die größesten Zauberformeln waren darinnen enthalten, und es stellte sich dem staunenden, höchst verwunderten Gesellen nach und nach alles so sonnenklar heraus, daß er schon anfing, Versuche im Zaubern zu machen. Alles gelang. Sprach der Bursche ein kräftiges Zaubersprüchlein aus diesem Büchlein, so lag im Nu das Gewünschte vor ihm da. Auch lehrte das Büchlein jede menschliche Gestalt in eine andere zu verwandeln. Nun probierte er mehr und mehr, <pb n="164" xml:id="tg91.3.3.1"/>
und zuletzt machte er sich zu einer Schwalbe, nahm das Büchlein und flog im schnellsten Fluge seiner Heimat zu. Sein Vater war nicht wenig erstaunt, als eine Schwalbe zu seinem Fenster einflog, und plötzlich dann aus ihr sein Sohn wurde, den er zwei Jahre lang nicht gesehen. Der Bursche aber drückte den Alten herzlich an seine Brust und sprach: »Väter, nun sind wir glück lich und geborgen, ich bringe ein Zauberbüchlein mit, durch welches wir die reichsten Leute werden können.« Das gefiel dem Alten wohl, denn er lebte sehr dürftig. Bald darauf machte sich der junge Zauberer zu einem überaus großen, fetten Ochsen, und sprach zu seinem Vater: »Nun führet mich zum Markt, und verkauft mich, aber fordert ja viel, recht viel, man wird mich teuer bezahlen, und vergesset ja nicht das kleine Stricklein, welches um meinen linken Hinterfuß gebunden ist, abzulösen, und wieder mit heim zu nehmen, sonst bin ich verloren.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg91.3.4">Das machte der Vater alles so; er verkaufte den Ochsen für ein schweres Geld, denn als er nun mit ihm auf dem Markte erschien, versammelte sich gleich ein Haufen Volkes um ihn, alles bewunderte den Raritäts-Ochsen und Christen und Juden schlugen sich darum, ihn zu kaufen. Der Käufer aber, der das höchste Gebot tat und bezahlte, und den Ochsen im Triumph von dannen führte, hatte am andern Morgen statt des herrlichen Ochsens ein Bündlein Stroh in seinem Stalle liegen. Und der Buchbindergeselle – der war <pb n="165" xml:id="tg91.3.4.1"/>
wohlgemut wieder daheim bei seinem Vater, und lebte mit ihm herrlich und in Freuden von dem Gelde. Manch einer macht sich auch zu einem großen fetten Ochsen, aber keiner kauft ihn teuer.</p>
                    <p rend="zenoPLm4n0" xml:id="tg91.3.5">Bald darauf verzauberte der Bursch sich wieder in einen prächtigen Rappen, und ließ sich von seinem Vater auf den Roßmarkt führen und verkaufen. Da lief wieder das Volk zusammen, um das wunderschöne glänzend schwarze Roß zu sehen. – Jener Meister Buchbinder aber, als er nach Hause zurückgekehrt war, hatte gleich gesehen, was vorgegangen, und da er eigentlich kein Buchbinder, sondern ein mächtiger Zauberer war, der nur zum Schein diese Beschäftigung trieb, so wußte er auch gleich, wieviel es geschlagen hatte, und setzte dem Entflohenen nach. Auf jenem Roßmarkt nun war der Meister unter den Käufern, und da er alle Stücklein des Zauberbüchelchens kannte so merkte er alsobald, was es für eine Bewandtnis mit dem Pferd habe, und dachte: Halt, jetzt will ich dich fangen. Und so suchte er für jeden Preis das Pferd zu kaufen, was ihm auch ohne große Mühe gelang, weil er es gleich um den ersten Kaufpreis annahm. Der Vater kannte den Käufer nicht, aber das Pferd fing an heftig zu zittern und zu schwitzen, und gebehrdete sich äußerst scheu und ängstlich, doch es konnte der Vater die nun so gefährliche Lage seines Sohnes nicht ahnen. Als das Pferd des neuen Eigentümers eingeführt und an den für dasselbe bestimmten Platz gestellt war, wollte der Vater wieder das Stricklein ablösen; aber der Käufer ließ dieses durchaus nicht zu, da er sehr wohl wußte, daß es dann um seinen Fang geschehen wäre. So mußte denn der Vater ohne Stricklein abziehen, und dachte in seinem Sinn: er wird sich schon selbst helfen, kann er so viel, daß er sich zu einem Pferde macht, kann er sich gewiß auch wieder durch seine Zauberkunst dort in dem Stall losmachen und heim kommen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg91.3.6">In jenem Pferdestall aber war ein mächtiges Gedränge von Menschen; groß und klein, alt und jung – alles wollte das ausgezeichnet schöne Roß beschauen. Ein kecker Knabe wagte sogar das Pferd zu streicheln und liebkosend zu klopfen, und es ließ sich dieses, wie es schien, gar gerne gefallen, und als dieser Knabe sich immer vertraulicher näherte und das Pferd am Kopf und am Hals streichelte, da flüsterte es dem Knaben ganz leise zu: »Liebster Junge, hast du kein Messerchen einstecken?« Und der froh verwunderte Knabe <pb n="166" xml:id="tg91.3.6.1"/>
antwortete: »O ja, ich habe ein recht scharfes.« Da sprach der Rappe wieder ganz leise: »Schneide einmal das Stricklein an meinem linken Hinterfuß ab«, und schnell schnitt es der Knabe entzwei. Und in diesem Augenblicke fiel das schöne Roß vor aller Augen zusammen und ward ein Bündlein Stroh, und daraus flog eine Schwalbe hervor, und aus dem Stall empor in die hohen blauen Lüfte. Der Meister hatte das Roß nur einen Augenblick außer acht gelassen, jetzt war keine Zeit zu verlieren. Er brauchte seine Kunst, verwandelte sich rasch in einen Geier, und schoß der flüchtigen Schwalbe nach. Es bedurfte nur noch einer kleinen Weile, so hatte der Geier die Schwalbe in seinen Klauen, aber das Schwälblein merkte den Feind, blickte nieder auf die Erde, und sah da gerade unter sich ein schönes Schloß und vor dem Schloß saß eine Prinzessin und flugs verwandelte sich das Schwälblein in einen goldenen Fingerreif, fiel nieder, und gerade der holden Prinzessin auf den Schoß. Die wußte nicht, wie ihr geschah, und steckte das Ringlein an den Finger. Aber die scharfen Augen des Geiers hatten alles gesehen, und rasch verwandelte sich der Zaubermeister aus einem Geier in einen schmucken Junker und trat heran zur Prinzessin und bat sie höflichst und untertänigst, dieses Ringlein, mit welchem er soeben ein Kunststück gemacht habe, ihm wieder einzuhändigen. Die schöne Prinzessin lächelte errötend, zog das Ringlein vom Finger, und wollte es dem Künstler überreichen, doch siehe, da entfiel es ihren zarten Fingern und rollte als ein winziges Hirsekörnlein in eine Steinritze. Im Augenblicke verwandelte sich der Junker und wurde ein stolzer Gückelhahn, der mit seinem Schnabel emsig in der Steinritze nach dem Hirsekörnlein pickte, aber gleich darauf wurde aus dem Hirsekörnlein ein Fuchs, und dieser biß dem Gückel den Kopf ab. Und somit war der Zaubermeister besiegt. Jetzt aber nahm der junge Geselle wieder seine Gestalt an, sank der Prinzessin zu Füßen, und pries sie dankend, daß sie ihn an ihrem Finger getragen und sich so mit ihm verlobt habe. Die Prinzessin war über alles, was vorgegangen war, mächtig erschrocken, denn sie war noch sehr jung und unerfahren und schenkte ihm ihr Herz und ihre Hand, doch unter der Bedingung, daß er fortan aller Verwandlung entsage, und ihr unwandelbar treu bleibe. Dies gelobte der Jüngling und opferte sein Zauberbüchlein den Flammen, woran er indes sehr übel tat, denn er hätte es ja <pb n="167" xml:id="tg91.3.6.2"/>
dir, lieber Leser, oder mir schenken und vermachen können; in <hi rend="italic" xml:id="tg91.3.6.3">Ochsen</hi> hätten <hi rend="italic" xml:id="tg91.3.6.4">wir zwei</hi> uns gewißlich nicht verwandelt.</p>
                </div>
            </div>
        </body>
    </text>
</TEI>