<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg101" n="/Literatur/M/Bechstein, Ludwig/Märchen/Deutsches Märchenbuch/Die Hexe und die Königskinder">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Die Hexe und die Königskinder</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Bechstein, Ludwig: Element 00105 [2011/07/11 at 20:31:17]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Ludwig Bechstein: Sämtliche Märchen. Mit Anmerkungen und einem Nachwort von Walter Scherf, München: Winkler, 1971.</title>
                        <author key="pnd:118654292">Bechstein, Ludwig</author>
                    </titleStmt>
                    <extent>208-</extent>
                    <publicationStmt>
                        <date when="1971"/>
                        <pubPlace>München</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1801" notAfter="1860"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg101.3">
                <div type="h4">
                    <head type="h4" xml:id="tg101.3.1">Die Hexe und die Königskinder</head>
                    <p xml:id="tg101.3.2">Mitten in einem Walde wohnte eine alte schlimme Hexe ganz allein mit ihrer Tochter, welche letztere ein gutes, mildes Kind war, und bei der das Sprüchwort: der Apfel fällt nicht weit vom Stamme, nicht zutraf. Der Stamm nämlich war über alle Maßen knorrig, stachlich und häßlich; wer die Alte sah, ging ihr aus dem Wege, und dachte: Weit davon ist gut vorm Schuß. Die Alte trug beständig eine grüne Brille, und über ihrem Zottelhaar, das ungekämmt ihr vom Kopfe weit herunter hing, einen roten Tuchlappen, und ging gern in kurzen Ärmeln, daß ihre dürren wettergebräunten Arme weit aus dem sie umschlotternden Gewand hervorragten. Auf dem Rücken trug sie für gewöhnlich einen Sack mit Zauberkräutern, die sie im Walde sammelte, und in der Hand einen großen Topf, darin sie dieselben kochte, und damit Ungewitter, Hagel und Schlossen, Reif und Frost zu Wege brachte, so oft es ihr beliebte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg101.3.3">Am Finger trug sie einen Hexenreif von Golde mit einem glühroten Karfunkelstein, mit dem sie Menschen und Tiere bezaubern konnte. Dieser Ring machte die Alte riesenstark und lebenskräftig, und machte sie, wenn sie wollte, auch ganz und gar unsichtbar; da konnte sie hingehen wohin sie wollte, und nehmen was sie wollte – und das tat sie auch, und im Walde suchte sie die Hirschkühe auf, und wenn die Tiere den Ring sahen und sahen den Stein funkeln, da mußten sie an eine Stelle gebannt stehen bleiben, und dann ging die Alte zu den Hirschkühen und molk deren Milch in ihren Topf, und trank sie mit ihrer Tochter. Diese Tochter hieß Käthchen, und hatte es nicht gut bei <pb n="208" xml:id="tg101.3.3.1"/>
ihrer bösen Mutter, doch trug sie geduldig alles Leid. Am schmerzlichsten war ihr, daß ihre Mutter manchesmal Kinder mitbrachte, mit denen Käthchen gern gespielt hätte, allein die Alte nahm immer den Kindern ihre Kleider, sperrte die Kinder ein und fütterte sie mit Hirschmilch, daß sie fett wurden, und was sie dann mit ihnen vornahm, ist gruselig zu erzählen; sie verwandelte sie nämlich in Hirschkälbchen und verkaufte diese an Jäger. Die Jäger aber schossen die armen verwandelten und verkauften Hirschkälbchen tot, und lieferten sie in die Stadt, wo die Leute das junge Wildpret gar gern essen. So schlimm und böse war die häßliche Alte, und da sie den ganzen Tag nichts tat, als zaubern und böse Ränke ersinnen, und dabei oft und viel laut vor sich hin murmelte, so lernte ihre Tochter Käthchen ihr unvermerkt einige Zauberstücklein ab, die sie ganz im stillen für sich behielt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg101.3.4">Da brachte eines Abends die Alte wieder zwei wunderschöne Kinder geführt, einen Knaben und ein Mädchen, denen sah man an, daß es Geschwister waren, und reicher Leute Kinder; beide hatten sich im Walde verirrt, waren von der Alten gefunden, und nach ihrem Hause mitgenommen worden, und sie hatte ihnen gesagt, sie wolle sie zurück zu den Eltern bringen. Die Kinder sahen sich schrecklich <pb n="209" xml:id="tg101.3.4.1"/>
getäuscht, als die Alte ihnen ihre schönen Kleider auszog, ihnen dafür Lumpen anlegte, und sie in ein dunkles Kämmerchen einsperrte. Doch bekamen sie einen ganzen Topf voll Hirschmilch zu essen, welche gut schmeckte, und ein Stück schwarzes Brot dazu, welches weniger gut schmeckte, aber endlich doch auch verzehrt wurde.</p>
                    <p rend="zenoPLm4n0" xml:id="tg101.3.5">Am andern Morgen humpelte die Alte schon frühzeitig in den Wald, und winkte den Hirschkühen. Da war eine Hirschfamilie, welche die Alte besonders gut kannte und schätzte, bestehend aus dem Herrn Hirsch, der Frau Hirschin und zwei jungen Kälbchen, die hielten sich immer treulich im Walde zusammen, waren aber doch in steter Furcht vor der bösen Alten, welche machen konnte, daß sie alle still stehen mußten, und mußten sich von der bösen Hexe die Muttermilch nehmen lassen, so daß die Kälbchen sich nicht satt trinken und nicht fett werden konnten. Könnt ich dir nur einmal mein Geweih durch den dürren Leib rennen! dachte oft der Hirsch, und die Hirschin hatte auch keine guten Wünsche für die Alte – es half aber ihr Wünschen allen beiden nichts. Während die Alte im Walde war, schlich Käthchen zu dem Kämmerlein, und sah durch eine Ritze in der Tür die armen gefangenen Kinder, welche seufzten und weinten, in großem Herzeleid. Da fragte Käthchen: »Wer seid ihr denn, ihr armen Kinder?« – »Wir sind eines Königs Kinder! O mache uns frei, mein Vater wird es dir lohnen!« – sprach der Königsprinz. »Und meine Mutter auch« – sagte die kleine Prinzessin, indem sie hinzufügte: »Du sollst auch unsre gute Schwester sein, und sollst bei mir im seidnen Bettchen schlafen, und ich will dir gar schöne goldne Kleider geben, hilf uns, hilf uns nur!« – Da sagte Käthchen: »Seid nur geduldig, liebe Königskinder; ich will schon zusehen, und darauf sinnen, daß ich euch befreie.« –</p>
                    <p rend="zenoPLm4n0" xml:id="tg101.3.6">Am andern Morgen in aller Frühe machte das gute Käthchen ein Zauberstück. Sie verließ eilig ihr Lager, hauchte hinein, und sagte leise:</p>
                    <lb xml:id="tg101.3.7"/>
                    <lg>
                        <l xml:id="tg101.3.8">»Liebes Bettchen, sprich für mich,</l>
                        <l xml:id="tg101.3.9">Bin ich weg, sei du mein Ich!«</l>
                    </lg>
                    <lb xml:id="tg101.3.10"/>
                    <p xml:id="tg101.3.11">So auch hauchte sie auf ihre Lade, auf die Treppe, und auf den Herd in der Küche, und sprach das nämliche Sprüchlein. Darauf ging sie an das wohlverwahrte Kämmerlein der <pb n="210" xml:id="tg101.3.11.1"/>
Königskinder, hielt eine Springwurzel, welche die Alte auf dem Kannrück liegen hatte, an das Schloß und sagte:</p>
                    <lb xml:id="tg101.3.12"/>
                    <lg>
                        <l xml:id="tg101.3.13">»Riegel, Riegel, Riegelein,</l>
                        <l xml:id="tg101.3.14">Öffne dich, laß aus und ein!«</l>
                    </lg>
                    <lb xml:id="tg101.3.15"/>
                    <p xml:id="tg101.3.16">Da sprangen gleich Schloß und Riegel auf, und Käthchen führte alsbald die Königskinder hinweg und in den Wald hinein.</p>
                    <p rend="zenoPLm4n0" xml:id="tg101.3.17">Als die Alte aufwachte, rief sie: »Käthchen, stehe auf und schüre Feuer an!« – da rief es aus dem Bettchen:</p>
                    <lb xml:id="tg101.3.18"/>
                    <lg>
                        <l xml:id="tg101.3.19">»Ich bin schon auf und munter!</l>
                        <l xml:id="tg101.3.20">Ich komme gleich in die Küche hinunter!«</l>
                    </lg>
                    <lb xml:id="tg101.3.21"/>
                    <p xml:id="tg101.3.22">Die Alte blieb nun noch liegen, doch da sie nach einer Weile nichts hörte, rief sie wieder: »Käthchen! Wo bleibt denn das faule Ding?« – »Gleich!« rief es von der Lade:</p>
                    <lb xml:id="tg101.3.23"/>
                    <lg>
                        <l xml:id="tg101.3.24">»Ich sitze auf der Lade</l>
                        <l xml:id="tg101.3.25">Und binde das Strumpfband über die Wade!«</l>
                    </lg>
                    <lb xml:id="tg101.3.26"/>
                    <p xml:id="tg101.3.27">Da nun wieder eine Weile verging, und sich im Hause nichts rührte noch regte, so ward die Alte böse, und schrie: »Käthe! Balg! Wo bleibst du denn?« Da scholl eine Stimme von der Treppe:</p>
                    <lb xml:id="tg101.3.28"/>
                    <lg>
                        <l xml:id="tg101.3.29">»Ich komme schon, ich fliege!</l>
                        <l xml:id="tg101.3.30">Ich bin ja schon leibhaftig auf der Stiege!«</l>
                    </lg>
                    <lb xml:id="tg101.3.31"/>
                    <p xml:id="tg101.3.32">Die Alte beruhigte sich noch einmal – aber nicht gar lange, denn da wieder alles still blieb, so fuhr sie auf und schalt und fluchte. Da rief es vom Herde her:</p>
                    <lb xml:id="tg101.3.33"/>
                    <lg>
                        <l xml:id="tg101.3.34">»Wozu die bösen Flüche?</l>
                        <l xml:id="tg101.3.35">Ich bin ja schon am Herd und in der Küche!«</l>
                    </lg>
                    <lb xml:id="tg101.3.36"/>
                    <p xml:id="tg101.3.37">gleichwohl blieb es in der Küche und im ganzen Hause totenstill. Jetzt riß der Alten völlig der Geduldsfaden, sie sprang aus ihrem Bett, fuhr in die Kleider und nahm einen Besenstiel, willens Käthchen unbarmherzig durchzuprügeln. Aber wie sie hinauskam, war kein Käthchen da, nicht zu sehen, nicht zu hören, und was das Schönste, für die Alte aber das Schlimmste war, auch die Königskinder waren fort. Jetzt hättet ihr sollen die Hexensprünge sehen, welche das zornige böse alte Weib machte. Ihr Ring zeigte ihr sogleich die Richtung an, nach welcher Käthchen mit den Kindern <pb n="211" xml:id="tg101.3.37.1"/>
geflohen war, und sie raste nun wild hinter ihnen her. Die Kinder aber, als sie in den Wald gekommen waren, hatten dort den Herrn von Edelhirsch nebst Gemahlin, Sohn und Tochter, angetroffen, und dieser Familie in aller Eile ihr Unglück und ihre Flucht erzählt und ihre edlen Herzen mächtig gerührt, so daß sie sich bereit zeigten, ihnen alle mögliche Hülfe angedeihen zu lassen. Die gute Dame Hirsch bot den Kindern ihren Rücken dar, sie alle drei nach dem Königsschlosse zu tragen, das jenseit des Waldes lag, und der Gemahl befahl seinen Kindern, sich in das Dickicht zurückzuziehen, er selbst stellte sich hinter dichtes Laubgebüsch nahe am Weg, willens die Alte, wenn sie vorbeirenne, und er ihren Ring nicht sehe, über den Haufen zu stoßen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg101.3.38">Es währte auch gar nicht lange, so kam die Alte in großen Sprüngen gesetzt; in ihrem Zorn und Eifer vergaß sie ganz, unsichtbar sein zu wollen, hielt auch den Finger mit dem Ring nicht empor, und so geschah es, daß plötzlich ein großes und stattliches Hirschgeweih mit ihr in eine sehr verwickelte Berührung kam, bei welcher eines der Enden des Geweihes mit Gewalt den Finger der Alten so streifte, daß der Zauberring vom Finger herabging und sich auf dem Ende feststeckte, und ehe sich's einer versah, so hatte der Hirsch die alte Hexe aufgegabelt, die nun durch des Ringes Kraft selbst starr und steif wurde, und trug sie in gestrecktem Lauf der Fährte nach, welche die gute Hinde, seine Gemahlin, im tauigen Grase zurückgelassen. Diese war indes mit den drei Kindern bereits im Königsschloß angekommen, und von dem König und der Königin waren die verlorenen Kinder und das gute Käthchen, das sie gerettet, mit großer <pb n="212" xml:id="tg101.3.38.1"/>
Freude empfangen worden – als sie plötzlich alle mit großer Verwunderung die Alte auf dem Geweih des stattlichen Edelhirsches sitzend und getragen daher schweben sahen. Der Hirsch aber sprang ohne Säumen in den Schloßteich, und tauchte mit dem Kopfe unter. Als er wieder auftauchte, war sein Geweih frei von der Last. Aber auch der Zauberring blieb im Grunde. Hirsch und Hirschin kehrten zu ihrem Walde und zu ihren Kindern zurück, und waren sehr froh, daß ihnen nun niemand mehr ihre Milch nahm; Käthchen aber blieb bei den Königskindern, und schlief in einem seidnen Bettchen und trug goldne Kleidchen und wurde selbst gehalten, wie ein Königskind.</p>
                </div>
            </div>
        </body>
    </text>
</TEI>