<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg2115" n="/Literatur/M/Strackerjan, Ludwig/Sagen/Aberglaube und Sagen aus dem Herzogtum Oldenburg/Zweiter Band/Drittes Buch/Dritter Abschnitt/P. Anhang/614. Graf Anton Günther (1603-1667)/d. [Wenn Anton Günther auf der Jagd war, verschmähte er es nicht, gelegentlich]">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>d. [Wenn Anton Günther auf der Jagd war, verschmähte er es nicht, gelegentlich]</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Strackerjan, Ludwig: Element 02406 [2011/07/11 at 20:28:54]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Ludwig Strackerjan: Aberglaube und Sagen aus dem Herzogtum Oldenburg 1–2. ed. K. Willoh., Band 2, 2. Aufl., Oldenburg: Stalling, 1909.</title>
                        <author key="pnd:117305553">Strackerjan, Ludwig</author>
                    </titleStmt>
                    <extent>417-</extent>
                    <publicationStmt>
                        <date>1909</date>
                        <pubPlace>Oldenburg</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1825" notAfter="1881"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg2115.2">
                <div type="h4">
                    <head type="h4" xml:id="tg2115.2.1">d.</head>
                    <p xml:id="tg2115.2.2">Wenn Anton Günther auf der Jagd war, verschmähte er es nicht, gelegentlich bei einem Bauern einzusprechen und bei ihm ein Mahl einzunehmen. Ein Hausmann von Wechloy, ein Vorfahr des jetzigen Hausmanns G. Bruns, bei dem er auch wohl einzeln einen Imbiß genommen hatte, kam einst zu ihm aufs Schloß, um ihm eine Sache vorzutragen. Der Graf bemerkte, daß des Mannes Augen während der Unterredung oft auf die im Zimmer stehenden vergoldeten Stühle gerichtet waren. »Gefallen euch die Stühle?« fragte er. »Sie sind prächtig«, war die Antwort, »aber Euer Gnaden sollen in meinem Hause doch noch einen bessern Stuhl finden.« Als bald darauf der Graf wieder einmal bei ihm essen wollte, fand er einen sehr bequemen Sitz von vier gefüllten Kornsäcken bereitet. Da fiel ihm jene Antwort wieder ein. »Recht so!« sagte er, »der Stuhl ist besser als einer von den meinigen.« Dann setzte er sich auf den bereiteten Sitz und ließ sichs wohlschmecken. Nach v. Halem, Oldb. Gesch. II., S. 508. In dieser Form ist die Erzählung am bekanntesten. Die Überlieferung kennt aber noch einen weiteren Zug. Auch die silbernen und porzellanenen Teller des gräflichen Tisches waren ihrer Pracht wegen bei dem Besuche des Wechloyer Hausmanns auf dem Schlosse zu Oldenburg besprochen worden. Als nun der Graf bei dem Hausmann speiste, hatte dieser gar absonderliche Teller anfertigen lassen. Es waren die Krusten hart gebrannten Brotes, von allen weichen Teilen gehörig gereinigt und ganz blank geputzt. »Die Teller«, sagte der Bauer, »sind wohl so gut als die eurigen, Herr Graf, und hättet ihr auch Teller von Diamant; denn wenn es einmal schlimm kommt, so könnt ihr die ganzen Teller mit verzehren.« Und der Graf lachte und gab ihm Recht.</p>
                    <p rend="zenoPLm4n0" xml:id="tg2115.2.3">Wie viel Oldenburger mag es geben, welche die Wahrheit der Sage, so weit sie von Halem mitteilt, bezweifeln? Wir können aber nicht verschweigen, daß bereits um das Jahr 1655 eine ähnliche, aber weiter ausgeführte Erzählung niedergeschrieben ist, die den Schauplatz nach Schleswig und in weit zurückreichende Zeiten verlegt, und der Schreiber, ein durchaus glaubhafter Mann, versichert, daß ihm das Geschichtchen von einen Eltern, seinem Großvater und anderen Verwandten als <pb n="417" xml:id="tg2115.2.3.1"/>
wahr bezeichnet sei; er selbst freilich glaubt nicht daran. In dem friesischen Küstenstriche des westlichen Schleswig, so heißt es, wohnte ein großer Bauer mit Namen Hatto. Dieser ritt einstmals auf seinem Bauernpferde nach Gottorp, dem Wohnsitze der schleswigschen Grafen, um dem Grafen Adolf VI. (der im Jahre 1227 in der Schlacht bei Bornhöved König Waldemar II. von Dänemark besiegen half) einen Besuch zu machen, und weil er wegen seines Reichtums und Verstandes bei dem Grafen sehr gern gesehen war, wurde er nicht nur zugelassen, sondern auch mit besonderer Pracht an des Grafen Tische wiederholt bewirtet. Als er Abschied nahm, bedankte er sich bei dem Grafen für die genossene Gastfreundschaft und lud ihn ein, auch einmal zu ihm als Gast zu kommen; wenn sein Tisch an Speise und Trank sich auch mit dem Reichtume des gräflichen Tisches nicht messen könne, so wolle er ihm zu Hause doch Stühle und eine lustige Musik schaffen, die besser seien als die des Grafen. Die Hofleute lachten über die Prahlerei, aber der leutselige Graf nahm die Einladung an und versprach, an einem bestimmten Tage mit einigen seiner Leute sich einzufinden. Gegen Ende des Frühlings machte der Graf mit sechs Edelleuten und sieben Dienern sich auf den Weg und ließ am Abend vorher sich bei Hatto anmelden, andern Morgens früh werde er mit einigen Begleitern kommen. Hatto befahl die Diele gehörig zu fegen und mit Sand zu bestreuen; die Schweine mit ihren Ferkeln, die Schafe und Lämmer, die Kühe und Kälber ließ er in einen Pferch zusammensperren. Auf der Diele wurde ein langer Tisch von Eichenholz aufgestellt. Die Stühle aber waren eigener Art. Für den Grafen wurde ein Sack mit Weizen, der zwei Tonnen hielt, für die Begleiter Säcke von einer Tonne Inhalt hingelegt, hinter diesen Säcken standen andere, die als Lehne dienten. Als nun am andern Morgen der Graf von Husum her sich näherte, ging ihm Hatto mit einigen angesehenen Angehörigen entgegen und führte ihn ruhig und ernsten Gesichtes in sein Haus, bedankte sich wegen der Herablassung des vornehmen Gastes und bat ihn und seine Begleiter, an dem Tische Platz zu nehmen. Die Säcke aber waren mit bunten Decken und Kissen so wohl verhüllt, daß niemand ahnte, was ihm zum Sitze diene. Hatto ließ nun durch seine fünf bereits erwachsenen Söhne das Mahl auftragen. Der erste Gang bestand nach friesischer Sitte aus Schinken. Dann <pb n="418" xml:id="tg2115.2.3.2"/>
folgte Steinbutt mit Butter und Essig, zum dritten Rauchfleisch mit Senf, hiernächst Süßwasserfische aus Hattos eigenem Teiche und endlich gebratene Gänse, Enten, Küken, Ferkel und Hechte. Zu allem gab es Roggenbrod, das damals noch allgemein beliebt war. Nach beendetem Mahle kam der Nachtisch, bestehend aus Pfefferkuchen und anderen würzigen Sachen, die den Durst reizen. Als Getränk wurde Bier vorgesetzt, das Hattos Landleute, wie der Erzähler einfließen läßt, nicht selten dem Weine vorzogen. Endlich erhob sich der Graf, dankte für die Gastfreundschaft und fragte lächelnd: »Aber wo sind denn die Stühle und die besondere Musik, womit du Schloß Gottorp übertreffen wolltest?« Da deckte Hatto die Säcke auf, zeigte den Weizen und sprach: »Das sind gewiß Stühle, die nützlicher sind und auch mehr kosten als Holz und Stein, die mit Gold oder Silber geziert sind.« »Du hast Recht«, erwiderte der Graf, »aber nun laß uns auch die besondere Musik einmal hören, von der du rühmtest.« Da ließ Hatto den Pferch öffnen, in den er sein Vieh eingesperrt hatte, und Kälber, Ferkel und Lämmer stürzten auf den Hof, brüllend, grunzend und blökend, und tummelten sich durcheinander in allerlei drolligen Sprüngen und Sätzen, ihrer Natur gemäß und des wieder gewonnenen Raumes sich freuend. Das wirkte auf die schon vorher fröhlichen Gäste so erheiternd, daß sie in ein unauslöschliches Gelächter ausbrachen und sich kaum wieder zu fassen vermochten. Da erklärte sich der Graf für besiegt und schenkte seinem Wirte das ganze reiche Dorf Hattstede, das hernach von Hatto seinen Namen erhielt, und die benachbarte Marsch mit nur ganz geringen Abgaben und Lasten. Hatto aber soll in hohem Alter in einem Anfalle heftigen Zornes seinen jüngsten Sohn erschlagen haben, darüber in Wahnsinn verfallen und in diesem auch bis an seinen Tod verblieben sein. (Abgekürzt nach Matth. Paysen, Rektors zu Oldeslö, handschriftl. Notizen zu <hi rend="italic" xml:id="tg2115.2.3.3">Saxo grammat.</hi> in der Privatbibliothek des Großherzogs von Oldenburg, Nr. 6.)</p>
                    <p rend="zenoPLm4n0" xml:id="tg2115.2.4">Stärker aufgetragen, aber weniger sinnreich heißt es von einem Bauern zu Niclauswalde in der Weichselniederung, er habe, als er den Hochmeister des deutschen Ordens Konrad von Jungingen nebst andern vornehmen Herren bewirtet, um den Tisch Bänke hergerichtet, die auf zwölf Tonnen standen. Elf von diesen Tonnen enthielten Gold, die zwölfte war leer. Der Hochmeister ließ auch die zwölfte Tonne mit Gold aus <pb n="419" xml:id="tg2115.2.4.1"/>
dem Schatze des Ordens anfüllen, aber dennoch ist der Bauer als Bettler gestorben. (Nach Simon Grunau bei von Tettau und Temme, die Volkssagen Ostpreußens etc., S. 92.) Jetzt lautet die Sage in Westpreußen dahin, daß ein reicher Bauer aus der Weichselniederung einmal den König Friedrich Wilhelm I. bewirtet habe, wobei der Tisch auf Fässern voll blanker Silbertaler ruhte und jeder Gast auf einem ähnlichen Fasse saß. (Fr. Tietz im Feuilleton der Berliner Zeitung »Die Post«, 1867 Nr. 324.)</p>
                </div>
            </div>
        </body>
    </text>
</TEI>