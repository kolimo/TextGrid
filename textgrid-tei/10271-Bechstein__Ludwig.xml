<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg179" n="/Literatur/M/Bechstein, Ludwig/Märchen/Neues deutsches Märchenbuch/Von einem Einsiedel und drei Gaunern">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Von einem Einsiedel und drei Gaunern</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Bechstein, Ludwig: Element 00184 [2011/07/11 at 20:31:17]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Ludwig Bechstein: Sämtliche Märchen. Mit Anmerkungen und einem Nachwort von Walter Scherf, München: Winkler, 1971.</title>
                        <author key="pnd:118654292">Bechstein, Ludwig</author>
                    </titleStmt>
                    <extent>665-</extent>
                    <publicationStmt>
                        <date when="1971"/>
                        <pubPlace>München</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1801" notAfter="1860"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg179.3">
                <div type="h4">
                    <head type="h4" xml:id="tg179.3.1">Von einem Einsiedel und drei Gaunern</head>
                    <p xml:id="tg179.3.2">»Ein kluger Mann vollbringt durch Einsicht und überlegten Entschluß, was manchem stärkeren mißlingt!« sagte der weise Rabe, des Rabenkönigs Ratgeber, zu diesem letzteren. »Ich muß dabei jener Gauner gedenken, die mit ihrer List und Schlauheit einen Einsiedel also täuschten, daß er das nicht mehr glaubte, was doch seine Augen sahen.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg179.3.3">»Wie geschahe das?« fragte der König, und der Rabe antwortete:</p>
                    <p rend="zenoPLm4n0" xml:id="tg179.3.4">»Es war einmal ein Einsiedel, der ging und kaufte sich eine Geiß, sie bei seiner Hütte zu halten und ihre Milch zu genießen. Das sahen von weitem drei Diebe und besprachen sich untereinander, wie sie sonder Gewalt den Waldbruder um die Geiß betrügen möchten. Sie verteilten sich alsbald so, daß einer nach dem andern dem Einsiedel begegnete, in kurzen Fristen hintereinander. Der erste, welcher zu ihm kam, bot ihm die Zeit, und sagte spöttisch: ›Waldbruder! Ihr sorget Euch gewiß, daß die Diebe Euch Eure Schätze stehlen wollen, weil Ihr Euch einen Hund gekauft habt. Was wollt Ihr mit dem Hunde tun?‹ – ›Es ist kein Hund, es ist eine Ziege!‹ sagte der Einsiedel gelassen, aber jener behauptete steif und fest, es sei ein Hund, bis der zweite Gauner hinzukam, und auch grüßte, und ebenfalls fragte, was der fromme Waldbruder mit dem Hunde tun wolle? ›Ein heiliger Mann‹, sagte er: ›muß sich nicht mit einem so unreinen Tiere befassen; ich täte mich seiner sicherlich und ohne Säumen ab. Eines Hundes Gebell stört Gebet und Andacht, und nirgend steht geschrieben, daß die heiligen Apostel Hunde geführt oder sich gar mit solchem Getier getragen hätten!‹</p>
                    <p rend="zenoPLm4n0" xml:id="tg179.3.5">Jetzt kam der dritte Schalk hinzu, als jene drei noch über den vorgeblichen Hund stritten, und sprach: ›Aha! Ihr habt hier einen Hundehandel! Was soll der Köter gelten? Ich suche just ein solches Vieh zu kaufen.‹</p>
                    <p rend="zenoPLm4n0" xml:id="tg179.3.6">Jetzt glaubte der Einsiedel allen Ernstes, seine Geiß sei ein Hund, und der sie ihm verkauft, habe ihn betrogen, und da warf er im Zorn die Geiß hin, und eilte von dannen, seiner Klause zu, wo er sich wusch und säuberte. Die drei Gauner aber nahmen die Geiß, trugen sie heim, schlachteten <pb n="665" xml:id="tg179.3.6.1"/>
und brieten sie, und ließen sich den Braten gut schmecken, indem sie des Einsiedels Einfalt noch lange belachten. –</p>
                    <p rend="zenoPLm4n0" xml:id="tg179.3.7">»Dieses sagte ich dir, mein König«, fuhr der weise Rabe fort: »auf daß du erwägest, daß, wie klug und mächtig auch die Adler sind, wir mit List und Schlauheit uns ihrer dennoch entledigen können.</p>
                    <p rend="zenoPLm4n0" xml:id="tg179.3.8">Und nun, o König, sage ich dir erst mein eigentliches Geheimnis, denn die Ursache der Feindschaft zwischen den Adlern und den Raben ist vielen kundig und von unserer Väter Überlieferung her noch manchem Alten im Gedächtnis. Mein Rat, den ich dir jetzt gebe, muß zwischen dir und mir das tiefste Geheimnis bleiben. Erstens überschütte mich vor den andern mit der scheinbaren Zornschale deiner Ungnade; tue als habe ich dir falschen und böslichen Rat gegeben, hacke auf mich vor dem ganzen Hofhalte, verwunde mich und laß mich auf der Erde liegen, dann erhebe dich mit deinem gesamten Volke, flieget von dannen so weit, daß man keinen Raben mehr ringsum erblicke, und haltet euch an einem andern Orte so lange still, bis ich wieder zu dir zurück kehre, und dir gute Botschaft ansage.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg179.3.9">Diesen Rat befolgte der König der Raben. Und als die Kundschafter der Adler wahrgenommen, daß das Volk der Raben samt seinem Könige sich von dannen gehoben, so kamen sie in Scharen samt ihrem Könige nach dem Rabenwalde, und zerstörten der Raben Nester, und einer unter ihnen sah den verwundeten Raben unter einem Baume liegen und flog zu ihm nieder.</p>
                </div>
            </div>
        </body>
    </text>
</TEI>