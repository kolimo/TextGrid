<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg67" n="/Literatur/M/Altenberg, Peter/Prosa/Wie ich es sehe/Ein schweres Herz">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Ein schweres Herz</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Altenberg, Peter: Element 00074 [2011/07/11 at 20:31:54]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Peter Altenberg: Wie ich es sehe. 8.–9. Auflage, Berlin: S. Fischer, 1914.</title>
                        <author key="pnd:118502255">Altenberg, Peter</author>
                    </titleStmt>
                    <extent>196-</extent>
                    <publicationStmt>
                        <date>1914</date>
                        <pubPlace>Berlin</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1859" notAfter="1919"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg67.3">
                <div type="h4">
                    <head type="h4" xml:id="tg67.3.1">Ein schweres Herz</head>
                    <p xml:id="tg67.3.2">Es steht mitten zwischen Wiesen und Obstgärten ein riesiges gelbes Haus. Es ist ein Mädchen-Institut der »Englischen Fräulein«. Es giebt viele »heilige Schwestern« darin und viel Heimweh.</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.3">Manchesmal kommen die Väter an, besuchen ihre Töchterchen. »Papa, grüss' Dich Gott – – –.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.4">In dieser einfachen Musik »Papa, grüss' Dich Gott – – –« liegen die tiefen Hymnen der kleinen Herzen. Und in »adieu, Papa – –« verklingen sie wie Harfen-Arpeggien!</p>
                    <lb xml:id="tg67.3.5"/>
                    <p xml:id="tg67.3.6">Es war ein regnerischer Land-November-Sonntag. Ich sass in dem lieben kleinen warmen Café und rauchte und träumte – – –.</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.7">Ein schöner grosser Herr trat ein mit einem kleinen wunderbaren Mädchen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.8">Es war eigentlich ein Engel ohne Flügel, in einer gelbgrünen Sammt-Jacke.</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.9">Der Herr nahm an meinem Tische Platz.</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.10">»Bringen Sie ›Illustrierte Zeitungen‹ für die Kleine« sagte er zu dem Marqueur.</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.11">»Danke, Papa, ich möchte keine – – –« sagte der Engel ohne Flügel.</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.12">Stille – – –.</p>
                    <pb n="196" xml:id="tg67.3.13"/>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.14">Der Vater sagte: »Was hast Du – – –?!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.15">»Nichts – – –« sagte das Kind.</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.16">Dann sagte der Vater: »Wo seid Ihr in Mathematik?!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.17">Er meinte: »Sprechen wir über etwas Allgemeines. In der Wissenschaft findet man sich – – –.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.18">»Capital-Rechnungen« sagte der Engel. »Was ist es?! Was bedeutet es?! Ich habe keine Idee. Wozu braucht man Capitals-Rechnungen?! Ich verstehe das nicht – – –.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.19">»Lange Haare – Kurzer Verstand« sagte der Vater lächelnd und streichelte ihre hellblonden Haare, welche wie Seide glänzten.</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.20">»Jawohl – –« sagte sie.</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.21">Stille – – –.</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.22">Ich habe ein so trauriges Gesichterl nie gesehen! Es erbebte gleichsam wie ein Strauch unter Schnee-Last. Es war, wie wenn Eleonora Duse sagt: »oh –!« Oder wenn Gemma Bellincioni es singt – – –.</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.23">Der Vater dachte: »Geistige Arbeit ist eine Ablenkung. Und jedesfalls, kann es schaden?! Man wiegt die Seele ein – –. Man muss das Interesse wecken. Natürlich schläft es noch – – –.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.24">Er sagte: »Capitals-Rechnungen! Oh, es ist interessant. Das war seinerzeit meine Force (ein Schimmer des vergangenen Capitalrechnung-Glückes huschte über sein Antlitz). Zum Beispiel – – warte ein bischen – – zum Beispiel Jemand kauft ein Haus.</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.25">Hörst Du zu?!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.26">»Oh ja. Jemand kauft ein Haus.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.27">
                        <pb n="197" xml:id="tg67.3.27.1"/>
»Zum Beispiel euer Geburtshaus in Görz. (Er machte die Sache spannender, indem er geschickt Wissenschaft und Familienverhältnisse in eine ziemlich nahe Beziehung brachte). Es kostet 20 000 Gulden. Wie viel muss er an Zins einnehmen, damit es 5% trage?!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.28">Der Engel sagte: »Das kann Niemand wissen – –. Papa, kommt Onkel Viktor noch oft zu Uns?!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.29">»Nein, er kommt selten. Wenn er kommt, setzt er sich immer in dein leeres Zimmer. Merke auf. 20 000 Gulden. Wieviel ist 5% bei 20 000 fl.?! Nun, doch jedesfalls soviel mal 5 Gulden, als hundert in 20 000 enthalten ist!? Das ist doch einfach, nicht?!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.30">»Oh ja – – –« sagte das Kind und begriff nicht, warum Onkel Victor so selten komme.</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.31">Der Vater sagte: »Also wieviel muss er einnehmen?! Nun, 1000 Gulden ganz einfach.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.32">»Ja, 1 000 Gulden. Papa, raucht die grosse weisse Lampe im Speisezimmer noch immer beim Anzünden?!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.33">»Natürlich. Also hast Du jetzt eine Idee von Capitalsrechnung?!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.34">»Oh ja. Aber wieso trägt Geld überhaupt Zinsen?! Es ist doch nicht wie ein Birnbaum?! Es ist doch ganz todt, Geld.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.35">»Dummerl – – –« sagte der Vater und dachte: »Uebrigens, es ist Sache des Institutes.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.36">Stille – – –.</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.37">Sie sagte leise: »Ich möchte nach Hause zu Euch – –.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.38">»No, Du bist doch ein gescheidtes Mäderl, nicht –?!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.39">
                        <pb n="198" xml:id="tg67.3.39.1"/>
Zwei Thränen kamen langsam die Wangen heruntergeschwommen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.40">Erlösung! Thränen! Schimmernde Perlen gewordenes Heimweh!!</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.41">Dann sagte sie lächelnd: »Papa, es sind drei kleine Mädchen im Institute. Die Älteste darf drei Buchteln essen, die Jüngere nur zwei und die Jüngste eine. So diätetisch sind sie! Ob sie nächstes Jahr gesteigert werden?!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.42">Der Vater lächelte: »Siehst Du, wie lustig es bei Euch ist?!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.43">»Wieso lustig?! Uns kommt es so vor, weil es lächerlich ist. Das Lächerliche ist doch nicht das Lustige?!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.44">»Kleine Philosophin – –« sagte der Vater glücklich und stolz und sah an den feuchtglänzenden Augen seines Töchterchens, dass Philosophie und Leben Zweierlei seien.</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.45">Sie wurde rosig und bleich, bleich und rosig –. Wie ein Kampf war es auf diesem süssen Antlitz. Es stand darauf geschrieben: »adieu Papa, oh, adieu –.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.46">Ich hätte dem Vater gerne gesagt: »Herr, schauen Sie dieses Marieen-Antlitz an! Sie hat ein brechendes kleines Herz! – – –.</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.47">Er hätte mir geantwortet: »Mein Herr, c'est la vie! So ist das Leben! Es können nicht alle Menschen im Caféhaus sitzen und vor sich hinträumen – –.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.48">Der Vater sagte: »Wie weit seid Ihr in Geschichte?!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.49">Er dachte: »Man muss sie ablenken. Das ist mein Princip.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.50">
                        <pb n="199" xml:id="tg67.3.50.1"/>
»Wir sind in Ägypten« sagte das kleine Mädchen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.51">»Oh, in Ägypten« sagte der Vater und machte, wie wenn dieses Land Einen wirklich ganz ausfüllen könne. Er war geradezu erstaunt, dass man sich noch etwas Anderes wünsche als Ägypten.</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.52">»Die Pyramiden« sagte er, »die Mumien, die Könige Sesostris, Cheops! Dann kommen die Assyrer, dann die Babylonier – – –.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.53">Er dachte: »Je mehr ich aufmarschiren lasse, desto besser.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.54">»So?!« sagte das Kind. Wie wenn man sagt: »Versunkene Völker – – –!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.55">»Wann habt Ihr Tanzen?!« sagte der Vater. Er dachte: »Tanzen ist ein lustiges Thema.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.56">»Heute – –.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.57">»Wann?!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.58">»Gleich, wenn Du weggefahren sein wirst. Dann ist Tanzen, von 7 – 8.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg67.3.59">»Oh, Tanzen ist sehr gesund. Tanze nur fleissig –.«</p>
                    <lb xml:id="tg67.3.60"/>
                    <p xml:id="tg67.3.61">Als der Herr sich erhob, um wegzugehen und mich freundlich grüsste, sagte ich: »Verzeihen Sie, mein Herr, oh verzeihen Sie mir, ich habe eine grosse, grosse Bitte an Sie – – –.«</p>
                    <lg>
                        <l rend="zenoPLm4n0" xml:id="tg67.3.62">»An mich?! Was ist es?!«</l>
                        <l rend="zenoPLm4n0" xml:id="tg67.3.63">»Oh bitte, lassen Sie heute Ihr Töchterchen von der Tanzstunde dispensiren.«</l>
                        <l rend="zenoPLm4n0" xml:id="tg67.3.64">Er sah mich an – – – und drückte mir die Hand.</l>
                        <l rend="zenoPLm4n0" xml:id="tg67.3.65">
                            <pb n="200" xml:id="tg67.3.65.1"/>
»Gewährt!«</l>
                        <l rend="zenoPLm4n0" xml:id="tg67.3.66">»Wieso verstehst Du mich, fremder Mensch?!« sagte der Engel zu mir mit seinen schimmernden Augen.</l>
                        <l rend="zenoPLm4n0" xml:id="tg67.3.67">»Gehe voraus – – –« sagte der Herr zu dem Kinde.</l>
                        <l rend="zenoPLm4n0" xml:id="tg67.3.68">Dann sagte er zu mir: »Pardon, halten Sie es für ein richtiges Prinzip?!«</l>
                        <l rend="zenoPLm4n0" xml:id="tg67.3.69">»Jawohl« sagte ich, »was <hi rend="italic" xml:id="tg67.3.69.1">die Seele betrifft, ist das einzige Prinzip, keine Prinzipien zu haben!</hi>«</l>
                        <pb n="201" xml:id="tg67.3.70"/>
                    </lg>
                </div>
            </div>
        </body>
    </text>
</TEI>