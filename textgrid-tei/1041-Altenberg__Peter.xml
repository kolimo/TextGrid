<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg76" n="/Literatur/M/Altenberg, Peter/Prosa/Wie ich es sehe/Das Leiden">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Das Leiden</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Altenberg, Peter: Element 00083 [2011/07/11 at 20:31:54]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Peter Altenberg: Wie ich es sehe. 8.–9. Auflage, Berlin: S. Fischer, 1914.</title>
                        <author key="pnd:118502255">Altenberg, Peter</author>
                    </titleStmt>
                    <extent>226-</extent>
                    <publicationStmt>
                        <date>1914</date>
                        <pubPlace>Berlin</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1859" notAfter="1919"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg76.3">
                <div type="h4">
                    <head type="h4" xml:id="tg76.3.1">Das Leiden</head>
                    <p xml:id="tg76.3.2">Sie konnte von ihrem Ich nicht loskommen, sich nicht wegreissen von diesem riesigen ungeschlachten Titanen, der sie in seinen Fäusten hielt und niederdrückte – – –.</p>
                    <p rend="zenoPLm4n0" xml:id="tg76.3.3">Das ist Nervenkrankheit, oder vielmehr, das macht nervenkrank; Hypertrophie des Ich.</p>
                    <p rend="zenoPLm4n0" xml:id="tg76.3.4">Dieses »heilige Auslöschen des Ich« ist die Gesundheit des Weibes!</p>
                    <p rend="zenoPLm4n0" xml:id="tg76.3.5">»Ich habe mich verloren,« fühlt sie. Da hat sie sich gefunden.</p>
                    <p rend="zenoPLm4n0" xml:id="tg76.3.6">Um dieses Ich des Fräulein auszulöschen, auszublasen, wäre nicht einmal ein starker Sturmwind männlicher Liebe und Kraft ausreichend gewesen – – –.</p>
                    <p rend="zenoPLm4n0" xml:id="tg76.3.7">Prügel in der Kindheit und später – –! Das wäre die Medizin gewesen! Oder eine gute anhaltende festspannende Thätigkeit. Nein, am besten Prügel. Immer hatte man die Empfindung, ihr etwas herauszuprügeln, sie furchtbar, erbarmungslos zu prügeln, sie ganz einfach einer Prügelkur zu unterwerfen – –.</p>
                    <p rend="zenoPLm4n0" xml:id="tg76.3.8">Aber wie die gerechte Natur den Blinden entschädigt durch ein überzartes Gehör und feinen Tastsinn, so giebt sie uns für diese Krankheit des Ich einen wunderbar feinen Geist, Beobachtungsgabe, kurz erhöhte Intelligenz. Das Ich wird ein kostbares Objekt, an welchem man immer und unersättlich Studien <pb n="226" xml:id="tg76.3.8.1"/>
machen kann, sich belehren kann, forschen, wie ein Botaniker, ein Arzt, ein Chemiker. Man repräsentirt quasi die Welt mit allen ihren Freuden und Leiden ihren Sehnsuchten, ihrem stupiden Leben, ihrem schweren Absterben, ihrem heiligen Wiederwerden. Und indem man in dieses zarte complicirte Getriebe seines Ich seine nervösen geistvollen Blicke dringen lässt, erfährt man etwas von dem, was der geschäftige rastlose Nächstenliebende nie erfährt und wird wirklich ein »Sehender«. »So gleicht sich Alles aus – –« dachte sie.</p>
                    <p rend="zenoPLm4n0" xml:id="tg76.3.9">Trotzdem suchte sie Liebe wie ein fernes Heilmittel.</p>
                    <p rend="zenoPLm4n0" xml:id="tg76.3.10">Aber eigentlich wollte sie nur erfahren, was daran war.</p>
                    <p rend="zenoPLm4n0" xml:id="tg76.3.11">»Vielleicht giebt es mir Ruhe, Frieden – –?!« dachte sie.</p>
                    <p rend="zenoPLm4n0" xml:id="tg76.3.12">»Oder wie ist es mit der schönen sinnlichen Welt – –?! Was ist es mit dieser?! Vielleicht kann die nützen – –!?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg76.3.13">Immer betrachtete sie diese Dinge, die aus dem Innern geheimnisvoll und überraschend emporblühen sollen, wie Rezepte für irgend ein Leiden des Organismus. Für Fieber Chinin! Für Melancholie den sinnlichen Rausch, die Liebe! Sie nahm das ein wie Medizin, war misstrauisch, fühlte sich momentan erleichtert und plötzlich kämpfte sich die Krankheit wieder durch, zersplitterte die Feinde und packte sie selbst mit den unerbittlichen Fäusten und drückte sie zu Boden – – –. »Immer komme ich wieder zu mir – – –« fühlte sie, »wie schade!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg76.3.14">
                        <pb n="227" xml:id="tg76.3.14.1"/>
Vielleicht, wenn sie trinken würde, rauchen und – kurz, ihr Nervensystem schwächen und in einem unnatürlichen Rausche erhalten – – –??</p>
                    <p rend="zenoPLm4n0" xml:id="tg76.3.15">Aber so tobte dieses junge rothe Blut im Körper herum, schoss in's Gehirn, in's Herz, erzeugte Siedehitze und ernährte und reizte die Nerven, welche vor Kraft und Leben sprangen und schrieen – –.</p>
                    <p rend="zenoPLm4n0" xml:id="tg76.3.16">Aber das Leben um die Dampfmaschine herum war stumpf und so drehte sich das Ganze um sich selbst wie ein tollgewordener Kreisel, statt auf einem ungeheuren Schienenwege dahinzubrausen bis an eine Endstation. Endstation »Friede«.</p>
                    <p rend="zenoPLm4n0" xml:id="tg76.3.17">Immerhin gab es solche Ingenieure, welche diese ganze Maschinerie erkannten und wussten, wie es damit stand und dass das nicht so ein gewöhnlicher brutaler Heizkessel war wie viele andere.</p>
                    <p rend="zenoPLm4n0" xml:id="tg76.3.18">Aber weil sie keinen Schienenstrang anlegen konnten mit einer schönen gediegenen Endstation, betrachteten sie das Ganze von ihrem Standpunkte aus als eine höchst sehenswerthe, interessante, aber in der Anlage verpfuschte Arbeit.</p>
                    <p rend="zenoPLm4n0" xml:id="tg76.3.19">Und das war es schliesslich – – –.</p>
                    <p rend="zenoPLm4n0" xml:id="tg76.3.20">Denn die Gesundheit, das Glück, die Ruhe des Weibes ist das »sich verlieren«, »sich auslöschen« »verschwinden« – – –!</p>
                    <p rend="zenoPLm4n0" xml:id="tg76.3.21">Aber davon war keine Rede!</p>
                    <p rend="zenoPLm4n0" xml:id="tg76.3.22">Seine Herrlichkeit Ich sass am Throne in starrer Majestät, wie Nero, wie Caligula, und während die Blicke drohend und unruhig über die Welt hinschweiften, brach das innere Feuer der Unzufriedenheit <pb n="228" xml:id="tg76.3.22.1"/>
aus und verzehrte Seine Majestät mit Haut und Haaren – –!</p>
                    <p rend="zenoPLm4n0" xml:id="tg76.3.23">Eines Abends sass sie mit wirren Haaren um die schöne Stirne, an ihrem Tische mit der weissen Lampe und las.</p>
                    <p rend="zenoPLm4n0" xml:id="tg76.3.24">»Huysmans, en route.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg76.3.25">Da kam sie zu der Stelle Seite 58.</p>
                    <p rend="zenoPLm4n0" xml:id="tg76.3.26">»Lidvine était née vers la fin du XIV. Siècle en Hollande. Sa beauté était extraordinaire Elle tombe malade à 15 ans. Elle demeure étendue sur un grabat jusqu'à sa mort. Les maux les plus effrayants se ruent sur elle, la gangrène court dans ses plaies et de ses chairs en putréfaction naissent des vers. La terrible maladie du Moyen Age, le feu sacré, la consume. Son bras droit est rongé. Un de ses yeux s'éteint. Pendant 35 années, elle vécut dans une cave, ne prenant aucun aliment solide, priant et attendant en paix. Elle suppliait le Seigneur de ne point l'épargner. Elle obtenait de lui d'expier par ses douleurs les péchés des autres. Le Christ descendait en elle et lui donnait l'amour éternelle. Elle souriait. Elle était la moissonneuse des supplices! Elle s'était offerte au Ciel comme victime d'expiation! Elle prit sur elle les péchés des autres, des faibles. Elle avait la force! Elle avait la charité rayonnante! Elle avait la victoire!!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg76.3.27">Sie schloss das Buch – – –.</p>
                    <p rend="zenoPLm4n0" xml:id="tg76.3.28">Abendfrieden – – –.</p>
                    <p rend="zenoPLm4n0" xml:id="tg76.3.29">Sie sass da, mit wirren Haaren um die schöne Stirne, an ihrem Tische mit der weissen Lampe und sann – – – –.</p>
                    <pb n="229" xml:id="tg76.3.30"/>
                </div>
            </div>
        </body>
    </text>
</TEI>