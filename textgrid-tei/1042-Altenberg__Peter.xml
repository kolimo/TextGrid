<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg77" n="/Literatur/M/Altenberg, Peter/Prosa/Wie ich es sehe/Landstädtchen">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Landstädtchen</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Altenberg, Peter: Element 00084 [2011/07/11 at 20:31:54]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Peter Altenberg: Wie ich es sehe. 8.–9. Auflage, Berlin: S. Fischer, 1914.</title>
                        <author key="pnd:118502255">Altenberg, Peter</author>
                    </titleStmt>
                    <extent>230-</extent>
                    <publicationStmt>
                        <date>1914</date>
                        <pubPlace>Berlin</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1859" notAfter="1919"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg77.3">
                <div type="h4">
                    <head type="h4" xml:id="tg77.3.1">Landstädtchen</head>
                    <head type="h4" xml:id="tg77.3.2">Die Frauen.</head>
                    <p xml:id="tg77.3.3">Hier ist keine »Prinzessin des Lebens«, keine, die lächelt wie die Morgenröthe und Alles wacht auf, lebt – –!</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.4">Wie nach einem Wochenbette sind sie, ein bischen erschöpft, müde, wie Schmetterlinge im Oktober, wenn Frost kommt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.5">»Was können wir bieten – – –?!« denken sie, »welches Amüsement?!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.6">Ueberall spüren sie Feinde ihres Glückes. Sie erbleichen vor dieser ewigen Jugend »Mann«, vor diesem raschen Altern »Weib«!</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.7">Ueberall spüren sie Feinde ihres Friedens, Zerstörer: das »rasende Bicycle«, welches ihre Gatten in unbekannte Gegenden entführt, die weiten Wälder, in welchen die Hirsche röhren, diese Leidenschaftlichkeit »Jagd«, den dicken braunen Champagner »Bier«, der belebt und festhält, die dumpfe siegreiche Gemüthlichkeit »Stammtisch«!</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.8">Nie nimmt man sie mit.</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.9">»Wir stören – – –« sagen sie sanft.</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.10">Und in der Ferne droht, hinter den tausend Kilometern Obstgärten und Feldern, in eine mysteriöse<pb n="230" xml:id="tg77.3.10.1"/>
 Dunstwolke gehüllt, Wien, die grosse Stadt! Wien! Babel!</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.11">An den langen müden Abenden träumen die Männer: »Wien – – –?!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.12">Die Damen, mit ihren einfachen klaren Seelen, erbeben: »Wien – – –?!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.13">Wien, grosse, in eine mysteriöse Dunstwolke gehüllte Stadt, hinter den tausend Kilometern Obstgärten und Feldern – – –!</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.14">»Ist Dir was geschehn – –?!« sagt Einer, der von »dort« kam. »Ich glaube, ich habe Alles zurückgebracht, Katharina – – –?!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.15">»Oh – – wie Du angekommen bist, hast Du zu mir gesagt: »Servus – – –.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.16">»No – – und?!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.17">»Ich habe es verstanden. Servus, altes Elend, ich habe die Ehre!« Es hat nicht geheissen: »Servus, meine Heimat – – –!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.18">Er, roth werdend, verlegen: »Das sind überspannte Sachen – – –. Bist Du eine Bürgersfrau oder bist Du eine Prinzessin?! Nun also!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.19">»Oh – – Aber dieses »Servus« ist mir halt durch und durch gegangen, wie eine Kugel. Du hast mich angeschossen – – –.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.20">»Du bist eine Romantische – – –« sagt er weich und nimmt ihre zitternde Hand in die seinen.</p>
                    <p xml:id="tg77.3.21">– – – – – – – – – – – – – – – – – – – – – – – – – – –</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.22">Hier sind keine »Prinzessinnen des Lebens«, keine, die lächeln wie die Morgenröthe und Alles wacht auf, lebt!</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.23">
                        <pb n="231" xml:id="tg77.3.23.1"/>
Wie nach einem Wochenbette sind sie, ein bischen erschöpft, müde, wie Schmetterlinge im Oktober, wenn Frost kommt – – –.</p>
                    <lb xml:id="tg77.3.24"/>
                </div>
                <div type="h4">
                    <head type="h4" xml:id="tg77.3.25">Die Mädchen.</head>
                    <p xml:id="tg77.3.26">Ich kenne viele hübsche Mädchen hier, in dem kleinen Landstädtchen, Rosa, Maria, Gretl, Bettina, Therese – –.</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.27">»Liebe Geschöpfe,« denke ich, »ich wünsche Euch ein glückliches Leben, keine Stürme, Frieden!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.28">So milde fühle ich für Rosa, Marie, Gretl, Bettina, Therese.</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.29">Anna ist fünfzehn Jahre alt, arm, blass, mager.</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.30">Seit fünf Tagen zahle ich ihr die »amerikanische Hutsche«, das »Paradies der Kinder«, auf dem grossen Wiesenplatze.</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.31">Sie bittet nie, nimmt stumm an.</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.32">Aus den Lüften sagt sie hie und da mit den Augen: »Danke – – –.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.33">Die Grossen hutschen auch, Rosa, Marie, Gretl, Bettina, Therese.</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.34">»Die Anna ist ein kecker Fratz – – –« sagen sie.</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.35">»Ich möchte sogar zehn Gulden verhutschen –« sagt Anna einmal, vor Vergnügen zitternd, zu den Mädchen. Diese tratschen es mir.</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.36">»Bitte sehr – –« sage ich.</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.37">
                        <pb n="232" xml:id="tg77.3.37.1"/>
»O, es kostet Sie so schon so viel, zwei Gulden vierzig Kreuzer.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.38">»Wieso wissen Sie es?!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.39">»Ich schreibe es mir auf – –; vierundzwanzigmal zehn Kreuzer.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.40">»Wozu?!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.41">»So – –« sagte sie und wurde rosig.</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.42">Heute sagte ich zu ihr: »Anna, hutschen wir miteinander – – –.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.43">»Sie werden es nicht aushalten – –« sagte sie wie zu einem Dilettanten.</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.44">Es war wirklich wie auf dem Meere. Das Riesen-Orchestrion sang dazu und brüllte Sturm! Anna sass vis-à-vis. Wir waren allein in den Lüften. Das Orchestrion brüllte. Wir stiegen hinauf, hinunter. Wie eine gestockte Welle im Luftoceane war die Schaukel. Beim Herunter blickte ich in ihre Augen. Dann sah ich ihre Kniee, den Saum ihrer weissen Höschen – – –.</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.45">Ich sagte: »Anna, ist es Ihnen zu hoch?!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.46">»Nein – – – –.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.47">Ich zog an dem Stricke in der Schaukel, hing mich an, zog, hinauf, höher, höher, höher – – – herunter!</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.48">»Ah – – –« sagte sie und bückte sich ganz zusammen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.49">»Anna, ist es zu hoch?!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.50">»Nein – – –.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.51">»Anna – – –.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.52">Es war wie auf dem Meere, Sturm! Das Orchestrion <pb n="233" xml:id="tg77.3.52.1"/>
heulte mit 21 Pfeifen. Hinauf – – – herunter!</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.53">Beim Aussteigen sagte ich: »Aennchen, Annita – – –.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.54">»Danke – – –« erwiderte sie mit ihren Augen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.55">»Prinzessin Anna – – –« sagten Rosa, Marie, Gretl, Bettina, Therese. »Gott, wie blass sie ist!«</p>
                    <p xml:id="tg77.3.56">– – – – – – – – – – – – – – – – – – – – – – – – – – –</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.57">Ich kenne viele hübsche Mädchen hier, in dem kleinen Landstädtchen, Rosa, Marie, Gretl, Bettina, Therese.</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.58">»Liebe Geschöpfe« denke ich, »ich wünsche Euch ein glückliches Leben, keine Stürme, Frieden!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.3.59">So milde fühle ich für Rosa, Marie, Gretl, Bettina, Therese – – –.</p>
                    <pb n="234" xml:id="tg77.3.60"/>
                </div>
            </div>
        </body>
    </text>
</TEI>