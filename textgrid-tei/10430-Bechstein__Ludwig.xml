<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg339" n="/Literatur/M/Bechstein, Ludwig/Sagen/Deutsches Sagenbuch/147. Tückebold Kludde">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>147. Tückebold Kludde</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Bechstein, Ludwig: Element 00345 [2011/07/11 at 20:31:17]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Ludwig Bechstein: Deutsches Sagenbuch. [Leipzig: Georg Wigand, 1853]. ed. Karl Martin Schiller. Meersburg, Leipzig: Hendel, 1930.</title>
                        <author key="pnd:118654292">Bechstein, Ludwig</author>
                    </titleStmt>
                    <extent>118-</extent>
                    <publicationStmt>
                        <date when="1930"/>
                        <pubPlace>Meersburg und Leipzig</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1801" notAfter="1860"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg339.2">
                <div type="h4">
                    <head type="h4" xml:id="tg339.2.1">147. Tückebold Kludde</head>
                    <p xml:id="tg339.2.2">In ganz Flandern und Brabant glaubt das Volk an das Dasein eines bösen Geistes und nennt ihn Kludde, aber auch Kleure. Er spukt überall und in allen Gestalten, häufig zeigt er sich dem Mahr verwandt, erscheint als altes mageres Pferd mit durchscheinenden Rippen und struppiger Mähne, mischt sich unter die des Nachts im Freien weidenden Rosse, und wenn einer der Hüter meint, er besteige einen der besten Hengste, um einen Ritt zu machen, so ist's der Geist Kludde in Pferdegestalt, der mit ihm wild davonrennt, als jage ihn der helle Teufel, bis er an ein Wasser kommt, wo er den verzagenden Reiter hineinwirft. Dann fängt der Geist Kludde an zu lachen, daß sich entsetzt, wer dies Gelächter hört, und legt sich auf den Bauch und wälzt sich vor Lachen, während sein Reiter aus dem Wasser- oder Schlammbade sich angstvoll herausarbeitet.</p>
                    <p rend="zenoPLm4n0" xml:id="tg339.2.3">Manchesmal flackern vor dem Kludde zwei blaue Flämmchen her, die nennen die Bauern und die Pferdeknechte Stalllichter und halten dafür, daß die Flämmchen des Geistes Augen seien. Kludde kann sich zum Baum machen, klein wie ein Schlehenstrauch und bis hoch in die Wolken wachsen; Kludde kann dich als Schlange umringeln und als Hornisse umsumsen, er schreckt dich als Fledermaus oder als Kröte, er kann Katze sein und Maus, Frosch und Ochse. Man hört ihn auch rufen, und sein Ruf lautet Kludde! Kludde! So ruft er seinen Namen, wie der Vogel Kuckuck, der verrufene Gauch. Er neckt und plagt zu Lande wie zu Wasser; am Seegestade ist er Neck, auf dem platten Lande Schreck, ein greulicher Spuk, selbst Werwolf. Geist Kludde soll der Geist eines Mannes sein, der mit dem Teufel ein Bündnis hatte, und zu ruhelosem Wandeln auf Erden und Plagen der Menschen verurteilt sein.</p>
                    <p rend="zenoPLm4n0" xml:id="tg339.2.4">Einstens ging ein Mädchen mit ihrem Geliebten und einem Freunde desselben über Land, und waren in guten Gesprächen, da rief der Liebhaber mit einem Male: Schaut dorthin! Was sehe ich dort? – Die andern sahen nichts. – Was siehst du denn? – Kludde ist's! Jetzt springt er als Hund! Seht, er streckt sich – jetzt ist er ein Schaf – jetzt eine Katze – nein – da ist er ein Baum geworden. – Die andern konnten nichts von alledem erblicken. – Sag's, wenn du ihn wieder siehst! rief der Begleiter, ich will auf ihn zugehen. – Da läuft er ja vor uns her! – Jener sah nichts, und sie wandten sich, nach Hause zu gehen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg339.2.5">Vor dem Hause lag eine Steinplatte etwas lose, unter die man den Hausschlüssel zu legen pflegte. Und da rief der Liebhaber wieder: Seht! Seht ihr ihn nicht? Er sitzt ja auf der Platte, da kann ich nicht zum Schlüssel! Komm, Mieken, wir wollen dich erst nach Hause geleiten, du ängstigst dich. – Als die Freunde wiederkamen, sah der Liebhaber immer noch den Geist auf der Platte hocken, und der andere sah nichts. Dieser ging nun stracks auf die Platte zu und nahm ungehindert den Schlüssel, der Geist sprang hinweg. Ungehindert kam der Liebhaber in sein Haus und schloß es schnell. Der Begleiter bekam Kludde nicht ein einziges Mal zu Gesicht.</p>
                </div>
            </div>
        </body>
    </text>
</TEI>