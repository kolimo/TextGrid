<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg80" n="/Literatur/M/Altenberg, Peter/Prosa/Wie ich es sehe/Absinth »Schönheit«">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Absinth »Schönheit«</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Altenberg, Peter: Element 00087 [2011/07/11 at 20:31:54]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Peter Altenberg: Wie ich es sehe. 8.–9. Auflage, Berlin: S. Fischer, 1914.</title>
                        <author key="pnd:118502255">Altenberg, Peter</author>
                    </titleStmt>
                    <extent>241-</extent>
                    <publicationStmt>
                        <date>1914</date>
                        <pubPlace>Berlin</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1859" notAfter="1919"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg80.3">
                <div type="h4">
                    <head type="h4" xml:id="tg80.3.1">Absinth »Schönheit«</head>
                    <p xml:id="tg80.3.2">Spät am grauen Morgen erwachte er. Eine wunderbare Kälte war in dem kleinen Gemache. Er glitt aus diesem warmen Kanale »Leinentuch, hellblaue Steppdecke, Plümeau«, sorgsam heraus, damit Kamilla keine kalte Luft erhalte und heizte das freundliche Regulir – Füll – Öfchen mit hartem Holze vermittelst Harz-Zündern. Früher schloss er natürlich das Fenster, diese »Lunge des Zimmers«, und hängte den dreifachen Kotzen vor aus weichem Kameelhaar.</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.3">Kamilla – – – jawohl, da lag sie für zehn Kronen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.4">Heiliger Athem der Nacht – – –! Ah, könnte man das von allen Frauen sagen!? Aber Dein Athem, Kamilla, kostet blos zehn Kronen und hat den Duft von Berg-Wiesen. Kein eigentlicher Geruch. Nur von Kraft und Frische ein Hauch!</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.5">
                        <hi rend="italic" xml:id="tg80.3.5.1">Ein Stier frisst Blumen, verdaut sie, macht sie zu Mist. Und dieser Mist, den er aus Blumen machte, gab ihm sein Leben, seine Kraft</hi>. Und seine Dankbarkeit heisst: <hi rend="italic" xml:id="tg80.3.5.2">Dirne</hi>!</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.6">Das Holz im Öfchen wurde durch und durch leuchtend und knackste. Dann begann ein Feuermeer und die Flammen benahmen sich wie keuchende Hunde: h-ts, h-ts, h-ts, h-ts.</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.7">
                        <pb n="241" xml:id="tg80.3.7.1"/>
Das Öfchen versandte Wärme; wie der Geist eines Dichters! Alles wurde imprägnirt mit Wärme, sträubte sich und wurde dennoch imprägnirt. Sogar die weisse Kalkmauer öffnete ihre Poren und athmete ein und wurde milder.</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.8">Da entfernte er sanft das Plümeau, die Decke, und betrachtete dieses »Kunstwerk Gottes«, das Bewegung in die träge Welt pumpt wie eine mysteriöse Elektrisirmaschine, diesen »Ruhe-Mörder« Frauenleib!</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.9">Er sass da und begann alle diese schrecklichen Phantome zu fürchten und zu hassen, die Gespenster, die in unheimlicher Macht und unfassbar im Herzen eines Mannes aufsteigen – – die »Anderen«. Den Herren mit den begeisterten Briefen, den Lieutenant-Stellvertreter mit der jugendlichen Lust, den düsteren Chef, welcher die Macht hatte, den Marqueur, welcher für sie ein Sparkassen-Buch angelegt hatte, den Besitzer der mechanischen Schiessstätte, welcher ihr für »Löcher in's Weisse« Preise gab, und Alle und sich selbst! Denn sich selbst ist man Phantom, Gespenst, wie ein Anderer, der man sonst nicht ist, in Weibes Nähe!</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.10">Was hatten sie ihr geraubt, die Phantome?! Welche Spuren zurückgelassen?! Nichts, nichts. Jedes Härchen war an seinem Platze und die Haut in ihrer milden Blässe strahlte wie der Schnee Weisse aus in die Augen und machte diese glücklich und voll Licht- Kraft.</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.11">
                        <pb n="242" xml:id="tg80.3.11.1"/>
Da lag sie, die »Verwüstete«. Ha ha ha ha – – –</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.12">Armselige Vorurtheile geknechteter Menschheits-Seele! Wo waren denn, bitte, die Spuren des Samum, welcher über »blühende Gelände« strich?! Wie ein See, dessen Spiegel durch nichts getrübt würde, ein ewiger Schönheits-Strahler! Wirf störende Steine, senke scharfe Ruder, ziehe den eisernen Kiel durch – – – Besiegter! In Klarheit liegt er. Wie das Genie, dessen Herz Niemand verletzen könnte und welches Welten-Pulse pocht!</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.13">So ist der schöne Leib des Weibes. Welche Spuren zieht ein Ruhe-Störer?! In »heiliger Elastizität des Lebens« verwischt der schöne Leib die böse Spur und wie ein Paradieses-Garten strömt er Schönheit aus und Schönheit und Schönheit und bringt Dir Frieden. Aber die Seele, die verwüstete?! Ihre Seele <hi rend="italic" xml:id="tg80.3.13.1">liegt in mir! Ich bin es selbst, nicht sie!</hi> Ihr »Geist gewordener« schöner Leib bin ich. Und ihre »Form« ist meine »Gebilde gewordene« Seele. Wie wenn Gott-Canova meine Seele ausgemeisselt hätte zu lebendigem Sein, Materie, ist ihr Leib! <hi rend="italic" xml:id="tg80.3.13.2">Ich bin ihr Wesen, sie ist meine Form</hi>. Wir beide sind das Sein der Welt im »Paare« – – –</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.14">Er ging zum Öfchen, betrachtete das Holz. Fertig war es. Zusammengesunken, abgefallen, erschöpft, grau und dünn lag es, weil es zu sehr geflammt hatte und athmete schwer, erstarb nach erfüllter Mission. Im Zimmer aber befand sich seine <pb n="243" xml:id="tg80.3.14.1"/>
Seele, die Wärme, und brachte Leben. Kamilla lag da, nackt, und athmete diese Holzes-Seele ein und stappelte neue Kräfte auf durch Wärme und Ruhe für den Lieutenant-Stellvertreter, diese Jugendlust, und den düsteren Chef und den Schiessbudenbesitzer und Alle, Alle.</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.15">Da dachte der Herr: »<hi rend="italic" xml:id="tg80.3.15.1">Alles bist Du nun geworden, da Du nichts mehr bist als schön,</hi> Dirne! Doch noch erstrahlst Du, ach, in heidnischer Wärme! Dich kalt machen! Dich in Eis legen, einsargen zwischen Kristall-Eisblöcken aus Fabriken! Dass Du kalt werdest! Und allen zuwider wie der Hauch des Winters. Dass nichts mehr von Dir ausstrahle, Du Unglücks-Ofen ›Weib‹ und keine Lebenswärme sich verbreite! Erlöst wären wir, Dich in Kristall-Eis-Blöcken gebettet zu sehen statt in den lauen Linnen! Und fühlen, dass die Kälte Dir hineindringe, ganz, ganz hinein, überallhin, in die Heiligthümer Deiner Hitze-Quellen und Alles auslöschen würde, Dich und die Phantome und den düsteren Chef, den Lieutenant-Stellvertreter und den Schiessbudenbesitzer; und Alle würden vor Kälte hin werden, abfallen, grau, dünn werden, ohne Mission. Ha ha ha ha – – – ich aber bliebe am Leben! Ich! Denn Dein <hi rend="italic" xml:id="tg80.3.15.2">Wesen</hi> ist abhängig von Gluthen, aber Deine <hi rend="italic" xml:id="tg80.3.15.3">Form</hi> ist ewig wie das Eis im Polarmeere! Und ich bliebe bei Dir! Denn ich liebe nicht Dein Wesen, welches nur die Form meiner Materie, sondern <hi rend="italic" xml:id="tg80.3.15.4">Deine Form, welche die Materie meines Wesens ist! Die Schönheit meiner <pb n="244" xml:id="tg80.3.15.4.1"/>
Gedanken ist die Schönheit Deines Leibes! Die Pracht meiner Seele sind die Linien Deiner Glieder!</hi> Und die Kristall-Eis-Blöcke der Fabriken, in welche Du gebettet wärest zu Tode, könnten nicht eine einzige Deiner Linien vernichten, welche allein meine Liebe in Brand erhalten! Und das Ewige würde sich mit dem Ewigen vermählen! Das Unzerstörbare mit dem Unzerstörbaren! Die Schönheit Deiner Seele: »Leib« mit der Schönheit meines Leibes: »Seele« – – –!</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.16">So aber herrscht nur das traurige Gesetz der Materie; hat sie vielleicht nicht Wärme genug für Alle, tausend Millionen Caloriferen?! Muss sie die latenten Kräfte nicht an den Weltenraum abgeben, wo kalte Leiber daran sich zu Gluthen lecken?!</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.17">Ha! kalt machen! Dass nur die Kaiserin »Schönheit« herrsche! Dass Du ewig werdest, Vergängliches! Und nur dem Einen, dem Gott-Menschen bliebest, der Dich »erkannt« hat, seiest Du warm gezeugt von einem Weibe oder kalt vom Marmor des Canova, gleichviel! Ewig wärest Du sein! »Mensch gewordene« Welten-Schönheit!!</p>
                    <lg>
                        <l xml:id="tg80.3.18">– – – – – – – – – – – – – – – – – – – – – – – – – – –</l>
                        <l xml:id="tg80.3.19">– – – – – – – – – – – – – – – – – – – – – – – – – – –</l>
                    </lg>
                    <lg>
                        <l rend="zenoPLm4n0" xml:id="tg80.3.20">Jene aber erwachte und war glühend.</l>
                        <l rend="zenoPLm4n0" xml:id="tg80.3.21">Und sie sagte: »Du, komm' – – –.«</l>
                        <pb n="245" xml:id="tg80.3.22"/>
                    </lg>
                </div>
            </div>
        </body>
    </text>
</TEI>