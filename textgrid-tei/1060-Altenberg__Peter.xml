<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg95" n="/Literatur/M/Altenberg, Peter/Prosa/Wie ich es sehe/Une femme est un état de notre âme/Friede">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Friede</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Altenberg, Peter: Element 00103 [2011/07/11 at 20:31:54]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Peter Altenberg: Wie ich es sehe. 8.–9. Auflage, Berlin: S. Fischer, 1914.</title>
                        <author key="pnd:118502255">Altenberg, Peter</author>
                    </titleStmt>
                    <extent>273-</extent>
                    <publicationStmt>
                        <date>1914</date>
                        <pubPlace>Berlin</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1859" notAfter="1919"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg95.2">
                <div type="h4">
                    <head type="h4" xml:id="tg95.2.1">
                        <pb n="273" xml:id="tg95.2.1.1"/>
                        <pb type="start" n="275" xml:id="tg95.2.1.2"/>Friede</head>
                    <p xml:id="tg95.2.2">Wie ist ihr Leben?! Das Leben von Christine, welche die Tanten »Christa« nennen und welche Augen hat wie der Gekreuzigte?!</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.3">Wie ist ihr Leben?! Sage mir!</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.4">Sie erwacht, streicht die braun-blonden Haare zurück, stellt sich zum Waschtische, der nach Doering-Seife duftet und Pasta Boutemard, taucht das liebliche Gesicht in's laue Wasser, seift sich ein, schwemmt die Seife fort und trocknet sich.</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.5">So geht es weiter – – –.</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.6">Dann Frühstück. Ein bischen müde sitzt sie da. Vom Ruhen ruhend. Immer dieselbe Tasse, dasselbe gestickte Deckchen, derselbe Duft nach Thee.</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.7">Ein bequemer, gut eingerichteter, klappender Mechanismus, dieses Morgenleben!</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.8">Dann geht sie hin und her, nimmt ein frisches Taschentuch, betrachtet es, ob es keine Löchlein habe, nimmt behutsam die kleine goldene Uhr, sperrt Kästen auf und zu, denkt: »Schöne Dinge habe ich – – –«, ordnet ein wenig, schiebt zusammen, theilt ein, trägt ihre lieben Blumenstöcke selbst hinaus und macht wie wenn es kleine Kinder wären, behandelt sie zärtlich, sorgenden Herzens, schneidet ein welkes Blatt ab, nein, noch nicht welk, doch <pb n="275" xml:id="tg95.2.8.1"/>
schon ein bischen spröde, Wasser wird es nicht mehr saugen können und den Andern nimmt es doch Etwas weg. Dann bläst sie in die Regenbrause und betrachtet.</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.9">»In Ordnung!« denkt sie.</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.10">So vergeht der Vormittag.</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.11">Immer gehen Thüren auf und zu und Alles sieht aus, als ob es nie in Ordnung käme.</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.12">Plötzlich aber ist Alles licht und rein und Du hast keine Ahnung, dass eine lange dumpfe Nacht war.</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.13">Die Blumenstöcke stehen wieder an dem kristallenen Fenster und sehen aus wie nach einem lauen Sommerregen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.14">Alles athmet Frische, Gesundheit.</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.15">Diese Stimmung ist seit tausend Tagen. Immer diese gesunde frische Ordnung.</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.16">Wieviel Uhr ist es?!</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.17">Wie vergeht die Zeit bis Mittag?!</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.18">Sie vergeht.</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.19">Dann setzt man sich an seinen Platz, nimmt die kühle Serviette.</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.20">Liebevoll betrachtet der Vater sein Töchterchen. Wie eine Rast der Augen ist es, im Leben, welches drängt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.21">Das ist seit tausend Tagen – – –. Es ist wie die begossenen Blumen und das Sonnenlicht im aufgeräumten Zimmer.</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.22">Wenn es nicht wäre, Christine – – –?!</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.23">
                        <pb n="276" xml:id="tg95.2.23.1"/>
Aber es ist, es ist! Und sicher, wie der Abend, der dem Tag folgt!</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.24">Man spricht. Man schweigt. Was gibt es Neues?! Jemand war hier und man war bei Jemand.</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.25">Immer derselbe Duft im Speisezimmer nach dem Essen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.26">Der Vater trinkt Kaffee und man merkt, dass er sein Töchterchen sehr lieb hat.</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.27">Aber welchen Blick hat er?! Sehnt er sich?! Ist sie für ihn wie Schubert-Lieder?! Wird er ein anderer Mensch und preist sich glücklich?! Klingt vielleicht ein Dankgebet in seinem Herzen?!</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.28">Keineswegs.</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.29">Er macht so eine Geberde: »Wenn Du nur gesund bist und ruhig und Alles so weitergeht in Frieden – – –!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.30">Kein Enträthseler, Erwecker ist er!</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.31">Er ist das stumme schwere Leben selbst. Es geht dahin, weiss sich nicht zu besinnen. Es geht dahin.</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.32">Nachmittag.</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.33">Der Nachmittag vergeht.</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.34">Die Blumen blühen weiss und grün am Fenster.</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.35">Ein Wagen fährt vorbei und donnert fern.</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.36">Man liest ein Buch. Wie Sterne sind die Bücher der Poëten. So unendlich weit von uns. Und dennoch schimmern sie.</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.37">Die Mutter ruft, die Schwester – – –.</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.38">Ein Bruder stürmt herein und eilt hinaus in's <pb n="277" xml:id="tg95.2.38.1"/>
fremde Leben, das die Männer haben und lässt im Rauch der Cigarette etwas Leichtlebiges, Freies zurück.</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.39">Das Leben spinnt sich ab. Man lässt es spinnen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.40">Und liebevolle Herzen um Euch herum besorgen das Nothwendige!</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.41">Abend.</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.42">Zündet die Lampen an!</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.43">»Wennt Abend ward – – –</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.44">Und still de Welt und still dat Hart – – –</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.45">Und müd upt Knie Dir liegt de Hand,</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.46">Und ut din Housklock (Stand-Uhr) an de Wand</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.47">Du hörst den Parpendikel-Slag,</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.48">De nit too Wort käm über Tag – – –</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.49">Wenn denn noch eenmol kiekt (guckt) de Sün (Sonne)</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.50">Mit gold'nem Schin zum Fenster rin,</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.51">Und eher de Slap (Schlaf) käm un de Nacht,</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.52">Noch eenmol Alles levt (lebt) un lacht – – –</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.53">Dat is so wat for Minschenhart (Menschenherz) – – –</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.54">Wennt Abend ward – – –.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.55">So singen die Dichter, welche so unendlich weit von uns sind – – – wie die Sterne.</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.56">Souper.</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.57">Man spricht, man schweigt. Was gibt es Neues?!</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.58">»Tante Marie war hier. Sie findet, dass Christa passabel aussieht. Im Sommer, räth sie uns – – –.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.59">»Minister Goluchowsky soll gestern im Theater gewesen sein. Habt Ihr ihn gesehen?!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.60">
                        <pb n="278" xml:id="tg95.2.60.1"/>
»Nein. Schade. Es hätte mich interessirt. Ein fades Publikum war übrigens.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.61">»Wofür ist Goluchowsky?!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.62">»Für's Äussere. Das solltest Du wissen. Was denn?! Romane?!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.63">»Was Christa mit ihren Blumen treibt?! Einen Gärtner solltest Du heirathen, Christa. Zu Weihnachten Blumen. Zum Geburtstag Blumen. Und Alle weiss. Farbige sind doch viel schöner. Nun freilich, in der Zimmerluft – – –.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.64">»Wieso in der Zimmerluft?!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg95.2.65">»Was wieso?! Die Tante hat übrigens gesagt, von allen ›fixen Ideen der Seele‹ sei Blumenpflege noch die practicabelste. Wie Die sich manchesmal ausdrückt!?«</p>
                    <p xml:id="tg95.2.66">
                        <hi rend="superscript" xml:id="tg95.2.66.1">...................................................................................</hi>
                    </p>
                    <lg>
                        <l rend="zenoPLm4n0" xml:id="tg95.2.67">»Gute Nacht, Papa.«</l>
                        <l rend="zenoPLm4n0" xml:id="tg95.2.68">»Schlafe gut, mein Kind.«</l>
                        <l rend="zenoPLm4n0" xml:id="tg95.2.69">Und wieder steht sie vor dem Waschtisch, der nach Seife duftet und Pasta Boutemard.</l>
                        <l rend="zenoPLm4n0" xml:id="tg95.2.70">In's kühle Bett mit seinen warmen Decken – – –!</l>
                        <l rend="zenoPLm4n0" xml:id="tg95.2.71">Löscht die Lampen aus!</l>
                        <l rend="zenoPLm4n0" xml:id="tg95.2.72">Der Tages-Mechanismus ist zu Ende.</l>
                    </lg>
                </div>
            </div>
        </body>
    </text>
</TEI>