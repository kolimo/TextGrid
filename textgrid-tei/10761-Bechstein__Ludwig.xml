<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg670" n="/Literatur/M/Bechstein, Ludwig/Sagen/Deutsches Sagenbuch/478. Das Alp als Flaumfeder">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>478. Das Alp als Flaumfeder</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Bechstein, Ludwig: Element 00676 [2011/07/11 at 20:31:17]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Ludwig Bechstein: Deutsches Sagenbuch. [Leipzig: Georg Wigand, 1853]. ed. Karl Martin Schiller. Meersburg, Leipzig: Hendel, 1930.</title>
                        <author key="pnd:118654292">Bechstein, Ludwig</author>
                    </titleStmt>
                    <extent>334-</extent>
                    <publicationStmt>
                        <date when="1930"/>
                        <pubPlace>Meersburg und Leipzig</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1801" notAfter="1860"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg670.2">
                <div type="h4">
                    <head type="h4" xml:id="tg670.2.1">478. Das Alp als Flaumfeder</head>
                    <p xml:id="tg670.2.2">In dem Stadtflecken Ruhla, in dessen Nähe vor alters der eiserne Thüringer Landgraf hart geschmiedet wurde, gibt es Sagen von Bergschätzen und allerlei Geistern, guten und bösen, vollauf. Jäger spuken, Pfarrer spuken, Jungfern spuken, Hexen, Mönche, Kroaten spuken, es spukt eine Gans und endlich sogar ein Esel, der Bieresel genannt, der sich den spät vom Biere heimkehrenden Männern aufhockt, sie auch wohl umhalst, wie das römische Gespenst Empusa in Eselgestalt die Reisenden. Da ist denn bei so vielerlei Geister- und Hexenspuk auch das Alp zu Hause, das die Schlummernden nächtlich quält, ähnlich dem oder der Mahr in den Sagen des Niederlands. Dagegen gibt es aber ein probates Mittel. Der Gequälte muß nämlich, sofern er es vermag, rasch aufstehen und das Schlüsselloch zustopfen, denn durch dieses geht das Alp aus und ein. Solches Kunststück wußte einmal einer und probierte es, und siehe, da ward das Alp sichtbar und saß auf seinem Bette, hatte einen weißen Schleier und war ein wohlgetanes Frauenbild. Das war dem Rühler gar nicht uneben, er behielt die Schöne bei sich und lebte mit ihr als einer Frau. Sie war auch still und gefügig, aber sie lachte nie und bat ihn stets, das Schlüsselloch zu öffnen, denn nur durch dieses und nicht durch offne Türen und Fenster kann das Alp wieder entweichen, daher auch Goethe im Faust den Mephistophiles sagen läßt:</p>
                    <lb xml:id="tg670.2.3"/>
                    <milestone unit="hi_start"/>
                    <p xml:id="tg670.2.4">
                        <seg rend="zenoTXFontsize80" xml:id="tg670.2.4.1">'S ist ein Gesetz der Geister und Gespenster:</seg>
                    </p>
                    <p xml:id="tg670.2.5">
                        <seg rend="zenoTXFontsize80" xml:id="tg670.2.5.1">Wo sie hereingeschlüpft, da müssen sie hinaus.</seg>
                    </p>
                    <milestone unit="hi_end"/>
                    <lb xml:id="tg670.2.6"/>
                    <figure>
                        <graphic url="graphics/bech0335.jpg" width="334px" n="break" xml:id="tg670.2.7"/>
                        <ab type="caption" xml:id="tg670.2.7.1">Die heilige Elisabeth (Ludwig Bechstein: Deutsches Sagebuch)</ab>
                    </figure>
                    <lb xml:id="tg670.2.8"/>
                    <pb n="334" xml:id="tg670.2.9"/>
                    <p xml:id="tg670.2.10">
                        <pb n="335" xml:id="tg670.2.10.1"/>
                        <pb type="start" n="334" xml:id="tg670.2.10.2"/>Einstmals aber, da die stille Frau ihre Bitten wiederholte, dachte er: Hm, du willst doch sehen, wo das hinaus will, sie geht ja doch nicht durch das Schlüsselloch, und wenn sie fort will, kannst du sie ja halten – ließ sich aber gegen sie nichts merken und räumte unversehens die Verstopfung des Schlüssellochs hinweg. Da wurde das Frauchen <pb n="334" xml:id="tg670.2.10.3"/>
                        <pb type="start" n="337" xml:id="tg670.2.10.4"/>kleiner und immer kleiner und endlich gar zu einer Flaumfeder, und da haschte er nach ihr, aber das Wehen seiner Hand trieb sie hinweg, und husch, flog und zog sie durch das Schlüsselloch und war dagewesen.</p>
                </div>
            </div>
        </body>
    </text>
</TEI>