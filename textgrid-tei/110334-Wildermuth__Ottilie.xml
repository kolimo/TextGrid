<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg42" n="/Literatur/M/Wildermuth, Ottilie/Erzählungen/Bilder und Geschichten aus Schwaben/Gestalten aus der Alltagswelt/1. Aus dem Leben einer Hausfrau der alten Zeit/Hochzeit und Ehestand">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Hochzeit und Ehestand</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Wildermuth, Ottilie: Element 00054 [2011/07/11 at 20:30:38]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Ottilie Wildermuth: Ausgewählte Werke. Illustrierte Ausgabe in vier Bänden. Band 1–2, Stuttgart, Berlin und Leipzig: Union Deutsche Verlagsgesellschaft, 1924.</title>
                        <author key="pnd:118632833">Wildermuth, Ottilie</author>
                    </titleStmt>
                    <extent>277-</extent>
                    <publicationStmt>
                        <date when="1924"/>
                        <pubPlace>Berlin und Leipzig</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1817" notAfter="1877"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg42.2">
                <div type="h4">
                    <head type="h4" xml:id="tg42.2.1">Hochzeit und Ehestand</head>
                    <p xml:id="tg42.2.2">Wer sich nun nach der Geschichte dieser Jugendliebe die Ahnfrau vorstellen wollte als eine Harfe mit zerrissenen Saiten, die nur noch <hi rend="spaced" xml:id="tg42.2.2.1">einen</hi> wehmütigen Akkord nachklingen, oder als eine Trauerweide, lebenslang hinabgebeugt auf das Grab des versunkenen Jugendtraumes, der wäre groß im Irrtum. Die Herzen à la Werther trug man dazumal noch nicht, und wenn man sie getragen hätte, so meinte sie just nicht, daß sie alle Moden mitmachen müsse. Sie ertrug das Leben nicht, sie griff es an mit frischem Mut, und wenn das Leben ein Kampf ist, so hat sie ihn freudig und ritterlich durchgefochten.</p>
                    <p rend="zenoPLm4n0" xml:id="tg42.2.3">Daß sie nicht eben sentimentaler Natur war, das beweist der erste Schmerz ihres Ehestandes, der in der Entdeckung bestand, daß ihr junger Ehegemahl – nicht tanzen könne. Neben aller Sitteneinfalt der guten alten Zeit wurden wichtige Lebensereignisse, Hochzeiten, Taufen und dergleichen stets mit entsprechender äußerlicher Feierlichkeit begangen. Hochzeiten, wo man sämtliche Gäste mit einer Flasche Malaga und einem Teller Süßigkeiten abfertigt, deren Rest der Konditor nachher wieder zurücknimmt, kannte man dazumal noch nicht. So wurde denn auch die Hochzeit der Ahnfrau mit gebührender Solennität gefeiert; alle Freunde, Verwandte und Bekannte, sämtliches Schreiberpersonal ihres Vaters waren geladen; alle Dienstleute, Wäscherinnen und Tagelöhner des Hauses, ja sogar ehemalige Mägde, nebst Eltern und Geschwistern der derzeitigen, wärmten sich am mächtigen Küchenfeuer und der reichlichen Mahlzeit, die daran bereitet wurde. Der Hofrat, ein Jugendfreund des Bräutigams, trug ein schalkhaftes Gedicht vor, auf Atlas gedruckt, aus dem ich mich nur noch der Strophe entsinne:</p>
                    <lb xml:id="tg42.2.4"/>
                    <lg>
                        <l xml:id="tg42.2.5">»Hinfüro darfst du deine Syndikussin</l>
                        <l xml:id="tg42.2.6">Ohne alle Sünde kussen.«</l>
                    </lg>
                    <lb xml:id="tg42.2.7"/>
                    <p xml:id="tg42.2.8">»Nun war am Abend,« erzählte die Ahnfrau, »wie üblich, der Hochzeitsball. Vorher hatte es seit langem keine Tanzgelegenheit gegeben, wo ich und mein Zukünftiger zusammen gewesen wären. Als nun die Spielleute ankamen, stand ich <pb n="277" xml:id="tg42.2.8.1"/>
mit meinem neuen Ehegemahl auf zum ersten Menuett. Als wir aber anfangen sollten, trippelte er nur so mit den Füßen hin und her. ›Ei, warum fangen Sie denn nicht an?‹ fragte ich – ich habe nämlich erst nach der Hochzeit du zu ihm gesagt. – ›Ja, tanzen habe ich noch nie können; es tut mir leid,‹ sagte er mit Lachen. Mir aber war's gar nicht lächerlich; ich schämte mich vor all meinen Gespielen, daß ich einen Mann habe, der nicht tanzen könne, und es schien mir eine rechte Unredlichkeit, daß er's nicht vorher gesagt.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg42.2.9">Viel Zeit zum Tanzen hat die gute Ahnfrau nun nicht gehabt, so tanzlustig auch damals die sechzehnjährige Hochzeiterin gewesen sein mochte. Als sie kaum zwanzig Jahre alt war, wimmelten schon drei Kinderchen um sie, »wären alle unter <hi rend="spaced" xml:id="tg42.2.9.1">einen</hi> Korb gegangen«. Auch hat ihr der Gatte die Tanzfreude, die er nicht selbst teilen konnte, wenigstens nicht mißgönnt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg42.2.10">»Man wußte damals gar nicht anders, als daß Frauen in gesegneten Umständen zu Ader lassen mußten, und es war der Brauch, sich an diesem Tage etwas zugute zu tun. Man machte einen Spaziergang über Land, und es wurde etwas Besonderes gekocht. Nun war's gerade ein regnerischer Tag, als ich das erstemal zur Ader gelassen hatte. Unser guter Freund, der Hofrat, kam herüber: ›So, die Frau hat heut zur Ader gelassen; was stellt ihr denn an bei dem bösen Wetter?‹ Man ratschlagte lange hin und her; endlich sagte der Hofrat zu meinem Mann: ›Wenn du deine Geige holen wolltest, so könnten wir ein Tänzchen machen.‹ Gesagt, getan. Du mußt wissen, daß mein Mann selig prächtig die Geige spielen konnte; so holte er denn die Geige und spielte den ganzen Nachmittag auf, und ich und der Hofrat tanzten dazu. Der Doktor ist nachher freilich böse geworden, aber es hat mir nichts getan, und unser Ältester war ein Prachtkind.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg42.2.11">»Aber, Ahnfrau, so jung möcht' ich doch nicht heiraten!« – »Ist auch ganz und gar nicht nötig, nicht im mindesten, will dir's durchaus nicht wünschen, und deinem künftigen Mann noch weniger, wenn's gleich bei uns gut ausgefallen ist. Ich <pb n="278" xml:id="tg42.2.11.1"/>
bin noch ein rechter Kindskopf gewesen und habe viel Lehrgeld bezahlt. Gesund war und blieb ich, Gott Lob und Dank! Als mich die Frau Försterin in meinem ersten Wochenbett besuchen wollte und ganz leise in das Schlafzimmer trat, sah sie nur das aufgemachte Staatsbett, weit und breit keine Frau Wöchnerin. ›Wo ist denn Ihre Frau?‹ fragte sie verwundert und ängstlich die Magd. – ›Ach, die Frau sind nur hinten im Hof und schleifen ein bißchen!‹ Es war nämlich Winter, und hatte mich schon lange gelüstet nach der prächtigen Glitschbahn im Hof. Du kannst dir aber denken, daß ich scharf von den Frauen mitgenommen wurde, und mehr noch später einmal, als ich am Jahrmarkt eine ganze Tafel voll Gäste sitzen ließ und dazwischen hinein mit meinen Schulkamerädinnen auf dem Markt spazieren ging, daß die Magd daheim sich derweil nicht zu helfen wußte.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg42.2.12">Vergeblich waren die Lehrgelder nicht ausgegeben, und die Ahnfrau ist eine so gute und tüchtige Hausfrau geworden wie nur je eine im schönen Schwabenland, die nicht nur der Küche und dem Haus, sondern auch Gärten, Feldern, Ställen und Wiesen nebst Kühen und Kälbern vorzustehen hatte. Und doch hat sie ihren frischen Mut dabei behalten. – Die Kinder, die rasch hintereinander nachwuchsen, wurden nicht als Last und Plage, sondern recht als Gottesgabe aufgenommen. Dafür machte man auch keine Umstände mit ihnen, kleidete die Jungen in Zwilchwämser und Lederhosen, die Mädchen in selbstgewobenen Barchent, wenngleich ein Bild, das die zwei ältesten des Hauses, den Knaben im Federhut, das Mädchen in hoher Frisur und Poschen darstellt, noch Zeugnis gibt, daß man bei festlichen Gelegenheiten auch Staat mit ihnen machen konnte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg42.2.13">Die Garderobe vererbte sich auf das nächstfolgende Familienglied; es wurde ihm anprobiert und paßte, wie die Ahnfrau meinte, stets »wie angegossen«. Die Söhne und Töchter selbst waren gerade nicht immer dieser Meinung, und die Kleider und Wämser mit recht gesunden Flicken darauf ließen meist noch hinlänglich Raum für künftige Körperentwicklung. – Die Betten waren je für zwei und zwei, und das konnte bei Störungen <pb n="279" xml:id="tg42.2.13.1"/>
der geschwisterlichen Eintracht fatal werden. Der Gottlieb und der Christian zum Beispiel, später die einträchtigsten Brüder, waren in ihrer Kindheit so eine Art Don Manuel und Don Cesar und gerieten sich im wörtlichsten Sinn dergestalt in die Haare, daß die Mutter darauf kam, ihnen die Köpfe glatt abrasieren zu lassen. Sie erzählten, es sei eigen gewesen, wie dann die Hände auf den kahlen Köpfen so ausgeglitten seien.</p>
                    <p rend="zenoPLm4n0" xml:id="tg42.2.14">Drangsale aller Art blieben natürlich nicht aus, Löcher und Beulen, Keuchhusten und Scharlachfieber; die Ahnfrau aber steuerte getrost auch durch solche Trübsalsfluten. Sie war einmal eben im Stall, um der Magd praktische Anleitung im Melken zu geben, da stürzte die gesamte Kinderschar mit Zetergeschrei herunter: »Der Christian hat Mausgift gegessen, der Christian hat Mausgift geschleckt (genascht)!« Schnell besonnen, reißt die Mutter den armen Sünder herbei, gießt ihm von der warmen, frischgemolkenen Milch so viel ein, als nur immer den Schlund hinab zu bringen ist, und noch ehe der in Eile herbeigerufene Arzt erscheint, ist durch die gewaltsame Explosion, die nun erfolgte, das Gift mit all seinen schädlichen Wirkungen entfernt, und der Christian bildete sich später noch etwas darauf ein, daß er in solcher Lebensgefahr gewesen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg42.2.15">Der Gottlieb freilich, der sah's bedenklicher an. Als er mit dem Christian einige Jahre später beim Herrn Onkel Landschaftsassessor in Stuttgart zu Gast essen durfte, wartete ihnen dieser zum Nachtisch drei Pfirsiche von einem Spalier seines Gartens als höchste Rarität auf und fragte triumphierend: »Nicht wahr, Buben, so habt ihr noch nichts gegessen?« – »Oh, warum nicht!« sagte der Christian; »'s Fritzles Käther gibt so sechs für einen Kreuzer.« Dem älteren Gottlieb, in tödlicher Verlegenheit, wie er des Bruders Taktlosigkeit beschönigen sollte, fällt endlich bei: »Ach, Herr Großonkel, nehmen Sie's doch nicht übel, daß mein Bruder so dumm ist! Aber vor drei Jahren hat er einmal Mausgift gegessen, das hat ihm vom Verstand genommen.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg42.2.16">Das Haus des Ahnherrn lag dicht neben dem Gasthof. So geschah es denn einmal, daß ein dicker Herr ganz patzig in die <pb n="280" xml:id="tg42.2.16.1"/>
Stube trat, wo eben die Ahnfrau mit den Kindern allein war. »Kann ich einen guten Schoppen haben?« – »Warum nicht!« meinte die Ahnfrau, die den Irrtum gleich gemerkt, und brachte aus ihrem Keller einen bessern, als jemals der Schwanenwirt seinen Gästen vorgesetzt. – »Ich möcht' auch ein Brot!« befahl der Dicke wieder. – »Da ist ein neugebackenes,« sagte die gefällige Hausfrau. – »Und ein Licht zu meiner Pfeife!« – »Sophie, bring Licht!« rief die Mutter. – »So, und jetzt möchte ich allein sein,« kommandierte er ferner, da sich die Jugend des Hauses mit aufgesperrten Mäulern um den ungenierten Gast zu scharen begann. – »Schon gut, will meine Kinder hinausschicken; kommt, Kinder!« Und mit unerschüttert gutem Humor verließ sie mit der jungen Schar das Zimmer. Der Dicke aber <pb n="281" xml:id="tg42.2.16.2"/>
stellte seinen Schoppen neben sich, öffnete das Fenster der Parterrewohnung und begann so recht behaglich seine Pfeife hinauszurauchen. Da bemerkte ihn sein Kutscher, der indes ausgespannt hatte. »Ei, Herr Amtmann, wo sind denn Sie?« fragte er erstaunt. – »Wo werd' ich sein? Im Wirtshaus um mein Geld,« schnauzte der hinaus. – »Du lieber Gott, nein! Da wohnt ja der Herr Syndikus; Sie sind um eine Tür zu weit.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg42.2.17">Da machte der dicke Amtmann sachte das Fenster zu, steckte die Pfeife ein und schlich sich ebenso leise hinaus, als er laut hereingetreten war, nachdem er vergeblich nach der Magd gespäht, um ihr etwa ein Trinkgeld in die Hand zu drücken. Um Weihnachten aber kam als Zeche eine Schachtel guten getrockneten Obstes für die liebe Jugend. Man sagt, der Herr Amtmann sei künftighin selbst in Wirtshäusern höflicher aufgetreten.</p>
                    <p rend="zenoPLm4n0" xml:id="tg42.2.18">Mit so frischem Humor wußte sie alle Verhältnisse aufzufassen, und ich bin nicht gewiß, ob es Ursache oder Wirkung der körperlichen Rührigkeit und Lebendigkeit war, mit der die Ahnfrau durch Wochenbetten, Kinderkrankheiten und Kriegsnöte rüstig durchsegelte. Niemand sah der zartgebauten Frau an, welch kräftiges Regiment sie als Hausfrau und Mutter führte, wie sie den Töchtern tätig voranging bei Feld-und Hausarbeiten aller Art, zu denen jetzt jede Magd von Bildung noch eine Untergehilfin verlangt. Darum ist ihr auch der reiche Segen, der ihr Haus sichtbar krönte und der mit jedem Kind zu wachsen schien, nicht zugeflogen wie eine gebratene Taube, sondern zugewachsen wie dem fleißigen Winzer der edle Wein, der die Frucht seines sauren Schweißes und doch eine wundersame Himmelsgabe ist.</p>
                </div>
            </div>
        </body>
    </text>
</TEI>