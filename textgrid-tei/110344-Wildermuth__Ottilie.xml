<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg52" n="/Literatur/M/Wildermuth, Ottilie/Erzählungen/Bilder und Geschichten aus Schwaben/Vom Dorf/4. Streit in der Liebe und Liebe im Streit/Die Nachbarskinder">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Die Nachbarskinder</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Wildermuth, Ottilie: Element 00066 [2011/07/11 at 20:30:38]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Ottilie Wildermuth: Ausgewählte Werke. Illustrierte Ausgabe in vier Bänden. Band 1–2, Stuttgart, Berlin und Leipzig: Union Deutsche Verlagsgesellschaft, 1924.</title>
                        <author key="pnd:118632833">Wildermuth, Ottilie</author>
                    </titleStmt>
                    <extent>351-</extent>
                    <publicationStmt>
                        <date when="1924"/>
                        <pubPlace>Berlin und Leipzig</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1817" notAfter="1877"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg52.2">
                <div type="h4">
                    <head type="h4" xml:id="tg52.2.1">Die Nachbarskinder</head>
                    <p xml:id="tg52.2.2">Mitten im Dorf stand des Schultheißen Haus, an den roten Jalousieläden und dem zweistöckigen Bau zu unterscheiden von den Bauernhäusern, auch wenn der Herr Schultheiß nicht gerade sein Pfeifchen unter dem Fenster dampfte. Er war nicht eben, was man einen Herrenschultheiß nennt; er stammte vom Dorf und war in seiner Jugend hinter dem Dungwagen einhergeschritten so gut wie einer. Aber er liebte jetzt, mit den Herren der Oberamtsstadt auf vertrautem Fuß zu verkehren; sein selbstgezogener Wein und die Strauben und Küchlein der Frau Schultheißin standen in gutem Geruch bei ihnen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg52.2.3">Neben des Schultheißen Haus stand ein sehr anspruchsloses Bauernhaus, einstöckig, mit geteilter Türe, dessen stattliche Scheune, in welcher der Dreschertakt noch bis Lichtmeß tönte, allein beurkundete, daß es einem »rechten«, das heißt vermöglichen Mann gehörte. Hinter dem Haus lag ein Gärtchen, mit Salat und Krautsetzlingen bepflanzt, auch mit etlichen Sonnenblumen geschmückt; der Eingang aber führte zwischen einer Gülle und Dunglege zur Haustür, wie das schon zu des Ähnes <pb n="351" xml:id="tg52.2.3.1"/>
Zeiten gewesen war. Zwar war »iabott« (jezuweilen) ein Kind des Hauses in die Gülle gefallen; weil aber noch keines darin ertrunken war, so dachte niemand daran, die Sache zu ändern, der jetzige Besitzer des Hauses am wenigsten. Seine Buben sollten aufpassen lernen, und sein Töchterlein, die Lisbeth, war ein gesetztes und vorsichtiges Kind, dem nicht so leicht ein Unschick begegnete.</p>
                    <p rend="zenoPLm4n0" xml:id="tg52.2.4">Des Schultheißen einziger Sohn hieß Georg, ein aufgeweckter Bursch, aber ein durchtriebener Schelm und so übermütig und mutwillig wie nur je der Sohn eines Machthabers, zumal wenn er der einzige ist. Wenn ihn die Würde seines Vaters auch nicht vor gelegentlichen Prügeln sicherte, so wäre er ohne diese Würde gewiß schon lange totgeschlagen worden; denn alle Streiche, die Simson vorzeiten den Philistern gespielt, sind nichts gegen die Possen, die er, wenn ihn seine tolle Laune ankam, an Freunden und Feinden verübte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg52.2.5">Was war nur das für eine Geschichte, als er an einem stillen Nachmittag, wo die Leute auf dem Felde waren, sämtliche Schweine losließ und von dem oberen Boden aus der blutigen Schlacht zusah, die es absetzte, bis jeder Eigentümer das seinige wiedergefunden hatte!</p>
                    <p rend="zenoPLm4n0" xml:id="tg52.2.6">Und wie er's angegriffen hatte, der Frau Müllerin ihre Staatshaube mit den handfesten Rosenknospen und krebsroten Bändern zu stehlen, das weiß kein Mensch; aber aus dem Kamin des ärmlichen Häuschens, das die blutarme Schwester der Müllerin bewohnte, ragte eines schönen Morgens eine lange Stange mit einem Strohkopf, auf dem das obgedachte Prachtstück saß. Einem reichen Weingärtner hatte er im Herbst Fischlein in die Weinbütte praktiziert, was diesen in den schlimmen Verdacht brachte, daß er seinen Wein mit Flußwasser vermehre, und dem Bäcker eine Brille auf den Laden genagelt, damit man seine Wecklein dadurch sehen könne: – kurz, es gab wenige im Dorf, die nicht ein Stücklein von seinem Mutwillen erzählen konnten. Und doch war ihm im Grunde keiner feind, wohl aber stimmten alle darin überein: »Der stirbt keinen rechten (natürlichen) Tod.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg52.2.7">
                        <pb n="352" xml:id="tg52.2.7.1"/>
Lisbeth, seines Nachbars Kind, hatte nicht am wenigsten von seinem Mutwillen zu leiden, und doch trugen die Streiche, die er ihr spielte, stets ein gewisses ritterliches Gepräge, freilich Ritterlichkeit in ihren rohesten Uranfängen. Es war einmal Familienfest bei Schultheißens, das heißt das große Schwein wurde geschlachtet. Lisbeth blieb zufällig unter der Haustür stehen. »Willst ums Würstle singen?« rief ihr Georg herüber. Lisbeth trat beleidigt zurück; das sollte man ihr nicht nachsagen, das war Sache der Bettelkinder. Wie sie aber abends sich an die Kunkel setzen wollte, war ihr schönes rotseidenes Band gestohlen und die Kunkel dafür mit Bratwürsten umwunden. Als sie einmal ihr Vieh zum Brunnen treiben wollte, waren ihr andre zuvorgekommen und wollten nicht Platz machen; da bemerkte Georg ihre Verlegenheit und brach wie der rasende Roland mit lautem Geschrei unter das Vieh der andern, das wie toll nach allen Seiten hinaussprang, so daß durchs ganze Dorf ein Rennen und Zetergeschrei anging, und führte siegreich Lisbeths Kühe zum Brunnen. Er stahl auch der Frau Pfarrerin die schönsten Rosen, um sie Lisbeth zum Kirchgang zu bringen; diese aber, die den Diebstahl ahnte, wies das Sträußchen patzig zurück, und Georg warf zur Rache die Fenster ihres Kämmerleins mit den schönsten Äpfeln ein, die seine Mutter noch als Rarität gespart hatte.</p>
                </div>
            </div>
        </body>
    </text>
</TEI>