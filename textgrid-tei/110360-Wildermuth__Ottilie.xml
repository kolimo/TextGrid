<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg68" n="/Literatur/M/Wildermuth, Ottilie/Erzählungen/Aus dem Frauenleben/Morgen, Mittag und Abend/Nach drei Jahren">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Nach drei Jahren</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Wildermuth, Ottilie: Element 00084 [2011/07/11 at 20:30:38]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Ottilie Wildermuth: Ausgewählte Werke. Illustrierte Ausgabe in vier Bänden. Band 1–2, Stuttgart, Berlin und Leipzig: Union Deutsche Verlagsgesellschaft, 1924.</title>
                        <author key="pnd:118632833">Wildermuth, Ottilie</author>
                    </titleStmt>
                    <extent>36-</extent>
                    <publicationStmt>
                        <date when="1924"/>
                        <pubPlace>Berlin und Leipzig</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1817" notAfter="1877"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg68.3">
                <div type="h4">
                    <head type="h4" xml:id="tg68.3.1">Nach drei Jahren</head>
                    <milestone unit="head_start"/>
                    <p rend="zenoPC" xml:id="tg68.3.2">Mathilde an Minna</p>
                    <milestone unit="head_end"/>
                    <lb xml:id="tg68.3.3"/>
                    <p xml:id="tg68.3.4">Liebste Minna!</p>
                    <p rend="zenoPLm4n0" xml:id="tg68.3.5">Ihr habt immer behauptet, ich komme nie in Verlegenheit, und Ihr habt mir damit sehr unrecht getan, denn gerade jetzt bin ich in der allergrößten Verlegenheit, wie ich Dir eine Neuigkeit mitteilen soll, die Du doch erfahren mußt. Nun, ich sehe aber auch gar nicht ein, warum mich's verlegen machen soll, habe ich doch auch ein Recht, zu tun, was ich will, so gut wie andre Leute. Also, jetzt sag' ich's gerade heraus, und Du brauchst Dich gar nicht zu wundern, hörst Du's! Seit gestern abend bin ich Braut mit dem Regierungsrat Fürst, nun ja, <pb n="36" xml:id="tg68.3.5.1"/>
mit Eurem Herrn Oberamtmann vor drei Jahren. So, jetzt weißt Du's, und am Ende überrascht Dich's nicht einmal.</p>
                    <p rend="zenoPLm4n0" xml:id="tg68.3.6">Wie es eigentlich gekommen, das ist schwer zu sagen, ich weiß es selbst nicht so recht. Du weißt ja, daß er als Regierungsrat hierher befördert wurde und daß der Zufall es fügte, daß er sich in demselben Hause einmietete, wo wir wohnten. Er wußte natürlich nichts davon; als ich ihm aber einige Tage nach seiner Ankunft im Hausgange begegnete, erkannte er mich zu meiner Verwunderung und sagte mit einer kurzen Verbeugung: »Fräulein Berg? Schon mal das Vergnügen ...« Du weißt, das ist schon viel von ihm.</p>
                    <p rend="zenoPLm4n0" xml:id="tg68.3.7">Er machte der Mutter einen Anstandsbesuch, und da er sehr solid lebt und viel zu Hause ist, so kam er noch manches Mal, meist nach Tisch zum Kaffee. Du weißt, er spricht nie viel, und <hi rend="spaced" xml:id="tg68.3.7.1">ich</hi> kann nicht ertragen, wenn nichts gesprochen wird; so habe ich denn vielleicht manchmal unnötig viel geredet. Das erste Mal rauchte er nicht bei uns, sah aber so unbehaglich aus, daß ich das nächste Mal fast froh war, als er seine Pfeife herauszog und die Mutter fragte: »Geniert's nicht?« Es war zwar unartig, daß er mich nicht auch gefragt, aber, was wollt' ich machen! Es freute mich doch zu sehen, wie es ihm nun behaglich wurde, und man sagt mir, daß der Tabakrauch gut für meine Blumenstöcke sei.</p>
                    <p rend="zenoPLm4n0" xml:id="tg68.3.8">Nun, daß ich's kurz mache, wie ich einmal heimkam, sagte mir die Mutter, daß der Regierungsrat bei ihr um mich geworben (ich wäre gar zu gern dabei gewesen, da <hi rend="spaced" xml:id="tg68.3.8.1">muß</hi> er doch mehr als drei Worte gesprochen haben!), und morgen wolle er kommen und meine Antwort holen. Ich fiel wie aus den Wolken. Die Mutter wünschte, daß ich Ja sage, denn er ist sehr geschickt (und auch gescheit und gebildet, ich versichere Dich, obgleich er so wenig spricht), aber sie ließ mir ganz freie Wahl. Nun, zuerst wollt' ich gar nicht, dann wollt' ich mich besinnen, aber lange, recht lange; das Warten würde ihm gar nichts schaden. Dann beschloß ich endlich, ihn doch am nächsten Tage kommen zu lassen; aber nur, um ihm all meine Bedenken wegen seiner Schweigsamkeit, seines Mangels an chevalereskem <pb n="37" xml:id="tg68.3.8.2"/>
Benehmen, überhaupt meinen Zweifel, ob er echte, wahre Liebe für mich empfinde, auseinanderzusetzen; das Jawort, wenn ich mich je dazu entschlösse, wollt' ich ihm noch recht sauer machen!</p>
                    <p rend="zenoPLm4n0" xml:id="tg68.3.9">Wie er nun am nächsten Tage die Treppe heraufkam, versteckte ich mich und war erst auf Zureden der Mutter zu bewegen, hineinzugehen. Da saß er mit der Pfeife, die legte er aber beiseite, das war schon viel von ihm, nicht wahr? Ich erwartete mit klopfendem Herzen, er werde nun seine Werbung bei mir selbst anbringen, und ich gestehe, ich war recht begierig, wie er das machen würde. Er aber sprach kein Wort, er sah mich nur an, als ob er von mir eine Erklärung erwarte, und als ich schwieg, fragte er endlich: »Mutter gesprochen?« Nun konnte ich's doch nicht ignorieren; ich sagte also, daß ich durch die Mutter von seinem ehrenvollen Antrag wisse, daß ich aber fürchte, unsere Naturen stimmen nicht zusammen ... »Gerade,« warf er ein. – Daß ich fürchte, es sei nicht tiefe, innige Liebe, die ihn zu mir führe ... – »Was sonst?« fragte er. Das machte mich etwas verlegen – und daß ich fürchte, er verstehe und achte die tieferen Herzensbedürfnisse, die zarteren Rechte und Ansprüche meines Geschlechtes nicht genug ... ich verwickelte mich wirklich ein wenig; es brachte mich so aus der Fassung, daß er auch kein einziges Wort erwiderte. Als ich still blieb, stand er langsam auf und fragte: »Also nein?« Ich versichere Dich, Minna, er sah dabei recht traurig aus; da dauerte er mich doch, und ich sagte etwas vorschnell in meinem Mitleid: »Nun, das nicht gerade.« – »Ja?« fragte er und bot mir die Hand hin. Nun, verachte mich nicht, Minchen, ich gab ihm die meinige, und ehe ich wußte wie, stand ich als seine Braut vor der Mutter. Es ärgert mich jetzt noch, daß ich's ihm nicht ein bißchen schwerer gemacht, und daß er mich eigentlich erschwiegen hat, nicht errungen; aber ich kann nichts mehr ändern, es ist geschehen, und ich versichere Dich, er fühlt sich <hi rend="spaced" xml:id="tg68.3.9.1">sehr</hi> glücklich, wenn man's ihm auch kaum anmerkt. Ich fürchte, daß ich ihn noch ein wenig verwöhne, aber das gibt sich mit der Zeit; als Braut muß man doch sachte tun mit Reformen. Die Pfeife bin ich nun schon gewöhnt, ich kann mir Ludwig gar nicht mehr <pb n="38" xml:id="tg68.3.9.2"/>
ohne sie denken. Denke, er spricht schon von Hochzeit; rüste nur den Brautjungfernstaat.</p>
                    <p rend="zenoPLm4n0" xml:id="tg68.3.10">Deine Herzensangelegenheit, meine arme, liebe Minna, habe ich nicht vergessen. Ludwig muß mir versprechen, für Deinen Arwed ein kleines Amt aufzufinden, das er ohne das leidige Examen erlangen kann, da Dein Vater nun eben darauf besteht, Deine Zukunft auf festeren Boden als die Schwingen eines Pegasus zu gründen. Ludwig, der sich's selbst mit seinen Studien sauer werden ließ, denkt zwar etwas streng und reell, und ich weiß noch nicht, wie ich's ihm beibringe, aber ich sorge gewiß dafür. Da ich nun einmal meine goldne Freiheit verscherzt, kann ich nichts Besseres tun, als mich in der Gefangenschaft glücklich fühlen, und dann darfst Du, meine Liebe, auch nicht unglücklich sein.</p>
                    <p rend="zenoPLm4n0" xml:id="tg68.3.11">Und nun lebe wohl, beklage mich nicht zu sehr; ich schicke mich außerordentlich in mein Los; um nicht zu viel auf einmal zu sagen, unterschreibe ich mich inzwischen</p>
                    <milestone unit="hi_start"/>
                    <p rend="zenoPR" xml:id="tg68.3.12">Deine zufriedene Mathilde.</p>
                    <milestone unit="hi_end"/>
                    <lb xml:id="tg68.3.13"/>
                    <p xml:id="tg68.3.14">Noch eins! Bitte Friederike, mir eine Sammlung erprobter Kochrezepte, hauptsächlich zur Bereitung von Braten und Ragouts zu schicken. Ludwig ißt alle Sonntag abend mit uns, da möcht' ich doch einige Abwechslung in unser gewöhnlich so einfaches Souper bringen.</p>
                    <pb n="39" xml:id="tg68.3.15"/>
                </div>
            </div>
        </body>
    </text>
</TEI>