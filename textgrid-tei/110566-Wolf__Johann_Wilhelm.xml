<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg13" n="/Literatur/M/Wolf, Johann Wilhelm/Märchen/Deutsche Hausmärchen/Die schlechten Kameraden">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Die schlechten Kameraden</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Wolf, Johann Wilhelm: Element 00016 [2011/07/11 at 20:28:29]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Johann Wilhelm Wolf: Deutsche Hausmärchen. Göttingen: Dieterich’sche Buchhandlung/Leipzig: Vogel, 1851.</title>
                        <author key="pnd:117444839">Wolf, Johann Wilhelm</author>
                    </titleStmt>
                    <extent>64-</extent>
                    <publicationStmt>
                        <date when="1851"/>
                        <pubPlace>Göttingen/Leipzig</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1817" notAfter="1855"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg13.3">
                <div type="h4">
                    <head type="h4" xml:id="tg13.3.1">
                        <pb n="64" xml:id="tg13.3.1.1"/>
Die schlechten Kameraden.</head>
                    <p xml:id="tg13.3.2">Ein Schuster und ein Schneiderlein gingen mitsammen auf die Wanderschaft; sie gelobten einander treu beizustehn, zusammen zu halten in Freud und Leid und was der Eine hätte, müsse auch der Andere haben. Sie fanden aber nirgendwo Arbeit und da ihnen das Fechten nicht länger behagte, so nahmen sie Dienst unter den Soldaten. Es war nämlich schon seit langer Zeit Frieden und kein Krieg zu fürchten, so daß ihr Muth keinen Schiffbruch leiden konnte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg13.3.3">Das Schneiderlein hatte flinkere Beine, wie der Schuster und kam viel schneller vorwärts; auch verstand es den Mund recht voll zu nehmen und machte viel Wesens von seiner Tapferkeit, wie es immer muthig zugestochen habe und wie es sein Eisen geführt, daß die Lappen gefallen seien und wie es stets kalten Blutes vorm Feuer gestanden. So wurde es bald Gefreiter, dann Unteroffizier und brachte es endlich selbst zum Feldwebel. Je höher es aber rückte um so hochmüthiger wurde es und zuletzt kannte sein Stolz keine Grenzen mehr. Am meisten litt der arme Schuster darunter. Der Dienst wurde ihm endlich so verleidet, daß er eines Abends sein Bündel schnürte und weglief, als ob es hinter ihm brenne.</p>
                    <p rend="zenoPLm4n0" xml:id="tg13.3.4">
                        <pb n="65" xml:id="tg13.3.4.1"/>
Gegen Mittag kam er in einen Wald und da er des Weges nicht kundig war, verirrte er sich. Als er so dastand und nicht wußte wo aus noch wo ein, kam ein Jäger daher, der hatte sich gleichfalls verirrt und frug ihn um den rechten Weg. ›Den sage du mir,‹ sprach der Schuster. ›Zwei können mehr als einer ausrichten‹ erwiederte der Jäger, ›drum laß uns zusammenhalten, dann kommen wir schon heraus.‹ Das thaten sie, aber es wurde Abend und finstre Nacht und der Wald wollte immer noch kein Ende nehmen. Da stieg der Schuster auf einen hohen Eichbaum, schaute sich um und sah weit weit ein Lichtchen. Frischen Muthes gingen sie in der Richtung fort und kamen an ein kleines Haus, darin saß eine alte Frau, welche Kartoffeln schälte und Suppe kochte. ›Können wir hier die Nacht über bleiben?‹ frug der Schuster. ›Nein,‹ sprach die alte Frau, ›geht vielmehr so schnell ihr könnt weiter, denn ihr seit in eine Räuberhöhle gerathen und wenn die Räuber euch finden, seit ihr euren Kopf los. Nachts um zwölf Uhr kommen ihrer zwölf mit ihrem Hauptmann und Mittags um zwölf Uhr kommen zwölf andere mit ihrem Hauptmann zum Essen. Die erste Parthie muß schon in der Nähe sein; sie setzen dem König nach, der sich im Walde verirrt hat, darum eilt und macht, daß ihr fortkommt, ehe sie euch treffen.‹</p>
                    <p rend="zenoPLm4n0" xml:id="tg13.3.5">›Das macht nichts,‹ sprach der Schuster, ›ich will uns schon heraushelfen; sage uns nur wie die Hauptleute heißen und was ihre Erkennungszeichen sind, und du Kamerad thu mir Alles nach, wie ich es dir vormache.‹ Da sagte die Frau ihnen Alles, denn sie war den Räubern von Herzen feind und diente ihnen nur gezwungen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg13.3.6">
                        <pb n="66" xml:id="tg13.3.6.1"/>
Bald darauf gab es Lärm draußen und der erste Hauptmann kam mit seinen zwölf Leuten. Der Schuster trat aber keck auf ihn zu und sprach: ›Einen schönen Gruß von unserm Hauptmann, ihr solltet uns sagen, ob ihr den König gefangen hättet oder nicht; wir haben seine Spur ganz verloren.‹ ›Uns geht's nicht besser,‹ antwortete der Hauptmann, und sah den Schuster scharf an, ›wir sind ihm wohl auf der Spur, aber vom Fangen war noch keine Rede. Setzt euch nun zu Tische und eßt mit, hernach sprechen wir weiter und ihr könnt dann um so besser marschiren.‹ Das thaten die Beiden und der Jäger gab genau auf Alles Acht, was der Schuster machte und that ebenso. Der legte aber Löffel und Gabel verkehrt, denn das war das Erkennungszeichen der andern Bande. ›Jetzt sehe ich, daß ihr zu der Bande gehört,‹ sagte der Hauptmann, ›bisher konnte ich es noch nicht glauben, denn du Jäger siehst gar nicht wie ein ächter Räuber aus.‹ Dann ging das Leben erst recht los, sie erzählten sich von ihren Thaten und der Schuster log ihnen den Buckel voll, mehr als ein Karrengaul ziehen kann; dazu wurde gegessen und getrunken, als sollte in acht Tagen kein Mittag mehr gehalten werden.</p>
                    <p rend="zenoPLm4n0" xml:id="tg13.3.7">Nach dem Essen mußte jeder ein Kunststück machen. Als die Reihe an den Schuster kam, sprach er, jetzt wolle er ein Stückchen machen, daß Alle ihre Freude daran haben sollten, und einen Kessel mit siedendem Oel austrinken. ›Das ist unmöglich,‹ riefen die Räuber. ›Nun ihr werdet schon sehen,‹ sprach der Schuster und die alte Frau setzte den größten Kessel aufs Feuer und goß <pb n="67" xml:id="tg13.3.7.1"/>
ihn voll Oel. Als es nun recht wallte und Blasen warf, sprach er: ›Setzt euch nun im Halbzirkel um mich, damit ihr es gut sehet, und du Kamerad tritt hinter mich und mach den andern Platz.‹ Jetzt holte er den Kessel vom Feuer, hob ihn zum Munde empor und rief: ›Run paßt auf!‹ Aber er hütete sich wohl, das Oel zu trinken, sondern schwenkte den Kessel im Kreise umher, daß das glühende Oel den Räubern in die Gesichter zischte, griff dann rasch nach seinem Schwert und schlug sie nieder, einen nach dem Andern, ehe sie sich besinnen konnten.</p>
                    <p rend="zenoPLm4n0" xml:id="tg13.3.8">Als der Schuster mit den Räubern fertig war, schaute er sich nach seinem Kameraden um, doch konnte er ihn lange nicht finden. Endlich zog er ihn unter einer Bank hervor, wohin er sich verkrochen hatte. ›Du bist mir ein tapferer Held,‹ sprach der Schuster, ›der die Kourage malterweise verschlungen hat. Heraus jetzt, du siehst ja, daß die Arbeit gethan ist und hilf mir die Kerle fortschaffen, ehe die andern kommen, wenn dir dein Leben lieb ist.‹ Da half der Jäger, aber er stellte sich schlecht an, man sah wohl, daß ihm die Arbeit nie sauer geworden war. Sie machten vor dem Räubernest ein großes Loch, warfen die ganze Bande hinein und stopften ihnen das Maul mit Erde. Die alte Frau aber reinigte derweil das Zimmer vom Oel und Blut und machte Alles wieder in Ordnung; dann kochte sie das Essen für die zweite Bande.</p>
                    <p rend="zenoPLm4n0" xml:id="tg13.3.9">Mittags um zwölf Uhr kam der Hauptmann mit seinen zwölf Spiesgesellen an. Keck ging der Schuster auf sie zu und sprach: ›Einen schönen Gruß von unserm Hauptmann, und er hätte den <pb n="68" xml:id="tg13.3.9.1"/>
König erwischt und käme um zwei Uhr mit ihm und unsern Leuten hierher; ihr solltet auf ihn warten.‹ ›Hat er ihn?‹ rief der Hauptmann. ›Aergert mich, daß ich ihn nicht fangen konnte, aber wir wollen doch lustig drauf essen und trinken. Setzt euch zu uns.‹ Da setzten sich die Beiden zu den Räubern und legten die Messer und Gabeln so, wie die erste Bande sie gelegt hatte. Sprach der Hauptmann: ›Nun sehe ich erst, daß ihr zur Bande gehört, bisher traute ich euch nicht, besonders dir nicht, Jäger, denn du siehst aus, als könntest du keinen Floh knicken!‹ Der Schuster fiel ihm in die Rede und erzählte viel von Mordthaten und Räubereien, welche die andre Bande seit gestern begangen habe, so daß die Räuber den Jäger ganz vergaßen. Nach dem Essen machte jeder sein Stückchen und der Schuster machte wieder seins mit dem siedenden Oel und so vortrefflich, daß keiner von den Räubern sich beklagen konnte, er habe zu wenig bekommen. ›Was könnt ihr mit euren verbrannten Köpfen noch machen,‹ rief er dann und hieb sie ihnen ab. Von dem Jäger war wieder keine Spur zu sehn. Als er die alte Frau frug, sprach sie, er sei auf den Boden gestiegen. Der Schuster stieg ihm nach und fand ihn in einem Bund Stroh versteckt. ›Sind sie Alle todt?‹ frug der Jäger in großer Herzensangst. ›Geh hinunter und frage sie selbst,‹ sprach der Schuster. ›Aber du bist ein rechter Zwiebelkopf, mich so im Stich zu lassen.‹ Da kroch der Jäger hervor und freute sich mit dem Schuster und der alten Frau, daß Alles so gut abgelaufen sei.</p>
                    <p rend="zenoPLm4n0" xml:id="tg13.3.10">Als sie das Raubnest durchsuchten, fanden sie ungeheuere <pb n="69" xml:id="tg13.3.10.1"/>
Schätze von Gold, Silber und Edelgestein, Kleidern, Waffen und andern Dingen. Diese schenkten sie dem Kloster, welches in der Nähe am Saume des Waldes lag und die alte Frau folgte den Schätzen in das Kloster, denn sie wollte nichts mehr von der Welt wissen. Der Schuster nahm nur so viel Gold für sich, als in seine Taschen ging, der Jäger wollte aber nichts anrühren.</p>
                    <p rend="zenoPLm4n0" xml:id="tg13.3.11">Jetzt gingen die beiden Gesellen weiter und kamen gegen Abend vor der Hauptstadt an. Da die Thore schon geschlossen waren, kehrten sie im nächsten Dorfe in einer kleinen Herberge ein. Als der Schuster am andern Morgen aufwachte und seinen Reisegefährten wecken wollte, war der durchgegangen und hatte nicht einmal seine Zeche bezahlt. ›Es ist gut, daß ich den Kerl los bin‹ dachte der Schuster, zahlte den Wirth und ging rüstig weiter der Stadt zu.</p>
                    <p rend="zenoPLm4n0" xml:id="tg13.3.12">Am Thor trat die Wache vor ihm in's Gewehr und der Offizier rief: ›Präsentirt's Gewehr!‹ ›Der hat wohl frühe schon zu tief in's Glas geguckt,‹ sprach der Schuster für sich und ging weiter auf das Schloß des Königs zu. Da sprang die Wache heraus, stellte sich in Reih' und Glied und der Offizier kommandirte: ›Präsentirt's Gewehr!‹ Er ging auf den Offizier zu und sprach verwundert: ›Was macht ihr denn für dummes Zeug, ich bin ja ein Schuster meines Handwerks und möchte wissen, ob ich beim König in Dienst treten kann.‹ ›Ich will eure Exzellenz zu seiner Majestät führen,‹ sprach der Offizier und der Schuster schüttelte den Kopf, zuckte die Achseln und dachte: ›Sind die Soldaten denn alle verrückt geworden?‹</p>
                    <p rend="zenoPLm4n0" xml:id="tg13.3.13">
                        <pb n="70" xml:id="tg13.3.13.1"/>
Der König war überaus gnädig gegen den Schuster, frug ihn, wie er heiße, woher er komme und was er verstehe? Der Schuster erzählte Alles aufs Haar. ›Würdest du denn deinen Kameraden den Jäger wohl wiedererkennen, wenn du ihn sähest? Ei den Burschen fände ich aus Hunderten heraus!‹ Da ging der König fort in ein anderes Zimmer und über eine Weile kam der Jäger herein. ›Ach da bist du ja, du Hasenfuß!‹ rief der Schuster; ›du bist mir der rechte Vogel, aber ich habe dem König Alles gesagt und der wird dir einen tüchtigen Ausputzer geben.‹ ›Gemach, gemach, mein guter Freund,‹ sagte der Jäger und knöpfte den Rock auf und zog die Mütze aus dem Gesicht; da sah der arme Schuster, daß es der König selber war. Er wäre vor Schrecken fast auf den Rücken gefallen, aber der König beruhigte ihn, verbot ihm etwas von der Sache zu erzählen und schickte ihn sogleich als Oberst zu dem Regiment, wobei das Schneiderlein Feldwebel war.</p>
                    <p rend="zenoPLm4n0" xml:id="tg13.3.14">›Warte Bürschlein, jetzt sollst du mir herhalten,‹ dachte der Oberst, als er zuerst in seiner prächtigen Uniform mit dem Federhut vor die Fronte ritt und seinen alten Kameraden, den Schneider erblickte. So oft jetzt exercirt wurde, hatte der Oberst etwas an dem Feldwebel auszusetzen. Bald war das Zeug nicht gut genug geputzt, oder der Säbel nicht blank, oder der Rock nicht rein, und dann regnete es Ehrentitel wie Fetthammel, Schmuzlappen, Faulpelz u.a. auf den Feldwebel von Morgens bis Abends. Wagte er, zu antworten oder sich zu entschuldigen, dann gab es Arrest wegen Widersetzlichkeit oder undienstlichen Benehmens; schwieg <pb n="71" xml:id="tg13.3.14.1"/>
er aber zu den Vorwürfen, dann wurde er verstockt und Gott weiß wie genannt; kurz das arme Schneiderlein konnte nichts mehr recht machen, er mochte sich anlegen, wie er wollte, und war mehr als einmal in heller Verzweiflung.</p>
                    <p rend="zenoPLm4n0" xml:id="tg13.3.15">Nach einiger Zeit wurde es plötzlich vor den Obersten gerufen. Der frug ihn: ›Kennst du mich?‹ Der Feldwebel wußte nicht, was antworten, denn sagte er ja, dann log er und es taugte nicht für ihn, sagte er nein, was die Wahrheit war, dann taugte es ebensowenig. Endlich entschloß er sich frisch heraus die Wahrheit zu sagen, weil dieß doch das Beste sei und sprach: ›Nein.‹ ›Dann will ich dir sagen, wer ich bin‹, sprach der Oberst, ich bin dein alter Kamerad, der Schuster. Ich denke du wirst jetzt klüger geworden sein und deinen Hochmuth und Prahlerei lassen. Zum Lohn für deine Nöthen ernenne ich dich aber zum Oberfeldwebel.</p>
                    <p rend="zenoPLm4n0" xml:id="tg13.3.16">Seitdem sprach das Schneiderlein nie wieder von seinem Muth, brachte es auch nicht weiter. Der Schuster aber starb als Generalfeldmarschall.</p>
                </div>
            </div>
        </body>
    </text>
</TEI>