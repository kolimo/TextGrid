<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg159" n="/Literatur/M/Altenberg, Peter/Prosa/Was der Tag mir zuträgt/Die Tante">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Die Tante</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Altenberg, Peter: Element 00169 [2011/07/11 at 20:31:54]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Peter Altenberg: Was der Tag mir zuträgt. 12.–13. Auflage, Berlin: S. Fischer, 1924.</title>
                        <author key="pnd:118502255">Altenberg, Peter</author>
                    </titleStmt>
                    <extent>105-</extent>
                    <publicationStmt>
                        <date>1924</date>
                        <pubPlace>Berlin</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1859" notAfter="1919"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg159.3">
                <div type="h4">
                    <head type="h4" xml:id="tg159.3.1">Die Tante</head>
                    <p xml:id="tg159.3.2">Ich habe eine Tante. In erster Erinnerung habe ich sie folgendermaassen: Mein Onkel hielt bei dem Hochzeitsdiner einen Toast. Es war um dieselbe Zeit, als sich in meinem Munde zwei candirte Erdbeeren, ein Praliné mit Kaffeefüllung, eine Pâte de Noisette und ein Ananas-Fondant sammt Papierhülse befanden. Da sagte mein Vater: »Siehst Du..!?« Es bezog sich auf das Citat von Goethe, mit welchem der Toast schloss. Siehst Du, wie schön es ist, wenn man Etwas weiss, Alles ehrt Dich und Du bekommst sogar eine Braut.</p>
                    <p rend="zenoPLm4n0" xml:id="tg159.3.3">Ich sah wirklich, dass man dafür eine ziemlich magere und nicht sehr wundervolle Braut erhalten könne. Am Schlusse des Diners sah ich, dass mein Onkel mit ihr bei einem gelben seidenen damastirten Vorhange stand und wahrscheinlich sagte: »So schliesse ich denn mit dem Ausspruche Goethe's..«, worauf meine Tante das bizarre Muster der Damastvorhänge in genauen Augenschein zu nehmen nicht umhin konnte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg159.3.4">Sehr bald nach diesen Begebenheiten wurde meine Tante fett und mein Onkel schrieb ein Werk über den National- Wohlstand. Ich wusste nur, das meine<pb n="105" xml:id="tg159.3.4.1"/>
 Tante schrecklich lachen konnte, zum Beispiel wenn Jemand sagte: »Sie wissen doch, wie Herr Z. geht?! So geht er..« Da lachte sie, schüttete sich aus und ihre Arme wurden ganz kurz und dick und bebten. Mein Onkel dachte über Alles: »vom Standpunkte des Nationalökonomen – –«. Er fühlte: »Das Genie dreht sich um einen fixen Punkt herum, bezieht Alles ein, das sind einmal seine Kehrseiten –«,</p>
                    <p rend="zenoPLm4n0" xml:id="tg159.3.5">»Wie Clotilde lachen kann ...!« sagten alle Damen bei den Jour – Jausen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg159.3.6">»Jawohl,« sagte Eine, »ihr Mann berechnet es als Ersparniss am National – Vermögen, man verwerthet die Nährstoffe besser, nützt sie aus; Lachen ist gesund. <hi rend="italic" xml:id="tg159.3.6.1">Trauer – Verschwendung an vitalen Kräften, Freude – Ersparniss</hi>! Alles ist eine Kette.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg159.3.7">Ein junges Mädchen sagte: »Ich glaube, ihr Lachen ist ein Weinen; es bleibt sich ziemlich gleich ... nur umgekehrt ...«.</p>
                    <p rend="zenoPLm4n0" xml:id="tg159.3.8">»Rede doch keine Dummheiten,« sagte man zu dem jungen Mädchen, »Du bist schon ganz eine Hysterische.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg159.3.9">Eines Nachts traf ich meine Tante mit ihrem Töchterchen auf einem Balle. Sie hatte ein rothes seidenes Kleid an, war sehr fett und sah überhaupt aus wie eine Mortadella.. Das Töchterchen promenirte mit Millionärssöhnen, welche »von« hiessen und schneeweisse Frackgilets hatten mit goldenen Knöpfen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg159.3.10">Die Tante sagte zu mir: »Du, ich möchte Dir Etwas zeigen, geh', komme mit mir..!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg159.3.11">
                        <pb n="106" xml:id="tg159.3.11.1"/>
Sie führte mich durch die Säle.</p>
                    <p rend="zenoPLm4n0" xml:id="tg159.3.12">Sie blieb in einem Zimmer stehen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg159.3.13">»Da ist es ...,« sagte sie, »ich bitte Dich, schaue Dir sie an ...!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg159.3.14">Da sass eine junge rothblonde Amerikanerin, welche aussah wie ein Engel und wie der Himmel und wie alle Blumen!</p>
                    <p rend="zenoPLm4n0" xml:id="tg159.3.15">Meine dicke Tante und ich standen da . ...</p>
                    <p rend="zenoPLm4n0" xml:id="tg159.3.16">Meine Tante, die Mortadella, faltete die Hände und sagte leise: »Gott schütze sie!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg159.3.17">Ich führte sie zurück ...</p>
                    <p rend="zenoPLm4n0" xml:id="tg159.3.18">Sie war ganz verlegen. »Du« sagte sie, »sage nichts davon meinem Manne und meiner Tochter, ich habe sie Dir nur gezeigt, weil Du doch so ein verrückter Mensch bist . ...«</p>
                    <p rend="zenoPLm4n0" xml:id="tg159.3.19">Ich blickte sie an und sagte: »Jawohl ...«</p>
                    <p rend="zenoPLm4n0" xml:id="tg159.3.20">Dann sagte sie: »Mit was für Herren Buben promeniert meine Tochter ...?!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg159.3.21">»Pst,« sagte ich, »vom Standpunkte des National-Wohlstandes ...«</p>
                    <p rend="zenoPLm4n0" xml:id="tg159.3.22">»Ja,« sagte sie, »Elsie soll aber auch heiraten, und gut und reich und glücklich ...«</p>
                    <p rend="zenoPLm4n0" xml:id="tg159.3.23">»Jawohl ...,« sagte ich; »bist Du glücklich, Tante?!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg159.3.24">»Ich bin zu fett und zu verrückt dazu ...,« sagte sie, »aber das bleibt unter uns.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg159.3.25">»Für Letzteres wenigstens bürge ich ...,« sagte ich, worauf meine Tante einen Lachkrampf bekam.</p>
                    <pb n="107" xml:id="tg159.3.26"/>
                </div>
            </div>
        </body>
    </text>
</TEI>