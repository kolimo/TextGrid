<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg183" n="/Literatur/M/Altenberg, Peter/Prosa/Was der Tag mir zuträgt/Café - Chantant">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Café - Chantant</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Altenberg, Peter: Element 00194 [2011/07/11 at 20:31:54]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Peter Altenberg: Was der Tag mir zuträgt. 12.–13. Auflage, Berlin: S. Fischer, 1924.</title>
                        <author key="pnd:118502255">Altenberg, Peter</author>
                    </titleStmt>
                    <extent>221-</extent>
                    <publicationStmt>
                        <date>1924</date>
                        <pubPlace>Berlin</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1859" notAfter="1919"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg183.2">
                <div type="h4">
                    <head type="h4" xml:id="tg183.2.1">Café – Chantant</head>
                    <p xml:id="tg183.2.2">Nach der Vorstellung, Mitternacht, soupieren die Kavaliere mit den »Stars«.</p>
                    <p rend="zenoPLm4n0" xml:id="tg183.2.3">Fünf junge Damen sind es, Schwestern. Vier sind hellblond, mit tiefen Scheiteln in ihren seidenen leichten Haaren. Eine ist hellbraun, mit tiefem Scheitel in ihren seidenen leichten Haaren.</p>
                    <p rend="zenoPLm4n0" xml:id="tg183.2.4">Alle fünf tragen weite seidene schwarze Kleider und hellgraue Empire-Hüte mit drei schwarzen Straussfedern. Eine sechste ist in Reserve da. Plötzlich ist sie verschwunden. Wohin?! Niemand könnte es ergründen. Entführt, versunken?!?</p>
                    <p rend="zenoPLm4n0" xml:id="tg183.2.5">Siehst Du, wie gut es ist, dass eine in der Reserve ist?! Gleich bestellt man einen neuen Reservisten und ein schwarzes Seidenkleid und einen Hut Empire.</p>
                    <p rend="zenoPLm4n0" xml:id="tg183.2.6">Ein Graf schrieb der wunderbaren Mage einmal in ihr englisches Stammbüchlein: »Wenn Sie haben eine üble Laune, mein Herr, so nehmen Sie nicht Beechams Pillen, sondern soupieren sie mit Mage, und Ihre Krankheit wird fort sein, ganz fort.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg183.2.7">Viele Herren versuchten seitdem dieses einfache Mittel und allen half es. Frohen Sinn verbreitet sie wirklich, wie ein Kind bei seinen Grosseltern.</p>
                    <p rend="zenoPLm4n0" xml:id="tg183.2.8">
                        <pb n="221" xml:id="tg183.2.8.1"/>
Ein Baron sagte einmal während eines Soupers: »Fünf little dogs wird man euch schenken, ihr Süssen, gelbe Hündchen mit dunklen Schnäuzchen. Alle werden zu gleicher Zeit auf eurem Schoosse sitzen und – – –«</p>
                    <p rend="zenoPLm4n0" xml:id="tg183.2.9">»Und?!« fragten die fünf jungen Mädchen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg183.2.10">»Und – – –. Kleine Hunde können nichts dafür.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg183.2.11">Die fünf Fräulein lachten darüber wie Kanarienvögel im Sonnenlichte. Ganze Trillerketten rollten sie, wie man bei »Harzern« sich auszudrücken pflegt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg183.2.12">»You are ein kleines Swein,« sagte Mage zu diesem Kavaliere und tippte ihn auf seine Glatze, welche er in höchstem Maasse besass.</p>
                    <p rend="zenoPLm4n0" xml:id="tg183.2.13">Die Kavaliere bestellten fünf Eierpünsche. Dafür schwärmen die jungen Fräulein. »Keinen Champagner! Keinen Rheinwein! Eierpunsch! Eierpunsch, o bitte – –!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg183.2.14">»Ich vermutete gar nicht, dass im Eierpunsche soviel Poesie läge,« sagte einer der Kavaliere und leckte Mages Löffelchen ab.</p>
                    <p rend="zenoPLm4n0" xml:id="tg183.2.15">Man fragte einmal die etwas massive Agne: »Agne, mein Mädchen, wieviel wiegst Du?!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg183.2.16">»Ich wiege soviel wie ich wiege – – plus immer dem Gewichte eines Eierpunsches.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg183.2.17">Mage war ganz verliebt in einen der Kavaliere.</p>
                    <p rend="zenoPLm4n0" xml:id="tg183.2.18">»Bin ich für dich Beechams Pille?!« sagte sie und sah ihm ganz hinein in seine Augen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg183.2.19">Ja, sie war für ihn Beechams Pille.</p>
                    <p rend="zenoPLm4n0" xml:id="tg183.2.20">
                        <pb n="222" xml:id="tg183.2.20.1"/>
»Wir werden Euch singen ein kleines englisches Lied, weil Ihr so gut seid zu uns und gebet Eierpunsch, ja?!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg183.2.21">Sie sangen ganz leise und freudig und wiegten ihre Köpfchen dabei.</p>
                    <p rend="zenoPLm4n0" xml:id="tg183.2.22">»Wundervoll – –,« dachten die Kavaliere, »sind wir mit Kindern oder mit Erwachsenen, zum Teufel?!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg183.2.23">Wie mit unseren Nichten ist es. Man sitzt auf dem Teppiche und sagt: ›Jetzt kommst Du dran, pitschi, patschi, hohohoho – – – –‹</p>
                    <p rend="zenoPLm4n0" xml:id="tg183.2.24">Jawohl, unsere ganzen Wünsche entziehen sie uns. Wir thun nur, was ihnen Freude macht, von ganzem Herzem. Durch nichts möchten wir sie kränken, aufschrecken.</p>
                    <p rend="zenoPLm4n0" xml:id="tg183.2.25">Agne, liebste Agne. Mage, liebste Mage. Fannie, liebste Fannie. Sissie, liebste Sissie. Maridy, liebste Maridy!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg183.2.26">Die fünf Mädchen trinken gerne Eierpunsch. Mit Kavalieren sitzen sie und amüsieren sich.</p>
                    <p rend="zenoPLm4n0" xml:id="tg183.2.27">Eine sechste ist in Reserve. Das schicksalsvolle Leben repräsentiert sie. Wie der Chor bei den Alten. Wie ein Roman im vorhinein. Ruhig schläft der Impresario; Die Romantik ist im Calcüle.</p>
                    <p rend="zenoPLm4n0" xml:id="tg183.2.28">Die Kavaliere aber werden zu Dichtern, die innerlich singen. Wie Lord Byron einst zur Gräfin, möchten sie sagen: »Oh, ne m'accordez jamais, ce que ma démence vous implore sans cesse, afin que notre amour reste éternellement beau et au-dessus de l'humanité! « <pb n="223" xml:id="tg183.2.28.1"/>
Ja wirklich, das möchten die Kavaliere beinahe beten, wenn auch nicht so schwungvoll.</p>
                    <p rend="zenoPLm4n0" xml:id="tg183.2.29">Mage, o Mage, Beechams wiederherstellende Pille!</p>
                    <pb n="224" xml:id="tg183.2.30"/>
                </div>
            </div>
        </body>
    </text>
</TEI>