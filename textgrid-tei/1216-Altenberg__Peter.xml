<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg251" n="/Literatur/M/Altenberg, Peter/Prosa/Was der Tag mir zuträgt/Drei junge Damen vom Hof-Opern-Balett">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Drei junge Damen vom Hof-Opern-Balett</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Altenberg, Peter: Element 00263 [2011/07/11 at 20:31:54]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Peter Altenberg: Was der Tag mir zuträgt. 12.–13. Auflage, Berlin: S. Fischer, 1924.</title>
                        <author key="pnd:118502255">Altenberg, Peter</author>
                    </titleStmt>
                    <extent>298-</extent>
                    <publicationStmt>
                        <date>1924</date>
                        <pubPlace>Berlin</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1859" notAfter="1919"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg251.3">
                <div type="h4">
                    <head type="h4" xml:id="tg251.3.1">Drei junge Damen vom Hof-Opern-Balett</head>
                    <p xml:id="tg251.3.2">Einmal, nach einer Vorstellung von »Rund um Wien«, sagte der Freund, der Mäcen, zu dem Dichter:</p>
                    <p rend="zenoPLm4n0" xml:id="tg251.3.3">»Nun, habe ich Dir zuviel erzählt von diesen drei ›Keimen von Königinnen?!‹ Eh!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg251.3.4">»Die Rundliche, die <hi rend="italic" xml:id="tg251.3.4.1">Ueberzarte</hi>, die <hi rend="italic" xml:id="tg251.3.4.2">Correcte</hi> – – –«, erwiderte der Dichter kalt. »Was weiter?!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg251.3.5">»Oh – – – schäme Dich. Ein Dichter bist Du?! Diese drei Elevinnen des Corps de ballet, das sind die wahren Märchenprinzessinnen von heutzutage! Ganz exceptionel leben sie dahin. Wie in einer schöneren Welt in einem fort. Gepflegt vom Scheitel bis zur Zehe. Denke Dir, und immer eigentlich in Zauberreichen – – –. Daraus musst Du eine Studie machen. Das ist ein Thema für Dich. Du sagst doch immer: »Ideal und Leben« à la Schiller. Nun, hier hast Du es – – –.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg251.3.6">»Nein«, erwiderte der Dichter, »es ist kein Thema. Ich spüre es. Es riecht nach Mädchen-Institut. Süsslich, sentimental, zu billig.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg251.3.7">»Weil Du es nicht verstehst, ganz einfach. Nimm diese drei jungen Damen zum Beispiel gleich, wie <pb n="298" xml:id="tg251.3.7.1"/>
sie zu Hause im ärmlichen Gemache, Aschenbrödelchen – – –. Und Abends, in Seide und Sammet oder fliegenden Röckchen! Oder wie sie sich als »Sironi träumen, bereits bequemer auf dem Daumen-Ballen schweben oder einen Cavalier ersehnen der Parterre-Logen und bereits den Fiaker spüren mit elektrischem Lichte an der Wagendeichsel! Und ihre Freundinnen zerspringen! Sie zerspringen! Sie zerplatzen directement!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg251.3.8">»Das ist gut. Das ist wahr. Das ist das ›Wesentliche‹. Das Ewige! Ein einziges fassbares Glück wird es hier geben bei diesen drei Rosenknöspchen im Ballet-Corps: ›Die Anderen zerplatzen!‹ Davon kann man leben: ›Sie zerplatzen!‹«</p>
                    <p rend="zenoPLm4n0" xml:id="tg251.3.9">»Ich verstehe das nicht,« erwiderte der Mäcen, »warum gerade dieses brutale Wort? Ist es kein Thema vielleicht, das Schicksal dieser drei Nixen, welche in den Ocean des Lebens sich begeben, vollkommen bestimmen zu können im vorhinein?! Wie wenn man es selbst leitete und lenkte und ein Poseidon wäre mit Gabelzinken, mit Gabelzinken und Delphinen?! So bestimmt kennte man es. Das Werden, das Vergehen, das Erträumen, das Erreichen und dennoch Alles wieder nur Plunder und Hoffart?!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg251.3.10">»Dichter – – –« sagte der Dichter.</p>
                    <p rend="zenoPLm4n0" xml:id="tg251.3.11">»Wie wenn die Minderwerthigen, das ganze Corps de ballet bereits fahl hingrinste: Frau Gräfin, Frau Baronin, Frau Fürstin auf 1000 Stunden! Die beste Rouge erhält man bei – – –. Eine Tabaktrafik <pb n="299" xml:id="tg251.3.11.1"/>
wäre billig zu haben – – –. Von Creosot nehme man je 3 Pastillen, steige bis 9, dann pfutsch!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg251.3.12">»Es ist dennoch kein Thema,« erwiderte der Dichter, »man kommt aus dem Formulare nicht heraus. Aber das verstehst Du nicht. Physiologisch müsste man diese drei Prinzessinnen nehmen, hier wäre die versteckte Quelle ihres Schicksals: Die eine, die nicht weiss, dass sie in drei Jahren ganz fett sein wird, schwammig, und die Andere, dass sie ganz dünn sein wird, zum Hin-Werden. Aber die dritte, die rothbraune mit den schwarzen Augen, Teresa B., hat Ewigkeiten in sich, Alm-Rasse, einen herrlichen Stoffwechsel. Wie nach dem Metronome Gottes kreist ihr Blut. Stürme werden erbrausen und sie wird einen Fiaker erhalten mit elektrischem Lichte an der Wagendeichsel, vielleicht sogar einen Self-Motor mit Benzin!</p>
                    <p rend="zenoPLm4n0" xml:id="tg251.3.13">Alles ist der Stoffwechsel! Er führt zum Sieg oder zu Niederlagen! Turne, schwinge Keulen, atme sauerstoffreiche Luft, iss leicht verdauliche Speisen, bade bei 24 Grad, übe die Rumpfbeuge nach vorne und rückwärts – – und Du erhälst einen Prinzen und einen Motor-Wagen! Verfette Dich, verdünne Dich – und Du erhältst einen Schmarren!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg251.3.14">»Entschuldige,« sagte der Mäcen, »ist es ein Thema?! Keine Zeitschrift nimmt es Dir auf.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg251.3.15">»Es ist die einzige tiefe Wahrheit, die in diesen Dreien verborgen liegt, das Mysterium ihrer Natur, der Schlüssel zu dem Schicksal ihres Werdens. Verfettung und Verdünnung, das entscheidet im <pb n="300" xml:id="tg251.3.15.1"/>
Damen-Leben! Was meinst Du?! Die Laune einer Frau, die Grazie hängt davon ab, der Glanz der Augen und der Teint und die Verführungskünste des Blutes und der Lebensdrang der Nerven und ihre Durstigkeiten!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg251.3.16">»Ein Dichter – –?!«, fühlte der Mäcen, »ein Hygieniker!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg251.3.17">»Siehe! Diese Herrliche, Jugendliche, in purpur-rothem Sammet, hat ihr ›Sedan‹ in sich. Sie wird verfetten! Hélas – – –. Und pfutsch! Und diese Süsse, Zarte, wie ein Stengel einer Schlingpflanze, hat ihr ›Waterloo.‹ Sie wird verdünnen! Ein Gerippchen mit seelenvollem Antlitz! Die Dritte aber wird bleibend sein und siegreich wie in Erz gegossene Standbilder von Göttinnen! Ewige Reorganisations-Kräfte liegen in ihrem Leibe wie im Gehirne Shakespeare's!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg251.3.18">Der Mäcen schwieg. Dann sagte er verlegen:</p>
                    <p rend="zenoPLm4n0" xml:id="tg251.3.19">»Es war eine dumme Idee von mir. Nein, es ist kein Thema. Es ist nichts. Man glaubt manchmal. Es ist jedoch gar nichts herauszuschälen. Lasse es sein. Selbst Deiner Kunst – – –«</p>
                    <p rend="zenoPLm4n0" xml:id="tg251.3.20">»Oho – – –« erwiderte der Dichter, »es ist ein Thema! Und just!! Feigling, elender Feigling! Erbärmlich, verlegen, furchtsam zieht Ihr Euch zurück vor den frischen freien Wahrhaftigkeiten, den Wesentlichkeiten des Lebendigseins!? Aber die lauwarmen dumpfen und verlogenen Erdichtungen vom Menschendasein und seinen Nöthen erwünschet Ihr!? Schulknaben des Lebens! Feiglinge! Ich aber sage<pb n="301" xml:id="tg251.3.20.1"/>
 Dir: Die Dramen des Frauen-<hi rend="italic" xml:id="tg251.3.20.2">Leibes</hi> sind tiefer und poetischer vielleicht als die der Frauen-<hi rend="italic" xml:id="tg251.3.20.3">Seele</hi>! Das sind die wahren Tragödien der Natur, die Dramen von Gott-Shakespeare! Nicht eure ›Weh und Ach‹ der Seele!!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg251.3.21">Der Mäcen ging schweigend. Er dachte: »Keine Zeitschrift der Welt nähme es ihm auf – – –.«</p>
                    <pb n="302" xml:id="tg251.3.22"/>
                </div>
            </div>
        </body>
    </text>
</TEI>