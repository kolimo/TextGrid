<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg253" n="/Literatur/M/Altenberg, Peter/Prosa/Was der Tag mir zuträgt/In München">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>In München</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Altenberg, Peter: Element 00265 [2011/07/11 at 20:31:54]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Peter Altenberg: Was der Tag mir zuträgt. 12.–13. Auflage, Berlin: S. Fischer, 1924.</title>
                        <author key="pnd:118502255">Altenberg, Peter</author>
                    </titleStmt>
                    <extent>305-</extent>
                    <publicationStmt>
                        <date>1924</date>
                        <pubPlace>Berlin</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1859" notAfter="1919"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg253.3">
                <div type="h4">
                    <head type="h4" xml:id="tg253.3.1">In München</head>
                    <p xml:id="tg253.3.2">Ich bin seit einigen Tagen zum erstenmale in München. Ich habe noch nichts gesehen, nichts, was in den Büchelchen angemerkt ist, keine Monumente, keine Bilder. Mich interessiren nicht die Dinge, die waren. Mich interessiren die Dinge, die <hi rend="italic" xml:id="tg253.3.2.1">sind</hi>, die <hi rend="italic" xml:id="tg253.3.2.2">kommen werden</hi>! Siehe! Aus den Vitrinen der edlen Geschäfte aber strahlte mir die <hi rend="italic" xml:id="tg253.3.2.3">»neue Kunst«</hi> entgegen, das, was Jeden, im Leben Eintrocknenden, zum etwas schwärmerischen Künstler-Menschen machen soll, wenn er es in seiner Heimaths-Klause Stunden und Stunden betrachten dürfte. Europäer, wo weilet Ihr noch?!? Ohne innere Freude stellet Ihr noch Meissener Figürchen und Vasen auf geschnitzte Schränke! Ihr düpirt Euch selbst!</p>
                    <p rend="zenoPLm4n0" xml:id="tg253.3.3">Ohne Zusammenhänge mit den wunderbaren Farben und Formen der Natur selber lebet Ihr, saget »ah!« zu Dingen, die Euch fremd und unsympathisch sind, nähret Euch von Phrasen, von Historie, kaufet Vasen mit Blüthen, die nie waren! Augen habt Ihr, die nicht geniessen können an und für sich selber, sondern von Namen und Schablonen dirigirt werden! Darum, weil Ihr diese edelsten <pb n="305" xml:id="tg253.3.3.1"/>
Organe nicht ausnützet, die Schätze dieser beiden reichen Augen, dieser unerschöpflichen, nicht aushebet, darum bleibet Ihr armselig, leer, traurig, suchet anderen Organen Genüsse zu entlocken, die sind und bereits nicht mehr sind! Dann kommen lange öde Stunden, die ermordet werden müssen mit diesen Giften »Trinken,« »Spielen« –!</p>
                    <p rend="zenoPLm4n0" xml:id="tg253.3.4">Sehet, der neue, der moderne Künstler will Euch aber mit der Natur vereinigen und ihren tiefen Prächten! Er will Eure Augen liebevoll machen für den Glanz des Lebens selbst, nicht für die Truggestalten der Phantasie, die nicht mehr wirken! <hi rend="italic" xml:id="tg253.3.4.1">Quellen</hi> sollet Ihr rauschen hören, nicht Cascaden! Eure Augen sollen sich in Liebe vermälen mit den Dingen, sollen Hochzeit feiern, edle Vereinigung!</p>
                    <p rend="zenoPLm4n0" xml:id="tg253.3.5">Aber Ihr weilet ferne, nehmet Plunder! Sehet, wie nahe noch ist der Knabe der Natur, wenn er den wunderbaren Apollofalter anschleicht auf der Berg-Distel?! Oder das Mäderl, welches ein Sträusschen bindet von Wiesenblumen?! <hi rend="italic" xml:id="tg253.3.5.1">Später aber kommt das Leben, macht blind, leer</hi> Lawn-tennis spielen sie dann auf den Wiesen, in der Natur! Lawn-tennis! Heisse Wangen mit kalten Seelen!</p>
                    <p rend="zenoPLm4n0" xml:id="tg253.3.6">Lernet vom Japaner! Wenn die Kirschenbäume in Blüte stehen, zieht das Volk hinaus, steht stumm und stundenlang vor der weissrosigen Pracht. Keine Bänke und Tische sind errichtet, an welchen man frisst und säuft. Stumm steht das künstlerische Volk vor der weiss-rosigen Pracht, stundenlange! In den Zimmern aber hängen an reinlichen edlen <pb n="306" xml:id="tg253.3.6.1"/>
zarten hellgelben Matten Bambuskörbchen mit feinen Blumen. Da gehen Männer und Frauen hin, betrachten die Körbchen mit Blumen, gehen wieder weg, still an's Tagewerk. Was für Krimskrams habet Ihr aber auf den Schreibtischen, an Euren Wänden?! Ihr <hi rend="italic" xml:id="tg253.3.6.2">habt</hi> es und fertig! Was giebt es da zu betrachten?! Man <hi rend="italic" xml:id="tg253.3.6.3">besitzt</hi> es, man <hi rend="italic" xml:id="tg253.3.6.4">liebt</hi> es nicht!</p>
                    <p rend="zenoPLm4n0" xml:id="tg253.3.7">Stellet doch da lieber unter Glas die wirklichen Kunstwerke der Natur, wunderbare exotische Käfer oder edle Muscheln in matten Farben! Diese Farben der Käfer, Muscheln, Schmetterlinge, Steine, die wirklichen Formen der Blüthen und Blätter fangen nun die »neuen Künstler« für Euch ein im Kunstgewerbe, stellen es in die Vitrinen, schenken Euch die wunderbare Natur, die anzuschauen Niemand müde wird, der einmal, einmal wirklich geschaut hat mit jenen Augen, welche Verbindungsbahnen haben mit der Seele und dem Geiste, ja der erschauende Geist die betrachtende Seele selbst geworden sind!</p>
                    <p rend="zenoPLm4n0" xml:id="tg253.3.8">Was kaufet Ihr zusammen?! Schämet Euch! Besitz!? Gott, Besitz muss sein wie der Besitz seiner Haut, seiner Hände! Es gehört zu mir, ist unentbehrlich, erhält gleichsam den Gesammt-Organismus, ist ein feiner Theil desselben, der äusserste, über die Epidermis hinaus! Was auf meinem Tischchen steht, an meinen Wänden hängt, gehört mir zu, wie meine Haut und meine Haare. Es lebt mit mir, in mir, von mir. Ohne dasselbe wäre ich fast ein Rudimentärer, Verkümmerter, Aermerer. Zum Beispiel meine Freundin, die »dunkle Dame« und das Bild <pb n="307" xml:id="tg253.3.8.1"/>
von Burne-Jones: »Ein Mädchen sitzt im Garten am Gestade, hat die Hände auf einem alten Buche auf ihrem Schoosse, ist zurückgelehnt. Zwei Engel musiziren ihr vor, und, die Hände auf dem alten Buche, zurückgelehnt, träumt das Mädchen, im Garten am Gestade, schwebt hinweg von Buch und Garten, wohin, wohin denn?!« Dieses Bild und die »entrückte Dame«, über deren Bett es hing, waren <hi rend="italic" xml:id="tg253.3.8.2">eines</hi>! Wer das Bild verstand, verstand sie, wer sie verstand, verstand das Bild. Kein Anderes könnte über ihrem Bette hängen. Es gehörte zu ihr, zu ihr, wie ihre Hände und Haare. Die Dame lauscht, wohin, wohin denn?!</p>
                    <p rend="zenoPLm4n0" xml:id="tg253.3.9">Mit solchen Euch zugehörigen, mit Euch einheitlichen Dingen müsset Ihr Euch umgeben, neue Menschen! Der neue Künstler gestaltet dieselben aus seinem Genie heraus, die Dinge, die für Eure Seelen sind! Die wirklich zugehörigen! Tünchet Eure Wände einfach weiss und stellet in eine Ecke oder an die Wand eine jener wunderbaren Schalen, die den Glanz von fliegenden Kolibris, untergehenden Sonnen und Meeresschäumen haben!</p>
                    <p rend="zenoPLm4n0" xml:id="tg253.3.10">Eine Vase sah ich hier, hellbraun mit Aufleuchten in Gold und Erdüstern in dunklen Streifen. Dann eine gelbliche, in Milchfarbe erblassend. Dann eine, ganz durchscheinend, geformt als eine riesige Bienen-Wabe mit Zellen, wachsgelb. Dann solche wie die grünen Flügel von Eintagsfliegen. Eine dunkelblaue, welche in Früh – Himmelfarben überging, von Nacht zum Morgen und wieder düster wurde, nächtlich. Dann kugelige, hellbraune Knollen aus Glas auf GlasBambusstengeln, <pb n="308" xml:id="tg253.3.10.1"/>
herrliche Gebilde. Gallé – Gläser; hellbraune Blumen kommen nebelhaft aus dem Glase selbst hervor und dennoch nicht hervor, verschwinden.</p>
                    <p rend="zenoPLm4n0" xml:id="tg253.3.11">Solche Vasen stünden nicht verlassen da in Euren Heimaths-Klausen! Solche Vasen liebte man mit allen Zärtlichkeiten! Wenn man in's Zimmer tritt, begrüsst man sie. Und wenn man geht, grüsst man sie wieder. Vertrauliche Genossen!</p>
                    <p rend="zenoPLm4n0" xml:id="tg253.3.12">Tünche Deine Wände weiss, in Einfachheiten, stelle Dinge hin, die Du lieb haben kannst, brüderlich, schwesterlich lieb, nicht kalte, fremde! So wirst Du reich sein und niemals vereinsamt!..</p>
                    <p rend="zenoPLm4n0" xml:id="tg253.3.13">Ich bin seit einigen Tagen in München, zum erstenmale, habe noch nichts gesehen, nichts, was in den Büchelchen angemerkt steht, keine Monumente, keine Bilder. Ich interessire mich nicht für die Dinge, die<hi rend="italic" xml:id="tg253.3.13.1"> waren</hi>; ich interessire mich für die Dinge, die sind, die <hi rend="italic" xml:id="tg253.3.13.2">kommen werden</hi>! Aus den Vitrinen aber der edlen Geschäfte strahlte mir die <hi rend="italic" xml:id="tg253.3.13.3">»neue Kunst«</hi> entgegen, als ich einsam durch die schönen Strassen ging!</p>
                    <pb n="309" xml:id="tg253.3.14"/>
                </div>
            </div>
        </body>
    </text>
</TEI>