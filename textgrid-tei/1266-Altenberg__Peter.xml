<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg301" n="/Literatur/M/Altenberg, Peter/Prosa/Märchen des Lebens/Das Tanzturnen">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Das Tanzturnen</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Altenberg, Peter: Element 00315 [2011/07/11 at 20:31:54]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Peter Altenberg: Märchen des Lebens. 7.–8. Auflage, Berlin: S. Fischer, 1924.</title>
                        <author key="pnd:118502255">Altenberg, Peter</author>
                    </titleStmt>
                    <extent>48-</extent>
                    <publicationStmt>
                        <date>1924</date>
                        <pubPlace>Berlin</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1859" notAfter="1919"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg301.3">
                <div type="h4">
                    <head type="h4" xml:id="tg301.3.1">Das Tanzturnen</head>
                    <p xml:id="tg301.3.2">Ich habe die Absicht, heranreifenden Mädchen, mit Hinweglassung des schrecklichen und unnützen <hi rend="italic" xml:id="tg301.3.2.1">Geräteturnens</hi>, höchste Beweglichkeit, Anmut, Elastizität beizubringen!</p>
                    <p rend="zenoPLm4n0" xml:id="tg301.3.3">Bewegung ohne <hi rend="italic" xml:id="tg301.3.3.1">sauerstoffreiche</hi> Luft ist Widersinn gegen die Natur, da eben durch Bewegung die Verbrennungsprozesse des <hi rend="italic" xml:id="tg301.3.3.2">Sauerstoffes</hi> der intensiv eingeatmeten Luft mit den Kohlehydraten des Organismus <hi rend="italic" xml:id="tg301.3.3.3">vehement gesteigert werden sollen!</hi>
                    </p>
                    <p rend="zenoPLm4n0" xml:id="tg301.3.4">Turnen <hi rend="italic" xml:id="tg301.3.4.1">ohne Mithilfe sauerstoffreicher Luft</hi> ist direkt <hi rend="italic" xml:id="tg301.3.4.2">eine Schädigung!</hi> Ich versetze hierbei den unglückseligen Organismus durch erhöhte Bewegung in die Lage, seine Verbrennungsprozesse zu steigern, und entziehe ihm zugleich das <hi rend="italic" xml:id="tg301.3.4.3">hierzu nötige Material. Also eine unbewußte Folter der Organisation!</hi>
                    </p>
                    <p rend="zenoPLm4n0" xml:id="tg301.3.5">Turnen in einem Raume, dem nicht <hi rend="italic" xml:id="tg301.3.5.1">ununterbrochen</hi> reine Luft zugeführt wird, ist eine <hi rend="italic" xml:id="tg301.3.5.2">Schädigung, Schwächung!</hi>
                    </p>
                    <p rend="zenoPLm4n0" xml:id="tg301.3.6">In meinem Turnraume befinden sich daher <hi rend="italic" xml:id="tg301.3.6.1">zwei erstklassige Elektroventilatoren,</hi> direkt in die oberen Fenster mittels Kreisausschnittes und schief gegen den Plafond zu zugeneigt, eingesetzt, die ununterbrochen frische Luft rasend schnell hereindrehen! Zugleich wird durch einen minutiös kontrollierten Heizapparat die Luft auf 14° R. erhalten.</p>
                    <p rend="zenoPLm4n0" xml:id="tg301.3.7">In diesem nun so vorbereiteten Raume erfolgt das »<hi rend="italic" xml:id="tg301.3.7.1">Tanzturnen</hi>«.</p>
                    <p rend="zenoPLm4n0" xml:id="tg301.3.8">
                        <pb n="48" xml:id="tg301.3.8.1"/>
Das wenn auch noch so scharfe und präzise Kommandowort des Turnleiters ist ein <hi rend="italic" xml:id="tg301.3.8.2">schwächlicher und unzulänglicher Auslöser</hi> von Bewegungskräften aus den Nervenzentren des Turnenden. Gestatten Sie es mir, Ihre Aufmerksamkeit darauf hinzulenken, daß unserem Organismus eine erstaunliche Menge in edelste leichteste intensivste Bewegung <hi rend="italic" xml:id="tg301.3.8.3">umsetzbarer</hi> vitaler Kräfte, latenter Potenzen geweckt, ausgelöst und verwertet werden könnte durch Zuhilfenahme des scharfen exzitierenden auslösenden und mitreißenden<hi rend="italic" xml:id="tg301.3.8.4"> Rhythmus der Musik!</hi>
                    </p>
                    <p rend="zenoPLm4n0" xml:id="tg301.3.9">Die »kleine Kniebeuge«, das »Wippen«, der »port de bras«, das »Anfersen«, »Grätschen«, das »Beinwerfen nach vor- und seitwärts«, die »tiefe Rumpfbeuge«, das »Armstoßen nach auf-, seit- und vorwärts« usw., würden bei einem unerhört scharfen präzisen, fast leidenschaftlichen Rhythmus eines amerikanischen Kriegsmarsches sofort ebenfalls in scharfer, fast leidenschaftlicher Art ausgeführt werden können, angefeuert und beflügelt gleichsam von inneren mysteriösen Kommandoworten, die ununterbrochen wirksam wären, <hi rend="italic" xml:id="tg301.3.9.1">innerliche Peitschen und Sporen!</hi>
                    </p>
                    <p rend="zenoPLm4n0" xml:id="tg301.3.10">Von unbewußter Lebensfreudigkeit, von Lebensbehendigkeiten angetrieben, führte man da rastlos die Bewegungen aus, folgte magisch angelockt dem Tempo des Musikstückes, würde mitgerissen in den Taumel des Rhythmus!</p>
                    <p rend="zenoPLm4n0" xml:id="tg301.3.11">Das laute gellende Kommandowort des Turnleiters möge dann die äußerste Ekstase bringen, letzte Lebenskräfte aus Schlupfwinkeln hervorlocken!</p>
                    <p rend="zenoPLm4n0" xml:id="tg301.3.12">
                        <pb n="49" xml:id="tg301.3.12.1"/>
Musik ist ein mysteriöser Motor für den Bewegungsapparat unseres Organismus und es ist merkwürdig genug, daß man bisher einen solchen beim Turnen nicht in Aktion gebracht hat!?!</p>
                    <p rend="zenoPLm4n0" xml:id="tg301.3.13">Wie schreitet das Militär zum Marschrhythmus? Wie werden Ermüdungen hinausgeschoben? Ausgelöscht!</p>
                    <p rend="zenoPLm4n0" xml:id="tg301.3.14">Meine Annonce lautet daher:</p>
                    <milestone unit="hi_start"/>
                    <p rend="zenoPC" xml:id="tg301.3.15">»<hi rend="italic" xml:id="tg301.3.15.1">Das Tanzturnen</hi>.</p>
                    <milestone unit="hi_end"/>
                    <p rend="zenoPLm4n0" xml:id="tg301.3.16">Freiturnen zur Musikbegleitung (Klavier). Ausnützung, Verwertung der <hi rend="italic" xml:id="tg301.3.16.1">motorischen Kräfte</hi> des Musikrhythmus für die Beweglichkeit des Organismus. (Zwei erstklassige Elektroventilatoren führen ununterbrochen reine Luft zu.)</p>
                    <p rend="zenoPLm4n0" xml:id="tg301.3.17">P.S. Ideal angelegte, von der Natur prädestinierte Geschöpfe zahlen die Hälfte!«</p>
                    <pb n="50" xml:id="tg301.3.18"/>
                </div>
            </div>
        </body>
    </text>
</TEI>