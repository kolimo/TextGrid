<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg303" n="/Literatur/M/Altenberg, Peter/Prosa/Märchen des Lebens/Unser Opernhaus">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Unser Opernhaus</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Altenberg, Peter: Element 00317 [2011/07/11 at 20:31:54]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Peter Altenberg: Märchen des Lebens. 7.–8. Auflage, Berlin: S. Fischer, 1924.</title>
                        <author key="pnd:118502255">Altenberg, Peter</author>
                    </titleStmt>
                    <extent>52-</extent>
                    <publicationStmt>
                        <date>1924</date>
                        <pubPlace>Berlin</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1859" notAfter="1919"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg303.3">
                <div type="h4">
                    <head type="h4" xml:id="tg303.3.1">Unser Opernhaus</head>
                    <p xml:id="tg303.3.2">Ein Freund sagte zu mir: »Komme mit zu ›Tristan und Isolde‹, du hast die neuen Dekorationen von <hi rend="italic" xml:id="tg303.3.2.1">Roller</hi> noch nicht gesehen ... Ich lade dich ein auf einen Parkettsitz.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg303.3.3">Wenn ich dieses »heilige Haus« betrete, dem ich die süßen Schauer in meiner Kindheit verdanke, dieses <hi rend="italic" xml:id="tg303.3.3.1">Wirklichkeit gewordene</hi> Märchenhaus, diesen edlen weiten Saal, in dem ich einst fast angstbeklommen »kommender Dinge lauschte«, bin ich immer sogleich ergriffen und gerührt, wenn auch nur wenige Besucher vorhanden sind und gleichsam noch Dämmerstimmung herrscht.</p>
                    <p rend="zenoPLm4n0" xml:id="tg303.3.4">Genialstes wunderbarstes Gebäude, mit deinem aristokratischen Entree, deiner wunderbaren Logenstiege, deinen breiten niederen Logengängen, deinen Logen, die wie kleine gemütliche intime Gemächer sind, mit deinen Galerien, auf denen liebevoll gesorgt ist für jeden einzelnen, der sehen und lauschen möchte! O, du fast merkwürdig geräumiger Saal, der alle Töne dennoch liebevollst und zartest in sich aufnimmt, wie wenn er es wüßte, daß es eben seine heilige ernste Aufgabe ist! Und deine beiden Erbauer, deine Erdichter mußten sich umbringen! Aus Furcht, ein unvollkommenes Werk geschaffen zu haben!?!</p>
                    <p rend="zenoPLm4n0" xml:id="tg303.3.5">Aber es war <hi rend="italic" xml:id="tg303.3.5.1">vollkommen, vollkommen</hi>; und wie ich in meiner Kindheit zum erstenmal zur Vorstellung »Die Hugenotten« diese wirklich heiligen Hallen betreten habe, so trete ich heute, ein Sechsundvierzigjähriger, noch immer mit süßem Schauer ein in dieses<hi rend="italic" xml:id="tg303.3.5.2"> Wundergebäude!</hi>
                    </p>
                    <p rend="zenoPLm4n0" xml:id="tg303.3.6">
                        <pb n="52" xml:id="tg303.3.6.1"/>
Du, du geliebter Saal, geliebtester Raum bist geblieben, liebevoll und zärtlich jene behandelnd, die sich dir anvertrauen, um ideal zu genießen!</p>
                    <p rend="zenoPLm4n0" xml:id="tg303.3.7">Aber was ist in uns, in uns vorgegangen seitdem!</p>
                    <p rend="zenoPLm4n0" xml:id="tg303.3.8">Wie flüchten wir immer angstvoller und angst, voller von Jahr zu Jahr in deinen heiligen Frieden-<hi rend="italic" xml:id="tg303.3.8.1">Operngebäude</hi>, vor den Tücken des Schicksals!</p>
                    <p rend="zenoPLm4n0" xml:id="tg303.3.9">Und nun kam ich durch die Güte eines Freundes wieder in »Tristan und Isolde«, anfangs September 1906. Wie eine märchenhafte Symphonie an und für sich: »Garten in der Sommernacht« ist der Beginn des zweiten Aktes. Und ebenso hat Roller es gemalt, erdichtet. Man hört, man spürt, man ahnt den nächtlichen Garten, in Stille und Duft vergraben, ein düsterer melancholischer Mitwisser menschlicher Begebenheiten. Wie wenn er das unglückselige Liebespaar liebevoll beschützen möchte durch seine nächtliche Stille, wie wenn er die letzte Romantik herbeischaffte für diese Edelromantiker, die dem Untergange geweiht sind!</p>
                    <p rend="zenoPLm4n0" xml:id="tg303.3.10">Die Sterne am nächtlichen Himmel funkeln und die Gebüsche sind schwarz und kompakt. Dann kommt der feuchte, kalte, graue Übergang. Alles wird hellgrau, nebelig und der Morgen dämmert. Und das Verhängnis bricht herein. Einzelne Rosenstöcke heben sich ab von der sanften Morgenröte und sind schwarz wie Silhouetten. Feuchte Ausdünstung ist im Garten. Die Gebüsche sind hellgraugrün. Man müßte sich Katarrhe holen, aber das Verhängnis läßt keine Zeit dazu! Der Morgen dämmert gelassen und bereits empfängt Tristan die Todeswunde ... Die Rosenstöcke heben sich ab von der Morgendämmerung.</p>
                    <p rend="zenoPLm4n0" xml:id="tg303.3.11">
                        <pb n="53" xml:id="tg303.3.11.1"/>
Als Kind kratzte ich meinem teuren, geliebten Vater auf der Violine zum erstenmal etwas vor. Es war schrecklich. Aber er war sehr ergriffen. Er führte mich zur Belohnung in die »Hugenotten« ins »neue Opernhaus«.</p>
                    <p rend="zenoPLm4n0" xml:id="tg303.3.12">Mein Vater sagte: »Das ist etwas für Kinder ...«</p>
                    <p rend="zenoPLm4n0" xml:id="tg303.3.13">Aber es war gar nichts für Kinder. Denn ich langweilte mich schrecklich und verstand gar nichts.</p>
                    <p rend="zenoPLm4n0" xml:id="tg303.3.14">Am nächsten Tage spielte uns Papa zu Hause die Partitur am Klavier vor. Das war noch entsetzlicher. Mit Meyerbeer waren wir endgültig verfeindet. Ein Beginn moderner Entwicklung.</p>
                    <p rend="zenoPLm4n0" xml:id="tg303.3.15">Papa sagte: »Ich weiß nicht, meine Kinder haben nichts von meinem musikalischen Talent geerbt ...« Nein, das hatten sie nicht.</p>
                    <p rend="zenoPLm4n0" xml:id="tg303.3.16">Aber das Haus, der Raum blieb mir in respektvoller Erinnerung. Es war eine <hi rend="italic" xml:id="tg303.3.16.1">Musikkirche.</hi> Und als ich damals erfuhr, daß die beiden Erbauer sich wegen der angeblichen Unvollkommenheit des Gebäudes umgebracht hatten, spürte ich einen fast persönlichen Schmerz über die grausame Ungerechtigkeit des Daseins! <hi rend="italic" xml:id="tg303.3.16.2">Heil Van der Nüll</hi> und <hi rend="italic" xml:id="tg303.3.16.3">Siccardsburg</hi>!</p>
                    <pb n="54" xml:id="tg303.3.17"/>
                </div>
            </div>
        </body>
    </text>
</TEI>