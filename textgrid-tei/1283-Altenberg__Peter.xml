<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg318" n="/Literatur/M/Altenberg, Peter/Prosa/Märchen des Lebens/Sonnenuntergang im Prater">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Sonnenuntergang im Prater</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Altenberg, Peter: Element 00332 [2011/07/11 at 20:31:54]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Peter Altenberg: Märchen des Lebens. 7.–8. Auflage, Berlin: S. Fischer, 1924.</title>
                        <author key="pnd:118502255">Altenberg, Peter</author>
                    </titleStmt>
                    <extent>90-</extent>
                    <publicationStmt>
                        <date>1924</date>
                        <pubPlace>Berlin</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1859" notAfter="1919"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg318.3">
                <div type="h4">
                    <head type="h4" xml:id="tg318.3.1">Sonnenuntergang im Prater</head>
                    <p xml:id="tg318.3.2">Sie waren stundenlang im Grabenkiosk gesessen, letzter Augusttag, hatten Fiaker betrachtet mit Fremden, Automobile, wie Zugvögel von fernen Reisen, Damen auf dem Trottoire, die wunderbar sicher dahinglitten, und andere, die trippelten und tänzelten, um etwas Besonderes aus sich zu machen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg318.3.3">In dem Kiosk saß eine Französin, die man nur mit den Augen grüßte. Und ein süßes, junges Geschöpf mit seiner »Tante«, das man auch nur mit den Augen begrüßte. Und fremde Damen mit Schleierhüten, die man überhaupt nicht grüßte. Und einige Männer, die schon vom Urlaube zurückgekehrt waren. Alle diese Menschen kamen sich ein bißchen deklassiert vor, daß man sie im Grabenkiosk ertappte in der Haute-Saison, während die anderen noch in Ostende oder Biarritz – – –</p>
                    <p rend="zenoPLm4n0" xml:id="tg318.3.4">Die beiden Freunde machten trotz alledem einige wichtige Beobachtungen, sammelten einige seltene Exemplare von Menschlein für ihre innerliche Käfersammlung, spießten sie auf, teilten sie ein in allgemeinere Klassen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg318.3.5">Um 6 Uhr kam das rote Automobil, Mercedes 18–24, entführte sie in die Krieau. Dort war ganz staubfreie Landluft und Stille. Ein Herr in schwarzem Anzug und schneeweißen Handschuhen bestieg ein Pferd. Ein Fiaker brachte eine Tänzerin (die Hofoper war bereits geöffnet), ein graues Automobil kam an, dumpf, Baryton singend, also über 30 HP. Das Gärtchen war voll gelber Blumen, die wie kleine Sonnenblumen aussahen, und die Kaninchen im <pb n="90" xml:id="tg318.3.5.1"/>
Käfig stellten die Ohren unregelmäßig schief. Die beiden Freunde rauchten Prinzesas und glotzten auf die zumeist leeren weißen Tische und Bänke. Im Vorfrühling, im Herbste entwickelt sich hier ein Leben und »Treiben«. Aber man hatte den 31. August!</p>
                    <p rend="zenoPLm4n0" xml:id="tg318.3.6">Infolgedessen fuhren die beiden Freunde weiter zum Winterhafen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg318.3.7">Donau, kleines Bahngeleise, große Lederfabrik, holperiges Granitpflaster, gut genug für Schneckengang gehende breiträderige Lastwagen! Das Automobil aber sprang, galoppierte, hüpfte, war wie deklassiert auf dieser gepflasterten Lastenstraße. Links war der Winterhafen, rechts ein erhöhtes Plateau aus Donausand und Donaukieselsteinen errichtet, bespickt mit jungen Birken. Da hatte man einen Rundblick auf bleigraue Hügel, schwarze Fabrikschornsteine und die Glut des Sonnenunterganges. Man sah das düstere Pulvermagazin, den Laaerberg, den Zentralfriedhof, den Kahlenberg – – –. Wie in grauem, flüssigem Blei des Himmels und der Erde wogte die dunkelrote Glut der Sonnenuntergangsstreifen. Die Lederfabrik war wie ein schwarzes Ungeheuer, und drei riesige Schornsteine sandten schwarzen Rauch in die Glut, wie schmale Dampfspritzen, die ungeheuere Brände löschen möchten! Die dünnen, zarten Birken auf dem Donauschütte bebten im Abendwind, und die beiden Freunde suchten schöne, glatte, hellbraune Kieselsteine aus als Andenken an den friedevollen Abend. Auf der Landstraße wartete das rote Automobil, Mercedes 18–24, das ein kleiner Landstraßen-Orientexpreßzug werden konnte bei Schnelligkeit vier.</p>
                    <p rend="zenoPLm4n0" xml:id="tg318.3.8">
                        <pb n="91" xml:id="tg318.3.8.1"/>
Die rote Glut im Blei des Himmels wurde himbeerfarbig, dann dunkelgraurot. Die beiden Freunde sagten: »Nun gibt es nichts mehr zu schauen. Das Stück ist zu Ende«. Sie bestiegen daher das rote Automobil und sagten zu dem Chauffeur: »Geschwindigkeit vier, bitte – – –«</p>
                    <p rend="zenoPLm4n0" xml:id="tg318.3.9">Sie rasten in den Grabenkiosk zurück.</p>
                    <p rend="zenoPLm4n0" xml:id="tg318.3.10">Dort saß noch die Französin, die man nur mit den Augen begrüßen durfte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg318.3.11">Aber in dieser Stunde durfte man bereits zu ihr sagen: »Guten Abend – – –«</p>
                    <p rend="zenoPLm4n0" xml:id="tg318.3.12">Und die beiden Herren sagten höflich: »Bon soir – – –«.</p>
                    <pb n="92" xml:id="tg318.3.13"/>
                </div>
            </div>
        </body>
    </text>
</TEI>