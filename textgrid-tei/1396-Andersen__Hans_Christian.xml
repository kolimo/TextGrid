<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg37" n="/Literatur/M/Andersen, Hans Christian/Märchensammlung/Märchen/Die glückliche Familie">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Die glückliche Familie</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Andersen, Hans Christian: Element 00032 [2011/07/11 at 20:24:31]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Andersen, H[ans] C[hristian]: Sämmtliche Märchen. Übers. v. Julius Reuscher, illustriert von Ludwig Richter u.a., 31. vermehrte Aufl., Leipzig: Abel &amp; Müller, [um 1900].</title>
                        <author key="pnd:118502794">Andersen, Hans Christian</author>
                    </titleStmt>
                    <extent>249-</extent>
                    <publicationStmt>
                        <date when="1900"/>
                        <pubPlace>Leipzig</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1805" notAfter="1875"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg37.2">
                <div type="h4">
                    <head type="h4" xml:id="tg37.2.1">
                        <pb n="249" xml:id="tg37.2.1.1"/>
Die glückliche Familie.</head>
                    <p xml:id="tg37.2.2">Das größte grüne Blatt hier zu Lande ist sicherlich das Klettenblatt; hält man es vor seinen kleinen Leib, so ist es gerade wie eine ganze Schürze, und legt man es auf seinen Kopf, dann ist es im Regenwetter fast ebenso gut wie ein Regenschirm, denn es ist ungeheuer groß. Nie wächst eine Klette allein, nein! Wo eine wächst, da wachsen auch mehrere, es ist eine große Herrlichkeit, und all' diese Herrlichkeit ist Schneckenspeise. Die großen weißen Schnecken, woraus vornehme Leute in früheren Zeiten Leckerbissen bereiten ließen, speisten und sagten: »Hm! Schmeckt das prächtig!« – denn sie glaubten nun einmal, daß dieselben gut schmecken – diese Schnecken lebten von Klettenblättern und deswegen wurden die Kletten gesäet.</p>
                    <p rend="zenoPLm4n0" xml:id="tg37.2.3">Nun gab es da ein altes Rittergut, wo man keine Schnecken mehr speiste, diese waren beinahe ganz ausgestorben, aber die Kletten waren nicht ausgestorben, sie wuchsen über alle Gänge und Beete, man konnte ihrer nicht mehr Meister werden. Es war ein förmlicher Klettenwald, hin und wieder stand ein Apfel- und ein Pflaumenbaum, sonst hätte man gar nicht vermuten können, daß dies ein Garten gewesen sei. Alles war Klette und drinnen wohnten die beiden letzten steinalten Schnecken.</p>
                    <p rend="zenoPLm4n0" xml:id="tg37.2.4">
                        <pb n="250" xml:id="tg37.2.4.1"/>
Sie wußten selbst nicht, wie alt sie waren, aber sie konnten sich sehr wohl erinnern, daß ihrer weit mehr gewesen, daß sie von einer Familie aus fremden Ländern abstammten und daß für sie und die Ihrigen der ganze Wald gepflanzt worden war. Sie waren nie aus demselben hinaus gekommen, aber sie wußten doch, daß es außerdem noch etwas in der Welt gab, was der Herrenhof hieß, und da oben wurde man gekocht, und dann wurde man schwarz, und dann wurde man auf eine silberne Schüssel gelegt, was aber dann weiter geschah, das wußten sie nicht. Wie das übrigens war, gekocht zu werden und auf einer silbernen Schüssel zu liegen, das konnten sie sich nicht denken, aber schön sollte es sein, und außerordentlich vornehm. Weder die Maikäfer, noch die Kröten oder die Regenwürmer, welche sie darum befragten, konnten ihnen Bescheid darüber geben; keiner von ihnen war gekocht worden oder hatte auf einer silbernen Schüssel gelegen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg37.2.5">Die alten, weißen Schnecken waren die vornehmsten in der Welt, das wußten sie; der Wald war ihrethalben da, und der Herrenhof war da, damit sie gekocht und auf eine silberne Schüssel gelegt werden konnten.</p>
                    <p rend="zenoPLm4n0" xml:id="tg37.2.6">Sie lebten nun sehr einsam und glücklich, und da sie selbst keine Kinder hatten, so hatten sie eine kleine, gewöhnliche Schnecke angenommen, die sie wie ihr eigenes Kind erzogen; aber die Kleine wollte nicht wachsen, denn es war nur eine gewöhnliche Schnecke. Die Alten, besonders die Mutter, die Schneckenmutter, glaubte doch zu bemerken, daß sie zunahm, und sie bat den Vater, wenn er das nicht sehen könnte, so möge er doch nur das kleine Schneckenhaus anfühlen, und dann fühlte er und fand, daß die Mutter recht habe.</p>
                    <p rend="zenoPLm4n0" xml:id="tg37.2.7">Eines Tages regnete es stark.</p>
                    <p rend="zenoPLm4n0" xml:id="tg37.2.8">»Höre, wie es auf den Kletten tromme-romme-rommelt!« sagte der Schneckenvater.</p>
                    <p rend="zenoPLm4n0" xml:id="tg37.2.9">»Da kommen auch Tropfen!« sagte die Schneckenmutter. »Es läuft ja gerade am Stengel herab! Du wirst sehen, daß <pb n="251" xml:id="tg37.2.9.1"/>
es hier naß werden wird. Ich bin froh, daß wir unsere guten Häuser haben und daß der Kleine auch eins hat! Für uns ist freilich mehr gethan, als für alle andern Geschöpfe, man kann also sehen, daß wir die Herren der Welt sind! Wir haben ein Haus von der Geburt ab und der Klettenwald ist unsertwegen gesäet! – Ich möchte wohl wissen, wie weit er sich erstreckt und was außerhalb desselben ist!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg37.2.10">»Da ist nichts außerhalb!« sagte der Schneckenvater. »Besser als bei uns kann es nirgends sein, und ich habe nichts zu wünschen!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg37.2.11">»Ja,« sagte die Schneckenmutter, »ich möchte wohl nach dem Herrenhof kommen, gekocht und auf eine silberne Schüssel gelegt werden, das ist allen unsern Vorfahren widerfahren, und glaube mir, es ist ganz etwas Besonderes dabei!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg37.2.12">»Der Herrenhof ist vielleicht zusammengestürzt,« sagte der Schneckenvater, »oder der Klettenwald ist darüber hinweg gewachsen, sodaß die Menschen nicht herauskommen können. Übrigens hat das keine Eile, Du eilst immer gewaltig und der Kleine fängt auch schon damit an; er ist nun in drei Tagen an dem Stiel hinaufgekrochen, mir wird schwindlig, wenn ich zu ihm hinaufsehe!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg37.2.13">»Du mußt nicht schelten!« sagte die Schneckenmutter. »Er kriegt so besonnen; wir werden noch Freude an ihm erleben, und wir Alten haben ja nichts anderes, wofür wir leben können! Hast Du aber wohl daran gedacht, wo wir eine Frau für ihn hernehmen? Glaubst Du nicht, daß da weit hinein in den Klettenwald noch Jemand von unserer Art sein möchte?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg37.2.14">»Schwarze Schnecken, glaube ich, werden wohl da sein,« sagte der Alte; »schwarze Schnecken ohne Haus, aber das ist gemein, und doch sind sie stolz. Aber wir könnten die Ameisen damit beauftragen, die laufen hin und her, als ob sie etwas zu thun hätten, sie wissen sicher eine Frau für unsern Kleinen.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg37.2.15">»Ich weiß freilich die allerschönste,« sagte eine der Ameisen, »aber ich fürchte, es geht nicht, denn sie ist eine Königin!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg37.2.16">
                        <pb n="252" xml:id="tg37.2.16.1"/>
»Das schadet nichts!« sagten die Alten. »Hat sie ein Haus?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg37.2.17">»Sie hat ein Schloß,« sagte die Ameise, »das schönste Ameisenschloß mit siebenhundert Gängen.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg37.2.18">»Schönen Dank!« sagte die Schneckenmutter. »Unser Sohn soll nicht in einen Ameisenhaufen! Wißt Ihr nichts besseres, so geben wir den Auftrag den weißen Mücken, die fliegen bei Regen und Sonnenschein weit umher und kennen den Klettenwald von innen und außen.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg37.2.19">»Wir haben eine Frau für ihn!« sagten die Mücken. »Hundert Menschenschritte von hier sitzt auf einem Stachelbeerstrauch eine kleine Schnecke mit einem Hause, sie ist ganz allein, und alt genug, sich zu verheiraten. Es sind nur hundert Menschenschritte!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg37.2.20">»Ja, laßt sie zu ihm kommen,« sagten die Alten, »er hat einen Klettenwald, sie hat nur einen Strauch!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg37.2.21">Sie holten das kleine Schneckenfräulein. Es währte acht Tage, ehe sie eintraf, aber das war gerade das Vornehme dabei, daran konnte man sehen, daß sie von der rechten Art war.</p>
                    <p rend="zenoPLm4n0" xml:id="tg37.2.22">Dann hielten sie Hochzeit. Sechs Johanniswürmer leuchteten so gut sie konnten; übrigens ging es im ganzen still zu, denn die alten Schnecken konnten Schwärmen und Lustbarkeiten nicht ertragen. Aber eine schöne Rede wurde von der Schneckenmutter gehalten; der Vater konnte nicht reden, er war zu bewegt, und dann gaben sie ihnen den ganzen Klettenwald zur Erbschaft und sagten, was sie immer gesagt hatten, daß es das beste in der Welt sei und wenn sie redlich und ordentlich lebten und sich vermehrten, dann würden sie und ihre Kinder einst nach dem Herrenhofe kommen, schwarz gekocht und auf eine silberne Schüssel gelegt werden.</p>
                    <p rend="zenoPLm4n0" xml:id="tg37.2.23">Nachdem die Rede gehalten war, krochen die Alten in ihre Häuser und kamen nie wieder heraus; sie schliefen. Das junge Schneckenpaar regierte im Walde und erhielt eine große Nachkommenschaft, aber sie wurden nie gekocht und sie kamen nie auf eine silberne Schüssel, woraus sie den Schluß zogen, daß <pb n="253" xml:id="tg37.2.23.1"/>
der Herrenhof zusammengestürzt sei und daß alle Menschen in der Welt ausgestorben seien, und da ihnen niemand widersprach, so mußte es ja wahr sein. Der Regen schlug auf die Klettenblätter, um für sie eine Trommelmusik zu veranstalten, und die Sonne schien, um den Klettenwald für sie zu beleuchten, und sie waren sehr glücklich und die ganze Familie war glücklich.</p>
                </div>
            </div>
        </body>
    </text>
</TEI>