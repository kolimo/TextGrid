<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg783" n="/Literatur/M/Busch, Wilhelm/Autobiographisches/Von mir über mich">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Von mir über mich</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Busch, Wilhelm: Element 00666 [2011/07/11 at 20:23:55]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <notesStmt>
                <note>
                           Dritte Fassung entstanden 1894. Erstdruck der zweiten Fassung in: »Die Fromme Helene«, Jubiläumsausgabe (100. Tausend), München (Bassermann) 1893.
                        </note>
            </notesStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Wilhelm Busch: Werke. Historisch-kritische Gesamtausgabe, Bde. I-IV, bearbeitet und Herausgegeben v. Friedrich Bohne, Hamburg: Standard-Verlag, 1959.</title>
                        <author key="pnd:118517880">Busch, Wilhelm</author>
                    </titleStmt>
                    <extent>205-</extent>
                    <publicationStmt>
                        <date when="1959"/>
                        <pubPlace>Hamburg</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date when="1894"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg783.3">
                <head type="h3" xml:id="tg783.3.2">Wilhelm Busch</head>
                <head type="h2" xml:id="tg783.3.3">Von mir über mich</head>
                <div type="h4">
                    <head type="h4" xml:id="tg783.3.4">Letzte Fassung (1894)</head>
                    <pb type="start" n="205" xml:id="tg783.3.6"/>
                    <p xml:id="tg783.3.7">Kein Ding sieht so aus, wie es ist. Am wenigsten der Mensch, dieser lederne Sack voller Kniffe und Pfiffe. Und auch abgesehn von den Kapriolen und Masken der Eitelkeit. Immer, wenn man was wissen will, muß man sich auf die zweifelhafte Dienerschaft des Kopfes und der Köpfe verlassen und erfährt nie recht, was passiert ist. Wer ist heutigen Tages noch so harmlos, daß er Weltgeschichten und Biographien für richtig hält? Sie gleichen den Sagen und Anekdoten, die Namen, Zeit und Ort benennen, um sich glaubhaft zu machen. Sind sie unterhaltlich erzählt, sind sie ermunternd und lehrreich, oder rührend und erbaulich, nun gut! so wollen wir's gelten lassen. Ist man aber nicht grad ein Professor der Beredsamkeit und sonst noch allerlei, was der Heilige Augustinus gewesen, und will doch partout über sich selbst was schreiben, dann wird man wohl am besten tun, man faßt sich kurz. Und so auch ich.</p>
                    <p rend="zenoPLm4n0" xml:id="tg783.3.8">Ich bin geboren 1832 in Wiedensahl. Mein Vater war Krämer, heiter und arbeitsfroh; meine Mutter, still und fromm, schaffte fleißig in Haus und Garten. Liebe und Strenge sowohl, die mir von ihnen zuteil geworden, hat der »Schlafittig« der Zeit aus meiner dankbaren Erinnerung nicht zu verwischen vermocht.</p>
                    <p rend="zenoPLm4n0" xml:id="tg783.3.9">Mein gutes Großmütterlein war zuerst wach in der Früh. Sie schlug Funken am P-förmigen Stahl, bis einer zündend ins »Usel« sprang, in die halbverkohlte Leinwand im Deckelkästchen des Feuerzeugs, und bald flackerte es lustig in der Küche auf dem offenen Herde unter dem Dreifuß und dem kupfernen Kessel; und nicht lange, so hatte auch das Kanonenöfchen in der Stube ein rotglühendes Bäuchlein, worin's bullerte. Als ich sieben, acht Jahr alt war, durft ich zuweilen mit aufstehn; und im Winter besonders kam es mir wonnig geheimnisvoll vor, so früh am Tag schon selbstbewußt in dieser Welt zu sein, wenn ringsumher noch alles still und tot und dunkel war. Dann saßen wir zwei, bis das Wasser kochte, im engen Lichtbezirk der pompejanisch geformten zinnernen Lampe. Sie spann. Ich las ein paar schöne Morgenlieder aus dem Gesangbuch vor.</p>
                    <p rend="zenoPLm4n0" xml:id="tg783.3.10">Später beim Kaffee nahmen Herrschaft, Knecht und Mägde, wie es guten Freunden geziemt, am nämlichen Tische Platz.</p>
                    <p rend="zenoPLm4n0" xml:id="tg783.3.11">
                        <pb n="205" xml:id="tg783.3.11.1"/>
Um diese Zeit meines Lebens passierte eine kleine Geschichte, die recht schmerzhaft und schimpflich für mich ablief. Beim Küster diente ein Kuhjunge, fünf, sechs Jahre älter als ich. Er hatte in einen rostigen Kirchenschlüssel, so groß wie dem Petrus seiner, ein Zündloch gefeilt, und gehacktes Fensterblei hatte er auch schon genug; bloß das Pulver fehlte ihm noch zu Blitz und Donner. Infolge seiner Beredsamkeit machte ich einen stillen Besuch bei einer gewissen steinernen Kruke, die auf dem Speicher stand. Nachmittags zogen wir mit den Kühen auf die einsame Waldwiese. Großartig war der Widerhall des Geschützes. Und so beiläufig ging auch ein altes Bäuerlein vorbei, in der Richtung des Dorfes. – Abends kehrt ich fröhlich heim und freute mich so recht auf das Nachtessen. Mein Vater empfing mich an der Tür und lud mich ein, ihm auf den Speicher zu folgen. Hier ergriff er mich am linken Flügel und trieb mich vermittels eines Rohrstockes im Kreise umher, immer um die Kruke herum, wo das Pulver drin war. Wie peinlich mir das war, ließ ich weithin verlautbaren. Und sonderbar! Ich bin weder Jäger noch Soldat geworden.</p>
                    <p rend="zenoPLm4n0" xml:id="tg783.3.12">Als ich neun Jahr alt war, sollte ich zu dem Bruder meiner Mutter nach Ebergötzen. Wie Kinder sind, halb froh, halb wehmütig, plätscherte ich am Abend vor der Abreise mit der Hand in der Regentonne, über die ein Strauch von weißen Rosen hing, und sang Christine! Christine! versimpelt für mich hin.</p>
                    <p rend="zenoPLm4n0" xml:id="tg783.3.13">Und so spannte denn in der Früh vor Tag Knecht Heinrich das Pommerchen vor den Leiterwagen. Die ganze Familie, ausgenommen der Vater, stieg auf, um dem guten Jungen das Geleite zu geben. Hell schienen die Sterne, als wir durch den Schaumburger Wald fuhren. Hirsche sprangen über den Weg.</p>
                    <p rend="zenoPLm4n0" xml:id="tg783.3.14">In Wirtshäusern einkehren taten wir nicht; ein wenig seitwärts von der Straße wurde stillgehalten, der Deckel der Ernährungskiepe wurde aufgetan und unter anderm ein ganzer geräucherter Schinken entblößt, der sich bald merklich verminderte. Nach mehrmaligem Übernachten bei Verwandten erreichten wir glücklich das Ebergötzener Pfarrhaus.</p>
                    <p rend="zenoPLm4n0" xml:id="tg783.3.15">Gleich am Tage nach der Ankunft schloß ich Freundschaft mit dem Sohne des Müllers. Wir gingen vors Dorf hinaus, um zu baden. Wir machten eine Mudde aus Erde und Wasser, die wir »Peter und Paul« benannten, überkleisterten uns damit von oben bis unten, legten uns in die Sonne, bis wir inkrustiert waren wie Pasteten, und spülten's im Bach wieder ab.</p>
                    <p rend="zenoPLm4n0" xml:id="tg783.3.16">Auch der Wirt des Ortes, weil er ein Piano besaß, wurde bald mein guter Bekannter. Er war rauh, wie Esau. Ununterbrochen kroch das schwarze Haar in die Krawatte und aus den Ärmeln wieder heraus <pb n="206" xml:id="tg783.3.16.1"/>
bis dicht an die Fingernägel. Beim Rasieren mußte er weinen, denn das Jahr 48, welches selbst den widerspenstigsten Bärten die Freiheit gab, war noch nicht erschienen. Er trug lederne Klappantoffeln und eine gelbgrüne Joppe, die das hintere Mienenspiel der blaßblauen Hose nur selten zu bemänteln suchte. Seine Philosophie war der Optimismus mit rückwirkender Kraft; er sei zu gut für diese Welt, pflegte er gern und oft zu behaupten. Als er einst einem Jagdhunde mutwillig auf die Zehen trat und ich meinte, das stimmte nicht recht mit seiner Behauptung, kriegt ich sofort eine Ohrfeige. Unsere Freundschaft auch. Doch die Erschütterung währte nicht lange. Er ist mir immer ein lieber und drolliger Mensch geblieben. Er war ein geschmackvoller Blumenzüchter, ein starker Schnupfer und hat sich dreimal vermählt. Bei ihm fand ich einen dicken Notenband, der durchgeklimpert, und freireligiöse Schriften jener Zeit, die begierig verschlungen wurden. Mein Freund aus der Mühle, der meine gelehrten Unterrichtsstunden teilte, teilte auch meine Studien in freier Natur. Dohnen und Sprenkeln wurden eifrig verfertigt, und der Schlupfwinkel keiner Forelle, den ganzen Bach entlang, unter Steinen und Baumwurzeln, blieb unbemerkt von uns.</p>
                    <p rend="zenoPLm4n0" xml:id="tg783.3.17">Zwischen all dem herum aber schwebte beständig das anmutige Bildnis eines blondlockigen Kindes. Natürlich sehnte ich oft die bekannte Feuersbrunst herbei mit nachfolgendem Tode zu den Füßen der geretteten Geliebten. Meist jedoch war ich nicht so rücksichtslos gegen mich selbst, sondern begnügte mich mit dem Wunsch, daß ich zauberhaft fliegen und hupfen könnte, hoch in der Luft, von einem Baum zum andern, und daß sie es mit ansähe und wäre starr vor Bewunderung.</p>
                    <p rend="zenoPLm4n0" xml:id="tg783.3.18">Von meinem Onkel, der äußerst milde war, erhielt ich nur ein einzigmal Hiebe, mit einem trocknen Georginenstengel, weil ich den Dorftrottel geneckt hatte. Dem war die Pfeife voll Kuhhaare gestopft und dienstbeflissen angezündet. Er rauchte sie aus, bis aufs letzte Härchen, mit dem Ausdruck der seligsten Zufriedenheit. Also der Erfolg war unerwünscht für mich in zwiefacher Hinsicht. Es macht nichts. Ein Trottel bleibt immer eine schmeichelhafte Erinnerung.</p>
                    <p rend="zenoPLm4n0" xml:id="tg783.3.19">Gern gedenk ich auch des kleinen alten Bettelvogts, welcher derzeit dat baddelspeit trug, den kurzen Spieß, als Zeichen seines mächtigen Amtes. Zu warmer Sommerzeit hielt er sein Mittagschläfchen im Grase. Er konnte bemerkenswert schnarchen. Zog er die Luft ein, so machte er den Mund weit auf, und es ging: Krah! Stieß er sie aus, so machte er den Mund ganz spitz, und es ging: Püh! wie ein sanfter Flötenton. Einst fanden wir ihn tot unter dem berühmtesten Birnbäume des Dorfes; Speer im Arm; Mund offen; so daß man sah: Krah! war <pb n="207" xml:id="tg783.3.19.1"/>
sein letzter Laut gewesen. Um ihn her lagen die goldigsten Sommerbirnen; aber für diesmal mochten wir keine.</p>
                    <p rend="zenoPLm4n0" xml:id="tg783.3.20">Etwa ums Jahr 45 bezogen wir die Pfarre zu Lüthorst.</p>
                    <p rend="zenoPLm4n0" xml:id="tg783.3.21">Unter meinem Fenster murmelte der Bach. Gegenüber am Ufer stand ein Haus, eine Schaubühne des ehelichen Zwistes. Das Stück fing an hinter der Szene, spielte weiter auf dem Flur und schloß im Freien. Sie stand oben vor der Tür und schwang triumphierend den Reiserbesen, er stand unten im Bach und steckte die Zunge heraus; und so hatte er auch seinen Triumph.</p>
                    <p rend="zenoPLm4n0" xml:id="tg783.3.22">In den Stundenplan schlich sich nun auch die Metrik ein. Dichter, heimische und fremde, wurden gelesen. Zugleich fiel mir die »Kritik der reinen Vernunft« in die Hände, die, wenn auch damals nur spärlich durchschaut, doch eine Neigung erweckte, in der Gehirnkammer Mäuse zu fangen, wo es nur gar zu viel Schlupflöcher gibt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg783.3.23">Sechzehn Jahre alt, ausgerüstet mit einem Sonett und einer ungefähren Kenntnis der vier Grundrechnungsarten, erhielt ich Einlaß zur Polytechnischen Schule in Hannover.</p>
                    <p rend="zenoPLm4n0" xml:id="tg783.3.24">Hier ging mit meinem Äußern eine stolze Veränderung vor. Ich kriegte die erste Uhr – alt, nach dem Kartoffelsystem – und den ersten Paletot – neu, so schön ihn der Dorfschneider zu bauen vermochte. Mit diesem Paletot, um ihn recht sehen zu lassen, stellt ich mich gleich den ersten Morgen sehr dicht vor den Schulofen. Eine brenzlichte Wolke und die freudige Teilnahme der Mitschüler ließen mich ahnen, was hinten vor sich ging. Der umfangreiche Schaden wurde kuriert nach der Schnirrmethode, beschämend zu sehn; und nur noch bei äußerster Witterungsnot ließ sich das einst so prächtige Kleidungsstück auf offener Straße blicken.</p>
                    <p rend="zenoPLm4n0" xml:id="tg783.3.25">In der reinen Mathematik schwang ich mich bis zu »Eins mit Auszeichnung« empor, aber in der angewandten bewegt ich mich mit immer matterem Flügelschlage.</p>
                    <p rend="zenoPLm4n0" xml:id="tg783.3.26">Ein Maler wies mir den Weg nach Düsseldorf. Ich kam, soviel ich weiß, grad an zu einem jener Frühlingsfeste, für diesmal die Erstürmung einer Burg, die weithin berühmt waren. Ich war sehr begeistert davon und von dem Maiwein auch.</p>
                    <p rend="zenoPLm4n0" xml:id="tg783.3.27">Nachdem ich mich schlecht und recht durch den Antikensaal hindurchgetüpfelt hatte, begab ich mich nach Antwerpen in die Malschule, wo man, so hieß es, die alte Muttersprache der Kunst noch immer erlernen könne.</p>
                    <p rend="zenoPLm4n0" xml:id="tg783.3.28">In dieser kunstberühmten Stadt sah ich zum ersten Male die Werke alter Meister: Rubens, Brouwer, Teniers, Frans Hals. Ihre göttliche Leichtigkeit der Darstellung malerischer Einfälle, verbunden mit stofflich juwelenhaftem Reiz; diese Unbefangenheit eines guten Gewissens, <pb n="208" xml:id="tg783.3.28.1"/>
welches nichts zu vertuschen braucht; diese Farbenmusik, worin man alle Stimmen klar durchhört, vom Grundbaß herauf; haben für immer meine Liebe und Bewunderung gewonnen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg783.3.29">Ich wohnte am Eck der Käsbrücke bei einem Bartscherer. Er hieß Jan, seine Frau hieß Mie. In gelinder Abendstunde saß ich mit ihnen vor der Haustür; im grünen Schlafrock; die Tonpfeife im Munde; und die Nachbarn kamen auch herzu; die Töchter in schwarzlackierten Holzschuhen. Jan und Mie balbierten mich abwechselnd, verpflegten mich während einer Krankheit und schenkten mir beim Abschied in kalter Jahreszeit eine warme rote Jacke und drei Orangen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg783.3.30">Nach Antwerpen hielt ich mich in der Heimat auf.</p>
                    <p rend="zenoPLm4n0" xml:id="tg783.3.31">Was damals die Leute ut oler welt erzählten, sucht ich mir fleißig zu merken, doch wußt ich leider zuwenig, um zu wissen, was wissenschaftlich bemerkenswert war. Das Vorspuken eines demnächstigen Feuers hieß: wabern. Den Wirbelwind, der auf der Landstraße den Staub auftrichtert, nannte man: warwind; es sitzt eine Hexe drin. Übrigens hörte ich, seit der »Alte Fritz« das Hexen verboten hätte, müßten sich die Hexen überhaupt sehr in acht nehmen mit ihrer Kunst.</p>
                    <p rend="zenoPLm4n0" xml:id="tg783.3.32">Von Märchen wußte das meiste ein alter, stiller, für gewöhnlich wortkarger Mann. Für Spukgeschichten dagegen, von bösen Toten, die wiederkommen zum Verdrusse der Lebendigen, war der Schäfer Autorität. Wenn er abends erzählte, lag er quer über dem Bett, und wenn's ihm trocken und öd wurde im Mund, sprang er auf und ging vor den Tischkasten und biß ein neues Endchen Kautabak ab zur Erfrischung. Sein Frauchen saß daneben und spann.</p>
                    <p rend="zenoPLm4n0" xml:id="tg783.3.33">In den Spinnstuben sangen die Mädchen, was ihre Mütter und Großmütter gesungen. Während der Pause, abends um neun, wurde getanzt; auf der weiten Haustenne; unter der Stallaterne; nach dem Liede:</p>
                    <lb xml:id="tg783.3.34"/>
                    <lg>
                        <l xml:id="tg783.3.35">maren will wi hawern meihn,</l>
                        <l xml:id="tg783.3.36">wer schall den wol binnen?</l>
                        <l xml:id="tg783.3.37">dat schall meiers dortchen don,</l>
                        <l xml:id="tg783.3.38">de will eck wol finnen.</l>
                    </lg>
                    <lb xml:id="tg783.3.39"/>
                    <p xml:id="tg783.3.40">Von Wiedensahl aus besucht ich auf längere Zeit den Onkel in Lüthorst. Es hatte sich grad um einen Grundsatz der Wissenschaft, nämlich, daß nur aus einem befruchteten Ei ein lebendes Wesen entstehen könne, ein Streit erhoben. Ein schlichter katholischer Pfarrer wies nach, daß die Bienen eine Ausnahme machten. Mein Onkel, als gewandter Schriftsteller und guter Beobachter, ergriff seine Partei und beteiligte sich lebhaft an dem Kampf. Auch mich zog es unwiderstehlich abseits in das Reich der Naturwissenschaften. Ich las Darwin, <pb n="209" xml:id="tg783.3.40.1"/>
ich las Schopenhauer damals mit Leidenschaft. Doch so was läßt nach mit der Zeit. Ihre Schlüssel passen ja zu vielen Türen in dem verwunschenen Schloß dieser Welt; aber kein »hiesiger« Schlüssel, so scheint's, und wär's der Asketenschlüssel, paßt je zur Ausgangstür.</p>
                    <p rend="zenoPLm4n0" xml:id="tg783.3.41">Von Lüthorst ging ich nach München. Indes in der damaligen akademischen Strömung kam mein flämisches Schifflein, das wohl auch schlecht gesteuert war, nicht recht zum Schwimmen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg783.3.42">Um so angenehmer war es im Künstlerverein, wo man sang und trank und sich nebenbei karikierend zu necken pflegte. Auch ich war solchen persönlichen Späßen nicht abgeneigt. Man ist ein Mensch und erfrischt und erbaut sich gern an den kleinen Verdrießlichkeiten und Dummheiten anderer Leute. Selbst über sich selber kann man lachen mitunter, und das ist ein Extrapläsier, denn dann kommt man sich sogar noch klüger und gedockener vor als man selbst.</p>
                    <p rend="zenoPLm4n0" xml:id="tg783.3.43">Lachen ist ein Ausdruck relativer Behaglichkeit. Der Franzel hinterm Ofen freut sich der Wärme um so mehr, wenn er sieht, wie sich draußen der Hansel in die rötlichen Hände pustet. Zum Gebrauch in der Öffentlichkeit habe ich jedoch nur Phantasiehanseln genommen. Man kann sie auch besser herrichten nach Bedarf und sie eher tun und sagen lassen, was man will. Gut schien mir oft der Trochäus für biederes Reden; stets praktisch der Holzschnittstrich für stilvoll heitre Gestalten. So ein Konturwesen macht sich leicht frei von dem Gesetze der Schwere und kann, besonders wenn es nicht schön ist, viel aushalten, eh' es uns weh tut. Man sieht die Sach an und schwebt derweil in behaglichem Selbstgefühl über den Leiden der Welt, ja über dem Künstler, der gar so naiv ist.</p>
                    <p rend="zenoPLm4n0" xml:id="tg783.3.44">Auch das Gebirg, das noch nie in der Nähe gesehene, wurde für längere Zeit aufgesucht. An einem Spätnachmittag kam ich zu Fuß vor dem Dörfchen an, wo ich zu bleiben gedachte. Gleich das erste Häuschen mit dem Plätscherbrunnen und dem Zaun von Kürbis durchflochten sah verlockend idyllisch aus. Feldstuhl und Skizzenbuch wurden aufgeklappt. Auf der Schwelle saß ein steinaltes Mütterlein und schlief, das Kätzchen daneben. Plötzlich, aus dem Hintergrunde des Hauses, kam eine jüngere Frau, faßte die Alte bei den Haaren und schleifte sie auf den Kehrichthaufen. Dabei quäkte die Alte, wie ein Huhn, das geschlachtet werden soll. Feldstuhl und Skizzenbuch wurden zugeklappt. Mit diesem Rippenstoße führte mich das neckische Schicksal zu den trefflichen Bauersleuten und in die herrliche Gegend, von denen ich nur ungern wieder Abschied nahm.</p>
                    <p rend="zenoPLm4n0" xml:id="tg783.3.45">Es kann 59 gewesen sein, als zuerst in den »Fliegenden« eine Zeichnung mit Text von mir gedruckt wurde: zwei Männer, die aufs Eis gehn, wobei einer den Kopf verliert. Vielfach, wie's die Not gebot, <pb n="210" xml:id="tg783.3.45.1"/>
illustrierte ich dann neben eigenen auch fremde Texte. Bald aber meint ich, ich müßt alles halt selber machen. Die Situationen gerieten in Fluß und gruppierten sich zu kleinen Bildergeschichten, denen größere gefolgt sind. Fast alle hab ich, ohne wem was zu sagen, in Wiedensahl verfertigt. Dann hab ich sie laufen lassen auf den Markt, und da sind sie herumgesprungen, wie Buben tun, ohne viel Rücksicht zu nehmen auf gar zu empfindsame Hühneraugen; wohingegen man aber auch wohl annehmen darf, daß sie nicht gar zu empfindlich sind, wenn sie mal Schelte kriegen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg783.3.46">Man hat den Autor für einen Bücherwurm und Absonderling gehalten. Das erste mit Unrecht.</p>
                    <p rend="zenoPLm4n0" xml:id="tg783.3.47">Zwar liest er unter anderm die Bibel, die großen Dramatiker, die Bekenntnisse des Augustin, den Pickwick und Don Quichotte und hält die Odyssee für das schönste der Märchenbücher, aber ein Bücherwurm ist doch ein Tierchen mit ganz anderen Manierchen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg783.3.48">Ein Sonderling dürft er schon eher sein. Für die Gesellschaft, außer der unter vier bis sechs Augen, schwärmt er nicht sehr.</p>
                    <p rend="zenoPLm4n0" xml:id="tg783.3.49">Groß war auch seine Nachlässigkeit, oder Schüchternheit, im schriftlichen Verkehr mit Fremden. Der gewandte Stilist, der seine Korrespondenten mit einem zierlichen Strohgeflechte beschenkt, macht sich umgehend beliebt, während der Unbeholfene, der seine Halme aneinanderknotet, wie der Bauer, wenn er Seile bindet, mit Recht befürchten muß, daß er Anstoß erregt. Er zögert und vergißt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg783.3.50">Verheiratet ist er auch nicht. Er denkt gelegentlich eine Steuer zu beantragen auf alle Ehemänner, die nicht nachweisen können, daß sie sich lediglich im Hinblick auf das Wohl des Vaterlandes vermählt haben. Wer eine hübsche und gescheite Frau hat, die ihre Dienstboten gut behandelt, zahlt das Doppelte. Den Ertrag kriegen die alten Junggesellen, damit sie doch auch eine Freud haben.</p>
                    <p rend="zenoPLm4n0" xml:id="tg783.3.51">Ich komme zum Schluß. Das Porträt, um rund zu erscheinen, hätte mehr Reflexe gebraucht. Doch manche vorzügliche Menschen, die ich liebe und verehre, für Selbstbeleuchtungszwecke zu verwenden, wollte mir nicht passend erscheinen, und in bezug auf andere, die mir weniger sympathisch gewesen, halte ich ohnehin schon längst ein mildes, gemütliches Schweigen für gut.</p>
                    <p rend="zenoPLm4n0" xml:id="tg783.3.52">So stehe ich denn tief unten an der Schattenseite des Berges. Aber ich bin nicht grämlich geworden; sondern wohlgemut, halb schmunzelnd, halb gerührt, höre ich das fröhliche Lachen von anderseits her, wo die Jugend im Sonnenschein nachrückt und hoffnungsfreudig nach oben strebt.</p>
                    <pb n="211" xml:id="tg783.3.53"/>
                </div>
            </div>
        </body>
    </text>
</TEI>