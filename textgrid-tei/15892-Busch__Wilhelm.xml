<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg819" n="/Literatur/M/Busch, Wilhelm/Briefe/35. An Otto Bassermann">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>35. An Otto Bassermann</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Busch, Wilhelm: Element 00702 [2011/07/11 at 20:23:55]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Busch, Wilhelm: Sämtliche Briefe. Kommentierte Ausgabe in zwei Bänden, Band I: Briefe 1841 bis 1892, Band II: Briefe 1893 bis 1908, hg. v. Friedrich Bohne, Hannover: Wilhelm-Busch-Gesellschaft, 1968 (Bd. I), 1969 (Bd. II).</title>
                        <author key="pnd:118517880">Busch, Wilhelm</author>
                    </titleStmt>
                    <extent>30-</extent>
                    <publicationStmt>
                        <date notBefore="1841" notAfter="1968"/>
                        <pubPlace>Hannover</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1832" notAfter="1908"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg819.2">
                <div type="h4">
                    <head type="h4" xml:id="tg819.2.1">35. An Otto Bassermann</head>
                    <milestone unit="head_start"/>
                    <p rend="zenoPC" xml:id="tg819.2.2">
                        <pb n="30" xml:id="tg819.2.2.1"/> 35. An Otto Bassermann</p>
                    <milestone unit="head_end"/>
                    <lb xml:id="tg819.2.3"/>
                    <milestone unit="head_start"/>
                    <p rend="zenoPR" xml:id="tg819.2.4">
                        <seg rend="zenoTXFontsize80" xml:id="tg819.2.4.1">
                            <hi rend="italic" xml:id="tg819.2.4.1.1">Wiedensahl 19/11 64.</hi>
                        </seg>
                    </p>
                    <milestone unit="head_end"/>
                    <lb xml:id="tg819.2.5"/>
                    <p xml:id="tg819.2.6">Mein lieber Otto!</p>
                    <p rend="zenoPLm4n0" xml:id="tg819.2.7">Viel Dank für deinen Brief, wenn er auch etwas länger, als ich dachte, auf sich warten ließ. Ich stutzte über die Aufschrift »München« und machte mir schon allerlei Gedanken, bis ich denn im Verlaufe des Lesens doch sah, daß du bereits in der Heimath angekommen. Wenn nun auch das graue Nachtgevögel der Sorge hin und wieder seine Flügel über deinem Haupte schlägt, so wirst du doch anderseits nicht vergeßen, welch eine freundliche Wohlthat es ist, in der Nähe der Deinigen zu sein, deren natürliches Wohlwollen durch nichts in der Welt zu ersetzen ist. Das Leben in München, wenigstens das der letzten Zeit, gewann ja doch ein unerquickliches Ansehn; die Macht der trägen Gewohnheit gab ihm eine Art von Bierphysiognomie, welche auf die Dauer nichts Gesundes erwarten ließ. Glaube nicht, daß ich all das Angenehme verkenne und vergeßen habe, was mir dort wiederfahren; stets werde ich mich jener hellen Tage erinnern, an denen du ja meistens an meiner Seite gingst. Wie viel Freundliches ist mir auch von Lossow und Hanf wiederfahren, und von dem guten Seppel, an deßen Bilde nun schon seit anderthalb Jahren die Zeit vorübergeht. – Seit ich von München fort bin, habe ich Nichts von dort erfahren, außer dem, was ich aus deinem Briefe weiß. Ich weiß, daß es meine Schuld ist, sonst hätte ich ja wohl durch <hi rend="italic" xml:id="tg819.2.7.1">Wuzel</hi> Nachricht erhalten können; der Fehler soll aber in diesen Tagen wieder gut gemacht werden. – Von Hanf hätte ich wohl einen Brief erwartet, da ich ihm zuletzt geschrieben; er scheint aber zu sehr in seine nähern Angelegenheiten vertieft zu sein. Daß er sich nun definitiv und öffentlich verlobt, scheint mir sehr gut, denn dadurch wird auch die Versöhnung mit seinem Vater herbei geführt sein, die vor allen Dingen zu wünschen war. Mir wird aus deinem Briefe nicht ganz klar, ob er nach Paris zurückgekehrt; doch will ich ihm dahin bald einmal schreiben. – Was du mir über die Hochzeit deiner Bonzencousine geschrieben, hat mich recht unterhalten; aber wen hat sie denn eigentlich geheirathet? Ich meine, daß du mir einmal von einer bevorstehenden Verlobung zwischen Gombart und der Rosa gesagt. Ist es dieses Paar, welches somit an das Ziel seiner Wünsche gelangt ist? –</p>
                    <p rend="zenoPLm4n0" xml:id="tg819.2.8">Die Nachricht über die Auflösung des Künstlervereins <hi rend="italic" xml:id="tg819.2.8.1">Jung=München</hi> hat mich wenig überrascht und ist ohne jede Spur von Trauer an mir vorbei gegangen. Wir wußten ja schon längst, daß es so kommen mußte. Die Namen derjenigen, die den neuen Verein gebildet, bürgen mir für die Übrigen; und so wollen wir denn den Titel eines Mitglieds des Künstlervereins <hi rend="italic" xml:id="tg819.2.8.2">Jung=München</hi> unter unsern übrigen Titeln künftighin ganz kaltblütig weglaßen; ein Ehrentitel war es ja doch nicht mehr.</p>
                    <p rend="zenoPLm4n0" xml:id="tg819.2.9">An Richter in Dresden habe ich neulich wieder ein kleines Buch geschickt, das er auch in Verlag nehmen will. Er fragte auch vor einiger Zeit bei mir an, ob du etwas von mir zu Weihnachten herausgäbest, wie er das vom Böblinger Kaiser gehört habe. – Die Ausstattung der Bilderpoßen ist sehr elegant und solid. –</p>
                    <p rend="zenoPLm4n0" xml:id="tg819.2.10">Etwas Neues, das ich in Arbeit habe, soll demnächst fertig werden. Ich habe hier Zeit und Muße dazu. Ende September habe ich eine hübsche Weserfahrt gemacht, vom schönsten Wetter begünstigt. Die Klippen und waldigen Hügel des Hohnstein und der Porta westphalica, so wie die alte Rattenfängerstadt Hameln sind mir auf's Neue in freundliche Erinnrung getreten. – Wohin ich im Laufe des Winters gehen werde, weiß ich noch nicht zu sagen. Im folgenden Sommer hoffe ich dich aber einmal in <hi rend="italic" xml:id="tg819.2.10.1">Mannheim</hi> aufzusuchen. – Was soll ich dir nun aber über mein Leben hier mittheilen? Hier paßirt nichts Neues. Der Winter ist wieder einen Schritt zurückgetreten; der Boden in Dorf und Feld ist vom Regen erweicht; die letzten Feuer der Kuhhirten von den Wiesen in der Ebene sind verglommen; nur hier und da knarrt noch ein Pflug und die nächste Neuigkeit wird der Schnee sein, der sich über die bereits emporkeimenden Wintersaaten ausbreitet. Nur in der Dämmerung gehe ich aus zum Pfarrhause und erzähle meinem kleinen Neffen allerlei Märchen und Geschichten; er sitzt auf meinen Knieen und wird nicht müde und sagt immer wieder: »Onkel <pb n="31" xml:id="tg819.2.10.2"/> Wilhelm, nun noch ein mal die Geschichte von der Gans Adelheit und dem bösen Fuchse!« – – Kein Tropfen Bier geht über meine Lippen und kein böses Wort über meine Zunge. Friede sei mit dir!</p>
                    <p rend="zenoPLm4n0" xml:id="tg819.2.11">Über dein Kranksein hätte ich genaueres hören mögen. War es ernstlich?</p>
                    <p rend="zenoPLm4n0" xml:id="tg819.2.12">Nun leb wohl, lieber Freund! Behalt mich in gutem Andenken und schreib recht recht bald einmal wieder an</p>
                    <p rend="zenoPLm12n0" xml:id="tg819.2.13">deinen stets getr. Freund</p>
                    <milestone unit="hi_start"/>
                    <p rend="zenoPR" xml:id="tg819.2.14">Wilh. Busch</p>
                    <milestone unit="hi_end"/>
                    <lb xml:id="tg819.2.15"/>
                    <lg>
                        <l xml:id="tg819.2.16">Besten Gruß an <hi rend="italic" xml:id="tg819.2.16.1">Zeroni</hi> und den Vetter Wilhelm.</l>
                    </lg>
                    <lg>
                        <lb xml:id="tg819.2.17"/>
                        <l xml:id="tg819.2.18">Dein Relief erhielt ich.</l>
                    </lg>
                </div>
            </div>
        </body>
    </text>
</TEI>