<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg1735" n="/Literatur/M/Busch, Wilhelm/Briefe/910. An Nanda Keßler">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>910. An Nanda Keßler</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Busch, Wilhelm: Element 01618 [2011/07/11 at 20:23:55]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Busch, Wilhelm: Sämtliche Briefe. Kommentierte Ausgabe in zwei Bänden, Band I: Briefe 1841 bis 1892, Band II: Briefe 1893 bis 1908, hg. v. Friedrich Bohne, Hannover: Wilhelm-Busch-Gesellschaft, 1968 (Bd. I), 1969 (Bd. II).</title>
                        <author key="pnd:118517880">Busch, Wilhelm</author>
                    </titleStmt>
                    <extent>7-</extent>
                    <publicationStmt>
                        <date notBefore="1893" notAfter="1969"/>
                        <pubPlace>Hannover</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1832" notAfter="1908"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg1735.2">
                <div type="h4">
                    <head type="h4" xml:id="tg1735.2.1">910. An Nanda Keßler</head>
                    <milestone unit="head_start"/>
                    <p rend="zenoPC" xml:id="tg1735.2.2">910. An Nanda Keßler</p>
                    <milestone unit="head_end"/>
                    <lb xml:id="tg1735.2.3"/>
                    <milestone unit="head_start"/>
                    <p rend="zenoPR" xml:id="tg1735.2.4">
                        <seg rend="zenoTXFontsize80" xml:id="tg1735.2.4.1">
                            <hi rend="italic" xml:id="tg1735.2.4.1.1">Wiedensahl 27. März 93.</hi>
                        </seg>
                    </p>
                    <milestone unit="head_end"/>
                    <lb xml:id="tg1735.2.5"/>
                    <p xml:id="tg1735.2.6">Liebe Nanda!</p>
                    <p rend="zenoPLm4n0" xml:id="tg1735.2.7">Zur Zeit des Winters ist mancher Zug stecken geblieben. So auch, bei Disparität der Jahre, wie der Probleme, ist's nicht verwunderlich, wenn sich im schriftlichen Verkehr mal eine gewiße Stockung bemerkbar macht. Dem, der noch abenteuerlustig in's Leben hineindrängt, der Zickzacklinien in den Sand zeichnet, als Vorbild seiner Wünsche, wird Das, was alten Leuten paßirt, die nicht grad als Meckerböcke, sondern möglichst still und anständig und nachdenklich aus der Welt hinaus gehn möchten, naturgemäß recht langweilig vorkommen; eine Wahrheit, welche einst von der liebenswürdigsten Autorität ausdrücklich bestätigt wurde.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1735.2.8">Daher liegt der Fall so, daß der Onkel stets froh sein darf, wenn ihm sein hübsches Tantchen was schreibt, während er selbst, der nichts Rechtes zu melden weiß, nur selten, nur schüchtern zur Feder greift. – Es ist ein Naturphänomen. Man muß es ertragen, wie Regenwetter.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1735.2.9">
                        <pb n="7" xml:id="tg1735.2.9.1"/> Wo ihm recht ist, hat er auch das Wort "beßern" gelesen. Hat es tiefere Bedeutung, dann geht's eigentlich zunächst keinen andern <hi rend="spaced" xml:id="tg1735.2.9.2">Menschen</hi> was an; und bedenkt man, wie schwierig es ist und wie viel man noch mit sich selber zu thun hat, so nimmt man dergleichen Äußerungen, wenn auch nicht ohne Wohlwollen, so doch mit Vorsicht entgegen. Von "verstoßen" (so lautet der Text) kann aber schon deßhalb keine Red sein, weil ein Besitz, den dies voraussetzt, allhier nicht vorhanden ist. –</p>
                    <p rend="zenoPLm4n0" xml:id="tg1735.2.10">Über neueres Französisch kann ich leider keine genauere Auskunft geben. Ich las nur die <hi rend="italic" xml:id="tg1735.2.10.1">pécheurs d'Islande</hi> und <hi rend="italic" xml:id="tg1735.2.10.2">mon frere Yves</hi> von <hi rend="italic" xml:id="tg1735.2.10.3">Loti.</hi> Im ersteren die Meeresstimmung, Luft, Licht, Waßer, und wie die Menschenkinder nur so einen Augenblick aufrauschen und dann wieder spurlos zerfließen im großen pantheistischen Ocean - das kam mir so eindrücklich in seiner Art vor, als hätt ich's noch nie so gelesen. Das zweite dagegen, eine alkoholistische Vererbungsgeschicht, schien mir nur ein lang ausgezupftes Vexirstück nach neuster Mode zu sein.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1735.2.11">- Und also: Der Frühling kommt! Und Jeder natürlich, der Geld und Zeit und ein reizendes Haus und einen demnächst blühenden Garten hat, macht sich auf anitzt mit Sack und Pack per Eisenbahn, um fern in edlen Wirthshäusern ein schöneres Leben zu beginnen. Denn, wie es ungefähr im "Wandrer" heißt:</p>
                    <p rend="zenoPLm24n24" xml:id="tg1735.2.12">Da, wo du nicht bist, da ist Was los!</p>
                    <lg>
                        <l rend="zenoPLm4n0" xml:id="tg1735.2.13">Aber, Spaß beiseit! Es grüßt Dich und Alle recht freundlich und wünscht glückliche Reis'</l>
                    </lg>
                    <lg>
                        <l rend="zenoPLm12n0" xml:id="tg1735.2.14">Dein häuslicher</l>
                    </lg>
                    <closer>
                        <seg type="closer" rend="zenoPR" xml:id="tg1735.2.15">Onkel</seg>
                        <seg type="closer" rend="zenoPR" xml:id="tg1735.2.16">
                            <hi rend="italic" xml:id="tg1735.2.16.1">Wilhelm.</hi>
                        </seg>
                    </closer>
                </div>
            </div>
        </body>
    </text>
</TEI>