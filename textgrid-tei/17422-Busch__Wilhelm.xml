<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg2349" n="/Literatur/M/Busch, Wilhelm/Briefe/1519. An Grete Meyer">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>1519. An Grete Meyer</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Busch, Wilhelm: Element 02232 [2011/07/11 at 20:23:55]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Busch, Wilhelm: Sämtliche Briefe. Kommentierte Ausgabe in zwei Bänden, Band I: Briefe 1841 bis 1892, Band II: Briefe 1893 bis 1908, hg. v. Friedrich Bohne, Hannover: Wilhelm-Busch-Gesellschaft, 1968 (Bd. I), 1969 (Bd. II).</title>
                        <author key="pnd:118517880">Busch, Wilhelm</author>
                    </titleStmt>
                    <extent>242-</extent>
                    <publicationStmt>
                        <date notBefore="1893" notAfter="1969"/>
                        <pubPlace>Hannover</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1832" notAfter="1908"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg2349.2">
                <div type="h4">
                    <head type="h4" xml:id="tg2349.2.1">1519. An Grete Meyer</head>
                    <milestone unit="head_start"/>
                    <p rend="zenoPC" xml:id="tg2349.2.2">1519. An Grete Meyer</p>
                    <milestone unit="head_end"/>
                    <lb xml:id="tg2349.2.3"/>
                    <milestone unit="head_start"/>
                    <p rend="zenoPR" xml:id="tg2349.2.4">
                        <seg rend="zenoTXFontsize80" xml:id="tg2349.2.4.1">Mechtshausen 19. Nov. 1905.</seg>
                    </p>
                    <milestone unit="head_end"/>
                    <lb xml:id="tg2349.2.5"/>
                    <p xml:id="tg2349.2.6">Liebe Grete!</p>
                    <p rend="zenoPLm4n0" xml:id="tg2349.2.7">Da der Schnitt nun geschehn ist, so wünsch ich von ganzem Herzen, daß die Zeit, die alle Wunden kühlt, auch dir viel Freundliches bringen möge, was lindert und heilt. Das meiste freilich muß drinnen verfochten werden, wo allein für uns das Wesentliche und, wenn überhaupt wo, ein Zipfel der Freiheit zu faßen ist. Nur weil wir im Vergleich zur unendlichen Wirksamkeit ein so winziges wirkendes Theilchen sind, pflegt man zu sagen: Es <hi rend="spaced" xml:id="tg2349.2.7.1">mußte</hi> so kommen. Ob "gut", was im Besondern geschieht, lehrt die Erfahrung. Die allgemeine Frage jedoch, ob <hi rend="spaced" xml:id="tg2349.2.7.2">Alles</hi> auf's Beste zielt, muß einzig der Glaube zu lösen versuchen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg2349.2.8">
                        <pb n="242" xml:id="tg2349.2.8.1"/> Wie du, nahm ich kürzlich den Göthe zur Hand. Drin nah beieinander fand ich den Aufsatz über's Straßburger Münster und die letzten Schriften über allerlei Kunst. Der Bach, der einst im engen Waldthal sich schäumend über Felsen ergoß, ist später zum weiten und breiten Gewäßer geworden. – Bilder laßen sich nicht mit Worten kopieren. – Und doch, wer möchte nicht andächtig zuhören, wenn Göthe redet, wovon es auch sei. –</p>
                    <p rend="zenoPLm4n0" xml:id="tg2349.2.9">Unser Befinden, sehr günstig bisher, ist wieder mal etwas gestört. Mit Bertha hat's angefangen; sie wurde mit Serum geimpft. Dann befand sich Ruthchen nicht wohl; im Magen wohl nur. Bei Beiden scheint die Sache vorüber zu sein. Unser kleines Annelieschen kam heut an die Reihe; aber der Dokter, der sie vorhin untersuchte, kann's nicht bedenklich finden. –</p>
                    <p rend="zenoPLm4n0" xml:id="tg2349.2.10">Allmählig, nach dem ewigen Regen, schien der Winter zu kommen. Es fiel etwas Schnee, zuzeiten blickte sogar freundlich die Sonne auf den Bleichplatz herunter und hat die dort schwebende Wäsche getrocknet. – Eben, natürlich, fängt's wieder zu miestern an. –</p>
                    <p rend="zenoPLm4n0" xml:id="tg2349.2.11">Tausend Grüße, liebe Grete, von allen und besonders von deinem alten</p>
                    <milestone unit="hi_start"/>
                    <p rend="zenoPR" xml:id="tg2349.2.12">Onkel <hi rend="italic" xml:id="tg2349.2.12.1">Wilhelm,</hi>
                    </p>
                    <milestone unit="hi_end"/>
                    <lb xml:id="tg2349.2.13"/>
                    <lg>
                        <l xml:id="tg2349.2.14">der oft an dich denken muß.</l>
                    </lg>
                </div>
            </div>
        </body>
    </text>
</TEI>