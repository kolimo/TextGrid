<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg134" n="/Literatur/M/Claudius, Matthias/Gedichte und Prosa/Asmus omnia sua secum portans/Dritter Teil/Christiani Zachaei Telonarchae Prolegomena">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Christiani Zachaei Telonarchae Prolegomena</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Claudius, Matthias: Element 00138 [2011/07/11 at 20:28:12]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Matthias Claudius: Werke in einem Band. Herausgegeben von Jost Perfahl, München: Winkler, [1976].</title>
                        <author key="pnd:118521098">Claudius, Matthias</author>
                    </titleStmt>
                    <extent>150-</extent>
                    <publicationStmt>
                        <date when="1976"/>
                        <pubPlace>München</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1740" notAfter="1815"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg134.3">
                <div type="h4">
                    <head type="h4" xml:id="tg134.3.1"> / Christiani Zachaei Telonarchae Prolegomena</head>
                    <milestone unit="head_start"/>
                    <p rend="zenoPC" xml:id="tg134.3.2">
                        <hi rend="italic" xml:id="tg134.3.2.1">über »die neueste Auslegung der ältesten Urkunde des menschlichen Geschlechts«. In zweien Antwortschreiben an Apollonium Philosophum.</hi>
                    </p>
                    <milestone unit="head_end"/>
                    <lb xml:id="tg134.3.3"/>
                    <lg>
                        <l rend="zenoPLm12n12" xml:id="tg134.3.4">
                            <hi rend="italic" xml:id="tg134.3.4.1">Ergo ubi commota feruet plebecula bile,</hi>
                        </l>
                        <l rend="zenoPLm12n12" xml:id="tg134.3.5">
                            <hi rend="italic" xml:id="tg134.3.5.1">Fert animus calidae fecisse silentia turbae</hi>
                        </l>
                        <l rend="zenoPLm12n12" xml:id="tg134.3.6">
                            <hi rend="italic" xml:id="tg134.3.6.1">Majestate manus – – –</hi>
                        </l>
                    </lg>
                    <milestone unit="hi_start"/>
                    <p rend="zenoPR" xml:id="tg134.3.7">
                        <hi rend="italic" xml:id="tg134.3.7.1">Persius Sat. IV</hi>
                    </p>
                    <milestone unit="hi_end"/>
                    <lb xml:id="tg134.3.8"/>
                    <p xml:id="tg134.3.9">1774. <num type="fraction">1<num type="fraction">
                                <hi rend="superscript" xml:id="tg134.3.9.1">1</hi>/<hi rend="subscript" xml:id="tg134.3.9.2">2</hi>
                            </num>
                        </num> Bogen in 4° auf Postpapier.</p>
                    <p rend="zenoPLm4n0" xml:id="tg134.3.10">Die Plebecula hat außer der commota bile noch das Nebenverdienst, daß sie den Verfasser der <hi rend="italic" xml:id="tg134.3.10.1">neuesten Auslegung</hi> nicht versteht, und doch verstanden haben will, und darüber geschwätzig <pb n="150" xml:id="tg134.3.10.2"/>
wird; daher denn so 'n Wunder – Majestate manus –gar kein übler Einfall ist. Wir unsers Orts können auch diesen <hi rend="italic" xml:id="tg134.3.10.3">Rezensenten,</hi> nach so vielen und mancherlei Anzeigen der <hi rend="italic" xml:id="tg134.3.10.4">neuesten Auslegung,</hi> mit nichts Bessers vergleichen als mit dem bekannten Mann beim Virgil, der, wenn er sein Haupt über die Welle heraushebt, Majestate Oris und Manus alle windige, Beaux Esprits, Dog- und Schis-matiker, der Wasserwelt auf der Stelle Mores lehrt. Er gibt zuerst Kardinalpunkte der <hi rend="italic" xml:id="tg134.3.10.5">neuesten Auslegung</hi> an, und beantwortet denn einige vorläufige Fragen, doch alles nach seiner Art, d.i. daß er nicht schwätzt noch sagt, sondern nur Zeichen und Winke macht, der Leser aber viel zu denken und zu lernen hat. Übrigens ist er der Mamamuschi von 3 Federn, seiner Gansfeder, seiner Schwanenfeder und seiner Rabenfeder.</p>
                </div>
            </div>
        </body>
    </text>
</TEI>