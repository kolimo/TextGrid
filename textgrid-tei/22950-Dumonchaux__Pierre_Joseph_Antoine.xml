<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg28" n="/Literatur/M/Dumonchaux, Pierre-Joseph-Antoine/Werk/Medicinische Anecdoten/Medicinische Anekdoten/20. Das Maaß des Vergnügens und des Schmerzens">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>20. Das Maaß des Vergnügens und des Schmerzens</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Dumonchaux, Pierre-Joseph-Antoine: Element 00031 [2011/07/11 at 20:29:17]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>[Dumonchaux, Pierre-Joseph-Antoine] : Medicinische Anecdoten : oder Sammlung besonderer Fälle, welche in die Anatomie, Pharmaceutik, Naturgeschichte etc. einschlagen, nebst einigen merkwürdigen Nachrichten von den berühmtesten Aerzten, Aus dem Französischen übersetzt, 1. Theil, Frankfurt und Leipzig: Tobias Göbhart, 1767 [Nachdruck München: Hormon Chemie, o. J.].</title>
                        <author>Dumonchaux, Pierre-Joseph-Antoine</author>
                    </titleStmt>
                    <extent>31-</extent>
                    <publicationStmt>
                        <date when="1767"/>
                        <pubPlace>Frankfurt und Leipzig</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1733" notAfter="1766"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg28.2">
                <div type="h4">
                    <head type="h4" xml:id="tg28.2.1">
                        <pb n="31" xml:id="tg28.2.1.1"/>
XX.</head>
                    <head type="h4" xml:id="tg28.2.2">Das Maaß des Vergnügens und des Schmerzens.</head>
                    <p xml:id="tg28.2.3">Man pfleget das Vergnügen und den Schmerzen insgemein als zwey einander vollkommen entgegen gesezte Dinge zu betrachten; sie sind aber wirklich zwey aneinander angränzende Eigenschaften.</p>
                    <lb xml:id="tg28.2.4"/>
                    <p rend="zenoPLm4n0" xml:id="tg28.2.5">Das Licht ist den Augen angenehm, ein gar zu starkes Licht verblendet und verwundet sie, und kann jemand plötzlich stock blind machen. Der Schall, welcher die Ohren vergnüget, unterscheidet sich nur nach dem wenigern oder mehrern Grad desjenigen Schalls, welcher sie zerreisset, und eine gänzliche Taubheit verursachen kann. Die Süßigkeit reizet den Geschmack, aber eine gar zu grosse Süßigkeit verursachet einen Eckel und Abscheu und verdirbt den Magen. Zärtliche Personen fallen von einem Geruch in Ohnmacht, welcher weniger empfindlichen Leuten angenehm und lieblich ist. Ein leichtes Kützeln ist die Quelle des grösten Vergnügens; wenn man es aber gar zu weit treibet, so ziehet es ein zuckendes Lächeln und bisweilen gar den Tod nach sich. Alle diese Dinge sind so bekannt, daß sie nicht den geringsten Widerspruch leiden; aber scheinet es wohl nicht ganz widersinnig zu seyn, <pb n="32" xml:id="tg28.2.5.1"/>
wenn man behauptet, daß das Vergnügen gleichsam in dem Schoos der heftigsten Schmerzen entstehen könne? Wenn wir hievon ein richtiges Urtheil fällen wollen, so müssen wir alles Vorurtheil bey Seite setzen.</p>
                    <lb xml:id="tg28.2.6"/>
                    <p rend="zenoPLm4n0" xml:id="tg28.2.7">Tamerlan, ein Vater von hundert Kindern, und Ueberwinder von hundert Völkern, ließ sich bisweilen aus Muthwillen mit Ruthen streichen. Dieser Umstand wird von allen Geschichtschreibern dieses berühmten Eroberers bezeuget; wir wollen aber nicht weiter an die Sitten dieses Tartarn gedenken. Es giebt Dinge, die einem Arzt nicht unbekannt seyn dörfen, und von denen er nicht nachdrücklich genug reden kann, noch solche allzuleicht übersehen darf. Hier folgen andere Umstände, welche Aufmerksamkeit verdienen.</p>
                    <lb xml:id="tg28.2.8"/>
                    <p rend="zenoPLm4n0" xml:id="tg28.2.9">Ein junger Mensch zu Paris, der einen Geschmack an den mechanischen Künsten fande, aber auch von einer ganz besondern Gemüthsart war, schloß sich an einem Abend in seiner Kammer ein, band sich die Brust, den Bauch, die Arme, die Fäuste, die Schenkel und die Beine mit in Schleifen geschlungenen Stricken, deren Ende an Nägeln befestiget waren, die er in den vier Wänden fest gemachet hatte. Nachdem er einen Theil der Nacht in diesem Zustand zugebracht hatte, und sich wieder los machen wollte, aber damit nicht zurechte kommen <pb n="33" xml:id="tg28.2.9.1"/>
konnte, schrie er endlich um Hülfe; einige Weiber, welche gleich in aller Frühe zu Markte giengen, hörten ihn, und liesen die Wache kommen. Man stieß die Kammerthüre so gleich ein, und fand unsern jungen Menschen in der Luft hängend, der nur eine Hand von seinen Banden los gebracht hatte. Man führte ihn zu einem Commissär und von da zu dem Herrn <hi rend="bold" xml:id="tg28.2.9.2">B---</hi> der dazumal General-Policeylieutenant war, und ihn selbst verhören wollte. Dieser junge Mensch sagte aus, daß er schon öfters dergleichen Versuche gemacht habe, wobey er ein unaussprechliches Vergnügen fände; daß es ihm zwar anfänglich etwas Schmerzen verursachete, wenn aber das Drucken der Ligaturen einen gewissen Grad erreichet hätte, so spürte er eine darauf folgende so angenehme Empfindung, die ihm den ersten Schmerzen auf das süsseste wiederum vergütete.</p>
                    <lb xml:id="tg28.2.10"/>
                    <p rend="zenoPLm4n0" xml:id="tg28.2.11">Daß der Tod der Gehangenen sehr angenehm seye, ist eine Wahrheit, welche der berühmte <hi rend="bold" xml:id="tg28.2.11.1">Wepfer</hi> in seinem Tractat <hi rend="italic" xml:id="tg28.2.11.2">de Apoplexia p.</hi> 174. so gründlich erwiesen hat, daß man nicht daran zweifeln kann. Der Verfasser der medicinischen Zeitung, von dem ich diese Bemerkung genommen habe, räth aber gleichwohl niemand, sich deswegen aufhängen zu lassen. Eben dieser <hi rend="bold" xml:id="tg28.2.11.3">Wepfer</hi> berichtet uns, daß die ausserordentliche Kälte einschläfert; <pb n="34" xml:id="tg28.2.11.4"/>
daß aber dieser fast unüberwindliche Schlaf um so viel gefährlicher seye. Diejenigen, welche mitten durch den Schnee in einer sehr strengen Kälte reisen, werden insgemein von einem heftigen Trieb zu schlafen überfallen, und wenn sie sich dieser gefährlichen Lockung überlassen, so erwachen sie niemals mehr wieder, wenn ihnen nicht etwann durch einen glücklichen Zufall jemand zu Hülfe kommet. Dergleichen Fälle sind in den Alpen sehr gewöhnlich. Der grosse <hi rend="bold" xml:id="tg28.2.11.5">Boerhave</hi> fand sich solchem A. 1709. in Holland in dem strengsten Winter ausgesetzet. Er erzählet selbst, daß er, da er sich nebst einem Wundarzt in eine Fuhr gesetzet hatte, um nach einem Ort zwey Meilen weit von Leyden zu fahren, wo sie eine Frau besuchen wollten, die den Schenkel gebrochen hatte, nebst dem Wundarzt und dem Kutscher von einem so heftigen Schlaf angefallen wurde, der sie mit solcher Annehmlichkeit reizte, daß sie sich diesem höchst gefährlichen Reitz gewiß würden überlassen haben, woferne er, der die Gefahr, die daraus zu besorgen war, kannte, sie nicht genöthiget hätte, von dem Wagen abzusteigen, und durch das Gehen ihrem Geblüt eine neue Bewegung zu geben. Durch welches Mittel sie sich ermunterten, und ihre erstarrten Kräften wieder belebten.</p>
                    <lb xml:id="tg28.2.12"/>
                    <p rend="zenoPLm4n0" xml:id="tg28.2.13">Ein irrländischer Reuter von dem Regiment von F--- der in einen Fluß gefallen war, und da man<pb n="35" xml:id="tg28.2.13.1"/>
 glaubte, daß er ertrunken seye, von einem Quartiermeister eben dieses Regiments, aus dem Wasser Sinn und Witzlos hervor gezogen wurde, erkannte seit dieser Zeit, wie vielen Dank er seinem Erretter schuldig ware; er versicherte aber, daß ihm seine Gegenwart eine geheime und unüberwindliche Furcht verursache; und diese Empfindung, deren er nicht mächtig war, käme, wie er sagte, davon her, weil er in diesem tiefen Schlund eine unbeschreiblich süsse Ruhe gefühlet habe.</p>
                    <lb xml:id="tg28.2.14"/>
                    <p rend="zenoPLm4n0" xml:id="tg28.2.15">Herr <hi rend="bold" xml:id="tg28.2.15.1">L.C.</hi> einer der berühmtesten Apothecker zu Paris, bekame vor øhngefähr fünf und zwanzig Jahren ein bösartiges Fieber, von welchem ihn einige französische Aerzte und Wundärzte zu helfen suchten, und deswegen häufige Aderlässen verordneten. Nach der letztern Aderläße, die etwas stark war, fiel er in eine Ohnmacht, und verbliebe in diesem Zustand so lange, daß die Umstehenden deswegen sehr besorgt waren. Er versicherte, daß sich, nachdem er alle äusserliche Empfindung verlohren hatte, seinen Augen ein so helles und reines Licht zeigte, daß er beynahe glaubte, sich in dem Aufenthalt der Glückseligen zu befinden. Er erinnert sich dieses Zustandes noch vollkommen, und saget, daß er in seinem ganzen Leben keinen so schönen Augenblick empfunden habe. Unterschiedliche Personen von jedem Alter und Geschlecht sagen, daß <pb n="36" xml:id="tg28.2.15.2"/>
sie in eben diesen Umständen eine dieser ähnlichen Empfindung gespüret haben.</p>
                    <lb xml:id="tg28.2.16"/>
                    <p rend="zenoPLm4n0" xml:id="tg28.2.17">Sollte wohl nicht nach einigen Beobachtungen von dieser Art, dasjenige Statt finden, was ein Theologus aus dem zwölften Jahrhundert behauptet hat, daß nämlich alle Menschen bey der Herannahung der Trennung des Leibes und der Seele, von einem Strahl des ersten Lichtes erleuchtet würden? <hi rend="italic" xml:id="tg28.2.17.1">Luminositas lucis primae.</hi>
                    </p>
                    <lb xml:id="tg28.2.18"/>
                    <p rend="zenoPLm4n0" xml:id="tg28.2.19">Bey den mehresten Beyspielen dieser Art ist die Ursache der angenehmen Empfindung, die man spüret, im Grund allezeit einerley. Die Verfassung, welche durch die Stricke, die Kälte, durch den Druck des allenthalben herum befindlichen Wassers, ober das Niedersinken, welches durch eine starke Aderläß verursachet wird, nimmt fast alles Blut aus denen <hi rend="italic" xml:id="tg28.2.19.1">Venis cutaneis,</hi> oder läst nur sehr wenig, und dieses fast ohne Bewegung, in selbigen. Was entstehet daraus? Daß das Blut und alle Säfte desto reichlicher und stiller in den innerlichen Gefässen, sonderlich aber in den Gefässen des Gehirns, fliessen, die von allem äusserlichen Druck am mehresten gesichert sind: Denn eben dieser Zufluß des Blutes erreget lebhafte und starke Empfindungen, und der ruhige und gleiche Umlauf desselben, machet die Empfindungen angenehm.</p>
                </div>
            </div>
        </body>
    </text>
</TEI>