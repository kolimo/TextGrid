<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg433" n="/Literatur/M/Egenolff, Christian/Werk/Sprichwörter - Schöne - Weise Klugredenn/Teutscher Sprichwörter Gemeyne außlegung/Ein ding ist nicht böß - wann mans gut versteht">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Ein ding ist nicht böß - wann mans gut versteht</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Egenolff, Christian: Element 00436 [2011/07/11 at 20:31:13]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Egenolff, Christian: Sprichwörter / Schöne / Weise Klugredenn. Darinnen Teutscher vnd anderer Spraach-en Höfflichkeit [...] In Etliche Tausent zusamen bracht, Frankfurt/Main, 1552. [Nachdruck Berlin: Edition Leipzig, 1968].</title>
                        <author key="pnd:122968468">Egenolff, Christian</author>
                    </titleStmt>
                    <extent>134-</extent>
                    <publicationStmt>
                        <date notBefore="1552" notAfter="1968"/>
                        <pubPlace>Berlin</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1502" notAfter="1555"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg433.2">
                <div type="h4">
                    <head type="h4" xml:id="tg433.2.1">Ein ding ist nicht böß / wann mans gůt versteht.</head>
                    <head type="h4" xml:id="tg433.2.2">Notares mala, optima.</head>
                    <p xml:id="tg433.2.3">Ein böß weib ist nit böß / wann mans kennet. Das creutz ist nit böß / wers fassen <pb n="134" xml:id="tg433.2.3.1"/>
oder tragen kan. Das leiden ist heylig / wers kan. Das creutz / ein böß weib / sünd / vnnd was man böß nennen kan / ist nit böß /wann mans kennet vnd brauchen kan / Dem reynen ist all ding reyn. Also auch ein böß weib dienet dem Socrati darzů / daß er daheym gedult leret / wie er daussen die leut tragen solt. Die sünd ist böß / die ist aber dem der Gott liebet / so reyn vnd gůt / daß sie jm zum besten kompt vnd dienet / daß er sie meidet /vnnd nach dem fall / als er den grewel vnd vnflat der sünd erfaren / sich fleissiger hüte / dester hitziger liebt / dester dienstlicher züspringt / vnd dester leichter andern glaubt vnd verzeihet. Den nutz zeygen die Leerer über den vierdten vers des ersten Psalms: <hi rend="italic" xml:id="tg433.2.3.2">Et omnia quæ faciet, prospera buntur,</hi> Alles was der fromm thůt / gehet mit glück ab vnnd an / vnnd darff sagen / daß auch die sünd den reynen reyn / vnd zu gůtem kommen / <hi rend="italic" xml:id="tg433.2.3.3">Nā post lapsum cauent cautius, amant feruentius, succurrunt officiosius, credunt facilius.</hi>
                    </p>
                    <p rend="zenoPLm4n0" xml:id="tg433.2.4">Das creutz ist böß / so es aber erkant / recht gefaßt vnn geküßt wirt / ists eitel heylthumb / das den menschen Gott behäglich / zum ewigen leben einfürt / vnn das schiff Charontis ist / darauff wir übern See des wůtendē Meers inns Paradeiß schiffen / so wir anders drein vnnd drauff sitzen / dann so manns nit kennet /wazů es nütz / veracht / vnnd nit küßt / fürt es vns nit über inns gelobt land. Also ist einem frommen mann ein böß hefftig weib ein gůt weib / dann er kennets /kan sie brauchen / vnnd jm selbs nütz machen. Der todt ist ein köstlich ding den außerwelten heyligen /vnd ein port vnnd Charon zum leben / Den gottlosen aber so jn nit kennen / vnd als einn butzenmann fliehen / ein eingang ewiger verdammniß. Der teufel ist der frommen eygen / also daß er jhn dienen vnd nachgehn můß / vnd sie herr heyssen / also alle ding. Was dir böß / ist auß deiner schuld dir böß. Dargegen halt es sich mit den gottlosen / den ist nichts reyn / dann beide vnreyn ist jr sinn vnd gewissen / vnnd seind zu allem gůten vntüchtig. Auch das höchst gůt / Gott selbs / vnd alles was man gůt nennen mag / ist den argen arg / den verkertē verkert / Gericht / gerechtigkeyt / liecht / leben / vnnd Gott selbs. Das gůt ist nit gůt / so es nit gůt wirt verstanden noch gefaßt. Man weyß nit warzů der besem gůt ist / biß er verkert wirt.</p>
                </div>
            </div>
        </body>
    </text>
</TEI>