<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg17" n="/Literatur/M/Ernst, Paul/Erzählungen/Komödianten- und Spitzbubengeschichten/Das grüne Ungeheuer">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Das grüne Ungeheuer</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Ernst, Paul: Element 00016 [2011/07/11 at 20:29:00]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Paul Ernst: Komödianten- und Spitzbubengeschichten, München: Georg Müller, 1928.</title>
                        <author key="pnd:118530909">Ernst, Paul</author>
                    </titleStmt>
                    <extent>33-</extent>
                    <publicationStmt>
                        <date when="1928"/>
                        <pubPlace>München</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1866" notAfter="1933"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg17.3">
                <div type="h4">
                    <head type="h4" xml:id="tg17.3.1">
                        <pb n="33" xml:id="tg17.3.1.1"/>
Das grüne Ungeheuer</head>
                    <p xml:id="tg17.3.2">Ein grünes Ungeheuer ist in das Land eingefallen und haust in einer Höhle. Der König muß ihm täglich zehn Jungfrauen schicken, die es verspeist, ungerupft und ungebraten. Wenn der König das nicht tut, dann verheert das Ungeheuer das ganze Land. Nun ist die Reihe auch an die Königstochter gekommen; sie soll mit ihren neun Kammerjungfern dem Ungeheuer gleichfalls zum Opfer gebracht werden. Aber in die jüngste Kammerjungfer ist Arlechin verliebt. Wenn ein mutiger Ritter das Ungeheuer zum Lachen bringt, dann ist es besiegt, wird in einen Käfig gesperrt und für Geld gezeigt. Viele tapfere Ritter haben schon versucht, das Ungeheuer zum Lachen zu bringen. Sie haben es an der Kehle gekitzelt, hinter den Ohren, unter den Achseln; sie haben ihm Gesichter geschnitten, Geschichten erzählt und sich auf den Kopf gestellt; sie haben sich verkleidet, haben ihm Juckpulver gestreut und haben Lieder dazu gesungen. Das Ungeheuer hat keine Miene bewegt, es hat nur, wenn sie mit ihrer Weisheit zu Ende waren, den Rachen aufgesperrt und sie hinuntergeschluckt, indem es dabei mit den Augen blinzelte. Arlechin gelingt es, das grüne Ungeheuer zum Lachen zu bringen; es lacht, daß ihm die dicken Tränen über die Backen kullern; dann geht es gutwillig in den Käfig, den schon der erste Ritter mitgebracht hatte, gähnt behaglich und wird an den Hof geführt, wo es dann den durchreisenden Fremden der besseren Stände für fünf Silbergroschen gezeigt wird.</p>
                    <p rend="zenoPLm4n0" xml:id="tg17.3.3">Arlechins Frau hat kein Talent; sie muß denn solche Rollen spielen wie das grüne Ungeheuer; aber die spielt sie auch vorzüglich. Und Arlechin selber ist in dem Stück überhaupt einfach unübertrefflich. Die Vorstellung ist also gelungen, die <pb n="34" xml:id="tg17.3.3.1"/>
ganze Stadt spricht von Arlechin und dem grünen Ungeheuer; zuerst spricht die erste Gesellschaft, dann die zweite Gesellschaft, dann die dritte Gesellschaft; endlich werden auch die Kinder neugierig, schicken eine Abordnung an den Bürgermeister und ersuchen, daß sie sich das Stück ansehen dürfen. Der Bürgermeister gewährt ihnen die Bitte, die Kinder sehen sich die Vorstellung an und sind begeisterter als alle drei Gesellschaften zusammen waren.</p>
                    <p rend="zenoPLm4n0" xml:id="tg17.3.4">Aber nun kommt eine schwere Krankheit über die Kinder. Sie klagen über Halsschmerzen, legen sich ins Bett, haben Fieber; der Hals ist ihnen plötzlich wie zugeschnürt, sie können weder schlucken noch sprechen, kaum noch atmen; und die Ärzte sagen, daß manche Kinder sterben werden, welche die Krankheit befallen hat. Die armen Eltern weinen, geloben Wallfahrten und Kerzen, verändern ihren Lebenswandel; die Wut der Krankheit wird nicht gemildert.</p>
                    <p rend="zenoPLm4n0" xml:id="tg17.3.5">Der kleine Sohn des Bürgermeisters winkt seinen Vater aus Bett. Dann nimmt er sein Schreibtäfelchen, denn er gehört zu den Schwerkranken, und schreibt darauf: Er weiß wohl, daß er schwer krank ist, wenn die Erwachsenen auch immer lächeln und ihm sagen, daß er morgen wieder werde mit seinem weißen Schaf spielen können. Aber wenn er denn nun sterben muß, so bittet er seine Eltern, daß sie ihm vorher noch einmal eine große Freude machen, nämlich er bittet, daß Arlechin und das grüne Ungeheuer zu ihm kommen, und daß Arlechin das grüne Ungeheuer zum Lachen bringt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg17.3.6">Der Bürgermeister bezwingt sich, daß ihm nicht die Tränen kommen; er lächelt und sagt, er wolle Arlechin bitten; dann geht er und kommt zu Arlechin.</p>
                    <p rend="zenoPLm4n0" xml:id="tg17.3.7">Arlechin und seine Frau sitzen am Kleiderschrank und weinen. Der Kleiderschrank hat vor langen Seiten seine Türen verloren und dient nun als Lager für ihr krankes Kind; als Vorhang aber haben sie das Fell des grünen Ungeheuers aufgehängt, <pb n="35" xml:id="tg17.3.7.1"/>
damit das Kind dunkel liegen kann, wenn es schlafen will. Der Bürgermeister weint mit ihnen, als er das kranke Kind sieht, dann bringt er seine Bitte vor. Die Mutter läuft schnell in die Nachbarschaft und ersucht Colombinen, solange bei dem Kind zu bleiben; dann holen sie den Arlechinanzug aus dem Brotkasten, hängen das Fell des grünen Ungeheuers ab und folgen dem Bürgermeister.</p>
                    <p rend="zenoPLm4n0" xml:id="tg17.3.8">Arlechin hat seinen Sieg über das Ungeheuer durch seine Menschenkenntnis davongetragen. Wenn man einen Anderen zum Lachen bringen will, so muß man selbst ernsthaft bleiben. Er bleibt also ernsthaft und preist dem Ungeheuer einfach die Vorzüge der Tugend vor dem Laster. Das Ungeheuer wackelt mit dem Kopf, es bezwingt sich, es will nicht lachen; aber als endlich Arlechin in Tränen ausbricht und von der Seligkeit spricht, welche der arme, aber tugendhafte Schauspieler auf seiner Bühne genießt, indessen der lasterhafte Reiche sich auf seinem roten Plüschsessel langweilt, da öffnet das Ungeheuer seinen Rachen, hält sich den Bauch und lacht, bis ihm zuletzt die Tränen kommen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg17.3.9">Der Schn des Bürgermeisters lacht auch, er lacht immer angestrengter, sein Gesicht wird dunkelrot, Vater und Mutter stürzen hinzu, um ihn aufrechtzuerhalten; plötzlich macht er innerlich eine große Anstrengung, das Geschwür platzt, welches ihm in der Kehle saß, er schreit laut: »Das Ungeheuer weint ja«; dann sinkt er kraftlos zurück.</p>
                    <p rend="zenoPLm4n0" xml:id="tg17.3.10">Der Arzt verordnet ihm noch ein leicht schweißtreibendes Mittel und verlangt vierzehn Tage lang strenge Bettruhe, dann steht er dafür, daß das Kind gerettet ist.</p>
                    <p rend="zenoPLm4n0" xml:id="tg17.3.11">Der Bürgermeister drückte den beiden Komödianten selig die Hand und schüttelte sie kräftig, die Frau Bürgermeister sagte, sie müßten in das Eßzimmer gehen, und befahl, ihnen zu decken; aber in der allgemeinen Aufregung achteten die Dienstboten nicht auf sie. So gingen sie in die Stube, in welcher sie <pb n="36" xml:id="tg17.3.11.1"/>
sich angekleidet hatten, zogen sich wieder um und packten Arlechintracht und Ungeheuerfell zusammen, dann liefen sie schnell nach Hause zu ihrem kranken Kind.</p>
                    <p rend="zenoPLm4n0" xml:id="tg17.3.12">Colombine kam ihnen auf der Treppe entgegen. »Erschreckt nicht, es ist etwas eingeschlafen.« »Es ist tot!« schrie die Mutter und lief in die Stube; da lag der Kleine lang ausgestreckt, mit wächsernem Gesicht; Colombine hatte ihm die Augen zugedrückt.</p>
                </div>
            </div>
        </body>
    </text>
</TEI>