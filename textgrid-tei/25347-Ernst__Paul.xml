<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg43" n="/Literatur/M/Ernst, Paul/Erzählungen/Komödianten- und Spitzbubengeschichten/Der edelmütige Arlechin">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Der edelmütige Arlechin</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Ernst, Paul: Element 00042 [2011/07/11 at 20:29:00]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Paul Ernst: Komödianten- und Spitzbubengeschichten, München: Georg Müller, 1928.</title>
                        <author key="pnd:118530909">Ernst, Paul</author>
                    </titleStmt>
                    <extent>177-</extent>
                    <publicationStmt>
                        <date when="1928"/>
                        <pubPlace>München</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1866" notAfter="1933"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg43.3">
                <div type="h4">
                    <head type="h4" xml:id="tg43.3.1">
                        <pb n="177" xml:id="tg43.3.1.1"/>
Der edelmütige Arlechin</head>
                    <p xml:id="tg43.3.2">Man kennt Arlechin als einen positiven Charakter. Er sagt: »Was ich habe, das habe ich. Was ich gegessen habe, das habe ich erst recht. Das gibt Fleisch, das gibt Knochen, das gibt Nervenkraft, und die Nervenkraft ist die Hauptsache.« Arlechin ist nicht sentimental. Er sagt: »Wie man sich bettet, so liegt man.« Er ist skeptisch. Er sagt: »Was meine Augen sehen, das glaubt mein Verstand; was meine Augen nicht sehen, das glaubt er nicht.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg43.3.3">Man frage alle Menschenkenner der Welt, ob ein solcher Schauspieler wohl edelmütig sein kann? Sie werden es verneinen. Aber die Natur ist unerschöpflich in ihren Zusammenstellungen. Arlechin ist doch edelmütig.</p>
                    <p rend="zenoPLm4n0" xml:id="tg43.3.4">Arlechin hat einen Bruder, der gänzlich talentlos ist. Die Beiden hatten ursprünglich Vermögen. Arlechin hat es verloren, indem er ein Geschäft mit türkischem Honig auf den Jahrmärkten begründete; er hatte zu schnell reich werden wollen und nahm zu wenig Zucker, weshalb die Kinder die Konkurrenz bevorzugt hatten. Der Bruder – nennen wir ihn Ottavio – wurde von den Leuten eine Seele genannt; Arlechin war zu ihm gegangen, als seine Zahlungsschwierigkeiten eintraten und hatte ihm auseinandergesetzt, wie vorteilhaft es für ihn sei, wenn er sein Kapital bei ihm anlege, Ottavio wußte von der schlechten Lage seines Bruders und hatte sein Zartgefühl bewundert, daß er ihm nicht die Wahrheit sagte, wo er ihn ja denn als Bruder, der für den Bruder eben eintreten muß, gewissermaßen gezwungen hätte, das Geld zu geben; er war errötet und hatte so getan, als ob er an den großen Geschäftsgewinn glaube und hatte sein Vermögen hergegeben. Nach dem Zusammenbruch hatte er Arlechin bewiesen, daß er durchaus nicht etwa sein Gläubiger sei, denn er habe sein <pb n="178" xml:id="tg43.3.4.1"/>
Geld ja nicht ihm geliehen gehabt, sondern in das Geschäft gesteckt, und wenn das Geschäft dank der Rührigkeit und Einsicht Arlechins große Erträge abgeworfen hätte, so hätte er an denen ja auch seinen Vorteil gehabt; und Arlechin hatte das alles völlig eingesehen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg43.3.5">Nachher also hatte Arlechin sein Talent entdeckt und war zur Bühne gegangen. Ottavio hatte, wie wir wissen, kein Talent, aber Arlechin beschützte ihn. »Ich drücke dich durch,« hatte er gesagt, »ich bin der Mann dazu, dich durchzudrücken.« Und er hatte ihn durchgedrückt. Ottavio lernte bei einem Haarkünstler. Einen guten Haarkünstler braucht man bei einer Truppe. Und auf Arlechins Empfehlung beschäftigten ihn alle Komödianten.</p>
                    <p rend="zenoPLm4n0" xml:id="tg43.3.6">Man kann sich denken, wie Ottavio den Bruder rühmte. Ohne Arlechin wäre er untergegangen in dem erbarmungslosen Kampf ums Dasein, aber Arlechin hatte ihn durchgedrückt, und nun stand er da. Er stand da. Ja, Arlechin war das große Glück seines Lebens. Das konnte er wohl sagen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg43.3.7">Hier ist der Außenstehende vielleicht verwundert. Denn da die Komödianten alle kein Geld haben, außer Arlechin, der die größte Gage hat und sie auch oft bezahlt bekommt, weil ohne ihn, wie er sagt, der Karren im Dreck säße, und weil in Geldsachen bei ihm die Gemütlichkeit aufhört, obwohl er sonst sehr gemütlich ist, sehr gemütlich, nur in Geldsachen ist er kitzlich, und das kann ihm keiner übelnehmen, denn er kennt das Geschäftsleben, er hat in der Welt gestanden, ... also: wo waren wir denn? Ja, da die andern Komödianten alle kein Geld haben, der Direktor auch nicht, und von Arlechin kann er doch natürlich nichts nehmen als Bruder, und wo er ihm so verpflichtet ist, so bezahlen sie ihren Haarkünstler natürlich nicht, und dadurch war es auch nur möglich gewesen, daß Arlechin den Bruder durchgedrückt hatte, denn die meisten Leute pflegen ja solche Stellungen aufzugeben, in denen sie <pb n="179" xml:id="tg43.3.7.1"/>
nicht bezahlt werden, und so war auch die Stellung des Haarkünstlers bei der Truppe gerade frei gewesen, als Ottavio eintraf. Aber wer sich hier verwundert, der kennt die Eigentümlichkeiten des Lebens ohne Geld nicht. Man ist verloren ohne Geld, wenn man allein ist; wenn man aber zu einer Gesellschaft gehört, wo Alle kein Geld haben, so lebt man immer irgendwie, zuweilen ganz gut, zuweilen weniger gut, je nachdem, aber man lebt jedenfalls. Und so lebt auch Ottavio und rühmt den Edelmut seines Bruders.</p>
                    <p rend="zenoPLm4n0" xml:id="tg43.3.8">Lavinie ist eine sehr junge Schauspielschülerin, die aus guter Familie stammt und aus Idealismus zur Bühne gehen will, weil sie findet, daß die Schauspieler höhere Menschen sind. Ihr Vater hat sie verflucht, ihre Mutter schickt ihr zuweilen ein paar Skudi, die sie aus Schwenzelgroschen zusammenspart, und sie hat Stunden bei Arlechin, der erklärt, daß sie sehr viel Talent hat, während der Direktor sagt, sie habe so viel Temperament wie eine Kuh.</p>
                    <p rend="zenoPLm4n0" xml:id="tg43.3.9">Was ist natürlicher, als daß sie Arlechin liebt? Sie liebt zunächst den Künstler in ihm, dann aber auch den Menschen, denn Ottavio erzählt ihr beständig, wie er ihn durchgedrückt hat, und was er alles ihm verdankt, und daß er das große Glück seines Lebens ist. Natürlich liebt Arlechin sie auch, denn weshalb sollte er das nicht? Sie ist jung, sie ist hübsch, sie macht keine Ansprüche, sie bezahlt sogar ihre Stunden. Und dergestalt geht nun alles, wie es gehen soll.</p>
                    <p rend="zenoPLm4n0" xml:id="tg43.3.10">Aber wie es kommt, das ist unklar, das weibliche Herz ist wetterwendisch, Gründe sind nicht vorhanden: an einem Tage, plötzlich, steht Lavinie vor Ottavio und erklärt ihm, Arlechin sei ein trivialer Mensch und keine Künstlernatur, und sie liebe ihn, Ottavio, und Ottavio müsse mit ihr fliehen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg43.3.11">Natürlich entstehen hier Kämpfe in Ottavios Brust; aber solche Kämpfe sind nicht weiter interessant; kurz, Ottavio hat zwar ein schlechtes Gewissen, aber er entflieht mit Lavinien.</p>
                    <p rend="zenoPLm4n0" xml:id="tg43.3.12">
                        <pb n="180" xml:id="tg43.3.12.1"/>
Was ist die Wirklichkeit? Glückliche Menschen haben die Eigenschaft, daß bei ihnen sich die Wirklichkeit nach ihren Wünschen richtet. Und Ottavio ist ein glücklicher Mensch.</p>
                    <p rend="zenoPLm4n0" xml:id="tg43.3.13">Er hat einen Brief an seinen Bruder hinterlassen, in welchem er auf das lebhafteste schildert, wie er seit langem eine stille, aber heftige Leidenschaft für Lavinien hatte, wie das häufige Zusammensein mit der Geliebten diese Leidenschaft zu verzehrender Glut entfachte, welche Seelenqualen er erduldete, weil er gegen den geliebten Bruder, dem er alles verdankte, sich in der schändlichsten Weise undankbar zeigte, indem er sein Vertrauen mißbrauchte. Nun ist die Tragödie entschieden. Er kann seinem Bruder nur zurufen: Ich fügte dir das schwerste Leid zu, das man einem liebenden Mann zufügen kann. Wir haben kein Recht auf deine Verzeihung. Aber wenn ich an den Edelmut denke, den du in jedem Augenblick deines Lebens bewiesen hast, so rufe ich dir dennoch zu: Ein Anderer könnte es nicht, du kannst es: Verzeihe uns.</p>
                    <p rend="zenoPLm4n0" xml:id="tg43.3.14">Man versetze sich in die Lage Arlechins. Es kann ja vorkommen, daß man ganz froh ist, wenn einem ein Anderer die Geliebte entführt. Man ist gebrochen, man ist erbittert, aber man ist sie schließlich doch los. Hier liegt die Sache ganz anders. Nicht nur, daß ihm die Stunden entgehen, die anständig und pünktlich bezahlt wurden; weshalb hätte er Lavinien denn nicht behalten sollen?</p>
                    <p rend="zenoPLm4n0" xml:id="tg43.3.15">Er würde also an sich wütend werden über diesen Hans Narren von Ottavio. Und er hätte ein Recht dazu, wahrhaftig, er hätte es. Der Mensch machte doch nichts wie dumme Streiche. Aber wenn er nun den Brief liest, dann kann er doch wieder nicht erbittert sein. Er kann nur lachen. Ja, das kann er. Er kann lachen. Die Welt ist drollig. Es lohnt sich nicht, wenn man wütend wird. Lachen muß man. Und so lacht er denn. Er lacht aber nur, wenn er für sich allein ist; wenn er bei den andern Komödianten sitzt, dann schweigt er.</p>
                    <p rend="zenoPLm4n0" xml:id="tg43.3.16">
                        <pb n="181" xml:id="tg43.3.16.1"/>
Ottavio und Lavinie sind unsagbar glücklich. Beide haben endlich die Erfüllung ihrer Sehnsucht gefunden. Wenn Ottavio aus dem Hause gehen muß, um eine Besorgung zu machen, etwa beim Bäcker oder Fleischer, dann hängt sich Lavinie um seinen Hals und fragt: Wirst du mich auch nicht vergessen, wirst du mich lieb behalten? Und Ottavio unterdrückt eine Träne, preßt Lavinien wortlos an sich und eilt ab, indessen sie an das Fenster läuft, um zu sehen, wie er aus dem Hause tritt und die Straße hinuntergeht.</p>
                    <p rend="zenoPLm4n0" xml:id="tg43.3.17">Aber Ottavio hat seine Stellung bei der Bühne verloren, und Laviniens Mutter kann nicht so viel schicken, daß zwei Menschen durchkommen können; man begreift, daß die Beiden bald in Verlegenheit geraten. Sie versetzen einen Ring, den Lavinie einmal von einem Oheim geerbt hat; sie borgen; endlich sind sie so weit, daß Lavinie weinend, die Hände vor dem Gesicht, auf dem Bett liegt und sich in die Kissen eingewühlt hat, und Ottavio mit großen Schritten im Zimmer auf und ab geht, sich mit den Händen durch die Haare fährt und Ausrufe von sich gibt, wie: »Unglaublich!« »Unerhört!« »Wer hätte das von den Menschen gedacht!« »Man wird ja irr an der Menschheit!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg43.3.18">Eine einzige Hoffnung bleibt ihnen in ihrer Lage: der Edelmut Arlechins.</p>
                    <p rend="zenoPLm4n0" xml:id="tg43.3.19">Lavinie zweifelt an diesem Edelmut. Aber Ottavio beugt sich über sie, streichelt ihr die Wangen und sagt, er verstehe es, wenn sie so denke, das sei eben die tiefe Niedergeschlagenheit; aber er kenne seinen Bruder, und auch nach allem, was er ihm zugefügt, werde er nicht vergeblich an ihn schreiben.</p>
                    <p rend="zenoPLm4n0" xml:id="tg43.3.20">Und so schreibt er denn an ihn, indem er schildert, daß sie gar kein Geld haben, daß man sie ins Gefängnis werfen will, daß er ihre einzige Hoffnung ist, denn sonst stehen sie ganz verlassen in der Welt; daß andere Menschen – denn er kennt jetzt die Menschen, er hat sie kennen gelernt! – triumphieren <pb n="182" xml:id="tg43.3.20.1"/>
würden; aber Arlechin zeigt gerade in einer solchen Lage seine Seele, ein Arlechin rächt sich durch Edelmut. Und er, Ottavio, schämt sich nicht, diesen Edelmut anzuflehen, denn er weiß ja, daß Arlechin ihn kennt, daß er weiß: Ottavio ist desselben Blutes wie er und würde selber auch nicht anders handeln.</p>
                    <p rend="zenoPLm4n0" xml:id="tg43.3.21">Arlechin bekommt den Brief; er schüttelt den Kopf, er ist in einer dummen Lage. Natürlich hat er gar keine Lust, auch noch Geld hinter seinem Bruder herzuwerfen. Für ihn sorgt auch niemand. Er hat immer für sich selber sorgen müssen. Das Leben hat ihm nichts erspart. Andererseits, was soll man denn machen, wenn man einen solchen Brief bekommt? Überschwenglich ist ja Ottavio immer. Die Lage ist sehr verdrießlich. Aber schließlich setzt er sich hin, schreibt einen Brief, daß man die Welt nehmen muß wie sie ist, daß man nicht immer in seinen Träumen leben kann, und dies sei das erste und das letzte Mal, daß er Geld schicke, und Ottavio solle ihm nicht wieder kommen, und er solle sich eine bürgerliche Existenz gründen; und damit packt er vierzig Skudi ein und schickt sie ab.</p>
                    <p rend="zenoPLm4n0" xml:id="tg43.3.22">Vierzig Skudi sind ja nicht viel, wenn man achtunddreißig Skudi Schulden hat bei Bäcker, Fleischer, Krämer, Milchfrau und Zimmerwirtin. Ottavio sagt zu Lavinien: »Was habe ich dir gesagt? Ich kenne meinen Bruder! Die Schulden sind wir los, jetzt beginnt ein neues Leben!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg43.3.23">Der Krämer hat ein Lottogeschäft. Heute nachmittag soll die Ziehung sein. Ottavio geht mit Lavinien am Arm vorbei, zwei Skudi hat er noch. Er tritt wortlos in den Laden und setzt sie; heftig erschrocken erwartet ihn draußen Lavinie. »Das Geld meines Bruders bringt uns Glück«, sagt er; Lavinie seufzt, was geschehen ist, das ist geschehen, sie überlegt sich schon die ganze Zeit, wie sie etwas verdienen kann, um Ottavio den Kampf zu erleichtern.</p>
                    <p rend="zenoPLm4n0" xml:id="tg43.3.24">
                        <pb n="183" xml:id="tg43.3.24.1"/>
Aber was geschieht am Nachmittag? Sämtliche Nummern Ottavios haben gewonnen. Ottavio ist ein reicher Mann.</p>
                    <p rend="zenoPLm4n0" xml:id="tg43.3.25">Die Leute haben früher immer gelächelt, wenn sie mit Ottavio sprachen. Jetzt lächeln sie nicht mehr, sie haben Respekt. Laviniens Vater hat seinen Fluch zurückgezogen, Ottavio ist längst mit ihr verheiratet. Das Geld ist in sicheren Hypotheken festgelegt; er kann Lavinien nichts abschlagen, und Lavinie hatte gebeten, er solle ihren Vater die Anlage besorgen lassen, um ihm die Zuversicht zu geben, daß sein Kind nun auch wirklich gut untergebracht sei.</p>
                    <p rend="zenoPLm4n0" xml:id="tg43.3.26">Jeder weiß, wem Ottavio sein Glück verdankt: seinem Bruder Arlechin. Arlechin ist nach hartem Kampf mit sich selber zurückgetreten und hat ihm Lavinien überlassen; mit seinem Letzten hat er ihm durchgeholfen, als er nichts hatte, rein gar nichts; er lebt allein, ohne Familie, in angestrengter Arbeit; aber er weiß, daß sein Bruder Ottavio stets an ihn denkt.</p>
                </div>
            </div>
        </body>
    </text>
</TEI>