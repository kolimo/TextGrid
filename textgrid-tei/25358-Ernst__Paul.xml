<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg54" n="/Literatur/M/Ernst, Paul/Erzählungen/Komödianten- und Spitzbubengeschichten/Der Strick über der Rolle">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Der Strick über der Rolle</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Ernst, Paul: Element 00053 [2011/07/11 at 20:29:00]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Paul Ernst: Komödianten- und Spitzbubengeschichten, München: Georg Müller, 1928.</title>
                        <author key="pnd:118530909">Ernst, Paul</author>
                    </titleStmt>
                    <extent>252-</extent>
                    <publicationStmt>
                        <date when="1928"/>
                        <pubPlace>München</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1866" notAfter="1933"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg54.3">
                <div type="h4">
                    <head type="h4" xml:id="tg54.3.1">
                        <pb n="252" xml:id="tg54.3.1.1"/>
Der Strick über der Rolle</head>
                    <p xml:id="tg54.3.2">Es kam nach Rom ein Mann, der dem Heiligen Vater einen Plan von großem Werte unterbreitete. Er sagte: »Eure Heiligkeit besitzt viele Güter, Dörfer und Städte, aber Eure Heiligkeit hat kein Geld. Geld aber muß der Mensch vor allen Dingen haben, sonst ist er dumm. Nun schlage ich vor, Eure Heiligkeit läßt Ihr ganzes Eigentum abschätzen, und dann läßt Sie Zettel drucken, auf denen steht, daß dem Inhaber für zehn Skudi oder für hundert oder tausend Skudi, je nachdem, ein Teil des Eigentums Eurer Heiligkeit verschrieben ist. Diese Zettel nehmen dann die Leute gern an, denn sie sind ja so gut wie bares Geld; aber weil einer für zehn oder hundert oder tausend Skudi immer nur ein Stückchen von Eurer Heiligkeit Eigentum verschrieben bekommt, mit dem er gar nichts machen kann, so wird er nicht verlangen, daß ihm das Stückchen ausgeliefert wird, sondern wird es Eurer Heiligkeit lassen, bei der es ja sicher aufgehoben ist; und so wird Eure Heiligkeit mit den gedruckten Zetteln alle Leute bezahlen können, und wird außerdem neu bauen und kaufen können, was Sie will.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg54.3.3">Dieser Plan leuchtete dem Heiligen Vater, der ein kluger Mann war, sogleich ein. Er ließ einen guten Kupferstecher kommen, dem er den Auftrag gab, Platten zu stechen für die Verschreibungen, dann kaufte er eine Druckerpresse, und wie die Platten fertig waren, mußte der Fremde, welcher den Plan gehabt hatte, ihm Verschreibungen drucken.</p>
                    <p rend="zenoPLm4n0" xml:id="tg54.3.4">Alle Leute nahmen die Verschreibungen gern an, denn sie wurden nun bezahlt, und so gern nahmen sie sie, daß manche der Gläubiger ihre Rechnungen noch einmal durchsahen und um weniges erhöhten, damit sie nur mehr von den Verschreibungen <pb n="253" xml:id="tg54.3.4.1"/>
bekamen, und alle Handwerker bauten jetzt mit Lust, denn sie wußten, daß sie nun am Sonnabend immer ihren Lohn in Verschreibungen erhielten, die so gut waren wie bares Geld, und auch von allen Bäckern und Fleischern angenommen wurden. Und weil sie die Verschreibungen für ihre Rechnungen bekamen, die auch Noten hießen, so nannte man sie Noten.</p>
                    <p rend="zenoPLm4n0" xml:id="tg54.3.5">Es war Seiner Heiligkeit darauf angekommen, recht schnell die Noten drucken zu lassen, deshalb war vorher keine Aufnahme der Güter gemacht; und wie nun alle Leute sich so über die Noten freuten, da dachte er, daß er sich die Mühe sparen könne, sie nachträglich zu machen, denn wenn wirklich durch einen Zufall mehr Noten ausgegeben werden sollten, als sein Eigentum wert war, so merkten das die Leute ja doch nicht, denn da die Noten bei allen Handwerkern, Arbeitern, Kaufleuten, Bauern und sonstigen Untertanen verstreut waren, so wußte ja niemand, wie viele aus gegeben waren; und Seine Heiligkeit wußte es endlich selber nicht, weil er sie zuletzt nicht mehr gezählt hatte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg54.3.6">Nun hatte der Erfinder des Planes einen treuen Diener namens Matteuccio, einen frischen und heiteren jungen Mann, der ihm half, die Platten schwärzen, das Papier anfeuchten, die Presse anziehen, die gedruckten Scheine auf Bindfäden zum Trocknen hängen, und Ähnliches tun, was mit der Herstellung der Noten verbunden war. Dieser Matteuccio nun sah, wie er half, Scheine von zehn, hundert und tausend Skudi täglich erzeugen, die dann in dicke Pakete verschnürt und an den Schatzmeister abgeliefert wurden; er rechnete sich aus, daß er nur fünf Skudi Lohn im Monat bekam und in dieser Zeit viele hunderttausend Skudi machte, ohne weitere Unkosten als einige Paoli für ein Rieß Papier, und darüber befiel ihn ein heftiger Groll, denn er dachte, daß das doch eine unchristliche Ausbeutung seiner Arbeit war. So ließ er sich denn seine Gedanken <pb n="254" xml:id="tg54.3.6.1"/>
eine Weile im Kopfe herumgehen und trat endlich vor seinen Meister, um batzig seinen Abschied zu verlangen. Der gute Meister hatte sich immer so über das Gelingen seines Planes gefreut, daß er an Matteuccio weiter nicht gedacht hatte; so war er denn zuerst verwundert, aber er konnte seinen Gesellen nicht mit Gewalt zurückhalten, und deshalb ließ er ihn gehen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg54.3.7">Es war wohl einmal geschehen, daß eine Note nicht so gut gedruckt war, wie sie sein sollte; der Meister hatte sie dann verworfen, weil er darauf hielt, daß nur schöne Blätter aus seiner Werkstatt kamen. Solche verworfenen Noten hatte sich Matteuccio gesammelt zu einem kleinen Päckchen, und das nahm er nun mit. Die andern Leute waren nicht so sorgfältig wie sein Meister, und als sie seine Noten sahen, da nahmen sie die gern an Zahlungsstelle an, und so konnte Matteuccio sich denn eine hübsche Wohnung mieten in dem Palazzo eines päpstlichen Kammerherrn und konnte einem tüchtigen Kupferstecher Geld anbieten, daß er ihm Platten stach, wie sie sein Meister zum Drucken hatte, und konnte sich auch eine Druckerpresse anschaffen, mit der er arbeitete. Und weil er einen Ehrgeiz in sein Geschäft setzte, indem er doch nun gewissermaßen seinem Meister mit seiner Arbeit gegenübertrat, so druckte er seine Noten noch schöner als sie sein Meister druckte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg54.3.8">Dergestalt arbeitete er nun eine Weile, und indem er die vielen Päckchen, die er herstellte, doch nicht behalten wollte, so kaufte er für sie, was ihm behagte, damit seine Noten unter die Leute kamen wie die seines Meisters. Und zuerst erstand er den Palazzo, in welchem er wohnte, dann kaufte er eine große Villa in Frascati, die ihm viel Wein und Öl einbrachte, und zuletzt erhandelte er eine große Standesherrschaft in den Abruzzen und wurde dadurch Herzog, und hieß also nun der Duca Matteuccio.</p>
                    <p rend="zenoPLm4n0" xml:id="tg54.3.9">Während dergestalt Alle froh waren, Seine Heiligkeit, und der <pb n="255" xml:id="tg54.3.9.1"/>
Meister, und der Duca Matteuccio, und die Handwerker, Geschäftsleute, Bauern und sonstigen Untertanen, und Alle den Meister segneten, dessen Klugheit eine solche Blüte des Handels und Gewerbes verursacht hatte, geschah es, daß allmählich alle Waren teurer wurden; die Hausfrauen klagten, daß die Maronen und Bohnen nicht mehr zu erschwingen waren, die Arbeiter verlangten stürmisch höhere Löhne, die Schuhmacher erklärten, daß das Leder um das Doppelte gestiegen sei, und die Milch wurde immer wässeriger. Dem Heiligen Vater kamen diese Beschwerden zu Ohren, und da er seine Untertanen liebte, so war er sehr traurig und berief eine Versammlung der Kardinäle ein, um sie zu befragen, was man bei dieser Teuerung tun solle. In dieser Versammlung sagte er, er habe schon den Plan gefaßt, noch eine zweite Presse zu kaufen und noch mehr Noten drucken zu lassen, damit die Leute mehr Geld bekommen könnten, denn ihn koste es ja nicht viel, ob er noch einen andern Meister anstelle. Einer der Kardinäle aber, der ein sehr philosophischer Mann war, stand auf und hielt eine lange Rede, in welcher er zeigte, daß die Teuerung davon komme, daß schon zu viel Noten gedruckt seien, und als die Andern lachten, fügte er hinzu, er glaube, daß auch andere Leute als der Meister, den Seine Heiligkeit angestellt, sich ans Werk gesetzt haben. Hierüber wurde der Heilige Vater nun ärgerlich, denn das konnte er natürlich nicht erlauben, daß auch andere Leute druckten, denn die Noten waren doch Verschreibungen auf seine Güter. So befahl er denn dem Kardinal, den Polizeihauptmann Tromba zu sich kommen zu lassen und ihm die Untersuchung der Sache aufzutragen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg54.3.10">Der Hauptmann Tromba hatte schon einen Verdacht. Man muß nämlich wissen, daß Matteuccio, wie er nun Herzog geworden, und weil er ein hübscher und gewandter junger Mann war, beschlossen hatte, zu heiraten; und zwar hatte er um die <pb n="256" xml:id="tg54.3.10.1"/>
Hand einer sehr schönen, jungen Gräfin angehalten, deren Eltern ihm gegenüber wohnten. Die Eltern waren etwas verwundert über den großen Reichtum Matteuccios und hielten es für richtig, ehe sie ihm ihre Tochter gaben, erst Tromba zu befragen, was er von Matteuccio wisse. Tromba hatte Nachforschungen angestellt; und als nun der Kardinal ihn kommen ließ und ihm seine Gedanken mitteilte, da blies er die Backen auf, hielt sich den Zeigefinger auf die Nase, erklärte, daß Tromba die Sache machen werde, und ging sogleich mit zwei Sbirren, Matteuccio zu verhaften.</p>
                    <p rend="zenoPLm4n0" xml:id="tg54.3.11">Er ließ die beiden Sbirren auf der Straße vor dem Hause und stieg die Treppen hoch zu Matteuccios Kämmerchen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg54.3.12">Matteuccio bewohnte nämlich noch immer den kleinen Raum unterm Dach, den er zuerst gemietet hatte, denn er wollte nicht gern, daß die zahlreiche Dienerschaft etwas von seiner Arbeit erfuhr.</p>
                    <p rend="zenoPLm4n0" xml:id="tg54.3.13">Tromba klopft an; Matteuccio fragt innen, wer da sei; Tromba nennt seinen Namen; Matteuccio räuspert sich und antwortet nicht; Tromba klopft wieder und sagt, er komme wegen der Noten, die er gedruckt, denn er hält es in diesem Fall für richtig, mit offenen Karten zu spielen; Matteuccio hinter der Tür erwidert, er sei ein ruhiger Bürger, bezahle seine Steuern, und wünsche in Frieden gelassen zu werden; Tromba erklärt ihm nun, daß der Heilige Vater sehr ärgerlich sei und erörtert weitläufig, daß doch nicht jeder Bürger Noten drucken könne, weil das dem Kredit des Staates schaden würde.</p>
                    <p rend="zenoPLm4n0" xml:id="tg54.3.14">Unterdessen hat Matteuccio die Noten, welche er noch besitzt, in ein Bündel gebunden und in die Tasche gesteckt. Dann tritt er auf seinen kleinen Balkon hinaus und sieht sich um. Der Palazzo war recht verwittert, und um seiner künftigen, jungen Frau eine Freude zu machen, hatte er Maurer angenommen, welche die Außenseite ausbessern sollten. Unterm Dach ist eine <pb n="257" xml:id="tg54.3.14.1"/>
Rolle befestigt, auf der ein Strick läuft, mit welchem die Maurer ihren Kalk nach oben ziehen. Es ist gerade Mittagpause, auf den Gerüsten ist kein Mann zu sehen, auch die Straße ist leer. Während draußen Tromba über den Kredit des Staates spricht, hat Matteuccio eine Schlinge gemacht und sich unter den Arm gelegt. Nun nimmt er den anderen Teil des Strickes in die Hände, der über die Rolle läuft und denkt sich so langsam auf die Straße niederzulassen, indem er immer von dem anderen Teil des Strickes ein Stückchen nachgibt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg54.3.15">Dessen Ende aber liegt unten auf der Straße vor den Füßen der beiden Sbirren, die sich in den Hauseingang gestellt haben, um den Schatten zu genießen. Wie diese die merkwürdige Bewegung des Strickes sehen, treten sie vor und schauen hoch, und da erblicken sie unsern Matteuccio, wie er, ganz vertieft in seine Bewegung, langsam nach unten gleitet.</p>
                    <p rend="zenoPLm4n0" xml:id="tg54.3.16">Die Sbirren durchschauen sofort, daß das ein Fluchtversuch ist. Sie ergreifen den Strick und halten ihn fest. Nun kann er nicht mehr nachgeben, und Matteuccio, der etwa in der Mitte seines Palazzo in der Luft schwebt, kann nicht weiter nach unten kommen. Er späht vorsichtig nach unten und erblickt die Beiden.</p>
                    <p rend="zenoPLm4n0" xml:id="tg54.3.17">Aber er hängt gerade vor einem der Maurergerüste, auf dem ein großer Eimer mit Kalk steht. Es gelingt ihm, sich auf das Gerüst zu schwingen; dann hängt erden schweren Eimer an den Haken, der an seiner Strickseite befestigt ist und stößt sich mit dem Eimer wieder ab. Die beiden Sbirren haben verwundert zu geschaut; aber nun geschieht, was Matteuccio wollte: er selber mit dem Eimer zusammen ist schwerer als die beiden Sbirren, der Strick zieht an, und plötzlich schweben die Sbirren, die am anderen Ende halten, in der Luft. Dem einen glückt es noch, abzuspringen; der andere wird mit rasender Schnelligkeit trotz Schreiens und Zappelns an Matteuccio <pb n="258" xml:id="tg54.3.17.1"/>
vorbei in die Höhe gezogen; und wie Matteuccio mit dem Eimer unten angekommen ist, da schwebt der Sbirre in der Luft, unter sich nur ein kurzes Endchen Strick, und ruft um Hilfe.</p>
                    <p rend="zenoPLm4n0" xml:id="tg54.3.18">Der andere Sbirre stürzt sich auf Matteuccio; aber der hat sich schon von seiner Schlinge befreit und droht ihm: sowie der Sbirre ihn berührt, hakt er den Strick aus dem Eimer und läßt ihn los, und der Genosse in der Luft stürzt auf das Pflaster und bricht sich alle Knochen. Der in der Luft bittet und fleht; Tromba hat inzwischen oben die Tür eingetreten, ist in das leere Zimmer gedrungen und blickt ingrimmig nach unten; der untere Sbirre muß den Strick halten, um seinen Genossen zu retten; Matteuccio hakt den Haken aus dem Eimer, und da der zweite Sbirre ein magerer Mann ist, so hebt er sich langsam, indessen der dicke erste sinkt; von oben aber ruft Tromba ihnen die anzüglichsten Worte zu, die eigentlich Beamtenbeleidigungen sind.</p>
                    <p rend="zenoPLm4n0" xml:id="tg54.3.19">Marteuccio aber sieht kaltblütig nach, ob er seine Banknoten noch in der Tasche hat und biegt dann um die Ecke. Er hat keinen Grund, sich länger in Rom aufzuhalten; Palazzo, Villa und Standesherrschaft werden zwar vom Heiligen Vater eingezogen, aber er besitzt noch genug Geld, um, da er doch nun einmal Herzog ist, in Neapel standesgemäß zu leben.</p>
                    <p rend="zenoPLm4n0" xml:id="tg54.3.20">Er hat dann auch nach einiger Zeit eine Ehe in Neapel geschlossen; seine Gattin stammt aus einem der ersten Geschlechter; seine Familie blüht noch heute in Neapel und gilt als eine der vornehmsten in ganz Süditalien.</p>
                </div>
            </div>
        </body>
    </text>
</TEI>