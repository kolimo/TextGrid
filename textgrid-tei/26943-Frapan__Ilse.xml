<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg18" n="/Literatur/M/Frapan, Ilse/Erzählungen/Zwischen Elbe und Alster/Die Liebe ist gerettet">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Die Liebe ist gerettet</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Frapan, Ilse: Element 00016 [2011/07/11 at 20:29:14]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Ilse Frapan: Zwischen Elbe und Alster. Hamburger Novellen, 3. Auflage, Berlin: Gebrüder Paetel, 1908.</title>
                        <author key="pnd:11600987X">Frapan, Ilse</author>
                    </titleStmt>
                    <extent>133-</extent>
                    <publicationStmt>
                        <date>1908</date>
                        <pubPlace>Berlin</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1849" notAfter="1908"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg18.3">
                <div type="h4">
                    <head type="h4" xml:id="tg18.3.1">Die Liebe ist gerettet</head>
                    <p xml:id="tg18.3.2">
                        <pb n="133" xml:id="tg18.3.2.1"/>
                        <pb type="start" n="135" xml:id="tg18.3.2.2"/>Er hatte sehr früh geheiratet, aus Eigensinn mehr denn aus Liebe; zu früh und unglücklich, das sagte er sich selbst schon nach einigen Jahren. Aber er sagte es ganz leise. Was konnte <hi rend="spaced" xml:id="tg18.3.2.3">sie</hi> dafür, daß Frauen so schnell altern, geistig, daß ihr Wachstum überraschend bald aufhört, und daß sie dann, ein armselig Zwitterding, durchs Leben gehen? Kindisch in allen höchsten Dingen; ganz kalter Weltverstand, sobald der Mann an ihrer Seite einen Schwung wagen möchte. – Er fühlte, daß er weiterwachsen mußte seiner Natur nach, und daß sie es ihm erschwerte, und seine Augen wurden schwach über dem Anblick der eisernen Pflicht, sie fingen an, hinauszuspähen, zu suchen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg18.3.3">In einem großen Schmerz, der ihn betroffen, als Pflegerin, Trösterin, hätte die erstarrte Seele seiner Gattin vielleicht wieder weich und flüssig werden können; aber die Probe blieb erspart, es ging Jahr auf Jahr weiter ohne besonderes Unglück. Es waren auch keine Kinder da, die durch ihr Leben oder Sterben ein Bindeglied gebildet hätten.</p>
                    <p rend="zenoPLm4n0" xml:id="tg18.3.4">
                        <pb n="135" xml:id="tg18.3.4.1"/>
So blieb es bei jenem gefährlichen Suchen. Das Schicksal war ihm gnädig oder – grausam; es führte ihm ein Mädchen in den Weg, das gerade Widerspiel seiner Gattin, voll Leben und Saft. Er sträubte sich nicht einen Augenblick, stürzte sich mit aller zusammengesparten Glut auf das Glücksgeschenk, bediente sich aller Mittel, edler und ruchloser, um ihre Liebe zu gewinnen. Es gelang ihm, sie gehörte ihm, er hielt sich für den glücklichsten Menschen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg18.3.5">Daß die Geliebte ein ganzer Mensch war und darum auch ihn ganz verlangte, daß sie ihn mit Gram und Abscheu in der Lüge leben sah, lernte er erst durch ihre Worte erkennen; ja, erst als Jahre vergangen und er die Neigung der Geliebten erkalten fühlte, durchzuckte ihn der Gedanke, daß etwas geschehen, daß er sich befreien müsse. Diese Gewißheit verließ ihn nicht mehr, aber sie ward zu einem Alp, der ihm Tag und Nacht auf die Brust drückte. Er fand die Worte nicht, seiner Gattin, die ihn mit ihren schwarzen Augen so klug und ruhig ansah, wenn er ihr aus alter Gewohnheit die Hand küßte, – seiner Gattin, die er selbst gewählt, die er zu schützen und zu stützen versprochen (und die seinen Schutz auch täglich beanspruchte, wenn sie ins Theater ging oder spät aus einer Gesellschaft heimkam!) so plötzlich zu erklären, daß er jetzt eine andre heiraten müsse, und daß zwischen ihnen alles zu Ende sei. <pb n="136" xml:id="tg18.3.5.1"/>
»Was ›Collage‹«! rief er und schleuderte den französischen Roman, in dem er gelesen, auf den Boden, »die Ehe ist der wahre Collage! Wer mir das gesagt hätte, als ich so leicht hineinging!« Und er vergrub seinen Kopf in die Sofakissen und schloß die Augen mit dem Wunsche, sie nie wieder aufzutun.</p>
                    <p rend="zenoPLm4n0" xml:id="tg18.3.6">Um so wacher war seine Frau, sie beobachtete ihn eifersüchtig und scharf. Er hatte die Absicht gehabt, sich allmählich immer weiter von ihr zurückzuziehen, damit ihr der letzte Ruck nicht so wehe tue; aber sie ging ihm immer um soviel nach, als er ihr auswich, und so blieb die Entfernung doch die gleiche. Heute hatte sie ihn fast liebreich angesehen und gesagt: »Du arbeitest zu viel, schreibst immer bis tief in die Nacht; ich denke, du bist recht blaß, wollen wir nicht den Arzt fragen?« Ein roher Mensch würde sie jetzt anschreien, dachte er in Verzweiflung, würde sagen: »du bist mein Unglück, du machst mich krank!« Aber kann denn <hi rend="spaced" xml:id="tg18.3.6.1">ich</hi> das? Kann ich ihr so antworten, wenn sie mit diesem Blick, diesem Blick aus den ersten Tagen unsrer Ehe mich ansieht? O, wie beneide ich diese rohen Menschen! – Sie entfernte sich von ihm mit einem lauernden Lächeln, achselzuckend über sein Nichtantworten.</p>
                    <p rend="zenoPLm4n0" xml:id="tg18.3.7">Als er allein war, zog er nach vielem ängstlichen Umherschauen einen Brief von der Geliebten hervor; ach, er war schon ein halbes Jahr alt, und <pb n="137" xml:id="tg18.3.7.1"/>
sie sagte ihm darin, sie sei es müde; nicht sowohl des Wartens müde, als des Mannes, der solches Warten ertrug. »Alles ist gegen mich verschworen! Ich habe nie Glück gehabt im Leben, soll keins haben!« seufzte er.</p>
                    <p rend="zenoPLm4n0" xml:id="tg18.3.8">Es klingelte heftig an der Haustür. »Ein neues Unglück!« dachte er unwillkürlich, aber es war nur die neue Zeitung, die ihm mit solchem Ungestüm ins Haus geschleudert wurde. Er entfaltete sie, las hier und dort, – plötzlich stieß er einen furchtbaren Schrei aus. Frau Sophie trat im selben Augenblick herein; sie war nicht fern gewesen, nur an der Tür des Zimmers, am Schlüsselloch. Ihre Augen hefteten sich argwöhnisch auf das in seinen Händen zitternde Blatt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg18.3.9">»Du hast etwas Unangenehmes erfahren? Hugo, was ist dir?« fragte sie in dem ihr eigenen Ton des berechtigten Examinators.</p>
                    <p rend="zenoPLm4n0" xml:id="tg18.3.10">»Ach, Sophie, es ist fürchterlich, ich erliege darunter!« rief er und fuhr mit der Zeitung hinter sich, als wolle er sie verbergen, während seine Augen rot, aber ohne Tränen gerade vor sich hin starrten.</p>
                    <p rend="zenoPLm4n0" xml:id="tg18.3.11">»Laß mich sehen!« sagte sie und trat fast mit einem Sprung auf ihn zu.</p>
                    <p rend="zenoPLm4n0" xml:id="tg18.3.12">Er versuchte keinen Widerstand mehr. »Ja, warum sollte ich sie dir nicht geben,« wimmerte er, »warum verstecken? Warum solltest du es nicht wissen, jetzt, wo alles vorbei ist?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg18.3.13">Sie hatte das Blatt hastig aus seiner Hand genommen; <pb n="138" xml:id="tg18.3.13.1"/>
dann nach kurzem gespannten Hineinblicken faltete sie es zusammen und legte es auf den Schreibtisch. »Und das erregt dich so?« sagte sie in kalt verwundertem Ton und blickte auf den trostlos Zusammengesunkenen, der zitterte, als ob ein Fieber ihn schüttele. »Also auch eine von deinen Flammen? Nein, fahre nicht auf! Ich hab es übrigens längst gewußt. All diese Briefe, diese Zusammenkünfte in ästhetischen Gesellschaften, – schade, daß ich sie nie gesehen, es wäre mir doch interessant gewesen! War sie sehr verliebt in dich?« Und als er gar nicht antwortete: »Nun, du wirst nicht erwarten, daß ich wegen dieses Todesfalls Trauer anlege!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg18.3.14">»Aber ich!« rief er, und Schweißtropfen standen ihm auf der Stirn, wie er emporfuhr. »Bleibe noch, höre! Ich gehe zu ihrem Begräbnis, nichts soll mich abhalten, auch <hi rend="spaced" xml:id="tg18.3.14.1">du</hi> nicht!« Er schrie immer lauter, immer wilder.</p>
                    <p rend="zenoPLm4n0" xml:id="tg18.3.15">»Tue, was du willst,« sagte sie verächtlich und schlug krachend die Tür zu.</p>
                    <p rend="zenoPLm4n0" xml:id="tg18.3.16">»Und um diese Frau hab ich mein Glück versäumt!« jammerte der Unglückliche.</p>
                    <p rend="zenoPLm4n0" xml:id="tg18.3.17">Abends ward er vergeblich zum Tee gerufen, das Studierzimmer fand sich leer. »Der gnädige Herr ist noch nicht zurück seit Nachmittag,« meldete das Mädchen mit jener Horchermiene, welche Dienstboten in Familien annehmen, wo es ›nicht klappt‹.</p>
                    <p rend="zenoPLm4n0" xml:id="tg18.3.18">
                        <pb n="139" xml:id="tg18.3.18.1"/>
Hugo war wie ein Verzweifelnder nach der Wohnung der Toten geeilt. An der Tür empfing ihn die Mutter.</p>
                    <p rend="zenoPLm4n0" xml:id="tg18.3.19">»Lassen Sie mich zu ihr,« bat er, fast schreiend vor Aufregung, »ich weiche nicht von ihrem Sarge, nichts, niemand soll mich von dort vertreiben!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg18.3.20">Die alte Dame befreite sich hastig von seinen klammernden Händen, und zurücktretend winkte sie ihm mit einem scheuen Blick nach rückwärts. Ihr verweintes Gesicht sah ihn fremd und feindlich an. »Kommen Sie hier herein,« flüsterte sie, »ich möchte doch nicht, daß noch jetzt das Mädchen – –«</p>
                    <p rend="zenoPLm4n0" xml:id="tg18.3.21">Er folgte ihr in ein kleines Vorzimmer, in dem er früher oft gewartet. An den Wänden hingen Bilder und kleiner Zimmerschmuck, den er ihr geschenkt. »Ach, was sollen jetzt noch diese Rücksichten?« murmelte er.</p>
                    <p rend="zenoPLm4n0" xml:id="tg18.3.22">Die Mutter trat dicht auf ihn zu. »Sie hatten freilich nie Rücksichten für die Tote,« sagte sie hart und bitter, »Sie haben meine Tochter unglücklich gemacht, Sie sollen sie nicht noch im Tode beschimpfen! Gehen Sie mit Ihrem Kummer, den die Leute nicht verstehen! Sie, ein verheirateter Mann! Schämen Sie sich! So, das war es, was ich Ihnen sagen wollte.« Sie öffnete die Tür vor ihm und zeigte hinaus.</p>
                    <p rend="zenoPLm4n0" xml:id="tg18.3.23">»Ich habe sie tief und heiß geliebt,« schluchzte er und wollte auf die Knie sinken. Aber in dem <pb n="140" xml:id="tg18.3.23.1"/>
gramvollen Gesicht vor ihm stand derselbe unerbittliche Zug, den er beim letzten Abschied im Antlitz der Geliebten gesehen, und die Augen zeigten streng und stumm nach der Haustür.</p>
                    <p rend="zenoPLm4n0" xml:id="tg18.3.24">»Ich hätte doch wohl Rechte«, murmelte er hinauswankend.</p>
                    <p rend="zenoPLm4n0" xml:id="tg18.3.25">»Kein Recht, kein Recht!« tönte es in hartem Flüstern ihm nach.</p>
                    <p rend="zenoPLm4n0" xml:id="tg18.3.26">Nein, keines! Sie sprach wahr.</p>
                    <p rend="zenoPLm4n0" xml:id="tg18.3.27">Er wanderte mit müden Schritten vor dem Hause auf und ab, bis es dunkel wurde. Zuweilen sah er nach dem Fenster oben hinauf, dem offenen, mit dem heruntergelassenen Vorhang.</p>
                    <p rend="zenoPLm4n0" xml:id="tg18.3.28">Die Leute fingen an, ihn zu betrachten, Ladendiener und Mädchen, die vor ihren Haustüren standen, zeigten ihn einander, und eine der kokett geputzten Verkäuferinnen stieß ihn absichtlich mit dem Ellbogen, als er wieder vorüberkam. Aber er sah sie nicht einmal an, ging wie ohne Besinnung.</p>
                    <p rend="zenoPLm4n0" xml:id="tg18.3.29">Zuletzt mußte er stehen bleiben, weil ein Laternenanzünder mit Leiter und Lämpchen ihm den Weg versperrte. Im Licht der nun brennenden Laterne sah er an einer Hauswand einen Mietzettel hängen; es war gerade jener Wohnung gegenüber. Ohne Besinnen stieg er die Treppe hinauf, klingelte und verlangte ein Zimmer zu mieten. Da er das Geld für einige Monate voraus gleich auf den Tisch legte, <pb n="141" xml:id="tg18.3.29.1"/>
so war die Sache schnell abgemacht, und die Vermieterin ließ den stummen, blassen Herrn, der Lampe und Nachtessen abwies, ohne weiteres ›einziehen‹. Er setzte sich ans Fenster und starrte hinüber, bis jedes Licht in der Straße erlosch; und dann kam der späte Mond und warf seine traurigen Strahlen auf jenen Vorhang drüben. Im Stuhle sitzend, verfiel er endlich in einen unruhigen, unerquicklichen Halbtraum; der erste Morgenstrahl erweckte ihn daraus, und teilnahmslos beobachtete er das Erwachen der Straße, er sah Kränze in jenes Haus tragen und schwarzgekleidete Frauen ein-und ausgehen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg18.3.30">Und abends kamen zwei Träger mit dem leeren Sarge; die Gaslaterne schien auf das Metallschild, daß er fast ihren Namen lesen konnte. Als er das gesehen, warf er sich in den Kleidern aufs Bett, versteckte den Kopf und schlief vor Mattigkeit und Übermüdung tief und traumlos ein.</p>
                    <p rend="zenoPLm4n0" xml:id="tg18.3.31">Früh am andern Tage schreckte ihn ein lautes, heftiges Gespräch aus dem sonderbaren lähmungsartigen Zustande, in dem er sich befand.</p>
                    <p rend="zenoPLm4n0" xml:id="tg18.3.32">Ohne Anklopfen ward die Tür geöffnet, und seine Frau trat herein.</p>
                    <p rend="zenoPLm4n0" xml:id="tg18.3.33">Sie sah etwas erhitzt aus, sonst wie immer, aber ihre Stimme war fast unkenntlich, als sie ihm entgegenrief: »Ich verlange zu wissen, was das zu bedeuten hat!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg18.3.34">
                        <pb n="142" xml:id="tg18.3.34.1"/>
Er setzte sich aufrecht und biß die Zähne zusammen. Dann holte er tief Atem und begann: »Es ist mir jetzt klar geworden, Sophie, klar geworden, daß es so nicht fortgehen kann zwischen uns – –«</p>
                    <p rend="zenoPLm4n0" xml:id="tg18.3.35">»Komm nach Haus und rede dort!« rief sie aufbrausend, »es ist nicht nötig, daß die hier dich hören.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg18.3.36">»Nein,« sagte er, den Kopf schüttelnd, »nicht nach Haus, wir haben ja auch längst keins mehr –«</p>
                    <p rend="zenoPLm4n0" xml:id="tg18.3.37">Sie war bleich geworden, die Empörung erstickte ihre Stimme. »Hast du vergessen, daß wir – fast zwanzig Jahre verheiratet sind?« brachte sie rauh und stoßweise hervor.</p>
                    <p rend="zenoPLm4n0" xml:id="tg18.3.38">»Entsetzlich, entsetzlich!« schrie er. »Zwanzig Jahre! Warum erinnerst du mich daran! Und ich hätte glücklich sein können!« Sein Kopf sank auf die Brust, Tränen liefen ihm über die Wangen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg18.3.39">Sie rüttelte ihn am Arm. »Ist dein Verstand verwirrt? Hat dich der Tod jener Person toll gemacht? Weißt du, daß ich deine Frau bin, und daß du mir gehörst nach Pflicht und Gesetz?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg18.3.40">»O,« sagte er, »ich habe sie geliebt und hätte bei ihr bleiben sollen, das war meine Pflicht, die ich verkannte, verkannte! Aber« – und sein Gesicht leuchtete auf im Feuer eines plötzlichen Entschlusses<pb n="143" xml:id="tg18.3.40.1"/>
 – »es ist noch nicht zu spät! Jetzt tue ich, was ich längst hätte tun sollen, – ich lasse mich von dir scheiden, um ganz ihr zu gehören, ihr allein!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg18.3.41">Und wie erhoben von seiner eigenen tapferen Offenheit holte er tief Atem und sah die Frau stolz und fast freundlich an.</p>
                    <p rend="zenoPLm4n0" xml:id="tg18.3.42">»Es ist einfach toll,« sagte sie außer sich vor Zorn und doch halb zum Lachen geneigt, »vollkommen toll und unsinnig! Sie ist ja tot!« schrie sie ihm ins Gesicht, »was soll das jetzt? Wozu der Skandal? Bedenke doch – –«</p>
                    <p rend="zenoPLm4n0" xml:id="tg18.3.43">»Es geschieht, was muß,« rief er, ihre Hand abwehrend, »nicht eine Stunde mehr könnt ich mit dir leben.« Und als sie ihn immer noch halb ungläubig ansah, fügte er in sanfterem Tone hinzu: »Ja, sie ist tot, aber die Liebe, verstehst du? die – Lie–be!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg18.3.44">Er sprach ihr das Wort vor, langgezogen, eindringlich, als habe sie es noch nie gehört; das Spottlächeln erstarb ihr auf den Lippen. – Plötzlich bemerkte er, daß drüben auf der Straße der Trauerzug sich in Bewegung setzte. Er ergriff seinen Hut und eilte ohne ein weiteres Wort, ohne Gruß an ihr vorbei und zur Tür hinaus, um sich den Leidtragenden anzuschließen.</p>
                    <pb n="144" xml:id="tg18.3.45"/>
                </div>
            </div>
        </body>
    </text>
</TEI>