<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg1720" n="/Literatur/M/Goethe, Johann Wolfgang/Theoretische Schriften/»Urworte. Orphisch«">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>»Urworte. Orphisch«</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Goethe, Johann Wolfgang: Element 01125 [2011/07/11 at 20:24:11]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <notesStmt>
                <note>
                              Erstdruck in: Über Kunst und Altertum (Stuttgart), Bd. 2, Heft 3, 1820.
                           </note>
            </notesStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Johann Wolfgang von Goethe: Berliner Ausgabe. Herausgegeben von Siegfried Seidel: Poetische Werke [Band 1–16]; Kunsttheoretische Schriften und Übersetzungen [Band 17–22], Berlin: Aufbau, 1960 ff.</title>
                        <author key="pnd:118540238">Goethe, Johann Wolfgang</author>
                    </titleStmt>
                    <extent>567-</extent>
                    <publicationStmt>
                        <date when="1960"/>
                        <pubPlace>Berlin</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1749" notAfter="1832"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg1720.3">
                <milestone unit="sigel" n="Goethe-BA Bd. 17" xml:id="tg1720.3.1"/>
                <head type="h3" xml:id="tg1720.3.2">Johann Wolfgang Goethe</head>
                <head type="h2" xml:id="tg1720.3.3">»Urworte. Orphisch«</head>
                <p xml:id="tg1720.3.4">
                    <pb type="start" n="567" xml:id="tg1720.3.4.1"/>
Nachstehende fünf Stanzen sind schon im zweiten Heft der »Morphologie« abgedruckt, allein sie verdienen wohl einem größeren Publikum bekannt zu werden; auch haben Freunde gewünscht, daß zum Verständnis derselben einiges <pb n="567" xml:id="tg1720.3.4.2"/>
geschähe, damit dasjenige, was sich hier fast nur ahnen läßt, auch einem klaren Sinne gemäß und einer reinen Erkenntnis übergeben sei.</p>
                <p rend="zenoPLm4n0" xml:id="tg1720.3.5">Was nun von älteren und neueren orphischen Lehren überliefert worden, hat man hier zusammenzudrängen, poetisch-kompendios, lakonisch vorzutragen gesucht. Diese wenigen Strophen enthalten viel Bedeutendes in einer Folge, die, wenn man sie erst kennt, dem Geiste die wichtigsten Betrachtungen erleichtert.</p>
                <lb xml:id="tg1720.3.6"/>
                <div type="h4">
                    <head type="h4" xml:id="tg1720.3.7">Δαιμων, Dämon</head>
                    <lg>
                        <l xml:id="tg1720.3.8">Wie an dem Tag, der dich der Welt verliehen,</l>
                        <l xml:id="tg1720.3.9">Die Sonne stand zum Gruße der Planeten,</l>
                        <l xml:id="tg1720.3.10">Bist alsobald und fort und fort gediehen</l>
                        <l xml:id="tg1720.3.11">Nach dem Gesetz, wonach du angetreten.</l>
                        <l xml:id="tg1720.3.12">So mußt du sein, dir kannst du nicht entfliehen,</l>
                        <l xml:id="tg1720.3.13">So sagten schon Sibyllen, so Propheten;</l>
                        <l xml:id="tg1720.3.14">Und keine Zeit und keine Macht zerstückelt</l>
                        <l xml:id="tg1720.3.15">Geprägte Form, die lebend sich entwickelt.</l>
                    </lg>
                    <lb xml:id="tg1720.3.16"/>
                    <p xml:id="tg1720.3.17">Der Bezug der Überschrift auf die Strophe selbst bedarf einer Erläuterung. Der Dämon bedeutet hier die notwendige, bei der Geburt unmittelbar ausgesprochene, begrenzte Individualität der Person, das Charakteristische, wodurch sich der Einzelne von jedem andern bei noch so großer Ähnlichkeit unterscheidet. Diese Bestimmung schrieb man dem einwirkenden Gestirn zu, und es ließen sich die unendlich mannigfaltigen Bewegungen und Beziehungen der Himmelskörper unter sich selbst und zu der Erde gar schicklich mit den mannigfaltigen Abwechselungen der Geburten in Bezug stellen. Hiervon sollte nun auch das künftige Schicksal des Menschen ausgehen, und man möchte, jenes erste zugebend, gar wohl gestehen, daß angeborne Kraft und Eigenheit mehr als alles übrige des Menschen Schicksal bestimme.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1720.3.18">Deshalb spricht diese Strophe die Unveränderlichkeit <pb n="568" xml:id="tg1720.3.18.1"/>
des Individuums mit wiederholter Beteuerung aus. Das noch so entschieden Einzelne kann als ein Endliches gar wohl zerstört, aber, solange sein Kern zusammenhält, nicht zersplittert noch zerstückelt werden, sogar durch Generationen hindurch.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1720.3.19">Dieses feste, zähe, dieses nur aus sich selbst zu entwickelnde Wesen kommt freilich in mancherlei Beziehungen, wodurch sein erster und ursprünglicher Charakter in seinen Wirkungen gehemmt, in seinen Neigungen gehindert wird, und was hier nun eintritt, nennt unsere Philosophie</p>
                    <lb xml:id="tg1720.3.20"/>
                </div>
                <div type="h4">
                    <head type="h4" xml:id="tg1720.3.21">Τυχη, das Zufällige</head>
                    <lg>
                        <l xml:id="tg1720.3.22">Die strenge Grenze doch umgeht gefällig</l>
                        <l xml:id="tg1720.3.23">Ein Wandelndes, das mit und um uns wandelt;</l>
                        <l xml:id="tg1720.3.24">Nicht einsam bleibst du, bildest dich gesellig,</l>
                        <l xml:id="tg1720.3.25">Und handelst wohl so, wie ein andrer handelt:</l>
                        <l xml:id="tg1720.3.26">Im Leben ist's bald hin-, bald widerfällig,</l>
                        <l xml:id="tg1720.3.27">Es ist ein Tand und wird so durchgetandelt.</l>
                        <l xml:id="tg1720.3.28">Schon hat sich still der Jahre Kreis geründet,</l>
                        <l xml:id="tg1720.3.29">Die Lampe harrt der Flamme, die entzündet.</l>
                    </lg>
                    <lb xml:id="tg1720.3.30"/>
                    <p xml:id="tg1720.3.31">Zufällig ist es jedoch nicht, daß einer aus dieser oder jener Nation, Stamm oder Familie sein Herkommen ableite: denn die auf der Erde verbreiteten Nationen sind so wie ihre mannigfaltigen Verzweigungen als Individuen anzusehen, und die Tyche kann nur bei Vermischung und Durchkreuzung eingreifen. Wir sehen das wichtige Beispiel von hartnäckiger Persönlichkeit solcher Stämme an der Judenschaft; europäische Nationen, in andere Weltteile versetzt, legen ihren Charakter nicht ab, und nach mehreren hundert Jahren wird in Nordamerika der Engländer, der Franzose, der Deutsche gar wohl zu erkennen sein; zugleich aber auch werden sich bei Durchkreuzungen die Wirkungen der Tyche bemerklich machen, wie der Mestize an einer klarern Hautfarbe zu erkennen ist. Bei der Erziehung, wenn sie nicht öffentlich und nationell ist, behauptet <pb n="569" xml:id="tg1720.3.31.1"/>
Tyche ihre wandelbaren Rechte. Säugamme und Wärterin, Vater oder Vormund, Lehrer oder Aufseher, so wie alle die ersten Umgebungen an Gespielen, ländlicher oder städtischer Lokalität, alles bedingt die Eigentümlichkeit durch frühere Entwickelung, durch Zurückdrängen oder Beschleunigen; der Dämon freilich hält sich durch alles durch, und dieses ist denn die eigentliche Natur, der alte Adam und wie man es nennen mag, der, so oft auch ausgetrieben, immer wieder unbezwinglicher zurückkehrt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1720.3.32">In diesem Sinne einer notwendig aufgestellten Individualität hat man einem jeden Menschen seinen Dämon zugeschrieben, der ihm gelegentlich ins Ohr raunt, was denn eigentlich zu tun sei, und so wählte Sokrates den Giftbecher, weil ihm ziemte zu sterben.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1720.3.33">Allein Tyche läßt nicht nach und wirkt besonders auf die Jugend immerfort, die sich mit ihren Neigungen, Spielen, Geselligkeiten und flüchtigem Wesen bald da-, bald dorthin wirft und nirgends Halt noch Befriedigung findet. Da entsteht denn mit dem wachsenden Tage eine ernstere Unruhe, eine gründlichere Sehnsucht; die Ankunft eines neuen Göttlichen wird erwartet.</p>
                    <lb xml:id="tg1720.3.34"/>
                </div>
                <div type="h4">
                    <head type="h4" xml:id="tg1720.3.35">Ερως, Liebe</head>
                    <lg>
                        <l xml:id="tg1720.3.36">Die bleibt nicht aus! – Er stürzt vom Himmel nieder,</l>
                        <l xml:id="tg1720.3.37">Wohin er sich aus alter Öde schwang,</l>
                        <l xml:id="tg1720.3.38">Um Stirn und Brust den Frühlingstag entlang,</l>
                        <l xml:id="tg1720.3.39">Scheint jetzt zu fliehn, vom Fliehen kehrt er wieder,</l>
                        <l xml:id="tg1720.3.40">Da wird ein Wohl im Weh, so süß und bang.</l>
                        <l xml:id="tg1720.3.41">Gar manches Herz verschwebt im Allgemeinen,</l>
                        <l xml:id="tg1720.3.42">Doch widmet sich das edelste dem Einen.</l>
                    </lg>
                    <lb xml:id="tg1720.3.43"/>
                    <p xml:id="tg1720.3.44">Hierunter ist alles begriffen, was man von der leisesten Neigung bis zur leidenschaftlichsten Raserei nur denken möchte; hier verbinden sich der individuelle Dämon und die verführende Tyche miteinander; der Mensch scheint <pb n="570" xml:id="tg1720.3.44.1"/>
nur sich zu gehorchen, sein eigenes Wollen walten zu lassen, seinem Triebe zu frönen, und doch sind es Zufälligkeiten, die sich unterschieben, Fremdartiges, was ihn von seinem Wege ablenkt; er glaubt zu erhaschen und wird gefangen, er glaubt gewonnen zu haben und ist schon verloren. Auch hier treibt Tyche wieder ihr Spiel, sie lockt den Verirrten zu neuen Labyrinthen, hier ist keine Grenze des Irrens: denn der Weg ist ein Irrtum. Nun kommen wir in Gefahr, uns in der Betrachtung zu verlieren, daß das, was auf das Besonderste angelegt schien, ins Allgemeine verschwebt und zerfließt. Daher will das rasche Eintreten der zwei letzten Zeilen uns einen entscheidenden Wink geben, wie man allein diesem Irrsal entkommen und davor lebenslängliche Sicherheit gewinnen möge.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1720.3.45">Denn nun zeigt sich erst, wessen der Dämon fähig sei; er, der selbständige, selbstsüchtige, der mit unbedingtem Wollen in die Welt griff und nur mit Verdruß empfand, wenn Tyche da oder dort in den Weg trat, er fühlt nun, daß er nicht allein durch Natur bestimmt und gestempelt sei; jetzt wird er in seinem Innern gewahr, daß er sich selbst bestimmen könne, daß er den durchs Geschick ihm zugeführten Gegenstand nicht nur gewaltsam ergreifen, sondern auch sich aneignen und, was noch mehr ist, ein zweites Wesen eben wie sich selbst mit ewiger, unzerstörlicher Neigung umfassen könne.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1720.3.46">Kaum war dieser Schritt getan, so ist durch freien Entschluß die Freiheit aufgegeben; zwei Seelen sollen sich in <hi rend="italic" xml:id="tg1720.3.46.1">einen</hi> Leib, zwei Leiber in <hi rend="italic" xml:id="tg1720.3.46.2">eine</hi> Seele schicken, und indem eine solche Übereinkunft sich einleitet, so tritt zu wechselseitiger liebevoller Nötigung noch eine dritte hinzu; Eltern und Kinder müssen sich abermals zu einem Ganzen bilden, groß ist die gemeinsame Zufriedenheit, aber größer das Bedürfnis. Der aus so viel Gliedern bestehende Körper krankt gemäß dem irdischen Geschick an irgendeinem Teile, und anstatt daß er sich im Ganzen freuen sollte, leidet er am Einzelnen, und dem ohngeachtet wird <pb n="571" xml:id="tg1720.3.46.3"/>
ein solches Verhältnis so wünschenswert als notwendig gefunden. Der Vorteil zieht einen jeden an, und man läßt sich gefallen, die Nachteile zu übernehmen. Familie reiht sich an Familie, Stamm an Stamm, eine Völkerschaft hat sich zusammengefunden und wird gewahr, daß auch dem Ganzen fromme, was der Einzelne beschloß, sie macht den Beschluß unwiderruflich durchs Gesetz; alles, was liebevolle Neigung freiwillig gewährte, wird nun Pflicht, welche tausend Pflichten entwickelt, und damit alles ja für Zeit und Ewigkeit abgeschlossen sei, läßt weder Staat noch Kirche noch Herkommen es an Zeremonien fehlen. Alle Teile sehen sich durch die bündigsten Kontrakte, durch die möglichsten Öffentlichkeiten vor, daß ja das Ganze in keinem kleinsten Teil durch Wankelmut und Willkür gefährdet werde.</p>
                    <lb xml:id="tg1720.3.47"/>
                </div>
                <div type="h4">
                    <head type="h4" xml:id="tg1720.3.50">Αναγκη, Nötigung</head>
                    <lg>
                        <l xml:id="tg1720.3.51">Da ist's denn wieder, wie die Sterne wollten:</l>
                        <l xml:id="tg1720.3.52">Bedingung und Gesetz; und aller Wille</l>
                        <l xml:id="tg1720.3.53">Ist nur ein Wollen, weil wir eben sollten,</l>
                        <l xml:id="tg1720.3.54">Und vor dem Willen schweigt die Willkür stille;</l>
                        <l xml:id="tg1720.3.55">Das Liebste wird vom Herzen weggescholten,</l>
                        <l xml:id="tg1720.3.56">Dem harten Muß bequemt sich Will und Grille.</l>
                        <l xml:id="tg1720.3.57">So sind wir scheinfrei denn nach manchen Jahren</l>
                        <l xml:id="tg1720.3.58">Nur enger dran, als wir am Anfang waren.</l>
                    </lg>
                    <lb xml:id="tg1720.3.59"/>
                    <p xml:id="tg1720.3.60">Keiner Anmerkungen bedarf wohl diese Strophe weiter; niemand ist, dem nicht Erfahrung genugsame Noten zu einem solchen Text darreichte, niemand, der sich nicht peinlich gezwängt fühlte, wenn er nur erinnerungsweise sich solche Zustände hervorruft, gar mancher, der verzweifeln möchte, wenn ihn die Gegenwart also gefangenhält. Wie froh eilen wir daher zu den letzten Zeilen, zu denen jedes feine Gemüt sich gern den Kommentar sittlich und religios zu bilden übernehmen wird.</p>
                    <lb xml:id="tg1720.3.61"/>
                    <pb n="572" xml:id="tg1720.3.62.1"/>
                </div>
                <div type="h4">
                    <head type="h4" xml:id="tg1720.3.64">Ελπις, Hoffnung</head>
                    <lg>
                        <l xml:id="tg1720.3.65">Doch solcher Grenze, solcher ehrnen Mauer</l>
                        <l xml:id="tg1720.3.66">Höchst widerwärt'ge Pforte wird entriegelt,</l>
                        <l xml:id="tg1720.3.67">Sie stehe nur mit alter Felsendauer!</l>
                        <l xml:id="tg1720.3.68">Ein Wesen regt sich leicht und ungezügelt:</l>
                        <l xml:id="tg1720.3.69">Aus Wolkendecke, Nebel, Regenschauer</l>
                        <l xml:id="tg1720.3.70">Erhebt sie uns, mit ihr, durch sie beflügelt,</l>
                        <l xml:id="tg1720.3.71">Ihr kennt sie wohl, sie schwärmt nach allen Zonen;</l>
                        <l xml:id="tg1720.3.72">Ein Flügelschlag – und hinter uns Äonen!</l>
                    </lg>
                    <lb xml:id="tg1720.3.73"/>
                </div>
                <div type="h4">
                    <head type="h4" xml:id="tg1720.3.74">[Nachtrag]</head>
                    <p xml:id="tg1720.3.75">Meiner aufmerksamen kritischen Freunde willen bemerke nur mit wenigem: daß in der ersten Strophe der orphischen Worte ich einiges verändert habe, welchen Varianten ich Beifall wünsche.</p>
                    <pb n="573" xml:id="tg1720.3.76"/>
                </div>
            </div>
        </body>
    </text>
</TEI>