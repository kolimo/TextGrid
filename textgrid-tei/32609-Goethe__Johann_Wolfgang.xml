<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg1743" n="/Literatur/M/Goethe, Johann Wolfgang/Naturwissenschaftliche Schriften/Morphologie/Der Inhalt bevorwortet">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Der Inhalt bevorwortet</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Goethe, Johann Wolfgang: Element 01149 [2011/07/11 at 20:24:11]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <notesStmt>
                <note>
                                 Erstdruck in: Zur Morphologie, 1817.
                              </note>
            </notesStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Goethes Werke. Hamburger Ausgabe in 14 Bänden. Textkritisch durchgesehen und mit Anmerkungen versehen von Erich Trunz, Hamburg: Christian Wegener, 1948 ff. [Seitenkonkordanz zu einer Mischauflage aus den Jahren 1959 und 1960.]</title>
                        <author key="pnd:118540238">Goethe, Johann Wolfgang</author>
                    </titleStmt>
                    <extent>59-</extent>
                    <publicationStmt>
                        <date when="1948"/>
                        <pubPlace>Hamburg</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1749" notAfter="1832"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg1743.3">
                <div type="h4">
                    <head type="h4" xml:id="tg1743.3.1">Der Inhalt bevorwortet</head>
                    <milestone unit="head_start"/>
                    <p rend="zenoPC" xml:id="tg1743.3.2">(Aus: Zur Morphologie, 1817)</p>
                    <milestone unit="head_end"/>
                    <lb xml:id="tg1743.3.3"/>
                    <p xml:id="tg1743.3.4">Von gegenwärtiger Sammlung ist nur gedruckt der Aufsatz über Metamorphose der Pflanzen, welcher, im Jahre 1790 einzeln erscheinend, kalte, fast unfreundliche Begegnung zu erfahren hatte. Solcher Widerwille jedoch war ganz natürlich: die Einschachtelungslehre, der Begriff von Präformation, von sukzessiver Entwickelung des von Adams Zeiten her schon Vorhandenen, hatten sich selbst der besten Köpfe im allgemeinen bemächtigt; auch hatte <hi rend="italic" xml:id="tg1743.3.4.1">Linné</hi> geisteskräftig, bestimmend wie entscheidend, in besonderem Bezug auf Pflanzenbildung, eine dem Zeitgeist gemäßere Vorstellungsart auf die Bahn gebracht.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1743.3.5">Mein redliches Bemühen blieb daher ganz ohne Wirkung, und vergnügt den Leitfaden für meinen eigenen, stillen Weg gefunden zu haben, beobachtete ich nur sorgfältiger das Verhältnis, die Wechselwirkung der normalen und abnormen Erscheinungen, beachtete genau, was Erfahrung <pb n="59" xml:id="tg1743.3.5.1"/>
einzeln, gutwillig hergab, und brachte zugleich einen ganzen Sommer mit einer Folge von Versuchen hin, die mich belehren sollten, wie durch Übermaß der Nahrung die Frucht unmöglich zu machen, wie durch Schmälerung sie zu beschleunigen sei.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1743.3.6">Die Gelegenheit, ein Gewächshaus nach Belieben zu erhellen oder zu verfinstern, benutzte ich, um die Wirkung des Lichts auf die Pflanzen kennenzulernen, die Phänomene des Abbleichens und Abweißens beschäftigten mich vorzüglich, Versuche mit farbigen Glasscheiben wurden gleichfalls angestellt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1743.3.7">Als ich mir genugsame Fertigkeit erworben, das organische Wandeln und Umwandeln der Pflanzenwelt in den meisten Fällen zu beurteilen, die Gestaltenfolge zu erkennen und abzuleiten, fühlte ich mich gedrungen, die Metamorphose der Insekten gleichfalls näher zu kennen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1743.3.8">Diese leugnet niemand: der Lebensverlauf solcher Geschöpfe ist ein fortwährendes Umbilden, mit Augen zu sehen und mit Händen zu greifen. Meine frühere aus mehrjähriger Erziehung der Seidenwürmer geschöpfte Kenntnis war mir geblieben, ich erweiterte sie, indem ich mehrere Gattungen und Arten, vom Ei bis zum Schmetterling, beobachtete und abbilden ließ, wovon mir die schätzenswertesten Blätter geblieben sind.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1743.3.9">Hier fand sich kein Widerspruch mit dem, was uns in Schriften überliefert wird, und ich brauchte nur ein Schema tabellarisch auszubilden, wornach man die einzelnen Erfahrungen folgerecht aufreihen, und den wunderbaren Lebensgang solcher Geschöpfe deutlich überschauen konnte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1743.3.10">Auch von diesen Bemühungen werde ich suchen Rechenschaft zu geben, ganz unbefangen, da meine Ansicht keiner andern entgegensteht.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1743.3.11">Gleichzeitig mit diesem Studium war meine Aufmerksamkeit der vergleichenden Anatomie der Tiere, vorzüglich der Säugetiere zugewandt, es regte sich zu ihr schon ein großes Interesse. <hi rend="italic" xml:id="tg1743.3.11.1">Buffon</hi> und <hi rend="italic" xml:id="tg1743.3.11.2">Daubenton</hi> leisteten viel, <hi rend="italic" xml:id="tg1743.3.11.3">Camper</hi> erschien als Meteor von Geist, Wissenschaft, Talent und Tätigkeit, <hi rend="italic" xml:id="tg1743.3.11.4">Sömmerring</hi> zeigte sich bewundernswürdig, <hi rend="italic" xml:id="tg1743.3.11.5">Merck</hi> wandte sein immer reges Bestreben auf solche <pb n="60" xml:id="tg1743.3.11.6"/>
                        <pb type="start" n="62" xml:id="tg1743.3.11.7"/>Gegenstände; mit allen dreien stand ich im besten Verhältnis, mit Camper briefweise, mit beiden andern in persönlicher, auch in Abwesenheit fortdauernder Berührung.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1743.3.12">Im Laufe der Physiognomik mußte Bedeutsamkeit und Beweglichkeit der Gestalten unsre Aufmerksamkeit wechselsweise beschäftigen, auch war mit <hi rend="italic" xml:id="tg1743.3.12.1">Lavatern</hi> gar manches hierüber gesprochen und gearbeitet worden.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1743.3.13">Später konnte ich mich, bei meinem öftern und längern Aufenthalt in Jena, durch die unermüdliche Belehrungsgabe <hi rend="italic" xml:id="tg1743.3.13.1">Loders</hi>, gar bald einiger Einsicht in tierische und menschliche Bildung erfreuen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1743.3.14">Jene, bei Betrachtung der Pflanzen und Insekten, einmal angenommene Methode leitete mich auch auf diesem Weg: denn bei Sonderung und Vergleichung der Gestalten mußte Bildung und Umbildung auch hier wechselsweise zur Sprache kommen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1743.3.15">Die damalige Zeit jedoch war dunkler als man sich es jetzt vorstellen kann. Man behauptete zum Beispiel, es hange nur vom Menschen ab, bequem auf allen vieren zu gehen, und Bären, wenn sie sich eine Zeitlang aufrecht hielten, könnten zu Menschen werden. Der verwegene Diderot wagte gewisse Vorschläge, wie man ziegenfüßige Faune hervorbringen könne, um solche in Livree, zu besonderm Staat und Auszeichnung, den Großen und Reichen auf die Kutsche zu stiften.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1743.3.16">Lange Zeit wollte sich der Unterschied zwischen Menschen und Tieren nicht finden lassen, endlich glaubte man den Affen dadurch entschieden von uns zu trennen, weil er seine vier Schneidezähne in einem empirisch wirklich abzusondernden Knochen trage, und so schwankte das ganze Wissen, ernst – und scherzhaft, zwischen Versuchen das Halbwahre zu bestätigen, dem Falschen irgendeinen Schein zu verleihen, sich aber dabei in willkürlicher, grillenhafter Tätigkeit zu beschäftigen und zu erhalten. Die größte Verwirrung jedoch brachte der Streit hervor, ob man die Schönheit als etwas Wirkliches, den Objekten Inwohnendes, oder als relativ, konventionell, ja individuell dem Beschauer und Anerkenner zuschreiben müsse.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1743.3.17">Ich hatte mich indessen ganz der Knochenlehre gewidmet; <pb n="62" xml:id="tg1743.3.17.1"/>
denn im Gerippe wird uns ja der entschiedne Charakter jeder Gestalt sicher und für ewige Zeiten aufbewahrt. Ältere und neuere Überbleibsel versammelte ich um mich her, und auf Reisen spähte ich sorgfältig in Museen und Kabinetten nach solchen Geschöpfen, deren Bildung im ganzen oder einzelnen mir belehrend sein könnte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1743.3.18">Hiebei fühlte ich bald die Notwendigkeit einen Typus aufzustellen, an welchem alle Säugetiere nach Übereinstimmung und Verschiedenheit zu prüfen wären, und wie ich früher die Urpflanze aufgesucht, so trachtete ich nunmehr das Urtier zu finden, das heißt denn doch zuletzt: den Begriff, die Idee des Tiers.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1743.3.19">Meine mühselige, qualvolle Nachforschung ward erleichtert, ja versüßt, indem <hi rend="italic" xml:id="tg1743.3.19.1">Herder</hi> die Ideen zur Geschichte der Menschheit aufzuzeichnen unternahm. Unser tägliches Gespräch beschäftigte sich mit den Uranfängen der Wasser – Erde, und der darauf von altersher sich entwicklenden organischen Geschöpfe. Der Uranfang und dessen unablässiges Fortbilden ward immer besprochen und unser wissenschaftlicher Besitz, durch wechselseitiges Mitteilen: und Bekämpfen, täglich geläutert und bereichert.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1743.3.20">Mit andern Freunden unterhielt ich mich gleichfalls auf das lebhafteste über diese Gegenstände, die mich leidenschaftlich beschäftigten, und nicht ohne Einwirkung und wechselseitigen Nutzen blieben solche Gespräche. Ja es ist vielleicht nicht anmaßlich, wenn wir uns einbilden, manches von daher Entsprungene, durch Tradition in der wissenschaftlichen Welt Fortgepflanzte trage nun Früchte, deren wir uns erfreuen, ob man gleich nicht immer den Garten benamset, der die Pfropfreiser hergegeben.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1743.3.21">Gegenwärtig ist bei mehr und mehr sich verbreitender Erfahrung, durch mehr sich vertiefende Philosophie rnanches zum Gebrauch gekommen, was zur Zeit, als die nachstehenden Aufsätze geschrieben wurden, mir und andern unzugänglich war. Man sehe daher den Inhalt dieser Blätter, wenn man sie auch jetzt für überflüssig halten sollte, geschichtlich an, da sie denn als Zeugnisse einer stillen beharrlichen, folgerechten Tätigkeit gelten mögen.</p>
                    <pb n="63" xml:id="tg1743.3.22"/>
                </div>
            </div>
        </body>
    </text>
</TEI>