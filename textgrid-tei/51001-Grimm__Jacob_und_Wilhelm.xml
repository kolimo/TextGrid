<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg517" n="/Literatur/M/Grimm, Jacob und Wilhelm/Märchen/Kinder- und Hausmärchen/176. Die Lebenszeit">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>176. Die Lebenszeit</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Grimm, Jacob und Wilhelm: Element 00373 [2011/07/11 at 20:24:38]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Jacob und Wilhelm Grimm: Kinder- und Hausmärchen. Gesammelt durch die Brüder Grimm, München: Winkler, 1977.</title>
                        <author>Grimm, Jacob und Wilhelm</author>
                    </titleStmt>
                    <extent>723-</extent>
                    <publicationStmt>
                        <date when="1977"/>
                        <pubPlace>München</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg517.2">
                <div type="h4">
                    <head type="h4" xml:id="tg517.2.1">176. Die Lebenszeit.</head>
                    <p xml:id="tg517.2.2">Als Gott die Welt geschaffen hatte und allen Kreaturen ihre Lebenszeit bestimmen wollte, kam der Esel und fragte »Herr, wie lange soll ich leben?« »Dreißig Jahre,« antwortete Gott, »ist dir das recht?« »Ach Herr,« erwiderte der Esel, »das ist eine lange Zeit. Bedenke mein mühseliges Dasein: von Morgen bis in die Nacht schwere Lasten tragen, Kornsäcke in die Mühle schleppen, damit andere das Brot essen, mit nichts als mit Schlägen und Fußtritten ermuntert und aufgefrischt zu werden! erlaß mir einen Teil der langen Zeit.« Da erbarmte sich Gott und schenkte ihm achtzehn Jahre. Der Esel ging getröstet weg, und der Hund erschien. »Wie lange willst du leben?« sprach Gott zu ihm, »dem Esel sind dreißig Jahre zuviel, <pb n="723" xml:id="tg517.2.2.1"/>
du aber wirst damit zufrieden sein.« »Herr,« antwortete der Hund, »ist das dein Wille? bedenke, was ich laufen muß, das halten meine Füße so lange nicht aus; und habe ich erst die Stimme zum Bellen verloren und die Zähne zum Beißen, was bleibt mir übrig, als aus einer Ecke in die andere zu laufen und zu knurren?« Gott sah, daß er recht hatte, und erließ ihm zwölf Jahre. Darauf kam der Affe. »Du willst wohl gerne dreißig Jahre leben?« sprach der Herr zu ihm, »du brauchst nicht zu arbeiten wie der Esel und der Hund, und bist immer guter Dinge.« »Ach Herr,« antwortete er, »das sieht so aus, ist aber anders. Wenns Hirsenbrei regnet, habe ich keinen Löffel. Ich soll immer lustige Streiche machen, Gesichter schneiden, damit die Leute lachen, und wenn sie mir einen Apfel reichen und ich beiße hinein, so ist er sauer. Wie oft steckt die Traurigkeit hinter dem Spaß! Dreißig Jahre halte ich das nicht aus.« Gott war gnädig und schenkte ihm zehn Jahre.</p>
                    <p rend="zenoPLm4n0" xml:id="tg517.2.3">Endlich erschien der Mensch, war freudig, gesund und frisch und bat Gott, ihm seine Zeit zu bestimmen. »Dreißig Jahre sollst du leben,« sprach der Herr, »ist dir das genug?« »Welch eine kurze Zeit!« rief der Mensch, »wenn ich mein Haus gebaut habe, und das Feuer auf meinem eigenen Herde brennt: wenn ich Bäume gepflanzt habe, die blühen und Früchte tragen, und ich meines Lebens froh zu werden gedenke, so soll ich sterben! o Herr, verlängere meine Zeit.« »Ich will dir die achtzehn Jahre des Esels zulegen,« sagte Gott. »Das ist nicht genug,« erwiderte der Mensch. »Du sollst auch die zwölf Jahre des Hundes haben.« »Immer noch zu wenig.« »Wohlan,« sagte Gott, »ich will dir noch die zehn Jahre des Affen geben, aber mehr erhältst du nicht.« Der Mensch ging fort, war aber nicht zufriedengestellt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg517.2.4">Also lebt der Mensch siebenzig Jahr. Die ersten dreißig sind seine menschlichen Jahre, die gehen schnell dahin; da ist er gesund, heiter, arbeitet mit Lust und freut sich seines Daseins. Hierauf folgen die achtzehn Jahre des Esels, da wird ihm eine Last nach der andern aufgelegt: er muß das Korn tragen, das andere nährt, und Schläge und Tritte sind der Lohn seiner treuen Dienste. Dann kommen die zwölf Jahre des Hundes, da liegt er in den Ecken, knurrt und hat keine Zähne mehr zum <pb n="724" xml:id="tg517.2.4.1"/>
Beißen. Und wenn diese Zeit vorüber ist, so machen die zehn Jahre des Affen den Beschluß. Da ist der Mensch schwachköpfig und närrisch, treibt alberne Dinge und wird ein Spott der Kinder.</p>
                </div>
            </div>
        </body>
    </text>
</TEI>