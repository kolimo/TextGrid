<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg1248" n="/Literatur/M/Arnim, Ludwig Achim von/Erzählungen/Der Wintergarten/Fünfter Winterabend/Die neuen Amazonen">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Die neuen Amazonen</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Arnim, Ludwig Achim von: Element 01057 [2011/07/11 at 20:27:48]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Achim von Arnim: Sämtliche Romane und Erzählungen. Auf Grund der Erstdrucke herausgegeben von Walther Migge, Bde. 1–3, München: Carl Hanser Verlag, 1962–1965.</title>
                        <author key="pnd:118504177">Arnim, Ludwig Achim von</author>
                    </titleStmt>
                    <extent>249-</extent>
                    <publicationStmt>
                        <date notBefore="1962" notAfter="1965"/>
                        <pubPlace>München</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1781" notAfter="1831"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg1248.3">
                <div type="h4">
                    <head type="h4" xml:id="tg1248.3.1">Die neuen Amazonen</head>
                    <p xml:id="tg1248.3.2">»Ich kann mir die Götter, wie sie auf Erden wandelten, nicht anders als auf Schlittschuhen denken, diese höchste Geschwindigkeit mit leichtester Mühe ist ihrer allein würdig!« sagte eintretend die Gesunde; mit ihren roten Backen und hellen Augen schien sie aus dem grünen Helme wie eine prächtige Frucht. – »Und Sie wären doch auch der Götter wie der Menschen Siegerin geblieben, gleich Amor«, meinte der Gesandte. – »Warhhaftig Sie haben heute alle Liebhaber in dem Ringelrennen auf dem Eise überwunden. Heute tat mir zum erstenmal mein hölzerner Fuß wehe, daß ich keinen Gang mit Ihnen machen konnte meine schöne Amazone, denn das ist geringe Ehre, das jetzige Männergeschlecht zu überwinden, wo die Besten vor dem Feinde erschlagen sind«, sagte der Invalide. – »Und nun werden lauter Mädchen geboren, sehn Sie nur die Zeitungsanzeigen, es muß ein Amazonenreich entstehen und Sie meine schöne Schlittschuhläuferin werden Königin.« – Die neue Amazone antwortete dem Gesandten, der ihr das sagte: »Sie erfinden mir da nichts Neues, schon in meiner Kindheit war dies mein einziger Gedanke, und späterhin, als wir die Tyrannei der Männer kennen lernten, machte ich mit vielen Mädchen einen Bund, unverheiratet zu bleiben; sie sind aber alle abtrünnig geworden.« – Das Gespräch wurde durch einen Wagen unterbrochen, der im lockeren Schnee heranpfiff, unsere Abendunterhaltung hatte mit diesen Einfällen eine prophetische Berührung, was sich wohl öfter fände, wenn man öfter acht gebe.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1248.3.3">»Hören Sie wie die Räder pfeifen; die Pfeifenbegleitung der <pb n="249" xml:id="tg1248.3.3.1"/>
Schottentänze, scheint mir darnach komponiert.« – »Aber sehen Sie doch, wer da aussteigt mit unsrer Kranken! Wahrhaftig ein Lord«, sagte der Invalide, »ihr schönen Kinder legt euer bestes Gesicht an, schüttelt eure Locken auseinander, setzt euch hin als wenn ihr gar nicht Acht gebet auf den, der hereintritt; Ihnen mein gesundes Fräulein, kleidet ein wenig Melancholie sehr gut. Ist alles in Ordnung? Da ist er!« – Die Kranke trat am Arme eines jungen Mannes in das Zimmer, dessen Kleidung ganz Modenjournal, das Gesicht aber viel reichhaltiger und voll Weltkenntnis war. – »Sag, Liebe, bist du verheiratet?« rief ihr die Frau vom Hause entgegen, »hab ich das Vergnügen deinen Mann bei mir zu sehen?« – »Beinahe getroffen«, antwortete sie, »für diesen Abend haben wir alles Unglück als gute Eheleute mit einander geteilt. Den Namen muß ich noch verschweigen, mein guter Freund ist aber ein Musterreiter, und so will er genannt sein.« – Nach einigen Bewillkommnungen fuhr sie ungewöhnlich beredt fort: »Ohne diesen meinen Freund hätte ich heute aus Langeweile Krämpfe bekommen; denkt euch, wohl dreißig meist junge Leute beisammen zu einem langen Mittagsessen und keine einzige Intrige, keine Heimlichkeiten, keine Auswahl von Plätzen, und auch in den drei Stunden entstand nichts der Art, als zwischen uns beiden. Es ist doch ein Elend mit der jetzigen Jugend, daß sie sich alles so bequem macht, was wollen daraus für alte Leute werden. Die Paare, die sich für verliebt in einander ausgaben, waren auch schon versprochen und benutzten ihre Erlaubnis zu küssen so ungemessen, daß einem das Herz davon wehe tat. Und dabei denkt euch das ewige Reden von Schauspielern, und das ohne alle Bewunderung, ohne alle Bosheit, ohne allen Witz, bloß so in beurteilender Kritik; nein ohne meinen Freund wäre ich ohnmächtig geworden unter diesen halben Stimmen, die ihre Worte kaum selbst wert achteten verständlich ausgesprochen zu werden. Und dann wieder dieses nachgeäffte Französisch, dem nicht der französische Ton, aber wohl der Geist fehlte. Seht, zu meinem Glück saß dieser Herr bei mir, sieht er nicht recht frisch aus? Doch das war's nicht; er erzählte mir frisch fort seine seltsamsten Abenteuer, allen Skandal in seiner Familie, zeigte mir Briefe seiner alten Freundinnen: es ist ein Engel, und heute Abend muß er uns eine Entführungsgeschichte aus England aus seinem Tagebuche vorlesen, die ganz himmlisch ist, so was geht doch bei uns <pb n="250" xml:id="tg1248.3.3.2"/>
nicht vor; ehe sich unsre jungen Leute mit kahlen Platten zu einer Entführung entschlössen, müßte auf jeder Poststation eine kalte Pastete und echter Bordeaux vorrätig sein.« – Wir drangen natürlich in ihn, uns diese Geschichte vorzulesen, er schlug es erst ab und tat es dann, alles mit vielem Anstande. Er verschwieg Tag und Jahr seines Reisetagebuchs.</p>
                </div>
            </div>
        </body>
    </text>
</TEI>