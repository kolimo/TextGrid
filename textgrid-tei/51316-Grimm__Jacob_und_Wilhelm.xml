<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg833" n="/Literatur/M/Grimm, Jacob und Wilhelm/Sagen/Deutsche Sagen/Erster Band/268. Frau Berta oder die weiße Frau">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>268. Frau Berta oder die weiße Frau</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Grimm, Jacob und Wilhelm: Element 00693 [2011/07/11 at 20:24:38]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Jacob und Wilhelm Grimm: Deutsche Sagen. Zwei Bände in einem Band. Vollständige Ausgabe nach dem Text der dritten Auflage von 1891, mit der Vorrede der Brüder Grimm zur ersten Auflage 1816 und 1818 und mit einer Vorbemerkung von Hermann Grimm. Nachwort von Lutz Röhrich, München: Winkler, [1965].</title>
                        <author>Grimm, Jacob und Wilhelm</author>
                    </titleStmt>
                    <extent>267-</extent>
                    <publicationStmt>
                        <date when="1965"/>
                        <pubPlace>München</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg833.2">
                <div type="h4">
                    <head type="h4" xml:id="tg833.2.1">268. Frau Berta oder die weiße Frau</head>
                    <p xml:id="tg833.2.2">
                        <hi rend="spaced" xml:id="tg833.2.2.1">Die weiße Frau</hi> erscheint in den Schlössern mehrerer fürstlichen Häuser, namentlich zu Neuhaus in Böhmen, zu Berlin, Bayreuth, Darmstadt und Karlsruhe und in allen, deren Geschlechter nach und nach durch Verheiratung mit dem ihren verwandt geworden sind. Sie tut niemanden zuleide, neigt ihr Haupt, vor wem sie begegnet, spricht nichts, und ihr Besuch bedeutet einen nahen Todesfall, manchmal auch etwas Fröhliches, wenn sie nämlich keine schwarzen Handschuh anhat. Sie trägt ein Schlüsselbund und eine weiße Schleierhaube. Nach einigen soll sie im Leben <hi rend="spaced" xml:id="tg833.2.2.2">Perchta von Rosenberg</hi> geheißen, zu Neuhaus in Böhmen gewohnt haben und mit Johann von Lichtenstein, einem bösen, störrischen Mann, vermählt gewesen sein. Nach ihres Gemahls Tode lebte sie in Witwenschaft zu Neuhaus und fing an, zu großer Beschwerde ihrer Untertanen,<pb n="267" xml:id="tg833.2.2.3"/>
 die ihr frönen mußten, ein Schloß zu bauen. Unter der Arbeit rief sie ihnen zu, fleißig zu sein: »Wann das Schloß zustand sein wird, will ich euch und euren Leuten einen süßen Brei vorsetzen«, denn dieser Redensart bedienten sich die Alten, wenn sie jemand zu Gast luden. Den Herbst nach Vollendung des Baues hielt sie nicht nur ihr Wort, sondern stiftete auch, daß auf ewige Zeiten hin alle Rosenbergs ihren Leuten ein solches Mahl geben sollten. Dieses ist bisher fort geschehen<ref type="noteAnchor" target="#tg833.2.4">
                            <anchor xml:id="tg833.2.2.4" n="/Literatur/M/Grimm, Jacob und Wilhelm/Sagen/Deutsche Sagen/Erster Band/268. Frau Berta oder die weiße Frau#Fußnote_1"/>
                            <ptr cRef="/Literatur/M/Grimm, Jacob und Wilhelm/Sagen/Deutsche Sagen/Erster Band/268. Frau Berta oder die weiße Frau#Fußnoten_1"/>
                            <hi rend="superscript" xml:id="tg833.2.2.5.1">1</hi>
                        </ref>, und unterbleibt es, so erscheint sie mit zürnenden Mienen. Zuweilen soll sie in fürstliche Kinderstuben nachts, wenn die Ammen Schlaf befällt, kommen, die Kinder wiegen und vertraulich umtragen. Einmal, als eine unwissende Kinderfrau erschrocken fragte: »Was hast du mit dem Kinde zu schaffen?« und sie mit Worten schalt, soll sie doch gesagt haben: »Ich bin keine Fremde in diesem Hause wie du, sondern gehöre ihm zu; dieses Kind stammt von meinen Kindeskindern. Weil ihr mir aber keine Ehre erwiesen habt, will ich nicht mehr wieder einkehren.«</p>
                </div>
                <div type="footnotes">
                    <head type="h4" xml:id="tg833.2.3">Fußnoten</head>
                    <note xml:id="tg833.2.4.note" target="#tg833.2.2.4">
                        <p xml:id="tg833.2.4">
                            <anchor xml:id="tg833.2.4.1" n="/Literatur/M/Grimm, Jacob und Wilhelm/Sagen/Deutsche Sagen/Erster Band/268. Frau Berta oder die weiße Frau#Fußnoten_1"/>
                            <ref cRef="/Literatur/M/Grimm, Jacob und Wilhelm/Sagen/Deutsche Sagen/Erster Band/268. Frau Berta oder die weiße Frau#Fußnote_1" xml:id="tg833.2.4.2">1</ref> Der Brei wird aus Erbsen und Heidegrütz gekocht, auch jedesmal Fische dazugegeben.</p>
                    </note>
                </div>
            </div>
        </body>
    </text>
</TEI>