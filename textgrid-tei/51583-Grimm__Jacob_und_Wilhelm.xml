<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg1100" n="/Literatur/M/Grimm, Jacob und Wilhelm/Sagen/Deutsche Sagen/Zweiter Band/534. Freiherr Albrecht von Simmern">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>534. Freiherr Albrecht von Simmern</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Grimm, Jacob und Wilhelm: Element 00961 [2011/07/11 at 20:24:38]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Jacob und Wilhelm Grimm: Deutsche Sagen. Zwei Bände in einem Band. Vollständige Ausgabe nach dem Text der dritten Auflage von 1891, mit der Vorrede der Brüder Grimm zur ersten Auflage 1816 und 1818 und mit einer Vorbemerkung von Hermann Grimm. Nachwort von Lutz Röhrich, München: Winkler, [1965].</title>
                        <author>Grimm, Jacob und Wilhelm</author>
                    </titleStmt>
                    <extent>514-</extent>
                    <publicationStmt>
                        <date when="1965"/>
                        <pubPlace>München</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg1100.2">
                <div type="h4">
                    <head type="h4" xml:id="tg1100.2.1">534. Freiherr Albrecht von Simmern</head>
                    <p xml:id="tg1100.2.2">Albrecht Freiherr von Simmern war bei seinem Landesherrn Herzog Friedrich von Schwaben, der ihn auferzogen hatte, wohlgelitten und stand in besonderer Gnade. Einstmals tat dieser in der Begleitung seiner Grafen und Ritter, unter welchen sich auch der Freiherr Albrecht befand, einen Lustritt zu dem Grafen Erchinger, bei dem er schon öfter gewesen und dessen <pb n="514" xml:id="tg1100.2.2.1"/>
Schloß Mogenheim im Zabergau lag. Der Graf war ein Mann von fröhlichem Gemüte, der Jagd und andern ehrlichen Übungen ergeben. Mit seiner Frau, Maria von Tübing, hatte er nur zwei Töchter und keinen Sohn erzeugt, und sein gräflicher Stamm drohte zu erlöschen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1100.2.3">Nahe an dem Schlosse lag ein lustiges Gehölz, der Stromberg genannt; darin lief seit langer Zeit ein ansehnlicher großer Hirsch, den weder die Jäger noch Hofbediente je hatten fahen können. Als er sich eben jetzt wieder sehen ließ, freuten sich alle, besonders der Graf Erchinger, welcher die übrige Gesellschaft aufmahnte, sich mit dem gewöhnlichen Jägerzeuge dahin zu begeben. Unter dem Jagen kam der Freiherr Albrecht von den andern ab in eine besondere Gegend des Waldes, wo er eines großen und schönen Hirsches ansichtig ward, wie er noch nie glaubte einen gesehen zu haben. Er setzte ihm lange durch den Wald nach, bis er ihn ganz aus dem Gesicht verlor und er nicht wußte, wo das Tier hingeraten war.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1100.2.4">Indem trat ein Mann schrecklicher Gestalt vor ihn, und ob er gleich sonst beherzt und tapfer war, so entsetzte er sich doch heftig und wahrte sich wider ihn mit dem Zeichen des Kreuzes. Der Mann aber sprach: »Fürchte dich nicht! Ich bin von Gott gesandt, dir etwas zu offenbaren. Folge mir nach, so sollst du wunderbare Dinge sehen, wie sie deine Augen noch nie erblickt haben, und soll dir kein Haar dabei gekrümmt werden.« Der Freiherr willigte ein und folgte seinem Führer, der ihn aus dem Walde leitete. Als sie heraustraten, deuchte ihm, er sehe schöne Wiesen und eine überaus lustige Gegend. Ferner ein Schloß, das mit vielen Türmen und anderer Zier so prangte, daß dergleichen seine Augen niemals gesehen. Indem sie sich diesem Schlosse nahten, kamen viele Leute, gleich als Hofdiener, entgegen. Keiner aber redete ein Wort, sondern als er bei dem Tor anlangte, nahm einer sein Pferd ab, als wollte er es unterdessen halten. Sein Führer aber sprach: »Laß dich ihr Schweigen nicht befremden; dagegen rede auch nicht mit ihnen, sondern allein mit mir und tue in allem, wie ich dir sagen werde.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg1100.2.5">Nun traten sie ein, und Herr Albrecht ward in einen großen, schönen Saal geführt, wo ein Fürst mit den Seinigen zu Tische saß. Alle standen auf und neigten sich ehrerbietig, gleich als <pb n="515" xml:id="tg1100.2.5.1"/>
wollten sie ihn willkommen heißen. Darauf setzten sie sich wieder und taten, als wenn sie äßen und tränken. Herr Albrecht blieb stehen, hielt sein Schwert in der Hand und wollte es nicht von sich lassen; indessen betrachtete er das wunderköstliche silberne Tafelgeschirr, darin die Speisen auf- und abgetragen wurden, samt den andern vorhandenen Gefäßen. Alles dieses geschah mit großem Stillschweigen; auch der Herr und seine Leute aßen für sich und bekümmerten sich nicht um ihn. Nachdem er also lange gestanden und alles angeschaut, erinnerte ihn der, welcher ihn hergeführt, daß er sich vor dem Herrn neigen und dessen Leute grüßen solle; dann wolle er ihn wieder herausgeleiten. Als er das getan, stand der Herr mit allen seinen Leuten wiederum höflich auf, und sie neigten gleichfalls ihre Häupter gegen ihn. Darauf ward Herr Albrecht von seinem Führer zu der Schloßpforte gebracht. Hier stellten diejenigen, welche bisher sein Pferd gehalten, ihm selbes wieder zu, legten ihm aber dabei Stillschweigen auf; worauf sie ins Schloß zurückkehrten. Nun gürtete Herr Albrecht sein Schwert wieder an und ward von seinem Gefährten auf dem vorigen Wege nach dem Stromberger Walde gebracht. Er fragte ihn, was das für ein Schloß und wer dessen Einwohner wären, die darin zur Tafel gesessen. Der Geist antwortete: »Der Herr, welchen du gesehen, ist deines Vaters Bruder gewesen, ein gottesfürchtiger Mann, welcher vielmals wider die Ungläubigen gefochten. Ich aber und die andern, die du gesehen, waren bei Leibes Leben seine Diener und müssen nun unaussprechlich harte Pein leiden. Er hat bei Lebzeiten seine Untertanen mit unbilligen Auflagen sehr gedrückt und das Geld zum Krieg gegen die Ungläubigen angewendet; wir andern aber haben ihm dazu Rat und Anschläge gegeben und werden jetzt solcher Ungerechtigkeit willen hart gestraft. Dieses ist deiner Tugenden wegen offenbart, damit du vor solchen und ähnlichen Dingen dich hüten und dein Leben bessern mögest. Siehe, da ist der Weg, welcher dich wiederum durch den Wald an deinen vorigen Ort bringen wird; doch kannst du noch einmal zurückkehren, damit du siehest, in was für Elend und Jammer sich die vorige Glückseligkeit verkehrt hat.« Wie der Geist dieses gesagt, war er verschwunden. Herr Albrecht aber kehrte wieder zu dem Schlosse zurück. <pb n="516" xml:id="tg1100.2.5.2"/>
Siehe, da war alles miteinander zu Feuer, Pech und Schwefel worden, davon ihm der Geruch entgegenqualmte; dabei hörte er ein jammervolles Schreien und Klagen, worüber er sich so sehr entsetzte, daß ihm die Haare zu Berge stunden. Darum wendete er schnell sein Pferd um und ritt des vorigen Weges wieder nach seiner Gesellschaft zu.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1100.2.6">Als er anlangte, kam er allen so verändert und verstellet vor, daß sie ihn fast nicht erkannten. Denn ungeachtet er noch ein junger und frischer Mann war, hatte ihn doch Schrecken und Bestürzung zu einem eisgrauen umgestaltet, indem Haupthaar und Bart weiß wie der Schnee waren. Sie verwunderten sich zwar darüber nicht wenig, aber noch mehr über die durch seine veränderte Gestalt beglaubigte Erzählung, so daß sie insgesamt traurig nach Hause umkehrten.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1100.2.7">Der Freiherr von Simmern beschloß, an dem Orte, wo sich das zugetragen, zur Ehre Gottes eine Kirche zu erbauen. Graf Erchinger, auf dessen Gebiet er lag, gab gern seine Einwilligung, und er und seine Gemahlin versprachen Rat und Hilfe, damit daselbst ein Frauenkloster aufgerichtet und Gott stets gedienet würde. Auch der Herzog Friedrich von Schwaben verhieß seinen Beistand zur Förderung des Baues und hat verschiedene Zehnten und Einkünfte dazu verordnet. Die Geschichte hat sich im Jahr 1134 unter Lothar II. begeben.</p>
                </div>
            </div>
        </body>
    </text>
</TEI>