<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg308" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Der Sagenschatz des Königreichs Sachsen/Erster Band/289. Sprüche von der Stadt Freiberg">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>289. Sprüche von der Stadt Freiberg</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Grässe, Johann Georg Theodor: Element 00314 [2011/07/11 at 20:29:59]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Johann Georg Theodor Grässe: Der Sagenschatz des Königreichs Sachsen. Band 1, 2. Aufl., Dresden: Schönfeld, 1874.</title>
                        <author key="pnd:104076534">Grässe, Johann Georg Theodor</author>
                    </titleStmt>
                    <extent>264-</extent>
                    <publicationStmt>
                        <date>1874</date>
                        <pubPlace>Dresden</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1814" notAfter="1885"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg308.2">
                <div type="h4">
                    <head type="h4" xml:id="tg308.2.1">289) Sprüche von der Stadt Freiberg.</head>
                    <p xml:id="tg308.2.2">Die Stadt Freiberg ist nicht blos durch ihren reichen Bergsegen, sondern auch durch die Schönheit ihrer Lage berühmt gewesen; davon sagt ein altes Sprichwort (bei Knauth, <hi rend="italic" xml:id="tg308.2.2.1">Prodr. Misn.</hi> S. 172): wenn Leipzig mein wäre, wollte ich es in Freiberg verzehren. Obgleich das Freiberger Bier zwar keinen besondern Namen hatte, wie es im 16. und 17. Jahrhundert Mode war<ref type="noteAnchor" target="#tg308.2.12">
                            <anchor xml:id="tg308.2.2.2" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Der Sagenschatz des Königreichs Sachsen/Erster Band/289. Sprüche von der Stadt Freiberg#Fußnote_1"/>
                            <ptr cRef="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Der Sagenschatz des Königreichs Sachsen/Erster Band/289. Sprüche von der Stadt Freiberg#Fußnoten_1"/>
                            <hi rend="superscript" xml:id="tg308.2.2.3.1">1</hi>
                        </ref>, gab es doch zu einem andern Sprichworte Gelegenheit. Dieses hieß: es kitzelt einem in der Nase, wie das Freiberger Bier. Ein anderes Sprüchlein, daß sich zugleich mit auf zwei andere Städte Sachsens bezieht und deren Untergang prophezeit, lautet traurig genug also:</p>
                    <lb xml:id="tg308.2.3"/>
                    <pb n="264" xml:id="tg308.2.4"/>
                    <milestone unit="hi_start"/>
                    <p xml:id="tg308.2.5">
                        <seg rend="zenoTXFontsize80" xml:id="tg308.2.5.1">Meißen wird ertrinken,</seg>
                    </p>
                    <p xml:id="tg308.2.6">
                        <seg rend="zenoTXFontsize80" xml:id="tg308.2.6.1">Freiberg wird versinken,</seg>
                    </p>
                    <p xml:id="tg308.2.7">
                        <seg rend="zenoTXFontsize80" xml:id="tg308.2.7.1">Dresen</seg>
                    </p>
                    <p xml:id="tg308.2.8">
                        <seg rend="zenoTXFontsize80" xml:id="tg308.2.8.1">Wird man zusammenkehren mit Besen,</seg>
                    </p>
                    <milestone unit="hi_end"/>
                    <lb xml:id="tg308.2.9"/>
                    <p xml:id="tg308.2.10">allein glücklicher Weise ist diese böse Prophezeihung noch bei keinem der genannten Orte wahr geworden, wiewohl das theilweise Eintreffen derselben bei dem fast ganz durch den Bergbau unterminirten Freiberg nicht gerade zu den Unmöglichkeiten gehören würde.</p>
                </div>
                <div type="footnotes">
                    <head type="h4" xml:id="tg308.2.11">Fußnoten</head>
                    <note xml:id="tg308.2.12.note" target="#tg308.2.2.2">
                        <p xml:id="tg308.2.12">
                            <anchor xml:id="tg308.2.12.1" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Der Sagenschatz des Königreichs Sachsen/Erster Band/289. Sprüche von der Stadt Freiberg#Fußnoten_1"/>
                            <ref cRef="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Der Sagenschatz des Königreichs Sachsen/Erster Band/289. Sprüche von der Stadt Freiberg#Fußnote_1" xml:id="tg308.2.12.2">1</ref> Ein Verzeichniß solcher curioser Biernamen s. <hi rend="italic" xml:id="tg308.2.12.3">Curiosa Sax.</hi> 1753 <hi rend="italic" xml:id="tg308.2.12.4">p.</hi> 315. Iccander, Sächs. Kernchron. CXLIV. Paquet S. 1018. Klemm, Allg. Culturwiss. Bd. II. S. 332 <hi rend="italic" xml:id="tg308.2.12.5">sq.</hi> u. in meinen Bierstudien (Dresd. 1872). S. 68 fgg.</p>
                    </note>
                </div>
            </div>
        </body>
    </text>
</TEI>