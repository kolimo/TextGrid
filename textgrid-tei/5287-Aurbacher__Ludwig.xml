<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg43" n="/Literatur/M/Aurbacher, Ludwig/Märchen und Sagen/Ein Volksbüchlein/Erster Theil/2. Allerlei erbauliche und ergötzliche Historien/22. Eine häusliche Scene; oder: Der alte Gott lebt noch">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>22. Eine häusliche Scene; oder: Der alte Gott lebt noch</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Aurbacher, Ludwig: Element 00036 [2011/07/11 at 20:24:25]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Ludwig Aurbacher: Ein Volksbüchlein. Enthaltend: Die Geschichte des ewigen Juden [...]. Aus dem Nachlaß verm. und m. e. Nachw. ed. Joseph Sarreiter, Band 1, Leipzig: Reclam, [um 1878/79].</title>
                        <author key="pnd:11922349X">Aurbacher, Ludwig</author>
                    </titleStmt>
                    <extent>75-</extent>
                    <publicationStmt>
                        <date notBefore="1878" notAfter="1879"/>
                        <pubPlace>Leipzig</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1784" notAfter="1847"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg43.2">
                <div type="h4">
                    <head type="h4" xml:id="tg43.2.1">22. Eine häusliche Scene; oder: Der alte Gott lebt noch.</head>
                    <p xml:id="tg43.2.2">Es war eines Sonntags Morgen. Die Sonne schien hell und warm in die Stube; linde erquickliche Lüste zogen durch die offenen Fenster, im Freien unter dem blauen Himmel jubilirten die Vögel; und die ganze Landschaft, in Grün gekleidet und mit Blumen geschmückt, stand da, wie eine Braut an ihrem Ehrentage. Aber während nun draußen überall Freude herrschte, brütete im Hause, in jener Stube nur Trübsal und Trauer. Selbst die Hausfrau, die sonst immer eines heitern und guten Muthes war, saß <pb n="75" xml:id="tg43.2.2.1"/>
heute mit umwölktem Antlitz und mit niedergeschlagenem Blicke da beim Morgenimbiß, und sie erhob sich zuletzt, ohne etwas zu essen, vom Sitze, und eine Thräne aus dem Auge wischend, eilte sie gegen die Thür zu. – Es schien aber auch in der That, als wenn der Fluch auf diesem Hause lastete. Es war Theurung im Lande; das Gewerbe ging schlecht; die Auflagen wurden immer drückender; das Hauswesen verfiel von Jahr zu Jahr mehr, und es war am Ende nichts abzusehen, als Armuth und Verachtung. Das hatte den Mann, der sonst ein fleißiger und ordentlicher Bürger war, schon seit langer Zeit trübsinnig gemacht; dergestalt, daß er an seinem fernern Fortkommen verzweifelte, und manch mal sogar äußerte, er wolle sich selbst Leids anthun, und seinem elenden trostlosen Leben ein Ende machen. Da half denn auch kein Zureden von Seiten seiner Frau, die sonst immer aufgeräumten Sinnes war, und alle Trostgründe seiner Freunde, weltliche und geistliche, verschlugen nichts, und machten ihn nur schweigsamer und trübseliger. – Der geneigte Leser wird denken, da sei es kein Wunder gewesen, daß denn zuletzt auch die Frau all ihren Muth und Freude verloren hat. Es hatte aber mit ihrer Traurigkeit eine ganz eigene Bewandtniß, wie wir bald hören werden. Als der Mann sah, daß auch sein Weib trauerte und nun forteilte, hielt er sie an, und sprach: Ich laß dich nicht aus der Stube, bis du mir sagst, was dir fehle. Sie schwieg noch eine Weile, dann aber that sie den Mund auf, und indem sie einen tiefen Seufzer holte, sprach sie: Ach, lieber Mann! es hat mir heute Nacht geträumt, unser lieber Herrgott sei gestorben, und die lieben Engelein seien ihm zur Leiche gegangen. Einfalt! sagte der Mann, wie kannst du denn so etwas Albernes für wahr halten oder auch nur denken? Herzlieb! bedenk doch, Gott kann ja nicht sterben. Da erheiterte sich plötzlich das Gesicht der guten Frau, und indem sie des Mannes beide Hände erfaßte und zärtlich <pb n="76" xml:id="tg43.2.2.2"/>
drückte, sagte sie: Also lebt er noch, der alte Gott? Ja freilich! sprach der Mann. Wer wollte denn daran zweifeln? Da umfing sie ihn, und sah ihn an mit ihren holdseligen Augen, aus denen Zuversicht und Friede und Freudigkeit strahlte, und sie sprach: Ei nun, Herzensmann, wenn der alte Gott noch lebt, warum glauben und vertrauen wie denn nicht auf ihn – er, der unsere Haare gezählt hat, und nicht zuläßt, daß eines ohne sein Wissen ausfalle, der die Lilien des Feldes bekleidet, und die Sperlinge ernährt, und die jungen Raben, die nach Futter schreien? – Bei diesen Worten geschah es dem Manne, als fielen ihm plötzlich Schuppen vom Auge, und als lösete sich das Eis, das sich um sein Herz gelegt hatte. Und er lächelte zum ersten Male wieder nach langer Zeit; und er dankte seinem frommen lieben Weibe für die List, die sie angewandt, um seinen todten Glauben an Gott zu beleben, und das Zutrauen zu ihm hervorzurufen. Und die Sonne schien nun noch freundlicher in die Stube auf das Antlitz zufriedener Menschen, und die Lüfte wehten erquicklicher um ihre verklärten Wangen, und die Vögel jubilirten noch lauter in dem Dank ihrer Herzen gegen Gott.</p>
                </div>
            </div>
        </body>
    </text>
</TEI>