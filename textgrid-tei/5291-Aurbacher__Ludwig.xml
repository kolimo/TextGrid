<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg47" n="/Literatur/M/Aurbacher, Ludwig/Märchen und Sagen/Ein Volksbüchlein/Erster Theil/2. Allerlei erbauliche und ergötzliche Historien/26. Das Testament des Vaters">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>26. Das Testament des Vaters</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Aurbacher, Ludwig: Element 00040 [2011/07/11 at 20:24:25]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Ludwig Aurbacher: Ein Volksbüchlein. Enthaltend: Die Geschichte des ewigen Juden [...]. Aus dem Nachlaß verm. und m. e. Nachw. ed. Joseph Sarreiter, Band 1, Leipzig: Reclam, [um 1878/79].</title>
                        <author key="pnd:11922349X">Aurbacher, Ludwig</author>
                    </titleStmt>
                    <extent>81-</extent>
                    <publicationStmt>
                        <date notBefore="1878" notAfter="1879"/>
                        <pubPlace>Leipzig</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1784" notAfter="1847"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg47.2">
                <div type="h4">
                    <head type="h4" xml:id="tg47.2.1">26. Das Testament des Vaters.</head>
                    <p xml:id="tg47.2.2">Ein Vater hatte drei lasterhafte Söhne. Der eine war ein Buhler, der andere ein Spieler, der dritte ein Säufer. Als er nun zum Sterben kam, verordnete er in seinem Testamente: daß derjenige, der unter ihnen als lasterhaft befunden würde, von den andern enterbt werden sollte. Da entstand denn großer Streit unter ihnen. Jeder warf dem andern sein Laster vor, und jeder entschuldigte das seinige, und keiner wollte am wenigsten vom Erbtheil lassen, das ihm ohnedies zugestanden wäre. Endlich, als sie auf diese Art kein Ende des Streites vor Augen sahen, so beschlossen sie, das Urtheil einem Manne zu übertragen, der als ein weiser und unbescholtener Schiedsrichter in der Stadt bekannt war. Dieser, nachdem er ihren Vortrag angehört, erklärte rundweg: er wisse nicht, welches Laster größer, vor Gott sündhafter und des Menschen Seele und Leib verderblicher wäre, ob die Buhlerei, oder die Spielsucht oder die Trunkenheit. Und er suchte ihnen nun, einem jeden, <pb n="81" xml:id="tg47.2.2.1"/>
das Schändliche und Schädliche seines Thuns und Treibens vorzustellen. Der Buhler und der Spieler ließen sich auch bald berichten, und sie vermochten zuletzt zur Entschuldigung nichts vorzubringen, als ihre Jugend und ihren Leichtsinn. Nur der Säufer suchte allerlei Einreden zu machen, und hängte seinem Laster ein Mäntelein um, daß es ein ganz unschuldiges Ansehen gewann. Da nahm der Richter das Wort und sprach zu demselben: Ich will dir eine Geschichte erzählen, woraus erhellt, wie ein gar großes Laster die Trunkenheit sei. Es lebte zu einer Zeit ein Mann, der Haus und Hof verlassen hatte, und in die Einöde gekommen war, um da Gott zu dienen in Armuth, bei Arbeit und Gebet. Und er brachte es auch gar bald zu einem solch heiligen Sinne und Leben, daß er sich keiner unlautern Regung, keines sündhaften Gedankens mehr bewußt war. Aber der alte Feind, der nie ruhet, beschlich zuletzt doch sein Herz, das freilich nicht leer war von Stolz und Dünkel. Er erschien ihm im Gewande eines Engels des Lichts, und sagte: du dünkest dich hoch in Gnaden bei Gott, aber du fühlest nicht, wie tief du stehest, tiefer noch als jene, deren Sünden du meidest. Weißt du nicht, daß einer, der in eine Sünde fällt und wieder aufsteht, vor Gott angenehmer ist, als neun und neunzig Gerechte? Willst du also das höchste Heil erwerben, so mußt du eine schwere Sünde begehen. Ich nenne dir aber deren drei, die von Belang sind: den Todtschlag, den Diebstahl und die Trunkenheit. Der fromme Mann, als er dies gehört, entsetzte sich über den Antrag, und schlug ihn, so viel er vermochte, aus dem Sinne. Aber von der Zeit an empfand er Unruhe, und er konnte sie nicht bewältigen; denn der lüsterne Gedanke, durch die Sünde zur höchsten Gnade zu gelangen, stak in seinem Herzen wie ein Stachel, der ihn peinigte. – Nun geschah es eines Tags, daß bei finsterer Nacht, während es draußen stürmte und regnete, ein Wanderer zu der einsamen Zelle kam, und um Obdach bat. <pb n="82" xml:id="tg47.2.2.2"/>
Es war ein Kaufherr, der sich in dem Walde verirrt hatte, und mit seinem ermüdeten Pferde nicht mehr des Weges weiter konnte. Der Einsiedler öffnete, und der fremde Mann trug Sack und Pack in die Stube, nachdem er das Roß versorgt hatte. Indem nun beide einander gegenüber saßen, langte der Kaufherr ein Paar Flaschen guten alten Weines hervor, und lud den Einsiedler ein, an dem köstlichen Labsal Theil zu nehmen. Der Waldbruder weigerte sich anfangs dessen; als aber der Kaufherr weiter in ihn drang, so nahm er ein Gläslein an, das ihm wohl schmeckte. Der Kaufherr erzählte nun von seinen Reisen und den Abenteuern, die er bestanden, und er schenkte dem Einsiedler von Zeit zu Zeit wieder ein, das sich dieser gefallen ließ. Allgemach merkte er jedoch, daß ihm der Wein zu Kopfe stieg, und er wollte sich noch zu rechter Zeit zurückziehen. Er hatte aber so viel Wohlgefallen an den Geschichten und Schwänken seines Gastes, daß er sich von seiner Gesellschaft nicht zu trennen vermochte. Und indem ihm der Kaufherr noch immer zusetzte, er sollte als guter Wirth Bescheid thun, da fiel ihm jener Gedanke und der Rath ein, durch eine böse That Gott zu versuchen, und er dachte sich: ein Räuschlein sei doch unter allem Schuldigen das Unschuldigste. Und er trank und trank, bis er schier von Sinnen kam. Dann entfernte er sich, und legte sich auf sein Lager. Aber er konnte nicht schlafen. Die Geister des Weins regten alle Geister auf. Es traten die Freuden der Welt vor sein Auge, und die sinnlichen Gelüste, die bisher geruht, wachten auf in seinem Herzen. Und es überkam ihn ein unaussprechlicher Ekel an seinem, von Welt und Menschen entfernten, lieb- und freudlosen Leben in der Einsamkeit, und er faßte den Entschluß, mit dem Kaufherrn des andern Tags wegzuziehen und zur menschlichen Gesellschaft zurückzukehren. Aber indem er nun in Traurigkeit seiner Armuth und seines Ungeschickes gedachte, um in der Welt fortkommen zu können, da fiel ihm ein Gedanke ein, der <pb n="83" xml:id="tg47.2.2.3"/>
ihn zu jeder andern Zeit mit Entsetzen erfüllt hätte, jetzt aber mit Hoffnung und Freude erfüllte. Und er führte ihn sogleich aus; denn er erschlug den Mann, und raubte ihm sein Geld und Gut. Also ward er aus einem Trunkenbold zugleich ein Räuber und ein Mörder. – Siehst du nun – sagte der Richter, indem er sich wieder an den Jüngling wandte – wie groß das Laster der Trunkenheit sei, da es der Anfang und die Ursache aller andern und der größten Laster ist. Der junge Mensch stand beschämt da, und er erwartete schon das strenge Urtheil, das ihn, als den Lasterhaftesten, enterben sollte. Da nahm der Richter wieder das Wort, und sprach: Und so seid ihr denn alle drei gleich verdammenswerth, und keiner hat Ursache sich über den andern zu stellen. Nun aber vernehmt, was, wie ich glaube, euren Vater zu dieser seiner letzten Willensmeinung bestimmt haben mag. Er wollte wol nichts Anderes damit, als daß einer den anderen tadle, drohe und bestrafe, und daß jeder von euch in sich gehe, damit er nicht in die angedrohte Strafe verfalle. Und dieses Testament seiner Liebe und Vorsorge erfüllet nun denn, und jeder thue es dem andern zuvor in einem löblichen Lebenswandel vor Gott und den Menschen. Mit diesen Worten entließ er die drei Brüder, und jeder hütete sich nun wol, daß er nicht in das alte Unwesen verfiele, aus Furcht, es möchten ihn die beiden andern enterben. Und so war der Anfang zu ihrer Besserung gemacht; wie es denn des Vaters Absicht gewesen bei seinem Testament.</p>
                </div>
            </div>
        </body>
    </text>
</TEI>