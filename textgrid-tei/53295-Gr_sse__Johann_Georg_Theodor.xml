<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg1167" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Die Marken/110. Der düstere Teich bei Lindstädt">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>110. Der düstere Teich bei Lindstädt</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Grässe, Johann Georg Theodor: Element 01179 [2011/07/11 at 20:29:59]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Johann Georg Theodor Grässe: Sagenbuch des Preußischen Staates 1–2, Band 1, Glogau: Carl Flemming, 1868/71.</title>
                        <author key="pnd:104076534">Grässe, Johann Georg Theodor</author>
                    </titleStmt>
                    <extent>105-</extent>
                    <publicationStmt>
                        <date notBefore="1868" notAfter="1871"/>
                        <pubPlace>Glogau</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1814" notAfter="1885"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg1167.2">
                <div type="h4">
                    <head type="h4" xml:id="tg1167.2.1">110) Der düstere Teich bei Lindstädt.<ref type="noteAnchor" target="#tg1167.2.5">
                            <anchor xml:id="tg1167.2.1.1" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Die Marken/110. Der düstere Teich bei Lindstädt#Fußnote_1"/>
                            <ptr cRef="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Die Marken/110. Der düstere Teich bei Lindstädt#Fußnoten_1"/>
                            <hi rend="superscript" xml:id="tg1167.2.1.2.1">1</hi>
                        </ref>
                    </head>
                    <p xml:id="tg1167.2.2">Wenn man von den am Neuen Palais zu Potsdam entstandenen Anlagen aus dem kleinen Bache folgt, der die grünen Wiesen des Gutes Lindstädt bewässert, so kommt man an einen dunkeln Weiher, der einsam zwischen den steilen Höhen des Pan- und Herzberges liegt. Früher erhielt jener Bach sein Wasser aus sieben Quellen, von denen eine auf dem Grunde dieses Teiches befindlich ist. Dieser Ort war früher sehr verrufen wegen des Spukes, der an seinen Ufern vorgehen sollte, und ist auch jetzt noch gemieden. Es <pb n="105" xml:id="tg1167.2.2.1"/>
soll nämlich einst ein großer Stein, der Teufelsstein, an der Stelle dieses Sees gelegen haben, und erst zur Zeit des schwedischen Krieges verschwunden, dann aber Wasser aus der Tiefe an dieser Stelle hervorgedrungen sein und jenen Teich gebildet haben. Ueber den Ursprung dieses Steines existiren aber zwei verschiedene Sagen. Nach der einen soll ihn der Teufel von dem Berge hinter der Krempatz aus nach dem Kirchlein auf dem Kirchberge im Hainholze, ohnweit der Nedlitzer Fähre, der ersten christlichen Kirche in jenen Landen, aus Aerger über die Einführung des Christenthums geschleudert haben, derselbe sey aber über das Kreuz hinweggeflogen und am Fuße des Panberges liegen geblieben. Nach Andern wäre er ein heidnischer Opferstein gewesen, der früher auf der Kuppe des Panberges gelegen habe, dann aber in die mit dichtem Wald und Gestrüpp bedeckte Schlucht hinabgerollt sey, wo aber die Anhänger der alten Götter noch lange im Geheim ihren Gottesdienst und Opferfeste gefeiert hätten. Später habe sich nun ein Stamm der Unterirdischen unter dem Steine angesiedelt, von wo aus der Eingang in ihre Gemächer und Höhlen gegangen sey. Von diesen kleinen Männern werden nun aber in der Umgegend verschiedene Sagen erzählt, welche sie bald als gutmüthige, bald als tückische, schadenfrohe Wesen erscheinen lassen. Man sagt nun, es zerfielen diese kleinen Leutchen in drei Klassen, nämlich in die weißen und grauen, die stets in diesen Gegenden heimisch gewesen seien, und in die schwarzen, welche erst mit dem Teufel ins Land gekommen wären. Die weißen sind guten Sinnes, verkehren gern mit den Menschen und leisten ihnen bereitwillig Hülfe, so lange sie nicht geneckt oder beleidigt werden; die grauen sind weniger gut, denn sie necken und foppen die Menschen gerne, ohne viel zu fragen, welches Unheil sie damit anrichten. Am schlimmsten sind aber die schwarzen, sie thun den Menschen nur Böses, können aber von diesen durch gewisse geheime Künste gebannt und dienstbar gemacht werden. Um dies zu können, braucht man sich nur etwas ihnen Angehörendes zu verschaffen, dies müssen sie wieder einlösen. Am leichtesten geschieht dies, wenn man sie bei ihren Tanzfesten überrascht, die sie in den Vollmondsnächten auf einsamen Waldplätzen feiern. Schleicht man sich an sie heran und wirft mit Erbsen oder kleinen Steinen unter sie, so müssen sie liegen lassen, was getroffen wird. So machte es ein Bauer aus Bornim, der fand auf dem Platze eine kleine Glocke, wie sie die Kleinen an ihrer Mütze tragen. Am andern Morgen kam der Zwerg, dem sie gehörte, als ein Jude verkleidet in das Haus des Bauern, feilschte um die Glocke und kaufte sie für 200 Goldgülden. Oft begeben sich die weißen und grauen Zwerge in die Häuser, wenn Musik darin gemacht wird, oder setzen sich Nachts auf die warmen Feuerstellen, gehen auch wohl den Mägden und Knechten hilfreich zur Hand. Haben sie dies einmal gethan, so bleiben sie gern dienstbar, essen auch die für sie hingestellten Speisen, nur wenn man ihnen etwas schenkt, halten sie sich für abgelohnt und kommen nicht wieder.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1167.2.3">In die Höhlen der Unterirdischen, zu welchen der Eingang unter dem genannten Teufelssteine war, sind auch von Zeit zu Zeit Menschen aus der Umgegend gekommen, besonders Kinder, alle auf sonderbare und unvorhergesehene Weise. Einige haben Geld und Kleinodien mit zurückgebracht, Andere sind nach kurzer Zeit ganz alt und verändert wiedergekommen oder von argem Spuck geneckt worden. Alle aber konnten nicht genug erzählen von der Pracht <pb n="106" xml:id="tg1167.2.3.1"/>
und Ausdehnung der Höhlen und Gänge, von den Schätzen und wunderbaren Dingen, die sie unten gesehen und erlebt. Man erzählt auch von einer frommen Wittwe, die sieben Töchter gehabt, welche sie in Sorgen erzogen. Diese Kinder hätten die Zwerge mit in den Berg genommen, mit ihnen gespielt und sie ernährt, wenn die Mutter bei der Arbeit auf dem Felde war. Die Frau hat es wohl gewußt und es gern gesehen. Der Pfarrer aber hat sie sehr gescholten, und sie geheißen die Kinder zurückzuhalten, auch ihr einen Bannspruch gelehrt, dem die Zwerge gehorchen mußten. Als sie nun eines Tages früher vom Felde kam und die Kinder nicht zu Hause fand, ist sie zum Teufelsstein gegangen und hat die Kinder gerufen, wie ihr der Pfarrer geheißen. Da haben die sieben Mädchen an sieben verschiedenen Stellen die kleinen Köpfchen aus der Erde gesteckt und die Mutter recht wehmüthig angesehen. Als diese nun aber den Bannspruch gesagt, sind die Köpfchen in die Erde zurückgesunken und an ihrer Stelle sind die sieben Quellen hervorgekommen. Der Stein aber soll von den Unterirdischen in die Tiefe hinabgezogen worden sein, als der bekannte Alchimist Kunkel einst unter ihm nach dem Golde der Zwerge gegraben hatte. Dann sind sie selbst nach und nach aus der Gegend weggezogen und nur selten noch soll sich einer von ihnen, der die Schätze bewachen muß, am düstern Teich oder in den Kellern des Hauses Lindstädt sehen lassen.</p>
                </div>
                <div type="footnotes">
                    <head type="h4" xml:id="tg1167.2.4">Fußnoten</head>
                    <note xml:id="tg1167.2.5.note" target="#tg1167.2.1.1">
                        <p xml:id="tg1167.2.5">
                            <anchor xml:id="tg1167.2.5.1" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Die Marken/110. Der düstere Teich bei Lindstädt#Fußnoten_1"/>
                            <ref cRef="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Die Marken/110. Der düstere Teich bei Lindstädt#Fußnote_1" xml:id="tg1167.2.5.2">1</ref> Nach Reinhard S. 122 etc.</p>
                    </note>
                </div>
            </div>
        </body>
    </text>
</TEI>