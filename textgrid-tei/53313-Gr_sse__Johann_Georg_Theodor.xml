<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg1185" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Die Marken/128. Liefeld's Grund">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>128. Liefeld's Grund</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Grässe, Johann Georg Theodor: Element 01197 [2011/07/11 at 20:29:59]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Johann Georg Theodor Grässe: Sagenbuch des Preußischen Staates 1–2, Band 1, Glogau: Carl Flemming, 1868/71.</title>
                        <author key="pnd:104076534">Grässe, Johann Georg Theodor</author>
                    </titleStmt>
                    <extent>126-</extent>
                    <publicationStmt>
                        <date notBefore="1868" notAfter="1871"/>
                        <pubPlace>Glogau</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1814" notAfter="1885"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg1185.2">
                <div type="h4">
                    <head type="h4" xml:id="tg1185.2.1">128) Liefeld's Grund.<ref type="noteAnchor" target="#tg1185.2.4">
                            <anchor xml:id="tg1185.2.1.1" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Die Marken/128. Liefeld's Grund#Fußnote_1"/>
                            <ptr cRef="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Die Marken/128. Liefeld's Grund#Fußnoten_1"/>
                            <hi rend="superscript" xml:id="tg1185.2.1.2.1">1</hi>
                        </ref>
                    </head>
                    <p xml:id="tg1185.2.2">Zwischen dem Brauhaus- und Ravensberge zieht sich dicht vom Wege nach Langenwisch bis zur Havel beim Tornow ein tiefer Thaleinschnitt hinunter, der früher von mächtigen Kiefern überwachsen und durch Brombeerranken und Gestrüpp mancherlei Art versteckt und unwegsam gemacht war, da soll einst die Höhle des bösen Räubers Peter Dönges gewesen sein. Jene Gegend war früher wenig besucht und nur sehr selten zog ein Wanderer die unwirthliche Straße, welche durch den viele Meilen sich ausdehnenden, nur von Räubern und wilden Thieren bewohnten Wald führte. Die große Landstraße, welche bei Wittenberg über die Elbe ins nördliche Deutschland führte, theilte sich bald: der eine Arm ging bei der festen Burg Rabenstein vorbei, längs der Plane auf Brandenburg, der andere über Treuenbriezen und Saarmund nach Berlin; Potsdam war noch zu unbedeutend und der Weg über die Havelarme und Brüche zu beschwerlich und unsicher, als daß hier an eine Handelsstraße gedacht worden wäre. Daher war jeder Einzelnreisende verloren, wenn ihn sein Weg in diese Einöde führte; fiel er einem der zahlreichen Wegelagerer in die Hände, so konnte nichts ihn vom Tode oder Gefangenschaft retten, denn auf etwa nachkommende Hilfe hatte er nicht zu rechnen. Der schlimmste von allen war eben ein gewisser Peter Dönges, der sich mit seiner noch bösern Mutter, der sogenannten rothen Hanne, die einst wegen Diebstahl zu Potsdam gestäupt worden war, im Liefeldtsgrunde eine Höhle in den Boden gegraben und dieselbe so geschickt mit Baumstämmen, Moos und Rasen überdeckt hatte, daß sie auch den schärfsten Späheraugen verborgen blieb. Hierher schleppte er die Beute seiner Raubzüge, mit Gefangenen befaßte er sich nicht, denn gewöhnlich überfiel er nur einzelne Reisende, an Mehrere zusammen wagte er sich nur dann, wenn es ihm gelang, sie vereinzelt zu ermorden. Sein Hauptaugenmerk war darauf gerichtet, daß nie Jemand davon kam, den er angegriffen hatte, und darum ließ er sich nie durch Bitten eines seiner Opfer erweichen, ihm das Leben zu schenken. Mit Hilfe seiner Mutter versenkte er die Erschlagenen gewöhnlich in das sogenannte Teufelsmoor am jetzigen Fahrweg nach Drewitz und verbrannte alle ihre Kleidungsstücke, sowie alles Geräth, was etwa leicht wieder zu erkennen gewesen wäre. An allen Hauptwegen waren im Dickicht Schlupfwinkel angelegt, <pb n="126" xml:id="tg1185.2.2.1"/>
in denen er sich zeitweilig aufhielt, und in deren Nähe er eine Schnur über den Weg gezogen hatte, welche an einer Glocke in der Höhle befestigt war. Zog dann ein Wagen oder Wanderer vorüber, so machte die Berührung der Schnur die Glocke läuten, dann schlich er herbei und überfiel die Unvorbereiteten gewöhnlich von hinten. Besonders aber hatte seine Mutter ihn vor jeder nähern Verbindung mit dem andern Geschlechte abzuhalten gesucht, allein da trug es sich zu, daß er eines Morgens ein junges Fischermädchen aus Neuendorf im Walde traf, welches Netzgarn nach Saarmund trug. Statt sie zu ermorden, redete er sie an, fragte sie aus, wann und auf welchem Wege sie zurückkehren werde und begleitete sie bis kurz vor die Stadt in die Nähe des Eichberges. Bei ihrer Rückkehr am Nachmittage überfiel er sie im Dickicht, verband ihr den Mund und die Augen und schleppte sie auf vielen Umwegen zu seiner Höhle im Liefeldtsgrund. Allein seine Mutter verlangte zornig, er solle sie sofort ermorden und nahm endlich, da er dies ein Mal ihr nicht nachgab, der zum Tode geängstigten Dirne einen furchtbaren Schwur ab, daß sie den Räuber nicht verrathen, ja nie zu einer lebenden Kreatur von ihm sprechen wolle. In dieser Höhle lebte nun das arme Geschöpf lange Jahre, ohne je das Licht der Sonne zu erblicken, entweder blieb stets eins, Mutter oder Sohn, bei ihr zurück oder Dönges verrammelte die Thüre der Höhle so von außen, daß das schwache Weib sie nicht zu öffnen vermochte. Mittlerweile war sie Mutter geworden, da nahm die Alte das Kind und sagte, sie wolle es in die Stadt zu einer Amme tragen, denn hier könne durch das Schreien desselben einmal ein Vorübergehender zur Entdeckung der Höhle veranlaßt werden. So geschah es dreimal und jedesmal ward der Zank zwischen Mutter und Sohn deswegen heftiger, allein erstere blieb dabei, die Kinder müßten unbedingt in die Stadt zu einer Amme. Allein vier Jahre, nachdem das unglückliche Frauenzimmer in die Höhle geschleppt worden war, starb die Alte und nun sah sich der Räuber gezwungen, das Mädchen ihrer Gefangenschaft zu entlassen, denn wer hätte sonst Lebensmittel herbeischaffen sollen? Er ließ ihr aber vorher jenen furchtbaren Eid noch einmal schwören, und siehe, die arme Person hielt ihn auch gewissenhaft, sie kehrte jedesmal getreulich wieder zurück. Nun ging sie eines Tages den Grund hinauf, um frisches Moos zu holen, da stieß sie plötzlich auf drei gleich große Steine, die wie Grabsteine in einer Reihe da lagen. Ohne recht zu wissen warum, fing ihr bei diesem Anblick an das Herz zu pochen, sie mußte wissen, was unter den Steinen lag; sie rollte sie von der Stelle, grub die Erde unter ihnen auf und fand die Leichen ihrer drei Kinder unter ihnen. Da sann die unglückliche Mutter auf Rache; als sie das nächste Mal wieder von Dönges ausgeschickt ward, um Lebensmittel einzukaufen, verließ sie den vorgeschriebenen Weg und ging nach Potsdam. Dort ging sie zuerst in die Kirche und betete zu Gott um Beistand zu ihrem Vorhaben und um Vergebung, wenn sie ihren Eid breche. Dann ging sie gegen Abend zur Schloßwache, und trat an den Pfeiler, wo die Fahne stand, und erzählte dem leblosen Pfeiler Alles, was ihr geschehen war, sagte auch, daß sie heute Abend beim Zurückgehen nach der Höhle Mehl auf den Weg ausstreuen wolle, dem möge man nachgehen und wenn man Peter Dönges fangen wolle, möge man morgen nach dem Mittagsessen, wenn er schlafe, kommen, dann könne man ihn sicher überraschen. So hatte sie freilich keiner lebenden Kreatur den Aufenthalt des <pb n="127" xml:id="tg1185.2.2.2"/>
Mörders ihrer Kinder verrathen, allein der Soldat, der neben dem Pfeiler auf Wache stand, hat Alles mit angehört und demnächst angezeigt. Am andern Tage aber ist eine starke Mannschaft ausgeschickt worden, welche der Mehlspur bis an den Liefeldtsgrund folgte, der Mooshügel, an welchem sie sich verlor, ward von Soldaten umstellt und dann der Eingang, wiewohl mit vieler Mühe, gesucht und gefunden. Zwar leistete der Räuber heftigen Widerstand, allein er ward überwältigt, nach Potsdam gebracht und dort nach wenigen Tagen, als er seiner Schandthaten überführt und geständig war, vor dem Brandenburger Thore lebendig verbrannt.</p>
                </div>
                <div type="footnotes">
                    <head type="h4" xml:id="tg1185.2.3">Fußnoten</head>
                    <note xml:id="tg1185.2.4.note" target="#tg1185.2.1.1">
                        <p xml:id="tg1185.2.4">
                            <anchor xml:id="tg1185.2.4.1" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Die Marken/128. Liefeld's Grund#Fußnoten_1"/>
                            <ref cRef="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Die Marken/128. Liefeld's Grund#Fußnote_1" xml:id="tg1185.2.4.2">1</ref> Nach Reinhard S. 143 etc.</p>
                    </note>
                </div>
            </div>
        </body>
    </text>
</TEI>