<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg1283" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Die Marken/226. Die Hauptkirche zu Rathenow">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>226. Die Hauptkirche zu Rathenow</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Grässe, Johann Georg Theodor: Element 01295 [2011/07/11 at 20:29:59]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Johann Georg Theodor Grässe: Sagenbuch des Preußischen Staates 1–2, Band 1, Glogau: Carl Flemming, 1868/71.</title>
                        <author key="pnd:104076534">Grässe, Johann Georg Theodor</author>
                    </titleStmt>
                    <extent>202-</extent>
                    <publicationStmt>
                        <date notBefore="1868" notAfter="1871"/>
                        <pubPlace>Glogau</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1814" notAfter="1885"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg1283.2">
                <div type="h4">
                    <head type="h4" xml:id="tg1283.2.1">226) Die Hauptkirche zu Rathenow.<ref type="noteAnchor" target="#tg1283.2.4">
                            <anchor xml:id="tg1283.2.1.1" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Die Marken/226. Die Hauptkirche zu Rathenow#Fußnote_1"/>
                            <ptr cRef="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Die Marken/226. Die Hauptkirche zu Rathenow#Fußnoten_1"/>
                            <hi rend="superscript" xml:id="tg1283.2.1.2.1">1</hi>
                        </ref>
                    </head>
                    <p xml:id="tg1283.2.2">Eine fromme und gerechte Bürgerin Rathenow's in der Altmark hörte einst an einem Sonntage in aller Frühe die Kirchenglocken anschlagen, hielt es für das Frühmettengeläute und eilte fort nach dem Gotteshause. Als sie daselbst ankommt, ergreift sie Staunen und Entsetzen. Die Kirche ist mit Andächtigen überfüllt, die ihr sämmtlich unbekannt sind. Wohin sie ihre Blicke wendet, stehen fremde Personen, und selbst von den Mönchen, die am Altar und sonst umher stehen, hat sie keinen jemals gesehen. Der Gesang hebt an, doch sie versteht nicht, was man singt. Ein Geistlicher erscheint auf der Kanzel, er spricht in einer ihr völlig unverständlichen Sprache. Von steigender Bangigkeit gefoltert erkennt die Frau endlich dicht hinter sich eine Person, die aber schon seit vielen Jahren nicht mehr zu den Lebenden gehört. Diese wendet sich jetzt zu ihr und flüstert ihr ins Ohr: es ist Zeit, daß Du Dich wegbegiebst, ein längeres Verweilen hier möchte Dir Unheil bringen! Bebend am ganzen Leibe erhebt sich die Bürgerfrau von ihrem Sitze und wankt mühsam aus dem Gotteshaus hinaus. Sie ist kaum hinaus, so schlägt die Kirchenthüre mit furchtbarem Geprassel hinter ihr zu. Sie meldet das Geschehene ihrem Beichtvater,<ref type="noteAnchor" target="#tg1283.2.6">
                            <anchor xml:id="tg1283.2.2.1" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Die Marken/226. Die Hauptkirche zu Rathenow#Fußnote_2"/>
                            <ptr cRef="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Die Marken/226. Die Hauptkirche zu Rathenow#Fußnoten_2"/>
                            <hi rend="superscript" xml:id="tg1283.2.2.2.1">2</hi>
                        </ref> und dieser verspricht, wenn sie noch einmal früh Morgens läuten höre, mit ihr in die Kirche zu gehen. Nach einiger Zeit geschieht es wieder, und der Geistliche begleitet die Frau. Als sie die Thüre öffnen, ist die Kirche erleuchtet und von lauter fremd gekleideten Erscheinungen gefüllt. Doch in dem Augenblicke, wo der Geistliche über das Vortreten eines Mönches auf der Kanzel einige Worte äußert, verschwindet jener und Alles verwandelt sich in tiefe Finsterniß. Erst nach langem Suchen finden Beide die Ausgangsthüre der Kirche wieder.</p>
                </div>
                <div type="footnotes">
                    <head type="h4" xml:id="tg1283.2.3">Fußnoten</head>
                    <note xml:id="tg1283.2.4.note" target="#tg1283.2.1.1">
                        <p xml:id="tg1283.2.4">
                            <anchor xml:id="tg1283.2.4.1" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Die Marken/226. Die Hauptkirche zu Rathenow#Fußnoten_1"/>
                            <ref cRef="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Die Marken/226. Die Hauptkirche zu Rathenow#Fußnote_1" xml:id="tg1283.2.4.2">1</ref> Nach Ed. v. Felsthal, Des deutschen Volkes Sagenschatz S. 284.</p>
                    </note>
                    <note xml:id="tg1283.2.6.note" target="#tg1283.2.2.1">
                        <p xml:id="tg1283.2.6">
                            <anchor xml:id="tg1283.2.6.1" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Die Marken/226. Die Hauptkirche zu Rathenow#Fußnoten_2"/>
                            <ref cRef="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Die Marken/226. Die Hauptkirche zu Rathenow#Fußnote_2" xml:id="tg1283.2.6.2">2</ref> Dies ist nach Reichard, Vermischte Beiträge zur Einsicht in das Geisterreich, Bd. I. S. 29 etc., der nachher zu erwähnende Matthias Lüßau gewesen.</p>
                    </note>
                </div>
            </div>
        </body>
    </text>
</TEI>