<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg1361" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Provinz Sachsen und Thüringen/303. Das Blutgericht und der blutige Stein im Dom">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>303. Das Blutgericht und der blutige Stein im Dom</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Grässe, Johann Georg Theodor: Element 01374 [2011/07/11 at 20:29:59]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Johann Georg Theodor Grässe: Sagenbuch des Preußischen Staates 1–2, Band 1, Glogau: Carl Flemming, 1868/71.</title>
                        <author key="pnd:104076534">Grässe, Johann Georg Theodor</author>
                    </titleStmt>
                    <extent>249-</extent>
                    <publicationStmt>
                        <date notBefore="1868" notAfter="1871"/>
                        <pubPlace>Glogau</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1814" notAfter="1885"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg1361.2">
                <div type="h4">
                    <head type="h4" xml:id="tg1361.2.1">
                        <pb n="249" xml:id="tg1361.2.1.1"/>
303) Das Blutgericht und der blutige Stein im Dom zu Magdeburg.<ref type="noteAnchor" target="#tg1361.2.4">
                            <anchor xml:id="tg1361.2.1.2" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Provinz Sachsen und Thüringen/303. Das Blutgericht und der blutige Stein im Dom#Fußnote_1"/>
                            <ptr cRef="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Provinz Sachsen und Thüringen/303. Das Blutgericht und der blutige Stein im Dom#Fußnoten_1"/>
                            <hi rend="superscript" xml:id="tg1361.2.1.3.1">1</hi>
                        </ref>
                    </head>
                    <p xml:id="tg1361.2.2">Vor beinahe 1000 Jahren lebte zu Magdeburg ein Erzbischof, Namens Udo, der nicht nur der Wollust im höchsten Grade ergeben war, sondern auch sehr wenig Glauben und Frömmigkeit besaß. Derselbe unterhielt ein sträfliches Verhältniß mit der schönen Aebtissin von Lilienthal und besuchte dieselbe zum Oeftern in ihrem Kloster. Da begab es sich, daß in der dritten Nacht, wo er bei ihr war, um Mitternacht eine Stimme rief: (<hi rend="italic" xml:id="tg1361.2.2.1">Cessa nunc de ludo, ludisti satis, Udo!)</hi> Udo, höre auf zu kosen, Du hast genug gekost! Zwar erschracken die beiden Sünder, allein sie sammelten sich bald wieder und führten dasselbe Leben fort. Dieselbe Stimme ließ sich aber auch in den nächsten beiden Nächten vernehmen, jedoch abermals ohne Erfolg. Als nun am vierten Tage die Stimme sich nicht wieder hören ließ, glaubten die Gottlosen gewonnen zu haben, der Erzbischof beschloß noch längere Zeit bei der Aebtissin zu verweilen, schützte dem Domcapitel gegenüber eine nothwendige Reise, die drei Monate dauern würde, vor, statt aber zu verreisen, blieb er im Kloster bei seiner Buhlerin. Nun lebte aber damals unter den Domherren zu Magdeburg ein sehr frommer Mann, der nicht blos am Tage den Werken christlicher Gottesfurcht getreulich oblag, sondern auch noch des Nachts in den Dom zu gehen pflegte, um dort zu beten. Als er nun während dieser Zeit auch eines Nachts dahin um Mitternacht gegangen war, da löschte auf einmal ein plötzlich sich erhebender Sturmwind alle Lampen aus und er sah, daß plötzlich der ganze Dom in einem milden Lichte strahlte. Aus dem Hintergrunde traten zwei Jünglinge, zwei Kerzen in der Hand, die sie auf den Hochaltar stellten, hervor, zwei andere setzten zwei Sessel vor denselben auf einen Teppich, ein dritter aber, ein blitzendes Schwert in den Händen, stellte sich mitten in den Dom und rief alle dort begrabenen Bischöfe, Ritter und Frauen mit lauter Stimme auf, sie sollten erwachen, Gott wolle ein Gericht halten. Und siehe, eine Unzahl Schatten erhoben sich aus den Gräbern und stellten sich stillschweigend in langen Reihen um den Chor. Darauf erschienen, während der Dom zuletzt wie von einer Sonne erleuchtet ward, die zwölf Apostel, Christus mit der goldenen Krone geschmückt und das Scepter in der Rechten an ihrer Spitze. Letzterer setzte sich auf einen der Sessel und die Apostel stellten sich zu seiner Linken, dann aber erschien die Jungfrau Maria, gefolgt von einer Schaar heiliger Jungfrauen und nahm den andern Sessel neben ihm ein. Da trat der heil. Mauritius, der Schutzpatron des Doms, vor und klagte den Erzbischof Udo an, daß er durch seine Laster sein Amt entheilige, alle Buße verschmähe und in diesem Augenblick noch bei seiner Buhlerin, einer Gottgeweihten weile. Da befahl Christus zweien der Auferstandenen, den Sünder zur Stelle zu bringen. Nach wenig Minuten erschien er auch leichenblaß und zitternd an allen Gliedern vor Christus. Allein statt um Gnade zu flehen, sprach er frech: »Was hab' ich gethan, o Herr, daß ich vor deinem Richterstuhl erscheinen soll?« Und als Mauritius vortrat und ihm sein Verbrechen vorhielt, behauptete er, es sei Verleumdung; er habe nicht in den Armen der Abtissin geruht, sondern am<pb n="250" xml:id="tg1361.2.2.2"/>
 Altar seiner Zelle gebetet. Da fragte Christus: »Was hast Du gebetet, Udo?« Allein der Bösewicht konnte sich auf den Anfang und Namen keines Gebetes besinnen, und der Herr sprach: »Siehe, Du bist gerichtet, Du verlangtest Zeugen, jetzt zeugst Du wider Dich selbst!« Darauf rief er dreimal Wehe über Udo, winkte mit dem Scepter, und zwei der Auferstandenen führten ihn an das Ende des Doms, der Jüngling aber mit dem Schwerte trat vor und schlug ihm mit dem Schwerte das Haupt ab. Kaum war dies geschehen, so verschwand die ganze Versammlung, die Lampen brannten wieder und der Domherr sah sich allein, nur die Leiche des Hingerichteten bewies, daß das Gericht kein Traum gewesen war. Er blieb bis an den Morgen in der Kirche, dann rief er das Kapitel zusammen und theilte ihm das Geschehene mit. Der Stein aber, auf dem Udo gerichtet worden war, hat sich noch lange im Dome erhalten. Blutrothe Streifen bezeichnen seine Oberfläche, aus der, wenn man sie rieb, selbst flüssiges Blut herausgequollen sein soll.</p>
                </div>
                <div type="footnotes">
                    <head type="h4" xml:id="tg1361.2.3">Fußnoten</head>
                    <note xml:id="tg1361.2.4.note" target="#tg1361.2.1.2">
                        <p xml:id="tg1361.2.4">
                            <anchor xml:id="tg1361.2.4.1" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Provinz Sachsen und Thüringen/303. Das Blutgericht und der blutige Stein im Dom#Fußnoten_1"/>
                            <ref cRef="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Provinz Sachsen und Thüringen/303. Das Blutgericht und der blutige Stein im Dom#Fußnote_1" xml:id="tg1361.2.4.2">1</ref> Nach Relßieg Bd. I. S. 356 etc. Poetisch bearbeitet von Ziehnert Bd. I. S. 249 etc. Weitläufig bei Oldenbach, Preußische Sagen. Berlin 1840 in 12. S. 503 etc.</p>
                    </note>
                </div>
            </div>
        </body>
    </text>
</TEI>