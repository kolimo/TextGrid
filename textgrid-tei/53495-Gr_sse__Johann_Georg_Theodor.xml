<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg1367" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Provinz Sachsen und Thüringen/309. Der diebische Rabe zu Magdeburg">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>309. Der diebische Rabe zu Magdeburg</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Grässe, Johann Georg Theodor: Element 01380 [2011/07/11 at 20:29:59]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Johann Georg Theodor Grässe: Sagenbuch des Preußischen Staates 1–2, Band 1, Glogau: Carl Flemming, 1868/71.</title>
                        <author key="pnd:104076534">Grässe, Johann Georg Theodor</author>
                    </titleStmt>
                    <extent>256-</extent>
                    <publicationStmt>
                        <date notBefore="1868" notAfter="1871"/>
                        <pubPlace>Glogau</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1814" notAfter="1885"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg1367.2">
                <div type="h4">
                    <head type="h4" xml:id="tg1367.2.1">
                        <pb n="256" xml:id="tg1367.2.1.1"/>
309) Der diebische Rabe zu Magdeburg.<ref type="noteAnchor" target="#tg1367.2.6">
                            <anchor xml:id="tg1367.2.1.2" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Provinz Sachsen und Thüringen/309. Der diebische Rabe zu Magdeburg#Fußnote_1"/>
                            <ptr cRef="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Provinz Sachsen und Thüringen/309. Der diebische Rabe zu Magdeburg#Fußnoten_1"/>
                            <hi rend="superscript" xml:id="tg1367.2.1.3.1">1</hi>
                        </ref>
                    </head>
                    <p xml:id="tg1367.2.2">In Magdeburg stehen drei Häuser mit besondern Namen, die aber alle zu einander in einer gewissen Beziehung stehen, an welche sich eine Sage knüpft. Es sind diese der schwarze Rabe am Knochenhauer-Ufer, die weiße Taube daneben und die goldene Sonne auf der Stephansbrücke.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1367.2.3">Im Jahre 1598 hat nämlich ein reicher Brauherr auf der Stephansbrücke gewohnt und am 6. December, gerade an dem Tage, wo ein Erdbeben und Wetterleuchten früh um 6 Uhr die Bewohner Magdeburgs aus ihren Betten aufschreckte, seine Tochter mit einem reichen Kaufmann vermählt, der seine Wohnung in dem Hause am Knochenhauer-Ufer hatte, welches jetzt der schwarze Rabe heißt. Nachdem nun die jungen Eheleute aus dem Hause der Eltern der Braut nach abgehaltenem Hochzeitsmahle in ihre neue Wohnung gezogen waren und die junge Frau sich daselbst von ihrer Dienerin entkleiden ließ, hatte sie in ihrem Zimmer die von ihr an diesem Tage getragenen Kleinodien, einige Ringe und ein kostbares Halsgeschmeide der Dienerin übergeben, um sie aufzuheben, letztere aber dieselben aus Nachlässigkeit auf dem Tische liegen lassen. Als nun am andern Morgen die junge Frau nach denselben fragte, waren sie verschwunden, und da erweislich Niemand in das Haus gekommen war, die Dienerin aber auch nicht sagen konnte, wo die Schmucksachen hingekommen seien, so fiel der Verdacht der Entwendung auf sie. Zwar leugnete sie beharrlich und betheuerte ihre Unschuld, allein es half Alles nichts, sie ward gefangen gesetzt und vor die Richter gebracht, und als sie auch dort bei ihrem Leugnen beharrte, beschloß man, die Wahrheit durch die Folter von ihr zu erpressen. Kaum hatte man ihr aber die Daumschrauben angelegt, so schrie sie vor Schmerz, sie wolle bekennen und gestand auch, daß sie die Diebin sei. Als man sie aber fragte, wo die Kleinodien seien, wußte sie den Ort nicht anzugeben. Man drohte also, sie abermals zu foltern, wenn sie nicht sagte, wo sie sie verborgen habe. Da sagte sie in ihrer Herzensangst, sie befänden sich in dem Hause ihres Brodherrn. Man brachte sie also in das Haus desselben auf dem Knochenhauer-Ufer zurück und führte sie in demselben herum, damit sie das Versteck angeben solle. Allein natürlich fand sich nichts und so kamen sie zuletzt auf den Dachboden hinauf. Hier ermahnte man sie nun zum letzten Mal, die betreffende Stelle anzugeben oder sie selbst hervorzuholen, weil sie, nachdem man das ganze Haus durchsucht, eben an keinem andern Orte mehr sein könnten, und drohte ihr, falls sie abermals Ausreden machen und den Versteck nicht angeben werde, man sofort sie schärfer befragen lassen werde.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1367.2.4">Da schaute das Mädchen rathlos um sich, plötzlich sah sie nach dem durch die Dachluke hereinschimmernden, von der Sonne wunderbar erleuchteten Himmel, sie erhob in der Verzweiflung die Hände und bat flehentlich, Gott möge ein Zeichen geben, daß ihre Unschuld an den Tag kommen möge. Und der Herr erhörte sie auch, denn auf einmal entstand neben dem Schornstein ein sonderbares Geräusch, eine weiße Taube, die sich dort aufgehalten hatte, flog zur offenen Dachluke hinaus und setzte sich auf das Nachbarhaus. Beim raschen Fortfliegen hatte dieselbe aber etwas Holz oder anderes Gerölle <pb n="257" xml:id="tg1367.2.4.1"/>
losgerissen, welches hinter dem Schornstein gelegen hatte und jetzt zu Boden fiel. Siehe, unter demselben befand sich aber die Halskette, und als alle Anwesenden noch erstaunt auf dieselbe blickten, kam auf einmal der alte Hausrabe, an den Niemand gedacht hatte und der nach seiner Gewohnheit mit der Gesellschaft auf den Boden gelaufen war, herbeigehüpft, faßte die Kette mit seinem Schnabel und suchte sie bei Seite zu schleppen. Damit war das Räthsel gelöst und der wahre Dieb entdeckt, denn man fand bei näherem Nachsuchen auch die übrigen entwendeten Gegenstände hinter dem Schornstein liegen. Natürlich ward das arme Mädchen sogleich freigelassen, das Haus aber, wo die seltsame Begebenheit geschehen war, nannte man seitdem den schwarzen Raben, das Nachbarhaus, wohin die Taube als Eigenthum gehörte, die weiße Taube, und jenes dritte auf der St. Stephansbrücke die goldene Sonne, weil der Vater der jungen Frau in der der Entdeckung vorhergehenden Nacht geträumt hatte, er sehe über seinem Hause zwei Sonnen, eine goldig und hellstrahlend, die andere blutigroth; dieselben hatten im Kern menschliche Gesichter, das aber in der blutigrothen Sonne war das der Magd; diese Sonnen rückten einander immer näher und schwammen endlich in eine einzige hellstrahlende Sonne in einander. Er hatte diesen Traum darauf gedeutet, daß die Sonne die Unschuld des Mädchens an den Tag bringen werde, wie es auch geschehen ist.</p>
                </div>
                <div type="footnotes">
                    <head type="h4" xml:id="tg1367.2.5">Fußnoten</head>
                    <note xml:id="tg1367.2.6.note" target="#tg1367.2.1.2">
                        <p xml:id="tg1367.2.6">
                            <anchor xml:id="tg1367.2.6.1" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Provinz Sachsen und Thüringen/309. Der diebische Rabe zu Magdeburg#Fußnoten_1"/>
                            <ref cRef="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Provinz Sachsen und Thüringen/309. Der diebische Rabe zu Magdeburg#Fußnote_1" xml:id="tg1367.2.6.2">1</ref> Nach Relßieg Bd. II. S. 247 etc.</p>
                    </note>
                </div>
            </div>
        </body>
    </text>
</TEI>