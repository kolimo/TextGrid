<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg1379" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Provinz Sachsen und Thüringen/321. Die wandelnde Nonne auf dem Schlosse zu Loburg">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>321. Die wandelnde Nonne auf dem Schlosse zu Loburg</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Grässe, Johann Georg Theodor: Element 01392 [2011/07/11 at 20:29:59]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Johann Georg Theodor Grässe: Sagenbuch des Preußischen Staates 1–2, Band 1, Glogau: Carl Flemming, 1868/71.</title>
                        <author key="pnd:104076534">Grässe, Johann Georg Theodor</author>
                    </titleStmt>
                    <extent>266-</extent>
                    <publicationStmt>
                        <date notBefore="1868" notAfter="1871"/>
                        <pubPlace>Glogau</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1814" notAfter="1885"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg1379.2">
                <div type="h4">
                    <head type="h4" xml:id="tg1379.2.1">
                        <pb n="266" xml:id="tg1379.2.1.1"/>
321) Die wandelnde Nonne auf dem Schlosse zu Loburg.<ref type="noteAnchor" target="#tg1379.2.5">
                            <anchor xml:id="tg1379.2.1.2" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Provinz Sachsen und Thüringen/321. Die wandelnde Nonne auf dem Schlosse zu Loburg#Fußnote_1"/>
                            <ptr cRef="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Provinz Sachsen und Thüringen/321. Die wandelnde Nonne auf dem Schlosse zu Loburg#Fußnoten_1"/>
                            <hi rend="superscript" xml:id="tg1379.2.1.3.1">1</hi>
                        </ref>
                    </head>
                    <p xml:id="tg1379.2.2">Im Regierungsbezirke Magdeburg liegt das Städtchen Loburg. Daselbst befand sich früher ein Nonnenkloster, welches nach seiner Aufhebung bis auf die neueste Zeit den Namen des alten Schlosses geführt hat. Der Sage nach soll nun im 16. Jahrhundert einst ein junger Edelmann aus Thüringen einen Liebeshandel mit einer dort lebenden jungen Nonne angeknüpft, die Mauern des Klosters erstiegen, seine Geliebte mit vielen Schätzen und Kostbarkeiten entführt und auf sein väterliches Schloß in Thüringen gebracht haben. Hier sollen sie lange glücklich mit einander gelebt, die Nonne aber nach ihrem Tode auf einmal wieder in den Räumen des Klosters aufgetaucht sein und durch ruheloses Umherwandeln in der Nacht ihre Reue über den Treubruch an ihrem Gelübde zu erkennen gegeben haben. Lange nachher als auch aus der Burg wieder eine vollständige Ruine, von der nur noch der Thurm stand, geworden war, sah das Gesinde des Burgherrn zu gewissen Zeiten, am Tage sowohl als bei Nachtzeit, eine mit dem Gewande einer Klosterfrau bekleidete Frauensperson über den weiten Hofraum hinweg nach jenem Thurm und wieder zurück wandern, wo sie dann jedesmal in der Gegend des großen Kellers verschwand. Auch viele andere Personen sahen diese Nonne, Niemand aber hatte so viel Muth, sie aufzuhalten oder anzureden, weil man wußte, daß ein solcher Versuch einst einem Knechte sehr schlecht bekommen war. Derselbe hatte nämlich gegen sein Mitgesinde freventlich behauptet, er werde die Nonne nicht nur aufhalten, sondern sie auch mit in den Stall nehmen und sie da entweder sagen lassen, was sie ruhelos umhertreibe, oder sie, wenn es eine Betrügerin sei, entlarven. Eines Tages ging derselbe über den Hof, um sich zum Essen in die Gesindestube zu begeben, da begegnete ihm die Nonne; er vertrat ihr den Weg, und als sie ihm ausweichen wollte, versuchte er sie mit beiden Händen um den Leib zu fassen und festzuhalten. Dies bekam ihm aber sehr schlecht, sie widerstand ihm und im Ringen mit ihr kam er zum Fallen, die Nonne fiel auf ihn darauf und er fing in Folge dessen so gewaltig an zu schreien, daß ihm seine Mitknechte zu Hilfe eilten. Diese sahen aber nur den auf dem Boden liegenden und mit den Füßen um sich strampelnden Burschen, von einer Nonne aber gewahrten sie nichts. Sie hoben ihn auf, trugen ihn in's Haus und als er sich erholt hatte, sagte er, es sei ihm beim Fallen so gewesen, als wenn ein großer Leichenstein mit vielen Bildern und Inschriften sich auf ihn lege, und wirklich war sein Körper ganz blau und mit Schwielen und Rissen bedeckt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1379.2.3">Diese Strafe des Uebermuthes schreckte aber für lange Zeit jeglichen ähnlichen Fürwitz vollständig ab. Da trug es sich zu, daß eines Tages im Monat Mai eine arme Wittwe aus der nahen Mühle kam, wo sie Weizenmehl für das bevorstehende Hochzeitsfest ihrer einzigen Tochter mit einem Maurergesellen hatte einkaufen wollen, solches aber nicht bekommen hatte, und betrübt mit dem leeren Korbe über die sogenannte Burgwiese fortschreitend an den Zaun kam, wo sich noch bis auf die neueste Zeit eine sogenannte Uebersteige, d.h. ein zum Uebersteigen eingerichtetes Stück Zaun befand. Da sah sie gerade auf dieser Stelle eine in ein Nonnengewand gehüllte <pb n="267" xml:id="tg1379.2.3.1"/>
Frauensperson sitzen, die keine Miene machte aufzustehen und ihr zum Uebersteigen Platz zu machen. Sie forderte sie dem zu Folge mehr als einmal auf aufzustehen, jene aber that, als wenn sie nichts höre, und so mußte die Wittwe, um nicht wieder umkehren zu müssen, sich an ihr vorbeidrängen. Bei dieser Gelegenheit bemerkte sie, daß die Unbekannte sehr schön, aber im Gesicht leichenblaß und mit glänzenden Perlen und Steinschmuck geziert war. Als sie nach Hause kam, erzählte sie ihrer Tochter den Vorgang und diese rief sogleich: »Ei das ist die Nonne gewesen, Mutter! Die hättest Du anreden sollen, vielleicht hättest Du von ihr eine Gabe zu meiner Hochzeit bekommen!« Jene aber versetzte: »Dazu ist noch Zeit, denn gewiß ist sie noch da!« Sie kehrte also um und sah auch wirklich aus der Ferne die Nonne sitzen, allein als sie bald an sie heran war, stand jene auf und war auf der entgegengesetzten Seite verschwunden, zwar lief die Wittwe ihr nach, allein nichts war mehr von ihr zu sehen. Sie kehrte also traurig um, als sie aber wieder an die Uebersteige kam, sah sie an dem einen Pfahle derselben, gerade an der Stelle, wo jene gesessen hatte, ein Beutelchen hängen, durch dessen seidne Maschen Gold glänzte. Sie steckte es schnell ein und kam voller Freuden mit ihrem Funde nach Hause. Hier angekommen, fand sie in dem geöffneten Beutel 50 Goldstücke von altem Gepräge und zwei Kreuzchen mit Edelsteinen besetzt. Mittlerweile kam aber ihr zukünftiger Schwiegersohn, dem erzählte sie ihr Glück und versprach, ihm mit dem Inhalte des Beutels seine Wirthschaft einrichten zu wollen. Allein davon wollte derselbe nichts hören, sondern verlangte, sie solle gleich wieder hingehen und nachsehen, ob die Frau nicht eben vielleicht ihren Beutel suche, und wenn dies nicht der Fall sei, so solle sie ihn auf das Rathhaus tragen, wo dann schon der Verlustträger ermittelt werden würde. Er nöthigte sie auch trotz ihres Widerstrebens mit ihm nach der Schloßwiese zu gehen und wirklich sahen sie dort auch die bewußte Frau scheinbar mit auf den Boden gerichtetem Gesicht die verlorenen Gegenstände suchen. Er nahm also seiner Schwiegermutter den Beutel aus der Hand, sprach die Nonne an und händigte ihr das Verlorene wieder ein. Jene aber reichte ihm als Gegengeschenk eine wunderschöne Rose und verschwand. Obgleich etwas verwundert über den seltsamen Tausch, kehrte er doch seelenfroh, den Beutel wieder losgeworden zu sein, nach Hause zurück, stellte die Rose in ein Glas mit Wasser, hatte aber vorher aus Versehen ein Blatt derselben abgestoßen, welches auf dem Tische liegen blieb. Am andern Morgen aber lag statt des Blattes ein Goldstück da und die Braut hoffte, mit der Rose würde es sich ebenso verhalten, versuchte also dieselbe zu entblättern, allein umsonst, die Rose hatte sich in der Nacht zu einer harten und festen Knospe zusammengezogen, so daß es unmöglich war, ohne dieselbe zu vernichten, ein einziges Blatt loszubrechen. Der nächste Morgen brachte aber wieder ein Goldstück und so löste sich jeden Morgen ein neues von der Rosenknospe ab. So ward der arme Maurer bald ein reicher Mann, er baute sich von den Goldstücken ein Haus in der Stadt, legte aber in dem untern Raum desselben eine kleine Hauskapelle an, wo er jeden Tag mit seiner Frau für die Erlösung der armen Nonne betete. Auf dem Thurme der alten Burg befand sich aber eine vom Zahn der Zeit zerstörte und verwitterte Thür, auf der eine Nonne abgemalt war. Diese Thür ließ der damalige Besitzer der Burg, als er einst an dem Thurme einige <pb n="268" xml:id="tg1379.2.3.2"/>
Veränderungen hatte vornehmen wollen, wegnehmen und verbrennen, und siehe, von diesem Augenblicke an traf ihn und seine Familie nichts als Unglück, so daß er gar bald verarmte und das Schloß in fremde Hände kam. Während dieser ganzen Zeit hatte sich aber auch die Nonne nicht wieder sehen lassen. Der neue Besitzer ließ aber auf den Rath des nunmehr hochbetagten Maurers eine neue Thür mit dem Bilde der Nonne fertigen, und siehe, mit dem Bilde kehrte auch die Nonne und mit der Nonne Wohlstand und Glück in die Familie des Burgherrn zurück. Eines Tages betete aber der Greis in seiner Kapelle, da erschien ihm die Nonne, dankte ihm für seinen frommen Glauben an ihre Mildthätigkeit und sagte, die Zeit ihrer Buße sei abgelaufen und sie selbst sei erlöst. Sie gab ihm noch einmal eine schöne Rose, befahl ihm aber, dieselbe in der kleinen Kapelle zu verwahren und diese durch seinen Sohn zumauern zu lassen, da er ihrer nicht mehr bedürfen werde. Der Greis befolgte ihren Befehl, starb aber den Tag darauf und seit dieser Zeit hat Niemand die wandelnde Nonne mehr erblickt. Das Haus mit der vermauerten Kapelle steht noch jetzt und man sagt, daß noch heute dort eine Familie lebe, die Papiere verwahre, in welchen jener Greis, ihr Ahnherr, die Begebenheit niedergeschrieben und den Zeitpunkt angegeben habe, wann man jene Vermauerung öffnen und von der dort aufbewahrten Rose Gebrauch machen solle.</p>
                </div>
                <div type="footnotes">
                    <head type="h4" xml:id="tg1379.2.4">Fußnoten</head>
                    <note xml:id="tg1379.2.5.note" target="#tg1379.2.1.2">
                        <p xml:id="tg1379.2.5">
                            <anchor xml:id="tg1379.2.5.1" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Provinz Sachsen und Thüringen/321. Die wandelnde Nonne auf dem Schlosse zu Loburg#Fußnoten_1"/>
                            <ref cRef="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Provinz Sachsen und Thüringen/321. Die wandelnde Nonne auf dem Schlosse zu Loburg#Fußnote_1" xml:id="tg1379.2.5.2">1</ref> Nach Relßieg Bd. I. S. 72 etc.</p>
                    </note>
                </div>
            </div>
        </body>
    </text>
</TEI>