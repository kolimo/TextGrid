<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg1390" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Provinz Sachsen und Thüringen/332. Hans von Hackelberg der wilde Jäger">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>332. Hans von Hackelberg der wilde Jäger</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Grässe, Johann Georg Theodor: Element 01403 [2011/07/11 at 20:29:59]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Johann Georg Theodor Grässe: Sagenbuch des Preußischen Staates 1–2, Band 1, Glogau: Carl Flemming, 1868/71.</title>
                        <author key="pnd:104076534">Grässe, Johann Georg Theodor</author>
                    </titleStmt>
                    <extent>292-</extent>
                    <publicationStmt>
                        <date notBefore="1868" notAfter="1871"/>
                        <pubPlace>Glogau</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1814" notAfter="1885"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg1390.2">
                <div type="h4">
                    <head type="h4" xml:id="tg1390.2.1">332) Hans von Hackelberg der wilde Jäger.<ref type="noteAnchor" target="#tg1390.2.5">
                            <anchor xml:id="tg1390.2.1.1" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Provinz Sachsen und Thüringen/332. Hans von Hackelberg der wilde Jäger#Fußnote_1"/>
                            <ptr cRef="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Provinz Sachsen und Thüringen/332. Hans von Hackelberg der wilde Jäger#Fußnoten_1"/>
                            <hi rend="superscript" xml:id="tg1390.2.1.2.1">1</hi>
                        </ref>
                    </head>
                    <p xml:id="tg1390.2.2">Hans von Hackelberg, herzoglich braunschweigischer Oberjägermeister in der Mitte des 16. Jahrhunderts, lebte buchstäblich nur für die Jagd. Um seine Leidenschaft zu befriedigen, kaufte oder pachtete er mehrere benachbarte Jagden und so durchzog er mit seinem Gefolge und seiner großen Meute Hunde Felder und Gehölze und die Vorgebirge des Harzes Jahr aus Jahr ein bei Tag und Nacht. Einst übernachtete er in Harzburg. Da sah er im Traum einen furchtbaren Eber, der ihn nach langem Kampfe überwand. Als <pb n="292" xml:id="tg1390.2.2.1"/>
er erwachte, stand das schreckliche Traumbild ihm immer noch vor Augen und keine Vorstellung konnte den Eber ganz verwischen, wenn er auch selbst über seinen Traum lachte. Einige Tage nachher traf er wirklich im Vorharze einen gewaltigen Eber, ganz dem ähnlich, den er im Traume gesehen hatte, an Farbe, an aufsträubenden Borsten, an Größe und an Länge der Fänger. Mit Wildheit, Wuth und Kraft von beiden Seiten begann der Kampf, der lange unentschieden blieb. Seiner Gewandtheit verdankte jedoch Hans von Hackelberg den Sieg und er streckte seinen furchtbaren Feind glücklich nieder. Als er ihn zu seinen Füßen liegen sah, weidete er seine Augen eine Zeitlang an dem Anblick und dann stieß er mit dem Fuß nach seinen schrecklichen Hauern mit dem Ausruf: »Du sollst es mir auch noch nicht thun!« Allein er stieß mit solcher Gewalt, daß der eine der scharfen Zähne den Stiefel durchdrang und ihn am Fuße verwundete.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1390.2.3">Anfangs achtete er die Wunde wenig und setzte die Jagd fort, bis es Nacht wurde. Bei seiner Zurückkunft war aber der Fuß schon so angeschwollen, daß der Stiefel abgetrennt werden mußte. Aus Mangel eines sorgsamen Verbandes verschlimmerte sich aber die Wunde in einigen Tagen so, daß er nach Wolfenbüttel zurückeilte, um Hilfe zu suchen. Aber jede Erschütterung des Wagens war ihm unerträglich und nur mit Mühe erreichte er das Hospital bei Wulperode, einem Amtsdorf nahe bei Hornburg und an der Grenze des Herzogthums Braunschweig gelegen. Seine Asche deckt auf dem dortigen Kirchhofe ein Stein, worauf ein völlig geharnischter Ritter auf einem Maulthiere abgebildet ist. Sonst bewunderten Durchreisende in Wulperode die dort aufgehangene schwere ritterliche Rüstung des Hans von Hackelberg, jetzt ist nur noch der Helm dort zu sehen, alles übrige von der Rüstung ist nach Deersheim gekommen, warum, weiß man nicht.</p>
                </div>
                <div type="footnotes">
                    <head type="h4" xml:id="tg1390.2.4">Fußnoten</head>
                    <note xml:id="tg1390.2.5.note" target="#tg1390.2.1.1">
                        <p xml:id="tg1390.2.5">
                            <anchor xml:id="tg1390.2.5.1" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Provinz Sachsen und Thüringen/332. Hans von Hackelberg der wilde Jäger#Fußnoten_1"/>
                            <ref cRef="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Provinz Sachsen und Thüringen/332. Hans von Hackelberg der wilde Jäger#Fußnote_1" xml:id="tg1390.2.5.2">1</ref> Nach Otmar S. 247 etc.</p>
                    </note>
                </div>
            </div>
        </body>
    </text>
</TEI>