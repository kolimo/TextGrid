<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg1415" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Provinz Sachsen und Thüringen/357. Der Chiromant Nietzky zu Halle">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>357. Der Chiromant Nietzky zu Halle</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Grässe, Johann Georg Theodor: Element 01428 [2011/07/11 at 20:29:59]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Johann Georg Theodor Grässe: Sagenbuch des Preußischen Staates 1–2, Band 1, Glogau: Carl Flemming, 1868/71.</title>
                        <author key="pnd:104076534">Grässe, Johann Georg Theodor</author>
                    </titleStmt>
                    <extent>318-</extent>
                    <publicationStmt>
                        <date notBefore="1868" notAfter="1871"/>
                        <pubPlace>Glogau</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1814" notAfter="1885"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg1415.2">
                <div type="h4">
                    <head type="h4" xml:id="tg1415.2.1">357) Der Chiromant Nietzky zu Halle.<ref type="noteAnchor" target="#tg1415.2.5">
                            <anchor xml:id="tg1415.2.1.1" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Provinz Sachsen und Thüringen/357. Der Chiromant Nietzky zu Halle#Fußnote_1"/>
                            <ptr cRef="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Provinz Sachsen und Thüringen/357. Der Chiromant Nietzky zu Halle#Fußnoten_1"/>
                            <hi rend="superscript" xml:id="tg1415.2.1.2.1">1</hi>
                        </ref>
                    </head>
                    <p xml:id="tg1415.2.2">In der Mitte des vorigen Jahrhunderts lebte in Halle ein Professor, Namens Adam Nietzky, der als Arzt und Naturforscher, mehr aber noch als Chiromant angesehen war. Er hielt nie eine Vorlesung, ohne daß nicht seine Zuhörer sich in einem kleinen Kreise vor seinem Hause sammelten und über seine angeblich dunkeln und sinnlosen Orakelsprüche noch dunkelere und sinnlosere Commentare machten. Unter seinen Zuhörern befand sich nun aber ein aus dem Hannöverschen gebürtiger Student, Namens. T ..., auf den seine Ansichten einen mehr als gewöhnlichen Eindruck machten und der beschloß, sich von dem Professor Nietzky sein Schicksal vorhersagen zu lassen. Er begleitete denselben also nach Hause, betrat dessen Studierstube und eröffnete ihm sein Verlangen. Nietzky hatte bereits manche dunkele Frage an ihn gethan, die er allezeit richtig beantwortet hatte, war auch schon durch Umwege hinter eine Menge von dessen Lebens- und Familienumständen gekommen, endlich ergriff er seine Hand und auf den ersten Blick in dieselbe sagte er: »Er, junger Mensch, – bekannter Maßen war der Doctor sehr grob – wird in einem Jahre von seinem Vater aus Halle zurückgerufen werden, sechs Wochen darauf wird seine Tante mit Tode abgehen, dann wird Er alle Hoffnung zur Erlangung eines Amtes haben, aber ein unvermutheter Zufall wird diese Hoffnung vereiteln und – nun mache Er sich gefaßt, das Schlimmste zu hören – sechs Monate nachher stirbt Er selbst.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg1415.2.3">Hier endete der Wahrsager, ein bestellter Bedienter rief ihn ab, und der ganz verstummt dastehende Studiosus wurde für diesmal entlassen. Man kann sich leicht vorstellen, daß unser Held durch das jetzt Gehörte in eine nicht geringe Bestürzung gerathen sein wird; anfangs dachte er viele Stunden und Tage darüber nach, ja manche Nacht konnte er deshalb nicht schlafen. Allein er war überhaupt von etwas leichtsinniger Gemüthsart; als die Vorlesungen des Professors Nietzky geendigt waren, schwächten die Zeit und andere Beschäftigungen allmählich den Eindruck des Orakelspruchs und es währte nicht lange, so dünkte er sich von seiner begangenen Thorheit und dem Ungrunde der Chiromantie überzeugt. Er sah die Sache mit vollständiger Gleichgültigkeit <pb n="318" xml:id="tg1415.2.3.1"/>
an und erinnerte sich derselben blos noch zuweilen, um darüber zu spotten und zu lachen. Seine akademische Laufbahn ging nun mit dem dritten Jahre zu Ende, seine Wechsel blieben aus und auf Befehl seines Vaters mußte er Halle verlassen. Seine ziemlich bejahrte Tante hatte bereits über 16 Jahre an der Schwindsucht gekränkelt, sie näherte sich bei zunehmender Schwäche dem Grabe und kaum konnte der Anblick ihres zurückgekommenen geliebten Neffen ihr Leben noch um wenige Tage fristen. Sie starb genau sechs Wochen nach dessen Ankunft im Vaterhause. Jetzt fing der junge Mensch an besorgt zu werden, denn des Professors Nietzky Prophezeiung schien sich zu bestätigen. Die Unthätigkeit, worin er bei dem Aufenthalte in seines Vaters Hause lebte, ward ihm zur Last, mehrere seiner Landsleute und Universitätsfreunde wurden nach und nach versorgt und so begannen denn Ehrgeiz und Betriebsamkeit sich auch bei ihm mehr und mehr zu regen. Er bewarb sich bald um diese bald um jene erledigte Stelle, allein jedesmal schlugen ihm seine Absichten und Bemühungen fehl. Endlich ward die Stelle des Stadtschreibers in seiner kleinen Vaterstadt vacant. Es fehlte ihm nicht an Gönnern, sein Vater war selbst einer der ältesten Rathsmänner und viele der übrigen Mitglieder des Magistrats waren seine Blutsverwandten. Diese wünschten und bemüheten sich insgesammt, ihm zu dieser Ehrenstelle zu verhelfen, er wurde auch wirklich erwählt und es war nur noch die Bestätigung dazu vom Hofe erforderlich. Einer der damaligen Minister, welcher gerade diesen District zu seinem Departement und also starken Einfluß auf das Wohl und Wehe dieser Stadt hatte, brachte aber seinen Secretär dazu in Vorschlag und dieser mußte ohne Widerrede angenommen werden. T ... hatte sich also abermals in seinen Hoffnungen getäuscht und so war bis jetzt Alles, was ihm Nietzky prophezeit hatte, haarklein eingetroffen. Jetzt verwandelte sich auf einmal der Spott, den er über dieses Orakel ausgesprochen hatte, in Ernst und er schwebte in beständiger Furcht vor einem ihm nahe bevorstehenden unvermeidlichen Tode. Je näher er der letzten Periode seines Lebens kam, desto stärker wuchs seine Besorgniß, seine geschäftige Einbildungskraft wirkte stets auf seinen Körper, jedes Gefühl einer Unverdaulichkeit, jede Stockung des Geblüts, jede fleißig von ihm bemerkte Veränderung des Pulsschlages, jede zurückgetretene Ausdünstung ward dadurch verdoppelt und diese so nachtheiligen Wirkungen der Phantasie verwandelten gar bald die bisher nur eingebildete in eine wirkliche Krankheit. Ein schneller Witterungswechsel verursachte aber damals eine Menge Katarrhe und Rheumatismen in der Geburtsstadt unseres jungen Mannes. Auch er ward von diesem Uebel befallen und war der einzige, der trotz aller guten Vorschriften und trotz aller bewährten Beihilfe der erfahrensten Aerzte an diesem unbeträchtlichen Zufalle starb. So bewahrheitete sich auch hier die Wahrheit der Nietzky'schen Prophezeiungen.</p>
                </div>
                <div type="footnotes">
                    <head type="h4" xml:id="tg1415.2.4">Fußnoten</head>
                    <note xml:id="tg1415.2.5.note" target="#tg1415.2.1.1">
                        <p xml:id="tg1415.2.5">
                            <anchor xml:id="tg1415.2.5.1" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Provinz Sachsen und Thüringen/357. Der Chiromant Nietzky zu Halle#Fußnoten_1"/>
                            <ref cRef="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Provinz Sachsen und Thüringen/357. Der Chiromant Nietzky zu Halle#Fußnote_1" xml:id="tg1415.2.5.2">1</ref> S. Reichard, Vermischte Beiträge zur Beförderung einer nähern Einsicht in das Geisterreich, Bd. I. S. 221 etc.</p>
                    </note>
                </div>
            </div>
        </body>
    </text>
</TEI>