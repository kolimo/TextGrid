<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg1468" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Provinz Sachsen und Thüringen/410. Die Magdalenenkapelle zu Erfurt">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>410. Die Magdalenenkapelle zu Erfurt</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Grässe, Johann Georg Theodor: Element 01481 [2011/07/11 at 20:29:59]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Johann Georg Theodor Grässe: Sagenbuch des Preußischen Staates 1–2, Band 1, Glogau: Carl Flemming, 1868/71.</title>
                        <author key="pnd:104076534">Grässe, Johann Georg Theodor</author>
                    </titleStmt>
                    <extent>349-</extent>
                    <publicationStmt>
                        <date notBefore="1868" notAfter="1871"/>
                        <pubPlace>Glogau</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1814" notAfter="1885"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg1468.2">
                <div type="h4">
                    <head type="h4" xml:id="tg1468.2.1">410) Die Magdalenenkapelle zu Erfurt.<ref type="noteAnchor" target="#tg1468.2.5">
                            <anchor xml:id="tg1468.2.1.1" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Provinz Sachsen und Thüringen/410. Die Magdalenenkapelle zu Erfurt#Fußnote_1"/>
                            <ptr cRef="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Provinz Sachsen und Thüringen/410. Die Magdalenenkapelle zu Erfurt#Fußnoten_1"/>
                            <hi rend="superscript" xml:id="tg1468.2.1.2.1">1</hi>
                        </ref>
                    </head>
                    <p xml:id="tg1468.2.2">Vor langer Zeit lebte zu Erfurt ein Leinweber, der eine ebenso schöne als fromme Tochter hatte, die, während Andere ihres Gleichen Sonntags spazieren oder zum Tanze gingen, zu Hause saß und spann. Was sie an diesen dem Dienst des Herrn geweihten Tagen aber durch den Fleiß ihrer Hände erübrigte, das legte sie in ein Sparbüchslein und verwendete es lediglich zu Almosen. Natürlich fehlte es ihr dann nicht an Lob und Ruhm von Seiten der armen Leute, denen ihr Fleiß zu Gute kam, und so kam es auch, daß viele den besten Familien der Stadt angehörige Jünglinge das schöne Mädchen zu sehen wünschten, von dem so viel gesprochen ward. Dabei blieb es auch nicht. Viele warben um ihre Hand, allein sie hatte sich einen himmlischen Bräutigam erkoren und hielt es für besser, der Jungfrau Maria ihr Leben zu weihen. Da begab es sich, daß in der Stadt Erfurt eine schwere Theuerung ausbrach und daß Mancher Andere um Brod ansprechen mußte, der früher selbst den Armen gegeben hatte. Der Bettler wurden an jedem Tage mehr und der Leinweber selbst sah seinen eigenen Broderwerb auch geschmälert, denn man kaufte nur das Allernöthigste, und so ward er genöthigt, selbst zu den Sparpfennigen seiner frommen Tochter seine Zuflucht zu nehmen. Allein diese waren auch gewaltig zusammengeschmolzen, denn Lenchen, so hieß sie, ließ Niemand vergebens bei sich um eine Gabe anpochen und als die letzten Pfennige kaum noch den Boden ihrer Sparbüchse bedeckten, da grämte sie sich und sorgte, nicht etwa darüber, daß sie nun möglicher Weise selbst in die Lage kommen könne, zu hungern, sondern darum, daß sie nun genöthigt sein werde, die Armen unbeschenkt von ihrer Thüre weggehen zu sehen. Da warf sie sich eines Nachts, wo sie am Spinnrocken saß und weinend der Zukunft dachte, vor dem in ihrem Kämmerlein aufgehängten Marienbilde nieder und flehte, es möge der heil. Jungfrau gefallen, sich ihrer anzunehmen und ihr die Mittel, Armen wohlzuthun, auch für die Zukunft gewähren. Und die Himmlische erhörte ihr Gebet, sie stieg von strahlendem Licht umgeben aus dem Bilde herab zu ihr und reichte ihr eine Tasche von wunderbarem Gewebe und sprach zu ihr: »Du treue Magd, thue hinfort, wie Du bis diese Stunde gethan, so wird Dir meine Hilfe in jedem Drangsal nahe sein.« Segnend legte sie ihre Hände auf das Haupt der Jungfrau und verschwand. Lenchen griff nunmehr in die Tasche und fand darin 3 Goldgülden, die sie freudig in der Hand wog, indem sie im Geiste sofort berechnete, wieviel Arme sie damit sattmachen könne. Kaum konnte sie die Morgenstunde erwarten, wo sie zu dem Bäcker eilte und für einen dieser Goldgülden Marcusbrode einkaufte, dann stellte sie sich unter die Hausthüre und jeden Armen, der vorüberging, den rief sie an und reichte ihm, so lange ihr Vorrath dauerte, <pb n="349" xml:id="tg1468.2.2.1"/>
ein solches Brod, indem sie allen Dank mit den Worten abwies: »Die Gabe kommt nicht von mir, sondern von der heil. Jungfrau Maria, die hat sie Euch gespendet und mich hat sie gewürdigt, daß ich ihre Armenpflegerin sein darf. Betet zu ihr, so wird sie uns Allen fernerhin helfen in dieser trüben Zeit.« Am andern Morgen wollte sie wieder Brod für die Armen einkaufen, griff also in die Tasche, um den zweiten Goldgülden herauszunehmen. Allein wie erstaunte sie, als sie auch den gestern ausgegebenen wieder darin fand. Da erschrack sie gar sehr, und weil sie irrig der Meinung war, sie habe aus Versehen bei dem Bäcker den schon bezahlten Goldgülden wieder eingesteckt, so eilte sie zu ihm, um ihn deshalb zu befragen. Allein derselbe lachte, wie sie nur so etwas denken könne, und zeigte ihr zu mehrerer Bekräftigung seiner Worte den gestern empfangenen Goldgülden. Da merkte sie wohl, wie es war und daß sie eine Tasche mit unerschöpflichem Inhalte erhalten habe. Jeden Morgen wiederholte sie nun ihren Gang zum Bäcker und ihre Brodaustheilung an die Armen, ging aber auch selbst in der Stadt herum in die Wohnungen der Kranken, die nicht zu ihr kommen konnten, und brachte ihnen Speise und Trank. Denn wie oft sie auch ihre Goldgülden ausgab: griff sie wieder in die Tasche, war auch die Zahl der drei wieder voll. Die Fragen aber ihrer Eltern, woher ihr dieser Reichthum gekommen, beschwichtigte sie damit, daß sie sagte, auf ihrem Gelde ruhe Gottes Segen und es werde nie weniger.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1468.2.3">Unzählig waren die Armen, denen sie durch ihre Gaben das Leben erhielt, unzählig die Kranken, denen durch ihre Hilfe Genesung zu Theil ward. Darum ward auch die Schwelle ihres Vaterhauses nicht leer von Bittenden und in kurzer Zeit galt die Weberstochter in ganz Erfurt für eine halbe Heilige. Als nun die Hungersnoth vorüber war, da ward die Sehnsucht in der Jungfrau immer lebhafter, sich nur dem Dienste der heil. Jungfrau zu weihen. Sie nahm den Schleier in einem Nonnenkloster der Stadt und ward bald ein Vorbild der Frömmigkeit für ihre Schwestern. Nach einiger Zeit beschloß sie aus ihrer unerschöpflichen Tasche selbst ein Kloster zu bauen und so entstand das nach ihr benannte Magdalenenkloster, wo man noch lange die wunderbare Tasche aufbewahrte, bis dasselbe einst durch Feindeshand zerstört ward. Jetzt steht an seiner Stelle eine kleine Kirche, die Magdalenenkapelle, in welcher jährlich einmal Messe gehalten wird. Diese bewahrt den Namen der Jungfrau und ihre Frömmigkeit bis auf die spätesten Geschlechter.</p>
                </div>
                <div type="footnotes">
                    <head type="h4" xml:id="tg1468.2.4">Fußnoten</head>
                    <note xml:id="tg1468.2.5.note" target="#tg1468.2.1.1">
                        <p xml:id="tg1468.2.5">
                            <anchor xml:id="tg1468.2.5.1" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Provinz Sachsen und Thüringen/410. Die Magdalenenkapelle zu Erfurt#Fußnoten_1"/>
                            <ref cRef="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Provinz Sachsen und Thüringen/410. Die Magdalenenkapelle zu Erfurt#Fußnote_1" xml:id="tg1468.2.5.2">1</ref> Nach Ziehnert Bd. I. S. 272. etc.</p>
                    </note>
                </div>
            </div>
        </body>
    </text>
</TEI>