<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg1541" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Provinz Sachsen und Thüringen/483. Die Sage vom Schlosse Bodenstein">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>483. Die Sage vom Schlosse Bodenstein</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Grässe, Johann Georg Theodor: Element 01554 [2011/07/11 at 20:29:59]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Johann Georg Theodor Grässe: Sagenbuch des Preußischen Staates 1–2, Band 1, Glogau: Carl Flemming, 1868/71.</title>
                        <author key="pnd:104076534">Grässe, Johann Georg Theodor</author>
                    </titleStmt>
                    <extent>423-</extent>
                    <publicationStmt>
                        <date notBefore="1868" notAfter="1871"/>
                        <pubPlace>Glogau</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1814" notAfter="1885"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg1541.2">
                <div type="h4">
                    <head type="h4" xml:id="tg1541.2.1">483) Die Sage vom Schlosse Bodenstein.<ref type="noteAnchor" target="#tg1541.2.6">
                            <anchor xml:id="tg1541.2.1.1" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Provinz Sachsen und Thüringen/483. Die Sage vom Schlosse Bodenstein#Fußnote_1"/>
                            <ptr cRef="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Provinz Sachsen und Thüringen/483. Die Sage vom Schlosse Bodenstein#Fußnoten_1"/>
                            <hi rend="superscript" xml:id="tg1541.2.1.2.1">1</hi>
                        </ref>
                    </head>
                    <p xml:id="tg1541.2.2">In dem preußischen Untereichsfelde erhebt sich ein kleines Gebirge, das sogenannte Ohmgebirge, in der Nähe der Grafschaft Hohnstein. Es bildet ein unregelmäßiges Viereck, oben aber ein Plateau von ohngefähr einer halben Quadratmeile. Darauf befinden sich die beiden von Wintzingerodischen Stammgüter Bodenstein und Adelsborn, sowie die Markungen der beiden Dörfer Kirch- und Kaltohmfeld. Dicht am Fuße seiner südwestlichen Ecke liegt das preußische Städtchen Worbis, am Fuße der nordwestlichen aber das hannöversche Städtchen Duderstadt. Der schönste Schmuck dieses Gebirges ist aber das alte Schloß Bodenstein. Hier lebte zu Anfange des 16. Jahrhunderts ein Ritter Barthold von Wintzingerode, ein tapferer und unbeugsamer Mann, der als im Bauernkriege Thomas Münzer's Bande vor dasselbe zog, um es zu brechen, lieber das Leben zu verlieren, als es dieser zu überliefern beschloß. Alle Versuche der Belagerer schlugen fehl und ob sie auch den Plan machten, die Feste durch Hunger zu bezwingen und sich deshalb auf dem nächsten südlichen Vorgebirge, welches noch bis heute davon die Mühlhäuser Burg genannt wird, festsetzten, gelang es ihnen doch nicht; die Jungfräulichkeit der Feste ward gerettet und die Raubhorden mußten unverrichteter Weise abziehen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1541.2.3">Tapferkeit war jedoch die einzige Tugend des genannten Ritters Barthold, denn sonst war er weiter nichts als ein gewöhnlicher Raubritter. So ist z.B. ein Verzeichniß der Beschwerden, welche dereinst die Stadt Worbis bei den Mainzischen Räthen – unter der Oberhoheit von Mainz stand nämlich seine <pb n="423" xml:id="tg1541.2.3.1"/>
Burg – einreichte, noch jetzt vorhanden. Auf seine Gegner zu lauern, mit seinen wilden Knappen den Burgberg hinabzueilen, die Feinde niederzuwerfen und ihnen Leben und Freiheit zu nehmen, in dunkler Nacht, wenn der Sturm durch die hohen Bäume fegte, friedlich vorüberziehende Reisende, gleichviel ob Geistliche oder Bürger, zu überfallen, darin bestand sein Leben, Ruhe war sein Tod. Endlich aber schlug auch seine Stunde, der wilde Ritter wurde, weil er zu oft den Landfrieden gebrochen, in Mainz enthauptet. Zwar ward sein Leib in geweihter Erde bestattet, aber sein wilder Geist hat keine Ruhe gefunden, er hat sein Besitzthum, das Schloß Bodenstein, wieder aufgesucht, und Nachts, wenn der Mond mit seinem blassen, geisterhaften Scheine in die Gemächer blickt und wunderliche, phantastische Schatten an die Wände zeichnet, da wird es lebendig in den Gemächern, es schallen Tritte aus der Ferne, Sporenklang wird laut und es erscheint die hohe ernste Gestalt Ritter Barthold's von Wintzingerode. Aus einem unterirdischen Gange steigt er eine Treppe hinan, schaut wildrollenden Auges umher und wenn er in einem gewissen Gemache Gäste findet, da bemächtigt sich seiner eine grenzenlose Wuth. Mit riesenstarkem Arme ergreift er den Kühnen, der es gewagt hat, in den Kreis zu kommen, in welchem er allnächtlich waltet, wirft ihn mit wildem Hohngelächter die steile Treppe hinab und der Verwegene konnte sich glücklich schätzen, wenn er sein Leben aus den Händen des gespenstigen Ritters rettete. Als man in späterer Zeit diese Treppe, welche mit dem unterirdischen Gange, dessen Eingang noch vorhanden ist, in Verbindung stand, um den Spuk zu enden wegnahm, da warf der Ritter, sobald Jemand in dem unheimlichen Gemache schlief, sein eigenes Bild mit lautem Gepolter von der Wand, bis endlich dasselbe mit einem starken Nagel an die Wand angeheftet ward.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1541.2.4">Außer diesem bösen Geist gab es aber noch einen zweiten sanftern in dem alten Schlosse. Dies ist die Ahnfrau des ganzen, jetzt lebenden, in die beiden Linien Bodenstein und Adelsborn getheilten Geschlechtes, Anna Susanna, eine geborene von Barby-Koburg. Sie wandelte in stiller Trauer umher und suchte nach einer goldenen Brautkrone, die abhanden gekommen sein sollte, schaute aber noch nebenbei um, wie es in dem Schlosse, wo sie so lange als Burgfrau waltete, stehe. Mägden, welche still und fleißig ihre Arbeit machten, spann sie bei nächtlicher Weile den Rocken ab und treuen Knechten klopfte sie lobend auf die Backen. Einem derselben schien sie besonders gewogen, denn sie erschien ihm oft und sah ihn mit freundlichen Blicken an. Besonders häufig aber gewahrte er sie an einem Fenster der Burgmauer neben der Zugbrücke, und es mußte auch selbst den Freidenkenden überraschen, wenn er von der Kleidung der spukenden Burgfrau eine zwar nicht mit ihrem noch vorhandenen Bilde, wohl aber mit dem häuslichen Kostüme ihrer Zeit übereinstimmende Beschreibung machte, bei welcher auch nicht das Geringste vergessen war. Seitdem aber eine neue goldene Brautkrone angeschafft wurde, ist auch Frau Anna Susanna nicht wieder sichtbar geworden.</p>
                </div>
                <div type="footnotes">
                    <head type="h4" xml:id="tg1541.2.5">Fußnoten</head>
                    <note xml:id="tg1541.2.6.note" target="#tg1541.2.1.1">
                        <p xml:id="tg1541.2.6">
                            <anchor xml:id="tg1541.2.6.1" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Provinz Sachsen und Thüringen/483. Die Sage vom Schlosse Bodenstein#Fußnoten_1"/>
                            <ref cRef="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Provinz Sachsen und Thüringen/483. Die Sage vom Schlosse Bodenstein#Fußnote_1" xml:id="tg1541.2.6.2">1</ref> S. Thüringen und der Harz Bd. V. S. 18 etc.</p>
                    </note>
                </div>
            </div>
        </body>
    </text>
</TEI>