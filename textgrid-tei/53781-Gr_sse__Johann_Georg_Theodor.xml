<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg1653" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Der Harz/574. Die Schäferthürme in Quedlinburg">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>574. Die Schäferthürme in Quedlinburg</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Grässe, Johann Georg Theodor: Element 01668 [2011/07/11 at 20:29:59]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Johann Georg Theodor Grässe: Sagenbuch des Preußischen Staates 1–2, Band 1, Glogau: Carl Flemming, 1868/71.</title>
                        <author key="pnd:104076534">Grässe, Johann Georg Theodor</author>
                    </titleStmt>
                    <extent>519-</extent>
                    <publicationStmt>
                        <date notBefore="1868" notAfter="1871"/>
                        <pubPlace>Glogau</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1814" notAfter="1885"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg1653.2">
                <div type="h4">
                    <head type="h4" xml:id="tg1653.2.1">574) Die Schäferthürme in Quedlinburg.<ref type="noteAnchor" target="#tg1653.2.8">
                            <anchor xml:id="tg1653.2.1.1" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Der Harz/574. Die Schäferthürme in Quedlinburg#Fußnote_1"/>
                            <ptr cRef="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Der Harz/574. Die Schäferthürme in Quedlinburg#Fußnoten_1"/>
                            <hi rend="superscript" xml:id="tg1653.2.1.2.1">1</hi>
                        </ref>
                    </head>
                    <p xml:id="tg1653.2.2">Einst haben zwei Schäfer zu Quedlinburg, beides arme und redliche Leute, ihre Heerde neben einander auf die grüne Weide getrieben. Es war ein lieblicher Morgen, die Vögel sangen so schön im nahen Walde, die Lämmchen hüpften so lustig über die grünen Wiesen, die Lüfte wehten so mild und von dem säuselnden Morgenwinde getragen drang der Schall der Glocken von der nahen Stiftskirche zu ihnen herüber, daß ihnen das Herz aufging und sie voll Inbrunst und Andacht an den dachten, dem zu Ehren dieser erhabene Ton erklang. »O wie Schade«, sprach der Vater, »daß das Gotteshaus, welches sie jetzt in unserer Neustadt erbaut haben, noch so lange ohne Thurm und Glocken bleiben wird, wie Schade, daß ein Werk zu Gottes Ehre unbeendigt bleiben muß aus so irdischen Hindernissen, wie Geldnoth, während doch die Reichen sich täglich mehr kostbare Paläste aufbauen.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg1653.2.3">»Mein Vater«, unterbrach jetzt der Sohn die Rede desselben, »wo sind unsere Hunde geblieben? Ich sehe sie nicht mehr und wir müssen suchen, sie <pb n="519" xml:id="tg1653.2.3.1"/>
wieder zu finden.« – »Da drüben, mein Sohn«, sagte der Vater, »sehe ich beide im raschesten Laufe dem Walde zueilen, gewiß hat sie die Spur eines Wildes zum Verfolgen gereizt.« Sie pfiffen laut den Enteilenden nach, allein diese ließen sich nicht mehr zum Umkehren bewegen, zwar drehten sie die Köpfe nach ihren Herren, allein scheinbar mehr in der Absicht, sie aufzufordern, ihnen zu folgen, als auf ihr Pfeifen zurückzukehren. Es blieb also den Hirten nichts weiter übrig, als ihnen nachzueilen und ihre Schafe, denen übrigens hier nichts geschehen konnte, allein zurückzulassen. Der Sohn eilte dem Vater voran, weil Letzterer immer noch ein wachsames Auge auf seine Heerde warf. So war jener ihm ganz aus den Augen verschwunden, da glaubte er aus dem Walde nicht allein das Bellen der Hunde, sondern auch das ängstliche Rufen seines Sohnes zu hören. Schnell eilte er ihm jetzt nach, allein der Weg, den er zu nehmen hatte, war ihm völlig unbekannt, obwohl er schon oft in dem Walde gewesen war. Auch schien der Wald ihm selbst sehr verändert, an der Stelle der jungen schlanken Bäumchen, die er sonst dort gesehen, streckten mächtige uralte Eichen ihre breiten Kronen empor, und zwischen ihren Stämmen schimmerten ihm aus einer Lichtung der Bäume graue verfallene Gemäuer entgegen, wie von einer zerstörten Kirche. An dem Eingange in diese Ruinen, welche wildes rankendes Gestrüpp verdeckte und unzugänglich machte, sah er seinen Sohn mit erstaunter Miene und gespannter Erwartung stehen, denn auch dieser war zwar früher oft schon hier gewesen, aber niemals hatte er dieses Gemäuer bemerkt. Da nun auch die Fährte der Hunde in dasselbe hineinführte, so siegte bald bei ihnen die Neugierde über die Angst, sie bahnten sich mit Anstrengung einen Weg durch das Gestrüpp und gelangten an ein hohes, aber auf der einen Seite niedergestürztes Portal, welches offenbar den Eingang zu der Kirche gebildet hatte. Einzelne Theile der gewölbten Decke waren noch erhalten, aber Alles drohte den Einsturz und nur mit Zittern und Zagen nahten sie sich dem nahstehenden Altar, auf dem sie jedoch noch ein uraltes Krucifix erblickten. Sie beugten andächtig ihre Kniee vor dem Bilde und murmelten ein Gebet. Da schreckte sie ein Geräusch auf, um sich blickend gewahrte sie ihre Hunde hinter den Altartrümmern, wo diese kratzten und scharrten, ohne sich um die Nähe ihrer Herren zu kümmern, gleichsam als habe ein Zauber sie an diese Stelle gefesselt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1653.2.4">Die Hirten traten zu der Stelle, wo sie scharrten, und betrachteten aufmerksam die immer großer werdende Oeffnung in dem Boden, in welchem bald, von der Erde entblößt, eine große eiserne Kiste sichtbar ward. Die Hunde bellten, wie vor Freude über den Fund, hielten einen Augenblick mit ihrer Arbeit inne und sprangen wedelnd an ihren Herren auf. Diese bemühten sich die Kiste herauszuheben und zu öffnen. Sie war schwer, sehr schwer; ihr Inneres barg eine Masse von Gold- und Silbermünzen von uraltem, fremdartigem Gepräge. Ehe sie sich aber noch von ihrer Verwunderung erholen konnten, fingen die Hunde noch einmal an auf derselben Stelle zu kratzen und bald kam eine zweite Kiste zum Vorschein, aus welcher, als die Hirten sie geöffnet hatten, ihnen goldene Becher und Leuchter und andere Geräthschaften von unschätzbarem Werthe entgegenblickten. Erst jetzt waren die Hunde beruhigt und scharrten nicht mehr, sondern liefen vielmehr eiligst zu der verlassenen Heerde zurück, um dort ihren gewohnten Dienst zu verrichten. <pb n="520" xml:id="tg1653.2.4.1"/>
Die Schäfer aber folgten ihnen langsam, erst die eine, dann die andere Kiste mühsam mit sich forttragend.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1653.2.5">Noch am selbigen Abend begaben sich beide zur Aebtissin des Stiftes St. Servatii zu Quedlinburg, erzählten ihr diese Begebenheit und gaben ihr zugleich den Wunsch zu erkennen, daß von dem Schatze zwei Thürme an der neuerbauten Nicolaikirche aufgeführt werden möchten.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1653.2.6">Als die Begebenheit bekannt wurde, zogen eine Menge Leute in den Wald hinaus, um die Stelle zu sehen, wo die Schäfer den Schatz gefunden, aber keine Ruinen waren dort zu finden, spurlos waren sie verschwunden, selbst das Auge des Schäfers fand die Stelle nicht wieder, wo sie gestanden. Hätte der Schatz in ihrem Besitze nicht das Gegentheil bewiesen, so würden sie das Geschehene für einen Traum gehalten haben. Die Kirche zu St. Nicolaus, von hohen Linden umgeben, steht noch und an ihrer Abendseite erheben sich die beiden von dem Schatze der Schäfer (um die Mitte des 13. Jahrhunderts) erbauten Thürme. Die Gestalten der Schäfer und ihrer Hunde, in Stein gehauen und aus Dankbarkeit auf die Mauern der Thürme gesetzt, schauen noch heute von dort hernieder und ihre Schäferpelze und andere Kleidungsstücke wurden ehedem in der Sakristei daselbst verwahrt.</p>
                </div>
                <div type="footnotes">
                    <head type="h4" xml:id="tg1653.2.7">Fußnoten</head>
                    <note xml:id="tg1653.2.8.note" target="#tg1653.2.1.1">
                        <p xml:id="tg1653.2.8">
                            <anchor xml:id="tg1653.2.8.1" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Der Harz/574. Die Schäferthürme in Quedlinburg#Fußnoten_1"/>
                            <ref cRef="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Der Harz/574. Die Schäferthürme in Quedlinburg#Fußnote_1" xml:id="tg1653.2.8.2">1</ref> S. Sagen und Geschichten aus der Vorzeit des Harzes S. 45 etc.</p>
                    </note>
                </div>
            </div>
        </body>
    </text>
</TEI>