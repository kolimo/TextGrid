<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg1668" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Der Harz/589. Die Sagen vom Schlosse Quästenberg">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>589. Die Sagen vom Schlosse Quästenberg</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Grässe, Johann Georg Theodor: Element 01683 [2011/07/11 at 20:29:59]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Johann Georg Theodor Grässe: Sagenbuch des Preußischen Staates 1–2, Band 1, Glogau: Carl Flemming, 1868/71.</title>
                        <author key="pnd:104076534">Grässe, Johann Georg Theodor</author>
                    </titleStmt>
                    <extent>539-</extent>
                    <publicationStmt>
                        <date notBefore="1868" notAfter="1871"/>
                        <pubPlace>Glogau</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1814" notAfter="1885"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg1668.2">
                <div type="h4">
                    <head type="h4" xml:id="tg1668.2.1">589) Die Sagen vom Schlosse Quästenberg.<ref type="noteAnchor" target="#tg1668.2.8">
                            <anchor xml:id="tg1668.2.1.1" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Der Harz/589. Die Sagen vom Schlosse Quästenberg#Fußnote_1"/>
                            <ptr cRef="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Der Harz/589. Die Sagen vom Schlosse Quästenberg#Fußnoten_1"/>
                            <hi rend="superscript" xml:id="tg1668.2.1.2.1">1</hi>
                        </ref>
                    </head>
                    <p xml:id="tg1668.2.2">Unter die bedeutende Anzahl von Ruinen alter Ritterburgen, welche das Harzgebirge einst krönten, gehören auch die von Quästenberg. Man findet sie an der mittägigen Seite in der Grafschaft Stolberg, wo sie, eine Stunde von Roßla entfernt, dicht über dem Dorfe Quästenberg liegen. Der Ort, wo sie stand, ist zu einem Lug ins Land oder einer Raubburg wie geschaffen; <pb n="539" xml:id="tg1668.2.2.1"/>
der Berg, auf welchem sie liegt, erhebt sich ziemlich steil, zu einer Höhe von 3-400 Fuß. Er ist fast ringsum von höhern Bergen umkränzt, welche die Burg schützten und versteckten. Nur auf einer Seite bietet ein enges Thal der in der Ferne kaum bemerkbaren Burg eine freie Aussicht dar, denn hier sieht man quer durch die goldene Aue und am Ende des Horizonts den Bergzug, auf dem die Ruinen des Kiffhäusers liegen. Diese Burg hieß indeß früher Finsterberg und erhielt erst im 13. Jahrhundert den Namen Quästenberg und zwar aus folgendem Grunde.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1668.2.3">Es wohnten nämlich hier als Burgherren die Herren von Knut (oder Knaut), von diesen hatte einer ein kleines Töchterchen, welches er, da es ihm die alleinige Frucht seiner Ehe zu sein schien, unbeschreiblich liebte. Das Kind spielte wie alle Kinder gern mit Blumen und pflückte deren sich immer vor den Thoren der Burg, im Gesträuch und im nahen Walde. Die Wärterin, gewohnt, daß das Kind tief ins Gebüsch hinein sich verlor und oft lange ausblieb, weilte ruhig vor des Thores Pforte, bis es zurückkam. Dies ging auch lange Zeit ganz gut an, allein eines Abends blieb das nun vierjährige Kind ungewöhnlich lange aus. Die Nacht kam herauf, das Kind kam nicht zurück; die Wärterin lief in den Wald, rief es beim Namen, schrie und klagte, aber umsonst, das Kind kam nicht zurück. Voller Bestürzung eilte sie ins Schloß zurück, erzählte, was geschehen war und Alles in der Burg versank in tiefe Trauer und der bekümmerte Vater sandte seine Knappen nach allen Gegenden aus und ließ die Gemeinden seiner Dörfer aufbieten, das verlorene Kind zu suchen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1668.2.4">Das Kind hatte sich durch immer schönere Blumen immer tiefer in den Wald locken lassen, war in ein finsteres Thal gekommen, durch das kein Weg führte und endlich zu einer Köhlerhütte. Hier hatte es sich vor der Thüre niedergesetzt und drehte eben mit seinen zarten Fingerchen einen Blumenkranz zusammen, den man damals vermuthlich von den herabhängenden Blumenquasten Quäste hieß. Hier fand es der zurückkehrende Köhler; das Kind, das er nicht kannte und dessen Namen er von demselben auch nicht erfahren konnte, lächelte ihn freundlich wie einen alten Bekannten an und der Köhler küßte es, als wenn es sein eigenes wäre, hob es auf und trug es in seine enge Hütte und pflegte sein. Das kleine Mädchen zeigte jedoch am andern Tage kein Verlangen, zu seinem Vater zurückzukehren, denn es fand hier, wie vor der väterlichen Burg Blumen in Menge, täglich lief es um die Hütte herum, pflückte beide Hände voll, kehrte zurück, saß dann vor dem Eingange und flocht sie zu Kränzen. So fanden es endlich einige Tage nachher einige Leute aus dem Dorfe Quästenberg; groß war ihre Freude, jubelnd und singend nahmen sie die Kleine auf, banden den Blumenkranz oder die Quäste an eine Stange, trugen dieselbe vor sich her und der Köhler mußte mitgehen. Indessen saß der Vater des Kindes von früh bis spät auf dem Söller seiner Burg und schaute aus, ob nicht Jemand käme und ihm Nachricht von seinem Töchterchen brächte. Ach welche Freude, als er jetzt im Thale die Menschen mit dem Kranze und seinem Mädchen heranziehen sah, und der Vater und sein ganzes Burggesinde standen schon lange unten am Burgthore, ehe jene den steilen Weg heraufkommen, der Ritter riß ihnen sein Kind aus den Armen und trug es hinein in die Burg, die Stange mit dem Blumenkranze wurde im Burghofe ausgepflanzt und die Knappen und <pb n="540" xml:id="tg1668.2.4.1"/>
Bauern zechten um sie herum bis tief in die Nacht. Zum dankbaren Andenken schenkte der glückliche Vater den Quästenbergern einen Strich Waldes und der Gemeinde des ihm auch gehörigen Dorfes Roda (jetzt zu Mansfeld gehörig und anderthalb Stunden von Quästenberg entfernt) den Holzfleck, wo das Kind vor der Köhlerhütte gefunden ward. Das Holz ist nachher ausgerodet und in eine Wiese verwandelt worden, die noch gegenwärtig die Fräulein-Wiese heißt und zu den Grundstücken des Pfarrers in Roda gehört.<ref type="noteAnchor" target="#tg1668.2.10">
                            <anchor xml:id="tg1668.2.4.2" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Der Harz/589. Die Sagen vom Schlosse Quästenberg#Fußnote_2"/>
                            <ptr cRef="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Der Harz/589. Die Sagen vom Schlosse Quästenberg#Fußnoten_2"/>
                            <hi rend="superscript" xml:id="tg1668.2.4.3.1">2</hi>
                        </ref> Von dem Kranze aber nannte der Burgherr sein Schloß Quästenberg und bestimmte für alle Zeit ein Volksfest für seine Dienstmannen, wobei ein Kranz oder eine Quäste an der größten Eiche des höchsten Berges in der Gegend befestigt ward, um weithin gesehen zu werden. Es war aber dieses Volksfest auf folgende Weise beschaffen. Um dritten Pfingstfeiertage vor Aufgang der Sonne bringen die jungen Bursche des Dorfes Quästenberg die größte Eiche, welche sie in dem dortigen ansehnlichen Forste auffinden konnten, unter einem kaum zu zählenden Haufen jauchzender Zuschauer aus der ganzen Gegend, von Trompeten und Hörnern begleitet, den hohen Berg hinan, der auf die Ruinen der Quästenburg herabsieht. Diese Eiche haben sie aber bereits am Tage vor dem Pfingstfeste abgehauen und ihr eine halbe Elle vom Stamme alle Aeste genommen, so zwar, daß das abgehauene Holz ihr Eigenthum wird. Sie müssen aber dem Herkommen nach den ungeheuren Baum blos mit den Händen den Berg heranwälzen oder heraufziehen. Oben auf dem Gipfel des Berges, welcher die Gegend beherrscht, wird dann der Baum aufgerichtet und an einem Querbalken ein großer Kranz, von Baumzweigen geflochten, der einem Wagenrade gleicht, befestigt und Alles ruft: »Die Quäste hängt!« Dann wird oben auf dem Berge getanzt, welches die Hauptbelustigung ist. Nach einigen Stunden zieht die ganze versammelte Menge unter weitschallender Musik in Prozession den Berg hinab und nach dem Hause des Predigers in Quästenberg, den sie zu einem feierlichen Gottesdienst in der Kirche abholen, womit sich das Volksfest endigt. Die Eiche, welche nach dem Fällen verkauft wird und die Kosten des Festes trägt, bleibt ein Jahr lang auf dem Berge aufgerichtet stehen. Den großen Kranz von Baumzweigen aber nennen die Bewohner der Gegend: die Quäste. Mit dem Immerseltnerwerden des Holzes ist jedoch eine Aenderung eingetreten, jetzt dürfen sie immer nur alle acht Jahre einen neuen Baum aussuchen, erhalten aber dafür in den andern sieben Jahren jedes Mal 8 Thaler als eine Vergütung. Im achten Jahre findet aber die Aufrichtung des Baumes in der erwähnten Art statt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1668.2.5">Im 30jährigen Kriege stand das Schloß noch und man sagt, daß namentlich aus dem Dorfe Quästenberg sich viele Bauern hinaufflüchteten und dort ihre Habseligkeiten verbargen. Viele von den damals dorthin gebrachten Schätzen sollen aber noch dort liegen und zwar jetzt, nachdem das Schloß selbst verfallen ist, in einem großen Braukessel tief unten in einem unterirdischen Gewölbe, freilich aber von einem Gespenst gehütet. Einst ging einmal des Sonntags ein Einwohner aus Quästenberg auf die Burg, kroch aus <pb n="541" xml:id="tg1668.2.5.1"/>
langer Weile in den Ruinen herum und kam auch an eine Stelle, wo es tief in die Erde hinabging. Er drängte sich durch das dichte Gestrüpp, ging immer mehr abwärts und kam endlich an einen dunkeln Gang. Die Neugierde ließ ihn weiter gehen und so gewahrte er endlich im Hintergrunde, wohin kaum noch ein Schimmer von Tageslicht fiel, eine runde Oeffnung in dem Boden. Als er dicht vor dieser stand, erschien plötzlich ein Geist in einen Schleier gehüllt. Es wurde hell und der erschrockene Mann sah vor sich den Braukessel mit puren Goldstücken angefüllt, von dem ihm gar oft schon seine Großmutter erzählt hatte. Ob er gehen oder nehmen solle, das wußte er nicht. Da sprach der Geist mit heller Stimme: »Nimm eins der Goldstücke, komm alle Tage wieder und nimm Dir eins, aber nie mehr als eins!« und verschwand. Der Mann nahm auch eins der Goldstücke, steckte es hurtig ein und eilte mit klopfendem Herzen vor Angst und Freude nach der Oeffnung zurück, merkte sich aber den Ort genau und ging, das Geschenk des Geistes wieder und immer wieder besehend, nach seiner Wohnung zurück. Tags darauf kam er wieder, der Geist war zwar nicht da, wohl aber der Braukessel voll Gold. Er nahm sich wieder ein Stück. Den dritten, vierten und fünften Tag fand er sich wieder ein, holte immer ein Goldstück, und so trieb er's fort wohl ein Jahr lang. Seine Hütte hatte er während dessen in ein stattliches Haus umgewandelt, sich viele Aecker und Wiesen gekauft, schönes Zugvieh angeschafft und kein Bauer im Dorfe konnte es ihm gleich thun. Mit dem zunehmenden Wohlstande nahm aber auch sein Uebermuth zu. »Wozu soll ich arbeiten«, sprach er, »ich kann ja der Ruhe pflegen!« Und nun hielt er Knechte und Mägde, die das Feld bauen mußten, und saß zu Hause im Lehnstuhl oder ritt auf einem stattlichen Gaul hinaus aufs Feld, die Saat zu besehen, die er sonst selbst ausgestreut hatte. Nur den täglichen Gang zu dem Braukessel machte er selbst. Als nun sein Reichthum immer größer ward – denn so ein Goldstück war wohl an die zwanzig Thaler werth – und sein Stolz mit ihm, da kam ihm der Gedanke bei, daß es doch sehr lästig sei, täglich um eines Goldstücks wegen den hohen Berg erklimmen zu müssen, er wolle das nächste Mal zwei Goldstücke nehmen. Er that's, nahm Tags darauf zwei Goldstücke und trieb dies so einen ganzen Monat hindurch. Auch damit noch nicht zufrieden, sprach er: »Ei, was soll ich mich täglich quälen und nur zwei Goldstücke holen. Der ganze Schatz ist ja doch für mich bestimmt, ob ich ihn nun nach und nach oder auf ein Mal hole, das wird wohl dem Geiste einerlei sein. Ich werde gehen und den ganzen Braukessel auf einmal leeren, dann brauche ich mich doch nicht weiter zu bemühen!« Des andern Tages packte er viele Säcke auf, keuchte den Berg hinan – denn die gute Kost und das gemächliche Leben hatten seinen Körper gut genährt, – langte bei der bewußten Oeffnung ganz ermattet an, setzte sich erst nieder, um wieder zu Kräften zu kommen, freute sich, daß diese lästigen Gänge hierher nun aufhören würden, berechnete schon, was er nun beginnen wolle, wenn alle die mitgenommenen Säcke wohl gefüllt erst in seinem Hause ständen, wie er dann ein großes Rittergut kaufen, viele Gäste bei sich sehen und mit diesen zechen wolle, trotz den alten Rittern von Quästenberg und dergleichen mehr. Nun stand er auf, nahm die Säcke, ging durch den dunkeln Gang und langte bei dem Braukessel an, der trotz dem, was schon nach und nach weggeholt war, immer noch bis an den Rand <pb n="542" xml:id="tg1668.2.5.2"/>
gefüllt blieb. Er nahm den ersten Sack, kniete nieder an den Rand des Kessels, fuhr mit beiden Händen in das Gold gierig hinein und wollte eben die erste Ladung in den Sack werfen, als plötzlich der ganze Braukessel vor ihm mit schrecklichem Geprassel hinabsank, Feuerflammen und Schwefelgestank heraufqualmten und der betäubte Thor fast ohnmächtig zurückfiel. Fort war der Schatz, hin alle die schönen lustigen Luftschlösser. Kein Braukessel erschien wieder, so oft auch der Nimmersatt wiederkam, der nun gern immer nur ein Goldstück genommen hätte, wenn's vergönnt gewesen wäre.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1668.2.6">Seit der Zeit ist der schöne Braukessel mit dem vielen Golde nicht wieder gesehen worden, bis er endlich einmal von ein Paar Jesuiten, die davon hörten, aufgesucht und gefunden ward. Ihrem trunkenen Blicke zeigte er sich, voll des glänzendsten Metalls, und schon schickten sie sich an, den Schatz zu heben, als plötzlich der Geist ihnen erschien und sprach: »Nicht Euch sind diese Reichthümer beschieden und nie könnt Ihr sie heben. Das Schicksal bestimmt sie einem Grafen von Stolberg, der zweierlei Augen haben wird. Diesem allein darf ich sie übergeben, aber bis dieser kommt, schützt sie mein mächtiger Arm gegen jeden Angriff. Fort mit Euch!« Voll Angst und Entsetzen flohen die Jesuiten und erzählten den Anwohnern diese seltsame Begebenheit. Noch ist kein Graf von Stolberg mit zweierlei Augen geboren worden, der Schatz also noch vorhanden.</p>
                </div>
                <div type="footnotes">
                    <head type="h4" xml:id="tg1668.2.7">Fußnoten</head>
                    <note xml:id="tg1668.2.8.note" target="#tg1668.2.1.1">
                        <p xml:id="tg1668.2.8">
                            <anchor xml:id="tg1668.2.8.1" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Der Harz/589. Die Sagen vom Schlosse Quästenberg#Fußnoten_1"/>
                            <ref cRef="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Der Harz/589. Die Sagen vom Schlosse Quästenberg#Fußnote_1" xml:id="tg1668.2.8.2">1</ref> S. Gottschalck, Ritterburgen Bd. II. S. 37 etc. Hoffmann, Die Burgen des Harzes S. 1 etc. Otmar S. 121 etc. Sagen und Geschichten aus der Vorzeit des Harzes S. 108 etc. Fischer, Geschichte und Beschreibung der Burgvesten der Preuß. Monarchie. Schweidnitz o.J. in 8°. Bd. II. S. 161 etc.</p>
                    </note>
                    <note xml:id="tg1668.2.10.note" target="#tg1668.2.4.2">
                        <p xml:id="tg1668.2.10">
                            <anchor xml:id="tg1668.2.10.1" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Der Harz/589. Die Sagen vom Schlosse Quästenberg#Fußnoten_2"/>
                            <ref cRef="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Der Harz/589. Die Sagen vom Schlosse Quästenberg#Fußnote_2" xml:id="tg1668.2.10.2">2</ref> Als Zins mußte die Gemeinde alle Jahre am zweiten Pfingsttage vor Sonnenaufgang auf der Pfarre zu Quästenberg einige Brode abliefern. Wurde der Lehnzins nicht zur rechten gebracht, so hatte der Pfarrer das Recht, sich das beste Rind aus der Heerde der Gemeinde auszulesen.</p>
                    </note>
                </div>
            </div>
        </body>
    </text>
</TEI>