<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg1807" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Westphalen/727. Legenden vom heil. Liborius">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>727. Legenden vom heil. Liborius</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Grässe, Johann Georg Theodor: Element 01823 [2011/07/11 at 20:29:59]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Johann Georg Theodor Grässe: Sagenbuch des Preußischen Staates 1–2, Band 1, Glogau: Carl Flemming, 1868/71.</title>
                        <author key="pnd:104076534">Grässe, Johann Georg Theodor</author>
                    </titleStmt>
                    <extent>688-</extent>
                    <publicationStmt>
                        <date notBefore="1868" notAfter="1871"/>
                        <pubPlace>Glogau</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1814" notAfter="1885"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg1807.2">
                <div type="h4">
                    <head type="h4" xml:id="tg1807.2.1">727) Legenden vom heil. Liborius.<ref type="noteAnchor" target="#tg1807.2.5">
                            <anchor xml:id="tg1807.2.1.1" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Westphalen/727. Legenden vom heil. Liborius#Fußnote_1"/>
                            <ptr cRef="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Westphalen/727. Legenden vom heil. Liborius#Fußnoten_1"/>
                            <hi rend="superscript" xml:id="tg1807.2.1.2.1">1</hi>
                        </ref>
                    </head>
                    <p xml:id="tg1807.2.2">Vor mehr als tausend Jahren sind die Gebeine des heil. Liborius aus Frankreich nach Paderborn gebracht worden. Als man die Gruft, wo sie zuerst geruht, öffnete, drang ein Geruch besser wie Rosenduft daraus hervor und verbreitete sich rings umher. Nachdem man darauf Messen und Lieder über den heiligen Leichnam gesungen, ward er in einen goldenen Sarg gelegt und von greisen, ernsthaften Männern aus Frankreich getragen. So sie hinkamen, sproßten Blumen auf und fremde, nie gesehene Vögel kamen und sangen preisende Lieder. Wo sie im freien Felde übernachteten, quollen klare Brünnlein zu ihrer Erquickung; Segen brachte ihre Reise allen Fluren, durch welche ihre Straße ging. Kamen die Männer mit ihrer kostbaren Last an einen Fluß, so gingen sie hindurch, ohne nur ihren Fuß zu benetzen. Die Dornen am Wege stachen sie nicht und die scharfen Steine ritzten ihre Fußsohlen nicht blutig. So ging die Fahrt viele Tage lang und die Männer wurden nicht müde und sie spürten auch nicht Hunger noch Durst in der ganzen Zeit. Als sie aber endlich den Sarg auf den Hochaltar im Dome zu Paderborn niedergesetzt hatten, da fielen sie todt zur Erde, denn sie hatten ihr Werk vollbracht und gemeine Bürde sollte nicht wie der auf ihren Schultern ruhen. Manches Jahrhundert hatten die Gebeine des Heiligen im Dom zu Paderborn geruht und großer Segen war dem Lande durch einen so mächtigen Patron zugewendet worden, aber mit der Zeit fingen die Leute an gleichgültig gegen ihren Schutzheiligen zu werden, sie glaubten, sie brauchten ihn nicht mehr, die Prozessionen hörten auf und der Tag des heil. Liborius ward nicht höher als ein anderer Festtag gefeiert. Von der Zeit an aber ging es dem Paderbornischen Lande schlecht, Hungersnoth, Krieg und Seuchen brachen über dasselbe herein und die Noth lehrte die undankbaren Bürger wieder beten und der Himmel selbst gab ihnen Zeichen, daß der Heilige sie wieder zu Gnaden angenommen habe. Denn in einer Nacht öffnete sich die große Dompforte und dieselben Männer traten heraus, welche einst die heiligen Gebeine aus Frankreich geholt hatten. Zum zweiten Male ruhte jetzt der goldene Todtenschrein auf ihren Schultern und finster und schweigend hielten die Ehrwürdigen mit ihren Reliquien den Umzug durch die Stadt, ganz wie es früher geschehen war. Dann trugen sie den Sarg wieder in den Dom, die Pforte schloß sich geräuschlos hinter ihnen und die ganze Erscheinung war verschwunden. Dies nahmen sich die Paderborner <pb n="688" xml:id="tg1807.2.2.1"/>
wohl zu Herzen und als wieder St. Liboriustag einfiel, da hielten sie die Prozession feierlicher denn je zuvor und Pest und Krankheit und alles Elend war sogleich zu Ende.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1807.2.3">Im dreißigjährigen Kriege ward Paderborn von dem Herzog Christian von Braunschweig belagert und erobert (1622). Derselbe ließ die zwölf silbernen Apostel, die von bedeutender Größe in der Domkirche standen, herausnehmen und Thaler daraus münzen mit der Inschrift: »Gottes Freund, der Pfaffen Feind«, nachdem er sie vorher spottweise ausgescholten, daß sie dem Befehle ihres Meisters nicht besser nachgelebt, der ihnen in alle Welt zu gehen ernstlich anbefohlen, und mit einem Schwur versichert hatte, daß er ihnen diesen Augenblick Beine machen wolle. Derselbe nahm auch den Sarg des heil. Liborius und ließ Goldstücke daraus prägen und die Gebeine führte er in einem leinenen Sacke auf seinen Kriegszügen mit sich. Aber schon nach einem Jahre ereilte ihn die Strafe seiner That, denn um diese Zeit ward er im Münsterischen von Tilly völlig geschlagen und aller seiner Macht beraubt. Da saß er und klagte und rief: »Ach hätte ich den Alten ruhen lassen, er ist mein Unglück«. Darauf schickte er die Gebeine eiligst nach Paderborn zurück<ref type="noteAnchor" target="#tg1807.2.7">
                            <anchor xml:id="tg1807.2.3.1" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Westphalen/727. Legenden vom heil. Liborius#Fußnote_2"/>
                            <ptr cRef="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Westphalen/727. Legenden vom heil. Liborius#Fußnoten_2"/>
                            <hi rend="superscript" xml:id="tg1807.2.3.2.1">2</hi>
                        </ref>, wo sie lange in einer hölzernen Kiste lagen. Endlich als es wieder Friede war, ließ man wieder einen silbernen, ganz vergoldeten Sarg durch den Goldschmied Hans von Dringenberg zu Paderborn für sie verfertigen, in welchem sie noch jetzt liegen.</p>
                </div>
                <div type="footnotes">
                    <head type="h4" xml:id="tg1807.2.4">Fußnoten</head>
                    <note xml:id="tg1807.2.5.note" target="#tg1807.2.1.1">
                        <p xml:id="tg1807.2.5">
                            <anchor xml:id="tg1807.2.5.1" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Westphalen/727. Legenden vom heil. Liborius#Fußnoten_1"/>
                            <ref cRef="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Westphalen/727. Legenden vom heil. Liborius#Fußnote_1" xml:id="tg1807.2.5.2">1</ref> S. Jos. Seiler, Volkssagen und Legenden des Landes Paderborn. Cassel 1848 in 18. S. 40.</p>
                    </note>
                    <note xml:id="tg1807.2.7.note" target="#tg1807.2.3.1">
                        <p xml:id="tg1807.2.7">
                            <anchor xml:id="tg1807.2.7.1" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Westphalen/727. Legenden vom heil. Liborius#Fußnoten_2"/>
                            <ref cRef="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Westphalen/727. Legenden vom heil. Liborius#Fußnote_2" xml:id="tg1807.2.7.2">2</ref> Dies ist unwahr. Zwar hatte er die Gebeine mitgenommen, allein er schenkte sie der Rheingräfin Christina, einer geborenen Fürsten von Croy, und von dieser erhielt sie der Fürstbischof von Paderborn 1627 zurück.</p>
                    </note>
                </div>
            </div>
        </body>
    </text>
</TEI>