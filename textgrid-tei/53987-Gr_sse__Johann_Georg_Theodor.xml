<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg1859" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Westphalen/779. Das Erdmännchen von Hardenstein">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>779. Das Erdmännchen von Hardenstein</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Grässe, Johann Georg Theodor: Element 01875 [2011/07/11 at 20:29:59]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Johann Georg Theodor Grässe: Sagenbuch des Preußischen Staates 1–2, Band 1, Glogau: Carl Flemming, 1868/71.</title>
                        <author key="pnd:104076534">Grässe, Johann Georg Theodor</author>
                    </titleStmt>
                    <extent>730-</extent>
                    <publicationStmt>
                        <date notBefore="1868" notAfter="1871"/>
                        <pubPlace>Glogau</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1814" notAfter="1885"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg1859.2">
                <div type="h4">
                    <head type="h4" xml:id="tg1859.2.1">779) Das Erdmännchen von Hardenstein.<ref type="noteAnchor" target="#tg1859.2.6">
                            <anchor xml:id="tg1859.2.1.1" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Westphalen/779. Das Erdmännchen von Hardenstein#Fußnote_1"/>
                            <ptr cRef="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Westphalen/779. Das Erdmännchen von Hardenstein#Fußnoten_1"/>
                            <hi rend="superscript" xml:id="tg1859.2.1.2.1">1</hi>
                        </ref>
                    </head>
                    <p xml:id="tg1859.2.2">Zur Zeit des Kaisers Wenzeslaus lebte auf seiner jetzt verfallenen Burg Hardenstein an der Ruhr der Ritter Neveling von Hardenberg; zu diesem gesellte sich einstens ein Erdmännchen, welches sich König Goldemar nannte und lebte lange vertraulich mit ihm auf der Burg. Goldemar redete aber mit ihm und andern Menschen, spielte sehr lieblich auf der Harfe, ingleichen mit Würfeln, setzte dabei Geld auf, trank Wein und schlief oft bei Neveling in einem Bette. Als nun Viele sowohl Geist- als Weltliche ihn besuchten, redete er zwar mit Allen, aber also, daß es besonders den Geistlichen nicht immer wohlgefiel, indem er durch Entdeckung ihrer heimlichen Sünden dieselben oftmals schamroth machte. Neveling, welchen er seinen Schwager zu nennen pflegte, warnte er oft vor seinen Feinden und zeigte ihm, wie er deren Nachstellungen entgehen könne. Auch lehrte er ihn, sich mit diesen Worten zu kreuzigen und zu sagen: »Unerschaffen ist der Vater, unerschaffen ist der Sohn, unerschaffen ist der heilige Geist!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg1859.2.3">So pflegte er zu sagen, die Christen gründeten ihre Religion auf Worte, die Juden auf köstliche Steine, die Heiden auf Kräuter. Seine Hände, welche mager und wie ein Frosch und Maus kalt und weich im Angriff waren, ließ er zwar fühlen, Keiner aber konnte ihn sehen. Nachdem er nun drei Jahre bei Neveling ausgehalten hatte, ist er, ohne Jemand zu beleidigen, weggezogen. Es hatte aber Neveling eine schöne Schwester, um welcher willen Viele argwöhnten, daß sich dieses Erdmännchen bei ihm aufgehalten habe.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1859.2.4">Nach einer andern Sage war die Sache so. Auf dem Schlosse Hardenstein hielt sich vor alten Zeiten ein Erdmännchen auf, welches sich König Vollmar nannte und diejenige Kammer bewohnte, welche von jenem Tag bis auf den heutigen König Vollmars Kammer heißt. Dieser Vollmar aber mußte stets einen Platz am Tische und einen für sein Pferd im Stalle haben, da denn jederzeit die Speisen, wie auch Hafer und Heu verzehrt wurden, vom Menschen und Pferde aber sah man nichts als den Schatten. Nun trug es sich zu, daß auf diesem Hause ein Küchenjunge war, welcher begierig, diesen Vollmar, wenigstens seine Fußtapfen zu sehen, hin und wieder Erbsen und Asche streuete, um ihn in solcher Gestalt fallen zu machen. Allein es wurde sein Vorwitz sehr übel bezahlt, denn an einem gewissen Morgen, als dieser Knabe das Feuer anzündete, kam Vollmar, brach ihm den Hals und hieb ihn in Stücken, steckte die Brust an einen Spieß und <pb n="730" xml:id="tg1859.2.4.1"/>
briet sie, etliches röstete, das Haupt aber nebst den Beinen kochte er. Als nun der Koch bei seinem Eintritt in die Küche dieses erblick te, wurde er sehr erschrocken und wollte sich fast nicht in die Küche wagen. Sobald die Gerichte fertig waren, wurden solche auf Vollmars Kammer getragen, wo man denn hörte, daß sie unter Freudengeschrei und einer schönen Musik verzehrt wurden. Nach dieser Zeit hat man aber den König Vollmar nicht mehr verspürt. Ueber seiner Kammerthür aber war geschrieben, daß das Haus künftig so unglücklich sein sollte als es bisher glücklich gewesen wäre, auch daß die Güter zersplittert werden und nicht eher wieder zusammenkommen sollten, bis daß drei Hardenberge vom Hardenstein am Leben sein würden. Der Spieß und Rost sind lange zum Gedächtniß verwahrt worden, aber 1651, als die Lothringer in diesen Gegenden hauseten, weggeplündert worden; der Topf aber, der in der Küche eingemauert, ist auch später noch vorhanden gewesen.<ref type="noteAnchor" target="#tg1859.2.8">
                            <anchor xml:id="tg1859.2.4.2" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Westphalen/779. Das Erdmännchen von Hardenstein#Fußnote_2"/>
                            <ptr cRef="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Westphalen/779. Das Erdmännchen von Hardenstein#Fußnoten_2"/>
                            <hi rend="superscript" xml:id="tg1859.2.4.3.1">2</hi>
                        </ref>
                    </p>
                </div>
                <div type="footnotes">
                    <head type="h4" xml:id="tg1859.2.5">Fußnoten</head>
                    <note xml:id="tg1859.2.6.note" target="#tg1859.2.1.1">
                        <p xml:id="tg1859.2.6">
                            <anchor xml:id="tg1859.2.6.1" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Westphalen/779. Das Erdmännchen von Hardenstein#Fußnoten_1"/>
                            <ref cRef="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Westphalen/779. Das Erdmännchen von Hardenstein#Fußnote_1" xml:id="tg1859.2.6.2">1</ref> S. Von Steinen St. IV. S. 777-779. Stahl S. 113 etc. Kuhn, Westphäl. Sagen S. 136.</p>
                    </note>
                    <note xml:id="tg1859.2.8.note" target="#tg1859.2.4.2">
                        <p xml:id="tg1859.2.8">
                            <anchor xml:id="tg1859.2.8.1" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Westphalen/779. Das Erdmännchen von Hardenstein#Fußnoten_2"/>
                            <ref cRef="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Erster Band/Westphalen/779. Das Erdmännchen von Hardenstein#Fußnote_2" xml:id="tg1859.2.8.2">2</ref> Abgebildet bei Von Steinen.</p>
                    </note>
                </div>
            </div>
        </body>
    </text>
</TEI>