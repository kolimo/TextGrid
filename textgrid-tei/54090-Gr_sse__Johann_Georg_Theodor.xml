<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg1962" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Zweiter Band/Die Rheinprovinz/41. Der Kipphäuser oder der schwarze Mann in Zitterwalde">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>41. Der Kipphäuser oder der schwarze Mann in Zitterwalde</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Grässe, Johann Georg Theodor: Element 01980 [2011/07/11 at 20:29:59]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Johann Georg Theodor Grässe: Sagenbuch des Preußischen Staates 1–2, Band 2, Glogau: Carl Flemming, 1868/71.</title>
                        <author key="pnd:104076534">Grässe, Johann Georg Theodor</author>
                    </titleStmt>
                    <extent>54-</extent>
                    <publicationStmt>
                        <date notBefore="1868" notAfter="1871"/>
                        <pubPlace>Glogau</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1814" notAfter="1885"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg1962.2">
                <div type="h4">
                    <head type="h4" xml:id="tg1962.2.1">41. Der Kipphäuser oder der schwarze Mann in Zitterwalde.</head>
                    <milestone unit="head_start"/>
                    <p rend="zenoPC" xml:id="tg1962.2.2">
                        <seg rend="zenoTXFontsize80" xml:id="tg1962.2.2.1">(Nach Montanus Bd. I. S. 319 etc.)</seg>
                    </p>
                    <milestone unit="head_end"/>
                    <lb xml:id="tg1962.2.3"/>
                    <p xml:id="tg1962.2.4">Mitten zwischen Moor, Dickicht und Haiden liegt das Walddörfchen Refrath, welches aber eine der ältesten Kirchen im ganzen Bergischen Lande besitzt. Dorthin ziehen jeden Dienstag Schaaren von Betern nach der heiligen Reliquie des Antonius von Padua, dessen Gnaden-Bildniß die Kirche ziert. Jeden ersten Dienstag im Monat wird nämlich hier ein vollkommener Ablaß gewährt, an den übrigen 40 aber wenigstens ein partieller. Leider aber wird die Heiligkeit des Orts bis auf diese Stunde durch ein Phantom gestört, welches in der dortigen Gegend unter dem Namen des schwarzen Mannes zu Gladbach oder des schwarzen Mannes in Zitterwalde bekannt ist und die einsamen Wanderer in Schrecken setzt. Dieses Gespenst soll aber folgenden Ursprung haben.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1962.2.5">Vor ohngefähr 500 Jahren erhob sich über der dichten Waldung, welche heute noch Refrath umschließt, ein gewaltiges Schloß, von dem heute freilich nur noch wenig Trümmerhaufen übrig sind. Dieses hieß das Schloß Kipphausen nach dem Namen der Ritterfamilie, welcher hier Grund und Boden gehörte. Der letzte männliche Sproß war Herr Ulrich von Kipphausen gewesen, der bei seinem 1396 erfolgten Tode nur eine einzige Tochter Namens Elisabeth hinterließ. Es versteht sich von selbst, daß diese als die alleinige Erbin eines ziemlich bedeutenden Grundbesitzthums viele Freier hatte, allein von allen gefiel ihr nur einer, ein junger Spanier, den sie unter dem Namen eines Grafen zu Cölln, wo er sich aufhielt und viel Geld aufgehen ließ, hatte kennen lernen. Sie reichte ihm ihre Hand, aber sehr bald nach ihrer Vermählung zeigte er sich ganz anders als sie sich ihn gedacht hatte. Aus einem Verschwender, was er wenigstens mit seinem Eigenthum gewesen war, ward jetzt ein vollständiger Geizhals, dabei war er mißtrauisch, hart und unfreundlich gegen sie, vom Beten wollte er gar nichts wissen, und wenn sie ihn nach seinen frühern Verhältnissen fragte, erhielt sie entweder gar keine, oder doch eine ungenügende Antwort. Dabei hatte sich aber in der Umgegend das Gerücht verbreitet, er sei gar kein wirklicher Graf, sondern nur der Diener eines solchen gewesen, habe aber seinen Herrn ermordet und sich mit dessen Schätzen beladen ins Ausland geflüchtet. Natürlich war dies nur Gerücht und Niemand wagte es ihm diese Beschuldigungen ins Gesicht zu sagen. Nun hatte er sich aber der Verwaltung des Vermögens und der Herrschaft seiner Frau vollständig bemächtigt, so daß diese auf ihrem ererbten Grund und Boden gar nichts mehr zu sagen hatte und so trat denn an die Stelle des milden, nachsichtigen Regiments, welches Ritter Ulrich seinen Unterthanen gegenüber geführt, eine unerträgliche Tyrannei.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1962.2.6">Zu jener Zeit, wo überhaupt ein Grundherr ziemlich allmächtig war, mußte es hoch kommen, wenn einer in der Umgegend für einen Leuteschinder gehalten wurde, aber diesen Namen bekam er und verdiente ihn auch vollkommen, denn namentlich gegen seine Fröhner war er unerbittlich streng und <pb n="54" xml:id="tg1962.2.6.1"/>
hart, so daß er sie nicht blos durch fast unmögliche Arbeiten, die er ihnen auflegte, quälte, sondern ihnen auch noch täglich neue Lasten und Schatzungen aufbürdete. So war denn eines Tages einer derselben, der ihn fern glaubte, von der Arbeit zu seiner todtkranken Frau geeilt um sie einige Stunden zu pflegen, allein der böse Herr hatte es doch bemerkt, eilte ihm nach und forderte ihn auf, auf der Stelle zu seiner Arbeit zurückzukehren. Der arme Fröhner entschuldigte sich zwar mit der Krankheit seiner Frau, aber umsonst, der strenge Ritter ließ ihn von seinen Knechten ins Gefängniß schleppen, als Pfand aber der Frau desselben ihren besten Hausrath und selbst ihr Bett wegnehmen. Da sprang das arme im Fieberwahnsinn liegende Weib in die Höhe, stürzte auf den Kipphäuser los und stieß ihm ein schnell ergriffenes Messer in den Hals. Zwar stürzte er schwer verwundet zu Boden, allein er war nicht todt, wohl aber erwachte er ins Schloß gebracht und nachdem seine Wunde verbunden war, nur zu einem halben Leben. Er war wahnsinnig geworden, denn er hatte sich die furchtbare Erscheinung der fieberkranken Frau zu Sinne gezogen und in ihr nicht eine menschliche Erscheinung, sondern einen von Gott geschickten Racheengel zu sehen geglaubt. Er schloß sich also in sein Gemach ein und ließ selbst seine Gattin nicht mehr zu sich. Da sah man eines Tages schwarze Dampfwolken sich über die Refrather Wälder wälzen und als dieser Umstand eine Menge Zuschauer aus der Nachbarschaft herbeizog, so ward man bald darüber klar, daß das Kipphausener Schloß brenne. Mittlerweile kam aber auf einmal der arme von ihm so sehr schwer gemißhandelte Fröhner jubelnd herbeigelaufen, indem er eine bluttriefende Axt schwang und rief: »Jetzt habe ich dem Bösewicht warm gemacht, jetzt werden ihm die Flammen auch einheizen und er kann das Feuer der Hölle im Voraus schon empfinden!« Man wußte jetzt was aus ihm und dem Schlosse geworden war. Kühner geworden näherten sich die Leute nun der Brandstätte, allein selbst wenn man gewollt hätte, auch nicht das Geringste wäre von der Habe des Schloßherrn zu retten gewesen, an vielen Stellen brachen die Flammen durch die Mauern, nach und nach brachen die Thürme und Gewölbe zusammen und plötzlich stürzte dann das übrige Mauerwerk auf einmal herab. Zwar ward später an derselben Stelle von den Erben des Kipphäusers wieder ein Schloß aufgebaut, aber es ward nur kurze Zeit bewohnt, denn das Gespenst des frühern Schloßherrn ließ Niemanden in demselben Ruhe, sodaß auch dieses neue Haus bald wieder in Trümmer sank.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1962.2.7">Der gespenstige Spanier aber erscheint in mannigfacher Gestalt als Spuk, bald als wohlgekleideter Junker, der auf den Ruinen der Kipphäuser Burg herumwandelt, bald als ein zerlumpter Bursche mit schwarzbraunem sonneverbrannten Antlitz, ohne Hut und baarfuß im Walde Kinder in Schrecken setzt, bald als eine kleine, aber stämmige Mannesgestalt mit dreieckigem Hute und altmodischen Kleidern, aber von verwildertem Ansehen, der namentlich in dem an Gladbach grenzenden Zitterwalde sein Wesen treibt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1962.2.8">Einst kam ein Landmann den Weg von Bensberg längs Refrath herab, um nach Cölln zu gehen. Da begegnete ihm dort oberhalb Langbrücken an einer Brücke, die dort über ein Waldwasser führt, der Kipphäuser als ein Junker, aber altmodisch gekleidet, hohläugig, mit erdfahlem Gesichte und blutigem Halskragen. Das Gespenst sagte in ziemlich unverständlichem Tone <pb n="55" xml:id="tg1962.2.8.1"/>
dem Bauer, er werde vor Deutz einen greisen Kapuziner treffen, den solle er doch fragen, ob denn die Stunde seiner Erlösung noch nicht da sei. Gleich nach diesem Auftrage wirbelte eine Staubwolke zwischen dem Bauer und der Erscheinung auf und als dieselbe sich wieder legte, war die letztere verschwunden. Der Bauer ging nun fürbaß, bis er kurz vor Deutz wirklich auf den Kapuziner stieß, er fragte ihn so, wie ihm befohlen war, der Mönch aber versetzte, er möge nur dem Gespenste – es sei der Geist des Kipphäuser Burgherrn – sagen, er müsse umgehen bis zum jüngsten Tage. Auf das Bemerken des Bauern, ob er nicht einen andern Weg einschlagen könne, wo er dem Verfluchten nicht zu begegnen brauche, bemerkte der Kapuziner, er dürfe sich dem ihm gewordenen Auftrage nicht entziehen, er solle aber, wo jener ihm auch begegnen möge, doch erst hinter der bezeichneten Brücke ihm Rede stehen; denn jenseits des Baches habe das Gespenst keine Macht mehr ihm zu schaden, und er könne dort demselben ohne Gefahr sagen, was er wolle. Als nun der Bauer seine Geschäfte in der Stadt besorgt hatte, trat er ängstlich den Rückweg an, und richtig das Gespenst war bald an seiner Seite und fragte ihn, was der Mönch gesagt habe, der Bauer aber eilte, ohne zu antworten, bis er über die Brücke war, dann aber, als das Gespenst abermals fragte, was der Mönch gesagt habe, antwortete er, er müsse bis zum jüngsten Tage auf seine Erlösung hoffen. Da rollte sich die Figur des Kipphäusers zu einem Knäul zusammen und stob mit einem lauten Knalle in Staubwolken auseinander, wie wenn man Schießpulver angezündet hat.</p>
                </div>
            </div>
        </body>
    </text>
</TEI>