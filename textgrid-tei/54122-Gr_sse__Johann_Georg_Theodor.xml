<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg1994" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Zweiter Band/Die Rheinprovinz/73. Die buckligen Musikanten">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>73. Die buckligen Musikanten</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Grässe, Johann Georg Theodor: Element 02012 [2011/07/11 at 20:29:59]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Johann Georg Theodor Grässe: Sagenbuch des Preußischen Staates 1–2, Band 2, Glogau: Carl Flemming, 1868/71.</title>
                        <author key="pnd:104076534">Grässe, Johann Georg Theodor</author>
                    </titleStmt>
                    <extent>93-</extent>
                    <publicationStmt>
                        <date notBefore="1868" notAfter="1871"/>
                        <pubPlace>Glogau</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1814" notAfter="1885"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg1994.2">
                <div type="h4">
                    <head type="h4" xml:id="tg1994.2.1">
                        <pb n="93" xml:id="tg1994.2.1.1"/>
73. Die buckligen Musikanten.</head>
                    <milestone unit="head_start"/>
                    <p rend="zenoPC" xml:id="tg1994.2.2">
                        <seg rend="zenoTXFontsize80" xml:id="tg1994.2.2.1">(Nach A. Reumont, Rheinlands-Sagen. Cölln u. Aachen o.J., in 8. S. 78 und Jos. Müller a.a.O. S. 122 etc.)</seg>
                    </p>
                    <milestone unit="head_end"/>
                    <lb xml:id="tg1994.2.3"/>
                    <p xml:id="tg1994.2.4">Am Tage St. Matthäi im Jahre 1549 kam ein armer buckliger Spielmann spät in der Nacht nach Aachen von einem Dorfe zurück, wo er bei einer Hochzeit aufgegeigt hatte. Halb im Taumel, denn er hatte sich den Hochzeitstrunk wohl schmecken lassen, achtete er nicht auf die Zeit und gerade stand er vor dem Münster, als die Thurmglocke desselben Mitternacht schlug. Da wurde ihm Angst und bange, denn er glaubte Eulengeschrei und Gezwitscher von Fledermäusen um sich zu hören, er eilte also seiner Wohnung zu, die auf der Jacobstraße gelegen war. Als er aber den sogenannten Pervisch (vom lat. <hi rend="italic" xml:id="tg1994.2.4.1">parvisium</hi>) oder Fischmarkt betrat, da schimmerten alle Fischbänke mit schneeweißen Tischtüchern belegt und von unzähligen Lichtern erhellt ihm glänzend entgegen, köstliche Speisen waren in goldenen und silbernen Schüsseln aufgetragen und perlender Wein blinkte in großen Krystallkrügen. Um die Tische herum aber saß eine große Menge reichgekleideter Damen und ließ es sich herrlich schmecken. Erschrocken hockte sich der Spielmann in eine Ecke, denn es fiel ihm ein, daß heute ja die Quatembernacht sei und da die Hexen ihren Spuk treiben. Doch es war zu spät, eine der dort sitzenden Damen hatte ihn bereits bemerkt, sie stand auf und nöthigte ihn an ihren Tisch; als derselbe aber mit klappernden Zähnen und schlotternden Knieen vor ihr stand, da redete sie ihm zu, Muth zu fassen, es solle sein Schade nicht sein, wenn er ihnen ein lustiges Stücklein vorspielen wolle, sie reichte ihm auch einen Becher Weins, und als er denselben geleert, da nahm er die Geige zur Hand und fing lustig zu fiedeln an. Da wurden eilig die Bänke mit Allem, was darauf war, bei Seite geschafft, die Damen, unter denen er manche vornehme Frau aus der Stadt zu erkennen glaubte, erhoben sich allzumal bei dem Tone der Geige und bald wirbelten die Paare durcheinander. Nun aber ging es immer schneller und schneller und der Spielmann geigte, wie von unsichtbarer Hand getrieben, immer toller darauf los, so daß er mehrmals vermeinte, die Saiten müßten in tausend Stücke zerspringen und ihm Hören und Sehen vergehen. Indeß sausten die Paare noch immer durcheinander im Reigen, während sein Arm kräftig den Bogen führte, und sein Spiel so stark wurde, daß es ihm vorkam, als wenn ein ganzes Concert von Geigen und gellenden Flöten hinter ihm aufgestellt sei, welche alle in seine Töne einstimmten. Da summte plötzlich die Thurmuhr drei Viertel auf Eins und plötzlich hielten die Paare in sichtbarer Erschöpfung inne, Alles wurde wieder mit einem Male ruhig und in seine vorige Ordnung gerückt. Unentschlossen stand aber der Spielmann da, nicht wissend, ob er bleiben solle oder gehen dürfe. Da trat die frühere Dame wieder zu ihm heran und sprach: »Bravo, Spielmann, Du hast uns wacker vergnügt, darum soll Dir auch der gebührende Lohn werden.« Und damit hatte sie ihm bereits das Wamms ausgezogen und ehe er noch recht zur Besinnung kommen konnte, war sie schon hinter ihn getreten und hatte ihm mit einem Griffe seinen Höcker abgenommen. Wer war froher als der arme Geiger, dankdurchdrungen wollte er vor seiner Wohlthäterin niederfallen – da aber schlug es Eins und mit diesem Glockenschlage waren <pb n="94" xml:id="tg1994.2.4.2"/>
Damen, Lichter und Mahl verschwunden, und der Spielmann stand ganz allein auf dem Platze. Daß es aber kein Traum gewesen, was ihm begegnet, davon überzeugte er sich, als er seinen Rücken anfühlte, derselbe war schlank und glatt, kein Höcker war mehr da und als er in seine Taschen griff, die ihm ungewöhnlich schwer vorkamen, da fand er eitel Goldstücke, und eilte als zweifach glücklicher Mann in seine Wohnung. Dort aber erkannte die harrende Frau ihren verwandelten Mann fast nicht mehr wieder, bis ihr seine Erzählung die Begebenheit der verflossenen Nacht kund machte. Da dankte die Frau dem Himmel für das ihnen widerfahrene Glück und am andern Morgen ward die Geige, die so viel Heil ins Haus gebracht, unter dem Bilde des Schutzpatrons aufgehangen und fortan zum ewigen Gedächtniß für Kinder und Kindeskinder als ein Heiligthum bewahrt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1994.2.5">Des armen Spielmanns Glück wurde nun aber bald in der Nachbarschaft bekannt und erregte ihm viele Neider, namentlich ärgerte sich ein Camerad von ihm, ein häßlicher, buckliger Geselle, am Meisten darüber. Er dachte also Tag und Nacht darauf, wie auch er gerade werden möchte und so begab er sich auf St. Gerhardi Nacht um die zwölfte Stunde ebenfalls nach dem Pervisch. Dort fand er richtig auch dasselbe Gelage und ward gerade wie sein Camerad von einer der Anwesenden zum Spielen aufgefordert. Aber als er seine Weisen zu spielen anfing, da verdrehten sich die lustigen Tanzmelodieen in Sterbelieder und Trauermärsche, und es erhob sich um ihn ein höllisches Gepfeife und Gezische und die Paare drehten sich trübselig und langsam im Kreise. Der Spielmann aber, noch immer vermeinend, seine besten Melodieen vorzutragen, musicirte stark darauf los und erwartete nun, da der Tanz vollendet war, nichts weniger als einen noch reichern Lohn, denn sein Vorgänger, und trat keck an die Tafel und redete die vorsitzende Dame, in welcher er die Frau des Bürgermeisters zu entdecken wähnte, frech an und fragte sie »ob er denn für sein schönes Spiel nicht einen bessern Lohn verdiene, als der Stümper, den sie neulich so beschenkt habe?« Da nahm die Dame im Nu den Deckel von einer silbernen Schüssel und ehe er sichs versah, klebte der darin aufbewahrte Höcker seines Gesellen vorn an seiner Brust. So stand denn der Neidhart mit dem doppelten Bollwerk umgeben wie eingewurzelt da, bis beim ersten Schlage der Morgenstunde der Spuk verschwand und der arme Teufel sich mit der zweifachen Last nach Hause trollen mußte, um sie bis an seinen Tod mit sich herum zu tragen.</p>
                </div>
            </div>
        </body>
    </text>
</TEI>