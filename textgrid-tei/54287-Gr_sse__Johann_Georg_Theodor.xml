<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg2159" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Zweiter Band/Schlesien und die Niederlausitz/225. Die zwei Grabsteine in der Klosterkirche zu Leubus">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>225. Die zwei Grabsteine in der Klosterkirche zu Leubus</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Grässe, Johann Georg Theodor: Element 02180 [2011/07/11 at 20:29:59]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Johann Georg Theodor Grässe: Sagenbuch des Preußischen Staates 1–2, Band 2, Glogau: Carl Flemming, 1868/71.</title>
                        <author key="pnd:104076534">Grässe, Johann Georg Theodor</author>
                    </titleStmt>
                    <extent>260-</extent>
                    <publicationStmt>
                        <date notBefore="1868" notAfter="1871"/>
                        <pubPlace>Glogau</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1814" notAfter="1885"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg2159.2">
                <div type="h4">
                    <head type="h4" xml:id="tg2159.2.1">225. Die zwei Grabsteine in der Klosterkirche zu Leubus.</head>
                    <milestone unit="head_start"/>
                    <p rend="zenoPC" xml:id="tg2159.2.2">
                        <seg rend="zenoTXFontsize80" xml:id="tg2159.2.2.1">(Nach Gödsche S. 69 etc.)</seg>
                    </p>
                    <milestone unit="head_end"/>
                    <lb xml:id="tg2159.2.3"/>
                    <p xml:id="tg2159.2.4">In einer Seitenkapelle der Klosterkirche zu Leubus befindet sich noch bis auf den heutigen Tag ein uralter Grabstein und quer vor dem Eingange zur Kapelle ein zweiter. Von der Bedeutung derselben giebt es folgende Sage.</p>
                    <p rend="zenoPLm4n0" xml:id="tg2159.2.5">Zur Zeit der letzten Kreuzzüge lebten in der Nachbarschaft von Leubus zwei Ritter, Wolf von Uraz und Kunz von der Heinzenburg. Beide waren trotz der Verschiedenheit ihrer Charaktere – Wolf war ein tapferer, aber rauher, wilder Geselle, Kunz aber sanft und mild und ebenso allgemein beliebt, als jener verhaßt – innige Freunde und Keiner that etwas ohne den Andern. Da begab es sich, daß Wolf, der als Knabe schon mit der Tochter des Burggrafen Pribuslaw, der als Castellan auf Leubus hauste, verlobt war, daran dachte, dieselbe nun wirklich zu seiner Gemahlin zu machen. Das Mädchen aber liebte ihn nicht und Kunz, der ebenfalls in heimlicher Leidenschaft für sie erglühte, gefiel ihr weit besser, als ihr rauher Bräutigam. Als sie nun eines Abends weinend und traurig im Burggarten saß, kam Kunz herbei, fragte sie, was ihr fehle, und als ein Wort das andere gab, da erfuhr er, daß sie lieber sterben als Wolf heirathen wolle. Er tröstete sie also und versprach sie zu retten und natürlich blieb es am Ende nicht aus, daß sich beide Theile ihre Liebe gestanden. Leider war aber, ohne daß jene eine Ahnung davon hatten, Wolf in der Nähe gewesen, er hatte alles mit angehört, er stürzte jetzt wie ein Wüthender aus seinem Verstecke hervor und nöthigte mit gezücktem Schwerte Kunz zum Zweikampfe. Da er aber blindlinks auf jenen eindrang, so rannte er sich selbst in dessen Schwert und sank bald blutend zur Erde. Man hob ihn auf und trug ihn ins Schloß, wo er aufs Beste gepflegt ward und bald seine Gesundheit wieder erlangte. Kaum genesen, trat er aber nun vor den alten Burggrafen hin und verlangte ungesäumt die Hand seiner Braut, dieser aber ein guter, jedoch schwacher Vater, hatte sich bereits von seiner Tochter bestimmen lassen, sie ihres Wortes zu entbinden, und sagte nun, er wolle und könne seine einzige Tochter nicht zu ihrem Unglück zwingen. Da that Wolf einen theuern Eid, er wolle nicht eher ruhen, bis er an Kunz und seiner Braut ihre Treulosigkeit gerächt habe. So lange der alte Castellan lebte, mußte er nun schon seine Wuth in sich verschließen, als aber dieser nach zwei Jahren gestorben war und dessen Ansehn dem jungen Paare keinen Schutz mehr verleihen konnte, da sah Wolf die Gelegenheit ab und während Kunz eines Abends am Krankenbette seiner Gemahlin saß, da brachte ihm plötzlich sein Leibknappe die Nachricht, der wilde Wolf habe mit einer Schaar Knappen das schlecht bewachte Thor überfallen und sei bereits, nachdem die wenigen <pb n="260" xml:id="tg2159.2.5.1"/>
Knechte, die sich ihm im Hofe entgegengestellt hätten, überwältigt worden seien, so gut wie Meister der Burg. Kunzen blieb nichts übrig, wollte er seinem Todfeinde nicht in die Hände fallen, als sein krankes Weib auf die Schulter zu laden und mit wenigen Begleitern durch einen geheimen Gang, der ohngefähr eine Viertelstunde weit vom Schlosse im Walde seinen Ausgang hatte, zu flüchten. Er begab sich, nachdem er ins Freie gelangt war, mit seiner theuern Bürde in die Wohnung eines ihm ergebenen Köhlers, hatte aber das Unglück, nach wenigen Stunden seine Gemahlin in seinen Armen sterben zu sehen. Um sich selbst zu retten, ließ er die Leiche in den Händen seiner Leute zurück und floh weiter, und kaum hatte er sich einige Stunden entfernt, da kam auch Wolf in die Köhlerhütte, fand aber nichts Lebendes mehr vor, an welchem er seine Wuth auslassen konnte. Er versuchte nun alle Mittel heraus zu bekommen, wohin sich sein Feind gewendet habe, endlich erfuhr er, derselbe sei nach Italien gezogen um sich einem nach Palästina von Venedig abgehenden Kreuzheere anzuschließen. Er folgte ihm auf dem Fuße, allein als er in der alten Dogenstadt anlangte, war das Schiff, worauf sich Kunz befand, den Tag vorher abgegangen. Wolf entschloß sich kurz, ihm nachzueilen und seine Rache auch jenseits des Meeres zu verfolgen, allein auch in Palästina gelang es ihm nicht, seines Nebenbuhlers habhaft zu werden, überall wo er hinkam, kam er zu spät, jener war jedes Mal bereits abgereist, und schließlich fiel er selbst noch den Sarazenen in einem Gefechte in die Hände und mußte ganzer zehn Jahre lang in der Gefangenschaft ausharren. Endlich gelang es ihm die Flucht zu ergreifen und den Rückweg anzutreten, immer von der Hoffnung getrieben, Kunz wiederfinden zu können. Allein er erfuhr sehr bald, derselbe sei längst schon nach Europa zurückgekehrt. Er eilte also auch seinerseits so schnell als möglich nach Schlesien zurück, weil er ihn hier gewiß zu treffen hoffte. Leider kam er aber auch hier zu spät; Kunz war wenige Wochen vorher seiner Gattin ins bessere Jenseits gefolgt und in der Klosterkirche zu Leubus begraben worden. Da ergrimmte der wilde Wolf über das tückische Schicksal, welches ihm seinen Feind aus den Händen gerissen hatte. Er verschrieb also dem gedachten Kloster seine sämmtlichen Güter unter der Bedingung, daß man ihn, wenn er gestorben sei, vor der Kapellenthüre begrabe, worin sein Todfeind ruhe, damit er am Auferstehungstage aller Todten diesen alsogleich packen und wenigstens dann seine Wuth an ihm auslassen könne. So geschah es auch, als er nach einem wilden Leben endlich gestorben war, ward er, wie er befohlen, quer vor dem Eingange jener Seitencapelle beerdigt und ihm ein gleicher Grabstein gelegt wie seinem Todfeinde. Hier schläft er aber bis zum jüngsten Tage, der hoffentlich noch lange auf sich warten lassen wird.</p>
                </div>
            </div>
        </body>
    </text>
</TEI>