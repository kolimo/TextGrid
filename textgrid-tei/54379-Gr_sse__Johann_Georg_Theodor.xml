<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg2251" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Zweiter Band/Schlesien und die Niederlausitz/281. Der Salzbrunner Heilborn">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>281. Der Salzbrunner Heilborn</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Grässe, Johann Georg Theodor: Element 02273 [2011/07/11 at 20:29:59]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Johann Georg Theodor Grässe: Sagenbuch des Preußischen Staates 1–2, Band 2, Glogau: Carl Flemming, 1868/71.</title>
                        <author key="pnd:104076534">Grässe, Johann Georg Theodor</author>
                    </titleStmt>
                    <extent>333-</extent>
                    <publicationStmt>
                        <date notBefore="1868" notAfter="1871"/>
                        <pubPlace>Glogau</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1814" notAfter="1885"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg2251.2">
                <div type="h4">
                    <head type="h4" xml:id="tg2251.2.1">281. Der Salzbrunner Heilborn.</head>
                    <milestone unit="head_start"/>
                    <p rend="zenoPC" xml:id="tg2251.2.2">
                        <seg rend="zenoTXFontsize80" xml:id="tg2251.2.2.1">(S. Mosch, die Heilquellen Schlesiens S. 363.)</seg>
                    </p>
                    <milestone unit="head_end"/>
                    <lb xml:id="tg2251.2.3"/>
                    <p xml:id="tg2251.2.4">Zu Ende des 17. Jahrhunderts lebte in dem Dorfe Salzbrunn ein armer Weber, im Besitz eines kleinen Häuschens und einer großen Familie. So arm er auch war, so wenig konnte ihm irgend ein Unfall seinen zufriedenen Sinn rauben; denn er pflegte immer zu sagen: »Wem Gott Kinder gebe, dem helfe er sie zu ernähren und wo der Herr ein Unglück sende, da schicke er auch hinterdrein einen Freund, der es ertragen helfe!« Und dies schien auch wahr zu sein. Denn Alles, was er unternahm, das ging ihm leicht von statten, und es war sichtbar göttlicher Segen bei seinen Arbeiten und Geschäften, weil er sie gern und mit zufriedener Seele unternahm und eben so vollendete. Dabei hatte er in mancher Noth einen Freund getroffen, und wo ihn ja dieser von außen verlassen, in sich selbst einen Helfer gefunden.</p>
                    <p rend="zenoPLm4n0" xml:id="tg2251.2.5">So hatte er es glücklich dahin gebracht, daß seine Hütte jetzt schuldenfrei war, und wenn er das Innere derselben betrachtete, so mußte er sich selbst gestehen, daß sie einen Reichthum einschließe, den mancher Große entbehre: ein gutes Weib, wohl geartete Kinder, Reinlichkeit und Arbeitsamkeit, und ein zufriednes Herz, welches dies Alles dankbar erkenne.</p>
                    <p rend="zenoPLm4n0" xml:id="tg2251.2.6">Ohne Zweifel würde sich der Weber bald zu einer gewissen Freiheit von Nahrungssorgen empor gearbeitet haben, wenn ihm nicht der Himmel ein Unglück zugesendet hätte, das seine Standhaftigkeit auf eine harte Probe stellte. Er ward plötzlich mit bösen Geschwüren befallen, die anfänglich gar den ganzen Körper bedeckten, aber endlich sich an den Füßen festsetzten, und so den armen Unglücklichen an Betreibung seines Handwerkes hinderten. Wie sorgsam pflegte ihn nicht die treue Hausfrau; wie strebten die armen Kinder nicht ihn zu erheitern, und durch unschuldige Scherze und Spiele ihm seine Schmerzen weniger fühlbar zu machen! Vergebens, das Uebel blieb <pb n="333" xml:id="tg2251.2.6.1"/>
wie es war, und die Noth der armen Familie stieg immer höher. Seufzend legte sich der Leidende des Nachts auf sein Lager, mit bangen Sorgen für den kommenden Tag, wo wieder zwei fleißige Hände fehlten, den Kleinen Brod zu schaffen, und seufzend fand ihn die Morgenröthe, wenn sie über die Berge heraufzog, nach einer schlaflos durchwachten Nacht.</p>
                    <p rend="zenoPLm4n0" xml:id="tg2251.2.7">So litt der Unglückliche viele Jahre, und während dieser Zeit war kein Mittel unversucht gelassen worden, was ärztliche Bemühungen, oder freundschaftlicher Rath, oder der Aberglauben der Frauen anrieth. Die Familie war in tiefe Armuth versunken, und die Hütte, welche sonst von der Zufriedenheit bewohnt ward, schien jetzt ein Sammelplatz unverhehlter Klagen und nie versiegender Thränen zu sein. Aber sie war in der langen Zeit des Elends verschuldet und hartherzige Gläubiger raubten endlich dem Elenden auch sein letztes Obdach.</p>
                    <p rend="zenoPLm4n0" xml:id="tg2251.2.8">Als die Nachricht ankam, daß die Armen in wenigen Wochen ihre Hütte meiden sollten, da erfüllten Mutter und Kinder das ganze Haus mit Wehklagen, und benetzten mit Thränen die Stücklein Brod, welche fremde Milde ihnen zugetheilt hatte. Nur der Weber erhielt durch diesen letzten Schlag die Stärke der Seele wieder, die er seit Jahren her nicht mehr gekannt. Mit Festigkeit hörte er den Ausspruch der Unglücksboten an, und mit Ruhe überdachte er dann sein verfloßnes Leben. Aber er fand keine Schuld in dessen ganzem Lauf.</p>
                    <p rend="zenoPLm4n0" xml:id="tg2251.2.9">»Liebes Weib«, sprach er daher am Abende, als schon die Kleinen in süßem Schlummer auf ihrem Stroh lagen, – »liebes Weib, warum so kleinmüthig? Lebt denn nicht Gott noch, der vormals uns so wohl gethan? Warum verzweifeln wir an seiner Hülfe? An unsern Händen klebt kein unrecht Gut, und keiner Schuld sind wir uns ja bewußt! Durch Trübsal muß der Fromme eingehen zur Freude und zur Glückseligkeit; o, laß uns ruhig und getrost dieser Zeit entgegen sehen! Die letzte Leidens-Nachricht hat mich nicht gebeugt, sie hat vielmehr mich wunderbar erhoben. Ich fühle mich mit neuer Kraft ausgerüstet, und werde nun mit Geduld und Standhaftigkeit das Leiden tragen, das mir Gott zugesandt!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg2251.2.10">Das Weib weinte stille Thränen in die Dunkelheit der mondhellen Nacht hinein, und als sie endlich ihr Lager suchte an der Seite des hart geprüften Leidenden, da war er sanft entschlummert, und ein himmlisches Lächeln umschwebte das vom Glanze des Mondes erhellte Gesicht des Dulders.</p>
                    <p rend="zenoPLm4n0" xml:id="tg2251.2.11">Nachdem schon das treue Weib erwacht war, schlummerte der Gebeugte noch so sanft als am Abend. Die Morgenröthe war bereits in das Thal gezogen, und ihre milden Strahlen schlichen sich eben durch die kleinen runden Scheiben der Hütte, und malten die wankenden Schatten der vom frischen Morgenwinde bewegten Bäume des Gartens an die Wand. Halb freudig, halb ängstlich belauscht die gute Frau den Schlaf des Gatten, denn Jahre lang hatte dieser sich keines ähnlichen erfreuet. Ach, sie wußte nicht, ob dieses ungewöhnliche Ereigniß zum Leben oder zum Tode führen werde.</p>
                    <p rend="zenoPLm4n0" xml:id="tg2251.2.12">Endlich erwachte der Kranke, und blickte staunend und mit erheitertem Antlitz um sich her.</p>
                    <p rend="zenoPLm4n0" xml:id="tg2251.2.13">»Kaum weiß ich noch, liebes Weib«, sprach er, »wie mir ist! Die Ruhe, die mir Gott diese Nacht geschenkt, die Heiterkeit, mit welcher meine <pb n="334" xml:id="tg2251.2.13.1"/>
ganze Seele erfüllt ist, und dazu – der wunderbare Traum! – Ja, gutes Weib, mich hat in dieser Nacht ein wunderbares Traumgesicht gestärkt. – Es war am Tage vor dem Auszuge aus diesem Hause, wo wir bei aller Armuth doch einst so froh gelebt, da bat ich Dich, mich doch hinauszuführen in den Garten, damit ich dort im Schatten unseres großen Birnbaums mich noch einmal der Zeit erinnern könnte, wo unsere Liebe und der Kinder unschuldvolle Freuden dies kleine Gärtchen uns zum Paradiese machten. Du thatst dies, und setztest mich auf ein Lager von frischem Moos, und die milde Luft und die Strahlen der scheidenden Sonne bewegten mein Herz gar heftig. Ich lehnte mich mit meinem Rücken an den Stamm des Baumes, und meine Gefühle wurden zu einem Gebet zu Gott um Segen und Wohlergehn für Euch. – Da bewegte sich auf einmal der Glanz des Abends durch die Bäume zu mir her, aber immer milder und milder, und als derselbe unser Haus erhellte, war er zum Glanze des sanften Mondes geworden, und eine herrliche Jünglingsgestalt entfaltete sich daraus immer heller und klarer. Eine hohe himmlische Gestalt, mit rosenfarbnen Wangen, mit Augen, aus welchen des Himmels Seligkeit strahlt, und mit einem goldnen Haar, das in schön geringelten Locken über seine Schultern wallte, welche ein glänzendes weißes Gewand deckte! Sanft bewegte sich der Jüngling zu mir her und reichte mir freundlich seine Hand. ›Sei getrost‹, sprach er, ›Dein Glauben und Dein Dulden hat vor Gott Gnade gefunden; Deine Leiden sollen geändert werden. Wenn die Strahlen der Sonne über die Berge hereinbrechen, dann stehe auf von Deinem Lager, ergreife Hacke und Schaufel, und laß Dich in Deinen Garten führen. Da wo der Salzbach, vom Morgen kommend, seinen Lauf ändert und sich gen Mitternacht wendet, da schlage ein mit Deinem Werkzeuge; Gott wird Dir Kraft verleihen, und bald wird Dir ein Quell entgegensprudeln, für Dich ein Quell des Lebens. In diesen tauche Deine Füße und Du wirst genesen!‹ Mit diesen Worten zerfloß die Gestalt vor meinen Augen, es war dunkel um mich her und der Traum war mit der Gestalt zerronnen.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg2251.2.14">»Jetzt bin ich erwacht«, fuhr er fort, »aus einem Schlafe, wie ich ihn seit Jahren nicht genoß; die Strahlen der Sonne vergolden die Wände unserer Hütte, und in der That fühle ich mich gestärkt. Wie könnte ich dem Herrn undankbar sein für diese Gnade! – Weib, wecke die Kinder! Vereint mit uns mögen sie sich auf die Kniee werfen und die kleinen Händchen zum himmlischen Vater emporhebend mit uns im Gebet für seine Wohlthat ihm danken. Dann will ich hinaus und sehen, ob der Traum mir wahr gerathen!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg2251.2.15">Mit freudigem Beben sprang die gute Frau nach der Kammer, führte die Kleinen herein, und vereint beteten Vater und Mutter, von den Kindern umgeben, auf ihren Knieen zum Wohlthäter im Himmel. Und als sie so aus ihrem Herzen gebetet, führte die starke Frau den gestärkten Gatten nach dem Bache. Nur wenige Hiebe waren nöthig, das tiefer liegende Gestein von seiner Erdschicht zu befreien, und bald sprang hell und klar ein starker Brunnen dem erstaunten Paare entgegen. Nun war des Traumes erste Hälfte klar erfüllt; wie hätten sie an der zweiten zweifeln mögen? Vereint gruben sich Beide ein Becken in die Erde, in welchem sich das Wasser sammelte, und noch an demselben Tage tauchte der arme Leidende seine Füße <pb n="335" xml:id="tg2251.2.15.1"/>
in die heilende Fluth. Mit jedem Tage, an welchem er dies Bad wiederholte, verbesserte sich sein Zustand, und als er so wenige Wochen hindurch sich des Wassers bedient, sah er sich unter dem beseeligenden Gefühl des neu geschenkten Lebens von seinen Leiden geheilt. Nun schritt er rüstig wieder zur Arbeit, und in wenigen Jahren segnete ihn der Himmel so, daß er, von seinen drückenden Schulden befreit, sich wieder im Besitz seiner Hütte sah, und fröhlich mit seinem Weibe die Seinigen zur Tugend und zur Arbeitsamkeit erziehen konnte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg2251.2.16">Seit dieser Zeit hieß der Brunnen der Heilborn und ist nicht wieder versiegt, und allen denen, welche Genesung bei ihm suchen, ruft es seitdem mit sanftem Gemurmel zu: »Dulde und hoffe!«</p>
                </div>
            </div>
        </body>
    </text>
</TEI>