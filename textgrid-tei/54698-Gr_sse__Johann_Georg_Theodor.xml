<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg2570" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Zweiter Band/West- und Ostpreußen/595. Von dem Leben und Tode der h. Dorothea">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>595. Von dem Leben und Tode der h. Dorothea</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Grässe, Johann Georg Theodor: Element 02595 [2011/07/11 at 20:29:59]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Johann Georg Theodor Grässe: Sagenbuch des Preußischen Staates 1–2, Band 2, Glogau: Carl Flemming, 1868/71.</title>
                        <author key="pnd:104076534">Grässe, Johann Georg Theodor</author>
                    </titleStmt>
                    <extent>575-</extent>
                    <publicationStmt>
                        <date notBefore="1868" notAfter="1871"/>
                        <pubPlace>Glogau</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1814" notAfter="1885"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg2570.2">
                <div type="h4">
                    <head type="h4" xml:id="tg2570.2.1">595. Von dem Leben und Tode der h. Dorothea.</head>
                    <milestone unit="head_start"/>
                    <p rend="zenoPC" xml:id="tg2570.2.2">
                        <seg rend="zenoTXFontsize80" xml:id="tg2570.2.2.1">(Nach Voigt, Gesch. Preußens Bd. V. bei Karl, Danziger Sagen. Danzig 1843 H.I. S. 10 etc.)</seg>
                    </p>
                    <milestone unit="head_end"/>
                    <lb xml:id="tg2570.2.3"/>
                    <p xml:id="tg2570.2.4">Im Dorfe Montau, unfern dessen die Weichsel und die Nogat sich von einander trennen, lebte ein frommer Landmann, Namens Wilhelm Schwartz, dessen Ehe mit einer Frau Agatha durch neun Kinder gesegnet war. Eins derselben hieß Dorothea und war im Jahre 1336 geboren. Im siebenten Jahre ihres Alters ward sie einmal zufällig mit siedendem Wasser überschüttet und blieb eigentlich nur durch ein Wunder am Leben. Von dieser Zeit an fühlte sie jedoch schon als kleines Kind einen eigenthümlichen Drang nach Bußübungen und Beten. Sie geißelte sich, entzog sich den Schlaf, schlug sich mit glühenden Eisen selbst Löcher in ihre Beine und goß dann siedendes Wasser und kochendes Fett hinein um die Brandwunden recht schlimm zu machen. Sie glaubte so eine Idee von den Martern zu bekommen, welche einst der Heiland um der Welt Sünden willen erlitten hatte. Da sie sehr schön war, so fehlte es ihr nicht an Bewerbern, und auf den Wunsch ihrer Eltern reichte sie einem derselben, einem Waffenschmied aus Danzig ihre Hand und zog mit ihm in seine Heimath, wo sie sechsundzwanzig Jahre mit ihm lebte. Auch hier setzte sie ihr früheres andächtiges und schwärmerisches Leben fort, ward vielfach deshalb verspottet und getadelt, ließ sich aber durch nichts irre machen, um so mehr, als ihr Wesen auch ihrem Gatten zusagte. Endlich aber ließ sie sich nicht mehr halten, einen längst heimlich<pb n="575" xml:id="tg2570.2.4.1"/>
 gehegten Wunsch erfüllt zu sehen, nämlich nach Rom in die heilige Stadt zu pilgern. Ihr Mann machte keine Einwendungen, so verkauften sie denn all ihr Eigenthum und zogen 1384 erst nach Aachen, wo ein wunderthätiges Marienbild und viele Reliquien aufbewahrt wurden. Nach Verlauf von drei Jahren kehrten sie jedoch nach Danzig zurück, ohne ihre eigentliche Absicht ausgeführt zu haben, und wohnten hier in der Nähe der St. Katharinenkirche, die Dorothea täglich besuchte, wenn sie nicht kleine Wallfahrten nach den benachbarten Heiligthümern machte. Endlich aber, als bei dem angetretenen Jubeljahre 1390 Papst Urban VI. einen allgemeinen Ablaß verkündigen ließ und aus Preußen viele Personen von jeglichem Alter und Geschlecht nach Rom wanderten, ergriff auch sie den Pilgerstab und schloß sich diesen an, denn ihr Mann konnte vor Alter und vor Kränklichkeit sie diesmal nicht begleiten. Während der ganzen Reise und ihres Aufenthalts in Rom soll sich aber Dorothea nur eine einzige Nacht dem Schlafe überlassen haben. Zwei Monate hindurch besuchte sie täglich die sieben größten Kirchen jener Weltstadt, ging im strengsten Winter barfuß einher, genoß nur die nothdürftigste und schlechteste Nahrung und versagte sich jede Ruhe und Erholung. Dadurch verfiel sie in eine siebenwöchentliche Krankheit, welche sie an den Rand des Grabes brachte, allein sie genas und nachdem sie noch das Osterfest zu Rom mitgefeiert hatte, kehrte sie über Cöln nach Danzig zurück. Da sie hier ihren Mann nicht mehr am Leben antraf, beschloß sie ihre übrigen Tage gänzlich blos dem Himmel, der Kasteiung und dem Gebet zu weihen. Sie begab sich deshalb nach Marienwerder zu dem Domherrn Johannes, der ihr die Erlaubniß auswirkte, bis an ihren Tod in einer vermauerten Klause zu bleiben. Am 2. Mai des Jahres 1393 ward sie denn auch, nachdem sie das Abendmahl erhalten, von demselben unter ungeheurem Volkszulaufe in die St. Johanniskirche geführt und dort in eine Art Zelle so fest eingemauert, daß ihr nur eine kleine Oeffnung gestattete, durch diese Speise und Trank zu empfangen und mit der Außenwelt zu verkehren. Hier lebte sie einen Winter und Sommer bei weniger Speise, aber täglichem Genusse des heil. Abendmahls bis zum 26. Juni 1394. Bald verbreitete sich aber das Gerücht, daß an ihrem Grabe Wunder geschähen, durch die Ausdünstung ihres Leichnams sollen Kranke ihre Gesundheit, Blinde ihr Gesicht, Taube ihr Gehör, Stumme ihre Sprache, Lahme und Gichtbrüchige den Gebrauch ihrer Füße, ja selbst dorthin gebrachte Gestorbene das Leben wiedererhalten haben und ihr Grab ist bis auf das vergangene Jahrhundert der Wallfahrtsort frommer Pilger geblieben.</p>
                </div>
            </div>
        </body>
    </text>
</TEI>