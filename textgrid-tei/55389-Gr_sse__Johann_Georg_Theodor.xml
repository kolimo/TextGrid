<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg3261" n="/Literatur/M/Grässe, Johann Georg Theodor/Sagen/Sagenbuch des Preußischen Staats/Zweiter Band/Schleswig-Holstein/1279. Benno Butendick">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>1279. Benno Butendick</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Grässe, Johann Georg Theodor: Element 03292 [2011/07/11 at 20:29:59]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Johann Georg Theodor Grässe: Sagenbuch des Preußischen Staates 1–2, Band 2, Glogau: Carl Flemming, 1868/71.</title>
                        <author key="pnd:104076534">Grässe, Johann Georg Theodor</author>
                    </titleStmt>
                    <extent>1045-</extent>
                    <publicationStmt>
                        <date notBefore="1868" notAfter="1871"/>
                        <pubPlace>Glogau</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1814" notAfter="1885"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg3261.2">
                <div type="h4">
                    <head type="h4" xml:id="tg3261.2.1">1279. Benno Butendick.</head>
                    <milestone unit="head_start"/>
                    <p rend="zenoPC" xml:id="tg3261.2.2">
                        <seg rend="zenoTXFontsize80" xml:id="tg3261.2.2.1">(S. Jahrb. Bd. IV. S. 153.)</seg>
                    </p>
                    <milestone unit="head_end"/>
                    <lb xml:id="tg3261.2.3"/>
                    <p xml:id="tg3261.2.4">Im 16. Jhdt. lebte in Sandstrand ein Mann, Namens Benno, reich an Geld, Vieh und Land. Er hatte aber seinen Reichthum auf schlechte Art erworben. So lieh er armen Bauern ein Paar Joachimsthaler, und dafür mußten sie ihm für den Fall, daß sie nicht zur rechten Zeit wieder bezahlten, ihre Aecker als Pfand verschreiben; wenn dann jene den Termin nicht einhielten, so nahm der Reiche kecklich ihre Güter in Besitz. Er betrog Unmündige und Waisen, verdrehte die Testamente, besonders aber beraubte er Kirchen und Schulen um ihre Ländereien und Einkünfte. Da er nun seines Reichthums wegen großes Ansehen genoß und wegen seiner vielen Verbindungen unter den Bauern mächtig und zu fürchten war, so wagte der Uebervortheilte gegen denselben niemals sich aufzulehnen. Dieser Mann starb inzwischen plötzlich und ward mit großer Pracht in einem ausgemauerten Grabgewölbe mitten in der Kirche bestattet. In der nächsten Nacht hörten der Küster und die Nachbarn unversehens einen großen Lärm in der Kirche, so daß sie alle aus den Betten und Häusern hervorkamen. Am Morgen aber öffnete der Pastor mit seinem Collegen, dem Küster und Andern im Namen Jesu die Hauptthüre der Kirche und mit Schrecken sehen sie, daß das Grab jenes reichen Mannes geöffnet und leer war. Kurz darauf erschien der Teufel in Benno's Gestalt, sah die Leute mit wildem Blick an und sprach: »In diesem Leichnam wohne ich, er ist mein Eigenthum; die göttliche Gerechtigkeit befiehlt mir, drei Stunden lang bei Tag und drei Stunden lang bei Nacht in der Gestalt dieses Verdammten zu erscheinen; darum geht hinweg, oder es wird Euch übel gehen!« Der Pastor und sein College antworteten ihm muthig, er solle aus dem Leichnam weichen und den Tempel Gottes nicht beunruhigen, aber der Teufel fing laut an zu lachen und sagte auf Friesisch: »Hemm kaant möh nandte düen!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg3261.2.5">So wurde der Leichnam einige Wochen von den Würmern nicht verzehrt, sondern blieb gleichsam frisch und lebendig; und Satan trug denselben sogar bei hellem Mittage herum zum Schrecken der ganzen Gegend. Da wurden die Priester zusammengerufen, es wurden in allen Kirchen der Insel Gebete <pb n="1045" xml:id="tg3261.2.5.1"/>
angeordnet und dann ging man Mittags um eilf Uhr dem schon herumwandelnden Teufel muthig entgegen, aber dieser machte sich eine ganze Stunde lang nichts aus den frommen Bedrohungen und Gebeten. Endlich fing der Jüngste unter den anwesenden Pastoren an, heftig und mit derben Worten, den Teufel auszutreiben, da bekannte sich dieser endlich als überwunden und rief auf Friesisch: »Huort, huort, eck möth förth, döö würst eth düen!« und der Pastor, indem er auf den teufelischen Körper Bibeln warf, trieb den bösen Geist glücklich in die Hölle hinab. Der Leichnam aber wurde durch den Scharfrichter von Husum außerhalb des Ackerfeldes der Insel im Schlamm begraben und mitten durch den Körper hindurch ein langer, buntbemalter Pfahl gestoßen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg3261.2.6">Nicht lange nachher kam ein armer Bauer, dem es, da das ganze Land ohne Wald ist, an Brennholz fehlen mochte oder der von der ganzen Geschichte nichts wußte, und fing an mit kräftigen Armen den Pfahl auszureißen. Da schrie der Teufel sogleich: »Aa, Aa, lät jet murr!« Als der Bauer das hörte, stieß er den Pfahl mit aller Kraft wieder in die Tiefe, worauf der Teufel rief: »Dirr dä stör aß an Schialm!« Der Pfahl hat noch viele Jahre gestanden und ist erst zu Anfange des 17. Jhdts. durch eine Wasserfluth weggerissen worden.</p>
                    <p rend="zenoPLm4n0" xml:id="tg3261.2.7">Ein Enkel jenes Unseligen war später Rathsherr in Husum, ein vortrefflicher und liebenswürdiger Mann, aber beim Pöbel kamen er und seine Kinder nicht ohne Sticheleien weg und mußten oftmals das spöttische Wort: »Benneke Büttendick« hören.</p>
                </div>
            </div>
        </body>
    </text>
</TEI>