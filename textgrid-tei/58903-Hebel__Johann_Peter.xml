<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg94" n="/Literatur/M/Hebel, Johann Peter/Prosa/Schatzkästlein des rheinischen Hausfreundes/Des Adjunkts Standrede im Gemüsgarten seiner Schwiegermutter">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Des Adjunkts Standrede im Gemüsgarten seiner Schwiegermutter</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Hebel, Johann Peter: Element 00100 [2011/07/11 at 20:28:22]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title> Johann Peter Hebel: Poetische Werke. Nach den Ausgaben letzter Hand und der Gesamtausgabe von 1834 unter Hinzuziehung der früheren Fassungen, München: Winkler, 1961.</title>
                        <author key="pnd:118547453">Hebel, Johann Peter</author>
                    </titleStmt>
                    <extent>28-</extent>
                    <publicationStmt>
                        <date when="1961"/>
                        <pubPlace>München</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1760" notAfter="1826"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg94.3">
                <div type="h4">
                    <head type="h4" xml:id="tg94.3.1">Des Adjunkts Standrede im Gemüsgarten seiner Schwiegermutter</head>
                    <p xml:id="tg94.3.2">Setzt ohne Anstand die Hüte auf, gute Nachbarn und Freunde. Ich will nun von der Fruchtbarkeit und schnellen Verbreitung der Pflanzen mit euch reden.<hi rend="italic" xml:id="tg94.3.2.1"> »Es ging ein Säemann aus, zu säen seinen Samen, und etliches fiel auf ein gut Land</hi>.«</p>
                    <lb xml:id="tg94.3.3"/>
                </div>
                <div type="h4">
                    <head type="h4" xml:id="tg94.3.4">1</head>
                    <p xml:id="tg94.3.5">Man kann sich nicht genug über die Menge und Mannigfaltigkeit der Pflanzen verwundern, mit welchen die Natur alle Jahre die Erde bekleidet. In dem kleinen Raum, den das Auge auf einmal überschauen kann, welch eine Vielfachheit der Gestalten, welch ein Spiel der Farben, welche Fülle in der Werkstätte der reichsten Kraft und der unerforschlichen Weisheit? Nicht weniger muß man sich wundern über die Geschwindigkeit, mit welcher die Natur jede leere Stelle auf<pb n="28" xml:id="tg94.3.5.1"/> öden Feldern, verlassenen Wegen, kahlen Felsen, Mauern und Dächern, wo nur eine Handvoll fruchtbare Erde hingefallen ist, ansäet und mit Gras, Kräutern, Stauden, und Buschwerk besetzt. Das sieht man oft und achtet's nicht, eben weil man es von Kindheit an so oft sieht; die größte Weisheit verratet sich in der einfachen und natürlichen Einrichtung der Dinge, und man erkennt sie nicht, eben weil alles so einfach und natürlich ist.</p>
                    <lb xml:id="tg94.3.6"/>
                </div>
                <div type="h4">
                    <head type="h4" xml:id="tg94.3.7">2</head>
                    <p xml:id="tg94.3.8">Die meisten Pflanzen haben eine wunderbare Vermehrungskraft, wie jeder aufmerksame Landwirt wohl weiß. Tausend Samenkerne von einer einzigen Pflanze, solange sie lebt, ist zwar schon viel gesagt, nicht jede tragt's, aber es ist auch noch lange nicht das höchste. Man hat schon an einer einzigen Tabakspflanze 40 000 Körnlein gezählt, die sie in einem Jahre zur Reife brachte. Man schätzt einer Eiche, daß sie 500 Jahre leben könne. Aber wenn wir uns nun vorstellen, daß sie in dieser langen Zeit nur 50mal Früchte trage, und jedesmal in ihren weit verbreiteten listen und Zweigen nur 500 Eicheln, so liefert sie doch 25 000 wovon jede die Anlage hat, wieder ein solcher Baum zu werden. Gesetzt, daß dieses geschehe, und es geschehe bei jeder von diesen wieder, so hätte sich die einzige Eiche in der zweiten Abstammung schon zu einem Walde von 625 Millionen Bäumen vermehrt. Wieviel aber eine Million oder 1 000 mal 1 000 sei, glaubt man zu wissen, und doch erkennt es nicht jeder. Denn wenn ihr ein ganzes Jahr lang vom 1. Jänner bis zum 31. Dez. alle Tage 1 000 Striche an eine große Wand schreibet, so habt ihr am Ende des Jahrs noch keine Million, sondern erst 365 000 Striche, und das zweite Jahr noch keine Million, sondern erst 730 000 Striche, und erst am 26. September des dritten Jahrs würdet ihr zu Ende kommen. Aber unser Eichenwald hätte 625 solcher Millionen, und so wäre es bei jeder andern Art von Pflanzen nach Proportion in noch viel kürzerer Zeit, ohne an die zahlreiche Vermehrung durch Augen, Wurzelsprossen und Knollen zu gedenken. Wenn man sich also einmal<pb n="29" xml:id="tg94.3.8.1"/> über diese große Kraft in der Natur gewundert hat, so hat man sich über den großen Reichtum an Pflanzen aller Art nicht mehr zu verwundern. Obgleich viele 1 000 Kerne und Körnlein alle Jahre von Menschen und Tieren verbraucht werden, viele Tausend im Boden ersticken, oder im Aufkeimen durch ungünstige Witterung und andere Zufälle wieder zugrunde gehen, so bleibt doch jahraus jahrein, ein freudiger und unzerstörbarer Überfluß vorhanden. Auf der ganzen weiten Erde fehlt es nirgends an Gesäme, überall nur an Platz und Raum.</p>
                    <lb xml:id="tg94.3.9"/>
                </div>
                <div type="h4">
                    <head type="h4" xml:id="tg94.3.12">3</head>
                    <p xml:id="tg94.3.13">Aber wenn jeder reife Kern, der sich von seiner Mutterpflanze ablöset, unter ihr zur Erde fiele und liegen bliebe; alle lägen aufeinander, keiner könnte gedeihen, und wo vorher keine Pflanze war, käme doch keine hin. Das hat die Natur vor uns bedacht, und nicht auf unsern guten Rat gewartet. Denn einige Kerne, wenn sie reif sind, fliegen selbst durch eine verborgene Kraft weit auseinander, die meisten sind klein und leicht, und werden durch jede Bewegung der Luft davongetragen, manche sind noch mit kleinen Federlein besetzt, wie der Löwenzahn (Schlenke, Kettenblume) Kinder blasen sie zum Vergnügen auseinander, und tun damit der Natur auch einen kleinen Dienst, ohne es zu wissen, andere gehen in zarte breite Flügel aus, wie die Samenkerne von Nadelholzbäumen. Wenn die Sturmwinde wehen, wenn die Wirbelwinde, die im Sommer vor den Gewittern hergehen, alles von der Erde aufwühlen und in die Höhe führen, dann säet die Natur aus, und ist mit einer Wohltat beschäftiget, während wir uns fürchten, oder über sie klagen und zürnen; dann fliegen und schwimmen und wogen eine Menge von unsichtbaren Keimen in der bewegten Luft herum, und fallen nieder weit und breit, und der nachfolgende Staub bedeckt sie. Bald kommt der Regen und befeuchtet ihn, und so wirds auf Flur und Feld, in Berg und Tal, auf First und Halden auch wahr, daß etliches auf dem Weg von den Vögeln des Himmels gefressen wird, etliches unter den Dornen<pb n="30" xml:id="tg94.3.13.1"/> zu Grund geht, etliches auf trockenem Felsengrund in der Sonnenhitze erstirbt, etliches aber gut Land findet, und hundertfältige Frucht bringt. Weiter sind manche Kerne für den Wind zu groß und zu schwer, aber sie sind rund und glatt, rollen auf der Erde weiter, und werden durch jeden leichten Stoß von Menschen oder Tieren fortgeschoben. Andere sind mit umgebogenen Spitzen oder Häklein versehen, sie hängen sich an das Fell der Tiere, oder an die Kleider der Menschen an, werden fortgetragen, und an einem andern Orte wieder weggestreift, oder abgelesen und ausgesäet, und der es tut, weiß es nicht, oder denkt nicht daran. Viele Kerne gehen unverdaut und unzerstört durch den Magen und die Gedärme der Tiere, denen sie zur Nahrung dienen sollen, und werden an einem andern Ort wieder abgesetzt. So haben wir ohne Zweifel durch Strichvögel schon manche Pflanze aus fremden Gegenden bekommen, die jetzt bei uns daheim ist, und guten Nutzen bringt. So gehen auf hohen Gemäuern und Türmen Kirschbäume und andere auf, wo gewiß kein Mensch den Kern hingetragen hat. Noch andere fallen von den überhangenden Zweigen ins Wasser, oder sie werden durch den Wind und Überschwemmungen in die Ströme fortgerissen und weitergeführt, und an andern Orten durch neue Überschwemmungen wieder auf dem Lande abgesetzt. Ja einige schwimmen auch wohl auf den Strömen bis ins Meer, erreichen das jenseitige Gestade, und heimen sich alsdann in einer landesfremden Erde ein. Es sind da und dort schon Pflanzen als Unkraut aufgegangen, von denen man wohl wissen kann, daß der Samen dazu auf diese Art über das Meer gekommen sei. Also müssen alle Kräfte und Elemente die wohltätigen Absichten des Schöpfers befördern, Schnee und Regen, Blitz und Hagel, Sturm und Winde, die seine Befehle ausrichten.</p>
                    <lb xml:id="tg94.3.14"/>
                </div>
                <div type="h4">
                    <head type="h4" xml:id="tg94.3.15">4</head>
                    <p xml:id="tg94.3.16">Aber das ist ja eben die Plage des Landmannes! daher kommt also das viele Unkraut im Gartengelände und auf den Ackerfurchen, das der schönen gereinigten Saat Raum<pb n="31" xml:id="tg94.3.16.1"/> und Nahrung stiehlt, soviel Mühe macht, und doch mit aller Geduld und Sorgfalt nicht vertilgt werden kann! Die Sache ist nicht so schlimm, wie sie scheint. Denn <hi rend="italic" xml:id="tg94.3.16.2">zum ersten</hi>, so ist der Mensch nicht <hi rend="italic" xml:id="tg94.3.16.3">allein</hi> auf der Erde da. Viele 1000 Tiere aller Art, von mancherlei Natur und Bedürfnissen wollen auch genährt sein, und warten auf ihre Speise zu seiner Zeit. Manche davon sind uns unentbehrlich und wir wis sen's wohl, manche schaffen uns großen Nutzen, und wir wissen's nicht; und es muß doch wahr bleiben, woran wir uns selber so oft erinnern, daß sich eine milde Hand auftut, und sättiget alles, was da lebet mit Wohlgefallen. <hi rend="italic" xml:id="tg94.3.16.4">Zum andern</hi>, so hat doch der Mensch auch schon von manchem Kräutlein Nutzen gezogen, das er nicht selber gesäet und gepflanzet, nicht im Frühlingsfrost gedeckt, und in der Sommerhitze begossen hat. Und eine einzige unscheinbare und verachtete Pflanze, deren Kraft dir oder deinen Kindern, oder auch nur deinem Vieh eine Wunde heilt, einen Schmerz vertreibt oder gar das Leben rettet, bezahlt die Mühe und den Schaden reichlich, den tausend andere verursachen. Aber wer stellt den Menschen zufrieden? Wenn die Natur nicht so wäre, wie sie ist, wenn wir <hi rend="italic" xml:id="tg94.3.16.5">Baldrian</hi> und <hi rend="italic" xml:id="tg94.3.16.6">Wohlgemut, Ehrenpreis</hi> und<hi rend="italic" xml:id="tg94.3.16.7"> Augentrost</hi>, und alle Pflanzen in Feld und Wald, die uns in gesunden und kranken Tagen zu mancherlei Zwecken nützlich und nötig sind, selber ansäen, warten und pflegen müßten, wie würden wir alsdann erst klagen über des viel bedürftigen Lebens Mühe und Sorgen!</p>
                    <closer>
                        <seg type="closer" rend="zenoPR" xml:id="tg94.3.17">[1803]</seg>
                    </closer>
                </div>
            </div>
        </body>
    </text>
</TEI>