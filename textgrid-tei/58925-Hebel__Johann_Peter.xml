<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg116" n="/Literatur/M/Hebel, Johann Peter/Prosa/Schatzkästlein des rheinischen Hausfreundes/Der Zahnarzt">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Der Zahnarzt</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Hebel, Johann Peter: Element 00123 [2011/07/11 at 20:28:22]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title> Johann Peter Hebel: Poetische Werke. Nach den Ausgaben letzter Hand und der Gesamtausgabe von 1834 unter Hinzuziehung der früheren Fassungen, München: Winkler, 1961.</title>
                        <author key="pnd:118547453">Hebel, Johann Peter</author>
                    </titleStmt>
                    <extent>66-</extent>
                    <publicationStmt>
                        <date when="1961"/>
                        <pubPlace>München</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1760" notAfter="1826"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg116.3">
                <div type="h4">
                    <head type="h4" xml:id="tg116.3.1">Der Zahnarzt</head>
                    <p xml:id="tg116.3.2">Zwei Tagdiebe, die schon lange in der Welt miteinander herumgezogen, weil sie zum Arbeiten zu träg, oder zu ungeschickt waren, kamen doch zuletzt in große Not, weil sie wenig Geld mehr übrig hatten, und nicht geschwind wußten, wo nehmen. Da gerieten sie auf folgenden Einfall: Sie bettelten<pb n="66" xml:id="tg116.3.2.1"/> vor einigen Haustüren Brot zusammen, das sie nicht zur Stillung des Hungers genießen, sondern zum Betrug mißbrauchen wollten. Sie kneteten nämlich und drehten aus demselben lauter kleine Kügelein oder Pillen, und bestreuten sie mit Wurmmehl aus altem zerfressenem Holz, damit sie völlig aussahen wie die gelben Arzneipillen. Hierauf kauften sie für ein paar Batzen einige Bogen rotgefärbtes Papier bei dem Buchbinder: (denn eine schöne Farbe muß gewöhnlich bei jedem Betrug mithelfen.) Das Papier zerschnitten sie alsdann und wickelten die Pillen darein, je sechs bis acht Stücke in ein Päcklein. Nun ging der eine voraus in einen Flecken, wo eben Jahrmarkt war, und in den roten Löwen, wo er viele Gäste anzutreffen hoffte. Er forderte ein Glas Wein, trank aber nicht, sondern saß ganz wehmütig in einem Winkel, hielt die Hand an den Backen, winselte halblaut für sich, und kehrte sich unruhig bald so her, bald so hin. Die ehrlichen Landleute und Bürger, die im Wirtshaus waren, bildeten sich wohl ein, daß der arme Mensch ganz entsetzlich Zahnweh haben müsse. Aber was war zu tun? man bedauerte ihn, man tröstete ihn, daß es schon wieder vergehen werde, trank sein Gläslein fort, und machte seine Marktaffären aus. Indessen kam der andere Tagdieb auch nach. Da stellten sich die beiden Schelme, als ob noch keiner den andern in seinem Leben gesehen hätte. Keiner sah den andern an, bis der zweite durch das Winseln des erstern, der im Winkel saß, aufmerksam zu werden schien. »Guter Freund«, sprach er, »Ihr scheint wohl Zahnschmerzen zu haben?« und ging mit großen und langsamen Schritten auf ihn zu. »Ich bin der Doktor Schnauzius Rapunzius von Trafalgar«, fuhr er fort. Denn solche fremde volltönige Namen müssen auch zum Betrug behülflich sein, wie die Farben. »Und wenn Ihr meine Zahnpillen gebrauchen wollt«, fuhr er fort, »so soll es mir eine schlechte Kunst sein, Euch mit einer, höchstens zweien, von Euren Leiden zu befreien.« – »Das wolle Gott«, erwiderte der andere Halunk. Hierauf zog der saubere Doktor Rapunzius eines von seinen roten Päcklein aus der Tasche, und verordnete dem Patienten ein Kügelein daraus auf den bösen Zahn zu legen und herzhaft darauf zu beißen. Jetzt streckten die Gäste<pb n="67" xml:id="tg116.3.2.2"/> an den andern Tischen die Köpfe herüber, und einer um den andern kam herbei, um die Wunderkur mit anzusehen. Nun könnt ihr euch vorstellen, was geschah. Auf diese erste Probe wollte zwar der Patient wenig rühmen, vielmehr tat er einen entsetzlichen Schrei. Das gefiel dem Doktor. Der Schmerz, sagte er, sei jetzt gebrochen, und gab ihm geschwind die zweite Pille zu gleichem Gebrauch. Da war nun plötzlich aller Schmerz verschwunden. Der Patient sprang vor Freuden auf, wischte den Angstschweiß von der Stirne weg, obgleich keiner daran war, und tat, als ob er seinem Retter zum Danke etwas Namhaftes in die Hand drückte. – Der Streich war schlau angelegt, und tat seine Wirkung. Denn jeder Anwesende wollte nun auch von diesen vortrefflichen Pillen haben. Der Doktor bot das Päcklein für 24 Kreuzer, und in wenig Minuten waren alle verkauft. Natürlich gingen jetzt die zwei Schelme wieder einer nach dem andern weiters, lachten, als sie wieder zusammenkamen, über die Einfalt dieser Leute, und ließen sich's wohl sein von ihrem Geld.</p>
                    <p rend="zenoPLm4n0" xml:id="tg116.3.3">Das war teures Brot. So wenig für 24 kr. bekam man noch in keiner Hungersnot. Aber der Geldverlust war nicht einmal das Schlimmste. Denn die Weichbrotkügelein wurden<pb n="68" xml:id="tg116.3.3.1"/> natürlicherweise mit der Zeit steinhart. Wenn nun so ein armer Betrogener nach Jahr und Tag Zahnweh bekam, und in gutem Vertrauen mit dem kranken Zahn einmal und zweimal darauf biß, da denke man an den entsetzlichen Schmerz, den er, statt geheilt zu werden, sich selbst für 24 Kreuzer aus der eigenen Tasche machte. Daraus ist also zu lernen, wie leicht man kann betrogen werden, wenn man den Vorspiegelungen jedes herumlaufenden Landstreichers traut, den man zum erstenmal in seinem Leben sieht, und vorher nie und nachher nimmer; und mancher, der dieses liest, wird vielleicht denken: »So einfältig bin ich zu meinem eigenen Schaden auch schon gewesen.« – Merke: Wer so etwas kann, weiß an andern Orten Geld zu verdienen, lauft nicht auf den Dörfern und Jahrmärkten herum mit Löchern im Strumpf, oder mit einer weißen Schnalle im rechten Schuh, und am linken mit einer gelben.</p>
                    <closer>
                        <seg type="closer" rend="zenoPR" xml:id="tg116.3.4">[1807]</seg>
                    </closer>
                </div>
            </div>
        </body>
    </text>
</TEI>