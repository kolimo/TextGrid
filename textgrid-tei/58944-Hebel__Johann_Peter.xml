<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg135" n="/Literatur/M/Hebel, Johann Peter/Prosa/Schatzkästlein des rheinischen Hausfreundes/Drei Wünsche">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Drei Wünsche</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Hebel, Johann Peter: Element 00142 [2011/07/11 at 20:28:22]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title> Johann Peter Hebel: Poetische Werke. Nach den Ausgaben letzter Hand und der Gesamtausgabe von 1834 unter Hinzuziehung der früheren Fassungen, München: Winkler, 1961.</title>
                        <author key="pnd:118547453">Hebel, Johann Peter</author>
                    </titleStmt>
                    <extent>104-</extent>
                    <publicationStmt>
                        <date when="1961"/>
                        <pubPlace>München</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1760" notAfter="1826"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg135.3">
                <div type="h4">
                    <head type="h4" xml:id="tg135.3.1">Drei Wünsche</head>
                    <p xml:id="tg135.3.2">Ein junges Ehepaar lebte recht vergnügt und glücklich, beisammen, und hatte den einzigen Fehler, der in jeder menschlichen Brust daheim ist: Wenn man's gut hat, hätt man's gerne besser. Aus diesem Fehler entstehen so viele törichte Wünsche, woran es unserm<hi rend="italic" xml:id="tg135.3.2.1"> Hans</hi> und seiner <hi rend="italic" xml:id="tg135.3.2.2">Lise</hi> auch nicht fehlte. Bald wünschten sie des Schulzen Acker, bald des Löwenwirts Geld, bald des Meiers Haus und Hof und Vieh,<pb n="104" xml:id="tg135.3.2.3"/> bald einmal hunderttausend Millionen bayerische Taler kurzweg. Eines Abends aber, als sie friedlich am Ofen saßen und Nüsse aufklopften, und schon ein tiefes Loch in den Stein hineingeklopft hatten, kam durch die Kammertür ein weißes Weiblein herein, nicht mehr als einer Elle lang, aber wunderschön von Gestalt und Angesicht, und die ganze Stube war voll Rosenduft. Das Licht löschte aus, aber ein Schimmer wie Morgenrot, wenn die Sonne nicht mehr fern ist, strahlte von dem Weiblein aus, und überzog alle Wände. Über so etwas kann man nun doch ein wenig erschrecken, so schön es aussehen mag. Aber unser gutes Ehepaar erholte sich doch bald wieder, als das Fräulein mit wundersüßer silberreiner Stimme sprach: »Ich bin eure Freundin, die Bergfei, <hi rend="italic" xml:id="tg135.3.2.4">Anna Fritze</hi>, die im kristallenen Schloß mitten in den Bergen wohnt, mit unsichtbarer Hand Gold in den Rheinsand streut, und über siebenhundert dienstbare Geister gebietet. Drei Wünsche dürft ihr tun; drei Wünsche sollen erfüllt werden.« Hans drückte den Ellenbogen an den Arm seiner Frau, als ob er sagen wollte: Das lautet nicht übel. Die Frau aber war schon im Begriff, den Mund zu öffnen, und etwas von ein paar Dutzend goldgestickten Hauben, seidenen Halstüchern und dergleichen zur Sprache zu bringen, als die Bergfei sie mit aufgehobenem Zeigefinger warnte: »Acht Tage lang«, sagte sie, »habt Ihr Zeit. Bedenkt Euch wohl, und übereilt Euch nicht.« Das ist kein Fehler, dachte der Mann, und legte seiner Frau die Hand auf den Mund. Das Bergfräulein aber verschwand. Die Lampe brannte wie vorher, und statt des Rosendufts zog wieder wie eine Wolke am Himmel der Öldampf durch die Stube.</p>
                    <p rend="zenoPLm4n0" xml:id="tg135.3.3">So glücklich nun unsere guten Leute in der Hoffnung schon zum voraus waren, und keinen Stern mehr am Himmel sahen, sondern lauter Baßgeigen; so waren sie jetzt doch recht übel dran, weil sie vor lauter Wunsch nicht wußten, was sie wünschen wollten, und nicht einmal das Herz hatten, recht daran zu denken oder davon zu sprechen, aus Furcht, es möchte für gewünscht passieren, ehe sie es genug überlegt hätten. Nun sagte die Frau: »Wir haben ja noch Zeit bis am Freitag.«<pb n="105" xml:id="tg135.3.3.1"/>
                    </p>
                    <p rend="zenoPLm4n0" xml:id="tg135.3.4">Des andern Abends, während die Kartoffeln zum Nachtessen in der Pfanne prasselten, standen beide, Mann und Frau, vergnügt an dem Feuer beisammen, sahen zu, wie die kleinen Feuerfünklein an der rußigen Pfanne hin und her züngelten, bald angingen, bald auslöschten, und waren, ohne ein Wort zu reden, vertieft in ihrem künftigen Glück. Als sie aber die gerösteten Kartoffeln aus der Pfanne auf das Plättlein anrichteten, und ihr der Geruch lieblich in die Nase stieg; – »Wenn wir jetzt nur ein gebratenes Würstlein dazu hätten«, sagte sie in aller Unschuld, und ohne an etwas anders zu denken, und – o weh, da war der erste Wunsch getan. – Schnell wie ein Blitz kommt und vergeht, kam es wieder wie Morgenrot und Rosenduft untereinander durch das Kamin herab, und auf den Kartoffeln lag die schönste Bratwurst. – Wie gewünscht so geschehen. – Wer sollte sich über einen solchen Wunsch und seine Erfüllung nicht ärgern? Welcher Mann über solche Unvorsichtigkeit seiner Frau nicht unwillig werden?<pb n="106" xml:id="tg135.3.4.1"/>
                    </p>
                    <p rend="zenoPLm4n0" xml:id="tg135.3.5">»Wenn dir doch nur die Wurst an der <hi rend="italic" xml:id="tg135.3.5.1">Nase angewachsen wäre</hi>«, sprach er in der ersten Überraschung, auch in aller Unschuld, und ohne an etwas anders zu denken – und wie gewünscht, so geschehen. Kaum war das letzte Wort gesprochen, so saß die Wurst auf der Nase des guten Weibes fest, wie angewachsen in Mutterleib, und hing zu beiden Seiten hinab wie ein Husarenschnauzbart.</p>
                    <p rend="zenoPLm4n0" xml:id="tg135.3.6">Nun war die Not der armen Eheleute erst recht groß. Zwei Wünsche waren getan und vorüber, und noch waren sie um keinen Heller und um kein Weizenkorn, sondern nur um eine böse Bratwurst reicher. Noch war ein Wunsch zwar übrig. Aber was half nun aller Reichtum und alles Glück zu einer solchen Nasenzierat der Hausfrau? Wollten sie wohl oder übel, so mußten sie die Bergfei bitten, mit unsichtbarer Hand Barbiersdienste zu leisten, und Frau Lise wieder von der vermaledeiten Wurst zu befreien. Wie gebeten, so geschehen, und so war der dritte Wunsch auch vorüber, und die armen Eheleute sahen einander an, waren der nämliche Hans und die nämliche Lise nachher wie vorher, und die schöne Bergfei kam niemals wieder.</p>
                    <p rend="zenoPLm4n0" xml:id="tg135.3.7">
                        <hi rend="italic" xml:id="tg135.3.7.1">Merke</hi>: Wenn dir einmal die Bergfei also kommen sollte, so sei nicht geizig, sondern wünsche</p>
                    <p rend="zenoPLm4n0" xml:id="tg135.3.8">
                        <hi rend="italic" xml:id="tg135.3.8.1">Numero eins</hi>: Verstand, daß du wissen mögest, was du</p>
                    <p rend="zenoPLm4n0" xml:id="tg135.3.9">
                        <hi rend="italic" xml:id="tg135.3.9.1">Numero zwei</hi> wünschen sollest, um glücklich zu werden. Und weil es leicht möglich, wäre, daß du alsdann etwas wähltest, was ein törichter Mensch nicht hoch anschlägt, so bitte noch</p>
                    <p rend="zenoPLm4n0" xml:id="tg135.3.10">
                        <hi rend="italic" xml:id="tg135.3.10.1">Numero drei</hi>: um beständige Zufriedenheit und keine Reue.</p>
                    <p rend="zenoPLm4n0" xml:id="tg135.3.11">Oder so:</p>
                    <p rend="zenoPLm4n0" xml:id="tg135.3.12">Alle Gelegenheit, glücklich zu werden, hilft nichts, wer den Verstand nicht hat, sie zu benutzen.</p>
                    <closer>
                        <seg type="closer" rend="zenoPR" xml:id="tg135.3.13">[1808]</seg>
                    </closer>
                </div>
            </div>
        </body>
    </text>
</TEI>