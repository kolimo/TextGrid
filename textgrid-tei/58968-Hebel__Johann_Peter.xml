<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg159" n="/Literatur/M/Hebel, Johann Peter/Prosa/Schatzkästlein des rheinischen Hausfreundes/Der Husar in Neiße">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Der Husar in Neiße</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Hebel, Johann Peter: Element 00166 [2011/07/11 at 20:28:22]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title> Johann Peter Hebel: Poetische Werke. Nach den Ausgaben letzter Hand und der Gesamtausgabe von 1834 unter Hinzuziehung der früheren Fassungen, München: Winkler, 1961.</title>
                        <author key="pnd:118547453">Hebel, Johann Peter</author>
                    </titleStmt>
                    <extent>149-</extent>
                    <publicationStmt>
                        <date when="1961"/>
                        <pubPlace>München</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1760" notAfter="1826"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg159.3">
                <div type="h4">
                    <head type="h4" xml:id="tg159.3.1">Der Husar in Neiße</head>
                    <p xml:id="tg159.3.2">Als im Anfang der französischen Revolution die Preußen mit den Franzosen Krieg führten, und durch die Provinz Champagne zogen, dachten sie nicht daran, daß sich das Blättlein wenden könnte, und daß der Franzos noch im Jahr 1806 nach Preußen kommen, und den ungebetenen Besuch wettmachen werde. Denn nicht jeder führte sich auf, wie es einem braven Soldaten in Feindesland wohl ansteht. Unter andern drang damals ein brauner preußischer Husar, der ein böser Mensch war, in das Haus eines friedlichen Mannes ein, nahm ihm all sein bares Geld, so viel war, und viel Geldswert, zuletzt auch noch das schöne Bett mit nagelneuem Oberzug, und mißhandelte Mann und Frau. Ein Knabe von 8 Jahren bat ihn knieend, er möchte doch seinen Eltern nur das Bett wiedergehen. Der Husar stoßt ihn unbarmherzig von sich. Die Tochter lauft ihm nach, hält ihn am Dolman fest, und fleht um Barmherzigkeit. Er nimmt sie, und wirft sie in den Sodbrunnen, so im Hofe steht, und rettet seinen Raub. Nach Jahr und Tagen bekommt er seinen Abschied, setzt sich in der Stadt <hi rend="italic" xml:id="tg159.3.2.1">Neiße</hi> in Schlesien, denkt nimmer daran, was er einmal verübt hat, und meint, es sei schon lange Gras darüber gewachsen. Allein, was geschieht im Jahr 1806? Die Franzosen rücken in Neiße ein; ein junger Sergeant wird abends einquartiert bei einer braven Frau, die ihm wohl aufwartet.<pb n="149" xml:id="tg159.3.2.2"/> Der Sergeant ist auch brav, führt sich ordentlich auf, und scheint guter Dinge zu sein. Den andern Morgen kommt der Sergeant nicht zum Frühstück. Die Frau denkt: Er wird noch schlafen, und stellt ihm den Kaffee ins Ofenrohr. Als er noch immer nicht kommen wollte, ging sie endlich in das Stüblein hinauf, macht leise die Türe auf, und will sehen, ob ihm etwas fehlt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg159.3.3">Da saß der junge Mann wach und aufgerichtet im Bette, hatte die Hände ineinandergelegt, und seufzte, als wenn ihm ein groß Unglück begegnet wäre, oder als wenn er das Heimweh hätte, oder so etwas, und sah nicht, daß jemand in der Stube ist. Die Frau aber ging leise auf ihn zu, und fragte ihn: »Was ist Euch begegnet, Herr Sergeant, und warum seid Ihr so traurig?« Da sah sie der Mann mit einem Blick voll Tränen an, und sagte: die Oberzüge dieses Bettes, in dem er heute nacht geschlafen habe, haben vor 18 Jahren seinen Eltern in Champagne angehört, die in der Plünderung alles verloren haben und zu armen Leuten geworden sein, und jetzt denke er an alles, und sein Herz sei voll Tränen. Denn er war der Sohn des geplünderten Mannes in Champagne, und kannte die Überzüge noch, und die roten Namensbuchstaben, womit sie die Mutter gezeichnet hatte, waren ja auch noch daran. Da erschrak die gute Frau, und sagte, daß sie dieses Bettzeug von einem braunen Husaren gekauft habe, der noch hier in Neiße lebe, und sie könne nichts dafür. Da stand der Franzose auf, und ließ sich in das Haus des Husaren führen, und kannte ihn wieder.</p>
                    <p rend="zenoPLm4n0" xml:id="tg159.3.4">»Denkt Ihr noch daran«, sagte er zu dem Husaren, »wie Ihr vor 18 Jahren einem unschuldigen Mann in Champagne Hab und Gut, und zuletzt auch noch das Bett aus dem Hause getragen habt, und habt keine Barmherzigkeit gehabt, als Euch ein achtjähriger Knabe um Schonung anflehte; und an meine Schwester?« Anfänglich wollte der alte Sünder sich entschuldigen, es gehe bekanntlich im Krieg nicht alles wie es soll, und was der eine liegenlasse, hole doch ein anderer; und lieber nehme man's selber. Als er aber merkte, daß der Sergeant der nämliche sei, dessen Eltern er geplündert und mißhandelt hatte; und als er ihn an seine Schwester erinnerte,<pb n="150" xml:id="tg159.3.4.1"/> versagte ihm vor Gewissensangst und Schrecken die Stimme, und er fiel vor dem Franzosen auf die zitternde Knie nieder, und konnte nichts mehr herausbringen, als: »<hi rend="italic" xml:id="tg159.3.4.2">Pardon</hi>!« dachte aber: Es wird nicht viel helfen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg159.3.5">Der geneigte Leser denkt vielleicht auch: »Jetzt wird der Franzos den Husaren zusammenhauen«, und freut sich schon darauf. Allein das könnte mit der Wahrheit nicht bestehen. Denn wenn das Herz bewegt ist, und vor Schmerz fast brechen will, mag der Mensch keine Rache nehmen. Da ist ihm die Rache zu klein und verächtlich, sondern er denkt: Wir sind in Gottes Hand, und will nicht Böses mit Bösem vergelten. So dachte der Franzose auch, und sagte: »Daß du mich mißhandelt hast, das verzeihe ich dir. Daß du meine Eltern mißhandelt und zu armen Leuten gemacht hast, das werden dir meine Eltern verzeihen. Daß du meine Schwester in den Brunnen geworfen hast, und ist nimmer davongekommen, das verzeihe dir Gott.« – Mit diesen Worten ging er fort, ohne dem Husaren das Geringste zuleide zu tun, und es ward ihm in seinem Herzen wieder wohl. Dem<pb n="151" xml:id="tg159.3.5.1"/> Husaren aber war es nachher zumut, als wenn er vor dem Jüngsten Gericht gestanden wäre, und hätte keinen guten Bescheid bekommen. Denn er hatte von der Zeit an keine ruhige Stunde mehr, und soll nach einem Vierteljahr gestorben sein.</p>
                    <p rend="zenoPLm4n0" xml:id="tg159.3.6">
                        <hi rend="italic" xml:id="tg159.3.6.1">Merke</hi>: Man muß in der Fremde nichts tun, worüber man sich daheim nicht darf finden lassen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg159.3.7">
                        <hi rend="italic" xml:id="tg159.3.7.1">Merke</hi>: Es gibt Untaten, über welche kein Gras wächst.</p>
                    <closer>
                        <seg type="closer" rend="zenoPR" xml:id="tg159.3.8">[1809]</seg>
                    </closer>
                </div>
            </div>
        </body>
    </text>
</TEI>