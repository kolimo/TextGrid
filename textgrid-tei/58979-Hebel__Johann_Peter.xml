<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg170" n="/Literatur/M/Hebel, Johann Peter/Prosa/Schatzkästlein des rheinischen Hausfreundes/Das Bombardement von Kopenhagen">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Das Bombardement von Kopenhagen</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Hebel, Johann Peter: Element 00177 [2011/07/11 at 20:28:22]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title> Johann Peter Hebel: Poetische Werke. Nach den Ausgaben letzter Hand und der Gesamtausgabe von 1834 unter Hinzuziehung der früheren Fassungen, München: Winkler, 1961.</title>
                        <author key="pnd:118547453">Hebel, Johann Peter</author>
                    </titleStmt>
                    <extent>165-</extent>
                    <publicationStmt>
                        <date when="1961"/>
                        <pubPlace>München</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1760" notAfter="1826"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg170.3">
                <div type="h4">
                    <head type="h4" xml:id="tg170.3.1">Das Bombardement von Kopenhagen</head>
                    <p xml:id="tg170.3.2">In der ganzen gefahrvollen Zeit von 1789 an, als ein Land nach dem andern entweder in die Revolution oder in einen blutigen Krieg gezogen wurde, hatte sich das Königreich Dänemark teils durch seine Lage, teils durch die Weisheit seiner Regierung den Frieden erhalten. Sie lebte niemand zulieb und niemand zuleid, dachte nur darauf, den Wohlstand der Untertanen zu vermehren, und wurde deswegen von allen Mächten in Ehren gehalten. Als aber im Jahr 1807 der Engländer sah, daß Rußland und Preußen von ihm abgegangen sei, und mit dem Feind Frieden gemacht habe, und, daß die Franzosen in allen Häfen und festen Plätzen an der Ostsee Meister sind, und die Sache schlimm gehen kann, wenn sie auch noch sollten nach Dänemark kommen, sagte er kein Wort, sondern ließ eine Flotte auslaufen, und niemand wußte wohin. Als aber die Flotte im Sund und an der dänischen Küste und vor der königlichen Haupt- und Residenzstadt Kopenhagen stand, und alles sicher und ruhig war, so machten die Engländer Bericht nach Kopenhagen hinein: »Weil wir so gute Freunde zusammen sind, so geht uns gutwillig bis zum Frieden eure Flotte, damit sie nicht in des Feindes Hände kommt, und die Festung. Denn es wäre uns entsetzlich leid, wenn wir euch müßten die Stadt über dem Kopf zusammenschießen.« Als wenn ein Bürgersmann oder Bauer mit einem andern einen Prozeß hat, und kommt in<pb n="165" xml:id="tg170.3.2.1"/> der Nacht mit seinen Knechten einem Nachbarn vor das Bette, und sagt: »Nachbar, weil ich mit meinem Gevattermann einen Prozeß habe, so müßt Ihr mir bis Ausgangs der Sache Eure Rosse in meine Verwahrung geben, daß mein Gegenpart nicht kann darauf zu den Advokaten reiten, sonst zünd ich Euch das Haus an, und müßt mir erlauben, daß ich an der Straße mit meinen Knechten in Euer Kornfeld stehe, auf daß, wenn der Gevattermann auf seinem eigenen Roß zum Hofgericht reiten will, so verrenn ich ihm den Weg.« Der Nachbar sagt: »Laßt mir mein Haus unangezündet! Was gehn mich eure Händel an?« Und so sagten die Dänen auch. Als aber der Engländer fragte: »Wollt ihr gutwillig oder nicht?« und die Dänen sagten: »Nein, wir wollen nicht gutwillig!« so stieg er mit seinen Landungstruppen ans Ufer, rückte immer näher gegen die Hauptstadt, richtete Batterien auf, führte Kanonen drein, und sagte am 2. September nach dem Frieden von Tilsit: jetzt sei die letzte Frist. Allein alle Einwohner von Kopenhagen und die ganze dänische Nation sagten: das Betragen des übermütigen Feindes sei unerhört, und es wäre eine Schande, die der Belt nicht abwaschen könnte, sich durch Drohungen schrecken zu lassen, und in seine ungerechten Forderungen einzuwilligen. Nein! Da fing das fürchterliche Gericht an, das über diese arme Stadt im Schicksal beschlossen war. Denn von abends um 7 Uhr an hörte das Schießen auf Kopenhagen, mit 72 Mörsern und schweren Kanonen, die ganze Nacht hindurch 12 Stunden lang nimmer auf; und ein Satan, namens Congreve, war dabei, der hatte ein neues Zerstörungsmittel erfunden, nämlich die sogenannten Brandraketen. Das war ungefähr eine Art von Röhren, die mit brennbaren Materien angefüllt wurden, und vorne mit einem kurzen spitzigen Pfeil versehen waren. Im Schuß entzündete sich die Materie, und, wenn nun der Pfeil an etwas hinfuhr, wo er Habung hatte, so blieb er stecken, manchmal, wo niemand zukommen konnte, und die Feuermaterie zündete an, was brennen konnte. Auch diese Brandraketen flogen die ganze Nacht in das arme Kopenhagen hinein. Kopenhagen hatte damals 4000 Häuser, 85965 Einwohner, 22 Kirchen, 4 königliche Schlösser, 22 Krankenspitäler, 30 Armenhäuser,<pb n="166" xml:id="tg170.3.2.2"/> einen reichen Handel und viele Fabriken. Da kann man denken, wie mancher schöne Dachstuhl in dieser angstvollen Nacht zerschmettert wurde, wie manches bange Mutterherz sich nicht zu helfen wußte, wie manche Wunde blutete, und wie die Stimme des Gebets und der Verzweiflung, das Sturmgeläute und der Kanonendonner durcheinanderging. Am 3. September, als der Tag kam, hörte das Schießen auf; und der Engländer fragte, ob sie noch nicht wollten gewonnen geben. Der Kommandant von Kopenhagen sagte: »Nein.« Da fing das Schießen nachmittags um 4 Uhr von neuem an, und dauerte bis den 4. September mittags fort, ohne Unterlaß und ohne Barmherzigkeit. Und als der Kommandant noch nicht wollte ja sagen, fing abends das Feuer wieder an, und dauerte die ganze Nacht bis den 5. des Mittags. Da lagen mehr als 300 schöne Häuser in der Asche; ganze Kirchtürme waren eingestürzt, und noch überall wütete die Flamme. Mehr als 800 Bürger waren schon getötet und mehrere schwer verwundet. Ganz Kopenhagen sah hier einer Brandstätte, oder einem Steinhaufen, da einem Lazarett, und dort einem Schlachtfeld gleich. Als endlich der Kommandant von Kopenhagen nirgends mehr Rettung noch Hülfe, und überall nur Untergang und Verderben sah, hat er am 7. September kapituliert, und der Kronprinz hat's nicht einmal gelobt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg170.3.3">Das erste war, die Engländer nahmen die ganze Seeflotte von Kopenhagen in Besitz und führten sie weg; 18 Linienschiffe, 15 Fregatten und mehrere kleinere bis auf eine Fregatte, welche der König von England ehmals dem König von Dänemark zum Geschenk gemacht hatte, als sie noch Freunde waren. Diese ließen sie zurück. Der König von Dänemark schickte sie ihnen aber auch nach, und will nichts Geschenktes mehr zum Andenken haben. Im Land selbst und auf den Schiffen hausten die Engländer als böse Feinde, denn der Soldat weiß nicht, was er tut, sondern denkt: Wenn sie es nicht verdient hätten, so führte man keinen Krieg mit ihnen. Zum Glück dauerte ihr Aufenthalt nicht lange; denn sie schifften sich am 19. Oktober wieder ein, und fuhren am 21. mit der dänischen Flotte und dem Raub davon; und der Congreve ist unterwegs ertrunken, und hat Frau und Kinder<pb n="167" xml:id="tg170.3.3.1"/> nimmer gesehen. Von dem an hielten die Dänen gemeinschaftlich mit den Franzosen, und Kaiser Napoleon will nicht eher mit den Engländern Friede machen, als bis sie die Schiffe wieder zurückgegeben, und Kopenhagen bezahlt haben. Dies ist das Schicksal von Dänemark, und die Freunde der Engländer sagen: es sei nicht so schlimm gemeint gewesen. Andre aber sagen: es hätte nicht können schlimmer sein, und die Dänen meinen's auch.</p>
                    <closer>
                        <seg type="closer" rend="zenoPR" xml:id="tg170.3.4">[1809]</seg>
                    </closer>
                </div>
            </div>
        </body>
    </text>
</TEI>