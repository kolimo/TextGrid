<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg177" n="/Literatur/M/Hebel, Johann Peter/Prosa/Schatzkästlein des rheinischen Hausfreundes/Die Kometen">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Die Kometen</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Hebel, Johann Peter: Element 00184 [2011/07/11 at 20:28:22]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title> Johann Peter Hebel: Poetische Werke. Nach den Ausgaben letzter Hand und der Gesamtausgabe von 1834 unter Hinzuziehung der früheren Fassungen, München: Winkler, 1961.</title>
                        <author key="pnd:118547453">Hebel, Johann Peter</author>
                    </titleStmt>
                    <extent>178-</extent>
                    <publicationStmt>
                        <date when="1961"/>
                        <pubPlace>München</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1760" notAfter="1826"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg177.3">
                <div type="h4">
                    <head type="h4" xml:id="tg177.3.1">Die Kometen</head>
                    <p xml:id="tg177.3.2">Der geneigte Leser ist nun bereits ein ganz anderer Mann, als vor kurzer Zeit, und wenn jetzt einmal im<hi rend="italic" xml:id="tg177.3.2.1"> wilden Mann</hi> oder in den <hi rend="italic" xml:id="tg177.3.2.2">drei Königen</hi> von den <hi rend="italic" xml:id="tg177.3.2.3">Planeten</hi> die Rede ist, und der <hi rend="italic" xml:id="tg177.3.2.4">Mars</hi> wird genannt, oder die <hi rend="italic" xml:id="tg177.3.2.5">Juno</hi>, oder der <hi rend="italic" xml:id="tg177.3.2.6">Jupiter</hi>, oder der <hi rend="italic" xml:id="tg177.3.2.7">Saturn</hi>, oder der<hi rend="italic" xml:id="tg177.3.2.8"> Uranus</hi>, so kann er auch ein Wort mitsprechen bei seinem Schöpplein, und ist nicht schuldig zu gestehn, daß er's aus dem Hausfreund hat. Der <hi rend="italic" xml:id="tg177.3.2.9">Hausfreund</hi> verlangt's nicht.</p>
                    <p rend="zenoPLm4n0" xml:id="tg177.3.3">Jetzt kommen wir zu den <hi rend="italic" xml:id="tg177.3.3.1">Kometsternen</hi>.</p>
                    <p rend="zenoPLm4n0" xml:id="tg177.3.4">Von den Kometsternen wäre nun viel zu sagen, weil man nicht viel von ihnen weiß. Allein der Hausfreund hat nie damit<pb n="178" xml:id="tg177.3.4.1"/> umgehen können den Leuten etwas anzubinden, zum Exempel einen Bären, und will sich deswegen kurz fassen, und alles in einer Predigt abtun, ob es gleich nicht nur eilf Kometsterne gibt, wie man nur von eilf Planeten weiß, sondern schon viel mehr als 400 seit undenklichen Zeiten entdeckt und beobachtet worden sind.</p>
                    <p rend="zenoPLm4n0" xml:id="tg177.3.5">Ein solcher Kometstern ist nun allemal eine sehr merkwürdige Erscheinung, wenn er so auf einmal unangemeldet und unbeschieden am Himmel sichtbar wird, und da steht, und sagt kein Wort, zumal ein solcher, wie im Jahr 1680, der 4mal so groß schien als der Abendstern, oder 146 Jahr vor Christi Geburt, der größer soll ausgesehen haben, als die Sonne, oder im Jahr 1769, dessen Schweif durch den 4. Teil des Himmels reichte, oder wenn gar zwei zugleich erscheinen, was auch schon geschehen ist. Es ist alsdann allemal, als wenn der liebe Gott einen Sternseher, ich will sagen, den Rheinischen Hausfreund, also anredete: »Meinst du, daß du jetzt fertig seist, und die Sterne des Himmels alle kennest? Sieh, da ist auch noch einer, den du noch nie gesehen hast, und wirst jetzt erst nicht wissen, was du daraus machen sollst.« Andere Leute aber schauen das Wundergestirn auch mit Begierde und Staunen an, und die Mutter zeigt es dem Kind, und sagt: »Sieh, wie wunderbar die göttliche Allmacht ist!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg177.3.6">Solche Kometsterne nun, sind einander nicht alle gleich, auch der nämliche, solang man ihn beobachten kann, verändert oft sein Aussehen, sie sind bald heller bald trüber, bald größer bald kleiner, rund und eckig, näher oder weiter von uns entfernt. Der Komet im Jahr 1770, war daheim 13mal größer als der Mond, ob man ihn gleich wegen der weiten Entfernung hierzuland nicht dafür angesehen hat. Einer im J. 1680, war 160mal näher bei der Sonne, als die Erde bei ihr ist. Einer im J. 1770, war 7mal weiter von der Erde weg als der Mond. Einige sind so weit entfernt, oder so klein, daß nur wir Sternseher und Kalendermacher mit unsern Perspektiven sie entdecken können, andere kann man ohne Zweifel gar nicht sehen, weil sie zu weit entfernt sind, oder bei Tag am Himmel stehen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg177.3.7">Die Kometsterne haben viel Ähnliches mit den Planeten<pb n="179" xml:id="tg177.3.7.1"/> und drehen sich ebenso wie sie um die Sonne herum. Aber sie sind auch wieder sehr von den Planeten verschieden. Sie werden nur selten sichtbar – sie haben keine so feste und kernhafte Masse als die Erde oder andere Planeten – sie sind mit einem schönen leuchtenden Schweif geziert. – Sie bedeuten ein großes Unglück.</p>
                    <p rend="zenoPLm4n0" xml:id="tg177.3.8">Sage erstens, sie erscheinen viel seltener, als die Planeten, die alle Tage am Himmel auf- und untergehen, denn sie sind nicht immer so nahe bei der Sonne oder bei uns, wie die Planeten. Nein, sondern sie sind rechte Nachtläufer und scheuen sich nicht in die Fremde zu gehen, wie manches Mutterkind sich scheut. Wenn so ein Stern einmal um die Sonne herum ist, und hat sich an ihr erwärmt, und einen kräftigen Sommer gehabt, so zieht er in einer langen langen Linie hinweg und in seinen Winter hinaus, weiß niemand wohin. Wenn er alsdann 30 oder 100 oder viele hundert Jahre lang immer weiter und weiter hinweg gezogen ist, und es fällt ihm ein, so kehrt er wieder um, damit er sich wieder einmal an der lieben Sonne recht erwärmen kann, und braucht wieder ebensoviel Zeit zu seiner Herreise, und selten einer, der ihn zum erstenmal gesehen hat, wartet's aus bis er wiederkommt, sondern legt sich schlafen, und bekümmert sich nachher nichts mehr darum. Es ist aufgeschrieben, daß ein Komet im Jahr 1456, einer im Jahr 1531, einer im Jahr 1607, einer im Jahr 1682 gestanden sei. Weil nun immer von einer Zeit zur andern ein Zwischenraum von ungefähr 76 Jahren etwas mehr oder weniger verflossen war, so behauptete ein gelehrter Mann, namens <hi rend="italic" xml:id="tg177.3.8.1">Halley</hi>, es sei allemal der nämliche gewesen, und er müßte längstens bis Anno 1759 wiederkommen, was auch richtig geschehen ist, und so muß er ungefähr im Jahr 1830 ebenfalls wieder erscheinen. Der Hausfreund will's seinem Nachfolger überlassen, den geneigten Leser bis dorthin wieder daran zu erinnern. Ebenso behauptete einst ein anderer Gelehrter, der Kometstern von 1532 und 1661 sei der nämliche und müsse deshalb im Jahr 1790 wiederkommen, ist aber doch ausgeblieben.</p>
                    <p rend="zenoPLm4n0" xml:id="tg177.3.9">Sage zweitens, der Kometstern hat keine so feste Masse, wie die Erde, oder ein anderer Planet. Einige sehen aus, wie<pb n="180" xml:id="tg177.3.9.1"/> ein bloßer Dunst, also, daß man durch sie hindurch die andern Sternlein will sehen können, die hinter ihnen stehen. Andere sind zwar schon etwas dichter, haben aber doch das Ansehen, als wenn nicht alles daran recht aneinanderhinge, sondern viel leere Zwischenräume da wären. Einige Gelehrte wollen jedoch behaupten, daß ein solcher Komet auf seiner langen Reise, wenn ihm unterwegs kein Unglück begegnet, immer dichter werden, und zuletzt die völlige Natur und Eigenschaft eines Planeten annehmen könne. Unsere Erde könne wohl auch einmal eine bloße Dunstkugel von viel 1 000 Meilen im Umfang gewesen sein, hernach sei sie immer wässeriger worden, dann habe sich das feste Land angesetzt, das Land und das Wasser habe sich geschieden, und sei zuletzt das draus worden, was jetzt ist. Aus Respekt vor der himmlischen Allmacht mischt sich der Hausfreund nicht in diesen Streit.</p>
                    <p rend="zenoPLm4n0" xml:id="tg177.3.10">Sage drittens, die Kometsterne sind mit einem schönen leuchtenden Schweif geziert, aber nicht alle. Einige zum Beispiel haben rings um sich bloß einen Strahlenschein, als wenn sie mit leuchtenden Haaren eingefaßt wären, wie in den großen Bibeln die Köpfe der heiligen Evangelisten und Apostel aussehen, und Johannes des Täufers. Hat aber ein solcher Stern einen Schweif, so hat er allemal das Ansehen eines Dunstes, der von Strahlen erhellt ist. Man kann hinter ihm immer die Sterne sehen, an denen er vorbeizieht, er ist immer etwas gebogener, wird bald größer bald kleiner, heller und bleicher. Er ist nie auf der Seite des Kometen, die gegen der Sonne steht, sondern allemal auf der entgegengesetzten. Sonst weiß man noch nicht für gewiß, was es mit ihm für eine Bewandtnis hat. Dem Hausfreund will manchmal vorkommen, es sei nur der Schein von Sonnenstrahlen die durch den dunstigen oder wässerigen Kometen hindurchfallen. Der geneigte Leser beliebe aber vorsichtig zu sein mit diesem Geheimnis, denn es wissen's noch nicht viel Leute.</p>
                    <p rend="zenoPLm4n0" xml:id="tg177.3.11">Sage viertens, der Komet bedeutet ein Unglück. Man darf sicher darauf rechnen, entweder es entsteht innerhalb Jahresfrist ein Krieg, oder ein Erdbeben, oder es gehen ganze Städte und Königreiche unter, oder es stirbt ein mächtiger Monarch,<pb n="181" xml:id="tg177.3.11.1"/> oder geschieht sonst etwas, woran niemand eine Freude haben kann. Dies ist aber nicht so zu verstehen, als wenn der Komet das Unglück herbeizöge, oder deswegen erschiene, um wie ein Postreuter es anzuzeigen. Nein, der Komet weiß nichts von uns. Er kommt wenn seine Stunde da ist. Man kann ihn auf den andern Planeten ebenso gut sehen als auf der Erde. Wir aber da unten, mit unsern Leiden und Freuden, mit unsern Herzen voll Furcht und Hoffnung, mit unsern Lustgärten und Kirchhöfen, sind in Gottes Hand. Allein es geschieht auf dem weiten Erdenrund, irgendwo, diesseits oder jenseits des Meeres, alle Jahre so gewiß ein großes Unglück, daß diejenigen, welche aus einem Kometen Schlimmes prophezeihen, gewonnen Spiel haben, er mag kommen, wann er will. Gerade als wenn ein schlauer Gesell in einem großen Dorf oder Marktflecken in der Neujahrsnacht auf der Straße stünde und nach den Sternen schaute und sagte: »Ich sehe kuriose Sachen da oben, dieses Jahr stirbt jemand im Dorf.« Der geneigte Leser darf nur an die letzten 20 Jahre zurückdenken, an die Revolutionen und Freiheitsbäume hin und wieder, an den plötzlichen Tod des Kaisers Leopolds, an das Ende des König Ludwigs des Sechszehnten, an die Ermordung des türkischen Kaisers, an die blutigen Kriege in Deutschland, in den Niederlanden, in der Schweiz, in Italien, in Polen, in Spanien, an die Schlachten bei Austerlitz und Eylau, bei Eßlingen und Wagram, an das gelbe Fieber, an die Petechen und Viehseuchen, an die Feuersbrünste in Kopenhagen, Stockholm und Konstantinopel, an die Zucker- und Kaffeeteurung, leider, wenn von 1789 bis 1810 alle Jahre ein anderer Komet, ja sechs auf einmal am Himmel erschienen wären, es wäre keiner von ihnen mit Schimpf bestanden.</p>
                    <p rend="zenoPLm4n0" xml:id="tg177.3.12">Soviel von den Kometen. Die Sterne, welche nächstens sollen beschrieben werden, bedeuten insgesamt Frieden und Liebe und Gottes allmächtigen Schutz.</p>
                    <closer>
                        <seg type="closer" rend="zenoPR" xml:id="tg177.3.13">[1810]</seg>
                    </closer>
                    <lb xml:id="tg177.3.14"/>
                    <closer>
                        <seg type="closer" rend="zenoPC" xml:id="tg177.3.15">[Fortsetzung <ref cRef="/Literatur/M/Hebel, Johann Peter/Prosa/Schatzkästlein des rheinischen Hausfreundes/Fortgesetzte Betrachtung des Weltgebäudes" xml:id="tg177.3.15.1">hier</ref>]</seg>
                        <pb n="182" xml:id="tg177.3.16"/>
                    </closer>
                </div>
            </div>
        </body>
    </text>
</TEI>