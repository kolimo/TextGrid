<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg199" n="/Literatur/M/Hebel, Johann Peter/Prosa/Schatzkästlein des rheinischen Hausfreundes/Schreckliche Unglücksfälle in der Schweiz">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Schreckliche Unglücksfälle in der Schweiz</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Hebel, Johann Peter: Element 00206 [2011/07/11 at 20:28:22]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title> Johann Peter Hebel: Poetische Werke. Nach den Ausgaben letzter Hand und der Gesamtausgabe von 1834 unter Hinzuziehung der früheren Fassungen, München: Winkler, 1961.</title>
                        <author key="pnd:118547453">Hebel, Johann Peter</author>
                    </titleStmt>
                    <extent>216-</extent>
                    <publicationStmt>
                        <date when="1961"/>
                        <pubPlace>München</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1760" notAfter="1826"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg199.3">
                <div type="h4">
                    <head type="h4" xml:id="tg199.3.1">Schreckliche Unglücksfälle in der Schweiz</head>
                    <p xml:id="tg199.3.2">Hat jede Gegend ihr Liebes, so hat sie auch ihr Leides, und wer manchmal erfährt, was an andern Orten geschieht, findet wohl Ursache, zufrieden zu sein mit seiner Heimat. Hat z.B. die Schweiz viel herdenreiche Alpen, Käse und Butter und Freiheit, so hat sie auch Lavinen. Der 12. Dezember<pb n="216" xml:id="tg199.3.2.1"/> des Jahrs 1809 brachte für die hohen Bergtäler dieses Landes eine fürchterliche Nacht, und lehrt uns, wie ein Mensch wohl täglich Ursache hat, an das Sprüchlein zu denken: »Mitten wir im Leben sind mit dem Tod umfangen.« Auf allen hohen Bergen lag ein tiefer frisch gefallener Schnee. Der zwölfte Dezember brachte Tauwind und Sturm. Da dachte jedermann an großes Unglück, und betete. Wer sich und seine Wohnung für sicher hielt, schwebte in Betrübnis und Angst für die Armen, die es treffen wird, und wer sich nicht für sicher hielt, sagte zu seinen Kindern: »Morgen geht uns die Sonne nimmer auf«, und bereitete sich zu einem seligen Ende. Da rissen sich auf einmal und an allen Orten von den Firsten der höchsten Berge die Lavinen oder Schneefälle los, stürzten mit entsetzlichem Tosen und Krachen über die langen Halden herab, wurden immer größer und größer, schossen immer schneller, toseten und krachten immer fürchterlicher, und jagten die Luft vor sich und so durcheinander, daß im Sturm, noch ehe die Lavine ankam, ganze Wälder zusammenkrachten, und Ställe, Scheuren und Waldungen wie Spreu davonflogen, und wo die Lavinen sich in den Tälern niederstürzten, da wurden stundenlange Strecken, mit allen Wohngebäuden, die darauf standen, und mit allem Lebendigen, was darin atmete, erdrückt und zerschmettert, wer nicht wie durch ein göttliches Wunder gerettet wurde.</p>
                    <p rend="zenoPLm4n0" xml:id="tg199.3.3">Einer von zwei Brüdern in Uri, die miteinander hauseten, war auf dem Dach, das hinten an den Berg anstoßt, und dachte: »Ich will den Zwischenraum zwischen dem Berg und dem Dächlein mit Schnee ausfüllen und alles eben machen, auf daß, wenn die Lavine kommt, so fährt sie über das Häuslein weg, daß wir vielleicht« – und als er sagen wollte: »daß wir vielleicht mit dem Leben davonkommen« – da führte ihn der plötzliche Windbraus, der vor der Lavine hergeht, vom Dach hinweg und hob ihn schwebend in der Luft, wie einen Vogel über einem entsetzlichen Abgrund. Und als er eben in Gefahr war in die unermeßliche Tiefe hinabzustürzen, und wäre seines Gebeins nimmer gefunden worden, da streifte die Lavine an ihm vorbei und warf ihn seitwärts an eine Halde. Er sagt, es habe ihm nicht wohlgetan, aber in der<pb n="217" xml:id="tg199.3.3.1"/> Betäubung umklammerte er noch einen Baum, an dem er sich festhielt, bis alles vorüber war, und kam glück lich davon und ging wieder heim zu seinem Bruder, der auch noch lebte, obgleich der Stall neben dem Häuslein wie mit einem Besen weggewischt war. Da konnte man wohl auch sagen: »Der Herr hat seinen Engeln befohlen über dir, daß sie dich auf den Händen tragen. Denn er macht Sturmwinde zu seinen Boten, und die Lavinen, daß sie seine Befehle ausrichten.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg199.3.4">Anders erging es im <hi rend="italic" xml:id="tg199.3.4.1">Sturnen</hi>, ebenfalls im Kanton Uri. Nach dem Abendsegen sagte der Vater zu der Frau und den drei Kindern: »Wir wollen doch auch noch ein Gebet verrichten für die armen Leute, die in dieser Nacht in Gefahr sind.« Und während sie beteten, donnerte schon aus allen Tälern der ferne Widerhall der Lavinen, und während sie noch beteten, stürzte plötzlich der Stall und das Haus zusammen. Der Vater wurde vom Sturmwind hinweggeführt, hinaus in die fürchterliche Nacht, und unten am Berg abgesetzt und von dem nachwehenden Schnee begraben. Noch lebte er, als er aber den andern Morgen mit unmenschlicher Anstrengung sich hervorgegraben, und die Stätte seiner Wohnung wieder erreicht hatte, und sehen wollte was aus den Seinigen geworden sei, barmherziger Himmel! da war nur Schnee und Schnee, und kein Zeichen einer Wohnung, keine Spur des Lebens mehr wahrzunehmen. Doch vernahm er nach langem ängstlichem Rufen, wie aus einem tiefen Grab, die Stimme seines Weibes unter dem Schnee herauf. Und als er sie glücklich und unbeschädiget hervorgegraben hatte, da hörten sie plötzlich noch eine bekannte und liebe Stimme: <hi rend="italic" xml:id="tg199.3.4.2">»Mutter, ich wäre auch noch am Leben«</hi>, rief ein Kind, <hi rend="italic" xml:id="tg199.3.4.3">»aber ich kann nicht heraus</hi>.« Nun arbeitete Vater und Mutter noch einmal und brachten auch das Kind hervor, und ein Arm war ihm abgebrochen. Da ward ihr Herz mit Freude und Schmerzen erfüllt, und von ihren Augen flossen Tränen des Dankes und der Wehmut. Denn die zwei andern Kinder wurden auch noch herausgegraben, aber tot.</p>
                    <p rend="zenoPLm4n0" xml:id="tg199.3.5">In <hi rend="italic" xml:id="tg199.3.5.1">Pilzeig</hi>, ebenfalls im Kanton Uri, wurde eine Mutter mit zwei Kindern fortgerissen, und unten in der Tiefe vom Schnee verschüttet. Ein Mann, ihr Nachbar, den die Lavine<pb n="218" xml:id="tg199.3.5.2"/> ebenfalls dahin geworfen hatte, hörte ihr Wimmern und grub sie hervor. Vergeblich war das Lächeln der Hoffnung in ihrem Antlitz. Als die Mutter halbnackt umherschaute, kannte sie die Gegend nicht mehr, in der sie war. Ihr Retter selbst war unmächtig niedergesunken. Neue Hügel und Berge von Schnee, und ein entsetzlicher Wirbel von Schneeflocken füllten die Luft. Da sagte die Mutter: »Kinder, hier ist keine Rettung möglich; wir wollen beten, und uns dem Willen Gottes überlassen.« Und als sie beteten, sank die siebenjährige Tochter sterbend in die Arme der Mutter, und als die Mutter mit gebrochenem Herzen ihr zusprach, und ihr Kind der Barmherzigkeit Gottes empfahl, da verließen sie ihre Kräfte auch. Sie war eine 14tägige Kindbetterin, und sie sank mit dem teuren Leichnam ihres Kindes in dem Schoß, ebenfalls leblos darnieder. Die andere eilfjährige Tochter hielt weinend und händeringend bei der Mutter und Schwester aus, bis sie tot waren, drückte ihnen alsdann, eh sie auf eigene Rettung bedache war, mit stummem Schmerz die Augen zu, und arbeitete sich mit unsäglicher Mühe und Gefahr erst zu einem Baum, dann zu einem Felsen herauf und kam gegen Mitternacht endlich an ein Haus, wo sie zum Fenster hinein aufgenommen, und mit den Bewohnern des Hauses erhalten wurde.</p>
                    <p rend="zenoPLm4n0" xml:id="tg199.3.6">Kurz, in allen Bergkantonen der Schweiz, in Bern, Glarus, Uri, Schwitz, Graubündten, sind in <hi rend="italic" xml:id="tg199.3.6.1">einer</hi> Nacht, und fast in der nämlichen Stunde, durch die Lavinen ganze Familien erdrückt, ganze Viehherden mit ihren Stallungen zerschmettere, Matten und Gartenland bis auf den nackten Felsen hinab aufgeschürft und weggeführt, und ganze Wälder zerstöre worden, also daß sie ins Tal gestürzt sind, oder die Bäume lagen übereinander zerschmettere und zerknickt, wie die Halmen auf einem Acker nach dem Hagelschlag. Sind ja in dem einzigen kleinen Kanton Uri fast mit<hi rend="italic" xml:id="tg199.3.6.2"> einem</hi> Schlag 11 Personen unter dem Schnee begraben worden, und sind nimmer auferstanden, gegen 30 Häuser, und mehr als 150 Heuställe zerstöre und 359 Häuptlein Vieh umgekommen, und man wußte nicht, auf wievielmal hunderttausend Gulden soll man den Schaden berechnen, ohne die verlornen Menschen. Denn das<pb n="219" xml:id="tg199.3.6.3"/> Leben eines Vaters oder einer Mutter oder frommen Gemahls oder Kindes ist nicht mit Geld zu schätzen.</p>
                    <closer>
                        <seg type="closer" rend="zenoPR" xml:id="tg199.3.7">[1810]</seg>
                    </closer>
                </div>
            </div>
        </body>
    </text>
</TEI>