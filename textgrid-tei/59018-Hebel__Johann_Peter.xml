<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg209" n="/Literatur/M/Hebel, Johann Peter/Prosa/Schatzkästlein des rheinischen Hausfreundes/Die Fixsterne">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Die Fixsterne</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Hebel, Johann Peter: Element 00216 [2011/07/11 at 20:28:22]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title> Johann Peter Hebel: Poetische Werke. Nach den Ausgaben letzter Hand und der Gesamtausgabe von 1834 unter Hinzuziehung der früheren Fassungen, München: Winkler, 1961.</title>
                        <author key="pnd:118547453">Hebel, Johann Peter</author>
                    </titleStmt>
                    <extent>234-</extent>
                    <publicationStmt>
                        <date when="1961"/>
                        <pubPlace>München</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1760" notAfter="1826"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg209.3">
                <div type="h4">
                    <head type="h4" xml:id="tg209.3.1">Die Fixsterne</head>
                    <milestone unit="head_start"/>
                    <p rend="zenoPC" xml:id="tg209.3.2">Fortsetzung</p>
                    <milestone unit="head_end"/>
                    <lb xml:id="tg209.3.3"/>
                    <p xml:id="tg209.3.4">Was bisher über die Fixsterne gesagt worden ist, kann zum Teil mit dem leiblichen Auge gesehen und erkannt werden. Allein das Auge des Verstandes sieht mehr als das Auge des Leibes.</p>
                    <p rend="zenoPLm4n0" xml:id="tg209.3.5">
                        <hi rend="italic" xml:id="tg209.3.5.1">Erstlich</hi>, die Fixsterne sind so weit von uns entfernt, daß gar kein Mittel mehr möglich ist, ihre ungeheure Entfernung auszurechnen. Merke: der nächste Fixstern bei uns ist ohne Zweifel der <hi rend="italic" xml:id="tg209.3.5.2">Sirius</hi> oder<hi rend="italic" xml:id="tg209.3.5.3"> Hundsstern</hi>, den der Herr Pfarrer auch kennt. Man schließt es aus seiner Größe und aus seinem wunderschönen Glanz, mit dem er vor allen andern Sternen herausstrahlt. Dessen ungeachtet muß er doch zum allerwenigsten 27664mal weiter von uns entfernt sein, als die Sonne, denn wenn er näher wäre, so könnte man's wissen, und eine Kanonenkugel im Sirius abgeschossen, müßte mit gleicher Geschwindigkeit mehr als 600000 Jahre lang fliegen, ehe sie die Erde erreichte. Ja man könnte noch viel mehr sagen. Aber dies soll genug sein, sonst glaubt's der geneigte Leser nicht. Ebenso weit, als der Sirius von der Erde entfernt ist, ebenso weit ungefähr ist er von der Sonne entfernt. Denn auf ein paar Millionen Meilen kömmt's hier gar nicht an.</p>
                    <p rend="zenoPLm4n0" xml:id="tg209.3.6">
                        <hi rend="italic" xml:id="tg209.3.6.1">Zweitens</hi>, der Sirius, der aus einer so unermeßlichen Weite doch noch so groß aussieht, und so ein strahlendes Licht zu uns herabwirft, muß in seiner Heimat wenigstens ebenso groß, nein, er muß noch viel größer als die Sonne, und folglich selber eine glorreiche strahlende Sonne sein. Das kann nicht fehlen. Haben wir aber Ursache, für gewiß zu glauben, der Sirius sei daheim eine Sonne, so haben wir Ursache zu glauben, jeder andere Fixstern sei auch eine Sonne. Denn wenn sie uns auch noch soviel kleiner erscheinen, so sind sie nur<pb n="234" xml:id="tg209.3.6.2"/> noch soviel weiter von uns entfernt. Aber alle strahlen in ihrem eigentümlichen ewigen Lichte, oder wo hätten sie's sonst her?</p>
                    <p rend="zenoPLm4n0" xml:id="tg209.3.7">
                        <hi rend="italic" xml:id="tg209.3.7.1">Drittens</hi>, die Entfernung unserer Sonne von dem Sirius dient uns nun zu einem mutmaßlichen Maßstab, wie weit eine himmlische Sonne oder ein Stern von dem andern entfernt sei. Denn wenn zwischen unserer Sonne und der Siriussonne ein Zwischenraum ist, den eine Kanonenkugel in 600000 Jahren nicht durchfliegen könnte, so kann man wohl glauben, daß die andern Sonnen auch ebenso weit jede von der nächsten entfernt sei, bis zur obersten Milchstraße hinauf, wo sie so klein scheinen und so nahe beieinander, daß uns ein paar hundert von ihnen zusammen kaum aussehen wie ein Nebelfleck, den man mit einem badischen Sechskreuzerstück bedecken könnte. Es gehört nicht viel Verstand dazu, daß er einem stillstehe.</p>
                    <p rend="zenoPLm4n0" xml:id="tg209.3.8">Wenn man nun <hi rend="italic" xml:id="tg209.3.8.1">viertens</hi> das alles bedenkt, so will es nicht scheinen, daß alle diese zahllosen Sterne, zumal diejenigen, die man mit bloßem Auge nicht sehen kann, nur wegen uns erschaffen worden wären, und damit der Kalendermacher für des Lesers Geld etwas darüber schreiben könnte. Wie wenn man in der fremden Stadt auf einer Pilgerreise über Nacht ist, und sieht zum erstenmal durch das Fensterlein der Schlafkammer heraus, rechts und links und über 20 Häuser hinaus, sieht man noch viel solche Lichter abermal brennen, wie in dem Schlafstüblein auch eins schimmert. Geneigter Pilger, diese Lichter sind nicht wegen deiner angezündet, daß es in dem Schlafstüblein lustig aussehe, sondern jedes dieser Lichter erleuchtet eine Stube, und es sitzen Leute dabei und lesen die Zeitung, oder den Abendsegen, oder sie spinnen und stricken, oder spielen Trumpfaus, und das Büblein macht ein Rechnungsexempel aus der Regeldetri.</p>
                    <p rend="zenoPLm4n0" xml:id="tg209.3.9">Gleicherweise wollen verständige Leute glauben, wo in einer solchen Entfernung von uns, in einer solchen Entfernung voneinander so unzählige prachtvolle Sonnen strahlen, da müssen auch Planeten und Erdkörper zu einer jeden derselben gehören, welche von ihr Licht und Wärme und Freude empfangen,<pb n="235" xml:id="tg209.3.9.1"/> wie unsere Planeten von unserer Sonne, und es müssen darauf lebendige und vernünftige Geschöpfe wohnen, wie auf unserer Erde, die sich des himmlischen Lichtes erfreuen und ihren Schöpfer anbeten, und wenn sie etwa bei Nacht in den glanzvollen Himmel herausschauen, wer weiß, so erblicken sie auch unsere Sonne wie ein kleines Sternlein, aber unsere Erde sehen sie nicht, und wissen nichts davon, daß in Östreich Krieg war, und daß die Türken die Schlacht bei Silistria gewonnen haben. Sie sehen nicht die Schönheit unserer Erde, wenn der Frühling voll Blüten und Sommervögel an allen Bäumen und Hecken hängt und wir sehen die Schönheit ihres himmlischen Frühlings nicht. Aber der ewige und allmächtige Geist, der alle diese Lichter angezündet hat, und alle die Heere von Weltkörpern in den Händen trägt, sieht das Kindlein lächeln auf der Mutter Schoß, und die Braut weinen um des Bräutigams Tod, und umfaßt die Erde und den Himmel und aller Himmel Himmel mit Liebe und Erbarmung. Seines Orts dem Hausfreund, wenn er den Sternenhimmel betrachtet, es wird ihm zumut, als wenn er in die göttliche Vorsehung hineinschaute, und jeder Stern verwandelt sich in ein Sprüchlein. Der erste sagt: <hi rend="italic" xml:id="tg209.3.9.2">Deine Jahre währen für und für, du hast vorhin die Erde gegründet und die Himmel sind deiner Hände Werk</hi>. Der zweite sagt: <hi rend="italic" xml:id="tg209.3.9.3">Bin ich nicht ein Gott der nahe ist, spricht der Herr, und nicht ein Gott der ferne sei? Meinest du, daß sich jemand so heimlich verbergen könne, daß ich ihn nicht sehe</hi>? Der dritte sagt: <hi rend="italic" xml:id="tg209.3.9.4">Herr, du erforschest mich und kennest mich, und siehest alle meine Wege</hi>. Der vierte sagt: <hi rend="italic" xml:id="tg209.3.9.5">Was ist der Mensch, daß du sein gedenkest, und Adams Kind, daß du dich sein annimmst</hi>? Der fünfte sagt: <hi rend="italic" xml:id="tg209.3.9.6">Und ob auch eine Mutter ihres Kindes vergäße, so will ich doch deiner nicht vergessen: spricht der Herr</hi>.</p>
                    <p rend="zenoPLm4n0" xml:id="tg209.3.10">Deswegen hat der Hausfreund im Kapitel von den Kometen geschrieben, unten am Ende: Die Sterne, die zum Beschluß sollen erklärt werden, bedeuten insgesamt Friede und Liebe und Gottes allmächtigen Schutz. Er weiß noch wohl, was er geschrieben hat.</p>
                    <closer>
                        <seg type="closer" rend="zenoPR" xml:id="tg209.3.11">[1811]</seg>
                        <pb n="236" xml:id="tg209.3.12"/>
                    </closer>
                </div>
            </div>
        </body>
    </text>
</TEI>