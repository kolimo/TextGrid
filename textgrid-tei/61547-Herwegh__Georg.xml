<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg266" n="/Literatur/M/Herwegh, Georg/Schriften/Platens Lieder und Romanzen">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Platens Lieder und Romanzen</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Herwegh, Georg: Element 00286 [2011/07/11 at 20:28:51]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <notesStmt>
                <note>
                              Erstdruck in: Deutsche Volkshalle (Bellevue), 1839.
                           </note>
            </notesStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Herweghs Werke in drei Teilen. Herausgegeben mit Einleitungen und Anmerkungen versehen von Hermann Tardel, Berlin, Leipzig, Wien, Stuttgart: Bong &amp; Co., [1909].</title>
                        <author key="pnd:118550128">Herwegh, Georg</author>
                    </titleStmt>
                    <extent>31-</extent>
                    <publicationStmt>
                        <date when="1909"/>
                        <pubPlace>Stuttgart</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1817" notAfter="1875"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg266.3">
                <div type="h3">
                    <milestone unit="head_start"/>
                    <head type="h3" xml:id="tg266.3.1">Georg Herwegh</head>
                    <head type="h2" xml:id="tg266.3.2">Platens Lieder und Romanzen</head>
                    <p xml:id="tg266.3.3">
                        <seg rend="zenoTXFontsize80" xml:id="tg266.3.3.1"> Ihr könnt mich nur nach leichten Worten messen,</seg>
                    </p>
                    <p xml:id="tg266.3.4">
                        <seg rend="zenoTXFontsize80" xml:id="tg266.3.4.1"> In diesen Busen konntet ihr nicht sehn:</seg>
                    </p>
                    <p xml:id="tg266.3.5">
                        <seg rend="zenoTXFontsize80" xml:id="tg266.3.5.1"> Ach, jeder Schmerz ist nur ein Selbstvergessen,</seg>
                    </p>
                    <p xml:id="tg266.3.6">
                        <seg rend="zenoTXFontsize80" xml:id="tg266.3.6.1"> Und jedes Lächeln kommt mich hoch zu stehn.</seg>
                    </p>
                    <milestone unit="head_end"/>
                    <lb xml:id="tg266.3.7"/>
                    <p xml:id="tg266.3.8">Man hat es vielleicht kaum begriffen, manche allzuleidenschaftliche Freunde haben es mir wohl gar bitter verdacht, daß ich einen Mann, der nie in die nächste Berührung mit dem Volke gekommen, so hastig und begeistert in die Reihe der Demokraten eingeführt habe. Das ist eben der beklagenswerte, unverzeihliche Fehler unserer Partei, daß sie überall sogleich abspricht, wo sie nicht den <hi rend="italic" xml:id="tg266.3.8.1">unmittelbarsten</hi> Ausdruck ihrer Sinn- und Denkweise findet, daß sie so blind ist, den Genius der Freiheit zu verkennen, wenn er einmal statt der Jakobinermütze den Lorbeer trägt. Das Auditorium eines Dichters ist immer zahlreicher, als das eines Publizisten; der Demokrat sollte daher mit viel weniger Mißtrauen und weit mehr Liebe an einen Sänger herangehen, der, wenn er auch keine Adressen verfertigt und keine Broschüren über Preßfreiheit verfaßt, doch in <hi rend="italic" xml:id="tg266.3.8.2">seiner Art</hi> das gleiche mit den Edelsten seiner Zeit angestrebt. Auch der beste Staat hat für den Einzelmenschen erdrückende Institutionen, und solange <pb n="31" xml:id="tg266.3.8.3"/>
es dichter gibt, haben sich dieselben in Opposition gestellt mit den Satzungen der Politik. Das harmloseste Lied ist, wenn man Konsequenzen daraus ziehen wollte, hochverräterisch. Eine Seite der Freiheit wird der Welt nie verloren gehen, und das ist die Seite, welche sich in den Sängern der Völker herausgebildet; die Subjektivität wird ewig Protest einlegen gegen jegliche Beengung durch die Objektivität. Mit dem ersten Dichter wurde der erste Protestant geboren; schon Homer war ein Protestant. Der Protestantismus war dem Begriffe nach längst in der Poesie vorhanden, ehe die Religion noch den glücklichen, zutreffenden Ausdruck für denselben gefunden hatte. Glücklicher Ausdruck? Ach! unsere schönsten Gedanken klingen in fremden Lauten an unser Ohr, und vielleicht nicht ohne Bedeutung ist es, daß das herrliche Wort »Demokrat« das Wort eines <hi rend="italic" xml:id="tg266.3.8.4">untergegangenen Volkes</hi> ist!</p>
                    <p rend="zenoPLm4n0" xml:id="tg266.3.9">Sonderbar, um wieder auf die Dichter zurückzukommen, ist es, daß sie gerade von Dichtern am schmerzlichsten mißverstanden werden. Hinter jeder trüben Wolke soll allerdings das Morgenrot der Versöhnung lauschen, – die trübe Wolke selbst aber darf nicht wegdisputiert werden, man muß ringen mit ihr, um durchzubrechen zum Frieden. Herr Prutz, der doch selbst Dichter ist, will in den »Halleschen Jahrbüchern« alle Poeten zum Glücke kommandieren, und kann nicht begreifen, wie es in einer so hübschen Zeit, als die unsrige ist, so viele Mißgestimmte gebe, warum nicht von jeder Leier ein Halleluja erklinge auf die göttliche Notwendigkeit, und Hymnen des Dankes erschallen an den Weltgeist für unsere artigen Zustände. Prutz will die Poesie auf das Postament der Philosophie hinauffschrauben und verlangt Dinge von den Dichtern, wodurch die Dichter eben aufhören würden, Dichter zu sein. Die Herren wollen doch sonst allem möglichen in der Welt seine Berechtigung vindizieren, warum nicht auch diesen Laut des Schmerzens, der durch alle Dichtungen der Gegenwart hindurchklingt?</p>
                    <p rend="zenoPLm4n0" xml:id="tg266.3.10">Diese Unzufriedenheit, dieses Mißbehagen schützt uns vor der Verknöcherung unserer jetzigen Lage und trägt sein gutes Teil dazu bei, die Weltgeschichte im Fluß zu erhalten. Ich beklage einen Dichter, ich beklage ein Volk, das zufrieden ist.</p>
                    <p rend="zenoPLm4n0" xml:id="tg266.3.11">Die Nation urteilt immer gerechter, als die Kritik, und keine hochtrabende Phrase ist imstande, sie Männern abspenstig zu machen, die sie einmal für ihre Lieblinge erklärt hat; sie läßt eher zehn Philosophen, als <hi rend="italic" xml:id="tg266.3.11.1">einen</hi> Dichter untergehen, und wird sich die Liebe zu ihrem <hi rend="italic" xml:id="tg266.3.11.2">Nikolaus Lenau</hi>, zu ihrem <hi rend="italic" xml:id="tg266.3.11.3">Anastasius Grün</hi>, zu ihrem <hi rend="italic" xml:id="tg266.3.11.4">Adelbert Chamisso</hi>, selbst zu <hi rend="italic" xml:id="tg266.3.11.5">Heine</hi>, wo er<pb n="32" xml:id="tg266.3.11.6"/>
 seine Unarten nicht zur Schau trägt, nie rauben lassen. Sie wird es mit Gleichgültigkeit anhören, wenn man ihr vorsagt: dieser Reim ist unecht, dieses Bild ist zu weit getrieben, diese Farbe zu stark aufgetragen; sie wird ewig glühend den Sänger ans Herz drücken, der ihren liebsten Regungen Sprache verliehen, ihrem Kummer Worte gegeben, ihr Elend in Harmonien gebracht, der ein Spiegel ist, in dem sie sich selbst anschaut.</p>
                    <p rend="zenoPLm4n0" xml:id="tg266.3.12">Zuweilen begegnet es, daß mutwillige Kritiker ein Vorurteil erwecken gegen einen Schriftsteller, das erst viele Jahre nachher mit Mühe und Not ausgerottet werden kann. Es ist dies der Fall mit <hi rend="italic" xml:id="tg266.3.12.1">August Platen,</hi> dessen Zukunft man seit seiner »<hi rend="italic" xml:id="tg266.3.12.2">Verhängnisvollen Gabel</hi>« und dem »<hi rend="italic" xml:id="tg266.3.12.3">Romantischen Ödipus</hi>« schon zum voraus beiseite gelegt hatte. Die Masse des Volks hat man ihm abwendig gemacht, und meine Aufgabe soll sein, von Zeit zu Zeit nachzuweisen, welche mit unserm ganzen Wesen und Treiben verwandte Töne in seinen Liedern zu vernehmen sind, wenn man das richtige Gehör besitzt, wie wir, trotz seiner oft schroffen Eigentümlichkeit, uns selbst ganz wiederhaben in seinen Dichtungen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg266.3.13">Ich will seine Werke nach der Anordnung der Cottaschen Gesamtausgabe gewissenhaft besprechen und erst am Schlusse mir ein definitives Urteil erlauben; meine Leser werden mir, denke ich, mit Liebe folgen, wo es gilt, ein so reiches, inneres Leben, nachdem es für die Wirklichkeit erloschen, aus schriftlichen Zeugnissen wieder herzustellen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg266.3.14">Der erste Abschnitt in der angeführten Ausgabe sind die <hi rend="italic" xml:id="tg266.3.14.1">Lieder</hi> und <hi rend="italic" xml:id="tg266.3.14.2">Romanzen</hi> des verstorbenen Sängers. Ich muß bei meiner Entwicklung dieses poetischen Charakters leider mehr negativ zu Werke gehen, denn ich habe mehr Vorwürfe zu widerlegen und deren Richtigkeit zu beweisen, als Lobsprüche zu bekräftigen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg266.3.15">Ein Gefühl, das durch diese ganze Abteilung durchgeht, ist das Gefühl der <hi rend="italic" xml:id="tg266.3.15.1">Verwaisung, der Einsamkeit, der unglücklichen Stellung des Dichters</hi> in der <hi rend="italic" xml:id="tg266.3.15.2">modernen Welt</hi>.</p>
                    <p rend="zenoPLm4n0" xml:id="tg266.3.16">Schon im Jahre 1815, mitten im Lärm der Waffen und des Krieges, singt der neunzehnjährige Jüngling:</p>
                    <lb xml:id="tg266.3.17"/>
                    <lg>
                        <l xml:id="tg266.3.18">Welle lispelt mit der Welle,</l>
                        <l xml:id="tg266.3.19">Aber ach, ich bin allein. (S. 4.)</l>
                    </lg>
                    <lg>
                        <lb xml:id="tg266.3.20"/>
                        <l xml:id="tg266.3.21">und im Jahre 1818:</l>
                    </lg>
                    <lg>
                        <lb xml:id="tg266.3.22"/>
                        <l xml:id="tg266.3.23">Bist du nicht gewohnt vor allen,</l>
                        <l xml:id="tg266.3.24">Als der Einsamkeit Geweihter,</l>
                        <l xml:id="tg266.3.25">Ohne Fußpfad und Begleiter</l>
                        <l xml:id="tg266.3.26">Durch den stillen Forst zu wallen? (S. 8)</l>
                    </lg>
                    <lb xml:id="tg266.3.27"/>
                    <p xml:id="tg266.3.28">
                        <pb n="33" xml:id="tg266.3.28.1"/>
Diese Absonderung von der Gewöhnlichkeit charakterisiert ihn bereits als jenen »<hi rend="italic" xml:id="tg266.3.28.2">Mann der Träume</hi>«, wie er in einem andern Liede sich selbst nennt. In solcher Einsamkeit stürmen nun alle Fragen, alle Zweifel auf ihn ein, die den echten Menschen, und namentlich den echten Dichter, Zeit seines Lebens quälen und verfolgen müssen. Er wird zuweilen irre an seinem Beruf, an seinem Talent, an seinem Schicksal. Was soll er? Was kann er? Was muß er? Ist am Ende ein Leichentuch so vieler Arbeit wert? (S. 8.) Bietet die Welt Ersatz für solche Opfer? Was hilft es, sich so abzumühen einem unerbittlichen Fatum gegenüber, das den Kainstempel auf meine Stirne gedrückt?</p>
                    <lb xml:id="tg266.3.29"/>
                    <lg>
                        <l xml:id="tg266.3.30">Ich bin geboren, zu entsagen,</l>
                        <l xml:id="tg266.3.31">Zum Glücke bin ich nicht gezeugt! (S. 10.)</l>
                    </lg>
                    <lg>
                        <lb xml:id="tg266.3.32"/>
                        <l xml:id="tg266.3.33">Wer denkt hier nicht an das Waiblingersche:</l>
                    </lg>
                    <lg>
                        <lb xml:id="tg266.3.34"/>
                        <l xml:id="tg266.3.35">Mein Kind, ich bin auf Erden</l>
                        <l xml:id="tg266.3.36">Ein ungebetner Gast!</l>
                    </lg>
                    <lg>
                        <lb xml:id="tg266.3.37"/>
                        <l xml:id="tg266.3.38">Soll er Trost suchen bei den Menschen? Aber ach, es kennt ihn ja keiner, und</l>
                    </lg>
                    <lg>
                        <lb xml:id="tg266.3.39"/>
                        <l xml:id="tg266.3.40">Keiner fragt ihn liebentglüht:</l>
                        <l xml:id="tg266.3.41">Was ist die Wange dir verblüht?</l>
                    </lg>
                    <lb xml:id="tg266.3.42"/>
                    <p xml:id="tg266.3.43">Es versteht mich niemand und jeder will mich mit seinem Maße messen (S. 27.). Daher ist es wohl am besten, wenn ich mich frei bewahre und verberge vor der ganzen Welt (S. 26.), oder wenn ich fortziehe in die Weite, wo das Herz von keinem Zwange entstellt wird (S. 26.).</p>
                    <p rend="zenoPLm4n0" xml:id="tg266.3.44">Wir sehen in den angeführten Liedern überall die ringende Natur, deren übervollem Herzen die arme Wirklichkeit nicht zu entsprechen vermag, mit einem Worte den Dichter. Ja, der Gott beseelt ihn und hat ihn zu seinem Priester geweiht! (S. 1.)</p>
                    <p rend="zenoPLm4n0" xml:id="tg266.3.45">Daß <hi rend="italic" xml:id="tg266.3.45.1">Platen</hi> die Freiheit geliebt, geliebt wie die Wackersten unsers Volkes, habe ich in meinem ersten Artikel zur Genüge dargetan. Wer daran zweifeln wollte, lese nur die Einladung in die Schweiz an einen Freund (S. 4.):</p>
                    <lb xml:id="tg266.3.46"/>
                    <lg>
                        <l xml:id="tg266.3.47">Willst du nach der Freiheit Eden,</l>
                        <l xml:id="tg266.3.48">Wo die Berge zeugend reden,</l>
                        <l xml:id="tg266.3.49">Nicht ein frommer Pilger gehn!</l>
                        <l xml:id="tg266.3.50">Dort, wo keine Dränger hausen,</l>
                        <l xml:id="tg266.3.51">Wo die Ströme freier brausen,</l>
                        <l xml:id="tg266.3.52">Wo die Lüfte reiner wehn!</l>
                    </lg>
                    <lb xml:id="tg266.3.53"/>
                    <p xml:id="tg266.3.54">
                        <pb n="34" xml:id="tg266.3.54.1"/>
Ein Vorwurf, der namentlich durch ein paar Worte<hi rend="italic" xml:id="tg266.3.54.2"> Goethes</hi> in den Gesprächen mit <hi rend="italic" xml:id="tg266.3.54.3">Eckermann</hi> aufgekommen ist, ist die Beschuldigung, <hi rend="italic" xml:id="tg266.3.54.4">Platen sei die Liebe</hi> fremd gewesen. Seine Lieder und Romanzen zu kennen und dies zu behaupten, ist meinem armen Verstande unbegreiflich. Hier könnte ich nach Herzenslust zitieren, wie <hi rend="italic" xml:id="tg266.3.54.5">Wolfgang Menzel</hi>; ich will mich in dessen damit begnügen, auf die Gedichte S. 28 und den Schluß der allerdings nicht in einem fort, sondern resigniert auch zuweilen und bescheidet sich. Denn jeder Mensch hat etwas, was ihm mißbehagt, und es rechte</p>
                    <lb xml:id="tg266.3.55"/>
                    <lg>
                        <l xml:id="tg266.3.56">– – – – Keiner mit den Sternen,</l>
                        <l xml:id="tg266.3.57">Denn jeder muß entsagen lernen,</l>
                        <l xml:id="tg266.3.58">Bis er dem Leben selbst entsagt.</l>
                    </lg>
                    <lb xml:id="tg266.3.59"/>
                    <p xml:id="tg266.3.60">Eine auffallende Bemerkung, die ich zum Erstaunen mancher heute noch mitteilen will, habe ich beim wiederholten Lesen der Lieder und Romanzen abermals gemacht. <hi rend="italic" xml:id="tg266.3.60.1">Heine</hi> und <hi rend="italic" xml:id="tg266.3.60.2">Platen</hi> sind in ihren lyrischen Gedichten einander verwandter, als man gewöhnlich zugeben will. Beide lieben die Pointen, die epigrammischen scharf zugespitzten Schüsse. Man vergleiche S. 7 das letzte Gedicht der ersten Spalte, sowie das dritte Gedicht der zweiten Spalte; ferner S. 11, zweites Lied der zweiten Spalte, S. 15, erstes Lied der zweiten Spalte. Schon in einem von Platens frühesten Gedichten, im »letzten Gast« (S. 1.) zeigt sich dieser verwandte Zug; die Verwandtschaft dehnt sich hier sogar auf den Inhalt aus. Bei <hi rend="italic" xml:id="tg266.3.60.3">Platen</hi> heißt es:</p>
                    <lb xml:id="tg266.3.61"/>
                    <lg>
                        <l xml:id="tg266.3.62">Sagt eurem Herrn, der frölich praßt,</l>
                        <l xml:id="tg266.3.63">Daß er den Reigen meide;</l>
                        <l xml:id="tg266.3.64">Denn unten warte noch ein Gast,</l>
                        <l xml:id="tg266.3.65">Den Degen aus der Scheide.</l>
                    </lg>
                    <lg>
                        <lb xml:id="tg266.3.66"/>
                        <l xml:id="tg266.3.67">Und der Schluß eines <hi rend="italic" xml:id="tg266.3.67.1">Heine</hi>schen Liedes lautet:</l>
                    </lg>
                    <lg>
                        <lb xml:id="tg266.3.68"/>
                        <l xml:id="tg266.3.69">O daß ich ihn nur fände,</l>
                        <l xml:id="tg266.3.70">So ganz allein im grünen Wald,</l>
                        <l xml:id="tg266.3.71">Sein Glück hätt' bald ein Ende.</l>
                    </lg>
                    <lb xml:id="tg266.3.72"/>
                    <pb n="35" xml:id="tg266.3.73"/>
                </div>
            </div>
        </body>
    </text>
</TEI>