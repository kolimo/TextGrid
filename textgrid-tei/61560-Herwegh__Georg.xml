<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg279" n="/Literatur/M/Herwegh, Georg/Schriften/Die deutschen Professoren">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Die deutschen Professoren</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Herwegh, Georg: Element 00299 [2011/07/11 at 20:28:51]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <notesStmt>
                <note>
                              Erstdruck in: Deutsche Volkshalle (Bellevue), 1840.
                           </note>
            </notesStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Herweghs Werke in drei Teilen. Herausgegeben mit Einleitungen und Anmerkungen versehen von Hermann Tardel, Berlin, Leipzig, Wien, Stuttgart: Bong &amp; Co., [1909].</title>
                        <author key="pnd:118550128">Herwegh, Georg</author>
                    </titleStmt>
                    <extent>141-</extent>
                    <publicationStmt>
                        <date when="1909"/>
                        <pubPlace>Stuttgart</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1817" notAfter="1875"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg279.3">
                <head type="h3" xml:id="tg279.3.1">
                    <pb n="141" xml:id="tg279.3.1.1"/>
                    <pb type="start" n="158" xml:id="tg279.3.1.2"/>Georg Herwegh</head>
                <head type="h2" xml:id="tg279.3.2">Die deutschen Professoren</head>
                <div type="h4">
                    <head type="h4" xml:id="tg279.3.3">Eine zoologische Abhandlung</head>
                    <milestone unit="head_start"/>
                    <p rend="zenoPLm16n16" xml:id="tg279.3.4">
                        <seg rend="zenoTXFontsize80" xml:id="tg279.3.4.1">Ja, ihr seid die Leute, mit euch wird die Weisheit sterben.</seg>
                    </p>
                    <milestone unit="head_end"/>
                    <milestone unit="head_start"/>
                    <p rend="zenoPR" xml:id="tg279.3.5">
                        <seg rend="zenoTXFontsize80" xml:id="tg279.3.5.1">
                            <hi rend="italic" xml:id="tg279.3.5.1.1">Hiob 12,2.</hi>
                        </seg>
                    </p>
                    <milestone unit="head_end"/>
                    <lb xml:id="tg279.3.6"/>
                    <p xml:id="tg279.3.7">Eine zoologische Abhandlung; ich werde sie anders benennen, sobald man mir beweist, daß ein Professor dem Staate je einen <hi rend="italic" xml:id="tg279.3.7.1">Menschen</hi> erzogen hat. Ausgenommen sind die Herren Professoren <hi rend="italic" xml:id="tg279.3.7.2">Schelling, Schiller, Fichte, Hegel</hi>, überhaupt die jungen und alten Zelebritäten unserer Nation, die das Unglück hatten, diesen traurigen Namen als Aushängeschild gebrauchen zu müssen. Es ist das schöne Vorrecht unseres Jahrhunderts, daß es eine Wahrheit nur dann als Wahrheit anzuerkennen hat, wenn sie aus dem Munde eines Patentierten, eines Angestellten kommt. Glaube, Liebe und Hoffnung sind offiziell geworden, und Gott selbst existiert nur, so lange nicht die Menschheit, sondern ein Professor es behauptet. Mein Charakter als Bürger, als vernünftiger Mann berechtigen mich heutzutage nicht mehr, ohne Hindernisse zu meiner Nation zu reden. Will ich mir einen Einfluß nicht nur auf die guten, sondern auch auf die bösen Geister erobern, so muß ich mich zur Annahme irgendeines Titels oder Ranges bequemen; ich muß einen Laufpaß vom Staate haben, wenn die liebe Jugend, die eine Karriere zu machen gedenkt, mir zuhorchen soll.</p>
                    <p rend="zenoPLm4n0" xml:id="tg279.3.8">Von zehn Untugenden, die ich besitze, habe ich immer neun einem Professor zu danken. Wenn ich trotz meinen hochverehrten Lehrern ein Mensch geworden bin, so preise ich dafür meinen Genius, der sorgsam über die ihm anvertraute Seele gewacht hat. Mein Feind wird es mir nicht nachsagen können, daß ich einem Professor eine Schuld abzutragen hätte. Ich bin heute auf hundert Sachen stolz, für die ich in der Schule Schläge, auf höheren Anstalten Verweise bekommen habe. Der unvertilgbare Spott der deutschen Jugend, den sie über ihre Lehrer, allerdings oft recht unhöflich ausgießt, ist wahr, unendlich wahr. Von dreißig Schülern stehen in der Regel zwanzig moralisch hoch über ihrem Professor.</p>
                    <p rend="zenoPLm4n0" xml:id="tg279.3.9">Sie besitzen noch, was der letztere vergeudet und verloren hat, die poetische Mitgift des Lebens ganz und ungeschmälert. Sie haben nicht den Fonds von Kenntnissen, wie er – sehr richtig, so unbedeutend diese oft bei den Lehrern sind; sie haben nicht seine Erfahrungen, – sie mögen sich glücklich schätzen; aber sie haben noch Blut im Herzen statt griechischer Partikeln, und sind noch naiv genug, bei sich anzufragen, was es sie eigentlich <pb n="158" xml:id="tg279.3.9.1"/>
interessieren könne, ob ut den Indikativ oder Konjunktiv regiere.</p>
                    <p rend="zenoPLm4n0" xml:id="tg279.3.10">Warum sie die blühende Gegenwart aufgeben sollen, um in eine verwitterte Vergangenheit sich zurückzuversetzen? Warum man ihnen Luft und Sonne stehle, um sie auf die staubigen Bänke der Schule oder des Kollegiums zu bannen? Daß sie es den Nachgebornen einst wieder so machen können? Daß sie ewig nur ein Rad im Kreise drehen? Ist es der Mühe wert, so viel schöne Jahre zu verschleudern, um es endlich nicht weiter zu bringen, als der Herr, der vom Katheder herunter die unfruchtbare Weisheit doziert? Alle Erziehung soll nur darauf hinauslaufen, den Menschen zu einem <hi rend="italic" xml:id="tg279.3.10.1">freien Mann</hi> zu bilden, oder vielmehr, da der Mensch so lange frei ist, bis er einem deutschen Professor unter die Hände gerät, die angeborne Freiheit zu erhalten, zu entwickeln, ihr Inhalt und Fülle zu geben. Nicht, daß ich mein Brot erwerbe, nicht, daß ich Jurist, nicht, daß ich Theolog, nicht, daß ich Mediziner werde, ist es zunächst, warum ich lerne, warum ich mir Kenntnisse sammle; ich lerne, ich sammle mir Kenntnisse zunächst, um durch diese Bereicherung meines Geistes mich freier und unabhängiger von den Zufälligkeiten des Lebens zu machen. Der Jüngling denkt früher an das Ideal, als an das Brot; der Professor, wie er sein soll, meistens nur noch an das letztere. Er ist der treugehorsame Diener des Staats, seine erste Pflicht, dem Staat ebenso treue, gehorsame Diener herauszubilden. Welches bessere Mittel findet er zu Erfüllung dieser seiner Obliegenheit, als seine Untertanen, die Schüler, recht bald fühlen zu lassen, daß sie zunächst seine, und so gradatim immer wieder die Sklaven eines Höheren sind bis in das religiöse Gebiet, da auch in diesem Gott stets als ein kleiner Tyrann geschildert wird. Das Altertum ist dem Professor nur vorhanden, um ihm Gelegenheit zu geben, den Kram von Notizen, die er durch Sitzfleisch sich angeeignet, vor den erstaunten Zöglingen recht prunkend auszubreiten; die Schlacht von Marathon findet er hübsch, weil er dabei eine geographische Bemerkung machen kann. Die Reden des Demosthenes patriotisch, weil sie im reinsten attischen Dialekte geschrieben sind. Am lustigsten benehmen sich diese Pygmäen den Männern der Geschichte gegenüber. Für den Kammerdiener gibt es keinen großen Mann. Da ist kein Held, an dem sie nichts auszusetzen wissen, und jedes Phantom von einem Professor wird die geistreiche Phrase anbringen: »wäre <hi rend="italic" xml:id="tg279.3.10.2">Hannibal</hi> nach der Schlacht bei Cannä nur gegen Rom aufgebrochen!« Kleiner Hannibal! Großer Professor!</p>
                    <p rend="zenoPLm4n0" xml:id="tg279.3.11">
                        <hi rend="italic" xml:id="tg279.3.11.1">
                            <pb n="159" xml:id="tg279.3.11.1.1"/>
Heinrich Heine</hi> hat diese Weltverbesserer himmlisch gezeichnet in dem Verse:</p>
                    <lb xml:id="tg279.3.12"/>
                    <lg>
                        <l xml:id="tg279.3.13">Zu fragmentisch ist Welt und Leben,</l>
                        <l xml:id="tg279.3.14">Ich will mich zum deutschen Professor begeben,</l>
                        <l xml:id="tg279.3.15">Der weiß das Leben zusammenzusetzen,</l>
                        <l xml:id="tg279.3.16">Und er macht ein verständlich System daraus;</l>
                        <l xml:id="tg279.3.17">Mit seinen Nachtmützen und Schlafrockfetzen</l>
                        <l xml:id="tg279.3.18">Stopft er die Lücken des Weltenbaus.</l>
                    </lg>
                    <lb xml:id="tg279.3.19"/>
                    <p xml:id="tg279.3.20">Die Däumlingsnatur, wie sie sich spreizt und wichtig tut, kann wahrhaftig nicht besser charakterisiert werden. Ja, so sind die Leute, welche das Elend Deutschlands immer größer füttern! – Die Eitelkeit eines Professors ist leider nicht so unschuldig, wie die eines Frauenzimmers, sie ist herrisch, eigensinnig, tyrannisch; sie möchte alles nach sich ummodeln, alles in das Prokrustesbett ihrer jeweiligen, meist ärmlichen Begriffe spannen. Wie manches Talent ist durch die Schuld dieser Herren schon untergegangen! Wie mancher Keim ward durch ihre sublime Torheit schon erstickt! Ein Professor muß ein Steckenpferd haben, und wehe dem, der es nicht mit ihm reitet! Der Professor ist ein Phlegma, und wehe dem, der es nicht mit ihm ist!</p>
                    <p rend="zenoPLm4n0" xml:id="tg279.3.21">Ein junger Mann ist warm und vollblütig, er liebt, das Leben im Prisma der Poesie anzuschauen; zufällig hat er einen Professor der Mathematik, dem seine Erziehung anvertraut ward; er muß ein Stümper in der Mathematik werden, statt daß er es, seiner Anlage nach, vielleicht zum Meister in der Poesie gebracht hätte. Das Talent, Talente zu entdecken, geht einem Professor in der Regel ab. Seine Rute ist meistens eine Birken-, selten eine Wünschelrute. Unsere Jugend wird systematisch zur Lüge erzogen, indem sie das Unglück hat, Köpfen unter die Hände zu fallen, die alles aus ihr machen, nur nicht, zu was sie von Gottes Gnaden berufen ist.</p>
                    <p rend="zenoPLm4n0" xml:id="tg279.3.22">Haß gegen jede schönere, freiere Lebensnatur ist die Mitgift einer echten professorischen Natur. Ich kenne einen Lehrer, der es mir heute noch nicht verzeiht, daß ich in einem Kollegium über Geschichte als den passendsten Kommentar dazu Börnes Briefe aus Paris unter dem Tisch gelesen. Wenn er vollends gewußt hätte, daß die Reden, die beim Hambacher Feste gehalten wurden, in meinem Pulte gewesen wären! Ich schlechter Mensch!</p>
                    <p rend="zenoPLm4n0" xml:id="tg279.3.23">Ein Professor ist ein Allerweltsmann. Er liest mit dem einen Auge den Homer, mit dem andern das Basler Missionsblatt. Unvergeßlicher Mann mit der flanellenen Halsbinde, der du mir einst die Tränen des Achilleus kommentiert!</p>
                    <p rend="zenoPLm4n0" xml:id="tg279.3.24">
                        <pb n="160" xml:id="tg279.3.24.1"/>
Derselbe Pietist erklärte uns den <hi rend="italic" xml:id="tg279.3.24.2">Sophokles</hi>. Durch ihn wäre ich nie zu einer Einsicht in die Ökonomie des griechischen Drama gelangt; ich hätte von Sophokles nicht mehr erfahren, als von Livius und Tacitus, von denen ich lange Zeit nur wußte, daß jener mit einem halben, dieser mit einem ganzen Hexameter anfange.</p>
                    <p rend="zenoPLm4n0" xml:id="tg279.3.25">Ich war gewohnt, bei dem nächtlichen Religionsunterricht mein Licht immer fünf Minuten früher auszulöschen, als mein begeisterter Lehrer das seinige, und so wurde ich bald als ein arger Zweifler bekannt. »Wie steht es mit Ihrem Herzen?« lautete die honigsüße Frage bei der monatlichen Revue. Wie steht es mit Ihrem Herzen? d.h. im pietistischen Jargon: Sind Sie orthodox oder sind Sie vernünftig? O, Deutschland hat noch seine Originale!</p>
                    <p rend="zenoPLm4n0" xml:id="tg279.3.26">Mein Humor verläßt mich, wenn ich an den letzten Teil meiner Abhandlung denke. Zorn, frommer Zorn führt meine Feder. <hi rend="italic" xml:id="tg279.3.26.1">Ein deutscher Professor ist geschworner Feind aller Politik</hi>. Er fand das Bestehende vernünftig, noch ehe Schelling und Hegel geboren waren. Untertänigkeit, Kriecherei, Speichelleckerei – ein Wörterbuch, ein Königreich um ein Wörterbuch, in dem das richtige Prädikat steht! Ich hasse jeden Kultus, zu welchem der Schneider am meisten beiträgt; so habe ich mich denn aus Eigensinn in meiner Jugend nie schwarz getragen. Da wurde eines Tages eine allerhöchste Person erwartet. Ich hatte ein graues Röckchen an, mein Professor verzweifelte. Ich tröstete mich mit Napoleon; die allerhöchste Person kam nicht. Wie glücklich war der gute Mann!</p>
                    <p rend="zenoPLm4n0" xml:id="tg279.3.27">Ich hätte für Polen kein Gefühl, für die Edelsten und Unglücklichsten meines Vaterlandes keine Tränen haben dürfen, hätte ich vorher die Erlaubnis eines deutschen Professors nachsuchen wollen. Bete, arbeite und krieche – – es leben die deutschen Professoren!</p>
                </div>
            </div>
        </body>
    </text>
</TEI>