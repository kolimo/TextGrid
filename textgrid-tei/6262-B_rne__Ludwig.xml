<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg27" n="/Literatur/M/Börne, Ludwig/Schriften/Literaturkritiken/Die Serapions-Brüder">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Die Serapions-Brüder</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Börne, Ludwig: Element 00031 [2011/07/11 at 20:27:42]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <notesStmt>
                <note>
                                 Entstanden 1820. Erstdruck in: Gesammelte Schriften, Hamburg (Hoffmann und Campe) 1828/1832.
                              </note>
            </notesStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Ludwig Börne: Sämtliche Schriften. Neu bearbeitet und herausgegeben von Inge und Peter Rippmann, Band 1–3, Düsseldorf: Melzer-Verlag, 1964.</title>
                        <author key="pnd:118512749">Börne, Ludwig</author>
                    </titleStmt>
                    <extent>555-</extent>
                    <publicationStmt>
                        <date when="1964"/>
                        <pubPlace>Düsseldorf</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date when="1820"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg27.3">
                <div type="h4">
                    <head type="h4" xml:id="tg27.3.1">Die Serapions-Brüder</head>
                    <head type="h4" xml:id="tg27.3.2">Gesammelte Erzählungen und Märchen.</head>
                    <milestone unit="head_start"/>
                    <p rend="zenoPC" xml:id="tg27.3.3">Herausgegeben von E.T.A. Hoffmann.</p>
                    <p rend="zenoPC" xml:id="tg27.3.4">Erster und zweiter Band. Berlin, 1819.</p>
                    <milestone unit="head_end"/>
                    <lb xml:id="tg27.3.5"/>
                    <p xml:id="tg27.3.6">Aus dem Meere der deutschen Leihbibliothek (nur das Salz und die Tiefe unterscheidet jenes von diesen) ragen <pb n="555" xml:id="tg27.3.6.1"/>
die Schriften Hoffmanns als tröstende, liebliche Eilande hervor. Jauchzend springen wir ans Ufer, küssen den grünenden Boden, umarmen Baum und Strauch und sind beglückt, uns aus der Wassernot gerettet zu sehen. Aber wie die Gefahr des Lebens zurückgetreten, stellen sich seine Bedürfnisse ein: der Hunger und der Durst; doch da rieselt keine Quelle, und so schöne Früchte uns auch locken, sie sind uns fremd, wir wagen die giftdrohenden nicht zu berühren. Wir dringen tiefer ins Land, da kommen von allen Seiten mit gräßlichem Geheule die wilden Bewohner, mit Pfeilen und Wurfspießen bewaffnet, auf uns zu. Überreste verzehrter Menschenopfer erfüllen uns mit Schauer. Wir fliehen entsetzt an den Strand zurück und vertrauen uns der greulichen Wasserwüste von neuem an.</p>
                    <p rend="zenoPLm4n0" xml:id="tg27.3.7">Unsere Furcht vor dem nassen Tode wird wohl verziehen, denn sie wird geteilt und unsere Freude an dem grünen Lande daher mitempfunden. Aber daß wir dieses so schnell verließen, daß wir vor den ungewöhnlichen Tönen der Wilden, die uns vielleicht freundschaftlich begrüßten, erbebten, daß wir die schönen Früchte nicht zu pflücken wagten, die vielleicht wohlschmeckend und nahrhaft waren, daß die Knochenreste, wahrscheinlich natürlich verstorbener Menschen, uns entsetzten – das bedarf einer Rechtfertigung. Sie ist schwer, verdrießlich. Denn, wie es unbequem ist, Menschen, die man nicht liebt, achten zu müssen, und schmerzlich, sie nicht lieben zu können, wenn man sie achtet – so ist es auch mit ihren Werken. Aber, wer ist Preisrichter über diese Werke? Das Herz oder der Kopf? Der Geist erkennt den Preis, das Herz überreicht ihn, oder – hält ihn auch zurück, wenn es mit dem Ausspruche nicht zufrieden ist.</p>
                    <p rend="zenoPLm4n0" xml:id="tg27.3.8">Mag der richtende Verstand diese gesammelten Erzählungen für preiswürdig erklären, die Empfindung schweigt gewiß, wenn sie nicht gar murrt gegen den Ausspruch.</p>
                    <p rend="zenoPLm4n0" xml:id="tg27.3.9">
                        <pb n="556" xml:id="tg27.3.9.1"/>
Aus verschiedenen Zeiten und Orten, wo die Erzählungen und Märchen zerstreut und einzeln erschienen, hat sie der Verfasser gesammelt und vereinigt. Daher wird es zum Gegenstande der Beurteilung, nicht bloß<hi rend="italic" xml:id="tg27.3.9.2"> wie,</hi> sondern auch, <hi rend="italic" xml:id="tg27.3.9.3">daß</hi> sie zusammengestellt worden. Denn oft geschieht, daß wir von der flüchtigen Stunde ertragen, was uns unerträglich wird, wenn Stunde an Stunde sich zum Tage reiht; daß ein kindisches oder verwegenes Spiel, eine trübe oder leidenschaftliche Laune uns reizt und ergötzt, dagegen uns schmerzlich berührt, wenn jenes Spiel, durch häufige Wiederholung, sich als Ernst, und jene Laune, durch ihre Dauer, sich als Gemütsart darstellt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg27.3.10">Einige Freunde verabreden sich, an bestimmten Tagen zusammenzukommen, um sich die Schöpfungen ihres Geistes und wechselseitig ihr Urteil darüber mitzuteilen. Sie nennen sich <hi rend="italic" xml:id="tg27.3.10.1">Serapions-Brüder,</hi> nicht darum bloß, weil sie am Kalendertage des Märtyrers Serapion sich zum ersten Male vereinigt hatten, sondern auch, weil sie im Geiste jenes Heiligen dichten und trachten wollten. Der heilige Serapion hatte, wie die Legende lehrt, unter dem Kaiser Decius den grausamsten Märtyrertod erlitten. Man trennte die Junkturen der Glieder und stürzte ihn dann vom hohen Felsen herab. Das ist aber keineswegs das hohe Ziel, das sich die Berliner Serapions-Brüder vorgesetzt; sie sitzen vielmehr bei Sala Tarone unter den Linden und trinken italienische Weine, auch wohl kalten Punsch, leben also gar nicht wie die Anachoreten. Sie haben nur in <hi rend="italic" xml:id="tg27.3.10.2">dem</hi> Sinne jenen Heiligen Schutzpatron ihres Klubs und seine Regel zu der ihrigen gemacht, als sie ihre poetische Dichtungen in dem Geiste eines gewissen verrückten Grafen schaffen wollten, der sich für den Märtyrer Serapion hielt und einsiedlerisch lebte. Mit der Geschichte dieses Wahnsinnigen beginnt das Buch. Einer der Freunde erzählt sie. Auf seinen Reisen <pb n="557" xml:id="tg27.3.10.3"/>
habe er von dem Grafen gehört und ihn in dem Walde, wo er sich angesiedelt, aufgesucht. Darauf habe er sich in ein Gespräch mit ihm eingelassen und ihn nach den Grundsätzen des Pinels und Reils von seiner fixen Idee heilen wollen, sei aber ganz beschämt abgeführt worden. Denn der Graf habe ihm bewiesen, wie er, der psychologische Experimentator, eigentlich verrückt sei, indem er nicht begreifen wolle, daß sie sich in der thebaischen Wüste befänden. Darauf habe ihm der Graf mit hoher Begeisterung einige Gesichte mitgeteilt, die in Erstaunen setzten wegen der <hi rend="italic" xml:id="tg27.3.10.4">plastischen Ründung</hi> und des <hi rend="italic" xml:id="tg27.3.10.5">glühenden Lebens,</hi> mit der sie dargestellt wurden.</p>
                    <p rend="zenoPLm4n0" xml:id="tg27.3.11">Nachdem diese Erzählung geendet, läßt sich einer der versammelten Serapions-Brüder wie folgt vernehmen:</p>
                    <p rend="zenoPLm4n0" xml:id="tg27.3.12">»Ich verehre Serapions Wahnsinn deshalb, weil nur der Geist des vortrefflichsten oder vielmehr des wahren Dichters von ihm ergriffen werden kann. Woher kommt es, daß so manches Dichterwerk wirkungslos bleibt, als daher, daß der Dichter nicht das wirklich schaute, wovon er spricht? Vergebens ist das Mühen des Dichters, uns dahin zu bringen, daß wir daran glauben sollen, woran er selbst nicht glaubt, nicht glauben kann, weil er es nicht erschaute. Der Einsiedler war ein wahrhafter Dichter, er hatte das wirklich geschaut, was er verkündete, und deshalb ergriff seine Rede Herz und Gemüt.« »Dessen wollen wir eingedenk sein, so oft wir bei unseren Zusammenkünften einer dem andern nach alter Weise manches poetische Prodüktlein, das wir unter dem Herzen getragen, mitteilen werden. Jeder prüfe wohl, ob er auch wirklich das geschaut, was er zu verkünden unternommen, ehe er es wagt, laut damit zu werden. Der Einsiedler Serapion sei unser Schutzpatron, er lasse seine Sehergabe über uns walten, seiner Regel wollen wir folgen als getreue Serapions-Brüder.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg27.3.13">So durch und durch, so ganz, nicht bloß nach innen, sondern <pb n="558" xml:id="tg27.3.13.1"/>
auch an seinen Oberflächen wertlos, so ohne die geringste Beimischung von Wahrheit ist jener Lehrsatz, der von der Natur des Dichters gegeben wird, daß Täuschung und Verwechslung unmöglich ist und es nur weniger Worte bedarf, um zu zeigen, worin die Falschheit bestehe. Wie die Anbetung den Gott, so schafft erst die Bewunderung das Kunstwerk, es sei ein Gedicht, eine Bildnerei oder ein anderes. Ist es in jedem Kunstwerk die Vollkommenheit irgend eines Wesens, was jene Bewunderung erregt, so muß, daß diese erregt werden könne, jenes Wesen<hi rend="italic" xml:id="tg27.3.13.2"> faßlich</hi> sein – faßlich für den Verstand, für den Glauben oder die Phantasie. Wie aber kann ein Kunstwerk faßlich werden, wenn es der Künstler nicht freigibt, wie kann es in unsere Sinne, in unseren Geist einziehen, wenn es die Werkstätte des Künstlers nicht verläßt? Will der Dichter mit den Blumen seiner Wartung, die er in den Boden unserer Phantasie verpflanzt, auch die Blumenerde versetzen, aus der jene hervorgesprossen, will er durch seine eigene Phantasie die des Lesers verdrängen, dann weisen wir seine Gaben zurück, weil nur für das Geschenk, nicht aber für den Geber Raum haben. Nie wird der Dichter glaublich machen, was er selbst glaubt, nie anschaulich, wenn er das, was er uns zeigt, selbst gesehen. Dann wird die Dichtung zur Wahrheit, das Märchen zur Geschichte, die den Verstand befriedigt, sättigt, und alle Lust der Einbildungskraft zerstört. Dann wird das Bild zur Konterfei, mit aller Beschränkung, worin jede Wirklichkeit gefangen ist; dann wird das Kunstwerk zum Spiegelbilde des Künstlers, ein Schatten, wenn wir vorwärts, ein nüchternes Dasein aus Fleisch und Bein, wenn wir es rückwärts schauen. Es ist falsch, daß der wahre Dichter ein Seher sei. Ein Seher ist ein verzückter oder ein verrückter Geist, ein Gott, zu dem wir nicht hinaufreichen, oder ein kranker Mensch, zu dem wir nicht hinabsteigen können. <pb n="559" xml:id="tg27.3.13.3"/>
Der Dichter aber muß menschlich fühlen, um Menschen zu bewegen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg27.3.14">Daß er dieses muß, daß er nicht glauben dürfe, was er glauben, nicht sehen, was er anschaulich machen möchte, das hat der Verfasser der Serapions-Brüder unwiderleglicher, als es ein anderer vermöchte, an seinem Werke selbst gezeigt. Er <hi rend="italic" xml:id="tg27.3.14.1">hat</hi> geglaubt, er <hi rend="italic" xml:id="tg27.3.14.2">hat</hi> gesehen, darum sind es aber auch keine Dichtungen, die er uns gibt; sie sind nicht etwa mehr, nicht etwa weniger, sie sind ein anderes. Er gibt uns eine werdende, noch im Gären begriffene, oder eine untergehende Welt. Sonne, Mond und Sterne, Tag und Nacht, Wasser, Feuer, Erde und alle Elemente, die Tiere des Waldes und die Fische des Meeres und die Vögel in den Lüften, alles bewegt sich in tollem Taumel und streitet um die Herrschaft; nur der Mensch ist abwesend. Aber es ist nicht etwa der heitere Mutwille, der mit Freiheit und Ergötzen alles untereinander wirft, es ist der vom Hexentrank berauschte Blocksbergreiter, der treibt, weil er wird getrieben, und so findet der Leser an der Besonnenheit des Dichters keine Brustwehr, die ihn vor dem Herabstürzen sichert, wenn ihn beim Anblicken der tollen Welt unter seinen Füßen der Schwindel überfällt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg27.3.15">In allen diesen gesammelten Erzählungen und Märchen herrscht eine abwärts gekehrte Romantik, eine Sehnsucht nach einem tieferen, nach einem unterirdischen Leben, die den Leser anfröstelt und verdrießlich macht. Es ist Phantasie darin, aber ohne den regelnden Verstand. Es ist Phantasie darin, aber nicht die hellaufflammende, schaffende, sondern eine rotglühende, zersetzende Phantasie. Wer auf Marionettenbühnen jene tanzenden Figuren gesehen hat, die Hände und Arme, dann Füße und Schenkel, endlich den Kopf wegschleudern, bis sie zuletzt als greuliche Rumpfe umherspringen, der hat die Gestalten der Hoffmannschen Erzählungen <pb n="560" xml:id="tg27.3.15.1"/>
gesehen, nur daß diese von allen Gliedern den Kopf zuerst verlieren. Man hört nicht die Aussprüche eines verzückten, begeisterten, man vernimmt nur die erzwungenen Geständnisse eines auf die Folter gespannten Gemüts. Es ist kein Tagesstrahl in den Gemälden, alles Licht kommt nur von Irrwischen, Blitzen und Feuersbrünsten. Man hört in dieser öden, herbstlichen, welken Natur keinen Ton eines frischen, gesunden, lebenskräftigen Wesens, man hört nur das Gewinsel der Kranken und Sterbenden und das Geschrei der Eulen, die um Äser schwirren. Selbst die Musik, die in allen Werken des Verfassers wiederklingt, sie dient nicht dazu, den Himmel, dessen Dolmetscherin sie ist, auf die Erde herabzuziehen und ihr verständlich zu machen, sie wird nur gebraucht, um höhnend den unermeßlichen Abstand zwischen Himmel und Erde zu beweisen, zu zeigen, daß jene Höhe von sehnsuchtsvollen Menschen nie erreicht werden könne, und ihnen <hi rend="italic" xml:id="tg27.3.15.2">»das Mißverhältnis des innern Gemüts mit dem äußern Leben</hi>« genau vorzurechnen, damit sie ja nicht der Verzweiflung entgehen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg27.3.16">In den Worten, die der Verfasser einem der Serapions-Brüder sagen läßt: »ich tadle, o Cyprian, deinen närrischen Hang zur Narrheit, deine wahnsinnige Lust am Wahnsinn. Es liegt etwas Überspanntes darin, das dir selbst mit der Zeit wohl lästig werden wird«, hat der Verfasser das Urteil gegen sich selbst gesprochen, und noch ein schonendes, denn beharrlich hat er durch alle seine Werke gezeigt, daß ihm jener Hang noch immer nicht lästig geworden ist. Eine Reihe heiterer Gemälde mag hier und dort, von einem schauerlichen Nachtstücke unterbrochen, noch genußbringender werden. Nur dürfen nicht alle Wände damit behängt sein, nur muß ein Sternenschein die Nacht sichtbar machen, daß sie nicht zum unergründlichen dunkeln Nichts werde. Der Schrecken <pb n="561" xml:id="tg27.3.16.1"/>
muß in der getäuschten Einbildungskraft, nicht in der Sache selbst sein, und Maß überall. Die Ägypter würzten ihre Freudengelage durch den Anblick des Todes; der Anblick des Sterbens hätte alle Lust zernichtet.</p>
                    <p rend="zenoPLm4n0" xml:id="tg27.3.17">Ich sagte früher: die Erzählungen, die uns der Verfasser gibt, sind keine Dichtungen, <hi rend="italic" xml:id="tg27.3.17.1">sie sind ein anderes,</hi> und hier ist das kurze freundliche Abendrot des langen mürrischen Urteils. Es wird gefragt, welchen Zweck hatten diese Erzählungen? Dieses ist zwar eine sehr philistermäßige Frage, wie die Serapions-Brüder mit Recht spotten können. Denn ein Buch <hi rend="italic" xml:id="tg27.3.17.2">will</hi> nichts, es zeigt sich, es ist da. Aber fordert auch ein Buch nichts, so gewährt ihm doch der Leser etwas, und er gewährt ihm, was er glaubt, daß ihm gebühre. Den Wert eines poetischen Werkes habe ich gewagt ihm abzusprechen, aber den eines wissenschaftlichen gebe ich ihm willig. Es ist ein Lehrbuch mit den schönsten Bildnissen geziert, es ist der elegante <hi rend="italic" xml:id="tg27.3.17.3">Pinel,</hi> es ist <hi rend="italic" xml:id="tg27.3.17.4">die Epopee des Wahnsinns.</hi> Ein lobenswertes Unternehmen, wenn es lobenswert ist, den menschlichen Geist, der nachtwandelnd an allen Gefahren unbeschädigt vorübergeht, aufzuwecken, um ihn vor dem Abgrunde zu warnen, der zu seinen Füßen droht.</p>
                    <pb n="562" xml:id="tg27.3.18"/>
                    <pb n="563" xml:id="tg27.3.19"/>
                    <pb type="start" n="987" xml:id="tg27.3.20"/>
                </div>
            </div>
        </body>
    </text>
</TEI>