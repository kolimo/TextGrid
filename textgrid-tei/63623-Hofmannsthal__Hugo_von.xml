<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg373" n="/Literatur/M/Hofmannsthal, Hugo von/Essays, Reden, Vorträge/Zur Physiologie der modernen Liebe">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Zur Physiologie der modernen Liebe</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Hofmannsthal, Hugo von: Element 00210 [2011/07/11 at 20:24:30]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <notesStmt>
                <note>
                              Erstdruck in: Die Moderne (Berlin), 1. Jg., Nr. 2/3, 8.2.1891.
                           </note>
            </notesStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Hugo von Hofmannsthal: Gesammelte Werke in zehn Einzelbänden. Reden und Aufsätze 1–3. Herausgegeben von Bernd Schoeller in Beratung mit Rudolf Hirsch, Frankfurt a.M.: S. Fischer, 1979.</title>
                        <author key="pnd:118552759">Hofmannsthal, Hugo von</author>
                    </titleStmt>
                    <extent>93-</extent>
                    <publicationStmt>
                        <date when="1979"/>
                        <pubPlace>Frankfurt a.M.</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1874" notAfter="1929"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg373.3">
                <head type="h3" xml:id="tg373.3.2">Hugo von Hofmannsthal</head>
                <head type="h2" xml:id="tg373.3.3">Zur Physiologie der modernen Liebe</head>
                <milestone unit="sigel" n="Hofmannsthal-RuA Bd. 1" xml:id="tg373.3.4"/>
                <div type="h4">
                    <head type="h4" xml:id="tg373.3.5">
                        <pb type="start" n="93" xml:id="tg373.3.5.1"/>
»Physiologie de l'amour moderne.</head>
                    <head type="h4" xml:id="tg373.3.6">Fragments posthumes d'un ouvrage de Claude Larcher, recueillis et publiés par Paul Bourget, son exécuteur testamentaire.«</head>
                    <p xml:id="tg373.3.7">Paul Bourgets künstlerische Entwickelung ist kein Weitergehen von Problem zu Problem, sondern ein Tieferwerden im Erfassen eines Phänomens: des doppelten Willens im Menschen. Fassen wir jedes menschliche Wissen als Erkenntnis des Zusammenhangs der Dinge, so ist auch jeder beliebige Angriffspunkt der Analyse ein Knotenpunkt aller Fäden; man kann nicht eine Saite berühren, ohne daß alle mitklingen, jede einzelne Willensäußerung des Individuums steht in geheimnisvoll-unlöslicher Verbindung mit allen Willensäußerungen desselben. Das ist die moderne Vertiefung des alten Künstlerworts: ex ungue leonem. Die kaum merkliche gleichartige Atmosphäre, in welcher sich alle Figuren eines Romanes bewegen, die ätherfeinen geistigen Schwingungen, welche sich aus dem Auge des Schauenden, des Autors, in das Geschaute, die dargestellten Seelenzustände, hinüberziehen und die auch das vollkommenste, naturalistisch vollendetste Kunstwerk vom wirklichen Leben unterscheiden müssen, an dem wir diese Schwingungen, eben weil sie aus unserem eigenen Auge kom men, nicht wahrnehmen: das nennen wir die Seele des Buches, und diese Individualität, die des Autors, können wir auch allein daraus erkennen, die der dargestellten Personen nur insofern, als der Dichter ein mehr oder minder unwahrscheinlich losgerissenes Werk seiner Individualität in sie gelegt hat. Dies ist der Grund (neben den von Hermann Bahr in seiner Abhandlung über die »neue Psychologie«<ref type="noteAnchor" target="#tg373.3.24">
                            <anchor xml:id="tg373.3.7.1" n="/Literatur/M/Hofmannsthal, Hugo von/Essays, Reden, Vorträge/Zur Physiologie der modernen Liebe#Fußnote_1"/>
                            <ptr cRef="/Literatur/M/Hofmannsthal, Hugo von/Essays, Reden, Vorträge/Zur Physiologie der modernen Liebe#Fußnoten_1"/>
                            <hi rend="superscript" xml:id="tg373.3.7.2.1">1</hi>
                        </ref> angerührten technischen Vorteilen), weshalb die modernen, analytischen Novellisten sich mit Vorliebe in ihren Romanen der Ichform bedienen. Mit dieser einen Silbe sagen sie uns, daß wir einen handelnden, leidenden, werdenden, räsonierenden <pb n="93" xml:id="tg373.3.7.3"/>
oder verfaulenden Mikrokosmos vor uns haben, der für uns ein paar Stunden lang den Mittelpunkt des Makrokosmos vorstellt, dem zu Gefallen andere Menschen – die Statisten – nach Bedürfnis auf die Welt kommen, schwatzen, fluchen, sterben oder gemein sein werden, während die Staffage – Sonne, Sterne, Milieu, Religion, Liebe, soziale Frage – die Ehre haben wird, ihm als Thema für Gespräche, Gefühle oder Nerveneindrücke zu dienen. Von den »Confessiones« des heiligen Augustinus zu denen Rousseaus und vom »Werther« zur »Kreutzersonate« waren das die Bücher, die am meisten von sich reden und über sich denken machten. Die Seele ist unerschöpflich, weil sie zugleich Beobachter und Objekt ist; das ist ein Thema, das man nicht aus schreiben und nicht aussprechen, weil nicht ausdenken kann.</p>
                    <p rend="zenoPLm4n0" xml:id="tg373.3.8">Die »Physiologie« ist, wie »Werthers Leiden«, eine Auflösungsgeschichte, ihr Held, Claude Larcher aus den »Mensonges«, wie Werther eine Halbnatur mit Dilettantenkräften und überkünstlerischer Sensibilität, die Form die denkbar vernünftigste für den Ichroman, keine Korrespondenz mit dem »Freund« qui tient le crachoir des sentiments, kein Tagebuch in der linken Lade eines kleinen Rokokoschreibtisches, einfach ein Buch für den Druck bestimmt, Todeskampf im Dreiviertelprofil, der stille Lebenswunsch des Hamlet journaliste.</p>
                    <p rend="zenoPLm4n0" xml:id="tg373.3.9">Claude Larcher schreibt mit der Hamletseele, der geistfunkelnden, zynischen, schillernden, sentimentalen, »oberen« Seele; und stirbt an der »unteren«, der Tierseele, dem kranken Willen des Körpers, der seine eigene Angst und Eifersucht, seine eigene Eitelkeit und Erinnerung hat: nur der Tod ist beiden gemeinsam. Das ist die grauenhafte Allegorie des Mittelalters von dem Königssohn, der blutleer dahinfriert, bis ihm die Ärzte Blut aus dem Leib eines starken Knechts in die Adern leiten; und wie er dann weiterlebt und das Bauernblut ihm die Königsgedanken mit Tierinstinkten durchtränkt; und wie er endlich stirbt an der Wunde, die zur selben Stunde eine Dirne dem Knecht in den Hals gebissen – – –</p>
                    <p rend="zenoPLm4n0" xml:id="tg373.3.10">Fühlen, wie die eine Hälfte unseres Ich die andere mitleidlos niederzerrt, den ganzen Haß zweier Individuen, die sich nicht <pb n="94" xml:id="tg373.3.10.1"/>
verstehen, in sich tragen, das führt bei der krankhaften Hellsichtigkeit des Neuropathen schließlich zur Erkenntnis eines Kampfes aller gegen alle: keine Verständigung möglich zwischen Menschen, kein Gespräch, kein Zusammenhang zwischen heute und gestern: Worte lügen, Gefühle lügen, auch das Selbstbewußtsein lügt. Dieser Kampf des Willens endigt jenseits von Gut und Böse, von Genuß und Qual: denn sind Genuß und Qual nicht sinnlose Worte, wenn das heißeste, wahnsinnige Begehren zugleich wütender Haß, wollüstiger Zerstörungstrieb und die sublimste Pose der Eitelkeit die der ekelhaften Selbstzerfleischung ist? Man erkennt solche Dinge, und man stirbt nicht daran. Die Ärzte beruhigen uns damit, daß wir nur nervenleidend sind, und vergleichen unser Gefühl mit dem Alpdruck, den ja auch eine lächerlich geringfügige Ursache hervorbringt; als ob es besonders angenehm wäre, jahrelang mit der Empfindung spazierenzugehen, daß wir mit dem Kopf nach abwärts aus einem Luftballon hängen, an den uns nur ein dünner Faden bindet.</p>
                    <p rend="zenoPLm4n0" xml:id="tg373.3.11">Sie raten uns auch, »jede Aufregung zu meiden« und »über unseren Zustand nicht nachzudenken«. Nun, Claude Larcher hat sich die letztere so zu Herzen genommen, que – croyant, en effet, devoir à ses désillusions de se livrer à l'alcool –, il ne sortit plus de deux ou trois bars anglais où il s'intoxicait de cocktails. – – – Und bei alledem der seltsame Hochmut, sich nicht gestehen zu wollen, daß es der Körper ist, an dem die Seele leidet; diese Scheu vor dem »Materialismus«! Welche Welle atavistischer Christlichkeit schlägt da durch die blaguierende Zynik, durch die dekadente Koketterie dieser »confessions de souffrance«? »L'âme seule agit sur l'âme«! Das ist eine Lüge, schlimmer als das: es ist eine Plattheit.</p>
                    <p rend="zenoPLm4n0" xml:id="tg373.3.12">Wenn wir am Körper sterben können, so danken wir auch dem Körper, den Sinnen, die Grundlage aller Poesie, von der ersten Ahnung, den Spuren des Frühlings in unserer Lyrik, bis zum bebenden Ahnen der Verwesung im Grab. Es zittert viel freies Christentum mit Klostersehnsucht durch Paul Bourgets letzte Bücher. Mir ist Tolstois demütig-proletarische Christlichkeit lieber. Sie ist überzeugender.</p>
                    <p rend="zenoPLm4n0" xml:id="tg373.3.13">Es gibt vielleicht noch einen anderen Heilsweg aus der »mourance« <pb n="95" xml:id="tg373.3.13.1"/>
heraus als den hinter die Klostermauern: die Reflexion vernichtet, Naivetät erhält, selbst Naivetät des Lasters; Naivetät, ingénuité, simplicitas, die Einfachheit, Einheit der Seele im Gegensatz zur Zweiseelenkrankheit, also Selbsterziehung zum ganzen Menschen, zum Individuum Nietzsches. Das sagt der Moralist Bourget nicht, obwohl in seinen Aphorismen viel Nietzsche steckt – wohl unbewußt, weils eben in der Luft liegt –, aber der Künstler Bourget sagt es um so deutlicher. Drei ganzen Menschen begegnet Claude auf seiner Leidensfahrt: Die erste Geschichte ist rührend gewöhnlich: Ein Mann, der am Krebs leidet und den Doktor bittet, ihm die Wahrheit zu sagen, damit er seine Angelegenheiten in Ordnung bringen könne. »Sie haben einen Monat Zeit; ich habe es ihm gesagt«, erzählt der Arzt, »das ist das härteste in unserem Beruf. Er drückte die Hände vors Gesicht und weinte stumm große Tränen, die auf das Tischtuch niederfielen. Dann dankte er mir und bat mich, es seiner Frau zu verheimlichen ... Als sie eintrat, plauderte er lächelnd mit mir. – Es ist doch etwas Schönes, ein Charakter.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg373.3.14">Der zweite ist ein Gewohnheitsspieler, mit dem großen Zug der gewaltigen Leidenschaft auf den unbeweglichen Zügen; »er wird sich früher oder später erschießen, mit derselben Ruhe, mit der er jetzt 10000 Frank dubliert, – aber er wird gelebt haben.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg373.3.15">Der dritte ist Raymond Casal, der vollendetste Gentleman unter den Gestalten Bourgets, der viveur im großen Stil, der echte homme à femmes ... Il comprenait, en regardant ces hommes, qu'il y a dans toute passion vraiment complète une poésie, un je ne sais quoi de tragique et de grandiose ... et je l'enviai à l'idée que je ne lui ressemblerai jamais.</p>
                    <p rend="zenoPLm4n0" xml:id="tg373.3.16">Es gibt auch eine Idee, eine Verpflichtung, die das Leben so ganz ausfüllen kann: Das Recht auf die Pflicht wird ebensosehr verkannt, als es das Recht auf Arbeit so lange wurde. Das Recht auf Arbeit gehört in die Soziologie und hat seine Märtyrer auf den Barrikaden gehabt; für das Recht auf die Pflicht kämpft man in sich selbst und stirbt an Leberleiden oder Rückenmarksschwindsucht, das heißt an den Folgen des ungesunden Milieus und des verfehlten Berufes.</p>
                    <p rend="zenoPLm4n0" xml:id="tg373.3.17">
                        <pb n="96" xml:id="tg373.3.17.1"/>
Viele Leute, denen man zu nahe treten würde, wenn man ihnen für gewöhnlich ernste Lektüre zumuten wollte, werden trotzdem dieses ernste Buch lesen. Ich sehe schon auf dem gelben Umschlag das 72me mille prangen. Ob das etwas beweist?</p>
                    <p rend="zenoPLm4n0" xml:id="tg373.3.18">Vielleicht, nachdem Bourget schon einmal die supercherie gehabt, das Buch unter einem falschen Namen herauszugeben, vielleicht begeht er auch noch die zweite, in Nachahmung von Molières »Critique de l'École des Femmes« eine »Physiologie des admirateurs de Claude Larcher« zu schreiben. Vielleicht werden dabei – geistreiche Schriftsteller kokettieren ja gern mit ein wenig Selbstironie – die Käufer von siebzig Zweiundsiebzigstel nicht allzu gut wegkommen. Es weht ein so aristokratisches Parfum nach cercle und mirliton, bookmakers und Marquisen in dem Buch; es ist eine so verzeihliche Schwäche, gern in Gesellschaft von Raymond Casal, François Coppée, Maurice Barrès und anderer Sterne der literarischen gomme zu rauchen, zu blaguieren und zu lieben, man speist und küßt dort so raffiniert geistreich – das dürfte die great attraction für die ersten Siebzigtausend sein. Unter den zweitausend übrigen dürften neben den Herrn Kollegen, die diese Schatzkammer psychologischer Detailbeobachtung mit dem wohlwollenden Lächeln innerlichen Ärgers über den Reichtum derselben kritisierend durchstöbern, auch ein paar Dilettanten – im alten hübschen Sinn dilettanti – sein, die darin nichts suchen als eine Seele, qui aiment à sentir sentir, wie der arme Claude so hübsch sagt. Wenn man heute, nachdem die Epidemie des Historismus so ziemlich vorüber ist, die großen epochemachenden Bücher der Bewegung durchblättert – etwa Taines »Histoire de la littérature en Angleterre« oder Carlyles »Cromwell« –, was durchströmt sie anders als diese Sehnsucht, hinauszuflüchten aus der verknöcherten Schablonenleidenschaft der Gegenwart, Menschen, versunkene Geschlechter, lieben und fluchen zu hören, rauschendes, lebendes Blut zu fühlen: à sentir sentir. Björnson wollte einen Dramenzyklus, die »Überspannung menschlicher Kräfte«, schreiben. Den Priester, der Wunder tun soll, an die er selbst nicht mehr glaubt, hat er ausgerührt. Den Zyklus aber werden <pb n="97" xml:id="tg373.3.18.1"/>
vielleicht kommende Jahrzehnte fertigschreiben; die Aufgabe ist Gemeingut, wie alles wahrhaft Zeitgemäße, das heißt Unzeitgemäße. Zolas »L'Oeuvre«, Bahrs »Gute Schule«, Albertis »Alte und Junge« und Bourgets »Claude Larcher« sind Variationen des zweiten Typus, des Künstlers, der über die Kraft hinaus will. Johannes Rosmer ist der dritte Typus, der Reformator.</p>
                    <p rend="zenoPLm4n0" xml:id="tg373.3.19">Daß das Leitmotiv in der »Physiologie« so bescheiden auftritt, daß nicht so viel von Kunst gesprochen oder besser über Kunst abgehandelt wird wie in den deutschen Büchern, sondern den meisten Raum psychologische Paradoxe, Theaterklatsch, Analyse des modernsten Gesellschaftstones mit seinen Anglizismen und dekadenten Neubildungen einnehmen, beweist mir, daß der Realismus Bourgets vollständiger sieht.</p>
                    <p rend="zenoPLm4n0" xml:id="tg373.3.20">Man denkt manchmal über allerlei Tiefstes, aber während es einem durch die Seele zuckt, steht man ganz ruhig vor der Affiche eines café chantant oder sieht zu, wie eine hübsche Frau dem Wagen entsteigt, große Gedanken, die eigentlichen Lebensgedanken der »oberen Seele« stimmen die »untere« <hi rend="italic" xml:id="tg373.3.20.1">nicht</hi> weihevoll, und wir können ganz gut einer abgebrochenen Gedankenreihe Nietzsches nachspüren und zugleich einen blöden crevé um sein englisches smoking beneiden. Darum begegnen uns bei Paul Bourget so viel Teetische von Leuckars, Toiletten von Doucet und Statuetten aus dem allerletzten Salon.</p>
                </div>
                <div type="footnotes">
                    <head type="h4" xml:id="tg373.3.23">Fußnoten</head>
                    <note xml:id="tg373.3.24.note" target="#tg373.3.7.1">
                        <p xml:id="tg373.3.24">
                            <anchor xml:id="tg373.3.24.1" n="/Literatur/M/Hofmannsthal, Hugo von/Essays, Reden, Vorträge/Zur Physiologie der modernen Liebe#Fußnoten_1"/>
                            <ref cRef="/Literatur/M/Hofmannsthal, Hugo von/Essays, Reden, Vorträge/Zur Physiologie der modernen Liebe#Fußnote_1" xml:id="tg373.3.24.2">1</ref> »Moderne Dichtung« 1890, 9. u. 10. Heft</p>
                    </note>
                </div>
            </div>
        </body>
    </text>
</TEI>