<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg392" n="/Literatur/M/Hofmannsthal, Hugo von/Essays, Reden, Vorträge/Appell an die oberen Stände">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Appell an die oberen Stände</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Hofmannsthal, Hugo von: Element 00229 [2011/07/11 at 20:24:30]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <notesStmt>
                <note>
                              Erstdruck in: Neue Freie Presse (Wien), 8.9.1914.
                           </note>
            </notesStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Hugo von Hofmannsthal: Gesammelte Werke in zehn Einzelbänden. Reden und Aufsätze 1–3. Herausgegeben von Bernd Schoeller in Beratung mit Rudolf Hirsch, Frankfurt a.M.: S. Fischer, 1979.</title>
                        <author key="pnd:118552759">Hofmannsthal, Hugo von</author>
                    </titleStmt>
                    <extent>347-</extent>
                    <publicationStmt>
                        <date when="1979"/>
                        <pubPlace>Frankfurt a.M.</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1874" notAfter="1929"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg392.3">
                <div type="h3">
                    <head type="h3" xml:id="tg392.3.2">Hugo von Hofmannsthal</head>
                    <head type="h2" xml:id="tg392.3.3">Appell an die oberen Stände</head>
                    <p xml:id="tg392.3.5">
                        <pb type="start" n="347" xml:id="tg392.3.5.1"/>
Das Ungeheure betäubt jeden Geist, aber es ist in der Gewalt des Geistes, diese Lähmung wieder von sich abzuschütteln. Unsere Lähmung von uns abzuschütteln, um das geht es jetzt. Das völlig Unfaßliche ist Ereignis geworden; wir erlebens und fassen es nicht, werden es durchstehen, und es wird gewesen sein, wie ein dunkler Traum, durch dessen Finsternisse doch Gottes Licht hinzuckte, Gottes Atem hinwehte, fühlbarer als in öden, stockenden Jahren, die wir zuvor zu ertragen hatten.</p>
                    <p rend="zenoPLm4n0" xml:id="tg392.3.6">Aber jetzt gilt es weiterzuleben, während dies Ungeheure um uns sich vollzieht. Es gilt, zu leben, als ob ein Tag wie alle Tage wäre. Es gilt, sich zu ernüchtern – daß wir nüchtern werden könnten, ist eine Gefahr. Aber gefährlich ist es und frevelhaft, von Erregungen einzig leben zu wollen und für Erregungen. Gefährlicher wäre es und frevelhafter, in der Absonderung das Ungeheure, das heute Wirklichkeit ist, vergessen zu wollen, an Behagen, an eigensüchtigen Genuß, und wäre es selbst geistiger Art, zu denken. Hier sind Skylla und Charybdis. Aber dazwischen führt ein Weg.</p>
                    <p rend="zenoPLm4n0" xml:id="tg392.3.7">Die schöne Berauschung ist das Kind des hohen Augenblicks; von ihr haben wir gekostet, und sie wird uns wiederkommen, in glorreichen und in leidvollen Stunden: aber wir dürfen mit ihr nicht unseren Alltag aufschmücken wollen. Schön war das Jauchzen der Mädchen, der Kinder, die Greise mit Früchten und Blumen in der Hand, von der Salzach bis an den Dnjestr, der jetzt das Blut von braven Männern trinkt; schön ist der scheue, ehrfürchtige Blick, mit dem Frauen und Knaben dem Verwundeten folgen, einem der <hi rend="italic" xml:id="tg392.3.7.1">Unseren,</hi> wenn sie ihn vorbeifahren sehen, oder er geht, einer von vielen, in der Straße an uns vorüber, blaß und mit einem Blick, aus dem das unsagbare Erlebnis zu uns spricht; aber nicht schön ist es, wenn Hunderte sichs zur trägen, schlendernden Gewohnheit machen, vor den Häusern zu stehen, wo man sie hinbringt, <pb n="347" xml:id="tg392.3.7.2"/>
um die Bahnhöfe zu lungern, und sich aus dem Ungeheuren einen Feiertag zu machen. Die draußen haben keinen Feiertag, und so ist auch Werktag für uns, Werktag und wieder Werktag, bis zu dem großen Feiertag, wo sie wieder heimkommen und wir ihnen zujubeln werden, daß es bis ans Gewölb des Himmels hinaufschlägt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg392.3.8">Wir wollten helfen, wir wollten alle mithelfen, wir streckten unsere Hände hin, wir hielten unsere Herzen hin, alle Frauen, alle Kinder wollten helfen; alles geriet außer Rand und Band, jeder verließ seinen Posten und das war menschlich und recht und schön. Aber jetzt muß jeder zurück auf seinen Posten und dem Werktag geben, was des Werktags ist. Wir haben Geld hergeschenkt, und es war viel und war doch wenig; es wäre kindlich zu glauben, daß das alles ist, was uns zu tun oblag: man spürt doch, jeder in seinem Herzen, daß das noch wenig war. Und der Bahnhoflabedienst, und die Liebesgaben, und die tausend anderen Dinge, sie sind alle schön – aber sie sind nicht alles. Und das Warten von einem Zeitungsblatt zum andern ist begreiflich – aber nicht produktiv. Und es handelt sich darum, produktiv zu sein, jeder auf seinem Posten. Aus sich herauszuholen, was herauszuholen ist, jeder auf seinem Gebiet, darum handelt sichs.</p>
                    <p rend="zenoPLm4n0" xml:id="tg392.3.9">Unser sind drei Millionen, die stehen jetzt im Felde, und heute oder morgen holt jeder von ihnen, jedes einzelne von einer Mutter geborne Menschenkind das Übermenschliche aus sich heraus, bei Tag und Nacht, in Sumpf und Wald, im Sand, im Lehm, im Kalkgestein, hungernd und im Feuer, dürstend und im Feuer, schlaflos und im Feuer. Das tun die. Und unser sind zwölf oder fünfzehn Millionen, die auf dem Acker arbeiten, und die haben in diesen Wochen geschafft und geschafft, und haben die Ernte eingebracht, mit den alten Männern unter ihnen und den halbwüchsigen Mädchen und den Kindern; und so werden sie die Hackfrüchte heimbringen, und sie werden den Wein heimbringen, und sie werden das Getreide in die Mühle bringen, und sie werden die Saat säen fürs kommende Jahr. So tun die, was an ihnen ist.</p>
                    <p rend="zenoPLm4n0" xml:id="tg392.3.10">Und unser sind zwölf oder fünfzehn Millionen, die arbeiten in den großen Betrieben, und sie finden vielleicht weniger <pb n="348" xml:id="tg392.3.10.1"/>
Arbeit, als sie leisten möchten, und da fehlts an Baumwolle und dort an Kohle, und da an Hanf, und dort an schwedischen Erzen und dort an Zylinderöl für die Maschinen, das uns Amerika liefert; aber dieses Ganze wird im Gang bleiben, der Staat erzwingts, die allgemeine Not erzwingts und sie werden sich durchbringen oder wir werden sie durchbringen, so oder so, es muß sein.</p>
                    <p rend="zenoPLm4n0" xml:id="tg392.3.11">Aber es handelt sich noch um anderes, das uns obliegt, uns allein, gerade uns, uns in den großen Städten, uns in Wien vor allem. Da ist unser Schneider, da ist die Putzmacherin, da ist der Wäscheladen, da ist die Federnschmückerin; sie wollen leben. Der Posamentierer und der Lederarbeiter wollen leben. Der Buchhändler und sein Gehilfe wollen leben. Fünftausend Menschen oder siebentausend, die bereit sind, Abend für Abend zu unserer und unserer Frauen Unterhaltung zu geigen und zu flöten, zu mimen und zu singen, und die wir sonst nur schwer entbehren konnten, wollen leben. Und es ist an uns, daß wir leben und sie leben lassen. Dies »leben Lassen« hat jetzt eine verzweifelt ernste Bedeutung bekommen. Wenn wir sie nämlich nicht leben lassen, so werden sie ernste Schwierigkeiten haben, überhaupt weiterzuleben.</p>
                    <p rend="zenoPLm4n0" xml:id="tg392.3.12">Der wohlhabende, ja nur der besitzende Mittelstand hat jetzt vor allem diese eine Aufgabe: zu leben und leben zu lassen. Zu vielen Zeiten hätte es ihn geziert, ein wenig bedürfnisloser zu sein, nur nicht zu dieser jetzigen. Im Augenblick, wo der äußere Markt abgeschnitten ist, aus Enge des Herzens und Dürre der Phantasie den inneren zu paralysieren, wäre Wahnsinn oder ein wenig schlimmer als Wahnsinn.</p>
                    <p rend="zenoPLm4n0" xml:id="tg392.3.13">Nur sehr bedingt ist jetzt das Verkleinern des Hausstandes anzuempfehlen, nur sehr bedingt der Verzicht auf das Überflüssige. Man hat vielfach so gern, so gedankenlos über seine Verhältnisse gelebt; nun tue man es gedankenvoll. Ostentation, sonst so abstoßend, jetzt wird sie hoher Anstand. Was sonst leeres Getue war, die Pflichten der Geselligkeit, nun<hi rend="italic" xml:id="tg392.3.13.1"> sind sie etwas.</hi> Was früher Anmaßung war und Vorwegnahme, jetzt wird es zur Pflicht. Jedes muntere Wort erfüllt jetzt eine hohe Pflicht, jeder Witz ist jetzt eine kleine Tat. Die Autos sind bei der Armee, die Pferde sind bei der Armee, <pb n="349" xml:id="tg392.3.13.2"/>
aber die behaglichen Häuser sind geblieben, und es werden nicht die schlechtesten Musikabende und Geselligkeiten sein, zu denen man wie im Vormärz zu Fuß geht. Die Bravsten sind bei der Armee, aber es bleiben die Witzigen, die Gelehrten, die Erfahrenen. Es gilt zu leben und leben zu lassen. Man wird diesen oder jenen Saal, in dem wir Beethoven zu hören pflegten, mit Verwundeten belegen und ihm dadurch für alle Zeiten zu seinem Adel noch einen Adel verleihen, aber es werden andere Säle bleiben, und wir werden in Konzerte gehen, wie wir ins Theater gehen werden: um unsere, genau unsere Pflicht zu erfüllen. Denn es ist unsere Pflicht, genau an dem Punkt, wo das Schicksal uns hingestellt hat, Schwierigkeiten aus dem Weg zu räumen. Dadurch, ja auch dadurch helfen wir denen, die für uns siegen und sterben. Wo nicht, so lassen wir sie erbärmlich im Stich; denn es gibt keine andere Pflichterfüllung als wie auf dem angewiesenen Posten.</p>
                    <p rend="zenoPLm4n0" xml:id="tg392.3.14">In Augenblicken wie dieser, den wir durchleben, gibt es kein gleichgültiges Handeln. Jeder ist vorgerufen, auf jedem ruhen, ohne daß er es weiß, tausend Blicke. Jetzt ist jeder mutig oder feige und also gut oder böse. Und gegen den Feigen, den Bösen ist jedes Mittel recht. Niemand steht heute gegen niemand in diesem weiten Reiche, nicht Nation wider Nation, nicht Klasse wider Klasse. Aber jeder Böse, jeder Feige muß fühlen, daß er diesen Gottesfrieden bricht. Diese Zeilen schreibt nur ein Einzelner, aber es gibt keinen Einzelnen, wo die Not allgemein ist, und wie stets, im Drang, der Entschlossene den Unentschlossenen niederschlägt, wird auch das Mittel gefunden werden, den zu strafen, der böse handelt. Hier versagen die Gesetze, und das Dickicht der sozialen Ordnung scheint auch dem frevelhaft Selbstsüchtigen noch Schutz zu gewähren; aber das Außerordentliche findet einen außerordentlichen Weg, und den Bösen wird eine unerwartete Strafe ereilen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg392.3.15">Unser sind drei Millionen, die heute und morgen ihre Pflicht tun werden bis zum letzten Atemzug. So seien denn nirgends, in keinem Winkel, ihrer auch nur ein paar hundert, die sich gegen die allgemeine Pflicht vergehen. Man würde sie aus dem Winkel hervorziehen und strafen müssen.</p>
                    <pb n="350" xml:id="tg392.3.16"/>
                </div>
            </div>
        </body>
    </text>
</TEI>