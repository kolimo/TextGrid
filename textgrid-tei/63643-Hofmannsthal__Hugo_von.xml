<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg393" n="/Literatur/M/Hofmannsthal, Hugo von/Essays, Reden, Vorträge/Grillparzers politisches Vermächtnis">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Grillparzers politisches Vermächtnis</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Hofmannsthal, Hugo von: Element 00230 [2011/07/11 at 20:24:30]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <notesStmt>
                <note>
                              Erstdruck in: Neue Freie Presse (Wien), 16.5.1915.
                           </note>
            </notesStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Hugo von Hofmannsthal: Gesammelte Werke in zehn Einzelbänden. Reden und Aufsätze 1–3. Herausgegeben von Bernd Schoeller in Beratung mit Rudolf Hirsch, Frankfurt a.M.: S. Fischer, 1979.</title>
                        <author key="pnd:118552759">Hofmannsthal, Hugo von</author>
                    </titleStmt>
                    <extent>405-</extent>
                    <publicationStmt>
                        <date when="1979"/>
                        <pubPlace>Frankfurt a.M.</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1874" notAfter="1929"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg393.3">
                <div type="h3">
                    <milestone unit="head_start"/>
                    <head type="h3" xml:id="tg393.3.1">Hugo von Hofmannsthal</head>
                    <head type="h2" xml:id="tg393.3.2">Grillparzers politisches Vermächtnis</head>
                    <p xml:id="tg393.3.4">
                        <seg rend="zenoTXFontsize80" xml:id="tg393.3.4.1">
                            <pb type="start" n="405" xml:id="tg393.3.4.1.1"/>
Feldmarschall Radetzky und sein Sänger</seg>
                    </p>
                    <p xml:id="tg393.3.5">
                        <seg rend="zenoTXFontsize80" xml:id="tg393.3.5.1"> Gelten in der Not, allein nicht länger!</seg>
                    </p>
                    <milestone unit="head_end"/>
                    <milestone unit="head_start"/>
                    <p rend="zenoPR" xml:id="tg393.3.6">
                        <seg rend="zenoTXFontsize80" xml:id="tg393.3.6.1">
                            <hi rend="italic" xml:id="tg393.3.6.1.1">Grillparzer</hi>
                        </seg>
                    </p>
                    <milestone unit="head_end"/>
                    <lb xml:id="tg393.3.7"/>
                    <p xml:id="tg393.3.8">In bedrängten Epochen wird der denkende Österreicher immer auf Grillparzer zurückkommen und dies aus zweifachem Grunde: einmal, weil es in Zeiten, wo alles wankt, ein Refugium ist, in Gedanken zu seinen Altvordern zurückzugehen und sich bei ihnen, die in der Ewigkeit geborgen sind, des nicht Zerstörbaren, das auch in uns ist, zu vergewissern; zum andern, weil in solchen Zeiten alles Angeflogene und Angenommene von uns abfällt und jeder auf sich selbst zurückkommen muß; in Grillparzer aber, der eine große Figur ist und bleibt so wenig er eine heroische Figur ist – treffen wir von unserem reinen österreichischen Selbst eine solche Ausprägung, daß wir über die Feinheit und Schärfe der Züge fast erschrecken müssen. Nur unser Blick ist sonst zuweilen unscharf, ihn und uns in ihm zu erkennen. Die Not der Zeiten aber schärft den Blick.</p>
                    <p rend="zenoPLm4n0" xml:id="tg393.3.9">Grillparzer war kein Politiker, aber neben Goethe und Kleist der politischeste Kopf unter den neueren Dichtern deutscher Sprache. Liest man eine seiner politischen Studien, etwa die über den Fürsten Metternich, so ergibt sich, mag man ihm recht geben oder nicht, das Gefühl seiner Kompetenz, ja dieses allen falls schon aus dem berühmten Resümee dieser Charakteristik in sieben Worten: »Ein guter Diplomat, aber ein schlechter Politiker«. Neben einer solchen kompetenten Behandlung des Politischen erscheint das, was gelegentlich ein so bedeutender Zeitgenosse wie Hebbel politisch äußert, eher nur als die geistreiche Anknüpfung eines Außenstehenden, Ideologie; doch bleibt es wenigstens stets gedanklich wesenhaft; wogegen die meisten politischen Äußerungen gleichzeitiger Dichter in Vers und Prosa ins Gebiet des bloß Rednerischen, <pb n="405" xml:id="tg393.3.9.1"/>
in höherem Sinn Gehaltlosen gehören und darum den Tag nicht überlebt haben. Eben darum aber galt Grillparzer den sukzessiven Schichten seiner Zeitgenossen kaum als politischer Kopf; wo die anderen Jungdeutsche, St. Simonisten, Liberale, Republikaner oder was immer Großartiges und Allgemeineuropäisches waren, war er Österreicher und gewissermaßen Realpolitiker. Wo die andern ins Allgemeine gingen, sah er das Besondere; er erfaßte das Bleibende, auch im Unscheinbaren, seine politischen Erwägungen sind immer gehaltvoll. Seine Tadler, wie Goethes Tadler, wollten ihn zeitgerechter: er war auf das Wirkliche gerichtet. Die Gegenwart bringt immer einen Schwall von Scheingedanken auf, aber des Denkenswerten ist wenig: er dachte das Denkenswerte. Man wollte von ihm die allgemeine politische Deklamation, er sah vor sich eine politische Materie, die ihn anging, die einzig in ihrer Art war, dieses alte lebendige Staatsgebilde, sein Österreich.</p>
                    <p rend="zenoPLm4n0" xml:id="tg393.3.10">Dieses liebte er und durchdrang es mit scharfem, politischem Denken; aber er liebte es nicht, sich unter die politische Kleie zu mengen, so war er den einen zu fortschrittlich, den andern zu reaktionär, den Ämtern schien er kühn und bedenklich, von der andern Seite gesehen kalt und an sich haltend; für die, welche allein politisch zu leben meinten, war er bei Lebzeiten ein toter Mann: nun ist freilich er lebendig, die anderen tot.</p>
                    <p rend="zenoPLm4n0" xml:id="tg393.3.11">In den Studien, den Epigrammen und Gedichten ist ein reichliches politisches Vermächtnis, ein größeres in den Dramen. Seine großen durchgehenden Themata waren diese: Herrschen und Beherrschtwerden, und Gerechtigkeit. Diese abzuwandeln, schuf er eine Kette großer politischer Figuren: den Bancban und seinen König, Ottokar und Rudolf von Habsburg, Rudolf II., Libussa. Man hat eine Gewohnheit angenommen, diese Seite seiner Welt über dem Zauber seiner Frauenfiguren zu übersehen, aber in einer schöpferischen Natur verschränkt sich vieles, und wer das Große einseitig betrachtet, verarmt nur selber.</p>
                    <p rend="zenoPLm4n0" xml:id="tg393.3.12">Politik ist Menschenkunde, Kunst des Umganges, auf einer höheren Stufe. Ein irrationales Element spielt hier mit, wie beim Umgang mit Einzelnen: wer die verborgenen Kräfte <pb n="406" xml:id="tg393.3.12.1"/>
anzureden weiß, dem gehorchen sie. So offenbart sich der große politische Mensch. Vom Dichter ist es genug, wenn er die Mächte ahnt und mit untrüglichem Gefühl auf sie hinweist.</p>
                    <p rend="zenoPLm4n0" xml:id="tg393.3.13">Für Österreich kommen ihrer zwei in Betracht, die von den politischen Zeitideen nur leicht umspielt werden, wie Gebirg und Tal von wechselnden Nebelschwaden: der Herrscher und das Volk. Zu beiden von den Zeitpolitikern nicht immer klar als solche erkannten Hauptmächten stand Grillparzers Gemüt und Phantasie in unablässiger Beziehung. Ihn trieb ein tiefer Sinn, sich wechselweise in beide zu verwandeln: er war in seinem Wesen Volk und war in seinen Träumen Herrscher. In beiden Verwandlungen entwickelte er das Besondere, Starke, Ausharrende seiner österreichischen Natur.</p>
                    <p rend="zenoPLm4n0" xml:id="tg393.3.14">Vielleicht darf man hier zwei Gestalten etwas überraschend zusammenstellen: Rudolf II. und die Frau aus dem Volke im »Armen Spielmann«, die Greislerstochter. Beide zusammen geben symbolisch Grillparzers Österreich. Sie sind beide von starker und tiefer Natur, geduldig, weise, gottergeben, unverkünstelt und ausharrend. Beide sind sie scheu und gehemmt; beide bedürfen sie des Mediums der Liebe, um von Menschen nicht verkannt zu werden, aber mit Gott und der Natur sind sie im reinen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg393.3.15">Man spricht nicht selten von einer gewissen Kunstgesinnung, wofür L'art pour l'art das Schlagwort ist und die man mit lebhaftem Unmut ablehnt, ohne sich immer ganz klar zu sein, was darunter zu verstehen ist; aber man darf nicht vergessen, daß eine ähnliche Gesinnung auf allen Lebensgebieten sich beobachten ließe, überall gleich unerfreulich: der Witz um des Witzes willen, das Geschäft um des Geschäftes willen, das Faktiöse um des Faktiösen willen, die Deklamation um der Deklamation willen. Es gibt ein gewisses L'art pour l'art der Politik, das viele Übel verschuldet hat; in die politische Rhetorik um der Rhetorik willen ist der Dichter, der als Politiker hervortreten will, zu verfallen in ernster Gefahr. Grillparzer war viel zu wesenhaft, um dies nicht scharf von sich abzulehnen; die Laufbahn Lamartines oder etwa die Aspirationen der Professoren und Dichter, die in der Frankfurter Paulskirche <pb n="407" xml:id="tg393.3.15.1"/>
laut wurden, lockten ihn nicht. Eine einzige Anknüpfung an das praktische politische Leben wäre seiner Natur möglich gewesen: im persönlich-dienstlichen Verhältnis zu einem schöpferischen Staatsmann, zu Stadion. Wo nämlich am politischen Fachmann jene freundlich glänzende Seite hervortritt, wo der Weltmann und Philosoph wird wie Prinz Eugen und Friedrich II., wie Kaunitz und de Maistre, da ergibt sich die Möglichkeit, daß er auch andere produktive Kräfte ins Spiel setze als die rein politischen. So entsteht Kultur: als ein Bewußtwerden des Schönen in dem Praktischen, als eine vom Geist ausgehende Verklärung des durch Machtverhältnisse konstruktiv Begründeten. So hat Goethe Kultur definiert: »Was wäre sie anders als Vergeistigung des Politischen und Militärischen?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg393.3.16">Hier war für Grillparzer die Konstellation nicht glücklich: er war zu unreif, als eines solchen Mannes wie Stadions Blick auf ihn fiel; später, als die schwere Krise von 1848 ihn für einen Augenblick im reinsten Sinne zum Politiker machte und zu einer ephemeren geistig-politischen Macht erhob, war er überreif. In den dazwischenliegenden Jahrzehnten hatte man ihn nicht gerufen. Es fehlt in Österreich selten an geistigen Kräften, öfter an dem Willen, von ihnen Gebrauch zu machen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg393.3.17">Grillparzer geht aus dem alten Österreich hervor und ragt in das neue hinein; er steht mitten zwischen der Zeit Maria Theresiens und unsrer eigenen. Sein Charakter, der hierher und dorthin paßt, beiderseits als ein lebendig zugehörendes Element, gibt uns den Begriff eines unzerstörbaren österreichischen Wesens. Man hat die spezifisch österreichische Geistigkeit gegenüber der süddeutschen etwa oder der norddeutschen oder der schweizerischen öfter abzugrenzen gesucht. Der Anteil an Gemüt, an Herz wird eifersüchtig bestritten; dieser geheimnisvollsten höchsten aller Fibern, zu der alles sich hinaufbildet, vindiziert jedes Volk eben die Eigenschaften, welche ihm, seiner Natur nach, die kostbarsten scheinen. Es ist nicht die dunkle Tiefe, durch welche das österreichische Gemüt den Kranz erringt, sondern die Klarheit, die Gegenwart. Der Deutsche hat ein schwieriges, behindertes Gefühl zur Gegenwart. Sei es Epoche, sei es Augenblick, ihm fällt <pb n="408" xml:id="tg393.3.17.1"/>
nicht leicht, in der Zeit zu leben. Er ist hier und nicht hier, er ist über der Zeit und nicht in ihr. Darum wohl ist bei keinem Volk so viel von der Zeit die Rede, als bei den Deutschen; sie ringen um den Sinn der Gegenwart, uns ist er gegeben. Dies Klare, Gegenwärtige ist am schönsten im österreichischen Volk realisiert, unter den oberen Ständen am schönsten in den Frauen. Dies ist der geheime Quell des Glücksgefühls, das von Haydns, Mozarts, Schuberts, Strauß' Musik ausströmt und sich durch die deutsche und die übrige Welt ergossen hat. Dies Schöne, Gesegnete würde ohne uns in Europa, in der Welt fehlen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg393.3.18">Dies ist auch der Seelenpunkt in Grillparzers dichterischen Werken, wodurch sie sich als österreichische hervorheben. Aber alle anderen Seiten des österreichischen Wesens sind an ihm nicht minder wahrnehmbar: zu diesen dürfen wir die natürliche Klugheit rechnen, die naiv ist, den Mutterwitz ohne einen Zusatz des Witzelnden, welches als ein von Natur Fremdes neuerdings hinzugetreten ist oder hinzutreten möchte; eine völlige Einfachheit, wovon der oberste Stand sich den Begriff der Eleganz ausgeprägt hat – der sich mit dem tieferen der Vornehmheit kaum berührt; dann eine gewisse Kargheit und Behinderung des Ausdrucks, das Gegenteil etwa der preußischen Gewandtheit und Redesicherheit: jenes lieber zuwenig als zu viel zu sagen, war bei Grillparzer bis zum Grillenhaften ausgebildet; in der Tat sagt er meistens mehr, als es auf den ersten Blick scheinen mochte. Im Ablehnen von Phrasen nicht nur, auch von neu aufkommenden Wörtern und Bildungen war er unerbittlich; das Übertreiben in Worten war ihm das wahre Symbol der um sich greifenden Schwäche und Liederlichkeit. Zum Schlusse nenne ich den österreichischen Sinn für das Gemäße, die schöne Mitgift unsrer mittelalterlichen, von zartester Kultur durchtränkten Jahrhunderte, wovon uns trotz allem noch heute die Möglichkeit des Zusammenlebens gemischter Völker in gemeinsamer Heimat geblieben ist, die tolerante Vitalität, die uns durchträgt durch die schwierigen Zeiten und die wir hinüberretten müssen in die Zukunft. Von ihr war in Grillparzer die Fülle und ganz unbewußt, sein Österreichertum hatte nichts Problematisches. Seinem innersten Gemüt, dem Leben seines <pb n="409" xml:id="tg393.3.18.1"/>
Lebens, der Phantasie standen die slawischen Böhmen und Mährer nahe, wie die Steirer oder Tiroler; er polemisiert gegen Palacky, aber wie formuliert er seinen Vorwurf: daß er allzu deutsch sei, allzu weit von deutschen Zeitideen sich verlocken lasse. Daß Böhmen zu uns gehört, die hohe, unzerstörbare Einheit: Böhmen und die Erblande, dies war ihm gottgewollte Gegebenheit, nicht ihm bloß, auch dem Genius in ihm, der aus dieser Ländereinheit von allen auf Erden seine Heimat gemacht hatte. Schillers Dramen spielen noch in aller Herren Ländern, die Grillparzers eigentlich alle in Österreich. Die griechischen haben ihren Schauplatz nirgends, es geht in ihnen das Heimatliche im zeitlosen idealisierten Gewande, von den andern haben vier den Schauplatz auf böhmischem und erbländischem Boden, eines in Spanien, das in gewissem Sinne zur österreichischen Geschichte dazu gehört, eines auf ungarischem. Der Kontrast zwischen slawischem und deutschem Wesen, verkörpert in Ottokar und Rudolf von Habsburg, tut niemandem weh, denn es ist das glänzende, dämonisch kraftvolle, aber unsichere slawische Seelengebilde mit ebensolcher gestaltender Liebe gesehen wie das schlichte tüchtige des Deutschen, der auf Organisation und Dauerhaftigkeit ausgeht. Die dunkle Drahomira, die so lange in den Räumen seiner Seele wohnte, aber nie ans Licht trat, und die helle Libussa, das späteste Kind seiner Phantasie, sind beide mit slawischem Wesen liebevoll durchtränkt, und Hero, die Wienerin Hero, ist nicht ohne einen Tropfen jähen slawischen Blutes.</p>
                    <lb xml:id="tg393.3.19"/>
                    <p xml:id="tg393.3.20">Er klagte und tadelte, aber er schuf und liebte; sein Österreich ist so groß, so reich, so natürlich und das »Austria erit« in seinem Munde eine Selbstverständlichkeit. Er war ein Spiegel des alten, des mittleren Österreich: wenn das neue in ihn hineinsieht, kann es gewahr werden, ob es sie nicht etwa ärmer geworden ist, ob wir nicht etwa an Gehalt verloren haben und an Seelenwärme. Ob, wenn schon sein Tadel auch uns zu treffen vermag – doch auch sein Lob noch immer gerechtfertigt ist – und für wen? Sein Stolz, sein Zutrauen noch immer begründet – und auf wen?</p>
                    <pb n="410" xml:id="tg393.3.21"/>
                </div>
            </div>
        </body>
    </text>
</TEI>