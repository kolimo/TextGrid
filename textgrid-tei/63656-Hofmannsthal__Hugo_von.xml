<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg409" n="/Literatur/M/Hofmannsthal, Hugo von/Essays, Reden, Vorträge/Blick auf den geistigen Zustand Europas">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Blick auf den geistigen Zustand Europas</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Hofmannsthal, Hugo von: Element 00243 [2011/07/11 at 20:24:30]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <notesStmt>
                <note>
                              Entstanden 1922. Erstdruck in: Gesammelte Werke in Einzelausgaben, Bd. 4, Frankfurt (S. Fischer) 1955.
                           </note>
            </notesStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Hugo von Hofmannsthal: Gesammelte Werke in zehn Einzelbänden. Reden und Aufsätze 1–3. Herausgegeben von Bernd Schoeller in Beratung mit Rudolf Hirsch, Frankfurt a.M.: S. Fischer, 1979.</title>
                        <author key="pnd:118552759">Hofmannsthal, Hugo von</author>
                    </titleStmt>
                    <extent>478-</extent>
                    <publicationStmt>
                        <date when="1979"/>
                        <pubPlace>Frankfurt a.M.</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date when="1922"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg409.3">
                <div type="h3">
                    <head type="h3" xml:id="tg409.3.1">Hugo von Hofmannsthal</head>
                    <head type="h2" xml:id="tg409.3.2">Blick auf den geistigen Zustand Europas</head>
                    <p xml:id="tg409.3.4">
                        <pb type="start" n="478" xml:id="tg409.3.4.1"/>
Die Beschädigung aller Staaten und aller Einzelnen durch den Krieg war so groß, die materiellen Folgen davon sind so schwer und verwickelt und bilden eine solche Bemühung und Belastung auch der Phantasie und des Gemütslebens der Einzelnen, daß darüber ein Gefühl nicht recht zum Ausdruck kommt, wenigstens nicht zu einem klaren und widerhallenden, sondern nur zu einem gleichsam betäubten Ausdruck, welches doch alle geistig Existierenden erfüllt: daß wir uns in einer der schwersten geistigen Krisen befinden, welche Europa vielleicht seit dem sechzehnten Jahrhundert, wo nicht seit dem dreizehnten, erschüttert haben, und die den Gedanken nahelegt, ob »Europa«, das Wort als geistiger Begriff genommen, zu existieren aufgehört habe.</p>
                    <p rend="zenoPLm4n0" xml:id="tg409.3.5">Es ist bemerkenswert, gehört aber zu der geheimen, anscheinend planvollen Übereinstimmung, die in allen solchen Weltkrisen herrscht, daß Europa in diesem Augenblick nicht über einen einzigen geistigen Repräsentanten verfügt, der wirklich als beherrschende europäische Figur angesehen werden könnte. Einige wenige sind europäische Figuren zwar im Sinne der Berühmtheit, nicht aber im Sinne einer von ihnen ausgehenden geistigen Macht und Autorität, wie eine solche etwa noch vor zwei Jahrzehnten Ibsen und Tolstoi eignete. Ein Mann wie Anatole France, den so eben die Stockholmer Akademie mit dem Nobelpreis gekrönt hat, ist sicher für den Augenblick eine geistige Erscheinung des ersten Ranges, aber es haftet dieser Figur doch im Verhältnis zu den größten Vertretern seiner eigenen nationalen Geisteswelt etwas Epigonenhaftes an, es geht ein geistiger Zauber von ihr aus, aber keine geistige Gewalt, vor der Europa sich beugen und die Jahrhunderte als kleine Zeitspannen erscheinen würden.</p>
                    <p rend="zenoPLm4n0" xml:id="tg409.3.6">Auch Bernard Shaw ist ohne Zweifel ein gesamteuropäisches Phänomen, und vielleicht die repräsentativste Figur des Augenblickes, <pb n="478" xml:id="tg409.3.6.1"/>
gegenüber der Übergewalt der technischen Ereignisse und angesichts der Masse von Ironie, welche durch all dies schreckliche wuchtige Geschehen und seine Verkettung mit so viel Armseligem und Lächerlichem in allen nicht völlig betäubten Intelligenzen entbunden wurde, erscheint seine witzige, ironische und in blitzartigen Sprüngen das Heterogenste zusammenbringende Geistessprache oft geradezu als der einzige Jargon, in dem sich intelligente Menschen über einen so schwindelnden Weltzustand verständigen können; in der Tat wird dieser Jargon in allen Ländern gesprochen und verstanden, nicht allein daß er in den germanischen Schule gemacht hat, er dringt auch in die romanischen und slawischen ein; es wohnt ihm etwas momentan Befreiendes inne und es ist abzusehen, daß die Shawsche Denk- und Sprechweise sich unter den journalistisch Arbeitenden eine unendliche Schülerschaft heranziehen und für Jahrzehnte das Erbe der Heinrich Heineschen Schreibweise antreten wird. Aber diese geistreiche Mentalität vermag die tiefere Schicht der menschlichen Seelen, die nach neuen – es muß das Wort gesagt werden – religiösen Bindungen begehrt, nur in eine leichte unruhige Vibration, nicht aber in wahre Erschütterung, die einem gewaltigen Umschwung vorhergeht, zu versetzen, und so bleibt auch der Ire eine Erscheinung mehr als ein Führer.</p>
                    <p rend="zenoPLm4n0" xml:id="tg409.3.7">Hat die Epoche einen geistigen Beherrscher, so ist es Dostojewski. Seine Gewalt über die Seele der Jugend ist unberechenbar, es ist eine wahre Faszination, das fieberhaft Gesteigerte in seinen Romanen ist der Jugend die gemäße Nahrung – das Gleiche, was vor hundert und noch vor fünfzig Jahren das Pathos Schillers für sie war –, er stößt durch die soziale Schilderung hindurch ins Absolute, ins Religiöse – die jungen Menschen aller Länder glauben in seinen Gestalten ihr eigenes Innere zu erkennen – er und kein Anderer ist Anwärter auf den Thron des geistigen Imperators – und wer könnte ihm diesen streitig machen – wenn nicht einer, dessen hundertster Todestag schon herannaht, und dessen Sich-Entfalten als eine geistige Macht des allerersten Ranges, nicht bloß Künstler, sondern Weiser, Magier, wahrer Führer der Seelen, Stiller <pb n="479" xml:id="tg409.3.7.1"/>
auch des religiösen Bedürfnisses, sich mit einer majestätischen Langsamkeit vollzieht: Goethe; seine Stunde immer herannahend, immer aber noch nicht da, immer neue Tore sich öffnend, neue Säulengänge auf das erhabene Zentrum weisend, wie beim Zulaß der Pilger zu einem ägyptischen Tempel.</p>
                    <p rend="zenoPLm4n0" xml:id="tg409.3.8">Es ist mehr als ein Zufall, daß uns dieses Jahr Würdigungen und Interpretationen Goethes aus der Feder von Männern aller Nationen vor Augen gekommen sind, die sämtlich weit das Gebiet des Literarhistorischen überragen, ja mit Absicht aus diesem Gebiet heraustreten, und es ist tief symbolisch, daß diese Schriften von reifen Männern herrühren, wie die Broschüre über »Die Weisheit Goethes« von dem Franzosen Henri Lichtenberger, Lehrer an der Sorbonne, oder das Buch »Goethe« von dem großen Italiener Benedetto Croce, so wie es natürlich mehr als ein bloßer Zufall ist, daß man keine von jungen Männern geleitete Zeitschrift Deutschlands, Frankreichs oder eines der anderen Länder aufschlagen kann, ohne nicht einmal, sondern zehnmal und in jeder Art von geschichtlicher, sozialwissenschaftlicher oder ästhetischer, religiöser Gedankenverbindung auf den Namen Dostojewski zu stoßen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg409.3.9">Und vielleicht ist dies das Greifbarste am europäischen Geistesleben des Augenblickes: das Ringen dieser beiden Geister um die Seele der Denkenden und Suchenden – vielleicht ist dieser Wirbel die eigentliche Mitte des sturmbewegten flutenden Aspektes, den das geistige Europa heute bietet. Über diese beiden Männer wäre es möglich, fast an jeder Stelle Europas, von einer Oxforder Studentenwohnung bis ins Sprechzimmer eines Moskauer Sowjetfunktionärs, ein Gespräch höherer Ordnung hervorzurufen, bei dem die tieferen Seelenkräfte der Unterredner, nicht bloß ihre ästhetischen Interessen ins Spiel kämen. Statt einer ruhigen monumentalen Erscheinung, zu der alle aufblicken, steht dieses Ringen zweier universeller Geister in der Mitte des allgemeinen Eruptionsfeldes.</p>
                    <p rend="zenoPLm4n0" xml:id="tg409.3.10">Es sind das alte, auf der Synthese von abendländischem Christentum und einer ins Blut aufgenommenen Antike <pb n="480" xml:id="tg409.3.10.1"/>
ruhende Europa und das zu Asien tendierende Rußland, die in Goethe und Dostojewski einander gegenüberstehen: denn die Orthodoxie, die in Dostojewski ihre notwendige Sublimierung fand, ist ein orientalisches Christentum, und diesem gegenüber erscheint das ganze europäische Christentum, Katholizismus, Luthertum und Puritanismus, in einem rein geistigen, kulturellen Sinn als Einheit. Aber noch schärfer stehen die beiden geistigen Gewalten einander gegenüber in der Verschiedenheit ihres Verhältnisses zum menschlichen Leiden. Goethes geistige Grundhaltung ist die Abwehr des Leidens, und die beiden Waffen, mit denen er es bekämpft, sind das weise Durchschauen und das weise Entsagen. Dostojewskis ganzer Lebensinhalt scheint es, das Leiden herbeizurufen und sich dem Leiden preiszugeben. Er stürzt sich gleichsam in seine Figuren hinein, um in der Vielheit ihrer Schicksale dem Leiden eine größere Angriffsfläche zu geben, als ein Einzelner ihm bietet; er läßt die Ereignisse sich überstürzen und sich aufeinandertürmen, damit das, was sich hinter ihnen verbirgt, »gleichsam von einer schicksalsschweren Höhe auf die Unzulänglichkeit der menschlichen Vernunft herabblicke«. Demgegenüber erscheint Goethes ganzes Lebenswerk, Dichtung, Betrachtung und Forschung, als eine einzige unendlich sinnvolle und planmäßige Anstalt, jenes Übergewaltige, das Dostojewski aufruft um sich ihm zu opfern, von sich abzuhalten, als eine Art von zauberischem Garten, darin ein Magier einer großartig selbstsüchtigen Einsamkeit frönt. Aber so geheimnisvoll Dostojewski ist, so ist vielleicht Goethe noch geheimnisvoller; vielleicht ist das abendländische Geheimnis noch kompakter, der Knoten noch dichter geschlungen wie beim morgenländischen. Dostojewskis letztes Wort ist vielleicht gesprochen, vielleicht weht es in einem Schrei heute von Rußland über die ganze Welt. Goethes letztes Wort aber von seinen heute noch festgeschlossenen Lippen abzulesen, wird erst einer späteren Generation, von uns abstammenden, uns unanalysierbaren Menschen gegeben sein: diese werden sich vielleicht »die letzten Europäer« nennen. Für uns wäre der Name verfrüht.</p>
                </div>
            </div>
        </body>
    </text>
</TEI>