<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg163" n="/Literatur/M/Bartsch, Karl/Märchen und Sagen/Sagen, Märchen und Gebräuche aus Meklenburg/Erster Band: Sagen und Märchen/Sagen/115. Graf Schwarzenberg">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>115. Graf Schwarzenberg</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Bartsch, Karl: Element 00184 [2011/07/11 at 20:27:53]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Karl Bartsch: Sagen, Märchen und Gebräuche aus Meklenburg 1–2. Wien: Braumüller, 1879/80.</title>
                        <author key="pnd:118506943">Bartsch, Karl</author>
                    </titleStmt>
                    <extent>101-</extent>
                    <publicationStmt>
                        <date notBefore="1879" notAfter="1880"/>
                        <pubPlace>Wien</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1832" notAfter="1888"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg163.2">
                <div type="h4">
                    <head type="h4" xml:id="tg163.2.1">115. Graf Schwarzenberg.</head>
                    <p xml:id="tg163.2.2">Ein Rittersmann mit seinem Knappen war auf einer weiten Reise begriffen. Auf seinem Wege kam er zu einem Schlosse, wo grade Hochzeit gehalten ward. Gastfreundlich ward er aufgenommen, und ihm ein Schlafgemach bereitet; allein den Ritter trieb die Eile weiter. Vergeblich warnte man ihn vor dem nahen Walde und vor dem Grafen Schwarzenberg, der darin hause; er aber schwang sich aufs Roß und verließ das Schloß. Schon ritten sie drei Stunden lang, und nichts begegnete den Reitern. ›Herr,‹ flüsterte endlich der Knappe, ›hinter uns reitet Jemand.‹ ›Guten Abend, Ritter!‹ rief eine tiefe Stimme, und der Ritter sah neben sich auf hohem Rappen einen dunklen Krieger. ›Gott grüß euch!‹ erwiderte er. Da bäumte der <pb n="101" xml:id="tg163.2.2.1"/>
Rappe sich hoch auf und die eiserne Rüstung klirrte. ›Den Gruß lieben wir nicht,‹ sprach der Fremde; ›doch was treibt dich zur Nachtzeit hieher? Kehre bei mir ein! Ich heiße Schwarzenberg; hier liegt mein Schloß im Dickicht. Niemand reitet im ersten Mondsviertel durch mein Gebiet; er muß bei mir einkehren.‹ Trotz der Warnungen seines Knappen nahm der Ritter die Einladung an. ›Dort liegt mein Schloß,‹ sagte Schwarzenberg, und links im Ellerngebüsche flimmerten die erhellten Gemächer der Behausung. ›Halt!‹ sprach Schwarzenberg; ›steig ab!‹ Sein Roß versank unter ihm. Ritter und Knappe stiegen ab. Vergebens warnte noch einmal der treue Diener. ›Folge mir!‹ rief der Graf, und der Ritter ging mit ihm in das Schloß, dessen innere Wände rabenschwarz angestrichen schienen. Auf dem Flur betrachtete der Ritter seinen seltsamen Wirth. Schwarz war sein eisernes Drahthemd und schwarz der Helm, auf dessen Spitze eine lebende, schwarze Eidechse den Kamm bildete, mit ihren Krallen fest angeklammert; der lange Schwanz schlackerte über den Nacken zwischen die Schultern hin. Mager und abgezehrt schien das Antlitz des langen Mannes; die Augen sahen scheel und ohne Wimpern; sein Athem glühte von Feuer. Sie stiegen eine Treppe hinauf, gingen durch manche krumme Windung und traten endlich in einen hellen, geräumigen Saal, in dessen Mitte die Leiche einer alten Frau im Sarge hingestreckt lag, weiß gekleidet, mit gefalteten Händen und sehr frommen Gesichtszügen. ›Das war meine Mutter und dieses Messer hat sie gemordet.‹ Da schlug die Thurmglocke Mitternacht. Der Ritter sah sich um, Schwarzenberg war nicht mehr da. Er wandte sich wieder zur Leiche; aber welche Veränderung ging mit derselben vor! Das weiße Antlitz verdunkelte sich zusehends; die ganze Leiche dehnte sich aus; der Sarg faßte sie nicht mehr. Jetzt beengte sie schon den Raum des Saales; jetzt mußte der Ritter in einen Winkel weichen. Die Glocke schlug immer weiter. Das Haupt ward wie der Vollmond; hoch starrten die geschwollenen Augen. ›Schwarzenberg,‹ rief der Ritter, ›du hast mich betrogen!‹ Der Leiche Antlitz reichte bis zur Decke; die Thurmuhr schlug aus; da platzte das Gräuel mit schrecklichem Krachen und das Haus stürzte ein. Der Ritter versank mit dem einbrechenden Gebäu in die Tiefe eines Moors; aber mit Geistesgegenwart kletterte er in der ungemessenen Tiefe durch Steine <pb n="102" xml:id="tg163.2.2.2"/>
und Gebälk, das Schwert in der Hand, und rief seinen Knappen um Hilfe an. ›Wo seid ihr, Herr?‹ fragte aus weiter Ferne der Knappe. Nach langem Suchen fand er ihn, band die Zäume der Rosse zusammen und warf das eine Ende dem Ritter zu; das andere knüpfte er an den Schwanz des Thieres und brachte ihn so aufs Trockene.</p>
                    <lb xml:id="tg163.2.3"/>
                    <closer>
                        <seg type="closer" rend="zenoPLm4n0" xml:id="tg163.2.4">
                            <seg rend="zenoTXFontsize80" xml:id="tg163.2.4.1">Mussäus in den Meklenburg. Jahrbüchern 5, 80 f.</seg>
                        </seg>
                    </closer>
                </div>
            </div>
        </body>
    </text>
</TEI>