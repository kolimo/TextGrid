<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg108" n="/Literatur/M/Janitschek, Maria/Erzählungen/Die neue Eva/Hysterie">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Hysterie</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Janitschek, Maria: Element 00033 [2011/07/11 at 20:31:06]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Maria Janitschek: Die neue Eva, Leipzig: Hermann Seemann Nachfolger, 1902.</title>
                        <author key="pnd:117078719">Janitschek, Maria</author>
                    </titleStmt>
                    <extent>69-</extent>
                    <publicationStmt>
                        <date when="1902"/>
                        <pubPlace>Leipzig</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1859" notAfter="1927"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg108.3">
                <div type="h4">
                    <head type="h4" xml:id="tg108.3.1">Hysterie</head>
                    <p xml:id="tg108.3.2">
                        <pb n="69" xml:id="tg108.3.2.1"/>
                        <pb type="start" n="71" xml:id="tg108.3.2.2"/>Die Frau Gräfin ist angekommen,« sagte der Hausknecht vom »Lamm« in Klausen und sprang vom Omnibus, um ein parr Koffer herabzuheben. Aus dem Innern des veralteten Gefährtes schälte sich eine blasse, zierliche Frau im eleganten Reisekostüm. Der Wirt lüftete respektvoll die Mütze vor der Dame. Ob ein paar Zimmerchen zu haben wären? Nach Auswahl. Bei dem verspäteten Frühling stände das halbe Haus noch leer. Er zeigte ihr die Stuben des geräumigen, <pb n="71" xml:id="tg108.3.2.3"/>
alten Gasthofs. Schliesslich entschloss sie sich zu zweien, die am komfortabelsten möbliert waren. Sie war ohne Begleitung gekommen, und das Hausmädchen musste ihr auspacken und in die Schränke einräumen helfen. Angela von Breitenbach war schon im vorigen Jahr hier gewesen, jemand zuliebe, den sie den Herrn Doktor aus Hamburg genannt hatten. Es war ein alter Herr gewesen, ein etwas verschrobener Philosoph, von den eleganten Müssiggängerinnen der vornehmen Welt umschwärmt, die sich ja so hitzig auf alles stürzen, das einigermassen originell ist. Auch diese Jahr war Angela erschienen, der alte Herr indes war inzwischen zu seinen Vätern versammelt<pb n="72" xml:id="tg108.3.2.4"/>
 worden. Sie bewohnte die Stuben neben jenen, die er im vorigen Jahr inne gehabt hatte. Sie verbrauchte Unmengen von Petroleum, denn sie brannte fast die ganze Nacht Licht. Bei Tag schlief sie oft, oder sie that so, denn ihre Fensterladen blieben verschlossen. Kein Sonnenstrahl konnte zu ihr hineindringen. Einmal musste der Arzt zu ihr geholt werden. Er blieb lange da und erwiderte später den fragenden Blick des Wirtes mit Achselzucken. »Es wird die Gräfinnenkrankheit sein«, meinte der Lammwirt spöttelnd zu seiner Ehehälfte, »die sind alle krank.« Und er dämpfte seinen Bierbass zum wunderlichsten Flüsterton, wenn er der »Kranken« im Hausflur begegnete und ihr guten Tag <pb n="73" xml:id="tg108.3.2.5"/>
bot. Sie sah aus wie ein leidendes, fünfzehnjähriges Mädchen, verschüchtert, neugierig, lebenshungrig. Das kurze, geringelte Schwarzhaar trug sie aus der Stirn gestrichen. Die kleine, scharf gebogne Nase mit den rosigen, beweglichen Nüstern stimmte mit dem weichen, sensitiven Mund überein, dessen Winkel leicht nach unten geneigt waren. Ihre Augensterne waren übergross, man konnte darauf schwören, dass sie Atropin gebrauchte. Trotz ihres jugendlichen Äussern zählte sie zweiunddreissig Jahre und war schon zweimal verheiratet gewesen. Ihr erster Mann hatte sich in einem Anfalle von Schwermut, wie es hiess, ertränkt, der zweite, ein Freund des ersteren, war zufällig auf der <pb n="74" xml:id="tg108.3.2.6"/>
Jagd verunglückt. Man erzählte sich allerlei Sonderbares aus ihrem Eheleben. Jedenfalls war sie unabhängig, selbständig und brauchte niemand von dem, was sie that, Rechenschaft abzulegen. Sie besass irgendwo in Westpreussen ein Gut, war aber meist auf Reisen. Ihre zierliche Gestalt, die grossen, dunklen, sich anklammernden Augen, ihr jugendliches Aussehen, ihre eleganten Kleider sicherten ihr die Teilnahme aller Männer. Sie bedauerte nichts so sehr, als dass die Zeit der Flagellanten vorüber war, die Zeit der Pilgerfahrten in heilige, unbekannte Länder, die Zeit der Märtyrer, der geheimen Liebesmahle in den Katakomben. So hatte sie sich schon vor ihrer ersten Verheiratung geäussert. <pb n="75" xml:id="tg108.3.2.7"/>
Schon damals sah sie schwach und hilfsbedürftig aus wie heute. In ihrer ersten Hochzeitsnacht hatte sie ausbedungen, dass zwei Schalen mit palästinischem Weihrauch an ihrem Lager dampfen sollten. Ihr armer Gatte musste in dieser Nacht sehr viel niesen, aber er nahm es geduldig hin. Er, der gute Normalmensch, wurde halbverrückt über die Zumutungen, die sie an ihn stellte. Seinem Nachfolger ging's nicht besser. Später, nach dessen Tod, weinte Angela oft bitterlich. Sie fühlte sich sehr vereinsamt. Jeder Mensch, dem sie sich näherte, suchte sie, sobald er sie besser kannte, von sich abzuschütteln.</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.3">Der Glaube an Gott setzt eine gewisse Genialität, setzt tiefe Bildung, <pb n="76" xml:id="tg108.3.3.1"/>
kurz, einen Vollmenschen voraus. Halbgelehrte, Pöbelmenschen, die nur an das glauben, was sie zwischen den Fäusten fühlen, können ihn nicht besitzen. Sie sind in ihrer geistigen Beschränktheit so borniert, zu meinen, dass die Welt als Wirkung der Ursache entbehren kann. Zu diesen letzteren gehörte auch Angela.</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.4">Sie glaubte an eine Kirche, hatte sich bislang aber nie bemüht, die Bedingungen ihres Entstehens oder ihren Stifter kennen zu lernen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.5">Eines Nachmittags, in Klausen, als die Natur in lichten Frieden gehüllt war, wanderte sie nach dem Pfarrhaus. Der Pfarrer war daheim.</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.6">
                        <pb n="77" xml:id="tg108.3.6.1"/>
Er lud sie ein, in der behaglichen Stube Platz zu nehmen. Was sie herführe, ob sie im »Lamm« gut aufgehoben sei, ob ihr Klausen gefalle? Sie erwiderte, dass ihr an alledem wenig gelegen wäre, und blickte melancholisch in sein gesund gerötetes Gesicht.</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.7">Sie suche etwas ganz Anderes. Was, wisse sie nicht, aber sie suche es. Sie begann zu weinen. Er blickte verlegen vor sich hin. Ob sie einen Trauerfall in der Familie gehabt hätte? Sie schüttelte den Kopf. Vielleicht hätte sie zu wenig Beschäftigung. Ihre Augen blitzten auf. Sie, die immer wünschte, dass der Tag die dreifache Stundenzahl besässe! Dann seien es wohl Sorgen. Wieder Verneinung. <pb n="78" xml:id="tg108.3.7.1"/>
Oder, er fixierte ihr blasses Gesicht, sie wäre krank. Krank, ja, das mochte es sein. »Die Sehnsucht verzehrt mich. Die Sehnsucht nach etwas Unbekanntem ... Gestern ging ich an einem Crucifix vorüber; der ausgestreckte, blutige Christus liess in mir die Frage erwachen, ob es vielleicht sein Kult sei, der mich befriedigen könnte. Ich habe immer Neigung zur Mystik gehabt; das Geheimnisvolle, die Qual aus Liebe, die verwirrenden Widersprüche, die aufs überraschendste gelöst werden, die Visionen, die aus der Zerstörung des Körpers aufsteigen, all das hat bestrickenden Reiz für mich.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.8">Der biedere Priester fühlte sich seltsam berührt. Er wusste nicht im geringsten, <pb n="79" xml:id="tg108.3.8.1"/>
in welche Kategorie seiner Erfahrungen er dieses Menschenexemplar einreihen sollte. Er hatte von asketischen Heiligen gelesen und kannte mehrere schwärmerische Personen, die im Ruf grosser Gottseligkeit standen. Er wagte hier nicht abweisend vorzugehen, bevor er sich klar über diese eigentümliche Frau war. Da sank sie auf die Knie vor ihm.</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.9">»Hochwürdiger Herr, hören Sie meine Beichte, erbarmen Sie sich, hören Sie meine Beichte.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.10">»Sind Sie denn katholisch?« Er senkte die Augen vor ihren drängenden Blicken.</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.11">»Ja, ich bin katholisch, aber ich habe meine Religion jahrelang aufs ärgste vernachlässigt. Bitte, bitte,« sie erhob die <pb n="80" xml:id="tg108.3.11.1"/>
gefalteten Hände zu ihm, »erfüllen Sie meinen Wunsch!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.12">Er erhob sich mit einer heftigeren Gebärde, als ihm sonst eigen war. Er ertappte sich auf einem dunklen Angstgefühl. Am liebsten wäre er hinausgerannt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.13">Aber er durfte seine Schwäche nicht zeigen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.14">»Kommen Sie, wenn es Ihnen beliebt, in die Kirche,« sagte er beherrscht.</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.15">»Heute abend um acht Uhr,« flüsterte sie hinter ihren Händen hervor. Er nickte stumm. Sie erhob sich, murmelte etwas mit gesenktem Gesicht und entfernte sich.</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.16">Er trat zum Wandschrank, goss sich ein Glas Enzian ein und trank es aus. Danach wurde ihm besser.</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.17">
                        <pb n="81" xml:id="tg108.3.17.1"/>
Seine Schwester kam mit einigen harmlosen Fragen herein, die ihren Haushalt betrafen. Er fand seinen gewohnten Gleichmut wieder, und ging ins Gärtchen, um nach den Rosen zu sehen, die dieses Jahr viel von den Raupen zu leiden hatten. Dann kam der Gemeindevorsteher auf ein Wort, dann noch ein Besuch. Es begann Ave Maria zu läuten. Der Geistliche nahm sein schwarzes Hauskäppchen ab und betete. Die Besucher entfernten sich. »Der ausgestreckte, blutige Christus, ... vielleicht ist's sein Kult ...« fuhr's ihm mitten im Gebet durch den Kopf. Er schlug schnell das Kreuz und folgte seiner Schwester zum Abendessen. Aber es wollte ihm nicht munden. Er beantwortete <pb n="82" xml:id="tg108.3.17.2"/>
kurz ihr munteres Geplauder, er dachte beständig bei sich: wenn mich doch ein Kranker holen liesse, dass ich nicht in die Kirche zu gehen brauchte! Aber kein Kranker liess ihn holen. Die Brauen zusammengezogen, begab er sich zögernd ins Gotteshaus hin über. Die letzten Strahlen der sinkenden Sonne fielen durch die bunten gotischen Fenster in das dämmerige Gewölbe hinein. Ein verwirrender Geruch, halb Duft, halb Heilkraut, den er sonst nie wahrgenommen hatte, drang ihm entgegen; er stammte von keinem Insassen des Ortes.</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.18">Vorm Hochaltar, auf den Knieen, lag eine dunkel verschleierte Gestalt. Der Pfarrer wollte leise auftreten, doch seine <pb n="83" xml:id="tg108.3.18.1"/>
derben, genagelten Schuhe vereitelten seinen Vorsatz; er fühlte ärgerliche Röte in die Wangen steigen, öffnete die Thür seines Beichtstuhls, zwängte sich auf den engen Sitz und schlug den Holzladen vor dem Gitter zurück. Ein seidnes Rauschen an ihm vorüber, dann strömte ihm ein warmer Atem entgegen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.19">»Sprechen Sie lauter«, sagte er trocken, »ich verstehe kein Wort.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.20">Aber sie sprach nicht lauter; um sie zu verstehen, musste er das Ohr dicht ans Fenster pressen. Er fühlte die Kante ihres Schleiers seine Schläfe berühren, oder waren es ihre Haare?</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.21">»Es giebt Entzückungen, intensiver als der Liebesgenuss. Ich zwang mein Mädchen, <pb n="84" xml:id="tg108.3.21.1"/>
ein junges Ding, durch Drohungen und Misshandlungen, mich zu geisseln; glauben Sie, dass das unrecht war? Ich dachte an den Ecce Homo dabei und empfand die tiefe Wonne seines bleichen, stummen Mundes. Einmal, in einem kleinen Orte Süditaliens, ich hatte mir dort ein Landhaus gemietet, habe ich einen armen Jungen – ich hatte vorher seine Eltern aus einer Notlage gerettet und sie schenkten mir ihn sozusagen – ich habe ihn überredet, sich von mir kreuzigen zu lassen. Man kommt dabei so vielem näher. Das Kind that mir schrecklich leid, aber mir war, als brächte ich Gott ein frommes Opfer. Ich hatte des Knaben Füsse auf das Kreuz nebeneinander <pb n="85" xml:id="tg108.3.21.2"/>
geheftet, wie sie den Christus in den ersten sechs Jahrhunderten darstellten. Beim dritten Nagel starb mir der Junge. Er erstickte. Ich hatte ihm einen Knebel in den Mund gesteckt, damit er nicht so laut schreie. Glauben Sie, dass ich ein Unrecht beging?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.22">Der Priester machte eine krampfhafte Bewegung, er fühlte Schweissperlen von seiner Stirne tropfen, doch es war sein Amt, sie bis zu Ende zu hören.</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.23">»O, es ist noch nicht alles,« flüsterte sie halberstickt, »ich habe noch viel auf mir. Ich weiss nicht, bin ich eine Heilige oder eine Verworfene. Ich habe viele Menschen der gemeinen Wollust entfremdet, um sie die zerfleischende <pb n="86" xml:id="tg108.3.23.1"/>
Süssigkeit jener andern fühlen zu lassen, die mit dem gewöhnlichen Gang der Natur nichts zu thun hat. Ist das ein Verdienst, ein Unrecht? Aber,« sie stöhnte auf, »ich glaube, ich fange an erschöpft zu werden, mein Herzschlag stockt, ich zittere ... o, wenn Ihnen mein Heil am Herzen liegt, strafen Sie mich, strafen Sie mich, ich folge Ihnen, wohin Sie wollen, in eine dunkle Ecke der Kirche, ich unterwerfe mich Ihnen ganz ... geisseln Sie mich ...«</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.24">Der Priester ballte sein Taschentuch in einen Knäuel zusammen und sagte: »Gehen Sie nach Hause.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.25">Sie fuhr zurück. »Wie, nach Hause?</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.26">Aber morgen abend –«</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.27">
                        <pb n="87" xml:id="tg108.3.27.1"/>
»Weder morgen abend, noch sonst jemals. Mit Beichtkindern Ihrer Art will ich nichts zu schaffen haben.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.28">Er stiess die Thüre des Beichtstuhls auf, durchschritt rasch die Kirche und eilte heim. In seiner Stube warf er sich auf die Kniee und presste das brennende Gesicht in die Hände. Er hatte sich eines Vergehens schuldig gemacht, einen Menschen, der sich ihm anvertrauen wollte, von sich gewiesen, weil er ihm zu schmutzig war. O meine Kirche, dachte er, weshalb verurteilst du den Priester dazu, das Gefäss zu sein, das geduldig die Jauche der Unreinheit in sich aufnehmen muss. Wähnst du uns aus anderm Stoff gemacht als die übrigen Menschen? Wenn die Ansteckung<pb n="88" xml:id="tg108.3.28.1"/>
 durch die Phantasie ins Herz dringt und dieses zur Erniedrigung führt, wer trägt die Schuld daran als du? O tiefe Gefahr, die in deinem bevorzugtesten Institut ruht! Weise Kirche, weshalb setzest du deine Diener ihr aus? Weshalb entblössest du deinen Herrn, heftest ihn ans Kreuz und zeigst ihn so in den Nöten seiner menschlichen Natur den Augen der Menschen? Meinst du ihm dadurch einen Dienst zu erweisen? Ihm, dem stolzen, überlegnen König, der die Geister der Erde beherrscht? Weshalb betonst du nicht einen aus der sieghaften Reihe seiner Triumphe, nur seine einzige Niederlage? Fort mit dem Crucifix, das der heimlichen Wollust kranker Schwärmer Vorschub leistet; zeige uns <pb n="89" xml:id="tg108.3.28.2"/>
Jesus, den Helden, der in der Sixtina Gericht hält; zeige uns Jesus den Hohenpriester, wie Thorwaldsen ihn uns giebt ...</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.29">Während der Geistliche mit den aufgewühlten Regungen seines Temperaments kämpfte, schlich Angela aus der Kirche. Sie fühlte sich geärgert über die Abweisung, die ihr widerfahren war. Die Stunde war voll geheimen Reizes gewesen, weshalb sollten ihr nicht ähnliche folgen? Dieser plumpe Mensch, der sie von sich gestossen hatte!</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.30">Sie ging nachdenklich in ihr Gasthaus, packte am nächsten Morgen ein und reiste ab.</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.31">Ihr Weg führte sie zu einem berühmten und berüchtigtern Seelenerforscher und Alchimisten <pb n="90" xml:id="tg108.3.31.1"/>
im Reiche des Geistes. Er prägte neue Werte aus ganz besonderm Metall, das in der vierten Dimension gegraben war. Zu seinen Füssen sassen verschiedene Personen, meistens Frauen, und lauschten seinen philosophischen Auseinandersetzungen. In seinen Mussestunden übersetzte er Bücher aus alten, halbverschollenen Sprachen des Morgenlandes, die Anweisungen über die verschiedenen Arten des Liebesgenusses enthielten. Er war auf allen Gebieten daheim, er hatte alles kennen gelernt, auch das Zuchthaus. Er war wahnsinnig interessant«. Als das kleine, bebende Persönchen Angela vor ihm stand, umschlang er es sofort und zog es an seine väterliche Brust. Sein <pb n="91" xml:id="tg108.3.31.2"/>
dickes, bleiches, glattrasiertes Gesicht schien halb einem Priester, halb einem Schauspieler zu gehören. Er trug einen dunklen Talar daheim und strahlende Edelsteine, eigenartig gefasst, an den Fingern. Er war hoch gewachsen, und seine derben Knochen liessen auf starke physische Kraft schliessen. Angela erzählte ihre Lebensgeschichte, weinte ein bischen und sagte, dass sie gekommen sei, sich Trost und Rat bei ihm zu holen. Er streichelte ihr Haar, sagte ihr einige landläufige Trostworte und empfahl sie einer seiner Freundinnen. Diese, eine Frau in den mittleren Jahren, hager, unfreundlich, nichts weniger als eine anziehende Person, ging Angela in ihren <pb n="92" xml:id="tg108.3.31.3"/>
äusseren Angelegenheiten wohl an die Hand, trug indes der verwöhnten Art der kleinen Dame durchaus keine Rechnung. Im Gegenteil. Sie beobachtete die neue Ankömmlingin im Kreis des »Meisters« mit argwöhnischen Blicken. Angela beklagte sich bei ihm über ihre Teilnahmlosigkeit. Er lächelte, ein fettes, herablassendes Lächeln. Eifersucht dulde er nicht um sich. Die Weiblein hätten sich untereinander zu vertragen. Hier gälte keine mehr als die andere. Sie alle wären Schwestern. Sie ergab sich in Anbetracht des Neuen, das sie durch ihn erleben würde. Es kam sehr langsam. Man musste sich erst durch sehr viele spiritistische Sitzungen durchöden, an den <pb n="93" xml:id="tg108.3.31.4"/>
lächerlichsten Betrug glauben, der Ringe, Brieftaschen, Blumen, Schriftstücke durch die Luft fliegen liess, man musste verzückte alte Weiber in medistischen Zuständen sehen, Gekritzel ohne Sinn aus der vierten Dimension bewundern, bis der »Meister« endlich aus diesen Proben des Gehorsams und der Verleugnung jedes Vernunftfunkens die Würdigkeit seiner Jüngerin erkannte. Zwischen kneifenden Elementels, knackenden Tischgeistern, unzufriedenen Selbstmörderseelen, verwirrten alten Weibern, meist solchen, die im Leben nichts mehr zu suchen hatten, aber noch sehr viel finden wollten, fühlte sich Angela noch nicht befriedigt. War das alles? Geister interessierten <pb n="94" xml:id="tg108.3.31.5"/>
sie weniger, ihr lag an andern Erlebnissen mehr.</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.32">Eines Tages entbot sie der Meister zu sich. Unterwürfig, wie sie die andern ihm gegenüber sah, gehorchte sie seiner Einladung. Er bewohnte ein paar mit weichlichem Luxus ausgestattete Zimmer. Von den Wänden sahen Bildnisse des Ewig-Weiblichen in seiner unverhülltesten Gestalt herab. Er sass essend an einem gedeckten Tisch, auf dem sich Thee, Obst und Backwerk befanden. Aus »innern Gründen« ass er nie Fleisch. Schüchtern liess sie sich neben ihm nieder. Nach der zweiten Tasse Thee legte er seine Hand wie segnend auf ihr Haupt und mass sie prüfend vom Scheitel bis zu <pb n="95" xml:id="tg108.3.32.1"/>
ihren in zarten gelben Stiefelchen steckenden Füssen. Dann schnitt er einen Apfel entzwei und gab ihr die Hälfte davon zu essen. Sie wurde befangen. Seine Augen sahen sie immer befehlender an, aber sie verstand ihren Ausdruck nicht ganz; verwirrt blickte sie um sich, doch die hüllenlosen Damen machten ihre Wimpern sich senken. Sie deutete mit leiser Gebärde auf die Bilder. Weshalb die alle? Bedurfte ein Mann wie er solcher Spielereien?</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.33">Gerade ein Mann wie er, sagte er pathetisch, bedurfte dieser Bildwerke zum – Abgewöhnen. Sie sah ihn heimlich, prüfend an. Da erhob er sich und flüsterte:</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.34">»Komm mit mir.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.35">
                        <pb n="96" xml:id="tg108.3.35.1"/>
Sie gehorchte. Im Nebenzimmer brannte eine kleine, von rotem Stoff verhüllte Lampe. Der Meister liess sich nach Art der Morgenländer auf den dicken Teppich nieder, und gebot Angela, die unschlüssig dastand, sich zu entkleiden. Sie zauderte ein wenig, sah ihn wieder prüfend an und begann zögernd ihre Taille zu öffnen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.36">»Weiter, weiter,« gebot er, »häng die Sachen dort über das Stühlchen.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.37">Angela hatte schon sehr viel erlebt, Liebesekstasen, wo sie einander die Kleider vom Leib gerissen hatten, aber dieses überlegne, kühle Zusehen, während sie sich auszog, brachte sie in wirkliche Verwirrung.</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.38">»Ich thue es lieber draussen,« meinte sie zaghaft, »und komm dann wieder.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.39">
                        <pb n="97" xml:id="tg108.3.39.1"/>
»Nein, ich wünsche, dass du es hier thust,« versetzte er gelassen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.40">Sie liess das Kleid fallen. Wieder Pause.</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.41">»Weiter, weiter,« sagte er, legte sich zurück und kreuzte die Arme unterm Kopf, während seine Augen auf sie gerichtet waren. Der seidne Unterrock glitt nieder. Sie fühlte ein unwilliges Rot ins Gesicht steigen. Ein kleines, weisses Unterröckchen hielt sie fest an die Taille gepresst.</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.42">»Zieh das Korsett aus und lass das Röckchen fallen. Das übrige kannst du anbehalten.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.43">Sie gehorchte und stand in einem kaum bis ans Knie reichenden Batisthöschen vor ihm.</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.44">
                        <pb n="98" xml:id="tg108.3.44.1"/>
»So ist's gut,« raunte er, »und nun komm her.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.45">Sie war ganz im Bann seiner Augen und kam. Er nestelte ein bischen an ihr herum und nahm seine frühere ruhende Pose ein. »Stell deinen rechten Fuss auf mein Bein.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.46">Sie gehorchte. Ein verzücktes Lächeln glitt über sein Gesicht, während seine Augen in ihrer Gestalt wühlten. Sie senkte den Kopf und fing zu weinen an. Er rührte sich nicht. Er war ganz stumm und steif geworden, dann liess er das Haupt sinken, schloss die Augen und seufzte auf. Sie machte sich über ihre Kleider her und zog sich an. Als sie schon angekleidet war, fühlte sie seine <pb n="99" xml:id="tg108.3.46.1"/>
Hand auf der Schulter. »Ich danke dir! Komm Anfang nächster Woche wieder.« Er entliess sie mit einer leichten Geste.</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.47">Mit schwerem Kopf kam sie heim. Nach der Scene die Entlassung! Das wollte ihr nicht passen. Das nicht. Das war ja Tyrannei. Sie ganz leer ausgehen zu lassen, während er genoss, der heilige Mann!</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.48">Trotzdem kam sie in der nächsten Woche wieder. Sie wusste, dass indessen so und so viele andere Frauen wahrscheinlich zu ähnlichen Scenen verwendet, bei ihm waren. Aber sie durfte nicht aufmucken, er duldete das nicht. War er ihr etwas?</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.49">Augenblicklich beschäftigte er stark ihre Phantasie.</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.50">
                        <pb n="100" xml:id="tg108.3.50.1"/>
Vielleicht erfüllte er das zweite Mal ihre Erwartungen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.51">Nach einer ähnlichen Einleitung wie jüngst, kauerte er sich wieder auf den Teppich und gebot ihr, sich auszukleiden. Sie gehorchte. Diesmal fiel das letzte Gewandstück. Er befahl ihr, allerlei Posen einzunehmen. Zum Schluss ganz verrückte, tolle, die ihr Schmerz bereiteten, weil sie in jeder seinen betastenden Blicken lange standhalten musste. Erschöpft vor Erregung und Müdigkeit brach sie zusammen, warf sich neben ihn hin, sah ihn mit jenem unterwürfigen, begehrenden Blick der stummen Bestie an und zerrte leise an seiner Hand. Längere Zeit rührte er sich nicht. Dann richtete er sich langsam auf. Was sie wolle?<pb n="101" xml:id="tg108.3.51.1"/>
 Anstatt der Antwort sah sie ihn heiss und scheu an. Da gebot er ihr, sich anzukleiden. Und als ihre Blicke sich noch bettelnder und leidender auf ihn hefteten, sagte er eisig: »Sei nicht unrein, kleide dich an und gehe.« Und mit diesem erhabenen Wort verfügte er sich in das letzte Zimmer, dessen Thür er hinter sich abschloss.</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.52">Mit bebenden Fingern zog sie sich an und verliess sein Haus. Sie erfuhr von einer seiner »Freundinnen«, einer jungen, üppigen Blondine, dass sie durchaus auf keine andern Überraschungen zu hoffen hätte, weiter liesse sich der heilige Mann mit keiner von ihnen ein. Angela wollte keinem Heiligen zum Abgewöhnen <pb n="102" xml:id="tg108.3.52.1"/>
dienen und dabei nichts als Unbequemlichkeit ernten. Sie suchte den »Meister« nicht mehr auf.</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.53">Sie fühlte sich sehr leidend und konsultierte einen berühmten Frauenarzt. Hier hatte sie kein Glück. Er selbst verspürte keine Lust, sie zu kurieren, sie war ihm zu wenig reizend dazu. Sein Geschmack ging nach einer andern Richtung. Er empfahl sie einem jungen, patientenbedürftigen Kollegen. Der nahm die Sache ernst und wollte sie wirklich von ihrem eingebildeten Leiden durch ehrliche Mittel heilen. Sie warf seine Rezepte ins Feuer und reiste ab. Von nun an vermehrte sie die Zahl der Frauen, die sich von Bad zu Bad schleppen, die sich nicht schämen, <pb n="103" xml:id="tg108.3.53.1"/>
Stubenmädchen und Kellnern vorzuweinen und ihre Verlassenheit zu klagen, die der Schrecken aller Hotelgäste sind, deren Teilnahme sie auf sich lenken wollen, die vom Arzt für eine Zumutung bei der Untersuchung ein derbes Wort zugeschleudert bekommen und dazu schweigen wie ein geschlagener Hund.</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.54">Sie hatte nur noch einen Wunsch: überwältigt zu werden. Nur ein einziges Mal! Das schien ihr der Gipfel aller erdenklichen Wollust. Gezwungen unter Püffen und Schlägen den Taumel des Mannes zu verspüren! Sie scheute keine Gelegenheit, ihren Wunsch erfüllt zu erhalten.</p>
                    <p rend="zenoPLm4n0" xml:id="tg108.3.55">Eines Nachmittags verliess sie elegant gekleidet, ihren kostbaren Schmuck an <pb n="104" xml:id="tg108.3.55.1"/>
sich, das Hotel. Sie wählte nicht die nur ein paar Schritte entfernte Waldpromenade, sondern schritt die heisse, staubige Landstrasse hinab. Sie ging immer weiter, weiter. Schwüle rote Abendluft schlug ihr ins Gesicht. Schon näherte sie sich dem Fluss, der die Wiesen durchquerte. Weiden begannen. Tümpel, von Gesträuch umgeben, zogen sich neben der Landstrasse hin. Plötzlich richtete sich aus niederem Buschwerk die zerlumpte Gestalt eines Landstreichers auf und streckte bettelnd die Hand zu ihr. Sie starrte einen Augenblick erschreckt auf ihn nieder. Dann veränderte sich ihr Gesicht, ihre Augen wurden gross und flammend, sie machte eine Grimasse und schrie ihm ein <pb n="105" xml:id="tg108.3.55.2"/>
Schimpfwort zu, ohne weiterzugehen. Er glotzte zu ihr auf, die Adern seiner Stirne schwollen dick an, dann machte er eine Bewegung nach ihr hin. Ein Hilfeschrei, ein Fluchtversuch, und sie wäre unbeschadet geblieben, aber sie schrie nicht. Mit einer einzigen Bewegung seiner plumpen Faust hatte er sie am Kleid gefasst und zu sich herabgerissen. Sie ächzte unter seinen wühlenden Griffen, trat ihn mit den Füssen ins Gesicht, dann wurde sie still und immer stiller, ihre Augen quollen aus den Höhlen heraus, und über ihr Gesicht verbreitete sich die rätselhafte Verzerrung des Todeskrampfes. Sie war unter den würgenden Händen des Strolches erstickt.</p>
                    <pb n="106" xml:id="tg108.3.56"/>
                </div>
            </div>
        </body>
    </text>
</TEI>