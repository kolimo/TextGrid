<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg47" n="/Literatur/M/Kafka, Franz/Erzählungen und andere Prosa/Zwei Gespräche/Gespräch mit dem Beter">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions" n="work:yes">
        <fileDesc>
            <titleStmt>
                <title>Gespräch mit dem Beter</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Kafka, Franz: Element 00010 [2012/04/19 at 05:24:28]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <notesStmt>
                <note>• Gespräch mit dem Beter
                              </note>
            </notesStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Franz Kafka: Gesammelte Werke. Herausgegeben von Max Brod, Band 1–9, Frankfurt a.M.: S. Fischer, 1950 ff.</title>
                        <author key="pnd:118559230">Kafka, Franz</author>
                    </titleStmt>
                    <extent>9-</extent>
                    <publicationStmt>
                        <date when="1950"/>
                        <pubPlace>Frankfurt a.M.</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1883" notAfter="1924"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <front>
            <div type="front">
                <head type="h3" xml:id="tg46.3.1">Franz Kafka</head>
                <head type="h2" xml:id="tg46.3.2">Zwei Gespräche aus der Erzählung</head>
                <head type="h2" xml:id="tg46.3.3">»Beschreibung eines Kampfes«</head>
            </div>
        </front>
        <body>
            <div type="text" xml:id="tg47.2">
                <milestone unit="sigel" n="Kafka-GW Bd. 5" xml:id="tg47.2.1"/>
                <div type="h4">
                    <head type="h4" xml:id="tg47.2.2">Gespräch mit dem Beter</head>
                    <p xml:id="tg47.2.3">Es gab eine Zeit, in der ich Tag um Tag in eine Kirche ging, denn ein Mädchen, in das ich mich verliebt hatte, betete dort kniend eine halbe Stunde am Abend, unterdessen ich sie in Ruhe betrachten konnte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.2.4">Als einmal das Mädchen nicht gekommen war und ich unwillig auf die Betenden blickte, fiel mir ein junger Mensch auf, der sich mit seiner ganzen mageren Gestalt auf den Boden geworfen hatte. Von Zeit zu Zeit packte er mit der ganzen Kraft seines Körpers seinen Schädel und schmetterte ihn seufzend in seine Handflächen, die auf den Steinen auflagen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.2.5">In der Kirche waren nur einige alte Weiber, die oft ihr eingewickeltes Köpfchen mit seitlicher Neigung drehten, um nach dem Betenden hinzusehn. Diese Aufmerksamkeit schien ihn glücklich zu machen, denn vor jedem seiner frommen Ausbrüche ließ er seine Augen umgehn, ob die zuschauenden Leute zahlreich wären. Ich fand das ungebührlich und beschloß, ihn anzureden, wenn er aus der Kirche ginge, und ihn auszufragen, warum er in dieser Weise bete. Ja, ich war ärgerlich, weil mein Mädchen nicht gekommen war.</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.2.6">Aber erst nach einer Stunde stand er auf, schlug ein sorgfältiges Kreuz und ging stoßweise zum Becken. <pb type="start" n="9" xml:id="tg47.2.6.1"/>
Ich stellte mich auf dem Wege zwischen Becken und Tür auf und wußte, daß ich ihn nicht ohne Erklärung durchlassen würde. Ich verzerrte meinen Mund, wie ich es immer als Vorbereitung tue, wenn ich mit Bestimmtheit reden will. Ich trat mit dem rechten Beine vor und stützte mich darauf, während ich das linke nachlässig auf der Fußspitze hielt; auch das gibt mir Festigkeit.</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.2.7">Nun ist es möglich, daß dieser Mensch schon auf mich schielte, als er das Weihwasser in sein Gesicht spritzte, vielleicht auch hatte er mich schon früher mit Besorgnis bemerkt, denn jetzt unerwartet rannte er zur Türe hinaus. Die Glastür schlug zu. Und als ich gleich nachher aus der Türe trat, sah ich ihn nicht mehr, denn dort gab es einige schmale Gassen und der Verkehr war mannigfaltig.</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.2.8">
                        <pb n="9" xml:id="tg47.2.8.1"/>
In den nächsten Tagen blieb er aus, aber mein Mädchen kam. Sie war in dem schwarzen Kleide, welches auf den Schultern durchsichtige Spitzen hatte – der Halbmond des Hemdrandes lag unter ihnen –, von deren unterem Rande die Seide in einem wohlgeschnittenen Kragen niederging. Und da das Mädchen kam, vergaß ich den jungen Mann, und selbst dann kümmerte ich mich nicht um ihn, als er später wieder regelmäßig kam und nach seiner Gewohnheit betete. Aber immer ging er mit großer Eile an mir vorüber, mit abgewendetem Gesicht. Vielleicht lag es daran, daß ich mir ihn immer nur in Bewegung denken konnte, so daß es mir, selbst wenn er stand, schien, als schleiche er.</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.2.9">Einmal verspätete ich mich in meinem Zimmer. Trotzdem ging ich noch in die Kirche. Ich fand das Mädchen nicht mehr dort und wollte nach Hause gehn. Da lag dort wieder dieser junge Mensch. Die alte Begebenheit fiel mir jetzt ein und machte mich neugierig.</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.2.10">Auf den Fußspitzen glitt ich zum Türgang, gab dem blinden Bettler, der dort saß, eine Münze und drückte mich neben ihn hinter den geöffneten Türflügel; dort saß ich eine Stunde lang und machte vielleicht ein listiges Gesicht. Ich fühlte mich dort wohl und beschloß, öfters herzukommen. In der zweiten Stunde fand ich es unsinnig, hier wegen des Beters zu sitzen. Und dennoch ließ ich noch eine dritte Stunde schon zornig die Spinnen über meine Kleider kriechen, während die letzten Menschen lautatmend aus dem Dunkel der Kirche traten. Da kam er auch. Er ging vorsichtig, und seine Füße betasteten zuerst leichthin den Boden, ehe sie auftraten.</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.2.11">Ich stand auf, machte einen großen und geraden Schritt und ergriff den jungen Menschen. »Guten Abend«, sagte ich und stieß ihn, meine Hand an seinem Kragen, die Stufen hinunter auf den beleuchteten Platz. Als wir unten waren, sagte er mit einer völlig ungefestigten Stimme: »Guten Abend, lieber, lieber Herr, zürnen Sie mir nicht, Ihrem höchst ergebenen Diener«. »Ja«, sagte ich, »ich will Sie einiges fragen, mein Herr; voriges Mal entkamen Sie mir, das wird Ihnen heute kaum gelingen.« »Sie sind mitleidig, mein Herr, und Sie werden mich nach Hause gehen lassen. Ich bin bedauernswert, das ist die Wahrheit.« »Nein«, schrie ich in den Lärm der vorüberfahrenden Straßenbahn, <pb n="10" xml:id="tg47.2.11.1"/>
»ich lasse Sie nicht. Gerade solche Geschichten gefallen mir. Sie sind ein Glücksfang. Ich beglückwünsche mich.« Da sagte er: »Ach Gott, Sie haben ein lebhaftes Herz und einen Kopf aus einem Block. Sie nennen mich einen Glücksfang, wie glücklich müssen Sie sein! Denn mein Unglück ist ein schwankendes Unglück, ein auf einer dünnen Spitze schwankendes Unglück, und berührt man es, so fällt es auf den Frager. Gute Nacht, mein Herr.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.2.12">»Gut«, sagte ich und hielt seine rechte Hand fest, »wenn Sie mir nicht antworten werden, werde ich hier auf der Gasse zu rufen anfangen. Und alle Ladenmädchen, die jetzt aus den Geschäften kommen, und alle ihre Liebhaber, die sich auf sie freuen, werden zusammenlaufen, denn sie werden glauben, ein Droschkenpferd sei gestürzt oder etwas dergleichen sei geschehen. Dann werde ich Sie den Leuten zeigen.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.2.13">Da küßte er weinend abwechselnd meine beiden Hände. »Ich werde Ihnen sagen, was Sie wissen wollen, aber bitte, gehen wir lieber in die Seitengasse drüben.« Ich nickte, und wir gingen hin.</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.2.14">Aber er begnügte sich nicht mit dem Dunkel der Gasse, in der nur weit voneinander gelbe Laternen waren, sondern er führte mich in den niedrigen Flurgang eines alten Hauses unter ein Lämpchen, das vor der Holztreppe tropfend hing.</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.2.15">Dort nahm er wichtig sein Taschentuch und sagte, es auf eine Stufe breitend: »Setzt Euch doch, lieber Herr, da könnt Ihr besser fragen, ich bleibe stehen, da kann ich besser antworten. Quält mich aber nicht.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.2.16">Da setzte ich mich und sagte, indem ich mit schmalen Augen zu ihm aufblickte: »Ihr seid ein gelungener Tollhäusler, das seid Ihr! Wie benehmt Ihr Euch doch in der Kirche! Wie ärgerlich ist das und wie unangenehm den Zuschauern! Wie kann man andächtig sein, wenn man Euch anschauen muß.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.2.17">Er hatte seinen Körper an die Mauer gepreßt, nur den Kopf bewegte er frei in der Luft. »Ärgert Euch nicht – warum sollt Ihr Euch ärgern über Sachen, die Euch nicht angehören. Ich ärgere mich, wenn ich mich ungeschickt benehme; benimmt sich aber ein anderer schlecht, dann freue ich mich. Also ärgert Euch nicht, wenn ich sage, daß es der Zweck meines Lebens ist, von den Leuten angeschaut zu werden.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.2.18">
                        <pb n="11" xml:id="tg47.2.18.1"/>
»Was sagt Ihr da«, rief ich, viel zu laut für den niedrigen Gang, aber ich fürchtete mich dann, die Stimme zu schwächen, »wirklich, was sagtet Ihr da. Ja, ich ahne schon, ja ich ahnte es schon, seit ich Euch zum erstenmal sah, in welchem Zustand Ihr seid. Ich habe Erfahrung, und es ist nicht scherzend gemeint, wenn ich sage, daß es eine Seekrankheit auf festem Lande ist. Deren Wesen ist so, daß Ihr den wahrhaftigen Namen der Dinge vergessen habt und über sie jetzt in einer Eile zufällige Namen schüttet. Nur schnell, nur schnell! Aber kaum seid Ihr von ihnen weggelaufen, habt Ihr wieder ihre Namen vergessen. Die Pappel in den Feldern, die Ihr den ›Turm von Babel‹ genannt habt, denn Ihr wußtet nicht oder wolltet nicht wissen, daß es eine Pappel war, schaukelt wieder namenlos, und Ihr müßtet sie nennen ›Noah, wie er betrunken war‹.« Ich war ein wenig bestürzt, als er sagte: »Ich bin froh, daß ich das, was Ihr sagtet, nicht verstanden habe.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.2.19">Aufgeregt sagte ich rasch: »Dadurch, daß Ihr froh seid darüber, zeigt Ihr, daß Ihr es verstanden habt.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.2.20">»Freilich habe ich es gezeigt, gnädiger Herr, aber auch Ihr habt merkwürdig gesprochen.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.2.21">Ich legte meine Hände auf eine obere Stufe, lehnte mich zurück und fragte in dieser fast unangreifbaren Haltung, welche die letzte Rettung der Ringkämpfer ist: »Ihr habt eine lustige Art, Euch zu retten, indem Ihr Eueren Zustand bei den anderen voraussetzt.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.2.22">Daraufhin wurde er mutig. Er legte die Hände in einander, um seinem Körper eine Einheit zu geben, und sagte unter leichtem Widerstreben: »Nein, ich tue das nicht gegen alle, zum Beispiel auch gegen Euch nicht, weil ich es nicht kann. Aber ich wäre froh, wenn ich es könnte, denn dann hätte ich die Aufmerksamkeit der Leute in der Kirche nicht mehr nötig. Wisset Ihr, warum ich sie nötig habe?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.2.23">Diese Frage machte mich unbeholfen. Sicherlich, ich wußte es nicht, und ich glaube, ich wollte es auch nicht wissen. Ich hatte ja auch nicht hierherkommen wollen, sagte ich mir damals, aber der Mensch hatte mich gezwungen, ihm zuzuhören. So brauchte ich ja jetzt bloß meinen Kopf zu schütteln, um ihm zu zeigen, daß ich es nicht wußte, aber ich konnte meinen Kopf in keine Bewegung bringen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.2.24">Der Mensch, welcher mir gegenüberstand, lächelte. Dann duckte er sich auf seine Knie nieder und erzählte mit schläfriger Grimasse: <pb n="12" xml:id="tg47.2.24.1"/>
»Es hat niemals eine Zeit gegeben, in der ich durch mich selbst von meinem Leben überzeugt war. Ich erfasse nämlich die Dinge um mich nur in so hinfälligen Vorstellungen, daß ich immer glaube, die Dinge hätten einmal gelebt, jetzt aber seien sie versinkend. Immer, lieber Herr, habe ich eine Lust, die Dinge so zu sehen, wie sie sich geben mögen, ehe sie sich mir zeigen. Sie sind da wohl, schön und ruhig. Es muß so sein, denn ich höre oft Leute in dieser Weise von ihnen reden.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.2.25">Da ich schwieg und nur durch unwillkürliche Zuckungen in mein Gesicht zeigte, wie unbehaglich mir war, fragte er: »Sie glauben nicht daran, daß die Leute so reden?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.2.26">Ich glaubte, nicken zu müssen, konnte es aber nicht. »Wirklich, Sie glauben nicht daran? Ach, hören Sie doch; als ich als Kind nach einem kurzen Mittagsschlaf die Augen öffnete, hörte ich, noch ganz im Schlaf befangen, meine Mutter in natürlichem Ton vom Balkon hinunterfragen: ›Was machen Sie, meine Liebe. Es ist so heiß.‹ Eine Frau antwortete aus dem Garten: ›Ich jause im Grünen.‹ Sie sagten es ohne Nachdenken und nicht allzu deutlich, als müßte es jeder erwartet haben.« Ich glaubte, ich sei gefragt, daher griff ich in die hintere Hosentasche und tat, als suchte ich dort etwas. Aber ich suchte nichts, sondern ich wollte nur meinen Anblick verändern, um meine Teilnahme am Gespräch zu zeigen. Dabei sagte ich, daß dieser Vorfall so merkwürdig sei und daß ich ihn keineswegs begreife. Ich fügte auch hinzu daß ich an dessen Wahrheit nicht glaube und daß er zu einem bestimmten Zweck, den ich gerade nicht einsehe, erfunden sein müsse. Dann schloß ich die Augen, denn sie schmerzten mich.</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.2.27">»Oh, das ist doch gut, daß Ihr meiner Meinung seid, und es war uneigennützig, daß Ihr mich angehalten habt, um mir das zu sagen. Nicht wahr, warum sollte ich mich schämen – oder warum sollten wir uns schämen –, daß ich nicht aufrecht und schwer gehe, nicht mit dem Stock auf das Pflaster schlage und nicht die Kleider der Leute streife, welche laut vorübergehen. Sollte ich nicht viel mehr mit Recht trotzig klagen dürfen, daß ich als Schatten mit eckigen Schultern die Häuser entlang hüpfe, manchmal in den Scheiben der Auslagsfenster verschwindend.</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.2.28">Was sind das für Tage, die ich verbringe! Warum ist alles so schlecht gebaut, daß bisweilen hohe Häuser einstürzen, ohne daß man einen äußeren Grund finden könnte. Ich klettere dann über <pb n="13" xml:id="tg47.2.28.1"/>
die Schutthaufen und frage jeden, dem ich begegne: ›Wie konnte das nur geschehn! In unserer Stadt- ein neues Haus – das ist heute schon das fünfte – bedenken Sie doch.‹ Da kann mir keiner antworten. Oft fallen Menschen auf der Gasse und bleiben tot liegen. Da öffnen alle Geschäftsleute ihre mit Waren verhangenen Türen, kommen gelenkig herbei, schaffen den Toten in ein Haus, kommen dann, Lächeln um Mund und Augen, heraus und reden: ›Guten Tag – der Himmel ist blaß – ich verkaufe viele Kopftücher – ja, der Krieg.‹ Ich hüpfe ins Haus, und, nachdem ich mehrere Male die Hand mit dem gebogenen Finger furchtsam gehoben habe, klopfe ich endlich an dem Fensterchen des Hausmeisters. ›Lieber Mann‹, sage ich freundlich, ›es wurde ein toter Mensch zu Ihnen gebracht. Zeigen Sie mir ihn, ich bitte Sie.‹ Und als er den Kopf schüttelt, als wäre er unentschlossen, sage ich bestimmt: ›Lieber Mann. Ich bin Geheimpolizist. Zeigen Sie mir gleich den Toten.‹ ›Einen Toten?‹ fragt er jetzt und ist fast beleidigt. ›Nein, wir haben keinen Toten hier. Es ist ein anständiges Haus.‹ Ich grüße und gehe.</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.2.29">Dann aber, wenn ich einen großen Platz zu durchqueren habe, vergesse ich alles. Die Schwierigkeit dieses Unternehmens verwirrt mich, und ich denke oft bei mir: ›Wenn man so große Plätze nur aus Übermut baut, warum baut man nicht auch ein Steingeländer, das durch den Platz führen könnte. Heute bläst ein Südwestwind. Die Luft auf dem Platz ist aufgeregt. Die Spitze des Rathausturmes beschreibt kleine Kreise. Warum macht man nicht Ruhe in dem Gedränge? Alle Fensterscheiben lärmen, und die Laternenpfähle biegen sich wie Bambus. Der Mantel der heiligen Maria auf der Säule windet sich, und die stürmische Luft reißt an ihm. Sieht es denn niemand? Die Herren und Damen, die auf den Steinen gehen sollten, schweben. Wenn der Wind Atem holt, bleiben sie stehen, sagen einige Worte zueinander und verneigen sich grüßend, stößt aber der Wind wieder, können sie ihm nicht widerstehn, und alle heben gleichzeitig ihre Füße. Zwar müssen sie fest ihre Hüte halten, aber ihre Augen schauen lustig, als wäre milde Witterung. Nur ich fürchte mich.‹«</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.2.30">Mißhandelt, wie ich war, sagte ich: »Die Geschichte, die Sie früher erzählt haben von Ihrer Frau Mutter und der Frau im Garten finde ich gar nicht merkwürdig. Nicht nur, daß ich viele derartige Geschichten <pb n="14" xml:id="tg47.2.30.1"/>
gehört und erlebt habe, so habe ich sogar bei manchen mitgewirkt. Diese Sache ist doch ganz natürlich. Meinen Sie, ich hätte, wenn ich auf dem Balkon gewesen wäre, nicht dasselbe sagen können und aus dem Garten dasselbe antworten können? Ein so einfacher Vorfall.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.2.31">Als ich das gesagt hatte, schien er sehr beglückt. Er sagte, daß ich hübsch gekleidet sei und daß ihm meine Halsbinde sehr gefalle. Und was für eine feine Haut ich hätte. Und Geständnisse würden am klarsten wenn man sie widerriefe.</p>
                </div>
            </div>
        </body>
    </text>
</TEI>