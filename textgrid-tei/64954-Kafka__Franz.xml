<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg86" n="/Literatur/M/Kafka, Franz/Erzählungen und andere Prosa/Ein Landarzt/Ein Brudermord">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions" n="work:yes">
        <fileDesc>
            <titleStmt>
                <title>Ein Brudermord</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Kafka, Franz: Element 00046 [2012/04/19 at 05:24:28]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <notesStmt>
                <note>
                                 Erstdruck in: Marsyas (Berlin), 1. Jg., 1. Heft, 1917.
                              </note>
            </notesStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Franz Kafka: Gesammelte Werke. Herausgegeben von Max Brod, Band 1–9, Frankfurt a.M.: S. Fischer, 1950 ff.</title>
                        <author key="pnd:118559230">Kafka, Franz</author>
                    </titleStmt>
                    <extent>135-</extent>
                    <publicationStmt>
                        <date when="1950"/>
                        <pubPlace>Frankfurt a.M.</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1883" notAfter="1924"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg86.3">
                <div type="h4">
                    <head type="h4" xml:id="tg86.3.1">Ein Brudermord</head>
                    <p xml:id="tg86.3.2">Es ist erwiesen, daß der Mord auf folgende Weise erfolgte:</p>
                    <p rend="zenoPLm4n0" xml:id="tg86.3.3">Schmar, der Mörder, stellte sich gegen neun Uhr abends in der mondklaren Nacht an jener Straßenecke auf, wo Wese, das Opfer, aus der Gasse, in welcher sein Büro lag, in jene Gasse einbiegen mußte, in der er wohnte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg86.3.4">Kalte, jeden durchschauernde Nachtluft. Aber Schmar hatte nur ein dünnes blaues Kleid angezogen; das Röckchen war überdies aufgeknöpft. Er fühlte keine Kälte; auch war er immerfort in Bewegung. Seine Mordwaffe, halb Bajonett, halb Küchenmesser, hielt er ganz bloßgelegt immer fest im Griff. Betrachtete das Messer gegen das Mondlicht; die Schneide blitzte auf; nicht genug für Schmar; er hieb mit ihr gegen die Backsteine des Pflasters, daß es Funken gab; bereute es vielleicht; und um den Schaden gutzumachen, strich er mit ihr violinbogenartig über seine Stiefelsohle, während er, auf einem Bein stehend, vorgebeugt, gleichzeitig dem Klang des Messers an seinem Stiefel, gleichzeitig in die schicksalsvolle Seitengasse lauschte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg86.3.5">Warum duldete das alles der Private Pallas, der in der Nähe aus seinem Fenster im zweiten Stockwerk alles beobachtete? Ergründe die Menschennatur! Mit hochgeschlagenem Kragen, den Schlafrock um den weiten Leib gegürtet, kopfschüttelnd, blickte er hinab.</p>
                    <p rend="zenoPLm4n0" xml:id="tg86.3.6">Und fünf Häuser weiter, ihm schräg gegenüber, sah Frau Wese, den Fuchspelz über ihrem Nachthemd, nach ihrem Manne aus, der heute ungewöhnlich lange zögerte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg86.3.7">Endlich ertönt die Türglocke vor Weses Büro, zu laut für eine Türglocke, über die Stadt hin, zum Himmel auf, und Wese, der fleißige Nachtarbeiter, tritt dort, in dieser Gasse noch unsichtbar, nur durch das Glockenzeichen angekündigt, aus dem Haus; gleich zählt das Pflaster seine ruhigen Schritte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg86.3.8">Pallas beugt sich weit hervor; er darf nichts versäumen. Frau Wese schließt, beruhigt durch die Glocke, klirrend ihr Fenster. Schmar aber kniet nieder; da er augenblicklich keine anderen Blößen hat, drückt er nur Gesicht und Hände gegen die Steine; wo alles friert, glüht Schmar.</p>
                    <p rend="zenoPLm4n0" xml:id="tg86.3.9">Gerade an der Grenze, welche die Gassen scheidet, bleibt Wese stehen, nur mit dem Stock stützt er sich in die jenseitige Gasse.</p>
                    <p rend="zenoPLm4n0" xml:id="tg86.3.10">
                        <pb n="135" xml:id="tg86.3.10.1"/>
Eine Laune. Der Nachthimmel hat ihn angelockt, das Dunkelblaue und das Goldene. Unwissend blickt er es an, unwissend streicht er das Haar unter dem gelüpften Hut; nichts rückt dort oben zusammen, um ihm die allernächste Zukunft anzuzeigen; alles bleibt an seinem unsinnigen, unerforschlichen Platz. An und für sich sehr vernünftig, daß Wese weitergeht, aber er geht ins Messer des Schmar.</p>
                    <p rend="zenoPLm4n0" xml:id="tg86.3.11">»Wese!« schreit Schmar, auf den Fußspitzen stehend, den Arm aufgereckt, das Messer scharf gesenkt. »Wese! Vergebens wartet Julia!« Und rechts in den Hals und links in den Hals und drittens tief in den Bauch sticht Schmar. Wasserratten, aufgeschlitzt, geben einen ähnlichen Laut von sich wie Wese.</p>
                    <p rend="zenoPLm4n0" xml:id="tg86.3.12">»Getan«, sagt Schmar und wirft das Messer, den überflüssigen blutigen Ballast, gegen die nächste Hausfront. »Seligkeit des Mordes! Erleichterung, Beflügelung durch das Fließen des fremden Blutes! Wese, alter Nachtschatten, Freund, Bierbankgenosse, versickerst im dunklen Straßengrund. Warum bist du nicht einfach eine mit Blut gefüllte Blase, daß ich mich auf dich setzte und du verschwändest ganz und gar. Nicht alles wird erfüllt, nicht alle Blütenträume reiften, dein schwerer Rest liegt hier, schon unzugänglich jedem Tritt. Was soll die stumme Frage, die du damit stellst?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg86.3.13">Pallas, alles Gift durcheinanderwürgend in seinem Leib, steht in seiner zweiflügelig aufspringenden Haustür. »Schmar! Schmar! Alles bemerkt, nichts übersehen.« Pallas und Schmar prüfen einander. Pallas befriedigt's, Schmar kommt zu keinem Ende.</p>
                    <p rend="zenoPLm4n0" xml:id="tg86.3.14">Frau Wese mit einer Volksmenge zu ihren beiden Seiten eilt mit vor Schrecken ganz gealtertem Gesicht herbei. Der Pelz öffnet sich, sie stürzt über Wese, der nachthemdbekleidete Körper gehört ihm, der über dem Ehepaar sich wie der Rasen eines Grabes schließende Pelz gehört der Menge.</p>
                    <p rend="zenoPLm4n0" xml:id="tg86.3.15">Schmar, mit Mühe die letzte Übelkeit verbeißend, den Mund an die Schulter des Schutzmannes gedrückt, der leichtfüßig ihn davonführt.</p>
                    <pb n="136" xml:id="tg86.3.16"/>
                </div>
            </div>
        </body>
    </text>
</TEI>