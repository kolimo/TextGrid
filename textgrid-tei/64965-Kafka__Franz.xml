<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg102" n="/Literatur/M/Kafka, Franz/Erzählungen und andere Prosa/Prosa aus dem Nachlaß/Der Jäger Grachhus">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions" n="work:yes">
        <fileDesc>
            <titleStmt>
                <title>Der Jäger Grachhus</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Kafka, Franz: Element 00059 [2012/04/19 at 05:24:28]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <notesStmt>
                <note>
                                 Entstanden 1917, Erstdruck in: Beim Bau der chinesischen Mauer.
                              </note>
            </notesStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Franz Kafka: Gesammelte Werke. Herausgegeben von Max Brod, Band 1–9, Frankfurt a.M.: S. Fischer, 1950 ff.</title>
                        <author key="pnd:118559230">Kafka, Franz</author>
                    </titleStmt>
                    <extent>75-</extent>
                    <publicationStmt>
                        <date when="1950"/>
                        <pubPlace>Frankfurt a.M.</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date when="1917"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg102.3">
                <div type="h4">
                    <head type="h4" xml:id="tg102.3.1">Der Jäger Grachhus</head>
                    <p xml:id="tg102.3.2">Zwei Knaben saßen auf der Quaimauer und spielten Würfel. Ein Mann las eine Zeitung auf den Stufen eines Denkmals im Schatten des säbelschwingenden Helden. Ein Mädchen am Brunnen füllte Wasser in ihre Bütte. Ein Obstverkäufer lag neben seiner Ware und blickte auf den See hinaus. In der Tiefe einer Kneipe sah man durch die leeren Tür – und Fensterlöcher zwei Männer beim Wein. Der Wirt saß vorn an einem Tisch und schlummerte. Eine Barke schwebte leise, als werde sie über dem Wasser getragen, in den kleinen Hafen. Ein Mann in blauem Kittel stieg ans Land und zog die Seile durch die Ringe. Zwei andere Männer in dunklen Röcken mit Silberknöpfen trugen hinter dem Bootsmann eine Bahre, auf der unter einem großen blumengemusterten, gefransten Seidentuch offenbar ein Mensch lag.</p>
                    <p rend="zenoPLm4n0" xml:id="tg102.3.3">Auf dem Quai kümmerte sich niemand um die Ankömmlinge, selbst als sie die Bahre niederstellten, um auf den Bootsführer zu warten, der noch an den Seilen arbeitete, trat niemand heran, niemand richtete eine Frage an sie, niemand sah sie genauer an.</p>
                    <p rend="zenoPLm4n0" xml:id="tg102.3.4">Der Führer wurde noch ein wenig aufgehalten durch eine Frau, die, ein Kind an der Brust, mit aufgelösten Haaren sich jetzt auf Deck zeigte. Dann kam er, wies auf ein gelbliches, zweistöckiges Haus, das sich links nahe beim Wasser geradlinig erhob, die Träger nahmen die Last auf und trugen sie durch das niedrige, aber von schlanken Säulen gebildete Tor. Ein kleiner Junge öffnete ein Fenster, bemerkte noch gerade, wie der Trupp im Haus verschwand, und schloß wieder eilig das Fenster. Auch das Tor wurde nun geschlossen, es war aus schwarzem Eichenholz sorgfältig gefügt. Ein Taubenschwarm, der bisher den Glockenturm umflogen hatte, ließ sich jetzt vor dem Hause nieder. Als werde im Hause ihre Nahrung aufbewahrt, sammelten sich die Tauben vor dem Tor. Eine flog bis zum ersten Stock auf und pickte an die Fensterscheibe. Es waren hellfarbige wohlgepflegte, lebhafte Tiere. In großem Schwung warf ihnen die Frau aus der Barke Körner hin, die sammelten sie auf und flogen dann zu der Frau hinüber.</p>
                    <p rend="zenoPLm4n0" xml:id="tg102.3.5">Ein Mann im Zylinderhut mit Trauerband kam eines der schmalen, <pb n="75" xml:id="tg102.3.5.1"/>
stark abfallenden Gäßchen, die zum Hafen führten, herab. Er blickte aufmerksam umher, alles bekümmerte ihn, der Anblick von Unrat in einem Winkel ließ ihn das Gesicht verzerren. Auf den Stufen des Denkmals lagen Obstschalen, er schob sie im Vorbeigehen mit seinem Stock hinunter. An der Stubentür klopfte er an, gleichzeitig nahm er den Zylinderhut in seine schwarzbehandschuhte Rechte. Gleich wurde geöffnet, wohl fünfzig kleine Knaben bildeten ein Spalier im langen Flurgang und verbeugten sich.</p>
                    <p rend="zenoPLm4n0" xml:id="tg102.3.6">Der Bootsführer kam die Treppe herab, begrüßte den Herrn, führte ihn hinauf, im ersten Stockwerk umging er mit ihm den von leicht gebauten, zierlichen Loggien umgebenen Hof und beide traten, während die Knaben in respektvoller Entfernung nachdrängten, in einen kühlen, großen Raum an der Hinterseite des Hauses, dem gegenüber kein Haus mehr, sondern nur eine kahle, grauschwarze Felsenwand zu sehen war. Die Träger waren damit beschäftigt, zu Häupten der Bahre einige lange Kerzen aufzustellen und anzuzünden, aber Licht entstand dadurch nicht, es wurden förmlich nur die früher ruhenden Schatten aufgescheucht und flackerten über die Wände. Von der Bahre war das Tuch zurückgeschlagen. Es lag dort ein Mann mit wild durcheinandergewachsenem Haar und Bart, gebräunter Haut, etwa einem Jäger gleichend. Er lag bewegungslos, scheinbar atemlos mit geschlossenen Augen da, trotzdem deutete nur die Umgebung an, daß es vielleicht ein Toter war.</p>
                    <p rend="zenoPLm4n0" xml:id="tg102.3.7">Der Herr trat zur Bahre, legte eine Hand dem Daliegenden auf die Stirn, kniete dann nieder und betete. Der Bootsführer winkte den Trägern, das Zimmer zu verlassen, sie gingen hinaus, vertrieben die Knaben, die sich draußen angesammelt hatten, und schlossen die Tür. Dem Herrn schien aber auch diese Stille noch nicht zu genügen, er sah den Bootsführer an, dieser verstand und ging durch eine Seitentür ins Nebenzimmer. Sofort schlug der Mann auf der Bahre die Augen auf, wandte schmerzlich lächelnd das Gesicht dem Herrn zu und sagte: »Wer bist du?« – Der Herr erhob sich ohne weiteres Staunen aus seiner knieenden Stellung und antwortete: »Der Bürgermeister von Riva.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg102.3.8">Der Mann auf der Bahre nickte, zeigte mit schwach ausgestrecktem Arm auf einen Sessel und sagte, nachdem der Bürgermeister seiner Einladung gefolgt war: »Ich wußte es ja, Herr Bürgermeister, <pb n="76" xml:id="tg102.3.8.1"/>
aber im ersten Augenblick habe ich immer alles vergessen, alles geht mir in der Runde und es ist besser, ich frage, auch wenn ich alles weiß. Auch Sie wissen wahrscheinlich, daß ich der Jäger Gracchus bin.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg102.3.9">»Gewiß«, sagte der Bürgermeister. »Sie wurden mir heute in der Nacht angekündigt. Wir schliefen längst. Da rief gegen Mitternacht meine Frau: ›Salvatore‹, – so heiße ich – ›sieh die Taube am Fenster!‹ Es war wirklich eine Taube, aber groß wie ein Hahn. Sie flog zu meinem Ohr und sagte: ›Morgen kommt der tote Jäger Gracchus, empfange ihn im Namen der Stadt.‹«</p>
                    <p rend="zenoPLm4n0" xml:id="tg102.3.10">Der Jäger nickte und zog die Zungenspitze zwischen den Lippen durch: »Ja, die Tauben fliegen vor mir her. Glauben Sie aber, Herr Bürgermeister, daß ich in Riva bleiben soll?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg102.3.11">»Das kann ich noch nicht sagen«, antwortete der Bürgermeister. »Sind Sie tot?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg102.3.12">»Ja«, sagte der Jäger »wie Sie sehen. Vor vielen Jahren, es müssen aber ungemein viel Jahre sein, stürzte ich im Schwarzwald – das ist in Deutschland – von einem Felsen, als ich eine Gemse verfolgte. Seitdem bin ich tot.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg102.3.13">»Aber Sie leben doch auch«, sagte der Bürgermeister.</p>
                    <p rend="zenoPLm4n0" xml:id="tg102.3.14">»Gewissermaßen«, sagte der Jäger, »gewissermaßen lebe ich auch. Mein Todeskahn verfehlte die Fahrt, eine falsche Drehung des Steuers, ein Augenblick der Unaufmerksamkeit des Führers, eine Ablenkung durch meine wunderschöne Heimat, ich weiß nicht, was es war, nur das weiß ich, daß ich auf der Erde blieb und daß mein Kahn seither die irdischen Gewässer befährt. So reise ich, der nur in seinen Bergen leben wollte, nach meinem Tode durch alle Länder der Erde.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg102.3.15">»Und Sie haben keinen Teil am Jenseits?« fragte der Bürgermeister mit gerunzelter Stirne.</p>
                    <p rend="zenoPLm4n0" xml:id="tg102.3.16">»Ich bin«, antwortete der Jäger, »immer auf der großen Treppe, die hinauf führt. Auf dieser unendlich weiten Freitreppe treibe ich mich herum, bald oben, bald unten, bald rechts, bald links, immer in Bewegung. Aus dem Jäger ist ein Schmetterling geworden. Lachen Sie nicht.« »Ich lache nicht«, verwahrte sich der Bürgermeister.</p>
                    <p rend="zenoPLm4n0" xml:id="tg102.3.17">»Sehr einsichtig«, sagte der Jäger. »Immer bin ich in Bewegung. Nehme ich aber den größten Aufschwung und leuchtet mir schon oben das Tor, erwache ich auf meinem alten, in irgendeinem irdischen<pb n="77" xml:id="tg102.3.17.1"/>
 Gewässer öde steckenden Kahn. Der Grundfehler meines einstmaligen Sterbens umgrinst mich in meiner Kajüte. Julia, die Frau des Bootsführers, klopft und bringt mir zu meiner Bahre das Morgengetränk des Landes, dessen Küste wir gerade befahren. Ich liege auf einer Holzpritsche, habe – es ist kein Vergnügen, mich zu betrachten – ein schmutziges Totenhemd an, Haar und Bart, grau und schwarz, geht unentwirrbar durcheinander, meine Beine sind mit einem großen, seidenen, blumengemusterten, langgefransten Frauentuch bedeckt. Zu meinen Häupten steht eine Kirchenkerze und leuchtet mir. An der Wand mir gegenüber ist ein kleines Bild, ein Buschmann offenbar, der mit einem Speer nach mir zielt und hinter einem großartig bemalten Schild sich möglichst deckt. Man begegnet auf Schiffen manchen dummen Darstellungen, diese ist aber eine der dümmsten. Sonst ist mein Holzkäfig ganz leer. Durch eine Luke der Seitenwand kommt die warme Luft der südlichen Nacht und ich höre das Wasser an die alte Barke schlagen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg102.3.18">Hier liege ich seit damals, als ich, noch lebendiger Jäger Gracchus, zu Hause im Schwarzwald eine Gemse verfolgte und abstürzte. Alles ging der Ordnung nach. Ich verfolgte, stürzte ab, verblutete in einer Schlucht, war tot und diese Barke sollte mich ins Jenseits tragen. Ich erinnere mich noch, wie fröhlich ich mich hier auf der Pritsche ausstreckte zum erstenmal. Niemals haben die Berge solchen Gesang von mir gehört wie diese vier damals noch dämmerigen Wände.</p>
                    <p rend="zenoPLm4n0" xml:id="tg102.3.19">Ich hatte gern gelebt und war gern gestorben, glücklich warf ich, ehe ich den Bord betrat, das Lumpenpack der Büchse, der Tasche, des Jagdgewehrs vor mir hinunter, das ich immer stolz getragen hatte, und in das Totenhemd schlüpfte ich wie ein Mädchen ins Hochzeitskleid. Hier lag ich und wartete. Dann geschah das Unglück.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg102.3.20">»Ein schlimmes Schicksal«, sagte der Bürgermeister mit abwehrend erhobener Hand. »Und Sie tragen gar keine Schuld daran?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg102.3.21">»Keine«, sagte der Jäger, »ich war Jäger, ist das etwa eine Schuld? Aufgestellt war ich als Jäger im Schwarzwald, wo es damals noch Wölfe gab. Ich lauerte auf, schoß, traf, zog das Fell ab, ist das eine Schuld? Meine Arbeit wurde gesegnet. ›Der große Jäger vom Schwarzwald‹ hieß ich. Ist das eine Schuld?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg102.3.22">»Ich bin nicht berufen, das zu entscheiden«, sagte der Bürgermeister, <pb n="78" xml:id="tg102.3.22.1"/>
»doch scheint auch mir keine Schuld darin zu liegen. Aber wer trägt denn die Schuld?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg102.3.23">»Der Bootsmann«, sagte der Jäger. »Niemand wird lesen, was ich hier schreibe, niemand wird kommen, mir zu helfen; wäre als Aufgabe gesetzt mir zu helfen, so blieben alle Türen aller Häuser geschlossen, alle Fenster geschlossen, alle liegen in den Betten, die Decken über den Kopf geschlagen, eine nächtliche Herberge die ganze Erde. Das hat guten Sinn, denn niemand weiß von mir, und wüßte er von mir, so wüßte er meinen Aufenthalt nicht, und wüßte er meinen Aufenthalt, so wüßte er mich dort nicht festzuhalten, so wüßte er nicht, wie mir zu helfen. Der Gedanke, mir helfen zu wollen, ist eine Krankheit und muß im Bett geheilt werden.</p>
                    <p rend="zenoPLm4n0" xml:id="tg102.3.24">Das weiß ich und schreie also nicht, um Hilfe herbeizurufen, selbst wenn ich in Augenblicken – unbeherrscht wie ich bin, zum Beispiel gerade jetzt – sehr stark daran denke. Aber es genügt wohl zum Austreiben solcher Gedanken, wenn ich umherblicke und mir vergegenwärtige, wo ich bin und – das darf ich wohl behaupten – seit Jahrhunderten wohne.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg102.3.25">»Außerordentlich«, sagte der Bürgermeister, »außerordentlich. – Und nun gedenken Sie bei uns in Riva zu bleiben?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg102.3.26">»Ich gedenke nicht«, sagte der Jäger lächelnd und legte, um den Spott gutzumachen, die Hand auf das Knie des Bürgermeisters. »Ich bin hier, mehr weiß ich nicht, mehr kann ich nicht tun. Mein Kahn ist ohne Steuer, er fährt mit dem Wind, der in den untersten Regionen des Todes bläst.«</p>
                    <pb n="79" xml:id="tg102.3.27"/>
                    <pb n="80" xml:id="tg102.3.28"/>
                    <pb type="start" n="84" xml:id="tg102.3.29"/>
                </div>
            </div>
        </body>
    </text>
</TEI>