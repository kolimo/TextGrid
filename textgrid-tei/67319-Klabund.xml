<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg445" n="/Literatur/M/Klabund/Erzählungen/Der Marketenderwagen/Abschied">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Abschied</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Klabund: Element 00461 [2011/07/11 at 20:28:14]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Klabund: Der Marketenderwagen. Ein Kriegsbuch, Berlin: Erich Reiß Verlag, 1916.</title>
                        <author key="pnd:118562681">Klabund</author>
                    </titleStmt>
                    <extent>26-</extent>
                    <publicationStmt>
                        <date when="1916"/>
                        <pubPlace>Berlin</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1890" notAfter="1928"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg445.3">
                <div type="h4">
                    <head type="h4" xml:id="tg445.3.1">Abschied</head>
                    <p xml:id="tg445.3.2">Als Balder sie in der grauen Felduniform, eine Rose in der Hand, am Kragen die Gefreitenknöpfe, die ihm noch am Morgen verliehen worden waren, verlassen hatte und sein schlanker Schritt auf der Treppe verklungen war, dachte Lilli, grauenvoll verwirrt und wie auseinandergefallen, allerlei widersinniges und lächerliches Zeug. Tennis ... ja, wie lange hatte sie eigentlich nicht Tennis gespielt? Flogen da nicht immer Bälle durch die Luft, und wenn man zuschlug, schlug man nicht in die Sonne und schlug man nicht die Sonne übers Netz? Wo nur ihre Tennisschuhe steckten? Richtig: Rehbraten gab es heute abend. Zum mindesten: eine Art Rehbraten. Einen richtigen Rehbraten ißt man ja nur Sonntag mittag. Also wahrscheinlich Rehschäuferl. Oder Rehragout. Mit Klößen. Klöße. Das Wort haftete ihr und sie hatte es noch in Gedanken, als ihr schon die Tränen erlöst über die Wangen strömten. –</p>
                    <p rend="zenoPLm4n0" xml:id="tg445.3.3">Als sie sich ausgeweint hatte, ging Lilli auf die Straße. Aber kaum war sie zehn Schritt gegangen, da erschrak <pb n="26" xml:id="tg445.3.3.1"/>
sie. Da ... jener feldgraue Soldat, welcher an Krücken humpelte ... war das nicht Balder? Sie stieß mit der Spitze ihres Sonnenschirms erregt aufs Pflaster, um zur Besinnung zu kommen. Wie töricht! Bal der war doch eben erst ins Feld ausgerückt ... konnte sie denn gar keinen vernünftigen Gedanken mehr fassen?</p>
                    <p rend="zenoPLm4n0" xml:id="tg445.3.4">Sie verzweifelte: jeder Verwundete, der ihr begegnete, schien ihr Balder. Jener mit dem verbundenen Kopf. Jener Dragoner mit dem Arm in der Binde. Säbelhiebe! Daß es so etwas noch gibt: er hat einen Hieb mit dem Säbel bekommen. Würde der Arm steif bleiben? Herrgott im Himmel, hilf: daß der Arm nicht steif bleibt. Sie würde alles, alles für ihn tun, daß der Arm wieder gut würde, ihn jede Stunde verbinden, jede Minute bei ihm bleiben. O, und dann der Tag, an dem sie ihm wieder zuerst die Hand schütteln durfte! Balder!</p>
                    <p rend="zenoPLm4n0" xml:id="tg445.3.5">Sie mußte sich wenden und den Schleier über ihr Gesicht ziehen, denn ihre Augen begannen silbern und immer silberner zu glänzen. Nur nicht auf der Straße weinen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg445.3.6">Als sie wieder aufzublicken wagte, kam ihr ein junger Leutnant entgegen. Kerngesund. Schlank wie Balder. In einer Gangart, der man den Kavalleristen anmerkte. Wenigstens einen, der viel zu Pferde sitzt. Er kam näher und sie erkannte, daß es ein Artillerist war. Sie freute <pb n="27" xml:id="tg445.3.6.1"/>
sich, daß es ihr gelungen war, seine Truppengattung zu bestimmen. Das ist in der feldgrauen Uniform nicht immer leicht. Der Leutnant grüßte. Sie dankte. Beglückt. Mit einem Lächeln im Herzen. Ich kenne ihn, dachte sie, gewiß kenne ich ihn. Ich weiß im Augenblick nur nicht woher. Das ist ja auch so gleichgültig. Ich bin so froh, daß er nicht verwundet ist. Und daß er Balder so ähnlich sieht.</p>
                    <p rend="zenoPLm4n0" xml:id="tg445.3.7">Und wie sie nun langsam weiter schritt, da sah sie wieder einen Soldaten. Und wieder einen. Und noch einen. Und alle waren auf einmal gesund. Gingen ohne Krücken. Trugen keinen Arm in der Binde. Rauchten Zigaretten. Manche lachten sogar. Und alle sahen Balder ähnlich.</p>
                    <p rend="zenoPLm4n0" xml:id="tg445.3.8">»Balder!« sagte sie, und ihre Füße hatten wieder festen Halt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg445.3.9">Sie stand am Odeonsplatz. Von der Theatinerhofkirche fiel ein Schwarm Tauben wie eine weiße Girlande sanft vor ihr nieder.</p>
                    <p rend="zenoPLm4n0" xml:id="tg445.3.10">Sie kramte in ihrer kleinen Handtasche und zog eine kleine braune Düte hervor. Sie schüttete die Körner in die Hand und neigte sich leicht zu den Tieren herab.</p>
                    <p rend="zenoPLm4n0" xml:id="tg445.3.11">Drüben, von der Wache am Schoß, klang Trommelrasseln und Kommandorufe.</p>
                    <p rend="zenoPLm4n0" xml:id="tg445.3.12">»Balder!« sagte sie leise vor sich hin.</p>
                    <pb n="28" xml:id="tg445.3.13"/>
                </div>
            </div>
        </body>
    </text>
</TEI>