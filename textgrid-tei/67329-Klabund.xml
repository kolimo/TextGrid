<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg455" n="/Literatur/M/Klabund/Erzählungen/Der Marketenderwagen/Die Revolutionärin">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Die Revolutionärin</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Klabund: Element 00471 [2011/07/11 at 20:28:14]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Klabund: Der Marketenderwagen. Ein Kriegsbuch, Berlin: Erich Reiß Verlag, 1916.</title>
                        <author key="pnd:118562681">Klabund</author>
                    </titleStmt>
                    <extent>71-</extent>
                    <publicationStmt>
                        <date when="1916"/>
                        <pubPlace>Berlin</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1890" notAfter="1928"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg455.3">
                <div type="h4">
                    <head type="h4" xml:id="tg455.3.1">Die Revolutionärin</head>
                    <p xml:id="tg455.3.2">Anna Emeljanowa ist die Tochter eines reichen russischen Bauern. Was man so einen reichen russischen Bauern nennt: er besitzt ein paar Schweine, ein paar Kühe, ein kleines Haus. Und das kleine Haus ist etwas weniger schmutzig als die Häuser der anderen. In der guten Stube sitzt auf irgendeiner Stuhllehne ein grauer Papagei, der aussieht wie ein Rabe. Er kann nur zwei Worte: »Anna« und »Nitschewo«. Wenn er »Anna« ruft, dann geht der alte Bauer vors Haus, hält die Hand vor die Augen und sieht in die leere Luft, bis ihm die Augen brennen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg455.3.3">Anna Emeljanowas Heimatsdorf steht hart an der preußisch-russischen Grenze. Man kann von der Grenzbarriere, wo die große Chaussee aus dem einen ins andere Land läuft, die Spitze seines Kirchturmes sehen. Und wer nur die Spitze des Kirchturmes seiner Heimat mit seinen Augen sieht: was sieht der mit seinem Herzen!</p>
                    <p rend="zenoPLm4n0" xml:id="tg455.3.4">Wie oft stand Anna Emeljanowa an der Barriere und sah hinüber nach ihrer Heimat mit der unendlichen Sehnsucht <pb n="71" xml:id="tg455.3.4.1"/>
des Russen, der wegen revolutionärer Umtriebe aus Rußland verbannt ist und nur über die Grenze blicken, sie aber niemals mehr überschreiten darf. Gewiß: man kommt mit einem falschen Paß schon wieder nach Rußland hinein, aber wer der russischen politischen Polizei von 1905 her so gut bekannt ist wie Anna Emeljanowa, darf es nur unter besonderen Umständen wagen, wenn er nicht »für die Sache« verloren sein will. Und Anna Emeljanowa wird sich »für die Sache« nur opfern, wenn es »der Sache« Nutzen bringt.</p>
                    <lb xml:id="tg455.3.5"/>
                    <milestone unit="hi_start"/>
                    <p rend="zenoPC" xml:id="tg455.3.6">*</p>
                    <milestone unit="hi_end"/>
                    <lb xml:id="tg455.3.7"/>
                    <p xml:id="tg455.3.8">Ich lernte Anna Emeljanowa vor zwei Jahren in Genf in einem Cafégarten kennen. Man träumte über den See hin, ließ sich den Schleier des Mont Blanc vor die Stirn wehen und wandte seine Blicke, angeödet von der braunen Langeweile des Salève, weg: zum violetten Wasser, zu den hellblau schimmernden Schwänen am Ufer, die man in Gedanken streichelte. Schwäne darf man nur in Gedanken streicheln. In Wirklichkeit beißen sie und sind sehr bösartig.</p>
                    <p rend="zenoPLm4n0" xml:id="tg455.3.9">»Sehen Sie,« sagte Anna Emeljanowa plötzlich – wir hatten das Gleiche gedacht – »Rußland ist für uns Revolutionäre ein solcher Schwan ...«</p>
                    <p rend="zenoPLm4n0" xml:id="tg455.3.10">Und sie fuhr mit einer zärtlichen Handbewegung durch die Luft.</p>
                    <p rend="zenoPLm4n0" xml:id="tg455.3.11">
                        <pb n="72" xml:id="tg455.3.11.1"/>
Anna Emeljanowa ist verheiratet.</p>
                    <p rend="zenoPLm4n0" xml:id="tg455.3.12">Ich lernte auch ihren Mann kennen: einen sanften, schwarzbärtigen, und wie es hieß, sehr talentvollen Maler.</p>
                    <p rend="zenoPLm4n0" xml:id="tg455.3.13">Anna Emeljanowa hat keine Kinder. Und dabei ein wundervoll mütterliches Herz wie viele russische Revolutionärinnen. Stundenlang spielt sie mit verdreckten Kindern in rohen und unreinlichen Gassen und geht zu ihren Eltern auf die Wohnung.</p>
                    <p rend="zenoPLm4n0" xml:id="tg455.3.14">Anna Emeljanowa darf keine Kinder haben. Die »Sache« will es. Sie darf sich an kein weltliches Glück binden.</p>
                    <p rend="zenoPLm4n0" xml:id="tg455.3.15">Ihr Gatte ist natürlich ebenfalls Revolutionär. Immer müssen sie warten, daß »die Sache« sie plötzlich ruft. Und dann müssen sie bereit sein. Sofort. Ohne Verzug. Sie besitzen nur das Allernotwendigste. Eine kleine, möblierte Wohnung. Zwei Zimmer. Ärmlich und asketisch eingerichtet. Selbst an Büchern nur das Notdürftigste. Etwa: Bakunin, Tolstoi. Eine Kiste Zigaretten. Einen Samowar. Und einen Teller süßliche Nußkuchen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg455.3.16">»Rußland ist so groß,« sagte Anna Emeljanowa immer, »muß man es nicht lieben?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg455.3.17">Und ihre Gedanken irrten wohl zu der Chausseebarriere an der preußisch-russischen Grenze, an der sie <pb n="73" xml:id="tg455.3.17.1"/>
manchmal ihrem alten Vater die Hand schütteln und die Kirchturmspitze ihrer Heimat sehen durfte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg455.3.18">Ich hatte lange von Anna Emeljanowa nichts gehört. Da kam neulich eine Karte aus Genf. Darauf standen nur diese Worte:</p>
                    <p rend="zenoPLm4n0" xml:id="tg455.3.19">»Die Sache ruft. Leben Sie wohl. Anna Emeljanowa.«</p>
                    <lb xml:id="tg455.3.20"/>
                    <milestone unit="hi_start"/>
                    <p rend="zenoPC" xml:id="tg455.3.21">*</p>
                    <milestone unit="hi_end"/>
                    <lb xml:id="tg455.3.22"/>
                    <p xml:id="tg455.3.23">Wie man in russischen Zeitungen liest, finden in den russischen Lazaretten Teeabende statt. Es wird gesungen, musiziert, rezitiert und von den Leichtverwundeten auch ein wenig gelacht und Schabernack getrieben. Die Schwestern sind angehalten, sich auf das angelegentlichste mit den geistigen Bedürfnissen der Leute zu beschäftigen. Die Schwestern und die Verwundeten sprechen sehr viel und sehr leise miteinander – so leise oft, daß man es am nächsten Bett nicht hört – und ich glaube, in einer dieser Schwestern ... Anna Emeljanowa zu erkennen.</p>
                    <pb n="74" xml:id="tg455.3.24"/>
                </div>
            </div>
        </body>
    </text>
</TEI>