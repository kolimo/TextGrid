<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg467" n="/Literatur/M/Klabund/Erzählungen/Der Marketenderwagen/Der Flieger">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Der Flieger</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Klabund: Element 00483 [2011/07/11 at 20:28:14]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Klabund: Der Marketenderwagen. Ein Kriegsbuch, Berlin: Erich Reiß Verlag, 1916.</title>
                        <author key="pnd:118562681">Klabund</author>
                    </titleStmt>
                    <extent>122-</extent>
                    <publicationStmt>
                        <date when="1916"/>
                        <pubPlace>Berlin</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1890" notAfter="1928"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg467.3">
                <div type="h4">
                    <head type="h4" xml:id="tg467.3.1">Der Flieger</head>
                    <p xml:id="tg467.3.2">Als der Fliegerunteroffizier Georg Henschke, Sohn eines märkischen Bauern, vom Kriege nach Hause auf Urlaub kam, stand sein Heimatdorf schon einige Tage vorher Kopf. Bei seiner Ankunft lief alles, was Beine hatte, ihm halber Wege, einige Beherzte sogar <num type="fraction">1<num type="fraction">
                                <hi rend="superscript" xml:id="tg467.3.2.1">1</hi>/<hi rend="subscript" xml:id="tg467.3.2.2">2</hi>
                            </num>
                        </num> Stunden bis zur Bahnstation Baudach entgegen, und die Kinder und die halbwüchsigen Mädchen saßen auf den Kirschbäumen, welche die Straße säumten, die er kommen mußte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg467.3.3">Nun war er da. Das ganze Dorf drängte sich eng um ihn, daß er kaum Luft holen konnte, seine Mutter weinte: »Georgi, mein Georgi!«, und der Pastor sagte: »Welch eine Fügung Gottes!« »Kinder,« lachte Georg Henschke, »Kinder, ich habe einen Mordshunger!« Da stob man auseinander, um sich gleich darauf zu einem Zuge zu gruppieren, der ihn würdevoll zur Tafel geleitete. Sie war unter freiem Himmel aufgeschlagen. Das Dorf nahm sich die Ehre, ihm ein Essen zu geben. Man zählte ungefähr sieben Gänge, und in jedem kam in <pb n="122" xml:id="tg467.3.3.1"/>
irgendeiner Form Schweinefleisch vor. Dazu trank man süßen, heurigen Most.</p>
                    <p rend="zenoPLm4n0" xml:id="tg467.3.4">Nach dem Essen, als der Wein seine Wirkung tat, wurde man keck. Man wagte Georg Henschke anzusprechen, zu fragen, zu bitten. »Georgi,« staunte zärtlich seine Mutter, »Du kannst nun fliegen!« »Wollen Sie uns nicht einmal etwas vorfliegen?« fragte schüchtern die kleine Marie. »O,« lachte Georg Henschke, »das geht nicht so ohne weiteres. Da gehört ein Apparat dazu!« »Er hat ihn sicher in der Tasche,« grinste verschmitzt der Hirt, »er will uns nur auf die Folter spannen.« »Ein Apparat, das ist so etwas zum Aufziehen?« fragte seine jüngste Schwester Anna. Denn sie dachte daran, daß er ihr einmal aus Berlin einen Elefanten aus Blech mitgebracht hatte. Eine Stange lief unbarmherzig durch seinen Bauch, und wenn man sie ein paarmal herumdrehte, begann der Elefant zu wackeln, mit seinem Rüssel auf den Boden zu klopfen und plötzlich wie ein Wiesel und in wirren Kreisen im Zimmer herumzulaufen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg467.3.5">»Nein,« sagte Georg Henschke, »ich habe den Apparat nicht bei mir, denn er gehört dem Staat.« »So, so,« meinte der Hirt mit seinem weißhaarigen Kopf, »der Staat. Das ist auch so eine neue Erfindung.« »Ganz recht,« lachte Georg Henschke.</p>
                    <p rend="zenoPLm4n0" xml:id="tg467.3.6">
                        <pb n="123" xml:id="tg467.3.6.1"/>
»So erzähle uns doch etwas vom Fliegen, und wie man es lernt, Georgi,« bat seine Mutter. Sie war so stolz auf ihn.</p>
                    <p rend="zenoPLm4n0" xml:id="tg467.3.7">Da stand Georg Henschke auf, und alle mit ihm.</p>
                    <p rend="zenoPLm4n0" xml:id="tg467.3.8">»Gut, ich will es tun. Hört zu!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg467.3.9">Er sprang auf einen Stuhl. Sie scharten sich um ihn. Aufgeregt, seinem Willen hingegeben, wie die Herde um das Leittier. Sie hoben ihre Köpfe, sehnsüchtig, und der blaue Himmel lag in ihren Augen. Georg Henschke aber reckte die Arme, schüttelte sie gegen das Licht, in seinen Blicken blitzte die Freude des Triumphators, und als er sprach, flammte es aus ihm. Er selber fühlte sich so leicht werden, so lächelnd leicht, der Boden sank unter seinen Füßen, seine Arme breiteten sich wie Schwingen, wiegten sich, und wie ein Adler stieß er hoch und steil ins Blau.</p>
                    <p rend="zenoPLm4n0" xml:id="tg467.3.10">Das ganze Dorf stand wie <hi rend="italic" xml:id="tg467.3.10.1">ein</hi> Wesen, das hundert Köpfe in den Himmel bog. Und sie sahen Georg Henschke im Äther schweben, ruhig und klar, fern und ferner, bis er ihren Blicken entschwand.</p>
                    <pb n="124" xml:id="tg467.3.11"/>
                </div>
            </div>
        </body>
    </text>
</TEI>