<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg496" n="/Literatur/M/Klabund/Erzählungen/Kunterbuntergang des Abendlandes/Paula">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Paula</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Klabund: Element 00513 [2011/07/11 at 20:28:14]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Klabund: Kunterbuntergang des Abendlandes. Grotesken, München: Roland-Verlag, 1922.</title>
                        <author key="pnd:118562681">Klabund</author>
                    </titleStmt>
                    <extent>82-</extent>
                    <publicationStmt>
                        <date when="1922"/>
                        <pubPlace>München</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1890" notAfter="1928"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg496.3">
                <div type="h4">
                    <head type="h4" xml:id="tg496.3.1">Paula</head>
                    <p xml:id="tg496.3.2">Paula, ein junges Mädchen von zweifelhaftem Berufe und lockeren Sitten, begab sich an den Wannsee und mietete sich dort ein Ruderboot, die Stunde zu 85 Pfennig. Wie sie es so von ihrem Leben gewohnt war, ließ sie sich von der Strömung treiben. Plötzlich teilten sich die Wogen vor ihr und ein junger Mann tauchte gleich einem Nix aus der grauen Flut und schwang sich mit nervigen Fäusten in das Boot. Er trug nicht einmal einen Badeanzug, was sie keineswegs verwunderte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg496.3.3">»Sind Sie ein Wassergott?« fragte Paula, die sich zuweilen mit Mythologie beschäftigte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg496.3.4">Der junge Mann öffnete den verständnislosen Mund zu einem gewinnenden Lächeln.</p>
                    <p rend="zenoPLm4n0" xml:id="tg496.3.5">»Gewiß doch; ich bin Stadtreisender.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg496.3.6">»Warum, wenn man fragen darf? Und was suchen Sie bei mir?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg496.3.7">
                        <pb n="82" xml:id="tg496.3.7.1"/>
»Eben das«, sagte der junge Mann und deckte seine Blöße mit einem Schatten zu, der von seinem Haupte fiel.</p>
                    <p rend="zenoPLm4n0" xml:id="tg496.3.8">Darauf zog er einen Ring von seinem Finger und flüsterte: »Elli, meine süße Braut.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg496.3.9">Paula, auf rechten Namen weniger als auf rechte Gesinnung bedacht, wagte es nicht, den Jüngling zu desillusionieren und ihm einen Korb zu geben, den er sich hätte höher hängen können, und sie waren sehr glücklich.</p>
                    <p rend="zenoPLm4n0" xml:id="tg496.3.10">Nach einer halben Stunde sah der Jüngling erschreckt zum Himmel und rief: »Es ist schon halb vier«, worauf er in den Wellen mit einem Hechtsprung verschwand, den ihm so leicht kein Hecht nachmachte. Paula winkte ihm, bis er im Freibad verfloß. Dann kam sie wieder zu sich und bemerkte den Ring an ihrem Finger. Sie küßte ihn und ruderte ans Ufer, bis sie Schwielen an den Händen bekam. Sie nahm in der Stadtbahn ein Billett zweiter Klasse, während sie sonst nur dritter fuhr.</p>
                    <p rend="zenoPLm4n0" xml:id="tg496.3.11">
                        <pb n="83" xml:id="tg496.3.11.1"/>
Sie ging zu einem Juwelier.</p>
                    <p rend="zenoPLm4n0" xml:id="tg496.3.12">Der Ring war falsch.</p>
                    <p rend="zenoPLm4n0" xml:id="tg496.3.13">Empört durchbohrte sie den Juwelier, der ihr diese schnöde Auskunft gab, mit einer Hutnadel, welche trotz polizeilicher Vorschrift ungesichert war.</p>
                    <p rend="zenoPLm4n0" xml:id="tg496.3.14">Die Polizei sollte wirklich darauf achten, daß ihren Verordnungen besser entsprochen wird. Viele Verbrechen und Unglücksfälle ließen sich so auf die einfachste Art vermeiden.</p>
                    <p rend="zenoPLm4n0" xml:id="tg496.3.15">Paula beschloß, Jünglingen ohne Badehose künftig aus dem Wege zu gehen.</p>
                    <pb n="84" xml:id="tg496.3.16"/>
                </div>
            </div>
        </body>
    </text>
</TEI>