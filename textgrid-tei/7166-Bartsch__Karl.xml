<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg877" n="/Literatur/M/Bartsch, Karl/Märchen und Sagen/Sagen, Märchen und Gebräuche aus Meklenburg/Erster Band: Sagen und Märchen/Märchen und Legenden/3. Der Königssohn">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>3. Der Königssohn</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Bartsch, Karl: Element 00951 [2011/07/11 at 20:27:53]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Karl Bartsch: Sagen, Märchen und Gebräuche aus Meklenburg 1–2. Wien: Braumüller, 1879/80.</title>
                        <author key="pnd:118506943">Bartsch, Karl</author>
                    </titleStmt>
                    <extent>477-</extent>
                    <publicationStmt>
                        <date notBefore="1879" notAfter="1880"/>
                        <pubPlace>Wien</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1832" notAfter="1888"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg877.2">
                <div type="h4">
                    <head type="h4" xml:id="tg877.2.1">3. Der Königssohn.</head>
                    <p xml:id="tg877.2.2">Es war einmal ein Königssohn, der ritt mit seinen Dienern auf die Jagd. Er hatte schon den ganzen Tag gejagt, ohne etwas zu treffen; er war im Begriffe, heimzukehren, als eine Ricke ihm aufstieß. Um doch wenigstens etwas nach Hause zu bringen, setzte er ihr nach. Aber immer, wenn er sie nahe genug glaubte, um seinen Speer werfen zu können, war sie ihm wieder entschwunden. Dabei verlor er seine Gefährten ganz, die denn ohne ihn heimkehrten. Die Ricke lief endlich über eine Brücke, der Königssohn hinter ihr her. Kaum war er hinüber, als die Brücke hinter ihm abbrach, und vor ihm stand statt der Ricke ein altes häßliches Weib, das ihn aufforderte zu folgen. Er mußte ihr gehorchen, er mochte wollen oder nicht. Sie führte ihn in ein Schloß mitten im Walde, das sie mit ihren drei Töchtern bewohnte. Die beiden ältesten waren so häßlich wie ihre Mutter und ebenso unfreundlich gegen ihn, die jüngste aber hübsch und freundlich. Nach einiger Zeit forderte die Alte ihn auf, <pb n="477" xml:id="tg877.2.2.1"/>
ihre älteste Tochter zu heiraten. Dagegen weigerte er sich aber und erbot sich, die Jüngste zu nehmen. Das wollte jedoch die Alte nicht, und er wurde von ihr und den beiden älteren Schwestern scharf bewacht, damit er nicht entrinne. Er fand aber doch Gelegenheit, der Jüngsten seine Liebe zu gestehen, die sie ihrerseits herzlich erwiderte. Beide beschlossen zu entfliehen. Im Herbste liefen sie eines Nachts davon. Aber am Morgen setzte ihnen die mittlere Schwester nach. Wie die Jüngste bemerkte, daß sie verfolgt wurden, verwandelte sie sich in einen Rosenstock und ihren Geliebten in eine Rose. Da kehrte die Schwester um und erzählte, sie habe die Flüchtlinge nicht finden können, und zugleich, daß sie mitten im Walde einen Rosenstock gesehen hätte. Da wurde sie von ihrer Mutter und Schwester gescholten, daß sie den Rosenstock nicht mitgebracht hatte. Nun wurde die älteste Tochter nachgeschickt. Als sie den Verfolgten auf die Spur kam, verwandelte ihre Schwester sich in ein Caroussel und ihren Geliebten in den Besitzer desselben, der in der Mitte sitzend in einem Buche las. Da kehrte die Aelteste um und berichtete, daß sie nichts gefunden und was sie im Walde gesehen. Nun eilte die Alte ihnen selbst nach. Diesmal verwandelte sich die jüngste Tochter in einen See und den Königssohn in eine Ente, die auf dem See schwamm; vorher aber hatte sie ihn gewarnt, dem Ufer nicht zu nahe zu kommen. Die Alte lockte die Ente mit Brot, und einmal glaubte sie sie so nahe, daß sie mit der Hand darnach griff; da verlor sie aber das Gleichgewicht und fiel ins Wasser und ertrank.</p>
                    <p rend="zenoPLm4n0" xml:id="tg877.2.3">Die beiden Liebenden setzten nun ihren Weg fort und kamen auch glücklich in die Heimat des Königssohns. Vor dem Thore verabredeten sie, die Braut solle noch draußen bleiben, während er hineingehe. Er traf nur seine Mutter noch am Leben, sein Vater war gestorben. Großer Jubel empfing ihn bei seiner Rückkehr und große Feste wurden veranstaltet, so daß er seine Braut ganz vergaß und ihm zuletzt sein ganzes Erlebniß im Walde wie ein Traum erschien. Die Braut wartete draußen bis an den verabredeten Tag. Als er da nicht kam, verkleidete sie sich und ging ins Schloß, wo sie sich als Kammerzofe verdingte und durch ihre Geschicklichkeit und Bescheidenheit sich bald die Gunst der Königin erwarb. Es gelang ihr aber nicht, ihren Geliebten zu Gesicht zu bekommen. Da wünschte <pb n="478" xml:id="tg877.2.3.1"/>
sie sich eines Tages ein prachtvolles Kleid, auf dem der ganze Sternenhimmel zu sehen war, und weil sie eine Zauberin war, bekam sie es auch. Das zeigte sie der Königin, und diese, ganz entzückt darüber, wollte es ihr abkaufen. Das Mädchen aber wollte es für Geld nicht hergeben, sondern es ihr schenken unter der Bedingung, daß sie eine Nacht im Schlafgemache des Königs zubringen dürfe. Das gewährte die Königin, sie gab aber ihrem Sohne vorher einen Schlaftrunk, damit er von der Gegenwart der Zofe nichts bemerke. Das Mädchen suchte ihn durch Weinen und Wimmern, zuletzt durch Schütteln und Rütteln zu erwecken, es gelang ihr aber nicht, sondern er schlief bis zum vollen Tage, wo sie das Zimmer wieder verlassen mußte. Da wünschte sie sich ein prachtvolles Tuch mit Gold und Perlen besetzt, daß es wie die Sonne leuchtete; das zeigte sie wieder der Königin und schenkte es ihr unter der gleichen Bedingung. Diesmal aber nahm der König den Schlaftrunk nicht, weil ihm einer seiner Diener verrathen, was die Königin das vorige Mal gethan hatte. Wie nun das Mädchen wieder in seinem Zimmer weinte und wimmerte, erwachte er und erkannte sie wieder. Und nun erkannte er auch, daß, was er im Walde erlebt, kein Traum gewesen war, erinnerte sich seines Versprechens und nahm am andern Tage das Mädchen zu seiner Frau und Beide lebten glücklich mit einander.</p>
                    <closer>
                        <lb xml:id="tg877.2.4"/>
                        <seg type="closer" rend="zenoPLm4n0" xml:id="tg877.2.5">
                            <seg rend="zenoTXFontsize80" xml:id="tg877.2.5.1">Ein Seminarist in Neukloster.</seg>
                        </seg>
                    </closer>
                </div>
            </div>
        </body>
    </text>
</TEI>