<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg22" n="/Literatur/M/Lehnert, Johann Heinrich/Märchen/Mährchenkranz für Kinder/17. Fingerhütchen">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>17. Fingerhütchen</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Lehnert, Johann Heinrich: Element 00025 [2011/07/11 at 20:31:21]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Johann Heinrich Lehnert: Mährchenkranz für Kinder, der erheiternden Unterhaltung besonders im Familienkreise geweiht. Berlin: Hasselberg, [1829].</title>
                        <author>Lehnert, Johann Heinrich</author>
                    </titleStmt>
                    <extent>98-</extent>
                    <publicationStmt>
                        <date when="1829"/>
                        <pubPlace>Berlin</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1792" notAfter="1848"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg22.2">
                <div type="h4">
                    <head type="h4" xml:id="tg22.2.1">17. Fingerhütchen.</head>
                    <p xml:id="tg22.2.2">Es war einmal ein armer Mann, der hatte einen großen Höcker auf dem Rücken, und es sah gerade aus, als wäre sein Leib heraufgeschoben, und auf seine Schultern gelegt worden. Von der Wucht war ihm der Kopf so tief herabgedrückt, daß, wenn er saß, sein Kinn sich auf seine Knie zu stützen pflegte. Die Leute in der Gegend, wo er lebte, hatten Scheu, ihm an einem einsamen Orte zu begegnen, und doch war das arme Männchen so harmlos und friedliebend, wie ein neugebornes Kind. Aber seine Ungestaltheit war so groß, daß er kaum wie ein menschliches Geschöpf aussah. Doch besaß er große Geschicklichkeit, Hüte <pb n="98" xml:id="tg22.2.2.1"/>
und Körbe aus Stroh und Binsen zu flechten, auf welche Weise er sich auch sein Brot erwarb.</p>
                    <p rend="zenoPLm4n0" xml:id="tg22.2.3">
                        <hi rend="spaced" xml:id="tg22.2.3.1">Fingerhütchen</hi> war sein Spottname, weil er alle Zeit auf seinem kleinen Hut einen Zweig von dem rothen Fingerhut, oder dem Elsenkäppchen, trug. Für seine geflochtenen Arbeiten erhielt er einen Groschen mehr als Andere, und aus Neid darüber hatten einige boshafte Leute seltsame Geschichten von ihm verbreitet.</p>
                    <p rend="zenoPLm4n0" xml:id="tg22.2.4">Nun trug es sich zu, daß <hi rend="spaced" xml:id="tg22.2.4.1">Fingerhütchen</hi> eines Abends, als er aus der entfernt gelegenen Stadt nach Hause zurückkehrte, wegen seines lästigen Höckers auf dem Rücken, nicht weiter fort konnte, und sich müde und ermattet unter einen Riesenhügel (Hünengrab) niedersetzte, um ein wenig auszuruhen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg22.2.5">Als er so da saß, und ganz betrübt den Mond ansah, der eben silberrein aufstieg, drang auf einmal eine fremdartige, unterirdische Musik zu den Ohren des armen <hi rend="spaced" xml:id="tg22.2.5.1">Fingerhütchens.</hi> Er lauschte, und ihm däuchte, daß er niemals so etwas Entzückendes gehört habe. Es war wie der Klang vieler Stimmen, deren jede zu der andern sich fügte, und wunderbar einmischte, so daß es nur eine einzige zu seyn schien, während doch jede einen besondern Ton hielt. Die Worte des Gesanges waren diese: <hi rend="italic" xml:id="tg22.2.5.2">Da Luan, Da Mort, Da Luan, Da Mort, Da Luan, Da Mort.</hi> Darnach kam eine kleine Pause, worauf die Musik von vorne wieder anfing.</p>
                    <p rend="zenoPLm4n0" xml:id="tg22.2.6">
                        <hi rend="spaced" xml:id="tg22.2.6.1">Fingerhütchen</hi> horchte aufmerksam, und getraute kaum Athem zu schöpfen, damit ihm nicht der geringste Ton verloren ginge. Er merkte nun deutlich, daß der Gesang aus dem Hügel kam, und obgleich er anfangs sehr darüber erfreut war, so ward er es endlich doch müde, denselben Rundgesang in einem fort, ohne Abwechselung, anzuhören. Als abermals <hi rend="italic" xml:id="tg22.2.6.2">Da Luan, Da Mort</hi> drei Mal gesungen war, <pb n="99" xml:id="tg22.2.6.3"/>
benutzte er die kleine Pause, nahm die Melodie auf, und führte sie weiter fort, mit den Worten: augus Da Cadine! dann fiel er mit den Stimmen in dem Hügel ein, sang Da Luan, Da Mort, endigte aber bei der Pause mit seinem augus Da Cadine.</p>
                    <p rend="zenoPLm4n0" xml:id="tg22.2.7">Die Kleinen in dem Hügel, als sie den Zusatz zu ihrem Geistergesang vernahmen, ergötzten sich außerordentlich daran, und beschlossen sogleich, das Menschenkind hinunter zu holen, dessen musikalische Geschicklichkeit die ihrige so weit übertraf, und <hi rend="spaced" xml:id="tg22.2.7.1">Fingerhütchen</hi> ward mit der kreisenden Schnelligkeit des Wirbelwindes zu ihnen getragen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg22.2.8">Das war eine Pracht, die ihm in die Augen leuchtete, als er in den Hügel hinabkam, rund umher schwebend, leicht wie ein Strohhälmchen! und die lieblichste Musik hielt ordentlich Tact bei seiner Fahrt. Die größte Ehre wurde ihm aber erzeigt, als sie ihn über alle die Spielleute setzten. Er hatte Diener, die ihm aufwarten mußten, Alles, was sein Herz begehrte, wurde erfüllt, und er sah, wie gern ihn die Kleinen hatten; kurz, er wurde nicht anders behandelt, als wenn er der erste Mann im Lande wäre.</p>
                    <p rend="zenoPLm4n0" xml:id="tg22.2.9">Darauf bemerkte <hi rend="spaced" xml:id="tg22.2.9.1">Fingerhütchen,</hi> daß sie die Köpfe zusammensteckten, und heimlich mit einander rathschlagten, und so sehr ihm auch ihre Artigkeit gefiel, so fing er doch an, sich zu fürchten. Da trat einer der Kleinen zu ihm hervor, und sagte:</p>
                    <lb xml:id="tg22.2.10"/>
                    <milestone unit="hi_start"/>
                    <p xml:id="tg22.2.11">
                        <seg rend="zenoTXFontsize80" xml:id="tg22.2.11.1">»<hi rend="spaced" xml:id="tg22.2.11.1.1">Fingerhut, Fingerhut,</hi>
                        </seg>
                    </p>
                    <p xml:id="tg22.2.12">
                        <seg rend="zenoTXFontsize80" xml:id="tg22.2.12.1">Faß dir frischen Muth!</seg>
                    </p>
                    <p xml:id="tg22.2.13">
                        <seg rend="zenoTXFontsize80" xml:id="tg22.2.13.1">Lustig und munter,</seg>
                    </p>
                    <p xml:id="tg22.2.14">
                        <seg rend="zenoTXFontsize80" xml:id="tg22.2.14.1">Dein Höcker fällt herunter,</seg>
                    </p>
                    <p xml:id="tg22.2.15">
                        <seg rend="zenoTXFontsize80" xml:id="tg22.2.15.1">Siehst ihn liegen, dir geht's gut,</seg>
                    </p>
                    <p xml:id="tg22.2.16">
                        <seg rend="zenoTXFontsize80" xml:id="tg22.2.16.1">
                            <hi rend="spaced" xml:id="tg22.2.16.1.1">Fingerhut, Fingerhut!</hi>«</seg>
                    </p>
                    <milestone unit="hi_end"/>
                    <lb xml:id="tg22.2.17"/>
                    <p rend="zenoPLm4n0" xml:id="tg22.2.18">Kaum waren die Worte zu Ende, so fühlte sich das<hi rend="spaced" xml:id="tg22.2.18.1"> Fingerhütchen</hi> so leicht, so selig, daß es wohl in einem Satz über den Mond weggesprungen wäre. Er sah mit <pb n="100" xml:id="tg22.2.18.2"/>
der größten Freude von der Welt den Höcker von seinen Schultern herab auf den Boden rollen. Er versuchte darauf, ob er seinen Kopf in die Höhe heben könnte, und es ging vortrefflich. Nun schaute er rings herum mit der größten Bewunderung, und ergötzte sich an all den Dingen, die ihm immer schöner vorkamen. Zuletzt ward er so überwältigt von der Betrachtung des glänzenden Aufenthalts, daß ihm der Kopf schwindelte, die Augen geblendet wurden, und er in einen tiefen Schlaf verfiel.</p>
                    <p rend="zenoPLm4n0" xml:id="tg22.2.19">Als er erwachte, war es heller Tag geworden. Die Sonne schien hell, die Vögel sangen, und er lag gerade an dem Fuße des Riesenhügels, während Kühe und Schaafe friedlich um ihn her weideten. Nachdem<hi rend="spaced" xml:id="tg22.2.19.1"> Fingerhütchen</hi> sein Gebet gesagt hatte, war sein erstes Geschäft, mit der Hand nach seinem Höcker zu greifen, aber es war auf dem Rücken keine Spur davon zu finden, und er betrachtete sich nicht ohne Stolz: denn es war aus ihm ein wohlgestalteter, behender Bursche geworden; zugleich sah er sich auch von Kopf bis zu den Füßen in neuen Kleidern, und merkte wohl, daß die kleinen Leute ihm diesen Anzug besorgt hatten.</p>
                    <p rend="zenoPLm4n0" xml:id="tg22.2.20">Nun machte er sich auf den Weg, und ging so tapfer daher, und sprang bei jedem Schritte, als wenn er es sein Lebtage nicht anders gewohnt gewesen wäre. Niemand, der ihm begegnete, erkannte <hi rend="spaced" xml:id="tg22.2.20.1">Fingerhütchen</hi> ohne den Höcker, und er hatte große Mühe, die Leute zu überreden, daß er es wirklich wäre, und in der That, seinem jetzigen Aussehen nach, war er es auch nicht mehr.</p>
                    <p rend="zenoPLm4n0" xml:id="tg22.2.21">Bald wurde aber die Geschichte von <hi rend="spaced" xml:id="tg22.2.21.1">Fingerhütchens</hi> Höcker überall bekannt, und meilenweit in der Gegend redete Jedermann, vornehm oder gering, von nichts, als von dieser Begebenheit.</p>
                    <p rend="zenoPLm4n0" xml:id="tg22.2.22">Eines Morgens saß <hi rend="spaced" xml:id="tg22.2.22.1">Fingerhütchen</hi> vor seiner Hausthüre, <pb n="101" xml:id="tg22.2.22.2"/>
und war guter Dinge. Da trat eine alte Frau zu ihm, und sagte: »Zeigt mir doch den Weg nach <hi rend="spaced" xml:id="tg22.2.22.3">Cappagh.</hi>«</p>
                    <p rend="zenoPLm4n0" xml:id="tg22.2.23">»Ist nicht nöthig, liebe Frau,« antwortete er, »denn das ist hier <hi rend="spaced" xml:id="tg22.2.23.1">Cappagh;</hi> aber wo kommt ihr her?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg22.2.24">»Ich komme mehrere Meilen weit her,« sagte die alte Frau, »und suche einen Mann, der <hi rend="spaced" xml:id="tg22.2.24.1">Fingerhütchen</hi> genannt wird, und dem die Zwerge einen Höcker von der Schulter sollen genommen haben. Da ist der Sohn meiner Gevatterinn, der hat einen Höcker auf sich sitzen, der ihn noch todt drücken wird; vielleicht würde er davon erlöst, wenn er, wie <hi rend="spaced" xml:id="tg22.2.24.2">Finger </hi>
                        <hi rend="spaced" xml:id="tg22.2.24.3">hütchen,</hi> ein Zaubermittel anwenden könnte. Nun stellt Ihr euch leicht vor, warum ich so weit hergekommen bin; ich möchte, wenn's möglich wäre, etwas von dem Zaubermittel erfahren.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg22.2.25">
                        <hi rend="spaced" xml:id="tg22.2.25.1">Fingerhütchen,</hi> das immer gutmüthig gewesen war, erzählte der alten Frau Alles umständlich, wie er an dem Riesenhügel gesessen, dort den unterirdischen Gesang gehört, und ihn fortgeführt, wie dann die Zwerglein unter dem Hügel den Höcker von seinen Schultern weggenommen, und wie sie ihm einen neuen Anzug von Kopf bis zu den Füßen noch obendrein gegeben hatten.</p>
                    <p rend="zenoPLm4n0" xml:id="tg22.2.26">Die alte Frau dankte tausendmal, und machte sich wieder auf den Heimweg, zufrieden gestellt und ganz glücklich in ihren Gedanken. Als sie bei ihrer Gevatterinn angelangt war, erzählte sie genau, was sie von<hi rend="spaced" xml:id="tg22.2.26.1"> Fingerhütchen</hi> erfahren hatte. Danach setzte sie den kleinen buckelichen Kerl, der sein Lebelang ein heimtückisches, hämisches Herz gehabt hatte, auf einen Wagen, und zog ihn fort. Es war ein langer Weg, und es wurde ihr recht sauer; aber was thut das, dachte sie, wenn er nur den Höcker los wird. – Sie ließ sich daher keine Mühe und Anstrengung verdrießen, und eben als die Nacht einbrach, langte sie bei dem Riesenhügel an, und legte ihn dabei nieder.</p>
                    <p rend="zenoPLm4n0" xml:id="tg22.2.27">
                        <hi rend="spaced" xml:id="tg22.2.27.1">
                            <pb n="102" xml:id="tg22.2.27.1.1"/>
Hans Madden,</hi> denn das war der Name des Buckelichen, hatte noch gar nicht lange gesessen, so hub schon die Musik im Hügel an, noch viel lieblicher als je, denn die Zwerglein sangen ihr Lied mit dem Zusatz, den sie von <hi rend="spaced" xml:id="tg22.2.27.2">Fingerhütchen</hi> gelernt hatten: Da Luan, Da Mort, Da Luan, Da Mort, Da Luan, Da Mort, augus Da Cadine, ohne Unterbrechung<ref type="noteAnchor" target="#tg22.2.40">
                            <anchor xml:id="tg22.2.27.3" n="/Literatur/M/Lehnert, Johann Heinrich/Märchen/Mährchenkranz für Kinder/17. Fingerhütchen#Fußnote_1"/>
                            <ptr cRef="/Literatur/M/Lehnert, Johann Heinrich/Märchen/Mährchenkranz für Kinder/17. Fingerhütchen#Fußnoten_1"/>
                            <hi rend="superscript" xml:id="tg22.2.27.4.1">1</hi>
                        </ref>. <hi rend="spaced" xml:id="tg22.2.27.5">Hans,</hi> der nur geschwind seinen Höcker los seyn wollte, wartete nicht, bis der Gesang zu Ende war, achtete auch nicht auf einen schicklichen Augenblick, um die Melodie weiter, als <hi rend="spaced" xml:id="tg22.2.27.6">Fingerhütchen</hi> fortzuführen, sondern als sie ihr Lied mehr als siebenmal in einem fort gesungen hatten, so schrie er, ohne Rücksicht auf Tact und Weise der Melodie, und wie er seine Worte passend anbringen könnte, aus vollem Halse: augus Da Dardine, augus Da Hena! und dachte: »war <hi rend="spaced" xml:id="tg22.2.27.7">ein</hi> Zusatz gut, so sind zwei noch besser, und hat <hi rend="spaced" xml:id="tg22.2.27.8">Fingerhütchen</hi> einen neuen Anzug erhalten, so werden sie mir wohl zwei geben.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg22.2.28">Kaum waren diese Worte über seine Lippen gekommen, so ward er aufgehoben, und mit wunderbarer Gewalt in den Hügel hineingetragen. Hier umringten ihn die Zwerglein, waren sehr böse, und riefen schreiend und kreischend: »Wer hat unsern Gesang geschändet? Wer hat unsern Gesang geschändet?« Einer trat hervor, und sprach zu ihm:</p>
                    <lb xml:id="tg22.2.29"/>
                    <milestone unit="hi_start"/>
                    <p xml:id="tg22.2.30">
                        <seg rend="zenoTXFontsize80" xml:id="tg22.2.30.1">»<hi rend="spaced" xml:id="tg22.2.30.1.1">Hans Madden, Hans Madden,</hi>
                        </seg>
                    </p>
                    <p xml:id="tg22.2.31">
                        <seg rend="zenoTXFontsize80" xml:id="tg22.2.31.1">Deine Worte schlecht klangen,</seg>
                    </p>
                    <p xml:id="tg22.2.32">
                        <seg rend="zenoTXFontsize80" xml:id="tg22.2.32.1">So lieblich wir sangen!</seg>
                    </p>
                    <p xml:id="tg22.2.33">
                        <seg rend="zenoTXFontsize80" xml:id="tg22.2.33.1">Hier bist du gefangen,</seg>
                    </p>
                    <p xml:id="tg22.2.34">
                        <seg rend="zenoTXFontsize80" xml:id="tg22.2.34.1">Was wirst du erlangen?</seg>
                    </p>
                    <p xml:id="tg22.2.35">
                        <seg rend="zenoTXFontsize80" xml:id="tg22.2.35.1">Zwei Höcker für einen! <hi rend="spaced" xml:id="tg22.2.35.1.1">Hans Madden!</hi>«</seg>
                    </p>
                    <milestone unit="hi_end"/>
                    <lb xml:id="tg22.2.36"/>
                    <p rend="zenoPLm4n0" xml:id="tg22.2.37">Und zwanzig von den stärksten Zwergen schleppten <hi rend="spaced" xml:id="tg22.2.37.1">Fingerhütchens</hi> Höcker herbei, und setzten ihn oben auf den <pb n="103" xml:id="tg22.2.37.2"/>
Buckel des unglückseligen <hi rend="spaced" xml:id="tg22.2.37.3">Hans Madden,</hi> und da saß er so fest, als ob er von dem besten Zimmermann aufgenagelt wäre. Darnach stießen sie ihn mit den Füßen aus ihrer Wohnung, und am Morgen, als <hi rend="spaced" xml:id="tg22.2.37.4">Hans Maddens</hi> Mutter und ihre Gevatterinn kamen, nach dem kleinen Kerl zu sehen, so fanden sie ihn an dem Fuß des Hügels liegen, halb todt, mit einem zweiten Höcker auf seinem Rücken. Sie betrachteten ihn, eine nach der andern, aber es blieb dabei; am Ende ward ihnen angst, es könnte ihnen auch ein Höcker auf den Rücken gesetzt werden. Sie brachten den armseligen <hi rend="spaced" xml:id="tg22.2.37.5">Hans</hi> wieder heim, so betrübt im Herzen, und so jämmerlich anzusehen, als noch je ein Paar alte Weiber.</p>
                    <p rend="zenoPLm4n0" xml:id="tg22.2.38">
                        <hi rend="spaced" xml:id="tg22.2.38.1">Hans,</hi> durch das Gewicht des zweiten Höckers, und die lange Fahrt erschöpft, starb bald hernach.</p>
                </div>
                <div type="footnotes">
                    <head type="h4" xml:id="tg22.2.39">Fußnoten</head>
                    <note xml:id="tg22.2.40.note" target="#tg22.2.27.3">
                        <p xml:id="tg22.2.40">
                            <anchor xml:id="tg22.2.40.1" n="/Literatur/M/Lehnert, Johann Heinrich/Märchen/Mährchenkranz für Kinder/17. Fingerhütchen#Fußnoten_1"/>
                            <ref cRef="/Literatur/M/Lehnert, Johann Heinrich/Märchen/Mährchenkranz für Kinder/17. Fingerhütchen#Fußnote_1" xml:id="tg22.2.40.2">1</ref> Die Worte des einfachen Gesanges bezeichnen die Wochentage: Montag, Dienstag und Mittwoch.</p>
                    </note>
                </div>
            </div>
        </body>
    </text>
</TEI>