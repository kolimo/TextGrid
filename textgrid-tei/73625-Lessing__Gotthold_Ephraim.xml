<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg909" n="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Rezensionen/Zimmermann, Das Leben des Herrn Haller">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Zimmermann, Das Leben des Herrn Haller</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Lessing, Gotthold Ephraim: Element 00490 [2011/07/11 at 20:32:29]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <notesStmt>
                <note>
                                 Erstdruck in: Berlinische privilegierte Zeitung, 59. Stück, 17.5.1755.
                              </note>
            </notesStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Gotthold Ephraim Lessing: Werke. Herausgegeben von Herbert G. Göpfert in Zusammenarbeit mit Karl Eibl, Helmut Göbel, Karl S. Guthke, Gerd Hillen, Albert von Schirmding und Jörg Schönert, Band 1–8, München: Hanser, 1970 ff.</title>
                        <author key="pnd:118572121">Lessing, Gotthold Ephraim</author>
                    </titleStmt>
                    <extent>247-</extent>
                    <publicationStmt>
                        <date when="1970"/>
                        <pubPlace>München</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1729" notAfter="1781"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg909.3">
                <div type="h4">
                    <head type="h4" xml:id="tg909.3.1">Zimmermann, Das Leben des Herrn Haller</head>
                    <p xml:id="tg909.3.2">
                        <hi rend="italic" xml:id="tg909.3.2.1">Das Leben des Herrn von Haller, von D. Johann Georg Zimmermann, Stadtphysicus in Brugg. Zürich bei Heidegger und Compagnie 1755. In 8vo. 1 Alphb. 7 Bogen</hi>. Der Herr von Haller gehört unter die glücklichen Gelehrten, welche schon bei ihrem Leben eines ausgebreitetern Ruhms genießen, als nur wenige erst nach ihrem Tode teilhaft werden. Dieses Vorzugs hat er sich unwidersprechlich durch überwiegende Verdienste würdig gemacht, die ihn auch noch bei der spätesten Nachwelt eben so groß erhalten werden, als er jetzt in unparteiischen Augen scheinen muß. Sein Leben beschreiben heißt nicht, einen bloßen Dichter, oder einen bloßen Zergliedrer, oder einen bloßen Kräuterkundigen, sondern einen Mann zum Muster aufstellen,</p>
                    <lb xml:id="tg909.3.3"/>
                    <lg>
                        <l xml:id="tg909.3.4">––––– whose Mind</l>
                        <l xml:id="tg909.3.5">Contains a world, and seems for all things fram'd.</l>
                    </lg>
                    <lb xml:id="tg909.3.6"/>
                    <p xml:id="tg909.3.7">Man ist daher dem Herrn D. Zimmermann alle Erkenntlichkeit schuldig, daß er uns die nähere Nachrichten nicht vorenthalten wollen, die er, als ein vertrauter Schüler des Herrn von Haller, am zuverlässigsten von ihm haben konnte. Alle die, welche überzeugt sind, daß die Ehre des deutschen Namens am meisten auf der Ehre der deutschen Geister beruhe, werden ihn mit Vergnügen lesen, und nur diejenigen werden eine höhnische Miene machen, welchen alle Ehrenbezeigungen unnütz verschwendet zu sein scheinen, die ihnen nicht widerfahren. Ein Auszug aus dieser Lebensbeschreibung würde uns leichter fallen, als er dem Leser vielleicht in der Kürze, welche wir dabei beobachten müßten, angenehm sein würde. Der Herr D. Zimmermann ist keiner von den trocknen Biographen, die ihr Augenmerk auf nichts höhers als auf kleine chronologische Umstände richten, und uns<pb n="247" xml:id="tg909.3.7.1"/>
 einen Gelehrten genugsam bekannt zu machen glauben, wenn sie die Jahre seiner Geburt, seiner Beförderungen, seiner ehelichen Verbindungen und dergleichen angeben. Er folgt seinem Helden nicht nur durch alle die merkwürdigsten Veränderungen seines Lebens, sondern auch durch alle die Wissenschaften, in denen er sich gezeigt, und durch alle die Anstalten, die er zur Aufnahme derselben an mehr als einem Orte gemacht hat. Dabei erhebt er sich zwar über den Ton eines kalten Geschichtschreibers; allein von der Hitze eines schwärmerischen Panegyristen bleibt er doch noch weit genug entfernt, als daß man bei seiner Erzählung freundschaftliche Verblendungen besorgen dürfte. Kostet in den Vossischen Buchläden hier und in Potsdam auf Druckpapier 16 Gr. und auf Schreibpapier 1 Rtlr.</p>
                    <pb n="248" xml:id="tg909.3.8"/>
                </div>
            </div>
        </body>
    </text>
</TEI>