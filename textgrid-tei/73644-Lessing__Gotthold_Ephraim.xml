<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg960" n="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Gleim, Preußische Kriegslieder">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Gleim, Preußische Kriegslieder</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Lessing, Gotthold Ephraim: Element 00510 [2011/07/11 at 20:32:29]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <notesStmt>
                <note>
                                 Erstdruck in: [Johann Wilhelm Ludwig Gleim:] Preußische Kriegslieder in den Feldzügen 1756 und 1757 von einem Grenadier. Mit Melodien, Berlin (Voss) 1758.
                              </note>
            </notesStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Gotthold Ephraim Lessing: Werke. Herausgegeben von Herbert G. Göpfert in Zusammenarbeit mit Karl Eibl, Helmut Göbel, Karl S. Guthke, Gerd Hillen, Albert von Schirmding und Jörg Schönert, Band 1–8, München: Hanser, 1970 ff.</title>
                        <author key="pnd:118572121">Lessing, Gotthold Ephraim</author>
                    </titleStmt>
                    <extent>545-</extent>
                    <publicationStmt>
                        <date when="1970"/>
                        <pubPlace>München</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1729" notAfter="1781"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg960.3">
                <pb n="545" xml:id="tg960.3.1"/>
                <pb type="start" n="15" xml:id="tg960.3.2"/>
                <milestone unit="sigel" n="Lessing-W Bd. 5" xml:id="tg960.3.3"/>
                <div type="h4">
                    <head type="h4" xml:id="tg960.3.4">[Johann Wilhelm Ludwig Gleim:]</head>
                    <head type="h4" xml:id="tg960.3.5">Preußische Kriegslieder</head>
                    <head type="h4" xml:id="tg960.3.6">in den Feldzügen 1756 und 1757</head>
                    <head type="h4" xml:id="tg960.3.7">von einem Grenadier</head>
                    <milestone unit="hi_start"/>
                    <p rend="zenoPC" xml:id="tg960.3.8">Mit Melodien</p>
                    <milestone unit="hi_end"/>
                    <lb xml:id="tg960.3.9"/>
                </div>
                <div type="h4">
                    <head type="h4" xml:id="tg960.3.10">Vorbericht</head>
                    <p xml:id="tg960.3.11">Die Welt kennet bereits einen Teil von diesen Liedern; und die <hi rend="italic" xml:id="tg960.3.11.1">feinern</hi> Leser haben so viel Geschmack daran gefunden, daß ihnen eine vollständige und verbesserte Sammlung derselben, ein angenehmes Geschenk sein muß.</p>
                    <p rend="zenoPLm4n0" xml:id="tg960.3.12">Der Verfasser ist ein gemeiner Soldat, dem eben so viel Heldenmut als poetisches Genie zu Teil geworden. Mehr aber unter den Waffen, als in der Schule erzogen, scheinet er sich eher eine eigene Gattung von Ode gemacht, als in dem Geiste irgend einer schon bekannten gedichtet zu haben.</p>
                    <p rend="zenoPLm4n0" xml:id="tg960.3.13">Wenigstens, wenn er sich ein deutscher Horaz zu werden wünschet, kann er nur den Ruhm des Römers, als ein lyrischer Dichter überhaupt, im Sinne gehabt haben. Denn die charakteristischen Schönheiten des Horaz, setzen den feinsten Hofmann voraus; und wie weit ist dieser von einem ungekünstelten Krieger un <pb type="start" n="15" xml:id="tg960.3.13.1"/>
terschieden!</p>
                    <p rend="zenoPLm4n0" xml:id="tg960.3.14">Auch mit dem Pindar hat er weiter nichts gemein, als das anhaltende Feuer, und die Ύπερβατα der Wortfügung.</p>
                    <p rend="zenoPLm4n0" xml:id="tg960.3.15">Von dem einzigen Tyrtäus könnte er die heroischen Gesinnungen, den Geiz nach Gefahren, den Stolz für das Vaterland zu sterben, erlernt haben, wenn sie einem Preußen nicht eben so natürlich wären, als einem Spartaner.</p>
                    <p rend="zenoPLm4n0" xml:id="tg960.3.16">
                        <pb n="15" xml:id="tg960.3.16.1"/>
Und dieser Heroismus ist die ganze Begeisterung unsers Dichters. Es ist aber eine sehr <hi rend="italic" xml:id="tg960.3.16.2">gehorsame</hi> Begeisterung, die sich nicht durch wilde Sprünge und Ausschweifungen zeigt, sondern die wahre Ordnung der Begebenheiten zu der Ordnung ihrer Empfindungen und Bilder macht.</p>
                    <p rend="zenoPLm4n0" xml:id="tg960.3.17">Alle seine Bilder sind erhaben, und alle sein Erhabnes ist naiv. Von dem poetischen Pompe weiß er nichts; und prahlen und schimmern scheint er, weder als Dichter noch als Soldat zu wollen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg960.3.18">Sein Flug aber hält nie einerlei Höhe. Eben der Adler, der vor in die Sonne sah, läßt sich nun tief herab, auf der Erde sein Futter zu suchen; und das ohne Beschädigung seiner Würde. Antäus, um neue Kräfte zu sammeln, mußte mit dem Fuße den Boden berühren können.</p>
                    <p rend="zenoPLm4n0" xml:id="tg960.3.19">Sein Ton überhaupt, ist ernsthaft. Nur da blieb er nicht ernsthaft – wo es niemand bleiben kann. Denn was erweckt das Lachen unfehlbarer, als große mächtige Anstalten mit einer kleinen, kleinen Wirkung? Ich rede von den drolligten Gemälden des Roßbachischen Liedes.</p>
                    <p rend="zenoPLm4n0" xml:id="tg960.3.20">Seine Sprache ist älter, als die Sprache der jetztlebenden größern Welt und ihrer Schriftsteller. Denn der Landmann, der Bürger, der Soldat und alle die niedrigern Stände, die wir <hi rend="italic" xml:id="tg960.3.20.1">das Volk</hi> nennen, bleiben in den Feinheiten der Rede immer, wenigstens ein halb Jahrhundert, zurück.</p>
                    <p rend="zenoPLm4n0" xml:id="tg960.3.21">Auch seine Art zu reimen, und jede Zeile mit einer männlichen Sylbe zu schließen, ist alt. In seinen Liedern aber erhält sie noch diesen Vorzug, daß man in dem durchgängig männlichen Reime, etwas dem kurzen Absetzen der kriegerischen Trommete ähnliches zu hören glaubet.</p>
                    <p rend="zenoPLm4n0" xml:id="tg960.3.22">Nach diesen Eigenschaften also, wenn ich unsern Grenadier ja mit Dichtern aus dem Altertume vergleichen sollte, so müßten es unsere <hi rend="italic" xml:id="tg960.3.22.1">Barden</hi> sein.</p>
                    <lb xml:id="tg960.3.23"/>
                    <lg>
                        <l xml:id="tg960.3.24">Vos quoque, qui fortes animas belloque peremtas</l>
                        <l xml:id="tg960.3.25">Laudibus in longum vates dimittitis aevum,</l>
                        <l xml:id="tg960.3.26">Plurima securi fudistis carmina Bardi.<ref type="noteAnchor" target="#tg962.3.34">
                                <anchor xml:id="tg960.3.26.1" n="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Gleim, Preußische Kriegslieder#Fußnote_3"/>
                                <ptr cRef="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Das Theater des Herrn Diderot#Fußnoten_3"/>
                                <hi rend="superscript" xml:id="tg960.3.26.2.1">3</hi>
                            </ref>
                        </l>
                    </lg>
                    <lb xml:id="tg960.3.27"/>
                    <p xml:id="tg960.3.28">
                        <pb n="16" xml:id="tg960.3.28.1"/>
Karl der große hatte ihre Lieder, so viel es damals noch möglich war, gesammelt, und sie waren die unschätzbarste Zierde seines Büchersaals. Aber woran dachte dieser große Beförderer der Gelehrsamkeit, als er alle seine Bücher, und also auch diese Lieder, nach seinem Tode an den Meistbietenden zu verkaufen befahl? Konnte ein römischer Kaiser der Armut kein ander Vermächtnis hinterlassen?<ref type="noteAnchor" target="#tg962.3.36">
                            <anchor xml:id="tg960.3.28.2" n="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Gleim, Preußische Kriegslieder#Fußnote_4"/>
                            <ptr cRef="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Das Theater des Herrn Diderot#Fußnoten_4"/>
                            <hi rend="superscript" xml:id="tg960.3.28.3.1">4</hi>
                        </ref> – O wenn sie noch vorhanden wären! Welcher Deutsche würde sich nicht, noch zu weit mehrerm darum verstehen, als Hickes?<ref type="noteAnchor" target="#tg962.3.38">
                            <anchor xml:id="tg960.3.28.4" n="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Gleim, Preußische Kriegslieder#Fußnote_5"/>
                            <ptr cRef="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Das Theater des Herrn Diderot#Fußnoten_5"/>
                            <hi rend="superscript" xml:id="tg960.3.28.5.1">5</hi>
                        </ref>
                    </p>
                    <p rend="zenoPLm4n0" xml:id="tg960.3.29">Über die Gesänge der nordischern <hi rend="italic" xml:id="tg960.3.29.1">Skalden</hi> scheinet ein günstiger Geschick gewacht zu haben. Doch die<hi rend="italic" xml:id="tg960.3.29.2"> Skalden</hi> waren die Brüder der <hi rend="italic" xml:id="tg960.3.29.3">Barden</hi>; und was von jenen wahr ist, muß auch von diesen gelten. Beide folgten ihren Herzogen und Königen in den Krieg, und waren Augenzeugen von den Taten ihres Volks. Selbst aus der Schlacht blieben sie nicht; die tapfersten und ältesten Krieger schlossen einen Kreis um sie, und waren verbunden sie überall hinzubegleiten, wo sie den würdigsten Stoff ihrer künftigen Lieder vermuteten. Sie waren Dichter und Geschichtschreiber zugleich; wahre Dichter, feurige Geschichtschreiber. Welcher Held von ihnen bemerkt zu werden das Glück hatte, dessen Name war unsterblich; so unsterblich, als die Schande des Feindes, den sie fliehen sahen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg960.3.30">Hat man sich nun in den kostbaren Überbleibseln dieser uralten nordischen Heldendichter, wie sie uns einige dänische Gelehrte aufbehalten haben,<ref type="noteAnchor" target="#tg962.3.40">
                            <anchor xml:id="tg960.3.30.1" n="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Gleim, Preußische Kriegslieder#Fußnote_6"/>
                            <ptr cRef="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Das Theater des Herrn Diderot#Fußnoten_6"/>
                            <hi rend="superscript" xml:id="tg960.3.30.2.1">6</hi>
                        </ref>umgesehen, und sich mit ihrem Geiste und ihren Absichten bekannt gemacht; hat man zugleich das jüngere Geschlecht von <hi rend="italic" xml:id="tg960.3.30.3">Barden</hi> aus dem schwäbischen Zeitalter, seiner Aufmerksamkeit wert geschätzt, und <pb n="17" xml:id="tg960.3.30.4"/>
ihre naive Sprache, ihre ursprünglich deutsche Denkungsart studiert: so ist man einigermaßen fähig über unsern neuen preußischen <hi rend="italic" xml:id="tg960.3.30.5">Barden</hi> zu urteilen. Andere Beurteiler, besonders wenn sie von derjenigen Klasse sind, welchen die französische Poesie alles in allem ist, wollte ich wohl für ihn verbeten haben.</p>
                    <p rend="zenoPLm4n0" xml:id="tg960.3.31">Noch besitze ich ein ganz kleines Lied von ihm, welches in der Sammlung keinen Platz finden konnte; ich werde wohl tun, wenn ich diesen kurzen Vorbericht damit bereichere. Er schrieb mir aus dem Lager vor Prag: »Die Panduren lägen nahe an den Werken der Stadt, in den Höhlen der Weinberge; als er einen gesehen, habe er nach ihn hingesungen:«</p>
                    <lb xml:id="tg960.3.32"/>
                    <lg>
                        <l xml:id="tg960.3.33">Was liegst du, nackender Pandur!</l>
                        <l xml:id="tg960.3.34">Recht wie ein Hund im Loch?</l>
                        <l xml:id="tg960.3.35">Und weisest deine Zähne nur?</l>
                        <l xml:id="tg960.3.36">Und bellst? So beiße doch!</l>
                    </lg>
                    <lb xml:id="tg960.3.37"/>
                    <p xml:id="tg960.3.38">Es könnte ein Herausfordrungslied zum Zweikampf mit einem Panduren heißen.</p>
                    <lb xml:id="tg960.3.39"/>
                    <p xml:id="tg960.3.40">Ich hoffe übrigens, daß er noch nicht das letzte Siegeslied soll gesungen haben. Zwar falle er bald oder spät; seine Grabschrift ist fertig:</p>
                    <lb xml:id="tg960.3.41"/>
                    <lg>
                        <l xml:id="tg960.3.42">Ειμι δ' εγω ϑεραπων μεν Ενυαλιοιο ανακτος</l>
                        <l xml:id="tg960.3.43">Και Μουσεων ερατον δωρον επισαμενος.</l>
                    </lg>
                    <lb xml:id="tg960.3.44"/>
                    <pb n="18" xml:id="tg960.3.45"/>
                </div>
            </div>
        </body>
    </text>
</TEI>