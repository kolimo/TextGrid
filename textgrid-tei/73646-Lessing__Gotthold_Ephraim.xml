<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg962" n="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Das Theater des Herrn Diderot">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Das Theater des Herrn Diderot</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Lessing, Gotthold Ephraim: Element 00512 [2011/07/11 at 20:32:29]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <notesStmt>
                <note>
                                 Erstdruck in: Das Theater des Herrn Diderot, Berlin (Voss) 1760, bzw. in der zweiten, verbesserten Ausgabe, Berlin (Voss) 1781.
                              </note>
            </notesStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Gotthold Ephraim Lessing: Werke. Herausgegeben von Herbert G. Göpfert in Zusammenarbeit mit Karl Eibl, Helmut Göbel, Karl S. Guthke, Gerd Hillen, Albert von Schirmding und Jörg Schönert, Band 1–8, München: Hanser, 1970 ff.</title>
                        <author key="pnd:118572121">Lessing, Gotthold Ephraim</author>
                    </titleStmt>
                    <extent>352-</extent>
                    <publicationStmt>
                        <date when="1970"/>
                        <pubPlace>München</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1729" notAfter="1781"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg962.3">
                <pb n="352" xml:id="tg962.3.1"/>
                <pb type="start" n="148" xml:id="tg962.3.2"/>
                <milestone unit="sigel" n="Lessing-W Bd. 4" xml:id="tg962.3.3"/>
                <div type="h4">
                    <head type="h4" xml:id="tg962.3.4">Das Theater des Herrn Diderot</head>
                    <head type="h4" xml:id="tg962.3.5">Vorrede des Übersetzers</head>
                    <head type="h4" xml:id="tg962.3.6">[1760]</head>
                    <p xml:id="tg962.3.7">Dieses Theater des Herrn Diderot, eines von den vornehmsten Verfassern der berufenen Enzyklopädie, bestehet aus zwei Stücken, die er als Beispiele einer neuen Gattung ausgearbeitet, und mit seinen Gedanken sowohl über diese neue Gattung, als über andere wichtige Punkte der dramatischen Poesie, und aller ihr untergeordneten Künste, der Deklamation, der Pantomime, des Tanzes begleitet hat.</p>
                    <p rend="zenoPLm4n0" xml:id="tg962.3.8">Kenner werden in jenen weder Genie noch Geschmack vermissen; und in diesen überall den denkenden Kopf spüren, der die alten Wege weiter bahnet, und neue Pfade durch unbekannte Gegenden zeichnet.</p>
                    <p rend="zenoPLm4n0" xml:id="tg962.3.9">Ich möchte wohl sagen, daß sich, nach dem Aristoteles, kein philosophischerer Geist mit dem Theater abgegeben hat, als <hi rend="italic" xml:id="tg962.3.9.1">er</hi>.</p>
                    <p rend="zenoPLm4n0" xml:id="tg962.3.10">Daher sieht er auch die Bühne seiner Nation bei weitem auf der Stufe der Vollkommenheit nicht, auf welcher sie unter uns die schalen Köpfe erblicken, an deren Spitze der Prof. Gottsched ist. Er gestehet, daß ihre Dichter und Schauspieler noch weit von der <pb type="start" n="148" xml:id="tg962.3.10.1"/>
Natur und Wahrheit entfernet sind; daß beider ihre Talente, guten Teils, auf kleine Anständigkeiten, auf handwerksmäßigen Zwang, auf kalte Etiquette hinauslaufen etc.</p>
                    <p rend="zenoPLm4n0" xml:id="tg962.3.11">Selten genesen wir eher von der verächtlichen Nachahmung gewisser französischen Muster, als bis der Franzose selbst diese Muster zu verwerfen anfängt. Aber oft auch dann noch nicht.</p>
                    <p rend="zenoPLm4n0" xml:id="tg962.3.12">Es wird also darauf ankommen, ob der Mann, dem nichts angelegener ist, als das Genie in seine alte Rechte wieder einzusetzen, aus welchen es die mißverstandene Kunst verdränget; ob der Mann, der es zugestehet, daß das Theater weit <pb n="148" xml:id="tg962.3.12.1"/>
stärkerer Eindrücke fähig ist, als man von den berühmtesten Meisterstücken eines Corneille und Racine rühmen kann; ob dieser Mann bei uns mehr Gehör findet, als er bei seinen Landsleuten gefunden hat.</p>
                    <p rend="zenoPLm4n0" xml:id="tg962.3.13">Wenigstens muß es geschehen, wenn auch wir einst zu den gesitteten Völkern gehören wollen, deren jedes <hi rend="italic" xml:id="tg962.3.13.1">seine</hi> Bühne hatte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg962.3.14">Und ich will nicht bergen, daß ich mich einzig in solcher Hoffnung der Übersetzung dieses Werks unterzogen habe.</p>
                </div>
                <div type="h4">
                    <head type="h4" xml:id="tg962.3.15">Vorrede des Übersetzers,</head>
                    <head type="h4" xml:id="tg962.3.16">zu dieser zweiten Ausgabe</head>
                    <head type="h4" xml:id="tg962.3.17">[1781]</head>
                    <p xml:id="tg962.3.18">Ich bin ersucht worden, dieser Übersetzung öffentlich meinen Namen zu geben.</p>
                    <p rend="zenoPLm4n0" xml:id="tg962.3.19">Da es nun vorlängst unbekannt zu sein aufgehöret hat, daß ich wirklich der Verfasser derselben bin; da ich mich des Fleißes, den ich darauf gewandt habe, und des Nutzens, den ich daraus gezogen, noch immer mit Vergnügen erinnere: so sehe ich nicht, warum ich mich einer Anfoderung weigern sollte, die mir Gelegenheit gibt, meine Dankbarkeit einem Manne zu bezeugen, der an der Bildung meines Geschmacks so großen Anteil hat.</p>
                    <p rend="zenoPLm4n0" xml:id="tg962.3.20">Denn es mag mit diesem auch beschaffen sein, wie es will: so bin ich mir doch zuwohl bewußt, daß er, ohne Diderots Muster und Lehren, eine ganz andere Richtung würde bekommen haben. Vielleicht eine eigenere: aber doch schwerlich eine, mit der am Ende mein Verstand zufriedener gewesen wäre.</p>
                    <p rend="zenoPLm4n0" xml:id="tg962.3.21">Diderot scheint überhaupt auf das deutsche Theater weit mehr Einfluß gehabt zu haben, als auf das Theater seines eigenen Volks. Auch war die Veränderung, die er auf diesem hervorbringen wollte, in der Tat weit schwerer zu bewirken, als das Gute, welches er jenem nebenher verschaffte. Die Französischen Stücke, welche auf unserm Theater gespielt wurden, stellten doch nur lauter fremde Sitten vor: und <pb n="149" xml:id="tg962.3.21.1"/>
fremde Sitten, in welchen wir weder die allgemeine menschliche Natur, noch unsere besondere Volksnatur erkennen, sind bald verdrängt. Aber je mehr die Franzosen in ihren Stücken wirklich finden, was wir uns nur zu finden einbilden: desto hartnäckiger muß der Widerstand sein, den ihre alten Eindrücke jeder, wie sie dafür halten, unnötigen Bemühung, sie zu verwischen oder zu überstempeln, entgegensetzen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg962.3.22">Wir hingegen hatten es längst satt, nichts als einen alten Laffen im kurzen Mantel, und einen jungen Geck in bebänderten Hosen, unter ein Halbdutzend alltäglichen Personen, auf der Bühne herumtoben zu sehen; wir sehnten uns längst nach etwas bessern, ohne zu wissen, wo dieses Bessere herkommen sollte: als der »Hausvater« erschien. In ihm erkannte sogleich der rechtschaffne Mann, was ihm das Theater noch eins so teuer machen müsse. Sei immerhin wahr, daß es seitdem von dem Geräusche eines nichts bedeutenden Gelächters weniger ertönte! Das wahre Lächerliche ist nicht, was am lautesten lachen macht; und Ungereimtheiten sollen nicht bloß unsere Lunge in Bewegung setzen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg962.3.23">Selbst unsere Schauspieler fingen an dem »Hausvater« zuerst an, sich selbst zu übertreffen. Denn der Hausvater war weder Französisch, noch deutsch: er war bloß menschlich. Er hatte nichts auszudrücken, als was jeder ausdrücken konnte, der es verstand und fühlte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg962.3.24">Und daß jeder seine Rolle verstand und fühlte, dafür hatte nun freilich Diderot vornehmlich gesorgt. Wenn ich aber doch gleichwohl auch meiner Übersetzung ein kleines Verdienst in diesem Punkte zuschreibe: so habe ich, wenigstens bis itzt, von den Kunstrichtern noch keinen besondern Widerspruch zu erfahren gehabt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg962.3.25">Nicht als ob ich meine Übersetzung frei von allen Mängeln halten wollte; nicht als ob ich mir schmeichelte, überall, auch da den wahren Sinn des Verfassers getroffen zu haben, wo er selbst in seiner Sprache sich nicht bestimmt genug ausgedrückt hat! Ein Freund zeigt mir nur erst izt eine dergleichen Stelle; und ich betaure, daß ich in dem Texte von diesem Winke nicht Gebrauch machen können. Sie ist in <pb n="150" xml:id="tg962.3.25.1"/>
dem »Natürlichen Sohne« in dem dritten Auftritte des ersten Aufzuges, wo Theresia ihrer Sorgfalt um Rosaliens Erziehung gedenkt. »Ich ließ mir es angelegen sein, sagt sie, den Geist und besonders den Charakter dieses Kindes zu bilden, von welchem einst das Schicksal meines Bruders abhangen sollte. Es war unbesonnen, ich machte es bedächtig. Es war heftig, ich suchte dem Sanften seiner Natur aufzuhelfen.« Das es ist in allen vier Stellen im Französischen durch il ausgedruckt, welches eben sowohl auf das vorhergehende enfant, auf Rosalien, als auf den Bruder gehen kann. Ich habe es jedesmal auf Rosalien gezogen: aber es kann leicht sein, daß es die beiden erstenmale auf den Bruder gehen, und sonach heißen soll: »Er war unbesonnen, ich machte sie bedächtig. Er war heftig, ich suchte dem Sanften ihrer Natur aufzuhelfen.« Ja dieser Sinn ist unstreitig der feinere.</p>
                    <p rend="zenoPLm4n0" xml:id="tg962.3.26">Es kann jemand keinen einzigen solchen Fehler sich zu Schulden kommen lassen, und doch noch eine sehr mittelmäßige Übersetzung gemacht haben!</p>
                </div>
                <div type="footnotes">
                    <head type="h4" xml:id="tg962.3.29">Fußnoten</head>
                    <note xml:id="tg962.3.30.note" target="#tg954.2.10.7">
                        <p xml:id="tg962.3.30">
                            <anchor xml:id="tg962.3.30.1" n="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Das Theater des Herrn Diderot#Fußnoten_1"/>
                            <ref cRef="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Vermischte Schriften des Hrn. Christlob Mylius/Erster Brief. Vom 20. März 1754#Fußnote_1" xml:id="tg962.3.30.2">1</ref> Man sehe in diesen vermischten Schriften S. 146.</p>
                    </note>
                    <note xml:id="tg962.3.32.note" target="#tg959.2.3.1">
                        <p xml:id="tg962.3.32">
                            <anchor xml:id="tg962.3.32.1" n="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Das Theater des Herrn Diderot#Fußnoten_2"/>
                            <ref cRef="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Vermischte Schriften des Hrn. Christlob Mylius/Sechster Brief. Vom 20. Junius#Fußnote_2" xml:id="tg962.3.32.2">2</ref> Man sehe diese vermischten Schriften, Seite 280 u. folg.</p>
                    </note>
                    <note xml:id="tg962.3.34.note" target="#tg960.3.26.1">
                        <p xml:id="tg962.3.34">
                            <anchor xml:id="tg962.3.34.1" n="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Das Theater des Herrn Diderot#Fußnoten_3"/>
                            <ref cRef="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Gleim, Preußische Kriegslieder#Fußnote_3" xml:id="tg962.3.34.2">3</ref> Lucanus.</p>
                    </note>
                    <note xml:id="tg962.3.36.note" target="#tg960.3.28.2">
                        <p xml:id="tg962.3.36">
                            <anchor xml:id="tg962.3.36.1" n="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Das Theater des Herrn Diderot#Fußnoten_4"/>
                            <ref cRef="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Gleim, Preußische Kriegslieder#Fußnote_4" xml:id="tg962.3.36.2">4</ref>
                            <hi rend="italic" xml:id="tg962.3.36.3">Eginhartus in vita Caroli</hi> M. cap. 33. Similiter et de libris – statuit, ut ab his, qui eos habere vellent, justo pretio redimerentur, pretiumque in pauperes erogaretur.</p>
                    </note>
                    <note xml:id="tg962.3.38.note" target="#tg960.3.28.4">
                        <p xml:id="tg962.3.38">
                            <anchor xml:id="tg962.3.38.1" n="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Das Theater des Herrn Diderot#Fußnoten_5"/>
                            <ref cRef="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Gleim, Preußische Kriegslieder#Fußnote_5" xml:id="tg962.3.38.2">5</ref>
                            <hi rend="italic" xml:id="tg962.3.38.3">Georg. Hickesins in Grammatica Franco-Theodisca</hi> c. I. O utinam jam extaret augusta Caroli M. Bibliotheca, in qua delicias has suas reposuit Imperator! O quam lubens, quam jucundus ad extremos Caroli imperii fines proficiscerer, ad legenda antiqua illa, aut barbara carmina!</p>
                    </note>
                    <note xml:id="tg962.3.40.note" target="#tg960.3.30.1">
                        <p xml:id="tg962.3.40">
                            <anchor xml:id="tg962.3.40.1" n="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Das Theater des Herrn Diderot#Fußnoten_6"/>
                            <ref cRef="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Gleim, Preußische Kriegslieder#Fußnote_6" xml:id="tg962.3.40.2">6</ref> Andreas Vellejus und Petrus Septimus.</p>
                    </note>
                    <note xml:id="tg962.3.42.note" target="#tg961.3.37.1">
                        <p xml:id="tg962.3.42">
                            <anchor xml:id="tg962.3.42.1" n="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Das Theater des Herrn Diderot#Fußnoten_7"/>
                            <ref cRef="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Friedrichs von Logau Sinngedichte#Fußnote_7" xml:id="tg962.3.42.2">7</ref> Sinngedicht 257 und 398.</p>
                    </note>
                    <note xml:id="tg962.3.44.note" target="#tg961.3.37.4">
                        <p xml:id="tg962.3.44">
                            <anchor xml:id="tg962.3.44.1" n="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Das Theater des Herrn Diderot#Fußnoten_8"/>
                            <ref cRef="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Friedrichs von Logau Sinngedichte#Fußnote_8" xml:id="tg962.3.44.2">8</ref> Sinngedicht 257.</p>
                        <p xml:id="tg962.3.46">Die Musen wirkten zwar, durch kluge Dichtersinnen,</p>
                        <p xml:id="tg962.3.47">Daß Deutschland sollte Deutsch, und artlich reden können,</p>
                        <p xml:id="tg962.3.48">Mars aber schafft es ab, und hat es so geschickt,</p>
                        <p xml:id="tg962.3.49">Daß Deutschland ist blut arm, drum geht es so geflickt.</p>
                    </note>
                    <note xml:id="tg962.3.51.note" target="#tg961.3.37.6">
                        <p xml:id="tg962.3.51">
                            <anchor xml:id="tg962.3.51.1" n="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Das Theater des Herrn Diderot#Fußnoten_9"/>
                            <ref cRef="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Friedrichs von Logau Sinngedichte#Fußnote_9" xml:id="tg962.3.51.2">9</ref> Sinngedicht 1594.</p>
                        <p xml:id="tg962.3.53">Wer nicht Französisch kann,</p>
                        <p xml:id="tg962.3.54">Ist kein gerühmter Mann etc.</p>
                    </note>
                    <note xml:id="tg962.3.56.note" target="#tg961.3.39.3">
                        <p xml:id="tg962.3.56">
                            <anchor xml:id="tg962.3.56.1" n="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Das Theater des Herrn Diderot#Fußnoten_10"/>
                            <ref cRef="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Friedrichs von Logau Sinngedichte#Fußnote_10" xml:id="tg962.3.56.2">10</ref> In der Überschrift des 488ten Sinngedichtes.</p>
                    </note>
                    <note xml:id="tg962.3.58.note" target="#tg961.3.40.1">
                        <p xml:id="tg962.3.58">
                            <anchor xml:id="tg962.3.58.1" n="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Das Theater des Herrn Diderot#Fußnoten_11"/>
                            <ref cRef="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Friedrichs von Logau Sinngedichte#Fußnote_11" xml:id="tg962.3.58.2">11</ref> In der Vorrede zu dem ersten Tausend seiner Sinngedichte, wo er sagt, daß er sich bei prosaischem Gebrauche der unbestimmten einsylbichten Wörter, nach dem <hi rend="italic" xml:id="tg962.3.58.3">Beilaute</hi>, so wie dieser im Reden und Lesen jedesmal falle, gerichtet habe. Desgleichen Sinngedicht 1526.</p>
                        <p xml:id="tg962.3.60">Deutscher Reimkunst meistes Werk, steht im<hi rend="italic" xml:id="tg962.3.60.1"> Beylaut</hi>, oder Schalle;</p>
                        <p xml:id="tg962.3.61">Ob der Sylben Ausspruch kurz, lang, und wo er hin verfalle.</p>
                    </note>
                    <note xml:id="tg962.3.63.note" target="#tg961.3.41.2">
                        <p xml:id="tg962.3.63">
                            <anchor xml:id="tg962.3.63.1" n="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Das Theater des Herrn Diderot#Fußnoten_12"/>
                            <ref cRef="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Friedrichs von Logau Sinngedichte#Fußnote_12" xml:id="tg962.3.63.2">12</ref> Sinngedicht 2363.</p>
                        <p xml:id="tg962.3.65">Cynthia will ihren Mann, wenn sie stirbt, der Chloris geben;</p>
                        <p xml:id="tg962.3.66">Chloris will die Erbschaft nicht weiter und zuvor erheben,</p>
                        <p xml:id="tg962.3.67">Bis ein <hi rend="italic" xml:id="tg962.3.67.1">Fundregister</hi> da, (Seht mir an den klugen Rath!)</p>
                        <p xml:id="tg962.3.68">Bis zuvor sie sey gewiß, was für Kraft die Erbschaft hat.</p>
                        <p xml:id="tg962.3.70">Mehrere glücklich übersetzte Kunstwörter wird man in dem Wörterbuche selbst antreffen.</p>
                    </note>
                    <note xml:id="tg962.3.72.note" target="#tg961.3.43.1">
                        <p xml:id="tg962.3.72">
                            <anchor xml:id="tg962.3.72.1" n="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Das Theater des Herrn Diderot#Fußnoten_13"/>
                            <ref cRef="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Friedrichs von Logau Sinngedichte#Fußnote_13" xml:id="tg962.3.72.2">13</ref> Sinngedicht 1747.</p>
                    </note>
                    <note xml:id="tg962.3.74.note" target="#tg961.3.52.3">
                        <p xml:id="tg962.3.74">
                            <anchor xml:id="tg962.3.74.1" n="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Das Theater des Herrn Diderot#Fußnoten_14"/>
                            <ref cRef="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Friedrichs von Logau Sinngedichte#Fußnote_14" xml:id="tg962.3.74.2">14</ref> (IV. 51)</p>
                    </note>
                    <note xml:id="tg962.3.76.note" target="#tg961.3.59.1">
                        <p xml:id="tg962.3.76">
                            <anchor xml:id="tg962.3.76.1" n="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Das Theater des Herrn Diderot#Fußnoten_15"/>
                            <ref cRef="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Friedrichs von Logau Sinngedichte#Fußnote_15" xml:id="tg962.3.76.2">15</ref> (VI. 36)</p>
                    </note>
                    <note xml:id="tg962.3.78.note" target="#tg961.3.88.4">
                        <p xml:id="tg962.3.78">
                            <anchor xml:id="tg962.3.78.1" n="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Das Theater des Herrn Diderot#Fußnoten_16"/>
                            <ref cRef="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Friedrichs von Logau Sinngedichte#Fußnote_16" xml:id="tg962.3.78.2">16</ref> (IV. 4)</p>
                    </note>
                    <note xml:id="tg962.3.80.note" target="#tg961.3.88.7">
                        <p xml:id="tg962.3.80">
                            <anchor xml:id="tg962.3.80.1" n="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Das Theater des Herrn Diderot#Fußnoten_17"/>
                            <ref cRef="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Friedrichs von Logau Sinngedichte#Fußnote_17" xml:id="tg962.3.80.2">17</ref> Sinngedicht 91.</p>
                    </note>
                    <note xml:id="tg962.3.82.note" target="#tg961.3.92.2">
                        <p xml:id="tg962.3.82">
                            <anchor xml:id="tg962.3.82.1" n="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Das Theater des Herrn Diderot#Fußnoten_18"/>
                            <ref cRef="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Friedrichs von Logau Sinngedichte#Fußnote_18" xml:id="tg962.3.82.2">18</ref> Sinngedicht 157.</p>
                    </note>
                    <note xml:id="tg962.3.84.note" target="#tg961.3.96.2">
                        <p xml:id="tg962.3.84">
                            <anchor xml:id="tg962.3.84.1" n="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Das Theater des Herrn Diderot#Fußnoten_19"/>
                            <ref cRef="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Friedrichs von Logau Sinngedichte#Fußnote_19" xml:id="tg962.3.84.2">19</ref> (XI. 24)</p>
                    </note>
                    <note xml:id="tg962.3.86.note" target="#tg961.3.101.1">
                        <p xml:id="tg962.3.86">
                            <anchor xml:id="tg962.3.86.1" n="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Das Theater des Herrn Diderot#Fußnoten_20"/>
                            <ref cRef="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Friedrichs von Logau Sinngedichte#Fußnote_20" xml:id="tg962.3.86.2">20</ref> Sinngedicht 1259.</p>
                    </note>
                    <note xml:id="tg962.3.88.note" target="#tg961.3.105.2">
                        <p xml:id="tg962.3.88">
                            <anchor xml:id="tg962.3.88.1" n="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Das Theater des Herrn Diderot#Fußnoten_21"/>
                            <ref cRef="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Friedrichs von Logau Sinngedichte#Fußnote_21" xml:id="tg962.3.88.2">21</ref> (X. 8)</p>
                    </note>
                    <note xml:id="tg962.3.90.note" target="#tg961.3.111.3">
                        <p xml:id="tg962.3.90">
                            <anchor xml:id="tg962.3.90.1" n="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Das Theater des Herrn Diderot#Fußnoten_22"/>
                            <ref cRef="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Friedrichs von Logau Sinngedichte#Fußnote_22" xml:id="tg962.3.90.2">22</ref> Sinnged.</p>
                    </note>
                    <note xml:id="tg962.3.92.note" target="#tg961.3.115.2">
                        <p xml:id="tg962.3.92">
                            <anchor xml:id="tg962.3.92.1" n="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Das Theater des Herrn Diderot#Fußnoten_23"/>
                            <ref cRef="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Friedrichs von Logau Sinngedichte#Fußnote_23" xml:id="tg962.3.92.2">23</ref> Erste Zugabe, Sinngedicht 201.</p>
                    </note>
                    <note xml:id="tg962.3.94.note" target="#tg961.3.122.1">
                        <p xml:id="tg962.3.94">
                            <anchor xml:id="tg962.3.94.1" n="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Das Theater des Herrn Diderot#Fußnoten_24"/>
                            <ref cRef="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Friedrichs von Logau Sinngedichte#Fußnote_24" xml:id="tg962.3.94.2">24</ref> (III. 31)</p>
                    </note>
                    <note xml:id="tg962.3.96.note" target="#tg961.3.127.1">
                        <p xml:id="tg962.3.96">
                            <anchor xml:id="tg962.3.96.1" n="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Das Theater des Herrn Diderot#Fußnoten_25"/>
                            <ref cRef="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Friedrichs von Logau Sinngedichte#Fußnote_25" xml:id="tg962.3.96.2">25</ref> (IV. 48)</p>
                    </note>
                    <note xml:id="tg962.3.98.note" target="#tg961.3.132.1">
                        <p xml:id="tg962.3.98">
                            <anchor xml:id="tg962.3.98.1" n="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Das Theater des Herrn Diderot#Fußnoten_26"/>
                            <ref cRef="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Friedrichs von Logau Sinngedichte#Fußnote_26" xml:id="tg962.3.98.2">26</ref> (IV. 80)</p>
                    </note>
                    <note xml:id="tg962.3.100.note" target="#tg961.3.142.1">
                        <p xml:id="tg962.3.100">
                            <anchor xml:id="tg962.3.100.1" n="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Das Theater des Herrn Diderot#Fußnoten_27"/>
                            <ref cRef="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Friedrichs von Logau Sinngedichte#Fußnote_27" xml:id="tg962.3.100.2">27</ref> (VI. 36)</p>
                    </note>
                    <note xml:id="tg962.3.102.note" target="#tg961.3.148.3">
                        <p xml:id="tg962.3.102">
                            <anchor xml:id="tg962.3.102.1" n="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Das Theater des Herrn Diderot#Fußnoten_28"/>
                            <ref cRef="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Friedrichs von Logau Sinngedichte#Fußnote_28" xml:id="tg962.3.102.2">28</ref> Sinngedicht 1041.</p>
                    </note>
                    <note xml:id="tg962.3.104.note" target="#tg961.3.152.3">
                        <p xml:id="tg962.3.104">
                            <anchor xml:id="tg962.3.104.1" n="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Das Theater des Herrn Diderot#Fußnoten_29"/>
                            <ref cRef="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Friedrichs von Logau Sinngedichte#Fußnote_29" xml:id="tg962.3.104.2">29</ref> Sinngedicht 1317.</p>
                    </note>
                    <note xml:id="tg962.3.106.note" target="#tg961.3.160.2">
                        <p xml:id="tg962.3.106">
                            <anchor xml:id="tg962.3.106.1" n="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Das Theater des Herrn Diderot#Fußnoten_30"/>
                            <ref cRef="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Friedrichs von Logau Sinngedichte#Fußnote_30" xml:id="tg962.3.106.2">30</ref> Sinngedicht 779.</p>
                    </note>
                    <note xml:id="tg962.3.108.note" target="#tg961.3.167.2">
                        <p xml:id="tg962.3.108">
                            <anchor xml:id="tg962.3.108.1" n="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Das Theater des Herrn Diderot#Fußnoten_31"/>
                            <ref cRef="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Friedrichs von Logau Sinngedichte#Fußnote_31" xml:id="tg962.3.108.2">31</ref> Sinngedicht 1586.</p>
                    </note>
                    <note xml:id="tg962.3.110.note" target="#tg961.3.176.1">
                        <p xml:id="tg962.3.110">
                            <anchor xml:id="tg962.3.110.1" n="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Das Theater des Herrn Diderot#Fußnoten_32"/>
                            <ref cRef="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Friedrichs von Logau Sinngedichte#Fußnote_32" xml:id="tg962.3.110.2">32</ref> Sinngedicht 2470.</p>
                    </note>
                    <note xml:id="tg962.3.112.note" target="#tg961.3.182.1">
                        <p xml:id="tg962.3.112">
                            <anchor xml:id="tg962.3.112.1" n="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Das Theater des Herrn Diderot#Fußnoten_33"/>
                            <ref cRef="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Friedrichs von Logau Sinngedichte#Fußnote_33" xml:id="tg962.3.112.2">33</ref> Sinngedicht 403.</p>
                    </note>
                    <note xml:id="tg962.3.114.note" target="#tg961.3.186.1">
                        <p xml:id="tg962.3.114">
                            <anchor xml:id="tg962.3.114.1" n="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Das Theater des Herrn Diderot#Fußnoten_34"/>
                            <ref cRef="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Friedrichs von Logau Sinngedichte#Fußnote_34" xml:id="tg962.3.114.2">34</ref> Sinngedicht 1725.</p>
                    </note>
                    <note xml:id="tg962.3.116.note" target="#tg961.3.188.4">
                        <p xml:id="tg962.3.116">
                            <anchor xml:id="tg962.3.116.1" n="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Das Theater des Herrn Diderot#Fußnoten_35"/>
                            <ref cRef="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Friedrichs von Logau Sinngedichte#Fußnote_35" xml:id="tg962.3.116.2">35</ref> Sinngedicht 1727 und 2148.</p>
                    </note>
                    <note xml:id="tg962.3.118.note" target="#tg961.3.190.2">
                        <p xml:id="tg962.3.118">
                            <anchor xml:id="tg962.3.118.1" n="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Das Theater des Herrn Diderot#Fußnoten_36"/>
                            <ref cRef="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Friedrichs von Logau Sinngedichte#Fußnote_36" xml:id="tg962.3.118.2">36</ref> (II. 78)</p>
                    </note>
                    <note xml:id="tg962.3.120.note" target="#tg961.3.192.7">
                        <p xml:id="tg962.3.120">
                            <anchor xml:id="tg962.3.120.1" n="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Das Theater des Herrn Diderot#Fußnoten_37"/>
                            <ref cRef="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Friedrichs von Logau Sinngedichte#Fußnote_37" xml:id="tg962.3.120.2">37</ref> (IV. 101)</p>
                    </note>
                    <note xml:id="tg962.3.122.note" target="#tg961.3.194.1">
                        <p xml:id="tg962.3.122">
                            <anchor xml:id="tg962.3.122.1" n="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Das Theater des Herrn Diderot#Fußnoten_38"/>
                            <ref cRef="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Friedrichs von Logau Sinngedichte#Fußnote_38" xml:id="tg962.3.122.2">38</ref> (III. 50)</p>
                    </note>
                    <note xml:id="tg962.3.124.note" target="#tg961.3.196.3">
                        <p xml:id="tg962.3.124">
                            <anchor xml:id="tg962.3.124.1" n="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Das Theater des Herrn Diderot#Fußnoten_39"/>
                            <ref cRef="/Literatur/M/Lessing, Gotthold Ephraim/Ästhetische Schriften/Vorreden/Friedrichs von Logau Sinngedichte#Fußnote_39" xml:id="tg962.3.124.2">39</ref> (XI. 130)</p>
                    </note>
                </div>
            </div>
        </body>
    </text>
</TEI>