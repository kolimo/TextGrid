<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg1105" n="/Literatur/M/Lessing, Gotthold Ephraim/Theologiekritische und philosophische Schriften/Das Christentum der Vernunft">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Das Christentum der Vernunft</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Lessing, Gotthold Ephraim: Element 00528 [2011/07/11 at 20:32:29]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <notesStmt>
                <note>
                              Entstanden um 1751/53, Erstdruck in: G. E. Lessings theologischer Nachlaß, Berlin (Voss) 1784.
                           </note>
            </notesStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Gotthold Ephraim Lessing: Werke. Herausgegeben von Herbert G. Göpfert in Zusammenarbeit mit Karl Eibl, Helmut Göbel, Karl S. Guthke, Gerd Hillen, Albert von Schirmding und Jörg Schönert, Band 1–8, München: Hanser, 1970 ff.</title>
                        <author key="pnd:118572121">Lessing, Gotthold Ephraim</author>
                    </titleStmt>
                    <extent>278-</extent>
                    <publicationStmt>
                        <date when="1970"/>
                        <pubPlace>München</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1751" notAfter="1753"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg1105.3">
                <milestone unit="sigel" n="Lessing-W Bd. 7" xml:id="tg1105.3.1"/>
                <head type="h3" xml:id="tg1105.3.3">Gotthold Ephraim Lessing</head>
                <head type="h2" xml:id="tg1105.3.4">Das Christentum der Vernunft</head>
                <milestone unit="sigel" n="Lessing-W Bd. 7" xml:id="tg1105.3.5"/>
                <pb type="start" n="278" xml:id="tg1105.3.6"/>
                <div type="h4">
                    <head type="h4" xml:id="tg1105.3.7">§ 1</head>
                    <p xml:id="tg1105.3.8">Das einzige vollkommenste Wesen hat sich von Ewigkeit her mit nichts als mit der Betrachtung des Vollkommensten beschäftigen können.</p>
                    <lb xml:id="tg1105.3.9"/>
                </div>
                <div type="h4">
                    <head type="h4" xml:id="tg1105.3.10">§ 2</head>
                    <lg>
                        <l xml:id="tg1105.3.11">Das Vollkommenste ist er selbst; und also hat Gott von Ewigkeit her nur sich selbst denken können.</l>
                    </lg>
                    <lb xml:id="tg1105.3.12"/>
                </div>
                <div type="h4">
                    <head type="h4" xml:id="tg1105.3.13">§ 3</head>
                    <p xml:id="tg1105.3.14">Vorstellen, wollen und schaffen, ist bei Gott eines. Man kann also sagen, alles was sich Gott vorstellet, alles das schafft er auch.</p>
                    <lb xml:id="tg1105.3.15"/>
                </div>
                <div type="h4">
                    <head type="h4" xml:id="tg1105.3.16">§ 4</head>
                    <p xml:id="tg1105.3.17">Gott kann sich nur auf zweierlei Art denken; entweder er denkt alle seine Vollkommenheiten auf einmal, und sich als den Inbegriff derselben; oder er denkt seine Vollkommenheiten zerteilt, eine von der andern abgesondert, und jede von sich selbst nach Graden abgeteilt.</p>
                    <lb xml:id="tg1105.3.18"/>
                </div>
                <div type="h4">
                    <head type="h4" xml:id="tg1105.3.19">§ 5</head>
                    <p xml:id="tg1105.3.20">Gott dachte sich von Ewigkeit her in aller seiner Vollkommenheit; das ist, Gott schuf sich von Ewigkeit her ein Wesen, welchem keine Vollkommenheit mangelte, die er selbst besaß.</p>
                    <lb xml:id="tg1105.3.21"/>
                </div>
                <div type="h4">
                    <head type="h4" xml:id="tg1105.3.22">§ 6</head>
                    <p xml:id="tg1105.3.23">Dieses Wesen nennt die Schrift den <hi rend="italic" xml:id="tg1105.3.23.1">Sohn Gottes,</hi> oder welches noch besser sein würde, den <hi rend="italic" xml:id="tg1105.3.23.2">Sohn Gott.</hi> Einen <hi rend="italic" xml:id="tg1105.3.23.3">Gott,</hi> weil ihm keine von den Eigenschaften fehlt, die Gott zukommen. Einen <hi rend="italic" xml:id="tg1105.3.23.4">Sohn,</hi> weil unserm Begriffe nach dasjenige, was sich etwas vorstellt, vor der Vorstellung eine gewisse Priorität zu haben scheint.</p>
                    <lb xml:id="tg1105.3.24"/>
                </div>
                <div type="h4">
                    <head type="h4" xml:id="tg1105.3.25">
                        <pb n="278" xml:id="tg1105.3.25.1"/>
§ 7</head>
                    <p xml:id="tg1105.3.26">Dieses Wesen ist Gott selbst und von Gott nicht zu unterscheiden, weil man es denkt, so bald man Gott denkt, und es ohne Gott nicht denken kann; das ist, weil man Gott ohne Gott nicht denken kann, oder weil es kein Gott sein würde, dem man die Vorstellung seiner selbst nehmen wollte.</p>
                    <lb xml:id="tg1105.3.27"/>
                </div>
                <div type="h4">
                    <head type="h4" xml:id="tg1105.3.28">§ 8</head>
                    <lg>
                        <l xml:id="tg1105.3.29">Man kann dieses Wesen ein Bild Gottes nennen, aber ein identisches Bild.</l>
                    </lg>
                    <lb xml:id="tg1105.3.30"/>
                </div>
                <div type="h4">
                    <head type="h4" xml:id="tg1105.3.31">§ 9</head>
                    <p xml:id="tg1105.3.32">Je mehr zwei Dinge mit einander gemein haben, desto größer ist die Harmonie zwischen ihnen. Die größte Harmonie muß also zwischen zwei Dingen sein, welche alles mit einander gemein haben, das ist, zwischen zwei Dingen, welche zusammen nur eines sind.</p>
                    <lb xml:id="tg1105.3.33"/>
                </div>
                <div type="h4">
                    <head type="h4" xml:id="tg1105.3.34">§ 10</head>
                    <p xml:id="tg1105.3.35">Zwei solche Dinge sind Gott und der Sohn Gott, oder das identische Bild Gottes; und die Harmonie, welche zwischen ihnen ist, nennt die Schrift den Geist, <hi rend="italic" xml:id="tg1105.3.35.1">welcher vom Vater und Sohn ausgehet.</hi>
                    </p>
                    <lb xml:id="tg1105.3.36"/>
                </div>
                <div type="h4">
                    <head type="h4" xml:id="tg1105.3.39">§ 11</head>
                    <p xml:id="tg1105.3.40">In dieser Harmonie ist alles, was in dem Vater ist, und also auch alles, was in dem Sohne ist; diese Harmonie ist also Gott.</p>
                    <lb xml:id="tg1105.3.41"/>
                </div>
                <div type="h4">
                    <head type="h4" xml:id="tg1105.3.42">§ 12</head>
                    <p xml:id="tg1105.3.43">Diese Harmonie ist aber so Gott, daß sie nicht Gott sein würde, wenn der Vater nicht Gott und der Sohn nicht Gott wären, und daß beide nicht Gott sein könnten, wenn diese Harmonie nicht wäre, das ist: <hi rend="italic" xml:id="tg1105.3.43.1">alle drei sind eines.</hi>
                    </p>
                    <lb xml:id="tg1105.3.44"/>
                </div>
                <div type="h4">
                    <head type="h4" xml:id="tg1105.3.45">§ 13</head>
                    <p xml:id="tg1105.3.46">Gott dachte seine Vollkommenheit zerteilt, das ist, er schaffte Wesen, wovon jedes etwas von seinen Vollkommenheiten hat; denn, um es nochmals zu wiederholen, jeder Gedanke ist bei Gott eine Schöpfung.</p>
                    <lb xml:id="tg1105.3.47"/>
                    <pb n="279" xml:id="tg1105.3.48.1"/>
                </div>
                <div type="h4">
                    <head type="h4" xml:id="tg1105.3.50">§ 14</head>
                    <lg>
                        <l xml:id="tg1105.3.51">Alle diese Wesen zusammen, heißen die Welt.</l>
                    </lg>
                    <lb xml:id="tg1105.3.52"/>
                </div>
                <div type="h4">
                    <head type="h4" xml:id="tg1105.3.53">§ 15</head>
                    <p xml:id="tg1105.3.54">Gott könnte seine Vollkommenheiten auf unendliche Arten zerteilt denken; es könnten also unendlich viel Welten möglich sein, wenn Gott nicht allezeit das vollkommenste dächte, und also auch unter diesen Arten die vollkommenste Art gedacht, und dadurch wirklich gemacht hätte.</p>
                    <lb xml:id="tg1105.3.55"/>
                </div>
                <div type="h4">
                    <head type="h4" xml:id="tg1105.3.56">§ 16</head>
                    <p xml:id="tg1105.3.57">Die vollkommenste Art, seine Vollkommenheiten zerteilt zu denken, ist diejenige, wenn man sie nach unendlichen Graden des Mehrern und Wenigern, welche so auf einander folgen, daß nirgends ein Sprung oder eine Lücke zwischen ihnen ist, zerteilt denkt.</p>
                    <lb xml:id="tg1105.3.58"/>
                </div>
                <div type="h4">
                    <head type="h4" xml:id="tg1105.3.61">§ 17</head>
                    <p xml:id="tg1105.3.62">Nach solchen Graden also müssen die Wesen in dieser Welt geordnet sein. Sie müssen eine Reihe ausmachen, in welcher jedes Glied alles dasjenige enthält, was die untern Glieder enthalten, und noch etwas mehr; welches etwas mehr aber nie die letzte Grenze erreicht.</p>
                    <lb xml:id="tg1105.3.63"/>
                </div>
                <div type="h4">
                    <head type="h4" xml:id="tg1105.3.64">§ 18</head>
                    <p xml:id="tg1105.3.65">Eine solche Reihe muß eine unendliche Reihe sein, und in diesem Verstande ist die Unendlichkeit der Welt unwidersprechlich.</p>
                    <lb xml:id="tg1105.3.66"/>
                </div>
                <div type="h4">
                    <head type="h4" xml:id="tg1105.3.67">§ 19</head>
                    <p xml:id="tg1105.3.68">Gott schafft nichts als einfache Wesen, und das Zusammengesetzte ist nichts als eine Folge seiner Schöpfung.</p>
                    <lb xml:id="tg1105.3.69"/>
                </div>
                <div type="h4">
                    <head type="h4" xml:id="tg1105.3.72">§ 20</head>
                    <p xml:id="tg1105.3.73">Da jedes von diesen einfachen Wesen etwas hat, welches die andern haben, und keines etwas haben kann, welches die andern nicht hätten, so muß unter diesen einfachen Wesen eine Harmonie sein, aus welcher Harmonie alles zu erklären ist, was unter ihnen überhaupt, das ist, in der Welt vorgehet.</p>
                    <lb xml:id="tg1105.3.74"/>
                </div>
                <div type="h4">
                    <head type="h4" xml:id="tg1105.3.75">
                        <pb n="280" xml:id="tg1105.3.75.1"/>
§ 21</head>
                    <p xml:id="tg1105.3.76">Bis hieher wird einst ein glücklicher Christ das Gebiete der Naturlehre erstrecken: doch erst nach langen Jahrhunderten, wenn man alle Erscheinungen in der Natur wird ergründet haben, so daß nichts mehr übrig ist, als sie auf ihre wahre Quelle zurück zu führen.</p>
                    <lb xml:id="tg1105.3.77"/>
                </div>
                <div type="h4">
                    <head type="h4" xml:id="tg1105.3.78">§ 22</head>
                    <p xml:id="tg1105.3.79">Da diese einfache Wesen gleichsam eingeschränkte Götter sind, so müssen auch ihre Vollkommenheiten den Vollkommenheiten Gottes ähnlich sein; so wie Teile dem Ganzen.</p>
                    <lb xml:id="tg1105.3.80"/>
                </div>
                <div type="h4">
                    <head type="h4" xml:id="tg1105.3.83">§ 23</head>
                    <p xml:id="tg1105.3.84">Zu den Vollkommenheiten Gottes gehöret auch dieses, daß er sich seiner Vollkommenheit bewußt ist, und dieses, daß er seinen Vollkommenheiten gemäß handeln kann: beide sind gleichsam das Siegel seiner Vollkommenheiten.</p>
                    <lb xml:id="tg1105.3.85"/>
                </div>
                <div type="h4">
                    <head type="h4" xml:id="tg1105.3.86">§ 24</head>
                    <p xml:id="tg1105.3.87">Mit den verschiedenen Graden seiner Vollkommenheit müssen also auch verschiedene Grade des Bewußtseins dieser Vollkommenheiten und der Vermögenheit denselben gemäß zu handeln, verbunden sein.</p>
                    <lb xml:id="tg1105.3.88"/>
                </div>
                <div type="h4">
                    <head type="h4" xml:id="tg1105.3.89">§ 25</head>
                    <p xml:id="tg1105.3.90">Wesen, welche Vollkommenheiten haben, sich ihrer Vollkommenheiten bewußt sind, und das Vermögen besitzen, ihnen gemäß zu handeln, heißen <hi rend="italic" xml:id="tg1105.3.90.1">moralische Wesen,</hi> das ist solche, welche einem Gesetze folgen können.</p>
                    <lb xml:id="tg1105.3.91"/>
                </div>
                <div type="h4">
                    <head type="h4" xml:id="tg1105.3.94">§ 26</head>
                    <p xml:id="tg1105.3.95">Dieses Gesetz ist aus ihrer eigenen Natur genommen, und kann kein anders sein, als: <hi rend="italic" xml:id="tg1105.3.95.1">handle deinen individualischen Vollkommenheiten gemäß.</hi>
                    </p>
                    <lb xml:id="tg1105.3.96"/>
                </div>
                <div type="h4">
                    <head type="h4" xml:id="tg1105.3.97">§ 27</head>
                    <p xml:id="tg1105.3.98">Da in der Reihe der Wesen unmöglich ein Sprung Statt finden kann, so müssen auch solche Wesen existieren, welche sich ihrer Vollkommenheiten nicht deutlich genung bewußt sind – – – – –</p>
                    <pb n="281" xml:id="tg1105.3.99"/>
                </div>
            </div>
        </body>
    </text>
</TEI>