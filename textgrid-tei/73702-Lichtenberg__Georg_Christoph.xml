<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg26" n="/Literatur/M/Lichtenberg, Georg Christoph/Aufsätze und Streitschriften/Amintors Morgen-Andacht">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Amintors Morgen-Andacht</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Lichtenberg, Georg Christoph: Element 00026 [2011/07/11 at 20:28:17]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <notesStmt>
                <note>
                              Erstdruck in: Göttinger Taschenkalender, 1791 (anonym).
                           </note>
            </notesStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Georg Christoph Lichtenberg: Schriften und Briefe. Herausgegeben von Wolfgang Promies, Band 1–3, München: Hanser, 1967 ff.</title>
                        <author key="pnd:118572628">Lichtenberg, Georg Christoph</author>
                    </titleStmt>
                    <extent>76-</extent>
                    <publicationStmt>
                        <date when="1967"/>
                        <pubPlace>München</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1742" notAfter="1799"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg26.3">
                <head type="h3" xml:id="tg26.3.2">Georg Christoph Lichtenberg</head>
                <head type="h2" xml:id="tg26.3.3">Amintors Morgen-Andacht<ref type="noteAnchor" target="#tg26.3.9">
                        <anchor xml:id="tg26.3.3.1" n="/Literatur/M/Lichtenberg, Georg Christoph/Aufsätze und Streitschriften/Amintors Morgen-Andacht#Fußnote_1"/>
                        <ptr cRef="/Literatur/M/Lichtenberg, Georg Christoph/Aufsätze und Streitschriften/Amintors Morgen-Andacht#Fußnoten_1"/>
                        <hi rend="superscript" xml:id="tg26.3.3.2.1">1</hi>
                    </ref>
                </head>
                <milestone unit="sigel" n="Lichtenberg-SuB Bd. 3" xml:id="tg26.3.4"/>
                <p xml:id="tg26.3.5">
                    <pb type="start" n="76" xml:id="tg26.3.5.1"/>
Wie wenn einmal die Sonne nicht wiederkäme, dachte Amintor oft, wenn er in einer dunkeln Nacht erwachte, und freute sich, wenn er endlich den Tag wieder anbrechen sah. Die tiefe Stille des frühen Morgens, die Freundin der Überlegung, verbunden mit dem Gefühl gestärkter Kräfte und wieder erneuerten Gesundheit, erweckte in ihm alsdann ein so mächtiges Vertrauen auf die Ordnung der Natur und den Geist, der sie lenkt, daß er sich in dem Tumult des Lebens so sicher glaubte, als stünde sein Verhängnis in seiner eignen Hand. Diese Empfindung, dachte er alsdann, die du dir nicht erzwingst und nicht vorheuchelst, und die dir dieses unbeschreibliche Wohlbehagen gewährt, ist gewiß das Werk eben jenes Geistes, und sagt dir laut, daß du jetzt wenigstens richtig denkst. Auch war dieses innere Anerkennen von Ordnung nichts anders, als wieder eben diese Ordnung selbst, nur auf ihn, der sie bemerkte, fortgesetzt, und daher immer für ihn der höchste Genuß seines Geistes. O ich weiß, rief er alsdann aus, dieses mein stilles Dankgebet, das Dir alle Kreatur darbringt, jedes mit seinem Gefühl und in seiner Sprache nach seiner Art, zu Tausenden, wie ich in der meinigen, wird gewiß von Dir gehört, der Du den Himmel lenkst; gewiß wird es von aller Kreatur dargebracht, aber mit doppeltem Genuß, von mir, dem Du Kraft verliehest zu erkennen, daß ich durch dieses Dankgefühl und in diesem Dankgefühl bin, was ich sein soll. O störe nicht, sprach er dann zu sich selbst, diesen himmlischen Frieden in dir heute durch Schuld! Wie würde dir der morgende Tag anbrechen wenn ihn diese reine Spiegelhelle deines Wesens nicht mehr in dein Inneres zurück würfe? Es wäre besser er erschiene nie wieder, oder wenigstens für dich Unglücklichen nicht mehr. – Diese Art <hi rend="italic" xml:id="tg26.3.5.2">in seinem Gott zu leben,</hi> wie er es nannte, die ihm von Betbrüdern, die lieber glaubten, als dachten, weil sie es so bequemer fanden, für Spinozismus ausgelegt wurde, <pb n="76" xml:id="tg26.3.5.3"/>
hatte er sich so sehr eigen gemacht, daß sie für ihn unzerstörbare Beruhigung über die Zukunft, und ein nicht zu überwältigender Trost in Todesgefahr wurde. Eines Tages als er sich nach einer seiner Morgenandachten selbst befragte, woher ihm dieses freudige Ergeben in die Führung der Welt, und dieses große Sicherheitsgefühl bei jedem Gedanken an die Zukunft komme (denn es war ihm zu fest um bloß dichterisches Aufwallen zu sein): so war es ihm entzückende Freude zu finden, daß er es allein dem Grad von Erkenntnis der Natur zu danken habe, den er sich erworben hatte, einem Grade, von dem er behauptete, daß er jedem Menschen von den gewöhnlichsten Anlagen erreichbar wäre. Nur müsse, wie er sagt, das Studium anhaltend, ohne Zank und Neuerungssucht und ohne alle Spekulationen des Inventurienten getrieben werden. Man wird ihm leicht glauben, daß es eine entzückende Betrachtung sein muß, sich sagen zu können: meine Ruhe ist das Werk meiner eigenen Vernunft; es hat sie mir keine Exegese gegeben und keine Exegese wird sie mir rauben. – O, nichts, nichts wird sie mir rauben können, als was mir meine Vernunft raubt. Daß die Betrachtung der Natur diesen Trost gewähren kann, davon ist er gewiß, denn er lebt in ihm; ob er es für alle sei, ließ er wenigstens unentschieden, und hierbei hinge, wie er sagte, vieles von der Art ab, wie die Wissenschaft getrieben und angewandt würde, eine Sache, die, wie vielleicht auch Spinozismus, wenn er unschädlich sein soll, nicht gelehrt, sondern selbst gefunden sein wolle; es sei nichts weniger als jene physicotheologische Betrachtung von Sonnen, deren uns deutlich sichtbares Heer nach einer Art von Zählung auf 75 Millionen geschätzt würde. Er nannte diese erhabene Betrachtungen bloße Musik der Sphären, die anfangs den Geist wie mit einem Sturm von Entzücken fast zur Betäubung hinreiße, er werde ihrer aber endlich gewohnt; allein das was davon immer bliebe, unstreitig das Beste, fände sich überall und vorzüglich in dem mit in die Reihe gehörigen Geist, der dieser Betrachtungen fähig sei. Es sei vielmehr eine zu anhaltendem Studio er Natur sich unvermerkt gesellende Freude über <hi rend="italic" xml:id="tg26.3.5.4">eignes Dasein,</hi> verbunden mit <hi rend="italic" xml:id="tg26.3.5.5">nicht ängstlicher,</hi> sondern <hi rend="italic" xml:id="tg26.3.5.6">froher Neugierde</hi> (wenn dieses das rechte Wort ist), die so weit über sogenannte Cüriosité erhaben sei, als hohes Gefühl für Ehre über Bauernstolz, zu erfahren, mit diesen Sinnen oder mit analogen, oder Verhältnissen anderer Art, die sich von jeder Art des Daseins hoffen lassen, <hi rend="italic" xml:id="tg26.3.5.7">was nun dieses alles sei und <pb n="77" xml:id="tg26.3.5.7.1"/>
werden wolle.</hi> Er fürchte zwar sehr, daß seine Freunde immer nur die Worte der Lehre und nicht die Lehre hören würden, hoffe aber alles, wenn er dereinst darüber sprechen würde, von eignem Versuch. Er denke nun seit der Zeit, daß das Vergnügen, das die Betrachtung der Natur dem Kinde und dem Wilden, sowie dem Manne von aller Art von Bildung gewährt, auch <hi rend="italic" xml:id="tg26.3.5.8">den</hi> großen Zweck mit zur Absicht habe, und in jedem Leben und in jeder Welt haben müsse, in welchem Zusammenhang sei: <hi rend="italic" xml:id="tg26.3.5.9">völlige Beruhigung in Absicht der Zukunft und frohes Ergeben in die Leitung der Welt;</hi> man gebe nun dieser Leitung einen Namen welchen man wolle. Er zähle es unter die wichtigste Begebenheit seines Lebens, wenigstens für sich gefunden zu haben, daß so wie wir natürlich leiden, auch natürliche von aller Tradition unabhängige Mittel haben, diese Leiden mit einer Art von Freude zu erdulten. Diese Philosophie hebe freilich den vorübergehenden Unmut nicht auf, so wenig als den Schmerz, weil eine solche Philosophie, wenn sie möglich wäre auch alles Vergnügen aufheben würde. Er pflegte dieses öfters seine <hi rend="italic" xml:id="tg26.3.5.10">Versöhnung mit Gott</hi> zu nennen, gegen den die Vernunft, selbst mit Hoffnung auf Vergebung, vielleicht murren könnte, wenn nicht im Gang der Dinge auch der Faden eingewebt wäre, der zu jener Beruhigung ohne weitere Hülfe leiten könnte. Überhaupt kamen bei seinem Vortrag viele <hi rend="italic" xml:id="tg26.3.5.11">Ausdrücke</hi> vor, deren sich die Bibel bedient; er sagte dabei: es sei nicht wohl möglich dieselbe Geschichte des menschlichen Geistes zu erzählen, ohne zuweilen auf dieselben Ausdrücke zu geraten, und glaubte, man werde die Bibel noch besser verstehen, als man sie versteht, wenn man sich selbst mehr studiere; und um mit ihren erhabenen Lehren immer zusammen zu treffen, sei der kürzeste Weg die Erreichung ihres Zwecks einmal auf einem andern, von ihr unabhängigen zu versuchen, und Zeit und Umstände dabei in Rechnung zu bringen; Spinoza selbst, glaube er, habe es nicht so übel gemeint, als die vielen Menschen die jetzt statt seiner meinen. – Es sei für Millionen Menschen bequemer und verständlicher vom Himmel herab zuhören: <hi rend="italic" xml:id="tg26.3.5.12">Du sollst nicht stehlen, und kein falsch Zeugnis reden,</hi> als im Himmel selbst die Stelle zu suchen, wo diese Worte wirklich mit Flammenschrift geschrieben stehen, wo sie von vielen gelesen worden sei. Übrigens glaube er, sei es für die Ferngläser und die Brillen unbedeutend, ob das Licht wirklich von der Sonne herabströme, oder ob die Sonne nur ein Medium zittern mache, und es <pb n="78" xml:id="tg26.3.5.13"/>
bloß ließe als strömte es herab; aber die Ferngläser und zumal die Brillen seien deswegen nichts weniger als unbedeutend, und bei der Brille pflegte ihm öfters einzufallen, daß der Mensch zwar nicht die Macht hätte die Welt zu modeln wie er wolle, aber dafür die Macht Brillen zu schleifen, wodurch er sie schier erscheinen machen könne wie wir wollen, und solcher Betrachtungen mehr, wodurch er seine Freunde nicht sowohl auf <hi rend="italic" xml:id="tg26.3.5.14">seinen</hi> Weg hinleiten, als ihnen vielmehr Winke geben wollte, den selbst zu finden, der ihnen der sicherste und bequemste wäre. Wie es denn wirklich an dem ist, daß Philosophie, wenn sie für den Menschen etwas mehr sein soll als eine Sammlung von Materien zum Disputieren, nur indirekte gelehrt werden kann.</p>
                <milestone unit="sigel" n="Lichtenberg-SuB Bd. 3" xml:id="tg26.3.7"/>
                <div type="h4">
                    <head type="h4" xml:id="tg26.3.8">Fußnote</head>
                    <note xml:id="tg26.3.9.note" target="#tg26.3.3.1">
                        <p xml:id="tg26.3.9">
                            <anchor xml:id="tg26.3.9.1" n="/Literatur/M/Lichtenberg, Georg Christoph/Aufsätze und Streitschriften/Amintors Morgen-Andacht#Fußnoten_1"/>
                            <ref cRef="/Literatur/M/Lichtenberg, Georg Christoph/Aufsätze und Streitschriften/Amintors Morgen-Andacht#Fußnote_1" xml:id="tg26.3.9.2">1</ref> Gegenwärtiger Aufsatz, der dem Herausgeber von einem Ungenannten zugekommen ist, kann vielleicht als eine Einleitung zum folgenden und einigen andern physikalischen Artikeln in diesem Kalender angesehen werden. Man kann ihn auch allein gebrauchen, oder gar keinen Gebrauch davon machen, oder auch mit ihm machen was man will, nur deute man ihn nicht wider den Verfasser oder den Herausgeber, weil man alsdann gewiß etwas sehr Unbilliges tun würde.</p>
                    </note>
                </div>
            </div>
        </body>
    </text>
</TEI>