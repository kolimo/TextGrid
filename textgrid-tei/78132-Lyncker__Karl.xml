<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg8" n="/Literatur/M/Lyncker, Karl/Sagen/Deutsche Sagen und Sitten in hessischen Gauen/2. Der Chatten und Hermunduren Streit um den heiligen Salzfluß">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>2. Der Chatten und Hermunduren Streit um den heiligen Salzfluß</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Lyncker, Karl: Element 00011 [2011/07/11 at 20:24:34]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Karl Lyncker: Deutsche Sagen und Sitten in hessischen Gauen. Kassel: Verlag von Oswald Bertram, 1854.</title>
                        <author key="pnd:139259066">Lyncker, Karl</author>
                    </titleStmt>
                    <extent>1-</extent>
                    <publicationStmt>
                        <date when="1854"/>
                        <pubPlace>Kassel</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1823" notAfter="1855"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg8.2">
                <div type="h4">
                    <head type="h4" xml:id="tg8.2.1">
                        <pb n="1" xml:id="tg8.2.1.1"/>
2. Der Chatten und Hermunduren Streit um den heiligen Salzfluß.</head>
                    <p xml:id="tg8.2.2">Wo das Gebiet der Hermunduren an das der Chatten grenzte, floß die Saale, ein beiden Völkern heiliger Fluß, dessen Wasser, über glühende Baumschichten gegossen, ihnen das Salz lieferte. Die Salzquellen waren den Germanen insgesammt heilig, denn sie glaubten, daß die Götter dort nahe wohnten und an solcher Stätte die Gebete der Sterblichen eher erhörten als anderswo. Chatten und Hermunduren kamen über den Besitz dieses Salzflusses in Streit; vor der Schlacht weihten die Chatten auf den Fall des Sieges das feindliche Heer, Männer und Rosse ihren Göttern Mars und Merkur<ref type="noteAnchor" target="#tg8.2.6">
                            <anchor xml:id="tg8.2.2.1" n="/Literatur/M/Lyncker, Karl/Sagen/Deutsche Sagen und Sitten in hessischen Gauen/2. Der Chatten und Hermunduren Streit um den heiligen Salzfluß#Fußnote_1"/>
                            <ptr cRef="/Literatur/M/Lyncker, Karl/Sagen/Deutsche Sagen und Sitten in hessischen Gauen/2. Der Chatten und Hermunduren Streit um den heiligen Salzfluß#Fußnoten_1"/>
                            <hi rend="superscript" xml:id="tg8.2.2.2.1">1</hi>
                        </ref>. Aber der Kampf fiel unglücklich für sie aus, und die Hermunduren vollzogen an ihnen selbst, was sie gelobt hatten, indem sie die gefangenen Chatten <hi rend="spaced" xml:id="tg8.2.2.3">ihren</hi> Göttern opferten.</p>
                    <lb xml:id="tg8.2.3"/>
                    <closer>
                        <seg type="closer" rend="zenoPLm4n0" xml:id="tg8.2.4">
                            <seg rend="zenoTXFontsize80" xml:id="tg8.2.4.1">
                                <hi rend="spaced" xml:id="tg8.2.4.1.1">
                                    <hi rend="italic" xml:id="tg8.2.4.1.1.1">Tacitus,</hi>
                                </hi> 13, 57.</seg>
                        </seg>
                    </closer>
                </div>
                <div type="footnotes">
                    <head type="h4" xml:id="tg8.2.5">Fußnoten</head>
                    <note xml:id="tg8.2.6.note" target="#tg8.2.2.1">
                        <p xml:id="tg8.2.6">
                            <anchor xml:id="tg8.2.6.1" n="/Literatur/M/Lyncker, Karl/Sagen/Deutsche Sagen und Sitten in hessischen Gauen/2. Der Chatten und Hermunduren Streit um den heiligen Salzfluß#Fußnoten_1"/>
                            <ref cRef="/Literatur/M/Lyncker, Karl/Sagen/Deutsche Sagen und Sitten in hessischen Gauen/2. Der Chatten und Hermunduren Streit um den heiligen Salzfluß#Fußnote_1" xml:id="tg8.2.6.2">1</ref> Nach <hi rend="spaced" xml:id="tg8.2.6.3">Grimm</hi> d. Myth., 2. Ausg. 999, wären unter Mars und Merkur die deutschen Götter Wuotan und Ziu zu verstehen.</p>
                    </note>
                </div>
            </div>
        </body>
    </text>
</TEI>