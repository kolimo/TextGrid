<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg264" n="/Literatur/M/Lyncker, Karl/Sagen/Deutsche Sagen und Sitten in hessischen Gauen/258. Ful-uf!">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>258. Ful-uf!</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Lyncker, Karl: Element 00267 [2011/07/11 at 20:24:34]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Karl Lyncker: Deutsche Sagen und Sitten in hessischen Gauen. Kassel: Verlag von Oswald Bertram, 1854.</title>
                        <author key="pnd:139259066">Lyncker, Karl</author>
                    </titleStmt>
                    <extent>184-</extent>
                    <publicationStmt>
                        <date when="1854"/>
                        <pubPlace>Kassel</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1823" notAfter="1855"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg264.2">
                <div type="h4">
                    <head type="h4" xml:id="tg264.2.1">258. Ful-uf!</head>
                    <p xml:id="tg264.2.2">Zu einer Zeit, als einmal die Stadt Cassel vom Feinde belagert wurde, hatte der Rath dem Wächter auf dem St. Martinsthurme aufgegeben, fleißig auf die Bewegungen des Feindes Acht zu haben und sogleich ein Zeichen zu geben, wenn derselbe zum Sturme oder Ueberfalle heranrücke. Eines Morgens in der Dämmerung, als der Wächter eben die dritte Stunde geblasen hatte,<ref type="noteAnchor" target="#tg264.2.6">
                            <anchor xml:id="tg264.2.2.1" n="/Literatur/M/Lyncker, Karl/Sagen/Deutsche Sagen und Sitten in hessischen Gauen/258. Ful-uf!#Fußnote_1"/>
                            <ptr cRef="/Literatur/M/Lyncker, Karl/Sagen/Deutsche Sagen und Sitten in hessischen Gauen/258. Ful-uf!#Fußnoten_1"/>
                            <hi rend="superscript" xml:id="tg264.2.2.2.1">1</hi>
                        </ref>
                        <pb n="184" xml:id="tg264.2.2.3"/>
bemerkte derselbe in der That ein verdächtiges Hin- und Herrennen im feindlichen Lager; er eilte sogleich auf den Altan des Thurmes und stieß in das Horn: »Ful-uf!« – besser konnte er den beabsichtigten Ruf: »Ihr Faulen, auf!« nicht hervorbringen. Die Bürger von Cassel lagen noch in süßem Schlafe; doch war das Signal vernommen worden. Es erhob sich Lärm durch alle Straßen und bald standen die Bürger mit Wehr und Harnisch auf dem Sammelplatze. In geordneten Reihen, ihren Kriegshauptmann an der Spitze, zogen sie dem anrückenden Feinde entgegen, griffen ihn herzhaft an und schlugen ihn in die Flucht. Zur Erinnerung an dies Ereigniß mußte von nun an der Wächter, wenn er die dritte Morgenstunde geblasen hatte, jedesmal sein »Ful-uf!« hinterdrein tuten. Dies ist auch bis in die neueste Zeit geschehen und erst bei den letzten Thürmern abgekommen, welche das »Ful-uf!« in der hergebrachten Weise nicht zu blasen verstanden.</p>
                    <lb xml:id="tg264.2.3"/>
                    <closer>
                        <seg type="closer" rend="zenoPLm4n0" xml:id="tg264.2.4">
                            <seg rend="zenoTXFontsize80" xml:id="tg264.2.4.1">Mündlich.</seg>
                        </seg>
                    </closer>
                </div>
                <div type="footnotes">
                    <head type="h4" xml:id="tg264.2.5">Fußnoten</head>
                    <note xml:id="tg264.2.6.note" target="#tg264.2.2.1">
                        <p xml:id="tg264.2.6">
                            <anchor xml:id="tg264.2.6.1" n="/Literatur/M/Lyncker, Karl/Sagen/Deutsche Sagen und Sitten in hessischen Gauen/258. Ful-uf!#Fußnoten_1"/>
                            <ref cRef="/Literatur/M/Lyncker, Karl/Sagen/Deutsche Sagen und Sitten in hessischen Gauen/258. Ful-uf!#Fußnote_1" xml:id="tg264.2.6.2">1</ref> Die große Glocke auf dem St. Martinskirchthurme thut täglich hundert Schläge; dies ist eins der Wahrzeichen von Cassel. Um 4 Uhr Morgens schlägt es zuerst, um 7 Uhr Abends zuletzt; um 8 Uhr wird geläutet und von 9 Uhr bis Morgens 3 Uhr werden die Stunden durch ebensoviele Trompetenstöße angezeigt.</p>
                    </note>
                </div>
            </div>
        </body>
    </text>
</TEI>