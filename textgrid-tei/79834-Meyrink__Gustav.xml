<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg39" n="/Literatur/M/Meyrink, Gustav/Erzählungen/Des Deutschen Spiessers Wunderhorn/Erster Teil/Blamol">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Blamol</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Meyrink, Gustav: Element 00020 [2011/07/11 at 20:27:52]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Gustav Meyrink: Gesammelte Werke, Band 4, Teil 1, München: Albert Langen, 1913.</title>
                        <author key="pnd:118582046">Meyrink, Gustav</author>
                    </titleStmt>
                    <extent>88-</extent>
                    <publicationStmt>
                        <date when="1913"/>
                        <pubPlace>München</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1868" notAfter="1932"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg39.3">
                <div type="h4">
                    <head type="h4" xml:id="tg39.3.1">
                        <pb n="88" xml:id="tg39.3.1.1"/>
Blamol</head>
                    <milestone unit="head_start"/>
                    <p rend="zenoPLm16n16" xml:id="tg39.3.2">
                        <seg rend="zenoTXFontsize80" xml:id="tg39.3.2.1">»Wahrhaftiglich, ohne Betrug und gewiß, ich sage dir: so wie es unten ist, ist es auch oben.«</seg>
                    </p>
                    <milestone unit="head_end"/>
                    <milestone unit="head_start"/>
                    <p rend="zenoPR" xml:id="tg39.3.3">
                        <seg rend="zenoTXFontsize80" xml:id="tg39.3.3.1">
                            <hi rend="italic" xml:id="tg39.3.3.1.1">Tabula smaragdina</hi>
                        </seg>
                    </p>
                    <milestone unit="head_end"/>
                    <lb xml:id="tg39.3.4"/>
                    <p xml:id="tg39.3.5">Der alte Tintenfisch saß auf einem dicken blauen Buch, das man in einem gescheiterten Schiffe gefunden hatte, und sog langsam die Druckerschwärze heraus.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.6">Landbewohner haben gar keinen Begriff, wie beschäftigt so ein Tintenfisch den ganzen Tag über ist.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.7">Dieser da hatte sich auf Medizin geworfen und von früh bis Abend mußten die beiden armen kleinen Seesterne – weil sie ihm so viel Geld schuldig waren – umblättern helfen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.8">Auf dem Leibe – dort wo andere Leute eine Taille haben – trug er einen goldenen Zwicker. – Ein Beutestück. Die Gläser standen weit ab – links und rechts –, und wer zufällig durchsah, dem wurde gräßlich schwindelig.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.9">– – – – Tiefer Friede lag ringsum. – –</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.10">Mit einem Mal kam der Polyp angeschossen, die sackförmige Schnauze vorgestreckt, die Fangarme lang nachschleppend wie ein Rutenbündel, und ließ sich neben dem Buche nieder. – Wartete, bis der <pb n="89" xml:id="tg39.3.10.1"/>
Alte aufschaute, grüßte dann sehr tief und wickelte eine Zinnbüchse mit eingepreßten Buchstaben aus sich heraus.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.11">»Sie sind wohl der violette Pulp aus dem Steinbuttgäßchen?« fragte gnädig der Alte. »Richtig, richtig, habe ja Ihre Mutter gut gekannt, – geborene ›von Octopus‹. (Sie, Barsch, bringen Sie mir 'mal den Gothaschen Polypenalmanach her.) Nun, was kann ich für Sie tun, lieber Pulp?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.12">»Inschrift, – ehüm, ehüm – Inschrift – lesen,« hüstelte der verlegen (er hatte so eine schleimige Aussprache) und deutete auf die Blechbüchse.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.13">Der Tintenfisch stierte auf die Dose und machte gestielte Augen wie ein Staatsanwalt:</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.14">»Was sehe ich, – Blamol!? – Das ist ja ein unschätzbarer Fund. – Gewiß aus dem gestrandeten Weihnachtsdampfer? – Blamol – das neue Heilmittel, – je mehr man davon nimmt, desto gesünder wird man!</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.15">Wollen das Ding gleich öffnen lassen. Sie, Barsch, schießen Sie mal zu den zwei Hummern rüber, – Sie wissen doch, Korallenbank, Ast II, Brüder Scissors, – aber rasch.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.16">Kaum hatte die grüne Seerose, die in der Nähe saß, von der neuen Arznei gehört, huschte sie sogleich neben den Polypen: – – Ach, sie nahm sie so gerne ein; – ach, für ihr Leben gern! –</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.17">
                        <pb n="90" xml:id="tg39.3.17.1"/>
Und mit ihren vielen hundert Greifern führte sie ein entzückendes Gewimmel auf, daß man kein Auge von ihr abwenden konnte. –</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.18">Hai – fisch! – war sie schön! Der Mund war ein bißchen groß zwar, doch das ist gerade bei Damen so pikant.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.19">Alle waren vergafft in ihre Reize und übersahen ganz, daß die beiden Hummern schon angekommen waren und emsig mit ihren Scheren an der Blechbüchse herumschnitten, wobei sie sich ihrem tschnetschenden Dialekt unterhielten. –</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.20">Ein leiser Ruck, und die Dose fiel auseinander.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.21">Wie ein Hagelschauer stoben die weißen Pillen heraus und – leichter als Kork – verschwanden sie blitzschnell in die Höhe.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.22">Erregt stürzte alles durcheinander: »Aufhalten, aufhalten!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.23">Aber niemand hatte rasch genug zugreifen können. Nur der Seerose war es geglückt, noch eine Pille zu erwischen und sie schnell in den Mund zu stecken.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.24">Allgemeiner Unwillen; – am liebsten hätte man die Brüder Scissors geohrfeigt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.25">»Sie, Barsch, Sie haben wohl auch nicht aufpassen können? – Wozu sind Sie eigentlich Assistent bei mir?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.26">War das ein Schimpfen und Keifen! Bloß der Pulp konnte kein Wort herausbringen, hieb nur <pb n="91" xml:id="tg39.3.26.1"/>
wütend mit den geballten Fangarmen auf eine Muschel, daß das Perlmutter krachte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.27">Plötzlich trat Totenstille ein: – Die Seerose!</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.28">Der Schlag mußte sie getroffen haben: sie konnte kein Glied rühren. Die Fühler weit von sich gestreckt, wimmerte sie leise.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.29">Mit wichtiger Miene schwamm der Tintenfisch hinzu und begann eine geheimnisvolle Untersuchung. Mit einem Kieselstein schlug er gegen einen oder den andern Fühler oder stach hinein. (Hm, hm, Babynskisches Phänomen, Störung der Pyramidenbahnen.) Nachdem er schließlich mit der Schärfe eines Flossensaumes der Seerose einigemal kreuz und quer über den Bauch gefahren war, wobei seine Augen einen undurchdringenden Blick annahmen, richtete er sich würdevoll auf und sagte: »Seitenstrangsklerose. – Die Dame ist gelähmt.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.30">»Ist noch Hilfe? Was glauben Sie? Helfen Sie, helfen Sie, – ich schieß rasch in die Apotheke,« rief da das gute Seepferd.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.31">»Helfen?! – Herr, sind Sie verrückt? Glauben Sie vielleicht, ich habe Medizin studiert, um Krankheiten zu heilen?« Der Tintenfisch wurde immer heftiger. »Mir scheint, Sie halten mich für einen Barbier, oder wollen Sie mich verhöhnen? Sie, Barsch, – Hut und Stock, – ja!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.32">
                        <pb n="92" xml:id="tg39.3.32.1"/>
Einer nach dem andern schwamm fort: »Was einen hier in diesem Leben doch alles treffen kann, schrecklich – nicht?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.33">Bald war der Platz leer, nur hin und wieder kam der Barsch mürrisch zurück, nach einigen verlorenen oder vergessenen Dingen zu suchen.</p>
                    <lb xml:id="tg39.3.34"/>
                    <milestone unit="hi_start"/>
                    <p rend="zenoPC" xml:id="tg39.3.35">
                        <hi rend="superscript" xml:id="tg39.3.35.1">*</hi>
                        <hi rend="subscript" xml:id="tg39.3.35.2">*</hi>
                        <hi rend="superscript" xml:id="tg39.3.35.3">*</hi>
                    </p>
                    <milestone unit="hi_end"/>
                    <lb xml:id="tg39.3.36"/>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.37">Auf dem Grunde des Meeres regte sich die Nacht. Die Strahlen, von denen niemand weiß, woher sie kommen und wohin sie entschwinden, schwebten wie Schleier in dem grünen Wasser und schimmerten so müde, als sollten sie nie mehr wiederkehren.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.38">Die arme Seerose lag unbeweglich und sah ihnen nach in herbem Weh, wie sie langsam, langsam in die Höhe stiegen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.39">Gestern um diese Zeit schlief sie schon längst, zur Kugel geballt, in sicherem Versteck. – Und jetzt? – Auf offener Straße umkommen zu müssen, wie ein – Tier! – Luftperlen traten ihr auf die Stirne.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.40">Und morgen ist Weihnachten!!</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.41">An ihren fernen Gatten mußte sie denken, der sich, weiß Gott wo, herumtrieb. – Drei Monate nun schon Tangwitwe! Wahrhaftig, es wäre kein Wunder gewesen, wenn sie ihn hintergangen hätte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.42">
                        <pb n="93" xml:id="tg39.3.42.1"/>
Ach, wäre doch wenigstens das Seepferd bei ihr geblieben! –</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.43">Sie fürchtete sich so! –</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.44">Immer dunkler war es, daß man kaum mehr die eigenen Fühler unterscheiden konnte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.45">Breitschultrige Finsternis kroch hervor hinter Steinen und Algen und fraß die verschwommenen Schatten der Korallenbänke.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.46">Gespenstisch glitten schwarze Körper vorüber – mit glühenden Augen und violett aufleuchtenden Flossen. – Nachtfische! – Scheußliche Rochen und Seeteufel, die in der Dunkelheit ihr Wesen treiben. – – – Mordsinnend hinter Schiffstrümmern lauern. –</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.47">Scheu und leise wie Diebe, öffnen die Muscheln ihre Schalen und locken den späten Wanderer auf weichen Pfühl zu grausigem Laster.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.48">In weiter Ferne bellte ein Hundsfisch.</p>
                    <lb xml:id="tg39.3.49"/>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.50">– – – Da zuckt durch die Ulven heller Schein: Eine leuchtende Meduse führt trunkene Zecher heim; – Aalgigerln mit schlumpigen Muränendirnen an der Flosse.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.51">Zwei silbergeschmückte junge Lachse sind stehen geblieben und blicke verächtlich auf die berauschende Schar. Wüster Gesang erschallt:</p>
                    <lb xml:id="tg39.3.52"/>
                    <pb n="94" xml:id="tg39.3.53"/>
                    <lg>
                        <l xml:id="tg39.3.54">»In dem grünen Tange – –</l>
                        <l xml:id="tg39.3.55">hab' ich sie gefragt,</l>
                        <l xml:id="tg39.3.56">Ob sie nach mir verlange. – –</l>
                        <l xml:id="tg39.3.57">Ja, hat sie gesagt.</l>
                        <l xml:id="tg39.3.58">Drauf hat sie sich gebückt –</l>
                        <l xml:id="tg39.3.59">und ich hab's sie gezwickt.</l>
                        <l xml:id="tg39.3.60">Ach im grünen Tange ....«</l>
                    </lg>
                    <lb xml:id="tg39.3.61"/>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.62">»No, no, aus dem Weg da, Sö, – Sö Frechlachs – Sö,« brüllt ein Aal plötzlich.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.63">Der Silberne fährt auf: »Schweigen Sie! Sie haben's nötig, weanerisch zu reden. Glauben wohl, weil Sie das einzige Viech sind, das nicht im Donaugebiet vorkommt – –«</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.64">»Pst, pst,« beschwichtigte sie die Meduse, »schämen Sie sich doch, schauen Sie dorthin!« –</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.65">Alle verstummten und blickten voll Scheu auf einige schmächtige, farblose Gestalten, die sittsam ihres Weges ziehen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.66">»Lanzettenfischchen,« flüsterte einer.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.67">? ? ? ? ?</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.68">– – – »O, das sind hohe Herren, – Hofräte, Diplomaten und so. – Ja die sind schon von Geburt dazu bestimmt, welche Naturwunder: Haben weder Gehirn noch Rückgrat.« –</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.69">Minuten stummer Bewunderung, dann schwimmen alle friedlich weiter.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.70">
                        <pb n="95" xml:id="tg39.3.70.1"/>
Die Geräusche verhallen. – Totenstille senkt sich nieder.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.71">Die Zeit rückt vor. – Mitternacht, die Stunde des Schreckens.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.72">Waren das nicht Stimmen? – Crevetten können es doch nicht sein, – jetzt so spät? –</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.73">Die Wache geht um: <hi rend="spaced" xml:id="tg39.3.73.1">Polizeikrebse</hi>! –</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.74">Wie sie scharren mit gepanzerten Beinen, über den Sand knirschend ihren Raub in Sicherheit bringen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.75">Wehe, wer ihnen in die Hände fällt; – vor keinem Verbrechen scheuen sie zurück, – – und ihre Lügen gelten vor Gericht wie Eide.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.76">Sogar der Zitterrochen erbleicht, wenn sie nahen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.77">Der Seerose stockt der Herzschlag vor Entsetzen, sie, eine Dame, wehrlos, – auf offenem Platze! – Wenn sie sie erblicken! Sie werden sie vor den Polizeirat, den schurkischen Meineidkrebs, schleppen, – den größten Verbrecher der Tiefsee – und dann – und dann – –</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.78">Sie nähern sich ihr – – jetzt – – ein Schritt noch, und Schande und Verderben werden die Fänge um ihren Leib schlagen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.79">Da erbebt das dunkle Wasser, die Korallenbäume ächzen und zittern wie Tang, ein fahles Licht scheint weit hin.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.80">
                        <pb n="96" xml:id="tg39.3.80.1"/>
Krebse, Rochen, Seeteufel ducken sich nieder und schießen in wilder Flucht über den Sand, Felsen brechen und wirbeln in die Höhe.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.81">Eine bläulich gleißende Wand – so groß wie die Welt – fliegt durch das Meer.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.82">Näher und näher jagt der Phosphorschein: die leuchtende Riesenflosse der Tintorera, des Dämons der Vernichtung, fegt einher und reißt abgrundtiefe glühende Trichter in das schäumende Wasser.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.83">Alles dreht sich in rasender Hast. Die Seerose fliegt durch den Raum in brausende Weiten, hinauf und hinab – über Länder von smaragdenem Gischt. –</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.84">Wo sind die Krebse, wo Schande und Angst! Das brüllende Verderben stürmt durch die Welt. – Ein Bacchanal des Todes, ein jauchzender Tanz für die Seele.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.85">Die Sinne erlöschen, wie trübes Licht.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.86">Ein furchtbarer Ruck. – Wirbel stehen, und schneller, schneller, immer schneller und schneller drehen sie sich zurück und schmettern auf den Grund, was sie ihm entrissen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.87">Mancher Panzer brach da.</p>
                    <lb xml:id="tg39.3.88"/>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.89">Als die Seerose nach dem Sturze endlich aus tiefer Ohnmacht erwachte, fand sie sich auf weiche Algen gebettet.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.90">
                        <pb n="97" xml:id="tg39.3.90.1"/>
Das gute Seepferd – es war heute gar nicht ins Amt gegangen – beugte sich über das Lager.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.91">Kühles Morgenwasser umfächelte ihr Gesicht, sie blickte um sich. Schnattern von Entenmuscheln und das fröhliche Meckern einer Geisbrasse drang an ihr Ohr.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.92">»Sie befinden sich in meinem Landhäuschen,« beantwortete das Seepferd ihren fragenden Blick und sah ihr tief in die Augen. »Wollen Sie nicht weiter schlafen, gnädige Frau, es würde Ihnen gut tun!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.93">Die Seerose konnte aber beim besten Willen nicht. Ein unbeschreibliches Ekelgefühl zog ihr die Mundwinkel herunter.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.94">»War das ein Unwetter heute nacht; mir dreht sich noch alles vor den Augen von dem Gewirbel,« fuhr das Seepferd fort. »Darf ich Ihnen übrigens mit Speck – so einem recht fetten Stückchen Matrosenspeck aufwarten?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.95">Beim bloßen Hören des Wortes Speck überkam die Seerose eine derartige Übelkeit, daß sie die Lippen zusammenpressen mußte. – Vergebens. Ein Würgen erfaßte sie (diskret blickte das Seepferd zur Seite), und sie mußte erbrechen. Unverdaut kam die Blamolpille zum Vorschein, stieg mit Luftblasen in die Höhe und verschwand.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.96">Gott sei Dank, daß das Seepferd nichts bemerkt hatte. –</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.97">
                        <pb n="98" xml:id="tg39.3.97.1"/>
Die Kranke fühlte sich plötzlich wie neugeboren.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.98">Behaglich ballte sie sich zusammen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.99">O Wunder, sie konnte sich wieder ballen, konnte ihre Glieder bewegen wie früher.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.100">Entzücken über Entzücken!</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.101">Dem Seepferd traten vor Freude Luftbläschen in die Augen. »Weihnachten, heute ist wirklich Weihnachten,« jubelte es ununterbrochen, »und das muß ich gleich dem Tintenfisch melden; Sie werden sich unterdessen recht, recht ausschlafen.«</p>
                    <lb xml:id="tg39.3.102"/>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.103">»Was finden Sie denn so Wunderbares an der plötzlichen Genesung der Seerose, mein liebes Seepferd?« fragte der Tintenfisch und lächelte mild. »Sie sind ein Enthusiast, mein junger Freund! Ich rede zwar sonst prinzipiell mit Laien (Sie, Barsch, einen Stuhl für den Herrn) nicht über die medizinische Wissenschaft, will aber diesmal eine Ausnahme machen und trachten, meine Ausdrucksweise Ihrem Auffassungsvermögen möglichst anzupassen. Also, Sie halten Blamol für ein Gift und schieben <hi rend="spaced" xml:id="tg39.3.103.1">seiner</hi> Wirkung die Lähmung zu. O, welcher Irrtum! Nebenbei bemerkt ist Blamol längst abgetan, es ist ein Mittel von gestern, <hi rend="spaced" xml:id="tg39.3.103.2">heute</hi> wird allgemein Idiotinchlorür angewandt (die Medizin schreitet nämlich unaufhaltsam vorwärts). Daß die Erkrankung mit dem Schlucken der Pille zusammentraf, war bloßer <pb n="99" xml:id="tg39.3.103.3"/>
Zufall – alles ist bekanntlich Zufall –, denn erstens hat Seitenstrangsklerose ganz andere Ursachen, die Diskretion verbietet mir, sie zu nennen, und zweitens wirkt Blamol wie alle diese Mittel gar nicht beim Einnehmen, sondern lediglich beim Ausspucken. Auch dann natürlich <hi rend="spaced" xml:id="tg39.3.103.4">nur</hi> günstig.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.104">Und was endlich die Heilung anbelangt? – Nun, da liegt ein deutlicher Fall von Autosuggestion vor. – In<hi rend="spaced" xml:id="tg39.3.104.1"> Wirklichkeit</hi> (Sie verstehen, was ich meine: ›Das Ding an sich‹ nach Kant) ist die Dame genau so krank wie gestern, wenn sie es auch nicht merkt. Gerade bei Personen mit minderwertiger Denkkraft setzen Autosuggestionen so häufig ein. – Natürlich will ich damit nichts gesagt haben, – Sie wissen wohl, wie hoch ich die Damen schätze: ›Ehret die Frauen, sie flechten und weben – – –‹ – Und jetzt, mein junger Freund, genug von diesem Thema, es würde Sie nur unnötig aufregen. – A propos, – Sie machen mir doch abends das Vergnügen? Es ist Weihnacht und – meine Vermählung.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.105">»Wa –? – Vermä – – –,« platzte das Seepferd heraus, faßte sich aber noch rechtzeitig: »O, es wird mir eine Ehre sein, Herr Medizinalrat.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.106">»Wen heiratet er denn?« fragte er beim Hinausschwimmen den Barsch. – »Was Sie nicht sagen: die<hi rend="spaced" xml:id="tg39.3.106.1"> Mies</hi> muschel?? – Warum nicht gar! – Schon wieder so eine Geldheirat.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.107">
                        <pb n="100" xml:id="tg39.3.107.1"/>
Als abends die Seerose, etwas spät, aber mit blühendem Teint an der Flosse des Seepferdes in den Saal schwamm, wollte der Jubel kein Ende nehmen. Jeder umarmte sie, selbst die Schleierschnecken und Herzmuscheln, die als Brautjungfern fungierten, legten ihre mädchenhafte Scheu ab.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.108">Es war ein glänzendes Fest, wie es nur reiche Leute geben können; die Eltern der Miesmuschel waren eben Millionäre und hatten sogar ein Meerleuchten bestellt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.109">Vier lange Austernbänke waren gedeckt. – Eine volle Stunde wurde schon getafelt, und immer kamen noch neue Leckerbissen. Dazu kredenzte der Barsch unablässig aus einem schimmernden Pokal (natürlich die Öffnung nach unten) hundertjährige Luft, die aus der Kabine eines Wracks stammte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.110">Alles war bereits angeheitert. – Die Toaste auf die Miesmuschel und ihren Bräutigam gingen in dem Knallen der Korkpolypen und dem Klappern der Messermuscheln völlig unter.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.111">Das Seepferd und die Seerose saßen am äußersten Ende der Tafel, ganz im Schatten, und achteten in ihrem Glück kaum der Umgebung.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.112">»Er« drückte »ihr« zuweilen verstohlen den einen oder anderen Fühler, und sie lohnte ihn dafür mit einem Glutblick.</p>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.113">
                        <pb n="101" xml:id="tg39.3.113.1"/>
Als gegen Ende des Mahles die Kapelle das schöne Lied spielte:</p>
                    <lb xml:id="tg39.3.114"/>
                    <lg>
                        <l xml:id="tg39.3.115">»Ja küssen, –</l>
                        <l xml:id="tg39.3.116">scherzen</l>
                        <l xml:id="tg39.3.117">mit jungen Herrn</l>
                        <l xml:id="tg39.3.118">ist selbst bei <hi rend="spaced" xml:id="tg39.3.118.1">Frauen</hi>
                        </l>
                        <l xml:id="tg39.3.119">sehr modern,«</l>
                    </lg>
                    <lb xml:id="tg39.3.120"/>
                    <p rend="zenoPLm4n0" xml:id="tg39.3.121">und sich dabei die Tischnachbarn der beiden verschmitzt zublinzelten, da konnte man sich dem Eindruck nicht verschließen, daß die allgemeine Aufmerksamkeit hier allerlei zarte Beziehungen mutmaßte.</p>
                </div>
            </div>
        </body>
    </text>
</TEI>