<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg40" n="/Literatur/M/Meyrink, Gustav/Erzählungen/Des Deutschen Spiessers Wunderhorn/Erster Teil/Das Gehirn">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Das Gehirn</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Meyrink, Gustav: Element 00021 [2011/07/11 at 20:27:52]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Gustav Meyrink: Gesammelte Werke, Band 4, Teil 1, München: Albert Langen, 1913.</title>
                        <author key="pnd:118582046">Meyrink, Gustav</author>
                    </titleStmt>
                    <extent>102-</extent>
                    <publicationStmt>
                        <date when="1913"/>
                        <pubPlace>München</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1868" notAfter="1932"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg40.3">
                <div type="h4">
                    <head type="h4" xml:id="tg40.3.1">
                        <pb n="102" xml:id="tg40.3.1.1"/>
Das Gehirn</head>
                    <p xml:id="tg40.3.2">Der Pfarrer hatte sich so herzlich auf die Heimkehr seines Bruders Martin aus dem Süden gefreut, und als dieser endlich eintrat in die altertümliche Stube, eine Stunde früher, als man erwartet hatte, da war alle Freude verschwunden.</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.3">Woran es lag, konnte er nicht begreifen, er empfand es nur, wie man einen Novembertag empfindet, an dem die Welt zu Asche zu zerfallen droht.</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.4">Auch Ursula, die Alte, brachte anfangs keinen Laut hervor.</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.5">Martin war braun wie ein Ägypter und lächelte freundlich, als er dem Pfarrer die Hände schüttelte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.6">Er bleibe gewiß zum Abendessen zu Hause und sei gar nicht müde, sagte er. Die nächsten paar Tage müsse er zwar in die Hauptstadt, dann aber wolle er den ganzen Sommer daheim sein.</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.7">Sie sprachen von ihrer Jugendzeit, als der Vater noch lebte, – und der Pfarrer sah, daß Martins seltsamer melancholischer Zug sich noch verstärkt hatte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.8">»Glaubst du nicht auch, daß gewisse überraschende, einschneidende Ereignisse bloß deshalb eintreten müssen, weil man eine innere Furcht vor ihnen nicht<pb n="103" xml:id="tg40.3.8.1"/>
 unterdrücken kann?« waren Martins letzte Worte vor dem Schlafengehen gewesen. »Und weißt du noch, welch grauenhaftes Entsetzen mich schon als kleines Kind befiel, als ich einmal in der Küche ein blutiges Kalbshirn sah ...«</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.9">Der Pfarrer konnte nicht schlafen, es lag wie ein erstickender, spukhafter Nebel in dem früher so gemütlichen Zimmer.</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.10">Das Neue, das Ungewohnte, – dachte der Pfarrer.</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.11">Aber es war nicht das Neue, das Ungewohnte, es war ein anderes, das sein Bruder hereingebracht hatte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.12">Die Möbel sahen nicht so aus wie sonst, die alten Bilder hingen, als ob sie von unsichtbaren Kräften an die Wände gepreßt würden. Man hatte das bange Ahnen, daß das bloße Ausdenken irgendeines fremden, rätselhaften Gedankens eine ruckweise, unerhörte Veränderung hervorbringen müsse. – Nur nichts Neues denken, – bleibe beim Alten, Alltäglichen, warnt das Innere. Gedanken sind gefährlich wie Blitze!</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.13">Martins Abenteuer nach der Schlacht bei Omdurman ging dem Pfarrer nicht aus dem Sinn: wie er in die Hände der Obeahneger gefallen war, die ihn an einen Baum banden – – – – – Der Obizauberer kommt aus seiner Hütte, kniet vor ihm hin <pb n="104" xml:id="tg40.3.13.1"/>
und legt noch ein blutiges Menschengehirn auf die Trommel, die eine Sklavin hält.</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.14">Jetzt sticht er mit einer langen Nadel in verschiedene Partien dieses Gehirns, und Martin schreit jedes mal wild auf, weil er den Stich im eigenen Kopfe<hi rend="spaced" xml:id="tg40.3.14.1"> fühlt</hi>.</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.15">Was hat das zu bedeuten?!</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.16">Der Herr erbarme sich seiner! ...</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.17">Gelähmt an allen Gliedern wurde Martin damals von englischen Soldaten ins Feldspital gebracht.</p>
                    <lb xml:id="tg40.3.18"/>
                    <milestone unit="hi_start"/>
                    <p rend="zenoPC" xml:id="tg40.3.19">
                        <hi rend="superscript" xml:id="tg40.3.19.1">*</hi>
                        <hi rend="subscript" xml:id="tg40.3.19.2">*</hi>
                        <hi rend="superscript" xml:id="tg40.3.19.3">*</hi>
                    </p>
                    <milestone unit="hi_end"/>
                    <lb xml:id="tg40.3.20"/>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.21">Eines Tages fand der Pfarrer seinen Bruder bewußtlos zu Hause vor.</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.22">Der Metzger mit seiner Fleischmulde sei gerade eingetreten, berichtete die alte Ursula, da plötzlich sei Herr Martin ohne Grund ohnmächtig geworden.</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.23">»Das geht so nicht weiter, du mußt in die Nervenheilanstalt des Professors Diokletian Büffelklein; der Mann genießt einen Weltruf,« hatte der Pfarrer zu seinem Bruder gesagt, als dieser wieder zu sich gekommen war, und Martin willigte ein. –</p>
                    <lb xml:id="tg40.3.24"/>
                    <milestone unit="hi_start"/>
                    <p rend="zenoPC" xml:id="tg40.3.25">
                        <hi rend="superscript" xml:id="tg40.3.25.1">*</hi>
                        <hi rend="subscript" xml:id="tg40.3.25.2">*</hi>
                        <hi rend="superscript" xml:id="tg40.3.25.3">*</hi>
                    </p>
                    <milestone unit="hi_end"/>
                    <lb xml:id="tg40.3.26"/>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.27">»Sie sind Herr Schleiden? Ihr Bruder, der Pfarrer, hat mir bereits von Ihnen berichtet. Nehmen <pb n="105" xml:id="tg40.3.27.1"/>
Sie Platz und erzählen Sie in kurzen Worten,« sagte Professor Büffelklein, als Martin das Sprechzimmer betrat, »was Ihnen fehlt.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.28">Martin setzte sich und begann:</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.29">»Drei Monate nach dem Ereignis bei Omdurman – Sie wissen – waren die letzten Lähmungserscheinungen ...«</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.30">»Zeigen Sie mir die Zunge – hm, keine Abweichung, mäßiger Tremor,« unterbrach der Professor. »Warum erzählen Sie denn nicht weiter?« –</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.31">»... waren die letzten Lähmungserscheinungen –« setzte Martin fort.</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.32">»Schlagen Sie ein Bein über das andere. So. Noch mehr, so –« befahl der Gelehrte und klopfte sodann mit einem kleinen Stahlhammer auf die Stelle unterhalb der Kniescheibe des Patienten. Sofort fuhr das Bein in die Höhe.</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.33">»Erhöhte Reflexe,« sagte der Professor. – »Haben Sie immer erhöhte Reflexe gehabt?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.34">»Ich weiß nicht; ich habe mir nie aufs Knie geklopft,« entschuldigte Martin.</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.35">»Schließen Sie ein Auge. Jetzt das andere. Öffnen Sie das linke, so – jetzt rechts – gut – Lichtreflexe in Ordnung. War der Lichtreflex bei Ihnen stets in Ordnung, besonders in letzter Zeit, Herr Schleiden?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.36">Martin schwieg resigniert.</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.37">
                        <pb n="106" xml:id="tg40.3.37.1"/>
»Auf solche Zeichen hätten Sie eben achten müssen,« bemerkte der Professor mit leichtem Vorwurf und hieß den Kranken sich entkleiden.</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.38">Eine lange, genaue Untersuchung fand statt, während welcher der Arzt alle Kennzeichen tiefsten Denkens offenbarte und dazu lateinische Worte murmelte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.39">»Sie sagten doch vorhin, daß Sie Lähmungserscheinungen hätten, ich finde aber keine,« sagte er plötzlich.</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.40">»Nein, ich wollte doch sagen, daß Sie nach drei Monaten <hi rend="spaced" xml:id="tg40.3.40.1">verschwunden</hi> seien,« entgegnete Martin Schleiden.</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.41">»Sind Sie denn schon so lange krank, mein Herr?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.42">Martin machte ein verblüfftes Gesicht.</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.43">»Es ist eine merkwürdige Erscheinung, daß sich fast alle deutschen Patienten so unklar ausdrücken,« meinte freundlich lächelnd der Professor; »da sollten Sie einmal einer Untersuchung auf einer französischen Klinik beiwohnen. Wie prägnant sich da selbst der einfache Mann ausdrückt. Übrigens hat es nicht viel auf sich mit Ihrer Krankheit. Neurasthenie, weiter nichts. – Es wird Sie wohl gewiß auch interessieren, daß es uns Ärzten – gerade in allerletzter Zeit – gelungen ist, diesen Nervensachen auf den Grund zu kommen. Ja, das ist der Segen der modernen Forschungsmethode, heute ganz genau zu <pb n="107" xml:id="tg40.3.43.1"/>
wissen, daß wir füglich gar keine Mittel – Arzneien – anwenden können. – Zielbewußt das Krankheitsbild im Auge behalten! Tag für Tag! Sie würden staunen, was wir damit erzielen können. Sie verstehen! – Und dann die Hauptsache: Vermeiden Sie jede Aufregung, das ist Gift für Sie – und jeden zweiten Tag melden Sie sich bei mir zur Visite. – Also nochmals: <hi rend="spaced" xml:id="tg40.3.43.2">keine Aufregung</hi>!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.44">Der Professor schüttelte dem Kranken die Hand und schien infolge der geistigen Anstrengung sichtlich erschöpft.</p>
                    <p xml:id="tg40.3.45">– – – – – – – – – – – – – – – – – – – – – – – – – – – –</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.46">Das Sanatorium, ein massiver Steinbau, bildete das Eck einer sauberen Straße, die das unbelebteste Stadtviertel schnitt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.47">Gegenüber zog sich das alte Palais der Gräfin Zahradka hin, dessen stets verhängte Fenster den krankhaft ruhigen Eindruck der leblosen Straße verstärkte.</p>
                    <p xml:id="tg40.3.48">Fast nie ging jemand vorbei, denn der Eingang in das vielbesuchte Sanatorium lag auf der anderen Seite bei den Ziergärten, neben den beiden alten Kastanienbäumen.</p>
                    <p xml:id="tg40.3.49"> – – – – – – – – – – – – – – – – – – – – – – – – – – – –</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.50">Martin Schleiden liebte die Einsamkeit, und der Garten mit seinen Teppichpflanzen, seinen Rollstühlen <pb n="108" xml:id="tg40.3.50.1"/>
und launischen Kranken, mit dem langweiligen Springbrunnen und den dummen Glaskugeln war ihm verleidet.</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.51">Ihn zog die stille Straße an und das alte Palais mit den dunklen Gitterfenstern. Wie mochte es drinnen aussehen?</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.52">Alte verblichene Gobelins, verschossene Möbel, umwickelte Glaslüster. Eine Greisin mit buschigen weißen Augenbrauen und herben, harten Zügen, die der Tod und das Leben vergessen hatte. –</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.53">Tag für Tag schritt Martin Schleiden das Palais entlang. –</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.54">In solchen öden Straßen muß man dicht an den Häusern gehen. –</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.55">Martin Schleiden hatte den ruhigen, eigentümlichen Schritt der Menschen, die lange in den Tropen gelebt haben. Er störte den Eindruck der Straße nicht; sie paßten so zueinander, diese weltfremden Daseinsformen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.56">Drei heiße Tage waren gekommen, und jedesmal begegnete er auf seinem einsamen Weg einem Alten, der stets eine Gipsbüste trug.</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.57">Eine Gipsbüste mit einem Bürgergesicht, das sich niemand merken konnte. –</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.58">Diesesmal waren sie zusammengestoßen – der Alte war so ungeschickt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.59">
                        <pb n="109" xml:id="tg40.3.59.1"/>
Die Büste neigte sich und fiel langsam zu Boden. – Alles fällt langsam, nur wissen es die Menschen nicht, die keine Zeit haben zur Beobachtung. –</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.60">Der Gipskopf zerbrach, und aus den weißen Scherben <hi rend="spaced" xml:id="tg40.3.60.1">quoll ein blutiges Menschengehirn</hi>. –</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.61">Martin Schleiden blickte starr hin, streckte sich und wurde fahl. Dann breitete er die Arme aus und schlug die Hände vors Gesicht.</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.62">Mit einem Seufzer stürzte er zu Boden. – –</p>
                    <p xml:id="tg40.3.63">– – – – – – – – – – – – – – – – – – – – – – – – – – – –</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.64">Der Professor und die beiden Assistenzärzte hatten den Vorgang von den Fenstern zufällig mit angesehen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.65">Der Kranke lag jetzt im Untersuchungszimmer. Er war gänzlich gelähmt und ohne Bewußtsein.</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.66">Eine halbe Stunde später war der Tod eingetreten. –</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.67">Ein Telegramm hatte den Pfarrer ins Sanatorium berufen, der jetzt weinend vor dem Mann der Wissenschaft stand. »Wie ist das nur alles so rasch gekommen, Herr Professor?« –</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.68">»Es war vorauszusehen, lieber Pfarrer,« sagte der Gelehrte. »Wir hielten uns streng an die Erfahrungen, die wir Ärzte im Laufe der Jahre in der Heilmethode gemacht haben, aber wenn der Patient selber nicht befolgt, was man ihm vorschreibt, so ist eben jede ärztliche Kunst verloren.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.69">
                        <pb n="110" xml:id="tg40.3.69.1"/>
»Wer war denn der Mann mit der Gipsbüste?« unterbrach der Pfarrer.</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.70">»Da fragen Sie mich nach Nebenumständen, zu deren Beobachtung mir Zeit und Muße fehlt – lassen Sie mich fortfahren:</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.71">Hier in diesem Zimmer habe ich wiederholte Male Ihrem Bruder auf das ausdrücklichste die Enthaltung von jeglicher Art Aufregung verordnet. – Ärztlich verordnet! Wer nicht folgte, war Ihr Bruder. Es erschüttert mich selbst tief, lieber Freund, aber Sie werden mir recht geben: Strikte Befolgung der ärztlichen Vorschrift ist und bleibt die Hauptsache. Ich selbst war Augenzeuge des Unglücksfalles:</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.72">Schlägt der Mann in höchster Aufregung die Hände vor den Kopf, wankt, taumelt und stürzt zu Boden. Da war jede Hilfe natürlich zu spät. – Ich kann Ihnen schon heute das Ergebnis der Obduktion voraussagen: Hochgradige Blutleere des Gehirnes, infolge diffuser Sklerosierung der grauen Hirnrinde. Und jetzt beruhigen Sie sich, lieber Mann, beherzigen Sie den Satz und lernen Sie daraus: Wie man sich bettet, so liegt man. –</p>
                    <p rend="zenoPLm4n0" xml:id="tg40.3.73">Es klingt hart, aber Sie wissen, die Wahrheit will starke Jünger haben.«</p>
                </div>
            </div>
        </body>
    </text>
</TEI>