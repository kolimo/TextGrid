<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg41" n="/Literatur/M/Meyrink, Gustav/Erzählungen/Des Deutschen Spiessers Wunderhorn/Erster Teil/Der Buddha ist meine Zuflucht">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Der Buddha ist meine Zuflucht</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Meyrink, Gustav: Element 00022 [2011/07/11 at 20:27:52]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Gustav Meyrink: Gesammelte Werke, Band 4, Teil 1, München: Albert Langen, 1913.</title>
                        <author key="pnd:118582046">Meyrink, Gustav</author>
                    </titleStmt>
                    <extent>111-</extent>
                    <publicationStmt>
                        <date when="1913"/>
                        <pubPlace>München</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1868" notAfter="1932"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg41.3">
                <div type="h4">
                    <head type="h4" xml:id="tg41.3.1">
                        <pb n="111" xml:id="tg41.3.1.1"/>
Der Buddha ist meine Zuflucht</head>
                    <p xml:id="tg41.3.2">Das hab' ich gehört:</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.3">Zu einer Zeit lebte ein alter Musiker in dieser Stadt, arm und verlassen. Das Zimmer, in dem er wohnte, in dem er einen Teil der Nacht zubrachte und einen Teil des Tages, war eng, düster, armselig, und in dem armseligsten, engsten, düstersten Viertel gelegen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.4">Nicht von je war der Alte so verlassen gewesen. An Jahre konnte er nicht zurückdenken, an Jahre voll Pracht und Prunk – und was an Glanz die Erde dem Reichtum bietet, das hatte sie einst ihm geboten.</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.5">Was an Freude der Erde vom Freudvollen bietet, das hatte sie einst ihm geboten.</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.6">Was an Wonnen und Schönheit die Erde dem Glücklichen bietet und dem Schönen bietet, das hatte sie auch ihm geboten.</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.7">An einem Tage aber war die Wende in seinem Glück gekommen; so wie an einem hellen Morgen die Sonne aufsteigt im wolkenlosen Himmel, ihren Höhepunkt erreicht an Klarheit, um dann niederzugehen und in trübes Dunkel zu tauchen, in dichtes Dunkel zu tauchen, in undurchdringliches Dunkel zu tauchen, dann unsichtbar wird, in Nacht versinkt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.8">
                        <pb n="112" xml:id="tg41.3.8.1"/>
Und als die Wende in seinem Glücke gekommen war und jeder neue Tag neues Unheil brachte, hatte er Hilfe im Gebet gesucht; – auf daß sein Untergang aufgehalten werde, auf den Knien gelegen lange und viele Nächte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.9">Aber Pracht und Prunk verblaßten, Freude und Glanz schwanden dahin, sein Reichtum zerbrach. Sein Weib verließ ihn, sein Kind starb, als er in seiner Armut nichts mehr besaß, es zu pflegen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.10">Da hatte er um nichts mehr gebetet.</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.11">– So trat seine Seele in die Dunkelheit. –</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.12">Wie in tiefer Nacht, wenn Finsternis die Formen und Kanten und Farben der Dinge und Wesen verschlungen hat und eins dem andern nicht mehr kann unterschieden werden, – wie in tiefer Nacht der Himmel sich leise, unmerklich hellt vom Schimmer des kommenden Mondes und flüsternd die verschwundenen Formen und Kanten der Dinge und Wesen zu einem anderen Leben weckt, so tauchten leise, unmerklich, flüsternd aus dem Dunkel seines Herzens die Worte auf, die er einstmals vernommen, gelesen irgendwo, irgendwann in der Zeit seines Reichtums, – die Worte des Buddha:</p>
                    <lb xml:id="tg41.3.13"/>
                    <lg>
                        <l xml:id="tg41.3.14">»Daher schließ dich an Liebes nicht,</l>
                        <l xml:id="tg41.3.15">Geliebtes lassen ist so schlimm!</l>
                        <l xml:id="tg41.3.16">Kein Daseinsband verstricket den,</l>
                        <l xml:id="tg41.3.17">Dem nichts mehr lieb noch unlieb ist.</l>
                        <pb n="113" xml:id="tg41.3.18"/>
                        <l xml:id="tg41.3.19">Aus Liebem sprießet Gram hervor,</l>
                        <l xml:id="tg41.3.20">Aus Liebem sprießet Furcht hervor,</l>
                        <l xml:id="tg41.3.21">Wer sich von Liebem losgesagt,</l>
                        <l xml:id="tg41.3.22">Hat keinen Gram und keine Furcht.</l>
                        <l xml:id="tg41.3.23">Dem Lebenstrieb entsprießt der Gram,</l>
                        <l xml:id="tg41.3.24">Dem Lebenstrieb entsprießt die Furcht:</l>
                        <l xml:id="tg41.3.25">Wer losgelöst vom Lebenstrieb,</l>
                        <l xml:id="tg41.3.26">Hat keinen Gram und keine Furcht.«</l>
                    </lg>
                    <lb xml:id="tg41.3.27"/>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.28">– – – – – – – – – – – – – – – – – – – – – – – – –</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.29">Da trat seine Seele in die Dämmerung.</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.30">Alles Wünschen und alles Hoffen war von ihm abgefallen, aller Gram, alle Gier, alles Leid, alle Freude.</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.31">Morgens, wenn er erwachte, sandte er seine Liebe und sein Mitleid nach Osten, nach Westen, nach Süden, nach Norden, nach oben, nach unten, und wenn er seine Arbeit begann, murmelte er: »Der Buddha ist meine Zuflucht,« und wenn er sich schlafen legte, murmelte er: »Der Buddha ist meine Zuflucht.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.32">Wenn er sein karges Mahl einnahm, wenn er trank, wenn er aufstand oder sich niedersetzte, wenn er fortging oder wiederkam, murmelte er: »Der Buddha ist meine Zuflucht.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.33">Verschlossen wurden da die Tore seiner Sinne, daß Wünschen und Hassen, – Gier, Leid und Freude keinen Einlaß mehr fanden.</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.34">An Feiertagen, wenn die Glocken läuteten, – zuweilen, <pb n="114" xml:id="tg41.3.34.1"/>
– holte er eine Glasplatte hervor und befestigte sie an seinem Tisch, schüttete feine Sandkörner darauf, und wenn er mit dem Bogen seines Cello an dem Rande des Glases niederstrich, daß es sang, schwingend und klingend, tanzte der Sand und bildete kleine, feine, regelmäßige Sterne. – Klangfiguren.</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.35">Und wie die Sterne und Formen entstanden, wuchsen und vergingen und wieder entstanden, gedachte er dumpf der Lehre des Buddha Gautama vom Leiden, von der Leidensentstehung, von der Leidensvernichtung, von dem zur Leidensvernichtung führenden Pfad: – –</p>
                    <lb xml:id="tg41.3.36"/>
                    <milestone unit="hi_start"/>
                    <p rend="zenoPC" xml:id="tg41.3.37">»Der Buddha ist meine Zuflucht.«</p>
                    <p rend="zenoPC" xml:id="tg41.3.38">– – – – – – – – – – – – – – – – – –</p>
                    <milestone unit="hi_end"/>
                    <lb xml:id="tg41.3.39"/>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.40">In das Land zu ziehen, wo die Heiligen leben, die um nichts mehr zu beten haben –, wo einst der Erhabene, Vollendete geweilt – der Asket Gotamo – und den Weg zur Freiheit gewiesen, – war seine glühende Sehnsucht.</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.41">Dort zu suchen, zu finden den Kreis der Wenigen, Erkorenen, die den lebendigen Sinn der Lehre behüten, den von Herz zu Herzen Vererbten, Unverdeuteten, Unverwirrten, zur atmenden Kraft Gewordenen, – war seine glühende Sehnsucht.</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.42">Und das Geld zu erwerben, nach Indien pilgern zu können, in das Land seiner glühenden Sehnsucht,<pb n="115" xml:id="tg41.3.42.1"/>
 spielte er mit verschlossenen Sinnen sein Cello in Schenken seit Tagen und Wochen und Monaten und vielen, vielen Jahren.</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.43">Wenn seine Gefährten ihm seinen schmalen Teil reichten, von dem, was sie ersammelt, dachte er an den Erhabenen, Vollendeten, – daß er Ihm wieder näher sei um einen Schritt: »Der Buddha ist meine Zuflucht.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.44">Weiß und gebrechlich war er so geworden, da kam der Tag, der ihm die letzten noch fehlenden Kreuzer brachte.</p>
                    <lg>
                        <l xml:id="tg41.3.45">– – – – – – – – – – – – – – – – – – – – – – – – – – – –</l>
                        <l xml:id="tg41.3.46">– – – – – – – – – – – – – – – – – – – – – – – – – – – –</l>
                    </lg>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.47">In seinem armseligen düsteren Zimmer stand er und starrte auf den Tisch.</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.48">Was sollte das Geld dort auf dem Tisch!? –</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.49">Warum hatte er es gesammelt?</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.50">Sein Gedächtnis war erloschen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.51">Er sann und sann, was sollte das Geld dort auf</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.52">dem Tisch!</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.53">Sein Gedächtnis war erloschen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.54">Er wußte nichts mehr und konnte nicht mehr denken. Nur immer wieder und wieder, wie eine Welle aus den Wassern springt und zurückfällt, tauchte der Satz auf in seinem Hirn: »Der Buddha ist meine Zuflucht. Der Buddha ist meine Zuflucht.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.55">
                        <pb n="116" xml:id="tg41.3.55.1"/>
Da öffnete sich die Türe, und sein Gefährte, der Geiger, ein mildtätiger, mitleidsloser Mensch, trat herein.</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.56">Der Alte hörte ihn nicht und starrte auf das Geld.</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.57">»Wir sammeln heute für die Kinder der Armen,« sagte endlich leise der Geiger.</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.58">Der Alte hörte ihn nicht.</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.59">»Wir sammeln heute für die Kinder derer, die vom Wege stehen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.60">Wir alle, arm und reich. – Daß sie nicht frieren und nicht verderben, nicht hungern. Daß sie gepflegt werden, wenn sie krank sind. – – –</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.61">Willst du nichts geben, Alter? – – – Und bist doch so reich!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.62">Der Alte begriff den Sinn der Worte kaum, das dumpfe Gefühl, er dürfe nichts wegnehmen, nichts hergeben von dem Gelde dort auf dem Tisch, hielt sein Herz fest wie ein Bann.</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.63">Er konnte nicht sprechen, ihm war, als hätte er diese Welt vergessen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.64">Ein Traumgesicht zog an ihm vorüber. – Er sah die glühende Sonne Indiens über regungslosen Palmen und schimmernden Pagoden und in der Ferne die weißen Berge blinken.</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.65">Die unbewegliche Gestalt Gautama Buddhas kam wie von weitem heran, und wie ein Echo hörte er im Herzen die kristallene Stimme des Vollendeten <pb n="117" xml:id="tg41.3.65.1"/>
erklingen, wie sie einst im Walde bei Sumsŭmara gĭram die seltsamen Worte gesprochen:</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.66">
                        <hi rend="spaced" xml:id="tg41.3.66.1">»So seh' ich dich denn hier, Böser! – Laß die Hoffnung fahren: ›Er kennt mich nicht!‹</hi>
                    </p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.67">
                        <hi rend="spaced" xml:id="tg41.3.67.1">Wohl kenn' ich dich, Böser, laß die Hoffnung fahren: ›Er kennt mich nicht‹ – Mārō bist du, der Böse.</hi>
                    </p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.68">
                        <hi rend="spaced" xml:id="tg41.3.68.1">Nicht den Vollendeten plage, nicht des Vollendeten Jünger. – –</hi>
                    </p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.69">
                        <hi rend="spaced" xml:id="tg41.3.69.1">Weiche von hinnen aus dem Herzen, Mārō, weiche von hinnen aus dem Herzen, Mārō.«</hi>
                    </p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.70">Da fühlte der Alte, als lasse eine Hand von ihm. Er gedachte seines eigenen Kindes, – das gestorben, weil er in seiner Armut nichts hatte, es zu pflegen. – Da nahm er all das Geld, das auf dem Tisch lag, und gab es dem Geiger. – – – – – – –</p>
                    <p xml:id="tg41.3.71">– – – – – – – – – – – – – – – – – – – – – – – – – – – –</p>
                    <lg>
                        <lb xml:id="tg41.3.72"/>
                        <l xml:id="tg41.3.73">: »Der Buddha ist meine Zuflucht.</l>
                        <l xml:id="tg41.3.74">: Der Buddha ist meine Zuflucht.«</l>
                        <l xml:id="tg41.3.75">– – – – – – – – – – – – – – – – – – –</l>
                    </lg>
                    <lb xml:id="tg41.3.76"/>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.77">Der Geiger war fort, und der Alte hatte, wie an Feiertagen, – zuweilen –, wenn die Glocken läuteten, die Glasplatte hervorgeholt und am Tische befestigt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.78">Und feine Sandkörner darauf geschüttet.</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.79">
                        <pb n="118" xml:id="tg41.3.79.1"/>
Als er mit dem Bogen seines Cello an dem Rande des Glases niederstrich, daß es sang, schwingend und klingend, tanzte der Sand und bildete kleine, feine, regelmäßige Sterne.</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.80">Und wie die Sterne und Formen entstanden, wuchsen und vergingen und wieder erstanden, gedachte er dumpf der Lehre des Buddha Gautama vom Leiden, von der Leidensentstehung, von der Leidensvernichtung, von dem zur Leidensvernichtung führenden Pfad. Da begab es sich, daß durch das löchrige Dach des Zimmers eine Schneeflocke herab auf den Tisch fiel, einen Augenblick verweilte und zerging. – Ein kleiner, feiner, regelmäßiger Stern.</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.81">Wie ein Blitz die Finsternis zerreißt, plötzlich – so war da das Licht der Erkenntnis in das Herz des Alten gefallen:</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.82">
                        <hi rend="spaced" xml:id="tg41.3.82.1">Töne</hi>, unerkannte, unhörbare, jenseitsliegende, sind der Ursprung dieser Flocken, dieser Sterne, sind der Ursprung der Natur, der Ursprung aller Formen, der Wesen und Dinge, sind der Ursprung <hi rend="spaced" xml:id="tg41.3.82.2">dieser</hi> Welt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.83">Nicht ist <hi rend="spaced" xml:id="tg41.3.83.1">diese</hi> Welt die wirkliche Welt: klar ward er sich dessen bewußt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.84">Nicht ist <hi rend="spaced" xml:id="tg41.3.84.1">diese</hi> Welt die wirkliche, nicht entstehende, nicht vergehende, nicht wiederumentstehende Welt: – klar ward er sich dessen bewußt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg41.3.85">
                        <pb n="119" xml:id="tg41.3.85.1"/>
Und klaren bewußten Sinnes erkannte er des Weltalls verborgenen Pulsschlag und das Innere seines Herzens des Abgeklärten, Trieberstorbenen, Wahnversiegten, darinnen die Stille des Meeres herrschte und eine letzte Welle schlafengehend sprang und fiel:</p>
                    <lb xml:id="tg41.3.86"/>
                    <lg>
                        <l xml:id="tg41.3.87">»Der Buddha ist meine Zuflucht ...</l>
                        <l xml:id="tg41.3.88">Der Buddha ist meine Zuflucht.«</l>
                    </lg>
                    <lb xml:id="tg41.3.89"/>
                </div>
            </div>
        </body>
    </text>
</TEI>