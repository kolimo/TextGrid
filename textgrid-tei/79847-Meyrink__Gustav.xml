<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg60" n="/Literatur/M/Meyrink, Gustav/Erzählungen/Des Deutschen Spiessers Wunderhorn/Erster Teil/Coagulum">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Coagulum</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Meyrink, Gustav: Element 00033 [2011/07/11 at 20:27:52]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Gustav Meyrink: Gesammelte Werke, Band 4, Teil 1, München: Albert Langen, 1913.</title>
                        <author key="pnd:118582046">Meyrink, Gustav</author>
                    </titleStmt>
                    <extent>242-</extent>
                    <publicationStmt>
                        <date when="1913"/>
                        <pubPlace>München</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1868" notAfter="1932"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg60.3">
                <div type="h4">
                    <head type="h4" xml:id="tg60.3.1">Coagulum</head>
                    <p xml:id="tg60.3.2">Hamilkar Baldrian, der einsame Sonderling, saß vor seinem Fenster und blickte durch die Scheiben in die herbstliche Dämmerung.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.3">Am Himmel standen dunkelgeballt graublaue Wolken, die langsam ihre Umrisse veränderten, wie das Schattenspiel einer Riesenhand, die sich irgendwo in unsichtbarer Ferne träg bewegte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.4">Über dem frostigen Dunst der Erde ein blindes, trauriges Abendrot.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.5">Dann sanken die Wolken, lagerten schwer im Westen und durch den Nebel spähten die Sterne mit glitzernden Augen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.6">Grübelnd erhob sich Baldrian und schritt auf und ab. Eine schwere Sache das – mit der Geisterbeschwörung! Aber hatte er nicht alles streng befolgt, was das große Grimoire des Honorius vorschrieb?! – Gefastet, gewacht, sich gesalbt und täglich das Seufzerlein der hl. Veronika hergesagt?</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.7">Nein, nein – es <hi rend="spaced" xml:id="tg60.3.7.1">muß</hi> gelingen, der Mensch ist auf Erden das Höchste und die Kraft der Hölle ihm untertan. –</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.8">
                        <pb n="242" xml:id="tg60.3.8.1"/>
Er ging wieder zum Fenster und wartete lange, bis die Hörner des Mondes, gelb und trüb, sich über die erstarrten Äste der Ulmen schoben.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.9">Dann zündete er vor Aufregung zitternd seinen alten Leuchter an und holte allerhand seltsame Dinge aus Schrank und Truhe: Zauberkreise, grünes Wachs, einen Stock mit Krone, trockene Kräuter. Knüpfte alles in ein Bündel, stellte es sorgfältig auf den Tisch und begann, ein Gebet murmelnd, sich langsam auszuziehen, bis er ganz nackt war.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.10">Der flackernde Leuchter warf hämische Reflexe auf den verfallenen Greisenkörper mit der welken, gelblichen Haut, die ölig glänzend sich über den spitzen Knien, Lenden- und Schulterknochen spannte. Der kahle Schädel nickte über der eingesunkenen Brust und sein kugelförmiger, grausiger Schatten fuhr an der kalkweißen Wand unschlüssig umher, als ob er etwas suchen wolle in qualvoller Ungewißheit.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.11">Fröstelnd ging der Alte zum Ofen, hob einen glasierten tönerne Topf herab und löste die raschelnde Hülle, die ihn verschloß; – eine fettige, übelriechende Masse war darin. Heute gerade vor einem Jahr hatte er sie zusammengeschmolzen: – Mandragorawurzel, Bilsenkraut, Wachs und Spermazeti und – und –, er schüttelte sich vor Ekel, – eine zu Brei verkochte Kinderleiche; – die Totenfrau hatte sie ihm verkauft.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.12">
                        <pb n="243" xml:id="tg60.3.12.1"/>
Zögernd grub er seine Finger in das Fett, schmierte es sich auf den Leib, verrieb es in die Kniekehlen und Achselhöhlen, dann wischte er seine Hände auf der Brust ab und zog ein altes vergilbtes Hemd an: das »Erbhemd«, das man zum Zaubern braucht, – und seine Kleider darüber. – Die Stunde war da!</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.13">Ein Stoßgebet. Und das Bündel mit den Geräten her. Nur nichts vergessen, sonst hat der Böse die Macht, den Schatz noch im letzten Augenblick zu verwandeln, wenn Tageslicht darauf fällt. – – Oh, solche Fälle sind schon dagewesen!</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.14">Halt, die Kupferplatte, Kohlenbecken – und Zunder zum Anglimmen!</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.15">Mit unsichern Schritten tappt Baldrian die Treppe hinab.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.16">Das Haus war in früheren Zeiten ein Kloster gewesen, jetzt wohnte er ganz allein darin, und das Waschweib aus der Nachbarschaft brachte ihm tagsüber, was er brauchte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.17">Kreischen und Dröhnen einer schweren eisernen Türe und ein verfallener Raum öffnete sich. –</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.18">Kellergeruch und dicke Spinnweben überall, Schutt in den Ecken und Scherben schimmeliger Blumentöpfe.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.19">Ein paar Hände voll Erde in die Mitte des Raumes getragen – – – – – so! (denn die Füße des Exorzisten müssen auf Erde stehen) – eine alte <pb n="244" xml:id="tg60.3.19.1"/>
Kiste zum Sitzen, den Pergamentkreis ausgebreitet. Mit dem Namen Tetragrammaton nach Norden; sonst kann das größte Unglück geschehen. Jetzt den Zunder und die Kohlen angezündet!</p>
                    <p xml:id="tg60.3.20">– – – – – – – – – – – – – – – – – – – – – – – – – – – –</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.21">Was war das?</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.22">Das Pfeifen von Ratten – nichts sonst.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.23">Kräuter und die Glut: Ginster, Nachtschatten, Stechapfel. – Wie das prasselt und qualmt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.24">Der Alte löscht die Laterne aus, beugt sich über die Pfanne und atmet den giftigen Rauch ein; er kann sich kaum aufrecht halten, so betäubt es ihn.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.25">Und das schreckliche Sausen in den Ohren!</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.26">Mit dem schwarzen Stock berührt er die Wachshäufchen, die auf der Kupferplatte langsam zerschmelzen, und murmelt mit letzter Kraft und stockender Stimme die Beschwörungsformeln des Grimoires:</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.27">»– – – rechte Himmelsbrot und Speise der Engel – – – – Schrecken der Teufel bist – – – – ob ich gleich voll sündigen Unflats – – – – diese reißenden Wölfe und stinkenden Höllenböcke zu bezwingen gewürdiget werde – – – – – Harnisch – – – – zaudert ihr länger vergebens – – – – – – – Aimaymon Astaroth – – – – – diesen Schatz nicht mehr länger zu verwehren – – – – – Astaroth – – – – – <pb n="245" xml:id="tg60.3.27.1"/>
– beschwöre – – – – – Eheye – – – Eschereheye.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.28">Er muß sich niedersetzen, Todesangst befällt ihn; – die drosselnde unbestimmte Furcht dringt durch den Boden und die Mauerritzen, senkt sich von der Decke herab: das grauenhafte Entsetzen, das das Nahesein der haßerfüllten Bewohner der Finsternis verkündet!</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.29">Es pfeifen die Ratten. Nein, nein – – nicht Ratten – – ein gellendes Pfeifen, das den Kopf zersprengt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.30">Das Sausen!</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.31">Es ist das Blut in den Adern. Das Sausen – – – – – von Flügeln. Die Kohlen verglimmen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.32">Da, ha: – – Schatten an der Wand. Der Alte stiert mit gläsernen Augen hin. – – – Moderflecke sind es und abgeschuppter Bewurf.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.33">– – – – Sie bewegen sich, sie bewegen sich – – – –: ein Knochenschädel mit Zähnen – – Hörnern! – – und leere, schwarze Augenhöhlen. Skelettarme schieben sich langsam, geräuschlos nach, ein Ungeheuer wächst aus der Wand – in hockender Stellung, und erfüllt das Gewölbe. Das Gerippe einer riesigen Kröte mit dem Schädel eines Stieres.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.34">Die gebleichten Knochen heben sich fast grell aus der Dunkelheit ab. – – – Der höllische Astaroth!</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.35">
                        <pb n="246" xml:id="tg60.3.35.1"/>
Der Alte hat sich aus dem Zauberkreis in einen Winkel geflüchtet und preßt sich bebend an die kalte Mauer, er kann das rettende Bannwort nicht sagen, die schwarzen, gräßlichen Augenhöhlen verfolgen ihn und starren auf seinen Mund. Sie haben ihm die Zunge gelähmt, – er kann nur mehr röcheln in furchtbarer Angst. –</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.36">Langsam, stetig kriecht das Gespenst auf ihn zu – – (er glaubt das Schlürfen der Rippen auf den Steinen zu hören) – – und hebt tastend die Krötenhand nach ihm. – – – An den Knochenfingern klirren silberne Ringe mit glanzlosen verstaubten Topasen, vermoderte Schwimmhäute verbinden lose die Glieder und strömen einen entsetzlichen Geruch aus nach verwestem Fleisch.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.37">Jetzt – – faßt es ihn an. – – Eisige Kälte steigt ihm ins Herz. – – Er will – will – –, da schwinden die Sinne, und er fällt vornüber aufs Gesicht. – –</p>
                    <p xml:id="tg60.3.38">– – – – – – – – – – – – – – – – – – – – – – – – – – – –</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.39">Die Kohlen sind erloschen, narkotischer Rauch hängt in der Luft und ballt sich längs der Decke. Durch das vergitterte, winzige Kellerfenster wirft das Mondlicht gelbe schräge Strahlen in den Winkel, wo der Alte bewußtlos liegt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.40">Baldrian träumt, daß er fliege. Sturmwind peitscht ihm den Leib. Ein schwarzer Bock rast vor <pb n="247" xml:id="tg60.3.40.1"/>
ihm durch die Luft, er fühlt die zottigen Läufe dicht vor seinen Augen, und die tollen Hufe schlagen ihm fast ins Gesicht.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.41">Unter ihm die Erde, – weit, weit. Dann fällt er, wie durch einen schwarzsamtnen Trichter, immer tiefer und schwebt über einer Landschaft. Er kennt sie gut: Dort der moosbewachsene Grabstein, – auf dem Erdbuckel der kahle Ahorn mit den entblätterten Ästen, die sich wie fleischlose Arme zum Himmel krampfen. Herbstlicher Reif auf dem nächtlichen Sumpfgras.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.42">Das Moorwasser steht seicht im Boden und schimmert durch den Nebel wie ein großes erblindetes Auge.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.43">Sind das nicht Gestalten in dunklen Hüllen, die dort im Schatten des Grabsteines sich sammeln mit blitzenden Waffen und metallfunkelnden Köpfen und Spangen?! Sie lagern sich im Halbkreis zu einer gespenstischen Beratung.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.44">Des Alten Seele durchzuckt ein Gedanke: Der Schatz! Die Schemen der Toten sind's, die einen vergrabenen Schatz hüten! Und sein Herz stockt vor Habgier.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.45">Er späht hinab von seiner Höhle, – immer näher rückt die Erde, jetzt klammert er sich an den Zweigen des Ahorns an, leise – leise. –</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.46">
                        <pb n="248" xml:id="tg60.3.46.1"/>
Da. – Ein dürrer Ast biegt sich und ächzt. – Die Toten schauen zu ihm empor. – – – Er kann sich nicht mehr halten und fällt – fällt mitten unter sie.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.47">Sein Kopf schlägt hart auf den Grabstein.</p>
                    <lb xml:id="tg60.3.48"/>
                    <milestone unit="hi_start"/>
                    <p rend="zenoPC" xml:id="tg60.3.49">
                        <hi rend="superscript" xml:id="tg60.3.49.1">*</hi>
                        <hi rend="subscript" xml:id="tg60.3.49.2">*</hi>
                        <hi rend="superscript" xml:id="tg60.3.49.3">*</hi>
                    </p>
                    <milestone unit="hi_end"/>
                    <lb xml:id="tg60.3.50"/>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.51">Er erwacht und sieht die Moderflecke an der Wand. Keuchend taumelt er zur Türe, die Treppe hinauf mit brechenden Knien.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.52">Er wirft sich auf das Bett, – – seine zahnlosen Kiefer schlottern vor Furcht und Kälte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.53">Die rote filzige Decke legt sich um ihn, raubt ihm den Atem, bedeckt ihm Mund und Augen. Er will sich umdrehen und kann nicht, auf seiner Brust hockt ein wolliges, scheußliches Tier: die Fledermaus des Fieberschlafs, mit riesigen purpurnen Flügeln, und hält ihn mit ihrer Last unwiderstehlich in die dumpfig schmutzigen Polster gepreßt.</p>
                    <p xml:id="tg60.3.54">– – – – – – – – – – – – – – – – – – – – – – – – – – – –</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.55">Den ganzen langen Winter lag der Greis an den Folgen dieser Nacht danieder. Langsam ging es mit ihm zu Ende.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.56">Er sah von seiner Lagerstätte zu dem kleinen Fenster hinüber, wenn die Schneeflocken im Sturm vorbeiflogen und ungeduldige Tänze aufführten, oder<pb n="249" xml:id="tg60.3.56.1"/>
 empor zur weißen Zimmerdecke, auf der ein paar Fliegen ihre planlosen Wanderungen hielten.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.57">Und wenn von dem alten Kachelofen her es gar so gut nach verbrannten Wacholderbeeren roch, (»Kreche, Kreche« – ach wie er husten mußte) da malte er sich aus, wie er im Frühjahr draußen beim Heidegrab den Schatz heben werde, von dem er geträumt, und fürchtete nur, daß sich das Geld vielleicht doch verwandeln könne, denn so ganz in Ordnung war die Beschwörung des Astaroth ja nicht gewesen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.58">Einen genauen Plan hatte er auf einem abgerissenen Buchdeckel gezeichnet: den einsamen Ahornbaum, den kleinen Moorweiher und hier † den Schatz, – ganz in der Nähe des verwitterten Grabsteines, den jedes Kind kennt.</p>
                    <lb xml:id="tg60.3.59"/>
                    <milestone unit="hi_start"/>
                    <p rend="zenoPC" xml:id="tg60.3.60">
                        <hi rend="superscript" xml:id="tg60.3.60.1">*</hi>
                        <hi rend="subscript" xml:id="tg60.3.60.2">*</hi>
                        <hi rend="superscript" xml:id="tg60.3.60.3">*</hi>
                    </p>
                    <milestone unit="hi_end"/>
                    <lb xml:id="tg60.3.61"/>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.62">Der Buchdeckel lag auf dem Bürgermeisteramt und Hamilkar Baldrian auf dem Friedhof draußen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.63">»Einen Millionenschatz hat der Alte entdeckt, er war nur zu schwer gewesen, daß er ihn hätte ausgraben können,« lief das Gerücht durch das Städtchen und man beneidete seinen Neffen, den Erben, einen Schriftsteller.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.64">Die Grabungen begannen, die Stelle war im Plane so deutlich bezeichnet.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.65">
                        <pb n="250" xml:id="tg60.3.65.1"/>
Einige Spatenstiche nur – – – da – – da: <hi rend="spaced" xml:id="tg60.3.65.2">Hurra, hurra, hurra! eine eiserne, rostbedeckte Kassette!</hi>
                    </p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.66">In Triumph wurde sie auf die Amtsstube getragen. Berichte gingen in die Hauptstadt, der Erbe sei von dem Funde zu verständigen, eine Kommission an Ort und Stelle zu entsenden usw. – usw.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.67">Der kleine Bahnhof wimmelte von Menschen, Beamten in Uniform, Reportern, Detektiven, Amateurphotographen, ja, sogar der Herr Landesmuseumsdirektor war angekommen, um den interessanten Fleck Erde zu besichtigen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.68">Alles zog hinaus auf die Heide und glotzte stunden lang in das frisch gegrabene Loch, vor dem der Flurschütz Wache hielt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.69">Das saftige Moorgras war zertreten von den vielen gekerbten Gummischuhen, aber die hellgrünen Weihersträucher in ihrem jugendfrischen Frühlingsschmuck blinzelten einander mit den seidenen Weidenkätzchen listig zu, und wenn ein Windstoß kam, krümmten sie sich in plötzlich ausbrechendem stummen Gelächter, daß ihre Häupter die Wasserfläche berührten. Warum wohl?</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.70">Auch die Krötenkönigin, die dicke mit der rotgetupften Weste, die in ihrer Veranda aus Ranunculus und Pfeilkraut die süße Maienluft genoß und doch sonst immer so würdevoll tat, weil sie 100,003 <pb n="251" xml:id="tg60.3.70.1"/>
Jahre alt war, hatte heute wahre Anfälle von Lachkrämpfen. Sie riß das Maul auf, daß ihre Augen ganz verschwanden und schlenkerte wie besessen die linke Hand in die Luft. Fast wäre ihr dabei ein silberner Topasring vom Finger gefallen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.71">Unterdessen war von der Kommission die gefundene Kassette geöffnet worden.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.72">Ein fauler Geruch entströmte ihr, so daß im ersten Augenblick alles zurückprallte. Seltsamer Inhalt!</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.73">Eine elastische Masse, in allen Farben spielend, zäh und von glänzender Oberfläche. –</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.74">Es wurde hin- und hergeraten und der Kopf geschüttelt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.75">»Ein alchimistisches Präparat – offenbar,« sagte endlich der Herr Landesmuseumsdirektor.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.76">»<hi rend="spaced" xml:id="tg60.3.76.1">Alchimistisch</hi>, – <hi rend="spaced" xml:id="tg60.3.76.2">alchimistisch</hi>,« lief es von Mund zu Mund.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.77">»Alchimistisch? Wie schreibt man das? – Mit zwei L?« drängte sich ein Zeitungsmensch vor.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.78">»Nebbich, ä Düngermittel,« murmelte ein anderer vor sich hin.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.79">Die Kassette wurde wieder verschlossen und an das wissenschaftliche Institut für Chemie und Physik mit dem Ersuchen um ein allgemeinverständliches Gutachten gesandt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.80">Alle weiteren Nachgrabungen in der Moorheide blieben erfolglos.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.81">
                        <pb n="252" xml:id="tg60.3.81.1"/>
Auch die verwitterte Grabschrift auf dem Stein gab keinen Aufschluß: »Teutobold Mucker ††† allteutscher Kunstkritiker«?? darunter eingemeißelt zwei gekreuzte Fußtritte, die sich wahrscheinlich auf irgendein verschleiertes Ereignis im Leben des Verblichenen bezogen?</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.82">Offenbar war der Mann eine neue Art Heldentod gestorben.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.83">Die geringen Mittel des erbenden Schriftstellers waren durch die Kosten gänzlich zusammengeschmolzen, und den Rest gab ihm das wissenschaftliche Gutachten, daß nach drei Monaten eintraf:</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.84">Zuerst einige Seiten hindurch die unternommenen vergeblichen Versuche angeführt, dann die Eigenschaften der rätselhaften Materie aufgezählt und zum Schluß das Resultat, daß die Masse in keiner Hinsicht in die Zahl der bisher bekannten Stoffe eingereiht werden könne.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.85">Also wertlos! – Die Kassette keinen Heller wert!</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.86">Am selben Abend noch setzte der Herbergswirt den armen Schriftsteller vor die Tür. – Die Schatzaffäre schien abgetan.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.87">Doch noch eine ganz kleine Aufregung sollte dem Städtchen blühen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.88">Am nächsten Morgen rannte der Dichter ohne Hut mit wallenden Locken durch die Straßen zum Magistrat.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.89">
                        <pb n="253" xml:id="tg60.3.89.1"/>
»Ich weiß es,« schrie er immerfort, »ich weiß es.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.90">Man umringte ihn. »Was wissen Sie?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.91">»Ich habe heute auf dem Moor übernachtet,« keuchte der Dichter atemlos, – »übernachtet – uch – da ist mir ein Geist erschienen und hat mir gesagt, was es ist. – Früher – uch – sind dort draußen so viele geheime Versammlungen abgehalten worden – uch – und da – uch – – –«</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.92">»Zum Teufel, was ist's also mit der Materie?« rief einer.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.93">Der Dichter fuhr fort:</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.94">»– spezifisches Gewicht 23, glänzende Außenseite, zweifarbig, in allen kleinsten Teilen gebrochen und dabei zusammenklebend wie Pech, – ungemein dehnbar, penetranter – – –«</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.95">Die Menge wurde ungeduldig. Aber das stand ja doch schon in der wissenschaftlichen Analyse!</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.96">»Also, der Geist sagte mir, es sein ein <hi rend="spaced" xml:id="tg60.3.96.1">fossiles</hi> Gutachten, wie künftighin die Dichtkunst zu handhaben sei, abgegeben von deutschtuenden Frömmlern und Revolverjaurnalisten. – Und ich habe gleich an ein Bankhaus geschrieben, um dieses Kuriosum zu Geld zu machen.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.97">Da schwiegen sie, griffen ihn und sahen, daß er irre redete.</p>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.98">Wer weiß, ob der Ärmste nicht mit der Zeit wieder<pb n="254" xml:id="tg60.3.98.1"/>
 vernünftig geworden wäre, als aber die Antwort auf seinen Brief kam:</p>
                    <lb xml:id="tg60.3.99"/>
                    <p rend="zenoPLm4n0" xml:id="tg60.3.100">»Wir bedauern, Ihnen mitteilen zu müssen, quästionierten Artikel weder lombardieren noch per komptant acquirieren zu können, da wir kein Wertobjekt in demselben, auch wenn er nicht fossil und koaguliert wäre, – zu erblicken vermögen. Wollen Sie sich immerhin an ein Haus zur Verwertung von Abfallstoffen wenden.</p>
                    <lb xml:id="tg60.3.101"/>
                    <milestone unit="hi_start"/>
                    <p rend="zenoPC" xml:id="tg60.3.102">Hochachtend</p>
                    <milestone unit="hi_end"/>
                    <milestone unit="hi_start"/>
                    <p rend="zenoPR" xml:id="tg60.3.103">Bankhaus A.B.C. Wucherstein Nachfolger.«</p>
                    <milestone unit="hi_end"/>
                    <lb xml:id="tg60.3.104"/>
                    <lg>
                        <l rend="zenoPLm4n0" xml:id="tg60.3.105">– da schnitt er sich die Kehle durch. –</l>
                        <l rend="zenoPLm4n0" xml:id="tg60.3.106">Jetzt ruht er neben seinem Onkel Hamilkar Baldrian.</l>
                        <pb n="255" xml:id="tg60.3.107"/>
                    </lg>
                </div>
            </div>
        </body>
    </text>
</TEI>