<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg106" n="/Literatur/M/Meyrink, Gustav/Erzählungen/Des Deutschen Spiessers Wunderhorn/Zweiter Teil/G.M.">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>G.M.</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Meyrink, Gustav: Element 00060 [2011/07/11 at 20:27:52]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Gustav Meyrink: Gesammelte Werke, Band 4, Teil 2, München: Albert Langen, 1913.</title>
                        <author key="pnd:118582046">Meyrink, Gustav</author>
                    </titleStmt>
                    <extent>253-</extent>
                    <publicationStmt>
                        <date when="1913"/>
                        <pubPlace>München</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1868" notAfter="1932"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg106.3">
                <div type="h4">
                    <head type="h4" xml:id="tg106.3.1">
                        <pb n="253" xml:id="tg106.3.1.1"/>
G.M.</head>
                    <p xml:id="tg106.3.2">»Makintosh ist wieder hier, das Mistviech.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.3">Ein Lauffeuer ging durch die Stadt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.4">George Makintosh, den Deutschamerikaner, der vor fünf Jahren allen adieu gesagt, hatte jeder noch gut im Gedächtnis, – seine Streiche konnte man gerade sowenig vergessen wie das scharfe, dunkle Gesicht, das heute wieder auf dem »Graben« aufgetaucht war. –</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.5">Was will denn der Mensch schon wieder hier?</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.6">Langsam, aber sicher war er damals weggeekelt worden; – alle hatten daran mitgearbeitet, – der mit der Miene der Freundschaft, jener mit Tücke und falschen Gerüchten, aber jeder mit einem Quentchen vorsichtiger Verleumdung – und alle diese kleinen Niederträchtigkeiten ergaben schließlich zusammen eine so große Gemeinheit, daß sie jeden anderen Mann wahrscheinlich zerquetscht hätte, den Amerikaner aber nur zu einer Abreise bewog. – – –</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.7">Makintosh hatte ein Gesicht, scharf wie ein Papiermesser, und sehr lange Beine. Das allein schon vertragen die Menschen schlecht, die die Rassentheorie so gerne mißachten.</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.8">
                        <pb n="254" xml:id="tg106.3.8.1"/>
Er war schrecklich verhaßt, und anstatt diesen Haß zu verringern, indem er sich landläufigen Ideen angepaßt hätte, stand er stets abseits der Menge und kam alle Augenblicke mit etwas neuem: – Hypnose, Spiritismus, Handlesekunst, ja eines Tages sogar mit einer symbolischen Erklärung des Hamlet. – Das mußte natürlich die guten Bürger aufbringen und ganz besonders keimende Genies, wie z.B. den Herrn Tewinger vom Tageblatt, der soeben ein Buch unter dem Titel »Wie ich über Shakespeare denke« herausgeben wollte.</p>
                    <p xml:id="tg106.3.9">– – – – – – – – – – – – – – – – – – – – – – – – – – – –</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.10">Und dieser »Dorn im Auge« war wieder hier und wohnte mit seiner indischen Dienerschaft in der »roten Sonne«.</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.11">»Wohl nur vorübergehend?« forschte ihn ein alter Bekannter aus.</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.12">»Natürlich: vorübergehend, denn ich kann mein Haus ja erst am 15. August beziehen. – Ich habe mir nämlich ein Haus in der Ferdinandstraße gekauft.« –</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.13">Das Gesicht der Stadt wurde um einige Zoll länger: – Ein Haus in der Ferdinandstraße! – Woher hat dieser Abenteurer das Geld?! –</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.14">Und noch dazu eine indische Dienerschaft. – Na, werden ja sehen, wie lange er machen wird! – –</p>
                    <p xml:id="tg106.3.15">– – – – – – – – – – – – – – – – – – – – – – – – – – – –</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.16">
                        <pb n="255" xml:id="tg106.3.16.1"/>
Mackintosch hatte natürlich schon wieder etwas Neues: Eine elektrische Maschinerie, mit der man Goldadern in der Erde sozusagen wittern könne, – eine Art moderner wissenschaftlicher Wünschelrute.</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.17">Die meisten glaubten es selbstverständlich nicht: »Wenn es gut wäre, hätten das doch schon andere erfunden!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.18">Nicht wegzuleugnen war aber, daß der Amerikaner während der fünf Jahre ungeheuer reich geworden sein mußte. Wenigstens behauptete dies das Auskunftsbureau der Firma Schnufflers Eidam steif und fest.</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.19">– – Und richtig, es verging auch keine Woche, daß er nicht ein neues Haus gekauft hätte. –</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.20">Ganz planlos durcheinander; eines auf dem Obstmarkt, dann wieder eines in der Herrengasse, – aber alle in der inneren Stadt. –</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.21">Um Gottes willen, will er es vielleicht bis zum Bürgermeister bringen?</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.22">Kein Mensch konnte daraus klug werden. –</p>
                    <p xml:id="tg106.3.23">– – – – – – – – – – – – – – – – – – – – – – – – – – – –</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.24">»Haben Sie schon seine Visitenkarte gesehen? Da schauen Sie her, das ist denn doch schon die höchste Frechheit, – bloß ein Monogramm, – gar kein Name! – Er sagt, er brauche nicht mehr zu <hi rend="spaced" xml:id="tg106.3.24.1">heißen</hi>, er hätte Geld genug!«</p>
                    <p xml:id="tg106.3.25">– – – – – – – – – – – – – – – – – – – – – – – – – – – –</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.26">
                        <pb n="256" xml:id="tg106.3.26.1"/>
Makintosh war nach Wien gefahren und verkehrte dort, wie das Gerücht ging, mit einer Reihe Abgeordneten, die täglich um ihn waren.</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.27">Was er mit ihnen gar so wichtig tat, konnte man nicht und nicht herausbekommen, aber offenbar hatte er seine Hand bei dem neuen Gesetzentwurf über die Umänderung der Schürfrechte im Spiele.</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.28">Täglich stand etwas in den Zeitungen, – Debatten für und wider, – und es sah ganz danach aus, als ob das Gesetz, daß man hinfort – natürlich nur außer gewöhnlichen Vorkommnissen – auch mitten in den Städten Freischürfe errichten dürfe, recht bald angenommen werden würde.</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.29">Die Geschichte sah merkwürdig aus, und die allgemeine Meinung lautete, daß wohl irgendeine große Kohlengewerkschaft dahinter stecken müsse.</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.30">Makintosh allein hatte doch gewiß kein so starkes Interesse daran, – wahrscheinlich war er nur von irgendeiner Gruppe vorgeschoben. – – – – –</p>
                    <p xml:id="tg106.3.31">– – – – – – – – – – – – – – – – – – – – – – – – – – – –</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.32">Er reiste übrigens bald nach Hause zurück und schien ganz vortrefflicher Laune. So freundlich hatte man ihn noch nie gesehen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.33">»Es geht ihm aber auch gut, – erst gestern hat er sich wieder eine ›Realität‹ gekauft, – es ist jetzt die dreizehnte,« – erzählte beim Beamtentische im Kasino der Herr Oberkontrolleur vom Grundbuchsamt. <pb n="257" xml:id="tg106.3.33.1"/>
– »Sie kennen's ja: das Eckhaus ›zur angezweifelten Jungfrau‹ schräg <hi rend="italic" xml:id="tg106.3.33.2">vis-à-vis</hi> von den ›drei eisernen Trotteln‹, wo jetzt die städtische Befundhauptkommission für die Inundations-Bezirkswasserbeschau drin ist.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.34">»Der Mann wird sich noch verspekulieren und so,« meinte da der Herr Baurat, – »wissen Sie, um was er jetzt wieder angesucht hat, meine Herren? – Drei von seinen Häusern will er einreißen lassen, das in der Perlgasse – das vierte rechts neben dem Pulverturm – und das <hi rend="italic" xml:id="tg106.3.34.1">Numero conscriptionis</hi> 47184/II. – Die neuen Baupläne sind schon bewilligt!« –</p>
                    <lb xml:id="tg106.3.35"/>
                    <milestone unit="hi_start"/>
                    <p rend="zenoPC" xml:id="tg106.3.36">
                        <hi rend="superscript" xml:id="tg106.3.36.1">*</hi>
                        <hi rend="subscript" xml:id="tg106.3.36.2">*</hi>
                        <hi rend="superscript" xml:id="tg106.3.36.3">*</hi>
                    </p>
                    <milestone unit="hi_end"/>
                    <lb xml:id="tg106.3.37"/>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.38">Alles sperrte den Mund auf.</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.39">Durch die Straßen jagte der Herbstwind, – die Natur atmete tief auf, ehe sie schlafen geht.</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.40">Der Himmel ist so blau und kalt, und die Wolken so backig und stimmungsvoll, als hätte sie der liebe Gott eigens vom Meister Wilhelm Schulz malen lassen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.41">O, wie wäre die Stadt so schön und rein, wenn der ekelhafte Amerikaner mit seiner Zerstörungswut nicht die klare Luft mit dem feinen Mauerstaub so vergiftet hätte. – – Das aber auch so etwas bewilligt wird!</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.42">
                        <pb n="258" xml:id="tg106.3.42.1"/>
Drei Häuser einreißen, na gut, – aber alle dreizehn gleichzeitig, da hört sich denn doch alles auf.</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.43">Jeder Mensch muß ja schon husten, und wie weh das tut, wenn einem das verdammte Ziegelpulver in die Augen kommt. – –</p>
                    <p xml:id="tg106.3.44">– – – – – – – – – – – – – – – – – – – – – – – – – – – –</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.45">»Das wird ein schön verrücktes Zeug werden, was er uns dafür aufbauen wird. – ›Sezession‹ natürlich, – ich möchte darauf wetten,« hieß es. –</p>
                    <p xml:id="tg106.3.46">– – – – – – – – – – – – – – – – – – – – – – – – – – – –</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.47">»Sie müssen wirklich nicht recht gehört haben, Herr Schebor! – Was?! gar nichts will er dafür hinbauen? – Ist er denn irrsinnig geworden, – wozu hätte er denn dann die neuen Baupläne eingereicht?« –</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.48">– – – – »Bloß damit ihm vorläufig die Bewilligung zum Einreißen der Häuser erteilt wird!«</p>
                    <lg>
                        <l xml:id="tg106.3.49">– – – – – – – – – – – ? ? ? ? ? ? – – – – – – – – – – –</l>
                        <l xml:id="tg106.3.50">– – – – – – – – – – – – – – – – – – – – – – – – – – – –</l>
                    </lg>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.51">»Meine Herren, wissen Sie das Neueste schon?« der Schloßbauaspirant Vyskotschil war ganz außer Atem: »Gold in der Stadt, ja wohl! – Gold! Vielleicht grad' hirr zu unsrrn Fißen.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.52">Alles sah auf die Füße des Herrn von Vyskotschil, die flach wie Biskuits in den Lackstiefeln staken.</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.53">Der ganze »Graben« lief zusamen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.54">
                        <pb n="259" xml:id="tg106.3.54.1"/>
»Wer hat da was gesaagt von Gold!« rief der Herr Kommerzienrat Steißbein.</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.55">»Mr. Makintosh will goldhaltiges Gestein in dem Bodengrund seines niedergerissenen Hauses in der Perlgasse gefunden haben,« bestätigte ein Beamter des Bergbauamtes, »man hat sogar telegraphisch eine Kommission aus Wien berufen.«</p>
                    <p xml:id="tg106.3.56">– – – – – – – – – – – – – – – – – – – – – – – – – – – –</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.57">Einige Tage später war George Makintosh der gefeiertste Mann der Stadt. In allen Läden hingen Photographien von ihm, – mit dem kantigen Profil und dem höhnischen Zug um die schmalen Lippen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.58">Die Blätter brachten seine Lebensgeschichte, die Sportberichterstatter wußten plötzlich genau sein Gewicht, seinen Brust- und Bizepsumfang, ja sogar, wie viel Luft seine Lunge fasse.</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.59">Ihn zu interviewen war auch gar nicht schwer.</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.60">Er wohnte wieder im Hotel »Zur roten Sonne«, ließ jedermann vor, bot die wundervollsten Zigarren an und erzählte mit entzückender Liebenswürdigkeit, was ihn dazu geführt hatte, seine Häuser einzureißen und in den freigewordenen Baugründen nach Gold zu graben:</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.61">Mit seinem neuen Apparat, der durch Steigen und Fallen der elektrischen Spannung genau das Vorhandensein von Gold unter der Erde anzeige <pb n="260" xml:id="tg106.3.61.1"/>
und der seinem eigenen Gehirn entsprungen sei, hätte er nachts nicht nur die Keller <hi rend="spaced" xml:id="tg106.3.61.2">seiner</hi> Gebäude genau durchforscht, sondern auch die aller seiner Nachbarhäuser, in die er sich heimlich Zutritt zu verschaffen gewußt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.62">»Sehen Sie, da haben Sie auch die amtlichen Berichte des Bergbauamtes und das Gutachten des eminenten Sachverständigen Professor Senkrecht aus Wien, der übrigens ein alter guter Freund von mir ist.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.63">– – – – Und richtig, da stand schwarz auf weiß, mit dem amtlichen Stempel beglaubigt, daß sich in sämtlichen dreizehn Bauplätzen, die der Amerikaner George Makintosh käuflich erworben, Gold in der dem Sande beigemengten, bekannten Form gefunden habe, und zwar in einem Quotienten, der auf eine immense Menge Gold besonders in den unteren Schichten mit Sicherheit schließen lasse. Diese Art des Vorkommens sei bis jetzt nur in Amerika und Asien nachgewiesen worden, doch könne man der Ansicht des Mr. Makintosh, daß es sich hier offenkundig um ein altes Flußbett der Vorzeit handle, ohne weiteres beipflichten. Eine genaue Rentabilität lasse sich ziffernmäßig natürlich nicht ausführen, aber daß hier ein Metallreichtum erster Stärke, ja vielleicht ein ganz beispielloses Lager verborgen liege, sei wohl außer Zweifel.</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.64">
                        <pb n="261" xml:id="tg106.3.64.1"/>
Besonders interessant war der Plan, den der Amerikaner von der mutmaßlichen Ausdehnung der Goldmine entworfen und der die vollste Anerkennung der sachverständigen Kommission gefunden hatte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.65">Da sah man deutlich, daß sich das ehemalige Flußbett von einem Haus des Amerikaners anfangend zu den übrigen in komplizierten Windungen gerade unter den Nachbarhäusern hinzog, um wieder bei einem Eckhause Makintoshs in der Zeltnergasse in der Erde zu verschwinden. –</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.66">Die Beweisführung, daß es so und nicht anders sein konnte, war so einfach und klar, daß sie jedem, – selbst wenn er nicht an die Präzision der elektrischen Metallkonstatierungsmaschine glauben wollte – einleuchten mußte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.67">– – – – War das ein Glück, daß das neue Schurfrecht bereits Gesetzeskraft erlangt hatte. –</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.68">Wie umsichtig und verschwiegen der Amerikaner aber auch alles vorgesehen hatte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.69">Die Hausherren, in deren Grund und Boden plötzlich solche Reichtümer staken, saßen aufgeblasen in den Kaffees und waren des Lobes voll über ihren findigen Nachbarn, den man früher so grundlos und niederträchtig verleumdet hatte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.70">»Pfui über solche Ehrabschneider!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.71">
                        <pb n="262" xml:id="tg106.3.71.1"/>
Jeden Abend hielten die Herren lange Versammlungen und berieten sich mit dem Advokaten des engeren Komitees, was nunmehr geschehen solle.</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.72">»Ganz einfach! – Alles genau dem Mr. Makintosh nachmachen,« meinte der, »neue x-beliebige Baupläne überreichen, wie es das Gesetz verlangt, dann einreißen, einreißen, einreißen, damit man so rasch wie möglich auf den Grund kommt. – Anders geht es nicht, denn schon jetzt in den Kellern nachzugraben, ist nutzlos und übrigens nach § 47a Unterabteilung Y gebrochen durch römisch XXIII unzulässig.« – –</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.73">– – – – Und so geschah es. –</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.74">Der Vorschlag eines überklugen ausländischen Ingenieurs, sich erst zu überzeugen, ob nicht Makintosh am Ende gar den Goldfund auf die Fundstellen heimlich habe hinschaffen lassen, um die Kommission zu täuschen, – wurde niedergelächelt.</p>
                    <p xml:id="tg106.3.75">– – – – – – – – – – – – – – – – – – – – – – – – – – – –</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.76">Ein Gehämmer und Gekrach in den Straßen, das Fallen der Balken, das Rufen der Arbeiter und das Rasseln der Schuttwagen, dazu der verdammte Wind, der den Staub in dichten Wolken umherblies! Es war zum Verstandverlieren.</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.77">Die ganze Stadt hatte Augenentzündung, die Vorzimmer der Augenklinik platzten fast vor dem Andrang der Patienten, und eine neue Broschüre <pb n="263" xml:id="tg106.3.77.1"/>
des Professors Wochenschreiber »über den befremdenden Einfluß moderner Bautätigkeit auf die menschliche Hornhaut« war binnen weniger Tage vergriffen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.78">Es wurde immer ärger.</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.79">Der Verkehr stockte. In dichter Menge belagerte das Volk die »Rote Sonne«, und jeder wollte den Amerikaner sprechen, ob er denn nicht glaube, daß sich auch unter andern Gebäuden als den im Plan bezeichneten – Gold finden müsse.</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.80">Militärpatrouillen zogen umher, an allen Straßenecken klebten die Kundmachungen der Behörden, daß vor Eintreffen der Ministerialerlässe strengstens verboten sei, noch andere Häuser niederzureißen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.81">Die Polizei ging mit blanker Waffe vor: kaum, daß es nützte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.82">Gräßliche Fälle von Geistesstörung wurden bekannt: In der Vorstadt war eine Witwe nachts und im Hemde auf das eigene Dach geklettert und hatte unter gellem Gekreisch die Dachziegel von den Balken ihres Hauses gerissen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.83">Junge Mütter irrten wie trunken umher, und arme verlassene Säuglinge vertrockneten in den einsamen Stuben.</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.84">Ein Dunst lag über der Stadt, – dunkel, als ob der Dämon Gold seine Fledermausflügel ausgebreitet hätte.</p>
                    <p xml:id="tg106.3.85">– – – – – – – – – – – – – – – – – – – – – – – – – – – –</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.86">
                        <pb n="264" xml:id="tg106.3.86.1"/>
Endlich, endlich war der große Tag gekommen. Die früher so herrlichen Bauten waren verschwunden, wie aus dem Boden gerissen, und ein Heer von Bergknappen hatte die Maurer abgelöst.</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.87">Schaufel und Spitzhaue flogen.</p>
                    <p xml:id="tg106.3.88">– – – – – – – – – – – – – – – – – – – – – – – – – – – –</p>
                    <lg>
                        <l rend="zenoPLm4n0" xml:id="tg106.3.89">Von Gold – – keine Spur! – Es mußte also wohl tiefer liegen, als man vermutet hatte.</l>
                    </lg>
                    <lg>
                        <l xml:id="tg106.3.90">– – – – – – – – – – – – – – – – – – – – – – – – – – – –</l>
                    </lg>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.91">– – – – Da! – – ein seltsames riesengroßes Inserat in den Tagesblättern: –</p>
                    <lb xml:id="tg106.3.92"/>
                    <milestone unit="hi_start"/>
                    <p rend="zenoPLm4n4" xml:id="tg106.3.93">
                        <seg rend="zenoTXFontsize80" xml:id="tg106.3.93.1">»<hi rend="spaced" xml:id="tg106.3.93.1.1">George Makintosh an seine teu</hi>
                            <hi rend="spaced" xml:id="tg106.3.93.1.2">ern Bekannten und die ihm so </hi>
                            <hi rend="spaced" xml:id="tg106.3.93.1.3">liebgewordene Stadt</hi>!</seg>
                    </p>
                    <p rend="zenoPLm4n4" xml:id="tg106.3.94">
                        <seg rend="zenoTXFontsize80" xml:id="tg106.3.94.1"> – – – – – – – – – – – – – – – – – – – – – – – – – – – –</seg>
                    </p>
                    <p rend="zenoPLm4n4" xml:id="tg106.3.95">
                        <seg rend="zenoTXFontsize80" xml:id="tg106.3.95.1"> Umstände zwingen mich, allen für immer Lebewohl zu sagen.</seg>
                    </p>
                    <p rend="zenoPLm4n4" xml:id="tg106.3.96">
                        <seg rend="zenoTXFontsize80" xml:id="tg106.3.96.1"> Ich schenke der Stadt hiermit den großen Fesselballon, den ihr heute nachmittags auf dem Josefsplatz das erstemal aufsteigen sehen und jederzeit zu meinem Gedächtnisse umsonst benutzen könnt. Jeden einzelnen der Herren nochmals zu besuchen, fiel mir schwer, darum lasse ich in der Stadt eine – <hi rend="spaced" xml:id="tg106.3.96.1.1">große Visitenkarte</hi> zurück.«</seg>
                    </p>
                    <milestone unit="hi_end"/>
                    <lb xml:id="tg106.3.97"/>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.98">
                        <pb n="265" xml:id="tg106.3.98.1"/>
»Also doch wahnsinnig!</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.99">›Visitenkarte in der <hi rend="spaced" xml:id="tg106.3.99.1">Stadt</hi> zurücklassen!‹ Heller Unsinn!</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.100">Was soll denn das Ganze überhaupt heißen? Verstehen Sie das vielleicht?« – So rief man allenthalben.</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.101">»Befremdend ist nur, daß der Amerikaner vor acht Tagen seine sämtlichen Bauplätze heimlich verkauft hat!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.102">– Der Photograph Maloch war es, der endlich Licht in das Rätsel brachte; er hatte als ersten den Aufstieg mit dem angekündigten Fesselballon mitgemacht und die Verwüstungen der Stadt von der Vogelperspektive aufgenommen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.103">Jetzt hing das Bild in seinem Schaufenster, und die Gasse war voll Menschen, die es betrachten wollten.</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.104">Was sah man da?</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.105">Mitten aus dem dunklen Häusermeer leuchteten die leeren Grundflächen der zerstörten Bauten in weißem Schutt und bildeten ein zackiges Geschnörkel:</p>
                    <lb xml:id="tg106.3.106"/>
                    <milestone unit="hi_start"/>
                    <p rend="zenoPC" xml:id="tg106.3.107">
                        <hi rend="italic" xml:id="tg106.3.107.1">»G M«</hi>
                    </p>
                    <milestone unit="hi_end"/>
                    <lb xml:id="tg106.3.108"/>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.109">Die Initialen des Amerikaners!</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.110">– – – – Die meisten Hausherren hat der Schlag getroffen, bloß dem alten Herrn Kommerzialrat <pb n="266" xml:id="tg106.3.110.1"/>
Schlüsselbein war es ganz wurst. Sein Haus war sowieso baufällig gewesen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.111">Er rieb sich nur ärgerlich die entzündeten Augen und knurrte:</p>
                    <p rend="zenoPLm4n0" xml:id="tg106.3.112">»Ich hab's ja immer gesagt, für was Ernstes hat der Makintosh nie ä Sinn gehabt.«</p>
                </div>
            </div>
        </body>
    </text>
</TEI>