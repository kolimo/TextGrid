<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg168" n="/Literatur/M/Mühsam, Erich/Lyrik und Prosa/Sammlung 1898-1928/Zweiter Teil: Prosa/Selbstbiographie">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Selbstbiographie</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Mühsam, Erich: Element 00182 [2011/07/11 at 20:31:07]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Erich Mühsam: Ausgewählte Werke, Bd.1: Gedichte. Prosa. Stücke, Bd. 2: Publizistik. Unpolitische Erinnerungen, Berlin: Volk und Welt, 1978.</title>
                        <author key="pnd:118584758">Mühsam, Erich</author>
                    </titleStmt>
                    <extent>165-</extent>
                    <publicationStmt>
                        <date when="1978"/>
                        <pubPlace>Berlin</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1878" notAfter="1934"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <front>
            <div type="front">
                <head type="h4" xml:id="tg168.3.1">Selbstbiographie</head>
            </div>
        </front>
        <body>
            <div subtype="work:no" xml:id="tg169" n="/Literatur/M/Mühsam, Erich/Lyrik und Prosa/Sammlung 1898-1928/Zweiter Teil: Prosa/Selbstbiographie/1.">
                <div>
                    <desc>
                        <title>1.</title>
                    </desc>
                </div>
                <div type="text" xml:id="tg169.2">
                    <div type="h4">
                        <head type="h4" xml:id="tg169.2.1">I</head>
                        <head type="h4" xml:id="tg169.2.2">Auszug aus einem Manuskript vom Jahr 1919:</head>
                        <p xml:id="tg169.2.3">Nicht die äußeren Daten eines Lebenslaufs geben das Bild eines Schicksals, sondern die inneren Wandlungen eines Menschen bezeichnen seine Bedeutung für die Mitwelt. Nur im Zusammenhang mit dem Weltgeschehen haben die Begebenheiten im Leben des einzelnen Interesse für die Gesamtheit. Wessen Privatleben niemals die Zentren des Gesellschaftslebens berührt, dessen Biographie kann für Seelenforscher höchst wichtig sein, die Allgemeinheit geht sie nichts an.</p>
                        <p rend="zenoPLm4n0" xml:id="tg169.2.4">Wäre meine Lyrik als Ausdruck meiner Gesamtpersönlichkeit alles, was ich den Volksgenossen zu bieten hätte, dann hätte ich der Aufforderung, eine Selbstbiographie zu schreiben, in der Weise entsprochen, daß ich den Literaturhistorikern Gelegenheit gegeben hätte, mich zu klassifizieren: Geboren 6. April 1878 in Berlin; Kindheit, Jugend, Gymnasialbesuch in Lübeck; unverständige Lehrer, niemand, der die Besonderheit des Kindes erkannt hätte, infolgedessen: Widerspenstigkeit, Faulheit, Beschäftigung mit fremden Dingen. Frühzeitige Dichtversuche, die weder in der Schule noch im Elternhause Förderung finden, im Gegenteil als Ablenkung von der Pflicht betrachtet werden und deshalb im geheimen geübt werden müssen. Dummejungenstreiche, zuletzt – als Untersekundaner – geheime Berichte über Schulinterna an die sozialdemokratische Zeitung; daher wegen »sozialistischer Umtriebe« Relegation. Ein Jahr Obersekunda in Parchim (Mecklenburg), dann Apothekerlehrling <pb n="165" xml:id="tg169.2.4.1"/>
in Lübeck; 1900 Apothekergehilfe an verschiedenen Orten, zuletzt in Berlin. Als freier Schriftsteller Teilnahme an der Neuen Gemeinschaft der Brüder Hart; Bekanntschaft mit vielen öffentlich sichtbaren Persönlichkeiten. Freundschaft mit Gustav Landauer, Peter Hille, Paul Scheerbart und anderen. Bohemeleben; Reisen in der Schweiz, in Italien, Österreich, Frankreich; schließlich 1909 dauernder Wohnsitz in München; Kabarettätigkeit, Theaterkritik, schriftstellerische Tätigkeit, meist polemisch-essayistisch. Freundschaftlicher Verkehr mit Frank Wedekind und vielen andern Dichtern und Künstlern. Drei Gedichtbände, vier Theaterstücke; 1911–14 Herausgeber der literarisch-revolutionären Monatsschrift »Kain. Zeitschrift für Menschlichkeit«, die vom November 1918 bis April 1919 als reines Revolutionsorgan in neuer Folge erschien. Seitdem in den Händen der konterrevolutionären bayerischen Staatsgewalt.</p>
                        <p rend="zenoPLm4n0" xml:id="tg169.2.5">Mit diesen Mitteilungen wäre meine Biographie erschöpft, wenn ich mein Leben allein in meinen literarischen Leistungen charakterisiert sähe. Aber ich betrachte meine schriftstellerische Arbeit, vor allem meine dichterischen Erzeugnisse, nur als das Archiv meiner seelischen Erlebnisse, als Teilausdruck meines Temperaments. Das Temperament eines Menschen ist die Summe der Stimmungen, die Hirn und Herz von den Ausströmungen der Umwelt empfangen. Das meinige ist revolutionär. Mein Werdegang und meine Lebenstätigkeit wurden bestimmt von dem Widerstand, den ich von Kindheit an den Einflüssen entgegensetzte, die sich mir in Erziehung und Entwicklung im privaten und gesellschaftlichen Leben aufzudrängen suchten. Die Abwehr dieser Einflüsse war von jeher der Inhalt meiner Arbeit und meiner Bestrebungen.</p>
                        <p rend="zenoPLm4n0" xml:id="tg169.2.6">Im Staat erkannte ich früh das Instrument zur Konservierung all der Kräfte, aus denen die Unbilligkeit der gesellschaftlichen Einrichtungen erwachsen ist. Die Bekämpfung des Staates in seinen wesentlichen Erscheinungsformen, <pb n="166" xml:id="tg169.2.6.1"/>
Kapitalismus, Imperialismus, Militarismus, Klassenherrschaft, Zweckjustiz und Unterdrückung in jeder Gestalt, war und ist der Impuls meines öffentlichen Wirkens. Ich war Anarchist, ehe ich wußte, was Anarchismus ist; ich war Sozialist und Kommunist, als ich anfing, die Ursprünge der Ungerechtigkeit im sozialen Betriebe zu begreifen. Die Klärung meiner Ansichten verdanke ich meinem Freunde Gustav Landauer; er war mein Lehrer, bis ihn die weißen Garden ermordeten, die eine sozialdemokratische Regierung zur Niederzwingung der Revolution nach Bayern gerufen hatte.</p>
                        <p rend="zenoPLm4n0" xml:id="tg169.2.7">Meine revolutionäre Tätigkeit hat mich oft mit den Staatsgewalten in Konflikt gebracht. So stand ich 1910 vor Gericht wegen des Versuches, das sogenannte Lumpenproletariat zu sozialistischem Bewußtsein heranzuziehen ... Während des Krieges stand ich in den Reihen der Opposition gegen die Lenker der deutschen Schicksale ... Wegen der Weigerung, eine Arbeit im vaterländischen Hilfsdienst anzunehmen, wurde ich Anfang 1918 nach Traunstein in Zwangsaufenthalt geschickt, wo ich bis zur Auflösung der »Großen Zeit« in Niederlage und Zerfall blieb.</p>
                        <p rend="zenoPLm4n0" xml:id="tg169.2.8">Selbstverständlich fand mich die Revolution von der ersten Stunde aktiv auf dem Posten ... Mitglied des Revolutionären Arbeiterrats ... Kampf gegen die Konzessionspolitik Kurt Eisners ... Teilnahme an der Ausrufung der bayerischen Räterepublik ... Standgericht: fünfzehn Jahre Festung ...</p>
                    </div>
                </div>
            </div>
            <div subtype="work:no" xml:id="tg170" n="/Literatur/M/Mühsam, Erich/Lyrik und Prosa/Sammlung 1898-1928/Zweiter Teil: Prosa/Selbstbiographie/2.">
                <div>
                    <desc>
                        <title>2.</title>
                    </desc>
                </div>
                <div type="text" xml:id="tg170.2">
                    <div type="h4">
                        <head type="h4" xml:id="tg170.2.1">II</head>
                        <head type="h4" xml:id="tg170.2.2">Nachtrag vom Dezember 1920</head>
                        <head type="h4" xml:id="tg170.2.3">(Festung Niederschönenfeld)</head>
                        <p xml:id="tg170.2.4">Diese Sätze schrieb ich vor einem Jahre in der Festungsanstalt Ansbach. Inzwischen hat sich in mir nichts, außer mir viel geändert ...</p>
                        <p rend="zenoPLm4n0" xml:id="tg170.2.5">Als Ertrag des letzten Jahres sind meinem Lebenslauf <pb n="167" xml:id="tg170.2.5.1"/>
nur ein paar Daten hinzuzufügen. Vom März bis zum Mai mußte ich zwei Monate im Ansbacher Landgerichtsgefängnis zubringen, weil ich einen bayerischen Minister beleidigt hatte. Ich benutzte die Abwechslung, um zwei Bücher zu schreiben: Eine Streitschrift »Die Einigung des revolutionären Proletariats« und das Bühnenwerk »Judas. Ein Arbeiterdrama«. Im ersten habe ich mich um den Nachweis bemüht, daß ... sämtlichen Parteiprogrammen die Parole zur kommunistischen Föderation aller wahrhaft revolutionären Korporationen und Individuen gegenüberzustellen sei. Das Drama unternimmt es, »Proletkult« unter dem Gesichtspunkt zu schaffen, der die Schaubühne als revolutionär-agitatorische Anstalt betrachtet wissen will. Der Proletarier soll im Theater keine Symbolik enträtseln und keine Kunstsprache in seine Prosa übersetzen. Der Arbeiterdichter hat weder die Aufgabe, das Proletariat zu sich hinaufzuziehen, noch sich zu ihm herabzulassen. Er ist kein Dichter des Proletariats, sofern er sich nicht selbst als Angehöriger des Proletariats von Natur wegen erkennt. Der Hirnarbeiter ist nichts Besseres als der Handarbeiter. Wer sich selbst den Charakter eines »Intellektuellen« gibt, versucht, sich über das Proletariat zu erhöhen. Ist mir mit »Judas« ein Zeitstück gelungen, das Wissen und Gefühl des Proletariats in seiner Sprache und in seinem Gedankenkreis bewegt und von proletarischen Herzen erfaßt wird, so ist das Stück gut, auch wenn alle literarische Kritik es verdammen sollte. Mit gesprochenen Opern, mit Mosaikszenerie, mit expressionistischem Gelall dient das Theater allenfalls dem Modernitätsbedürfnis der Bourgeoisie, aber nicht dem Drang des Proletariats, aus Kunst erhöhtes Erleben zu ziehen. Dieser Drang wird befriedigt durch Verständlichkeit im Wort, durch Abwandlung revolutionärer Probleme in bewegter lebendiger Handlung, durch Antönen an Saiten, die in der Arbeiterseele revolutionär schwingen.</p>
                        <p rend="zenoPLm4n0" xml:id="tg170.2.6">Im Sommer 1920 erschien mein Gedichtbuch »Brennende <pb n="168" xml:id="tg170.2.6.1"/>
Erde. Verse eines Kämpfers«. Auch diese Gedichte sollen Zeugnis des Geistes sein, der die Kunst nicht aus dem Leben herausheben, sondern dem Leben und seinem besten Teil, der Revolution, dienstbar machen will. Der Zweck heiligt die Kunst! Zweck meiner Kunst ist der gleiche, dem mein Leben gilt: Kampf! Revolution! Gleichheit! Freiheit!</p>
                    </div>
                </div>
            </div>
            <div subtype="work:no" xml:id="tg171" n="/Literatur/M/Mühsam, Erich/Lyrik und Prosa/Sammlung 1898-1928/Zweiter Teil: Prosa/Selbstbiographie/3.">
                <div>
                    <desc>
                        <title>3.</title>
                    </desc>
                </div>
                <div type="text" xml:id="tg171.2">
                    <div type="h4">
                        <head type="h4" xml:id="tg171.2.1">III</head>
                        <head type="h4" xml:id="tg171.2.2">Dezember 1927</head>
                        <p xml:id="tg171.2.3">In die Zeit, seit ich im Kerker Rechenschaft ablegte über mein Schaffen und Wollen, fällt das Kaspar-Hauser-Erlebnis meiner Rückkehr unter die Menschen, Weihnachten 1924. Ich bemühe mich, in der von der Zäsur des Weltkriegs tief aufgewühlten Welt durch Rede, Schrift und Beispiel auf die revolutionären Ziele hinzuwirken, die aus den vor sieben und acht Jahren geschriebenen Notizen zu erkennen sind. Die Dichtkunst ist nichts als eine meiner Waffen im Kampf.</p>
                        <p rend="zenoPLm4n0" xml:id="tg171.2.4">Erschienen sind seit der Veröffentlichung der »Brennenden Erde« unter dem Titel »Alarm, Manifeste aus zwanzig Jahren« eine kleine Sammlung politischer Gedichte, Aufsätze und Aufrufe; unter dem Titel »Revolution« die »Kampf-, Marsch- und Spottlieder«, ferner als Appell gegen die vergiftete Kampf weise der Klassenjustiz die Schrift »Gerechtigkeit für Max Holz!« Seit 1926 gebe ich die anarchistische Monatsschrift »Fanal« heraus. Dort sind die grundsätzlichen Bekenntnisse zu suchen, die meine Stellung zu den öffentlichen Problemen der Gegenwart klarlegen.</p>
                        <p rend="zenoPLm4n0" xml:id="tg171.2.5">Die private Gelegenheit des fünfzigsten Geburtstags gibt Anlaß, das Lebenswerk, soweit es ausgesprochen literarischen Charakter angenommen hat, im Überblick vorzulegen.</p>
                        <pb n="169" xml:id="tg171.2.6"/>
                    </div>
                </div>
            </div>
        </body>
    </text>
</TEI>