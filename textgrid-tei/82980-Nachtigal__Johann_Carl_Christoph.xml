<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg47" n="/Literatur/M/Nachtigal, Johann Carl Christoph/Sagen/Volcks-Sagen/2. Oertliche Volks-Sagen auf der Nord-Seite des Harzes/19. Das Grundlos/Das Grundlos">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Das Grundlos</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Nachtigal, Johann Carl Christoph: Element 00054 [2011/07/11 at 20:31:16]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Otmar [d.i. Johann Carl Christoph Nachtigal]: Volcks-Sagen. Bremen: Friedrich Wilmans, 1800.</title>
                        <author>Nachtigal, Johann Carl Christoph</author>
                    </titleStmt>
                    <extent>252-</extent>
                    <publicationStmt>
                        <date when="1800"/>
                        <pubPlace>Bremen</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1753" notAfter="1819"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg47.3">
                <div type="h4">
                    <head type="h4" xml:id="tg47.3.1">
                        <pb n="252" xml:id="tg47.3.1.1"/>
Das Grundlos.</head>
                    <p xml:id="tg47.3.2">»Unfern der nördlichen Spitze des Hakels sieht man, am Abhang des Berges, einen großen Erdfall, zum Theil mit Wasser ausgefüllt, am Rande mit hohem Schilf überwachsen, und in der Mitte mit der längsten Stange nicht zu ergründen. Darum heißt er mit Recht:<hi rend="spaced" xml:id="tg47.3.2.1"> das Grundlos.</hi>«</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.3.3">Hier stand einst, vor vielen hundert Jahren, als noch das ganze Land mit Wald überdeckt war, im Dickicht, eine Burg, der gewöhnliche Sammelplatz der Raubritter, welche die ganze Gegend umher unsicher machten. <pb n="253" xml:id="tg47.3.3.1"/>
Hier theilten sie ihren Raub, und die Aerndten der zerstreuten Bewohner des Landes, die sie für sich zu arbeiten zwangen bis auf das Blut, besonders bei den Burgfesten. Hieher brachten sie ihre Gefangnen, und die besten Töchter des Landmanns, die sie zum Hofe-Dienst raubten.</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.3.4">Hier schwelgten, und lärmten und tanzten die fahrenden Ritter lange Tage und Nächte hindurch. Und mit Schrecken hörte der ferne Wanderer oft, zwischen dem Lärm der Bunge<ref type="noteAnchor" target="#tg47.3.29">
                            <anchor xml:id="tg47.3.4.1" n="/Literatur/M/Nachtigal, Johann Carl Christoph/Sagen/Volcks-Sagen/2. Oertliche Volks-Sagen auf der Nord-Seite des Harzes/19. Das Grundlos/Das Grundlos#Fußnote_1"/>
                            <ptr cRef="/Literatur/M/Nachtigal, Johann Carl Christoph/Sagen/Volcks-Sagen/2. Oertliche Volks-Sagen auf der Nord-Seite des Harzes/19. Das Grundlos/Das Grundlos#Fußnoten_1"/>
                            <hi rend="superscript" xml:id="tg47.3.4.2.1">1</hi>
                        </ref> und der Drometen, das dumpfe Jammergeschrei derer, die in unterirdischen Hölen gemordet wurden. Menschliche Hülfe war hier umsonst; denn, mächtige verbündete Ritter schützten die Burg mit ihren Reisigen. Aber, zum Himmel stieg auf das Geschrei der Gewaltthat. Und, es nahte der Tag der Rache!</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.3.5">
                        <pb n="254" xml:id="tg47.3.5.1"/>
Einst verirrte sich an einem neblichten Herbsttage, ein fern her kommender Ritter aus Welschland mit seinem Knechte, im Harzgebirg, und kam zu dieser Burg, welche, abwärts von der Straße, welche Reisende zu ziehen pflegten, im dichten Walde versteckt lag. Schon war die Nacht eingebrochen, und rings um die Burgmauer her war weder Mensch zu sehen noch Thier. Doch hörten sie drinnen ein wildes Gekreisch wie von betrunkenen, lärmenden Männern, und Hörnern und Drometen, begleitet vom Geheul großer Hunde. Die Reisenden pochten und riefen an einer Hinterpforte im Dickicht. Aber zu ihrem Glück hörte niemand ihr Klopfen noch Rufen; denn, es tobte der Sturm in der Nacht, und der Regen rasselte auf den Dächern.</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.3.6">Der Knecht, des Rufens müde, suchte ein Obdach. Und endlich fand er tappend, und durch das Gebüsch sich drängend, unfern dem Eingange zur Burg, eine gewölbte Vertiefung, <pb n="255" xml:id="tg47.3.6.1"/>
und in ihr eine Pferdekrippe, woran ein fressender Klepper stand. Froh über diese Entdeckung brachte er seinen Herrn und die Pferde hieher, und ließ sie an dem Futter sich laben, womit die Krippe angefüllt war.</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.3.7">Der fremde Ritter, ermüdet von der langen mühvollen Reise, entschlief bald auf einem kleinen Heuschober, den er in dem einem Winkel der Hölung auffand. Die ferne Musik und das eintönige Rasseln des Regens wiegte ihn in festen Schlummer.</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.3.8">Aber nicht so gut ward es dem Knecht. Ihn erhielten wach die Sorge für seine Pferde und sein leerer Magen, den die Musik und der Gedanke an den Schmaus, der dem Tanz vorausging, wenig beruhigten. Und dann wurde es ihm, von Minute zu Minute, grauender und grauender in der dunkeln Höle. Er betastete ringsum den geräumigen gewölbten Stall, worin er mit seinem Herren war, fand <pb n="256" xml:id="tg47.3.8.1"/>
aber nichts, als reichlichen Vorrath für die Pferde, deren leicht zwanzig hier Platz gefunden hätten. – »Wer wohnt hier? – Wem gehört der Klepper? wem das Futter, das du deinen Pferden gabst, – Wie, wenn die Knechte der Tanzenden zurückkommen, oder, in dem Heu schlafend, erwachen? – Oder, wohnen vielleicht gar Räuber und Mörder hier?« – So durchkreuzten tausend Gedanken seine Seele, und er konnte nicht schlafen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.3.9">Die steigende Furcht trieb ihn näher hin zu seinem schlafenden Herrn. Und endlich warf er sich unmuthig auf das Heu, fiel aber in die Tiefe hinab. Unter ihm brachen einige morsche Stäbe; er fiel einige Fuß tief in eine unterirrdische Hölung; und sein Gesicht und seine Hände berührten – o Schrecken! – Menschenschädel und Menschengebeine, die hier zerstreut lagen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.3.10">Lautschreiend raffte er sich auf von dem verwünschten Ort, kroch zitternd hervor, und <pb n="257" xml:id="tg47.3.10.1"/>
wankte der Thür des Stalles zu, vergessend seines Herren und seiner Pferde. Hier saß er vor dem Gebüsch, das den Eingang umkleidete, vom Winde durchstürmt, vom Regen durchnäßt, und klappte mit allen Zähnen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.3.11">Allgemach schwiegen die Hörner und Drometen; und bald war rings um ihn eine Todtenstille, die ihm noch grauender war. – Jetzt schlug die Thurmuhr zwölf; und jedes Haar auf seinem Kopf sträubte sich auf. Denn, angstvoll erwartete er in jedem Augenblick die Erscheinung der Geister der Erschlagenen. Und so wagte er nicht, in die Höhe, noch vor sich, noch hinter sich zu sehen. Zusammengekrümmt, die Augen mit den ausgespreiteten Fingern bedeckt, saß er da.</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.3.12">Plötzlich fielen einzelne Strahlen, wie von einer auflodernden Fackel, auf das Gebüsch – und im Augenblick war alles wieder verschwunden. Oft glaubte er, entferntes Kettengeklirr und dumpfes Aechzen zu hören; er horchte, <pb n="258" xml:id="tg47.3.12.1"/>
und – plötzlich war alles wieder still. In jedem Augenblick erwartete er vor Angst zu sterben, und überlebte doch alle diese Schrecken.</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.3.13">Jetzt schlug endlich, nach langem vergeblichen Harren, die Thurmuhr: Eins. Das Gewölk zertheilte sich; einzelne Strahlen des Mondes fielen auf ihn durch die Gebüsche, und Hoffnung und Lust zum Leben kehrten zurück in sein Herz. – Bald stand der volle Mond in seiner Pracht da, am heitern nicht mehr bewölkten Himmel. Und nun wagt' es der Knappe, einige Schritte vorwärts zu thun, um sich umzusehen, wo er sey.</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.3.14">Er entdeckte bald eine nicht sehr hohe Mauer, auf der mehrere kleine Thürme standen, und nicht weit von dem Stalle, von einigen mächtigen Eichen überdeckt, ein eisernes Fallgatter, das den Eingang in den Burghof verschloß. Mit immer wachsendem Muth (denn vorbei war die Gespensterstunde, und der Mond leuchtete ihm) nahte er sich, mit <pb n="259" xml:id="tg47.3.14.1"/>
leisem Tritt, dem Fallgatter, und sah hinein in den Burghof, sah am Ende desselben das Thürmlein, das hinauf zum Rittersaal führte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.3.15">Auf der Mitte des Hofes stand eine Rug-Säule<ref type="noteAnchor" target="#tg47.3.31">
                            <anchor xml:id="tg47.3.15.1" n="/Literatur/M/Nachtigal, Johann Carl Christoph/Sagen/Volcks-Sagen/2. Oertliche Volks-Sagen auf der Nord-Seite des Harzes/19. Das Grundlos/Das Grundlos#Fußnote_2"/>
                            <ptr cRef="/Literatur/M/Nachtigal, Johann Carl Christoph/Sagen/Volcks-Sagen/2. Oertliche Volks-Sagen auf der Nord-Seite des Harzes/19. Das Grundlos/Das Grundlos#Fußnoten_2"/>
                            <hi rend="superscript" xml:id="tg47.3.15.2.1">2</hi>
                        </ref>, mit ausgespreiteten Armen. Mit einemmal zeigte sich hier dem Knecht ein wunderseltsames Abentheuer. Drei große Hähne stiegen majestätisch herab von dem runden Dach des Burgverließes, und wandelten langsam über den Hof, dem geharnischten Schwerdtträger zu. Dann hoben sie sich zugleich im Fluge. Der größte Hahn, höher und stärker befiedert, als ein Adler, setzte sich auf den Kopf der Rolands-Säule, die anderen nahmen Platz auf seinen Ellenbogen. Und nun krähten sie, alle drei zugleich dreimal, daß der Hof und der nahe Wald wiederhallten. Alles still. Dann erscholl es, wie aus dumpfer Ferne: <pb n="260" xml:id="tg47.3.15.3"/>
»Wehe! Wehe! Wehe!« – Siebenmal krähten nun noch lauter die Hähne, und das: »Wehe! Wehe! Wehe!« erscholl zum zweitenmal. – Neunmal krähten noch lauter die Hähne; und nun erhob sich der große Hahn hoch in die Lüfte, und schrie: »Wehe! Wehe! Wehe! Heute noch versinkt die Raubburg!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.3.16">Taumelnd wankte der Knecht nach dem überwölbten Stall zurück, rüttelte zitternd seinen Herrn, der wie im Todesschlafe da lag, bis er endlich erwachte, und verkündete, bebend wie Espenlaub, ihm die unerhörte Mähre, während er die noch gesattelten Rosse zäumte. Kopfschüttelnd strafte der Ritter aus Welschland seinen Knecht Lügen, und glaubte dennoch die Erzählung, und schauderte daß zusammen, bei dem: »Wehe! Wehe! Wehe!« – Und, ohne zu säumen, eilten beide von dannen, durch Gebüsch und Hecken, bis sie endlich die gebahnte Straße fanden.</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.3.17">Jetzt ging ihnen lang erwünscht die Sonne auf, aber halbverfinstert, und wie mit einem <pb n="261" xml:id="tg47.3.17.1"/>
Trauerflor umschleiert. Schon sahen sie von weitem die beglänzten Thurmspitzen Magdeburgs. Da hörten sie, in weiter Ferne hinter sich, ein dumpfes Getöse, wie von einem fernen Donner. Sie blickten zurück, und sahen eine große, dicke Dampfsäule aufsteigen, wie aus einem feuerspeienden Berge. – »Ha! rief der Knecht, gewiß ist dort versunken die verruchte Burg! Aus dem Schwefelpfuhl, worin die Unholde stürzten, steigt jener Dampf auf!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.3.18">»Komm, sprach sein Herr, wir wollen dorthin zurückkehren, um zu sehen die wunderbare Geschichte; ich habe so in dem Stall meine Handschuh zurück gelassen, das Abschiedsgeschenk meiner Verlobten.« – Aber höchlich weigerte sich des der Knecht. Und, zürnend und drohend ritt der Ritter aus Welschland allein der aufsteigenden Dampfsäule zu. Zitternd folgte ihm endlich der Knecht in langer Entfernung.</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.3.19">Nach einigen Stunden erreichten sie die Gegend, wo der Dampf aufbrudelte. Der <pb n="262" xml:id="tg47.3.19.1"/>
Ritter befahl seinem Knecht, hineinzureiten in die Dampfwolke, und seine Handschuhe zu holen. Der Knecht bebte zurück. Da riß der Ritter zähneknirschend sein Schwerdt aus der Scheide, und stieß es dem Knecht in die Brust. – Noch jetzt siehst du, unfern des Grundloses, den Stein, wo der Herr seinen zögernden Knecht würgte, und, bei Sonnenfinsternissen, noch hier und da, Tropfen des Bluts, das den Stein überspritzte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.3.20">Der Ritter weilte, bis die Sonne höher stieg, und nur noch eine dichte Dampfsäule die Mitte der Schreckensgegend verhüllte. Er sah nun vor sich einen See, der immer größer wurde, je mehr der Nebel sich in der Mitte zusammendrängte, und fand endlich am Ufer die Krippe, an der in der Nacht sein Streitroß stand, und in ihr – o Wunder! – die Handschuh, die seine Verlobte ihm gab.</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.3.21">In tiefem Nachdenken versunken stand der fremde Ritter aus Welschland, den stieren, starren Blick auf seine Handschuh gerichtet.</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.3.22">
                        <pb n="263" xml:id="tg47.3.22.1"/>
Bald aber weckt' ihn aus seinem Hinstarren ein Zetergeschrei. Er blickt auf. Die Sonne hatte jetzt die Mitte des Himmels erreicht, und die ganze Dampfwolke niedergedruckt. – Da sieht er das Dach der immer tiefer einsinkenden Burg ganz mit Menschen bedeckt, die in der größten Herzensangst immer höher klommen, je näher ihnen das Wasser des immer steigenden Sees kam. An der Kleidung unterschied er etwa acht Ritter und zwölf Knappen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.3.23">Am lautesten schrie »Zeter und Wehe!« über sich und über die Andern, ein dickes, ungestaltes Weib, mit feuerrothen Augen und Haaren. Um die Hände frei zu haben, hatte sie, in der Angst ihres Herzens, ein großes Schlüsselbund sich um den Hals geworfen. Denn, dieser Unholdin waren die Schlüssel anvertraut gewesen über die Keller, Gewölbe und die unterirdischen Gemächer der Raubburg, in welche sie die unglücklichen Schlachtopfer der Räuber herabgestürzt hatte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.3.24">
                        <pb n="264" xml:id="tg47.3.24.1"/>
Jetzt kam ein gräßlicher Anblick. Zahllose Gerippe von Erschlagenen und Erwürgten klimmten eins nach dem andern, von der entgegengesetzten Seite, das Dach hinauf, setzten auf den Forst sich hin, und blickten zähnefletschend auf die Unholde, die, in der Todesangst, nicht über sich, nicht unter sich zu sehen wagten. Dann aber erhoben sich die Gerippe, und schlugen mit ihren Knochenhänden, und mit den Ketten, womit sie belastet waren, auf ihre Peiniger.</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.3.25">Zuerst stürzte in die Fluthen die Schafnerin, mit dem Schlüsselbunde um den Hals, und wurde in dem Augenblick in eine ungeheure Karautsche verwandelt. Dann wurden herabgepeitscht die Raubritter. Sie verwandelten sich, so bald sie das Wasser berührten, in sechsfüßige Hechte. Zuletzt stürzten die Knappen zeterschreiend herab, und wurden Karpfen, ohne von ihrer Größe und Schwere zu verlieren.</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.3.26">Und so verfolgen, seit Jahrhunderten, bis auf den heutigen Tag, die ausgehungerten <pb n="265" xml:id="tg47.3.26.1"/>
Hechte die Karpfen und die Karautsche im Grundlos, ohne Ruhe noch Rast. Uebertäubt die Ermattung den Hunger; so jagen die Gerippe in der Tiefe des Wassers sie wieder auf.</p>
                    <p rend="zenoPLm4n0" xml:id="tg47.3.27">Noch jetzt sehen die Bewohner der Gegend zuweilen die Moosbewachsenen Karpfen, die kleinen schwimmenden Inseln gleichen, und die Centnerschwere Karautsche, mit den feuerrothen Augen, das große Schlüsselbund um den Hals, auf der Oberfläche des Sees. Aber in demselben Augenblick tauchen sie wieder unter, geschreckt von ihren Verfolgern.</p>
                </div>
                <div type="footnotes">
                    <head type="h4" xml:id="tg47.3.28">Fußnoten</head>
                    <note xml:id="tg47.3.29.note" target="#tg47.3.4.1">
                        <p xml:id="tg47.3.29">
                            <anchor xml:id="tg47.3.29.1" n="/Literatur/M/Nachtigal, Johann Carl Christoph/Sagen/Volcks-Sagen/2. Oertliche Volks-Sagen auf der Nord-Seite des Harzes/19. Das Grundlos/Das Grundlos#Fußnoten_1"/>
                            <ref cRef="/Literatur/M/Nachtigal, Johann Carl Christoph/Sagen/Volcks-Sagen/2. Oertliche Volks-Sagen auf der Nord-Seite des Harzes/19. Das Grundlos/Das Grundlos#Fußnote_1" xml:id="tg47.3.29.2">1</ref> Die ältere Pauke.</p>
                    </note>
                    <note xml:id="tg47.3.31.note" target="#tg47.3.15.1">
                        <p xml:id="tg47.3.31">
                            <anchor xml:id="tg47.3.31.1" n="/Literatur/M/Nachtigal, Johann Carl Christoph/Sagen/Volcks-Sagen/2. Oertliche Volks-Sagen auf der Nord-Seite des Harzes/19. Das Grundlos/Das Grundlos#Fußnoten_2"/>
                            <ref cRef="/Literatur/M/Nachtigal, Johann Carl Christoph/Sagen/Volcks-Sagen/2. Oertliche Volks-Sagen auf der Nord-Seite des Harzes/19. Das Grundlos/Das Grundlos#Fußnote_2" xml:id="tg47.3.31.2">2</ref> Rug- oder Ruge-land (Roland-) Säule, war das Zeichen der peinlichen Gerichtsbarkeit, und stellte einen mit einem großen Schwerdt bewaffneten Mann dar.</p>
                    </note>
                </div>
            </div>
        </body>
    </text>
</TEI>