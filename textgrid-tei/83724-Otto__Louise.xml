<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg265" n="/Literatur/M/Otto, Louise/Essays/Aufsätze aus der »Frauen-Zeitung«/Die Freiheit ist unteilbar">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Die Freiheit ist unteilbar</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Otto, Louise: Element 00236 [2011/07/11 at 20:31:47]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <notesStmt>
                <note>
                                 Erstdruck in: Frauen-Zeitung, redigirt von Louise Otto, Leipzig, 1. Jg., Nr. 1, 21. April 1849.
                              </note>
            </notesStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>»Dem Reich der Freiheit werb’ ich Bürgerinnen«. Die Frauen-Zeitung von Louise Otto. Herausgegeben und kommentiert von Ute Gerhard, Elisabeth Hannover-Drück und Romina Schmitter, Frankfurt a.M.: Syndikat, 1980.</title>
                        <author key="pnd:118590901">Otto, Louise</author>
                    </titleStmt>
                    <extent>40-</extent>
                    <publicationStmt>
                        <date when="1980"/>
                        <pubPlace>Frankfurt a.M.</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1819" notAfter="1895"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg265.3">
                <div type="h4">
                    <head type="h4" xml:id="tg265.3.1">Die Freiheit ist unteilbar</head>
                    <p xml:id="tg265.3.2">Die Freiheit ist unteilbar! – Dies ist ein so einfacher Lehrsatz, daß er der erste Artikel in jedem Glaubensbekenntnis sein sollte. Gleichwohl müssen wir es täglich erfahren, daß er noch nicht überall Eingang gefunden, vielmehr nur bei gar wenigen Fleisch und Blut geworden ist. Es meinen viele, sich Freiheitskämpfer nennen zu dürfen, welche doch von dem Ideal der Freiheit mit ihren Gedanken ferne sind und nur von einzelnen Freiheiten etwas wissen wollen, für deren Erringung sie sich abmühen. Wieviel z.B. ist in unserm Deutschland besonders nicht für Glaubensfreiheit gekämpft und gelitten worden, wieviel edle Männer und Frauen sind nicht dafür in den Tod gegangen. Sie nannten sich Freiheitskämpfer und wollten doch weiter nichts als die Freiheit, Gott anzubeten und ihm zu dienen je nach ihrem Bedürfnis. Weiter fragten sie nach nichts. So gibt es heute noch viele – ja selbst unter den Lichtfreunden und Deutschkatholiken – welche sich nicht scheuen, selbstgefällig zu erklären, daß ihr Streben nach religiöser Freiheit nichts gemein habe mit dem Streben nach politischer Freiheit, ja daß sie selbst ohne diese, sobald man ihnen nur eben jene garantiere, ganz zufrieden zu leben vermöchten. Höchstens bringt man diese heute mit der Frage in Verlegenheit: ob sie denn allen Ernstes einen so kindlichen Glauben haben, daß es ihnen nie einfällt zu bedenken, ob ein Staat, der nicht auf den Grundpfeilern der Freiheit ruht, in seinen engherzigen, bevormundenden Institutionen auch wirklich die religiöse Freiheit garantieren <hi rend="italic" xml:id="tg265.3.2.1">könne</hi>, davon noch gar nicht zu sprechen, ob er es <hi rend="italic" xml:id="tg265.3.2.2">wolle</hi>. Besonders aber meinen diejenigen sich Freiheitskämpfer vor allen andern nennen zu dürfen, welche nur den <hi rend="italic" xml:id="tg265.3.2.3">politischen</hi> Fortschritt im Auge haben und ihm allein dienen. Dazu gehören vor allen die Liberalen vor dem März, die nur nach einzelnen Freiheiten rangen, wie Preßfreiheit, Versammlungsfreiheit usw., und die man deshalb damals, als wir sogar <pb n="40" xml:id="tg265.3.2.4"/>
dieser Güter noch entbehren mußten, für Freiheitshelden hielt. Einige von ihnen, die Beschränkten und Engherzigen, deren Blicke nie über den engen Horizont des Konstitutionalismus hinausgingen, sind auf derselben Stufe stehengeblieben, auf der sie damals standen, und wer vor dem März als Freiheitsmärtyrer dastand, erweist sich jetzt als gutgesinnter Reaktionär. – Andere hingegen von diesen Politikern setzen mit den errungenen einzelnen Freiheiten, wie Preßfreiheit usw., den Kampf um andere einzelne Freiheiten fort, sie kämpfen für die honnette Republik, nehmen sich die Freiheit, den Adel abzuschaffen und sich selbst, die Bourgeoisie, an dessen Stelle zu setzen – aber sie beweisen durch all diese Bestrebungen, daß sie nichts wissen von der einen unteilbaren Freiheit! Und die Sozialisten? und die soziale Freiheit? <hi rend="italic" xml:id="tg265.3.2.5">Die</hi> Sozialisten, welche meinen, ihre Utopien mit Hülfe einer Zwingherrschaft gründen zu können, welche über den politischen Fortschritt geringschätzend lächeln und an die Stelle religiöser Freiheit einen erzwungenen Atheismus setzen wollen – die freilich sind eben so fern von der Erkenntnis des Satzes: die Freiheit ist unteilbar! Sie kann nicht in dem einen Zustande sein und in dem andern mangeln – die wahre Freiheit ist eben die Gottheit, die man nicht auf dem oder jenem Berge nur anbeten kann, sondern die man verehren und ihr dienen muß und kann allenthalben, wo ihr auch noch kein Tempel errichtet ist.</p>
                    <p rend="zenoPLm4n0" xml:id="tg265.3.3">Und nun laßt uns einmal fragen, wie viel Männer gibt es denn, welche, wenn sie durchdrungen sind von dem Gedanken, für die Freiheit zu leben und zu sterben, diese eben für <hi rend="italic" xml:id="tg265.3.3.1">alles</hi> Volk und <hi rend="italic" xml:id="tg265.3.3.2">alle</hi> Menschen erkämpfen wollen? Sie antworten gar leicht zu Tausenden mit <hi rend="italic" xml:id="tg265.3.3.3">Ja!</hi> aber sie denken bei all ihren endlichen Bestrebungen nur an <hi rend="italic" xml:id="tg265.3.3.4">eine</hi> Hälfte des Menschengeschlechts – nur an die <hi rend="italic" xml:id="tg265.3.3.5">Männer</hi>. Wo sie das Volk meinen, da zählen die Frauen nicht mit.</p>
                    <p rend="zenoPLm4n0" xml:id="tg265.3.4">Aber die Freiheit ist unteilbar! Also freie Männer dürfen keine Sklaven neben sich dulden – also auch keine Sklavinnen. Wir müssen den redlichen Willen oder die Geisteskräfte aller Freiheitskämpfer in Frage stellen, welche nur die Rechte der Männer, aber nicht zugleich auch die der Frauen vertreten. Wir können so wenig, wie sie uns selbst zu Bundesgenossinnen haben wollen, sie die Bundesgenossen der Fahnenträger der Freiheit nennen! Sie werden ewig zu dem »Halben« gehören, und wenn sie auch noch so stolz auf ihre entschiedene Gesinnung sein sollten.</p>
                    <closer>
                        <seg type="closer" rend="zenoPR" xml:id="tg265.3.5">
                            <hi rend="italic" xml:id="tg265.3.5.1">L. O.</hi>
                        </seg>
                        <pb n="41" xml:id="tg265.3.6"/>
                    </closer>
                </div>
            </div>
        </body>
    </text>
</TEI>