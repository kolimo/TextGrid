<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg277" n="/Literatur/M/Otto, Louise/Essays/Aufsätze aus der »Frauen-Zeitung«/Für die Arbeiterinnen (Schluß)">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Für die Arbeiterinnen (Schluß)</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Otto, Louise: Element 00248 [2011/07/11 at 20:31:47]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <notesStmt>
                <note>
                                 Erstdruck in: Frauen-Zeitung, redigirt von Louise Otto, Leipzig, 1. Jg., Nr. 21, 8. September 1849.
                              </note>
            </notesStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>»Dem Reich der Freiheit werb’ ich Bürgerinnen«. Die Frauen-Zeitung von Louise Otto. Herausgegeben und kommentiert von Ute Gerhard, Elisabeth Hannover-Drück und Romina Schmitter, Frankfurt a.M.: Syndikat, 1980.</title>
                        <author key="pnd:118590901">Otto, Louise</author>
                    </titleStmt>
                    <extent>139-</extent>
                    <publicationStmt>
                        <date when="1980"/>
                        <pubPlace>Frankfurt a.M.</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1819" notAfter="1895"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg277.3">
                <div type="h4">
                    <head type="h4" xml:id="tg277.3.1">
                        <pb n="139" xml:id="tg277.3.1.1"/>
                        <pb type="start" n="141" xml:id="tg277.3.1.2"/>Für die Arbeiterinnen</head>
                    <milestone unit="head_start"/>
                    <p rend="zenoPC" xml:id="tg277.3.2">Wie die Reichen die Armen vergessen – Das Glück einer Nähterin – Schneidermädchen – Die Reichen als Konkurrenten der Armen – Die Ehe eine Versorgungsanstalt – Demoralisation der Gesellschaft.</p>
                    <milestone unit="head_end"/>
                    <lb xml:id="tg277.3.3"/>
                    <p xml:id="tg277.3.4">
                        <hi rend="italic" xml:id="tg277.3.4.1">(Schluß)</hi>
                    </p>
                    <lb xml:id="tg277.3.5"/>
                    <p xml:id="tg277.3.6">
                        <hi rend="italic" xml:id="tg277.3.6.1">Die Reichen haben der Armen vergessen!</hi> – Viele der wirklich Reichen haben keinen Begriff davon, was Armut ist und daß ein armes Mädchen, das nicht gerade zum Betteln gezwungen ist oder nicht wie eine Bettlerin aussieht, ein paar Thaler sehr notwendig brauchen kann. Die feinen Damen wissen auch oft nicht, wie lange an einer Nähterei gearbeitet werden muß, <pb n="141" xml:id="tg277.3.6.2"/>
und statt es nach sich selbst zu beurteilen, was sie doch könnten, sagen sie: ja wir arbeiten natürlich lange an so etwas – aber bei denen, die den ganzen Tag nähen, fliegt die Nadel nur so hin – es ist unglaublich, wie viel sie in einem Tage fertig bringen. Denn das ist auch herkömmlich, daß der Reiche nie von sich auf den Armen schließt, sondern daß er diesen geradezu als ein ganz anderes Wesen, eine andere menschliche Gattung betrachtet denn sich. So kennen sie auch nicht die Sorgen und Bedürfnisse der Armen – ein paar Thaler oder Groschen, die er von ihnen zu fordern hat, sind so wenig, und eine solche Kleinigkeit wird deshalb oft wirklich <hi rend="italic" xml:id="tg277.3.6.3">vergessen</hi>. In diesem <hi rend="italic" xml:id="tg277.3.6.4">Vergessen</hi> aber selbst liegt der ganze Egoismus, die ganze Unnatur und Unmenschlichkeit der heutigen Gesellschaft. – Diejenigen nun, welche nicht so reich sind, sich aber doch den Schein des Reichtums und der Vornehmheit geben wollen, daher arbeiten lassen, was sie nicht bezahlen können und wollen, benutzen diesen nobeln Gebrauch – ehe sie bezahlen, warten sie ab, bis man sie mahnt – dann sagen sie wegwerfend: »Ach, diese Kleinigkeit habe ich ganz vergessen!« – Natürlich kommen bei solcher Gewohnheit die Schüchternsten und Schwächsten am schlechtesten weg – und das werden die armen Arbeiterinnen sein, die aus Zartgefühl nicht mahnen und die man auch im schlimmsten Falle nicht zu fürchten hat wie den Kaufmann oder Handwerker, der am Ende mit gerichtlichen Klagen droht, indes die Arbeiterin nur Tränen zu ihrem Fürsprecher hat. – Ja, so werden die Arbeiterinnen behandelt! – und wie müssen sie sich mühen!</p>
                    <p rend="zenoPLm4n0" xml:id="tg277.3.7">Glücklich sind diejenigen Mädchen, die, indem sie von weiblichen Handarbeiten leben, noch einer Familie angehören, so daß sie wohl, was sie verdienen, den Eltern oder Geschwistern mit zum Haushalt geben, oder doch nicht speziell dafür zu sorgen haben. Dann sitzen sie wenigstens in einer warmen Stube, und es wird zu einem warmen Mittagsessen eher Rat. Aber welches Glück? Eine fleißige Arbeiterin steht früh 5 Uhr auf und setzt sich gegen 6 Uhr an ihren Arbeitstisch – dann steht sie nicht eher auf als um 12 Uhr zum Mittagsessen – in längstens einer halben Stunde ist dies beendigt, und sie setzt sich gleich wieder hin – hat sie viel zu tun, so macht das Abendessen keine Unterbrechung, ein Stück Brot mit Salz kann neben der Arbeit gegessen werden – gegen 10 Uhr, oder je nachdem die Arbeit treibt, geht sie schlafen und sagt sich, daß sie heute vielleicht drei Neugroschen verdient hat. Und so Tag für Tag, Stich für Stich – die Gedanken stumpfen entweder ganz ab oder bleiben an den Sorgen hängen: wo wieder Arbeit herzubekommen, wenn diese fertig? und wird diese auch bezahlt werden? – Aber keine Träne darf in ihre Augen treten – dann möchte sie zu große Stiche machen – auch darf sie nicht durchs Fenster auf die Straße blicken – das wäre gleich eine Arbeitversäumnis. – Wie gesagt, eine Nähterin, die noch nicht ganz verlassen ist, kann ein solches Leben schon ertragen, dabei auskommen – aber wenn sie nun <hi rend="italic" xml:id="tg277.3.7.1">ganz allein</hi> steht? oder noch einen alten Vater, eine stumpfe Mutter mitzuerhalten hat? oder gar Kinder? Im erstern Falle ist sie noch besser daran, dann kann sie als Nähterin zum Ausbessern auf die Stube zu den Leuten gehen, da bekommt sie 2 Ngr. 5 Pf. und das Mittagsessen – so hat sie wenigstens dies und erspart Holz und Licht zu Hause, kann auch bis Tagesanbruch schlafen und ist abends von 8 Uhr an frei. Ist sie so glucklich, Schneidern gelernt zu haben, bekommt sie 5 Ngr., wohl auch 7 Ngr. 5 Pf. bis 10 Ngr. den Tag. Solche Mädchen haben unter den weiblichen Arbeiterinnen noch das glücklichste Los gezogen, aber sie haben in der Regel sich auch erst das Erlernen <pb n="142" xml:id="tg277.3.7.2"/>
der Schneiderei müssen etwas kosten lassen. – Auch wird von ihnen schon eine gute Erziehung und feinere Bildung verlangt, wenn die gebildeten Familien sie in ihrer nächsten Umgebung sehen sollen. Die arm und unwissend aufgewachsenen Mädchen eignen sich daher zu diesem Geschäfte nicht. Die Schneidermädchen haben in der Regel auch keine Ursache zur Klage – nur eine Unsitte will sich hier und da in ihre Behandlung einschleichen – es ist die, sie nicht mit dem Feierabend um 8 Uhr zu entlassen. In manchen Familien wird ihnen zugemutet, länger, ja bis 9 und 10 Uhr zu arbeiten, wenn etwa ein Stück noch nicht vollendet ist. Es mag dies manchmal wünschenswert sein, aber dann haben die Mädchen auch das Recht, für diese Stunden, welche sie über den Feierabend arbeiten, Bezahlung zu fordern. – In unserer Familie geschah dies immer aus natürlichem Rechtsgefühl, ehe nur irgend von Emanzipation der arbeitenden Klassen die Rede gewesen war – wir mußten darüber Vorwürfe anderer wohlhabender Familien hören: wir verdürben die Schneidermädchen, indem wir sie verwöhnten – das Lied ist sehr alt. – Wenn diese Mädchen entlassen werden, so können sie sich im Sommer noch im Freien ergehen, in schlechter Jahreszeit für sich selbst arbeiten und ihr Los wird nicht unerträglich sein. Aber ich habe viele gekannt, die monatelang nicht vor die Stadt gekommen waren, weil sie bis in die Nacht bei den fremden Leuten aushalten mußten – auch nur, weil sie aus Bescheidenheit nicht zu sagen wagten: »ich will gehen« – was ihnen doch wirklich niemand verwehren konnte. Auch fürchtet immer eine jede: wenn ich zeitig gehe und andere bleiben, wird man mich nicht wieder wählen, sondern jene – und so bleibt sie aus Furcht, um sich selbst nicht zu schaden, zum Schaden <hi rend="italic" xml:id="tg277.3.7.3">aller</hi>. Hier ist gleich ein Fall, wo die Assoziation von Nutzen wäre, wenn die Schneider-Mädchen sich vereinigten und erklärten, daß sie nicht länger als von früh 8 Uhr an bis Abends 7 Uhr auf Arbeit gehen möchten.</p>
                    <p rend="zenoPLm4n0" xml:id="tg277.3.8">Ich komme wieder zurück auf die große Konkurrenz der Näharbeit, die von den sogenannten höheren Ständen auf die niederen rückwirkt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg277.3.9">Die weiblichen Arbeiten werden auch von allen denen vorgezogen, welche es nicht wollen wissen lassen, daß sie einen Verdienst brauchen können. Die Lüge und Heuchelei herrscht einmal in unseren Zuständen. Jedermann will für reicher gehalten sein, als er ist, oder die »höheren Stände« halten es für ihrer unwürdig – zu arbeiten. Man kann kaum den einzelnen einen Vorwurf daraus machen, einem Unrecht, welches das Unrecht der ganzen Gesellschaft ist, sich zu unterwerfen – das ist nie zu oft zu wiederholen. Wenn die Frauen in den gebildeteren Ständen, denen man fast jedes Recht genommen hat – sogar auch das, für sich selbst zu sorgen, freiwillig oder von den Verhältnissen gezwungen, diese Sorge doch übernehmen wollen, so kommt dies den Leuten so wunderlich vor, daß es oft viel eher Schüchternheit und Furcht vor falscher Beurteilung, als Stolz und Dünkel ist, wenn diese Frauen nur in aller Stille sich etwas zu verdienen suchen, weibliche Arbeiten vornehmen, die sie zugleich oft, um nur eben etwas nebenbei zu verdienen, zu einem <hi rend="italic" xml:id="tg277.3.9.1">billigeren Preise</hi> herstellen, als sie es könnten, wenn sie ganz davon leben müßten. Die armen Mädchen, die in diesem Falle sind, sehen sich aber dadurch genötigt, <hi rend="italic" xml:id="tg277.3.9.2">um nur überhaupt Arbeit zu bekommen</hi>, sie noch billiger als jene zu liefern, und so erdrücken sie sich gegenseitig durch die Last der Konkurrenz.</p>
                    <p rend="zenoPLm4n0" xml:id="tg277.3.10">An dem ganzen Elend ist doch nur die ganze verkehrte Erziehung, die gänzliche rechtlose Stellung der Frauen schuld. – Dürften sie mehr lernen und würden sie nicht nur auf die Ehe als ihre <hi rend="italic" xml:id="tg277.3.10.1">einzige</hi> Bestimmung hingewiesen, <pb n="143" xml:id="tg277.3.10.2"/>
sondern würden sie vor allen Dingen gelehrt und würde ihnen Gelegenheit gegeben: sich selbst durchs Leben zu helfen und ihren Lebensunterhalt wenigstens so lange zu verdienen, bis ein geliebter Mann ihnen diese Sorge abnähme, indem er ihnen dafür eine heiligere und schönere Sorge auferlegte – nicht die für das eigene Leben, sondern für das Glück eines geliebten Gatten und hilfloser Kinder – so würde die <hi rend="italic" xml:id="tg277.3.10.3">Ehe</hi> aufhören zu einer <hi rend="italic" xml:id="tg277.3.10.4">»Versorgungsanstalt</hi>« herabgewürdigt zu werden; die Zahl der leichtsinnig und unüberlegt geschlossenen, der unglücklichen Ehen würde sich sehr vermindern – und dadurch auch die der verarmten Familien und verwahrlosten Kinder – und die Häuser der Prostitution würden nicht mehr von <hi rend="italic" xml:id="tg277.3.10.5">unglücklichen</hi>, sondern nur von <hi rend="italic" xml:id="tg277.3.10.6">verworfenen</hi> Frauen bewohnt werden.</p>
                    <p rend="zenoPLm4n0" xml:id="tg277.3.11">Die Mädchen der sogenannten »höheren« Stände, welche voraussehen, daß mit dem Tode ihres Familienhauptes das bisherige sorgenlose Leben aufhört, die sich aber nie vorbereiten durften und keine Gelegenheit sahen, sich allein durchs Leben zu helfen, werden den ersten besten Mann heiraten. Ein solches Mädchen wird unglücklich machen und unglücklich sein – und [wie] eine Verbrecherin steht sie am Altar, wenn sie dem Liebe schwört, den sie vielleicht haßt, oder indem sie das Bild eines andern im Herzen trägt, den sie nicht heiraten konnte, weil er arm war.</p>
                    <p rend="zenoPLm4n0" xml:id="tg277.3.12">Die Mädchen der untern Stände, welche dienen müssen, suchen auch einen Mann zu bekommen, nur »um es hernach besser zu haben«, da sie doch nicht zeitlebens dienen mögen – so geben sie sich oft leichtsinnig an einen armen Menschen hin, der die Braut oft verläßt, wenn sie ein Kind hat – oder das Paar wird getraut, und die unglücklichste Proletarier-Familie fällt bald der Gemeinde zur Last.</p>
                    <p rend="zenoPLm4n0" xml:id="tg277.3.13">Die armen Mädchen aber, die sich daheim etwas verdienen müssen und von früh bis in die Nacht arbeitend nur 1 und 2 Ngr. verdienen können – vielleicht auch nicht genug Arbeit bekommen und alte Eltern und kleine Geschwister zu ernähren haben – sie, die vielleicht auch gehört, »die weibliche Bestimmung« sei die Liebe oder die Ehe – und nie eine Aussicht haben, sich zu verheiraten – sie werden leicht der Verführung zum Opfer fallen – – und dann gehet hin und werft den ersten Stein auf die, welche die eigene Verzweiflung und die Lüsternheit der Männer zur Prostituierten machte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg277.3.14">Das ist die Demoralisation unserer Gesellschaft! Ihr mögt dagegen noch so sehr eifern, ihr werdet sie nicht wegbringen können – solange ihr nicht die gesellschaftliche Ungleichheit in ihren fürchterlichsten Konsequenzen aufgehoben, solange ihr nicht die Arbeit organisiert habt. –</p>
                    <p rend="zenoPLm4n0" xml:id="tg277.3.15">Wenn ihr aber nur die Arbeit der Männer organisiert und an die Arbeit der Frauen nicht denkt – so wird die Demoralisation dieselbe bleiben – wenigstens <hi rend="italic" xml:id="tg277.3.15.1">das</hi> bedenkt, wenn ihr nicht an die Rechte der Frauen denken wollt!</p>
                    <p rend="zenoPLm4n0" xml:id="tg277.3.16">Ich aber <hi rend="italic" xml:id="tg277.3.16.1">werde</hi> an sie denken und euch zuweilen an sie erinnern!</p>
                    <lb xml:id="tg277.3.17"/>
                    <p xml:id="tg277.3.18">
                        <hi rend="italic" xml:id="tg277.3.18.1">Meißen</hi>
                    </p>
                    <closer>
                        <seg type="closer" rend="zenoPR" xml:id="tg277.3.19">
                            <hi rend="italic" xml:id="tg277.3.19.1">Louise Otto</hi>
                        </seg>
                    </closer>
                    <lb xml:id="tg277.3.20"/>
                    <pb n="144" xml:id="tg277.3.21"/>
                </div>
            </div>
        </body>
    </text>
</TEI>