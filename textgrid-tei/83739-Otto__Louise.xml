<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg280" n="/Literatur/M/Otto, Louise/Essays/Aufsätze aus der »Frauen-Zeitung«/Für die Arbeiterinnen. Zweiter Artikel">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Für die Arbeiterinnen. Zweiter Artikel</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Otto, Louise: Element 00251 [2011/07/11 at 20:31:47]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <notesStmt>
                <note>
                                 Erstdruck in: Frauen-Zeitung, redigirt von Louise Otto, Leipzig, 1. Jg., Nr. 34, 8. Dezember 1849.
                              </note>
            </notesStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>»Dem Reich der Freiheit werb’ ich Bürgerinnen«. Die Frauen-Zeitung von Louise Otto. Herausgegeben und kommentiert von Ute Gerhard, Elisabeth Hannover-Drück und Romina Schmitter, Frankfurt a.M.: Syndikat, 1980.</title>
                        <author key="pnd:118590901">Otto, Louise</author>
                    </titleStmt>
                    <extent>160-</extent>
                    <publicationStmt>
                        <date when="1980"/>
                        <pubPlace>Frankfurt a.M.</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1819" notAfter="1895"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg280.3">
                <div type="h4">
                    <head type="h4" xml:id="tg280.3.1">
                        <pb n="160" xml:id="tg280.3.1.1"/>
                        <pb type="start" n="179" xml:id="tg280.3.1.2"/>Für die Arbeiterinnen.</head>
                    <head type="h4" xml:id="tg280.3.2">Zweiter Artikel</head>
                    <p xml:id="tg280.3.3">
                        <hi rend="italic" xml:id="tg280.3.3.1">Inhalt:</hi> Das Zeitalter des <hi rend="italic" xml:id="tg280.3.3.2">Rechtes</hi> muß beginnen – Demoralisation in den Palästen und »wilden Vierteln« – Entartung der Familie – Eine Proletarier-Familie – Proletarier-Kinder – Engelmacherei</p>
                    <lb xml:id="tg280.3.4"/>
                    <p xml:id="tg280.3.5">Ja, es ist viel, was ich auf dem Herzen habe, wenn ich an die Lage der Arbeiterinnen, an die Stellung der Frauen überhaupt erinnert werde. Ein Schrei des Entsetzens hat sich schon oft aus meiner Brust gerungen, die Röte der Scham ist auf meine Wangen getreten und heiße Tränen in meine Augen, wenn ich das Elend, die Niedrigkeit, die Verworfenheit und Vernachlässigung gesehen habe, in der Tausende meines Geschlechts schmachten. Ich habe es längst gefühlt, daß es nicht so bleiben könne, daß die ganze Nation an der allgemeinsten <pb n="179" xml:id="tg280.3.5.1"/>
Demoralisation zugrunde gehen müsse, wenn nicht durch gründliche Reformen geholfen werde. O es ist kindisch, nur im Umsturz eines Throns allein das Heil der Welt zu suchen, indes die Unmoralität der Geldherrschaft unangetastet bleibt. Ja, es ist auch viel und schön von den »unveräußerlichen Menschenrechten« geschrieben worden, aber bei alledem ist nur von Männerrechten die Rede gewesen – an die Rechte der Frauen hat man nicht gedacht. Aber die jetzige gesellschaftliche Unordnung wird nicht eher aufhören und nicht eher zur gesellschaftlichen Ordnung verkehrt werden, bis auch die Frauen Menschenrechte haben. Zuweilen wohl, wenn ein Blick auf das weibliche Proletariat gefallen, ist das Mitleid rege geworden und die Liebe hat helfen wollen – nein, weg damit! kein Mitleid, keine Liebesspenden, keine Wohltaten! – das ist entwürdigend. Ihr sagt es täglich: die Völker sind mündig geworden, <hi rend="italic" xml:id="tg280.3.5.2">das Zeitalter des Rechtes muß beginnen!</hi> nun, ihr könnt nicht für die einen fordern, was ihr den andern verweigert, – die Völker bestehen aus Männern und Frauen – darum muß Männern und Frauen das <hi rend="italic" xml:id="tg280.3.5.3">Recht</hi> werden.</p>
                    <p rend="zenoPLm4n0" xml:id="tg280.3.6">Schon oft, seitdem ich schreibe und auf dies Thema kam, habe ich mich dagegen verwahrt, zu den Frauen gezählt zu werden, welche die weibliche Würde zu erhöhen meinen, indem sie sich zur Karikatur von Männern machen, und wer mich oder mein Schreiben und Wirken irgend kennt, kann wissen, daß ich nie eine Forderung gestellt habe, die mit der weiblichen Natur in Widerspruch stünde – aber für die andern, die mich nicht kennen, wiederhole ich es auch hier wieder, daß ich das, was man gewöhnlich Frauen-Emanzipation nennt, für eine Verirrung erkläre und eben weil die Natur zwei verschiedene Ge schlechter schuf, auch eine verschiedene Stellung beider <hi rend="italic" xml:id="tg280.3.6.1">nebeneinander</hi> anerkenne und fordere – aber nicht <hi rend="italic" xml:id="tg280.3.6.2">über</hi>- und <hi rend="italic" xml:id="tg280.3.6.3">untereinander</hi> – denn als Menschen schuf die Natur sie <hi rend="italic" xml:id="tg280.3.6.4">gleichberechtigt</hi> zur Arbeit, zur Ausbildung, zum Genuß – zum <hi rend="italic" xml:id="tg280.3.6.5">Leben</hi> mit einem Wort. In diesem Sinne sage ich alles, was ich über dieses Thema zu sagen habe, in diesem Sinne habe ich schon vieles gesagt – aber noch vieles ist zu sagen übrig.</p>
                    <p rend="zenoPLm4n0" xml:id="tg280.3.7">Ich sprach von der allgemeinen Demoralisation. Niemand wird wagen, sie noch hinwegzuleugnen. »Die Korruption der höhern Stände« ist der Revolution vorhergegangen. Diese Korruption war ein stehender Artikel unserer Tagespresse geworden und wäre es noch mehr gewesen, wenn die Zensur nicht oft die Veröffentlichung dieser Beispiele der Entsittlichung an den Höfen und unter der Aristokratie verhindert hätte. Aber schon was man erfuhr und was wie die Frechheiten einer Lola Montez gar nicht einmal verborgen bleiben konnte noch wollte, ließ alle tiefe Blicke in eine Gesellschaft tun, deren Entsittlichung denen von uns, die zuweilen das Privatleben mit ihr in Berührung gebracht, längst kein Geheimnis mehr war. Wir sehen nun, wie es zuging in den Palästen der Hauptstadt, wo Pracht und Glanz herrschen – und dort, wo das Elend wohnt, im »wilden Viertel« der großen Städte, gewahrten wir ein Seitenstück – hier der Mangel und die Versuchungen der Not – dort der Überfluß und die Üppigkeit mit ihren Reizen zum Verbrechen – hier zum Tierleben durch die tiefste Erniedrigung herabgedrückte Menschen – dort durch Überfeinerung vergiftete und zu den unnatürlichsten Verbrechen Befähigte – und dazwischen ein Bürgerstand, angesteckt von dem Gift, das von diesen beiden Seiten sich an ihn drängt, aber ihn doch noch nicht ganz durchdrungen hat – das sind unsere Zustände. – Wo diese Entsittlichung am größten, in den sogenannten »vornehmsten«, wie in den niedrigsten Ständen, da <pb n="180" xml:id="tg280.3.7.1"/>
ist der Grundpfeiler der staatlichen Ordnung zerbröckelt – ich meine: die <hi rend="italic" xml:id="tg280.3.7.2">Familie</hi>. Ihr entscheidender Einfluß auf das Wohl eines Staates, auf die Moralität und Humanität aller ist unbestreitbar. Die Entartung der Familie geht mit der des Staates Hand in Hand – alle Zeiten haben das gelehrt, und die Gegenwart lehrt es wieder. Eines hat das andere bedingt. Die Familienglieder sind gleichzeitig auch Staatsglieder und können, wenn sie dem engern Pflichtenkreise, an den die Natur sie durch die heiligsten, festesten Bande kettete, nicht genügen, noch weniger jenem größern entsprechen, der die Pflichten der Familienliebe als Pflichten der Humanität von einzelnen Familiengliedern auf alle Staatsglieder überträgt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg280.3.8">Wie steht es aber mit der Familie des Proletariats?</p>
                    <p rend="zenoPLm4n0" xml:id="tg280.3.9">Da ist eine solche Familie: Ein Tagelöhner ist so glücklich, mit seiner Frau und seinen Kindern eine eigene Wohnung zu bewohnen – oft wohnen und schlafen viele Familien in <hi rend="italic" xml:id="tg280.3.9.1">einem</hi> Behältnis, und ein Kreidestrich oder etwas derartiges bildet die Scheidewand – der Mann ist Fabrikarbeiter und geht früh mit Tagesanbruch in die Fabrik – die Frau geht auf Tagelohn. Was geschieht unterdes mit den Kindern? entweder werden sie in eine dumpfe Stube eingesperrt und allen Zufälligkeiten überlassen, oder wenn sie schon etwas größer sind und die Eltern wenig verdienen, betteln geschickt, meist so, daß das älteste Mädchen das kleinste Kind mit sich herumschleppen muß, um das Mitleid der Leute zu erregen. Oder die Kinder werden auch in Fabriken geschickt und arbeiten darin in Gegenwart der Erwachsenen, die keine Rücksichten auf sie nehmen, so daß die Kinder früh alle Gewohnheiten der Erwachsenen nachahmen, die üblen schneller als die guten. Unpassende Reden treffen die Ohren der Kleinen und stumpfen früh ihr Sittlichkeitsgefühl ab. Oder die Kinder müssen irgendeinen Verdienst suchen, aufs Geratewohl. So suchen sie Beeren oder Blumen und bringen sie zum Verkauf – aber das geht nur in der guten Jahreszeit, und in den großen Städten ist es auch nicht tunlich, da müssen sie zu andern Beschäftigungen greifen – so gehen sie in die Häuser und erbetteln Knochen oder suchen sie auf den Straßen zusammen, sammeln Pferdedünger oder dergl. und bringen dies dann zum Verkauf. Kurz, zu einem solchen Erwerbszweig müssen die Mädchen greifen, und dabei wird gebettelt und oft gestohlen. Was ist wohl natürlicher? Die Kinder sind sich selbst überlassen – und nehmen wir auch an, ihre Eltern wollen nicht, daß sie stehlen, so wollen sie doch, daß sie so viel als möglich verdienen – ein Kind, das von der Arbeit eines Tages nur ein paar Pfennige mit nach Hause bringt, wird oft mit Schlägen empfangen werden – am andern Tag wird es mehr zu erwerben suchen, und gelingt es nicht auf rechtmäßige, so gelingt es auf unrechtmäßige Weise. Was weiß überhaupt ein solches Kind von Recht oder Unrecht. – Es hört das in der Schule! sagt ihr, und wenn es nun keine besucht? wenn es die Eltern davon zurückhalten, weil es darin nichts verdienen kann und die Schule noch Geld kostet? Und wenn auch – da sitzen 50 – 100 Kinder zusammen, wie kann da das einzelne schließen und denken lernen? – Und nun denkt an die ganze Schar der Kinder, die von denen, welche ihre Erzieher sein sollten, geradezu zum Verbrechen angehalten werden! – nicht etwa um des Verbrechens, sondern um des Zweckes willen, der dadurch erreicht wird! Wenn die Leute ehrlich so viel und leicht verdienen könnten, wie unehrlich, so würde dies bald niemand mehr sein! Aber die Armen möchte es oft genug erfahren, daß die ehrlichen Leute früher verhungern als die unehrlichen gehangen werden – und werden sie nur ein paar Monate eingesteckt, was ist das <pb n="181" xml:id="tg280.3.9.2"/>
weiter? dann sind sie einmal der Sorge um das tägliche Brot überhoben. Denkt an die Schar der Kinder, die nicht wissen, wer ihr Vater ist, die bei einer unsittlichen Mutter leben, in der jedes edlere Gefühl gestorben und selbst das der Mutterliebe zertreten ist – oder an die Kinder, welche entfernten Verwandten zur Last fallen, die durch sie noch ihren geringen Verdienst sich geschmälert sehen – welches wird ihr Los sein? – Ich will noch gar nicht von dem <hi rend="italic" xml:id="tg280.3.9.3">»Engelmachen</hi>« reden, das ihr wohl kennt. Wo, um durch sie zu verdienen und sie dann »los zu werden«, Kinder absichtlich verstümmelt und krank gemacht werden, damit sie durch ihr Elend auf dem Arm der Bettlerin das Mitleid erregen und endlich sterben. Ihr entsetzt euch, gefühlvolle, unerfahrene Frauen, die ihr noch nichts vom »Engelmachen« gewußt hab, und dies ist weiter nichts als ein Handelsartikel, wie es Tausende im Leben gibt, die Leute wollen nur etwas verdienen, und weil sie keine andere Erwerbsquelle wissen, machen sie Engel – gebt ihnen Brot für sie und diese Kinder, gebt ihnen eine lohnende, nährende Arbeit, und es wird ihnen nicht einfallen, ihre Scheußlichkeiten fortzutreiben. – Jene Kinder, von denen ich eben sprach, werden mit Härte von denen behandelt, die für sie sorgen sollten, diese kehren es um und lassen diese für sich sorgen. Sie schicken sie also betteln und stehlen, denn wenn ein Kind beim Diebstahl ergriffen wird, so wird es eingesteckt oder kommt in eine Korrektionsanstalt, und dann sind es seine Angehörigen ja »los«. Die Mädchen aber, die das Kindesalter verlassen haben, können den Ihrigen bald mehr einbringen – sie fallen nicht etwa der süßen Stimme der Verführung zum Opfer – nein, sie werden als ein Handelsartikel betrachtet; die Handelsspekulation anderer, die auch den Gewinn von ihnen ziehen, der Zwang und ihre eigene Unwissenheit, nicht die Stimme der Natur, sondern die der höchsten Unnatur macht sie zum Abschaum der Gesellschaft. – Die Mädchen, die als Kinder mit Blumen, Zwieback oder dergl. auf die Straßen, in die Häuser, vorzüglich in die öffentlichen Vergnügungsorte, die Wein- und Bierstuben usw. geschickt werden, sehen sich darin meist mit Härte behandelt und dadurch, daß sie zu Zeugen der Schwelgereien der Reichen gemacht werden, deren klingende Münzen sie vom Spieltisch oder in die Hände der Kellner fallen sehen, indes für sie jedermann rauhe Worte und kaum eine kupferne Münze hat, erbittert sich das jugendliche Gemüt und durch die rohen und unzüchtigen Scherze, die das Mädchen aus dem Munde Trunkener hören muß, stumpft alles zartere Gefühl sich ab. Dieses herumtreibende Leben muß die künftige Entsittlichung vorbereiten. Fände sich im Hause ein Damm dagegen! aber was die Kinder hier lernen, wo mehrere Familien Tag und Nacht in <hi rend="italic" xml:id="tg280.3.9.4">einer</hi> Stube zubringen und allen Rohheiten freien Lauf lassen, kann jeder Leser sich selbst sagen. – Das ist die Familie im »wilden Viertel«! Ihr kennt sie vielleicht schon aus Friedrich Saß' »Berlin« oder aus Eugen Sues »Martin, der Findling« – diese Schilderungen sind nicht übertrieben – aber ich mag sie nicht wiederholen.</p>
                    <closer>
                        <seg type="closer" rend="zenoPR" xml:id="tg280.3.10">(<hi rend="italic" xml:id="tg280.3.10.1">Schluß folgt</hi>.)</seg>
                        <pb n="182" xml:id="tg280.3.11"/>
                    </closer>
                </div>
            </div>
        </body>
    </text>
</TEI>