<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg288" n="/Literatur/M/Otto, Louise/Essays/Aufsätze aus der »Frauen-Zeitung«/Über Robert Blum">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Über Robert Blum</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Otto, Louise: Element 00255 [2011/07/11 at 20:31:47]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <notesStmt>
                <note>
                                 Erstdruck in: Frauen-Zeitung, redigirt von Louise Otto, Leipzig, 2. Jg., Nr. 21, 25. Mai 1850.
                              </note>
            </notesStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>»Dem Reich der Freiheit werb’ ich Bürgerinnen«. Die Frauen-Zeitung von Louise Otto. Herausgegeben und kommentiert von Ute Gerhard, Elisabeth Hannover-Drück und Romina Schmitter, Frankfurt a.M.: Syndikat, 1980.</title>
                        <author key="pnd:118590901">Otto, Louise</author>
                    </titleStmt>
                    <extent>249-</extent>
                    <publicationStmt>
                        <date when="1980"/>
                        <pubPlace>Frankfurt a.M.</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1819" notAfter="1895"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg288.3">
                <div type="h4">
                    <head type="h4" xml:id="tg288.3.1">
                        <pb n="249" xml:id="tg288.3.1.1"/>
                        <pb type="start" n="259" xml:id="tg288.3.1.2"/>Über Robert Blum</head>
                    <head type="h4" xml:id="tg288.3.2">Aus meinem Leben, zur Berichtigung Fanny Lewalds</head>
                    <p xml:id="tg288.3.3">
                        <hi rend="italic" xml:id="tg288.3.3.1">Fanny Lewald</hi> hat in ihren »Erinnerungen von 1848« in einem Brief aus Frankfurt vom 17. Okt. eine Charakteristik Robert Blums gegeben, die so durchaus falsch ist und mich so widerwärtig berührte, daß ich nicht umhin kann, sie hier zu berichtigen. Nicht in Verleumdungen R. Blums oder in Schilderungen, die von der Gegenpartei ausgehen, deren Gehässigkeit man kennen und längst verachten gelernt hat, würde ich Veranlassung zu diesem Artikel finden – ich finde sie eben darin, daß F. Lewald Demokratin zu sein scheint – (mit Gewißheit weiß man das leider bei den meisten unserer belletristischen Schriftstellerinnen nicht zu sagen!) – und daß sie eine unserer anerkanntesten Schriftstellerinnen ist, vielleicht eine Autorität für manche unserer Leserinnen, die gewiß, wie ich selbst, immer gern von ihr gelesen haben. Sie urteilt nun über R. Blum nur nach einem flüchtigen Eindruck und nach dem, was ihr vielleicht seine Feinde oder Neider, es brauchten darum noch keine politischen Gegner zu sein, von ihm gesagt haben. So darf ich wohl unsere Leserinnen bitten, ihr weniger zu glauben als mir, da ich so glücklich war, mit ihm in freundschaftlichem Verkehr zu leben.</p>
                    <lb xml:id="tg288.3.4"/>
                    <p xml:id="tg288.3.5">[...]</p>
                    <p rend="zenoPLm4n0" xml:id="tg288.3.6">Gebildete Damen, die in einem bestimmten Kreis zu leben pflegen, finden das Edle und Große nur dann heraus, wenn es in einer nicht nur ästhetischen, sondern auch geglätteten Form vor ihnen erscheint, die es eben salonfähig macht; an der einfachen volkstümlichen Urkraft, die jene Glätte sich weder aneignen konnte noch wollte, scheitert ihre Beobachtungsgabe, weil sie von vornherein sich dadurch abgestoßen findet. So ist es hier der Lewald ergangen. Fröbel hatte eine feinere Außenseite, bei aller Einfachheit des Republikaners, als Blum; diesen wird auch niemand einer Rohheit oder Gemeinheit zeihen können, aber er verleugnete niemals seine Abstammung vom Proletariat in seiner unverfälschten Naturwüchsigkeit – und hierin lag ein Teil seiner Popularität und seiner ungeheuern Macht über die Massen. Wer das Volk wahrhaft liebt und kennt, dem mußte auch der Volksmann Blum liebenswert erscheinen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg288.3.7">Ich habe sie gesehen, diese Macht, als ich ihn zuerst sah und sprechen hörte, <pb n="259" xml:id="tg288.3.7.1"/>
in jenen vormärzlichen Volksversammlungen im August 1845 im Leipziger Schützenhaus, wo die empörten Massen Rache schrien über eine Tat – die Schützen hatten bei einem nächtlichen Tumult 7 Leichen gemacht – , die damals als ein unerhörter Frevel erschien und deren Andenken erst durch die Ströme Bürger-Blut, die später überall geflossen sind und noch heute fließen, da und dort weggespült worden. Damals, als diese klangvolle Stimme den Aufgeregten zurief: »Verlasset den Boden des Gesetzes nicht!« und die, welche nach<hi rend="italic" xml:id="tg288.3.7.2"> Rache</hi> schrien, überredete, nur Forderungen der Sühne zu stellen – damals zeigte es sich, wie wenig er eine »grausame Natur« sei, wie wenig »Dämonisches« in ihm war, sondern wie er allein durch seine Besonnenheit mitten in der Begeisterung und durch das Schlagende seiner Worte und Gründe allerdings eine Art von Zauber auch auf die aufgeregtesten Massen ausübte, wie keiner vor und nach ihm vermocht hat. Damals ward Blum der Held des Tages, und selbst die geldstolze Bourgeoisie Leipzigs, die bis dahin nur verächtlich gelächelt hatte über den deutschkatholischen Theater-Sekretär, brachte ihm ihre Huldigungen dar. – Ich lebte damals 2 Monate in Gohlis bei Leipzig. Ich war mit dem Entschluß hingegangen, auch Blums zu besuchen – jetzt, als sich alles zu ihm drängte, ließ es meine Bescheidenheit nicht zu. Ich schrieb zwar damals schon in den unter seiner Leitung stehenden »Vaterlandsblättern« über das Thema »Die Teilnahme der Frauen am Staatsleben ist eine Pflicht«, aber ich hatte doch nur erst in den 2 Jahren, seit ich schrieb, so geringes geleistet, daß es mir jetzt wie Anmaßung erschien, mich zu den »Helden des Tags« zu drängen. Wohl hatten mich viele gesinnungsverwandte Schriftsteller aufgesucht, aber ich selbst hatte nirgends gewagt, den ersten Schritt dazu zu tun, ich habe es immer bis zur Ängstlichkeit vermieden, mich berühmten Personen zuerst zu nähern. – So kam es, daß ich abreiste, ohne Blum gesehen zu haben. Im Frühjahr darauf schrieb er an mich und lud mich ein, zu einem Oppositionsfest nach Leipzig zu kommen – ich konnte nicht, aber es war einer meiner glücklichsten Tage, da ich diesen Brief erhielt. Wir kamen nun in Briefwechsel, zu dem vorzüglich die konstitutionelle Staatsbürger-Zeitung und sein Volkstaschenbuch, an dem er mich zur Mitarbeit aufforderte, Veranlassung gab. Ich wußte nun, daß meine Bestrebungen nach seinem Sinne waren, ja, daß er etwas auf mich hielt, und ich kann nicht sagen, wie tröstlich und erhebend mir dies Bewußtsein war, da ich damals schon viele Anfeindungen erfuhr und nebenbei noch oft nahe genug daran war, an mir selbst zu verzweifeln. –</p>
                    <p rend="zenoPLm4n0" xml:id="tg288.3.8">Im Mai 1847 zog ich für die ganze Sommer-Saison nach Gohlis. Als ich Blums besuchen wollte, fand ich ihn nicht zu Hause – man sagte mir, daß er am andern Tag auf ein paar Wochen verreise und daß ich zu ihm in die Theater-Expedition gehen solle. Als ich diese im Theater erfragt, öffnete man mir, ohne mich anzumelden, eine Tür – und ich stand R. Blum gegenüber. Er nickte grüßend mit dem Kopfe und fragte, ohne aufzustehen: »Was steht zu Dienst?« Ich wollte schon verlegen werden über diesen Empfang und sagte meinen Namen. Ich werde es nie vergessen, wie anders plötzlich sein Gesicht ward, wie er aufsprang, mir die Hände schüttelte und mich herzlich willkommen hieß. »Ja so«, sagte er nach einer Weile, sich besinnend, »wir waren ja zuletzt im Krieg miteinander, aber auch das hat mich gefreut: die Sache gilt uns mehr als die Personen, und wir scheuen für unsere Überzeugung weder Freund noch Feind.« Ich antwortete ihm, daß ich deshalb auch den Widerspruch gewagt und daß wir so beide einander mehr vertrauen könnten, als <pb n="260" xml:id="tg288.3.8.1"/>
wenn er mich geschont und ich mich seiner Autorität gebeugt hätte. – Der »Krieg« war in der »Staatsbürger-Zeitung« geführt worden. Blum hatte mein »Schloß und Fabrik« darin sehr ehrend besprochen, zuletzt aber hinzugefügt: es sei eine Sünde wider das Prinzip, daß ich das Buch nicht habe 20 Bogen frei erscheinen lassen. Nun war das Buch schon deshalb konfisziert gewesen, und ich hatte seine Freigabe nur erlangt, nachdem ich mich zur Umänderung und Umdruckung einiger Stellen im 2. und 3. Band entschlossen. – Die Zensur gestattete mir aber nicht diese Tatsache zu veröffentlichen (es waren schöne Zeiten!), und mir blieb nichts übrig, als mich gegen Blums Beschuldigung »einer Sünde wider das Prinzip« damit zu verteidigen, daß ein Roman doch nur auf ein größeres Publikum wirken könne, wenn er nicht konfisziert sei, und daß ich dieser Wirkung in weiteren Kreisen einige stärkere Stellen geglaubt hätte gerade dem Prinzip: Wirkung auf die Massen, zum Opfer bringen zu müssen. – Von da an war ich viel mit Blum zusammen. [...]</p>
                    <closer>
                        <seg type="closer" rend="zenoPR" xml:id="tg288.3.9">
                            <hi rend="italic" xml:id="tg288.3.9.1">L.O.</hi>
                        </seg>
                        <pb n="261" xml:id="tg288.3.10"/>
                    </closer>
                </div>
            </div>
        </body>
    </text>
</TEI>