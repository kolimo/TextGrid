<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg1165" n="/Literatur/M/Pröhle, Heinrich/Sagen/Rheinlands schönste Sagen und Geschichten/Burg Gutenfels bei Kaub">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Burg Gutenfels bei Kaub</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Pröhle, Heinrich: Element 01309 [2011/07/11 at 20:31:24]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Heinrich Pröhle: Rheinlands schönste Sagen und Geschichten. Für die Jugend. Berlin: Tonger &amp; Greven, 1886.</title>
                        <author key="pnd:116292199">Pröhle, Heinrich</author>
                    </titleStmt>
                    <extent>107-</extent>
                    <publicationStmt>
                        <date when="1886"/>
                        <pubPlace>Berlin</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1822" notAfter="1895"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg1165.3">
                <div type="h4">
                    <head type="h4" xml:id="tg1165.3.1">
                        <pb n="107" xml:id="tg1165.3.1.1"/>
Burg Gutenfels bei Kaub.</head>
                    <p xml:id="tg1165.3.2">Graf Philipp von Kaub wohnte im dreizehnten Jahrhundert auf der Burg Falkenstein bei Kaub. Er war noch unvermählt, denn seine schöne Schwester Guta ersetzte ihm die Burgfrau. An vielen Bewerbern, die um ihretwillen nach dem Falkensteine hinaufritten, fehlte es ihr nicht, aber wie reich sie auch zum Teil waren, so konnte doch keiner von ihnen ihr Herz gewinnen, und so war sie gleich ihrem Bruder unverheiratet geblieben.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1165.3.3">Einstmals fuhren die beiden Geschwister auf einem schönen Schiffe den Rhein hinab, um in Köln einem Turnier beizuwohnen. Unter den Rittern, welche dort ihr Rößlein tummelten, war auch ein Fremder, der sich durch das prächtige Aussehen seiner Waffen und seiner Rüstung hervorthat. Seine Heimat war England, und wenn ihn auch die Ritter noch nicht kannten, so wurde doch der Erzbischof von Köln, an dessen Hofe das Turnier abgehalten wurde, mit ihm oft in einer vertrauten Unterredung erblickt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1165.3.4">Der Ritter zog bald auch Guta's Blick auf sich und kaum hatte sie ihn mit der Aufmerksamkeit betrachtet, welche seine hohe Erscheinung beanspruchen konnte, so fühlte sie sich zum ersten male im Leben vom Strahle der Liebe getroffen. Auch der fremde Ritter hatte in dem nämlichen Augenblicke die schöne Guta angeschaut, und als diese nun vor Verlegenheit <pb n="108" xml:id="tg1165.3.4.1"/>
einen ihrer Handschuhe zur Erde fallen ließ, hob der tapfere Held ihn auf und befestigte ihn an seinem Helme. Fast unbeweglich und ungerührt stand er nun vor der Königin des Festes, welche ihm den ersten Siegespreis erteilen mußte: ihr Blick machte während dieser feierlichen Handlung keinen Eindruck auf ihn, da sein Herz an die Dame vergeben war, deren Handschuh er trug.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1165.3.5">Erst bei dem Bankett, welches der Verteilung der Kampfpreise folgte, konnte sich der fremde Ritter der Jungfrau, deren Handschuh er aufgehoben hatte, wieder nähern. Mehr als einmal forderte er sie zum Tanze auf, dann aber legten beide einander auch das Geständnis ihrer Liebe ab.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1165.3.6">Es war Guta von dem Ritter verboten worden, ihren Bruder von diesem allen in Kenntnis zu setzen. Als dieser aber auf dem Bankett den Ritter wiederholt mit seiner Schwester tanzen sah, näherte er sich ihm selbst, wie es jedenfalls für den Begleiter der schönen Guta schicklich war. Es befremdete den Grafen von Falkenstein, daß der Ritter auch ihm gegenüber trotz mancher neugierigen Anspielungen, zu denen er sich als Bruder durch das Benehmen des Fremden gegen Guta berechtigt glaubte, seinen Stand und Namen verschwieg. Doch machte der Ritter den Eindruck eines Mannes von ebenso hoher Weltbildung als biederer und treuherziger Gesinnung. Besonders freute es den Grafen, daß der andere sich auf das sorgfältigste nach Guta's Familie sowie nach ihrer heimatlichen Burg Falkenstein erkundigte, und so trug er denn auch kein Bedenken, den Ritter auf das freundlichste dahin einzuladen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1165.3.7">Nach einiger Zeit erschien der fremde Ritter mit einem Knappen auf der Burg Falkenstein. Er blieb drei Tage dort oben, und wenn er schon durch das Aufheben des Handschuhes, sowie durch seine Gespräche mit Guta auf dem Bankett sich hinlänglich als deren Anbeter erklärt hatte, so schied er diesmal nicht von ihr, ohne daß sich beide das heilige Versprechen gegeben hatten, mit einander ein Ehebündnis zu schließen. Aber auch diesmal wurde der Jungfrau strenge Verschwiegenheit anempfohlen, und der Name des Ritters blieb ihr unbekannt. Der Graf von Falkenstein nahm an, daß er als Engländer der Fahne Richards von Cornwallis folge, welcher damals die Welt mit dem Rufe seines Reichtums und seiner Tapferkeit erfüllte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1165.3.8">
                        <pb n="109" xml:id="tg1165.3.8.1"/>
Im deutschen Reiche aber waren damals die traurigen Zeiten der Zwischenherrschaft. Nachdem Konradin der Hohenstaufe in Italien hingerichtet war, wollten nur noch fremde Fürsten die deutsche Kaiserwürde annehmen. In dieser schrecklichen Zeit, da die Raubritter in Deutschland schalteten, wurde auch Richard von Cornwallis zum deutschen Kaiser gewählt. Nach der Wahl ertönte ein Freudenschrei durch manche Gegenden unseres damals so unglücklichen Vaterlandes, besonders aber jubelten die Kölner, die sich manche Vorteile von dieser Wahl versprechen durften. Aber Guta lag in diesen Tagen krank vor Sehnsucht und Kummer auf Burg Falkenstein, denn sie hatte seit der Abreise ihres Bräutigams nichts mehr von ihm vernommen. Da schmetterten die Fanfaren, denn der neuerwählte Kaiser zog mit seiner Schar nach Burg Falkenstein hinauf. Mit geschlossenem Visier ritt er in den Burghof ein und wurde von dem Grafen mit aller Ehrfurcht begrüßt, welche einem deutschen Kaiser gebührte. Der Kaiser fragte den Grafen sogleich nach seiner Schwester. Dabei hätte dieser wohl erkennen sollen, daß die Stimme des Kaisers dieselbe war, welche er auf dem Bankett und dann auf Burg Falkenstein während der Anwesenheit des englischen Ritters gehört hatte. Doch fiel ihm dies nicht auf, da ihm der Accent, mit welchem diese damals am Rheine sehr häufigen Fremdlinge aus Britannien seine deutsche Muttersprache redeten, überhaupt wenig bekannt war, so daß es ihm nicht leicht wurde, diese Männer zu verstehen. Jedoch war auch der Sinn der Worte, die Richard von Cornwallis zu dem Grafen sprach, seltsam und auffallend. Er behauptete, daß unter seinem Gefolge sich ein Ritter befände, welchem die Gräfin Guta ihre Hand versprochen habe und der als Pfand ihrer Zuneigung ihren Handschuh aufbewahre, welchen er auf dem Turnier aufgehoben habe. Er selbst, der Kaiser, wollte nun für diesen Ritter um Guta werben.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1165.3.9">Von dem, was der Kaiser zu ihr gesagt hatte, war dem Grafen von Falkenstein zwar bisher durch seine Schwester manches verschwiegen worden; doch da der Graf merkte, daß es sich um den Ritter handelte, der ihn auf seiner Burg besucht hatte und seit dessen Abreise Guta in Trauer versunken war, so wollte er denselben eiligst im Gefolge des Kaisers suchen. Richard von Cornwallis bemerkte diese Absicht, hielt den Grafen zurück und befahl <pb n="110" xml:id="tg1165.3.9.1"/>
ihm gebieterisch, sogleich die Botschaft an seine Schwester auszurichten und sie herbeizuführen, selbst wenn sie krank sei, weil ja – wie er sagte – die Liebe ein Heilmittel gegen Leid und Krankheit habe.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1165.3.10">Obgleich Guta sich nicht einmal bei der Nachricht von der Ankunft des Kaisers von ihrem Krankenlager erhoben hatte, so sprang sie doch wie ein Reh empor, als sie durch ihren Bruder die Bestellung erhielt, welche ihr Richard von Cornwallis machen ließ. Sie fand den Kaiser und sein Gefolge im Rittersaale. Ihr erster Blick überflog das Gefolge des Kaisers, welches auch der Graf, der mit ihr in der Saalthüre stehen blieb, mit dem Auge musterte, ehe er die Schwester zum Kaiser führte. Erst als ihr Auge ebensowenig als das ihres Bruders im Stande war, aus dieser Menge sogleich den älteren Besucher herauszufinden, fiel ihr fragender Blick auch auf den Kaiser, gleichsam als wollte sie ihn nun schon von weitem zum Reden und zu einer näheren Erklärung über seinen Lehnsmann, der um ihre Hand geworben hatte, veranlassen. Als aber Guta aus diesem Grunde zuletzt auch den Kaiser betrachtete, hatte er das Visier des Helmes geöffnet und sie erkannte staunend, daß Richard von Cornwallis und jener Ritter aus England ein- und dieselbe Person waren.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1165.3.11">Die Burg Falkenstein bei Kaub erhielt von der Kaiserin Guta zum Andenken an diese Begebenheit den Namen Gutenfels.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1165.3.12">Wenn wir auch Richard von Cornwallis nur als einen Fremdling ansehen und keineswegs zu den deutschen Kaisern rechnen, auf welche wir stolz sind, so kann es doch als eine Anerkennung für die guten persönlichen Eigenschaften dieses Gemahles der rheinischen Gräfin Guta durch seine deutschen Zeitgenossen angesehen werden, daß sie trotz seines geringen Ansehens im deutschen Reiche doch nicht vor Richard's Tode Rudolf von Habsburg zu seinem Nachfolger erwählt haben.</p>
                </div>
            </div>
        </body>
    </text>
</TEI>