<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg11" n="/Literatur/M/Rabener, Gottlieb Wilhelm/Satire/Sammlung satirischer Schriften/1. Satirische Abhandlungen und Erzählungen/Von Unterweisung der Jugend">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Von Unterweisung der Jugend</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Rabener, Gottlieb Wilhelm: Element 00015 [2011/07/11 at 20:31:42]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Rabeners Werke. Auswahl von August Holder. Ein Beitrag zur Kulturgeschichte und Pädagogik des achtzehnten Jahrhunderts, Halle a.d.S.: Otto Hendel, [1888].</title>
                        <author key="pnd:118743368">Rabener, Gottlieb Wilhelm</author>
                    </titleStmt>
                    <extent>48-</extent>
                    <publicationStmt>
                        <date when="1888"/>
                        <pubPlace>Halle a.d.S.</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1714" notAfter="1771"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg11.3">
                <div type="h4">
                    <head type="h4" xml:id="tg11.3.1">Von Unterweisung der Jugend.</head>
                    <milestone unit="head_start"/>
                    <p rend="zenoPC" xml:id="tg11.3.2">
                        <seg rend="zenoTXFontsize80" xml:id="tg11.3.2.1">(Belustigungen des Verstandes und Witzes. Oktober 1742).</seg>
                    </p>
                    <milestone unit="head_end"/>
                    <lb xml:id="tg11.3.3"/>
                    <p xml:id="tg11.3.4">Ich habe unsern gestrigen Unterredungen weiter nachgedacht, mein werter Hermann. Wir bemühten uns, ausfindig zu machen, warum es so schwer sei, eine gründliche Gelehrsamkeit zu erlangen, und woher es komme, daß so wenige unter den Gelehrten den ansehnlichen Titel verdienen, mit welchem sie ihre Blöße sorgfältig zu bedecken wissen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg11.3.5">Die von dir angeführten Ursachen sind wichtig genug. Die blinde Liebe der meisten Eltern geht dahin, ihre Kinder zu ansehnlichen Mitgliedern des gemeinen Wesens zu machen. Der Sohn muß studieren, damit er Doktor werden kann. Er hat weder die Fähigkeit noch den Willen, etwas Rechtschaffenes zu lernen. Er lebt also sich zur Last und dem Vaterlande zum Schimpf. Wäre dieser ein Schneider geworden, so würde er gewiß sein Brot verdienen – da er jetzt von der Sparsamkeit seiner Vorfahren oder dem Einbringen seiner Frau leben muß.</p>
                    <p rend="zenoPLm4n0" xml:id="tg11.3.6">Du hast recht, mein Freund; vielleicht aber giebst du mir auch Beifall, wenn ich eine Ursache anführe, welche noch allgemeiner ist.</p>
                    <p rend="zenoPLm4n0" xml:id="tg11.3.7">
                        <pb n="48" xml:id="tg11.3.7.1"/>
Erwäge nur einmal, wie die Anführung unsrer Jugend zu der Gelehrsamkeit beschaffen ist. Bis in das zehnte Jahr überläßt man uns der Aufsicht der Frauenzimmer, welche glauben, sie haben genug gethan, wenn sie uns reinlich halten, wenn sie uns lesen lehren und allenfalls einige Fragen aus dem Katechismus ins Gedächtnis bringen. Nunmehr ist es Zeit, daß man uns der Aufsicht eines Hofmeisters übergiebt. Ob er von guten Sitten, ob er fleißig, ob er gelehrt ist: darnach fragt man nicht. Aber, wie viel verlangt der Herr für seine Mühe? das ist unsre erste Frage. Der Wohlfeilste bleibt allemal der Beste. Dieser führt uns eben den Weg, welchen er selbst unter so vielen Seufzern und Thränen gegangen ist.</p>
                    <p rend="zenoPLm4n0" xml:id="tg11.3.8">Ein Gelehrter muß die lateinische Sprache verstehen. Die Sache hat ihre Richtigkeit. Man wählt also eine Grammatik, welche die beste zu sein scheint. Durch eine unermüdete und oftmals nachdrückliche Unterweisung fassen wir eine Menge dunkler Kunstwörter und weitläufiger Regeln, welche wir gewiß noch weniger verstehen als die Sprache selbst, die wir daraus erlernen sollen. Endlich überwinden wir diese Schwierigkeit. Man giebt uns des Cicero Schriften nebst andern Büchern zu lesen, und unsre Väter weinen vor Freuden, wenn sie sehen, daß ihre Kinder im zwanzigsten Jahre dasjenige begriffen haben, was zu des Cicero Zeiten in Rom ein Junge von fünf Jahren verstand. Nunmehr zieht der gelehrte oder (besser zu sagen) der lateinische Sohn auf hohe Schulen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg11.3.9">Du darfst von ihm nicht verlangen, daß er in der alten und neuern Geschichte, in der Geographie, Genealogie, Zeitrechnung, Wappenkunst und dergleichen erfahren sein und einen Vorschmack von der Mathematik, Weltweisheit und andern Wissenschaften erlangt haben sollte. Dazu hat er nicht Zeit gehabt, er hat müssen Latein lernen. Es würde lächerlich sein, wenn du ihn fragen wolltest, ob er deutsch verstände? ob er einen guten Brief schreiben könnte? Er ist ja ein Deutscher, er ist in Meißen geboren; sollte er nicht deutsch verstehen? Von der griechischen Sprache hat er noch zur Not so viel begriffen, als er auf der hohen Schule binnen drei Jahren zu verlernen gedenkt. Wie geschwind verlaufen diese! Er muß eiligst nach Hause. Sein Vater verlangt es, weil ein Amt und eine reiche Frau auf ihn warten. Nunmehr ist unser Gelehrter fertig.</p>
                    <p rend="zenoPLm4n0" xml:id="tg11.3.10">Sage mir, mein Freund, ob nicht dieses die gewöhnlichste Art sei, unsre Jugend zu unterweisen. Du wirst es nicht <pb n="49" xml:id="tg11.3.10.1"/>
leugnen können; du wirst aber auch zugleich gestehen müssen, daß solches die wahrhafte Ursache sei, warum nur so wenige sich eine rechtschaffene Gelehrsamkeit erwerben. Der ganze Fehler beruht meines Erachtens darin, daß wir glauben, wer die lateinische Sprache verstehe, der sei ein Gelehrter, und daß wir durch eine weitläufige Erlernung derselben diejenige Zeit versäumen, welche wir zugleich auf nützlichere Sachen verwenden sollten.</p>
                    <p rend="zenoPLm4n0" xml:id="tg11.3.11">Aber soll ein Gelehrter kein Latein verstehen? Dieses ist meine Meinung keineswegs. Ich behaupte vielmehr, daß er in dieser Sprache ebenso stark sein müsse als in seiner Muttersprache. Nur das kann ich nicht begreifen, <hi rend="spaced" xml:id="tg11.3.11.1">warum wir der Jugend die Erlernung derselben so schwer machen</hi>.</p>
                    <p rend="zenoPLm4n0" xml:id="tg11.3.12">Der alte Richard, welcher gestern in unsrer Gesellschaft war, soll mir zum Beweise meines Satzes dienen. Du kennst seinen Sohn, der jetzt durch wirkliche Verdienste unter den Gelehrten eine ansehnliche Stelle bekleidet. Kaum hatte dieser das sechste Jahr erreicht, als ihm sein sorgfältiger Vater der Aufsicht eines jungen Menschen anvertraute, welcher ihm die nötigsten Gründe unsers Glaubens beibringen und ihn zu einer wohlanständigen Aufführung gewöhnen sollte. Alles, was er mit dem Knaben redete, was ihn dieser fragte, das mußte, soviel es möglich sein wollte, in lateinischer Sprache geschehen. Jede Sache, die im Hause, auf der Gasse, in der Küche oder im Garten vorkam, die gemeinsten Geschäfte, welche täglich vorfielen, wurden auf lateinisch benannt. Diese Bemühung ging glücklich von statten. Nach Verlauf einer Zeit von vier Jahren war der junge Richard schon vermögend, sich in der lateinischen Sprache ordentlich und deutlich auszudrücken und regelmäßig zu reden, ohne zu wissen, warum er seine Worte eben so und nicht anders setzen müsse. Nunmehr glaubte man, daß es Zeit wäre, ihn die vornehmsten Regeln der Grammatik zu lehren, und weil er die Sprache schon verstand, so faßte er diese in wenigen Monaten.</p>
                    <p rend="zenoPLm4n0" xml:id="tg11.3.13">Die griechische Sprache war ihm als einem künftigen Gelehrten zu wissen unentbehrlich. Weil aber sein Vater meinte, es sei eine gelehrte Eitelkeit, griechisch zu reden oder dergleichen Schriften und Gedichte zu verfertigen, so schien es genug zu sein, ihn nach den ordentlichen Regeln so weit zu bringen, daß er alles verstände, was griechisch abgefaßt wäre. Er erlangte auch solche Geschicklichkeit wirklich in wenigen Jahren. Weil man dieses nicht zu einem Hauptwerk machte, <pb n="50" xml:id="tg11.3.13.1"/>
so blieben noch Stunden genug übrig, ihm in andern Künsten und Wissenschaften Unterweisung zu geben.</p>
                    <p rend="zenoPLm4n0" xml:id="tg11.3.14">Nach unserer heutigen Einrichtung ist es eine bekannte Sache, daß die französische Sprache oft weit unentbehrlicher ist, als alle toten Sprachen der Morgenländer. Man nahm also einen Franzosen an, welcher ihn durch Unterricht und fleißigen Umgang zu der gehörigen Vollkommenheit brachte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg11.3.15">Hatte ihm sein Hofmeister schon in den ersten Jahren bloß durch Gespräche – wo nicht eine <hi rend="spaced" xml:id="tg11.3.15.1">Kenntnis</hi> von der Historie, dennoch eine <hi rend="spaced" xml:id="tg11.3.15.2">Lust</hi> dazu beigebracht, so war es nachher um so viel leichter, auch darin weiter zu gehen. Die ältere Geschichte wurde nicht vergessen; die neuere aber – und besonders die Geschichte seines Vaterlandes – blieb allemal der Hauptzweck.</p>
                    <p rend="zenoPLm4n0" xml:id="tg11.3.16">Die größeren Schriften der lateinischen Redner und Poeten wurden zugleich sorgfältig durchgegangen, nicht sowohl die Redensarten daraus zu erlernen, als vielmehr ihren ganzen Bau und die Bündigkeit des Vortrags einzusehen. Hierdurch lernte unser Richard die Zärtlichkeit einer Ode, die Stärke eines Heldengedichts und diejenigen Ursachen kennen, welche den Cicero zu einem Redner gemacht haben.</p>
                    <p rend="zenoPLm4n0" xml:id="tg11.3.17">Was konnte ihm auf eine solche Art wohl leichter fallen, als auch in seiner Muttersprache die Geschicklichkeit zu erlangen, die einem Gelehrten so wohlanständig ist?</p>
                    <p rend="zenoPLm4n0" xml:id="tg11.3.18">Man brachte ihm einen Begriff von der Weltweisheit bei, so weit er nämlich bei seinem damaligen Alter dazu vermögend war; und man brauchte zugleich die Vorsicht, die Kräfte seines Verstandes und Nachdenkens durch die mathematischen Wissenschaften zu schärfen und in Ordnung zu bringen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg11.3.19">Zu seiner Gemütsergötzung ward ihm ein Tanzmeister und ein Zeichenmeister nebst andern Künstlern gehalten, und Richard ist dennoch ein Gelehrter, ob er gleich wider die bisherige Gewohnheit gelernt hat, wie man leserlich und zierlich schreiben müsse.</p>
                    <p rend="zenoPLm4n0" xml:id="tg11.3.20">Wenn ich davon noch nichts gesagt habe, wie sorgfältig man ihn von Zeit zu Zeit in seinem Christentum unterwiesen, so darf man darum nicht denken, als ob dieses verabsäumt worden wäre. Du kennst seinen vernünftigen Vater, das ist schon genug.</p>
                    <p rend="zenoPLm4n0" xml:id="tg11.3.21">Auf solche Weise ward der Grund zu derjenigen Gelehrsamkeit gelegt, welche Richard nunmehr besitzt. Nur dieses muß ich noch erinnern, daß man ihn erst im neunzehnten Jahre auf die hohe Schule that, ungeachtet er die Kräfte vielleicht eher gehabt hätte, den Degen zu tragen. –</p>
                    <p rend="zenoPLm4n0" xml:id="tg11.3.22">
                        <pb n="51" xml:id="tg11.3.22.1"/>
Das Beispiel dieses gelehrten Mannes überhebt mich aller Mühe, einige Regeln von der Unterweisung der Jugend in den ersten Jahren zu geben. Vielleicht zweifelst du aber, ob diese Art, die Jugend zu unterweisen, auch allgemein und bei andern ebenfalls mit Nutzen anzuwenden sei? Ich getraue mir, solches zu behaupten.</p>
                    <p rend="zenoPLm4n0" xml:id="tg11.3.23">Ist es wohl schwerer, die lateinische Sprache zu erlernen, als die französische oder die deutsche? Das kannst du nicht sagen. Wie alt bist du gewesen, als du deutsch reden konntest? und entsinnst du dich wohl, daß du schon im achten Jahre mit einer Französin zu plaudern vermögend warst? Der Umgang, eine fleißige Übung und der Mangel einer verwirrten Methode und ekelhafter Regeln brachten dich so zeitig zu die ser Geschicklichkeit. Eben das verlange ich bei der lateinischen Sprache. Wo findet man aber diejenigen, welche geschickt sind, die Jugend auf solche Weise zu unterweisen? Wie viele giebt es nicht, die zwar wissen, wie sie auf dem Katheder, aber nicht, wie sie in der Küche lateinisch reden sollen? Wir beide haben studiert, wir lassen uns beide Gelehrte nennen, und dennoch sollte es uns schwer fallen, die gemeinsten Handlungen der Menschen auszudrücken. Ich gebe dieses zu, mein werter Hermann; ich glaube aber, daß dein Einwurf die Wahrheit meiner Meinung nicht widerlegt, sondern nur noch mehr bekräftigt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg11.3.24">Wären wir, wären andere in ihrer Jugend besser angeleitet worden, so würde es uns und anderen an der Geschicklichkeit nicht fehlen, welche man allerdings bei wenigen antrifft. Unterdessen will ich dir doch verschiedene aufweisen, welche diese Geschicklichkeit wirklich besitzen – noch mehrere aber, welche gar wohl fähig wären, solche zu erlangen, wenn man nur ihre Bemühung durch billige Vergeltungen aufmunterte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg11.3.25">Die Schuld fällt allemal auf die Eltern zurück, welche die Art, ihre Kinder zu unterweisen, entweder selbst nicht verstehen oder aus Geiz die nötigen Kosten scheuen. Du kennst jenen Vater, welcher mehr auf seine Pferde wendet, als auf seinen Sohn. Er scheut keine Kosten, seinen Pudel recht abrichten zu lassen; wenn er aber dem Lehrmeister seines Sohnes ein Quartal bezahlen soll, so geschieht es niemals ohne innerlichen Widerwillen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg11.3.26">Bedächten wir nur, daß das Glück unserer Kinder, daß unsere eigene Ehre auf eine vernünftige Unterweisung derselben ankäme, so würden wir hierin eher verschwenderisch als karg sein, und ich weiß gewiß, es würden sich viele finden, welche <pb n="52" xml:id="tg11.3.26.1"/>
vermögend wären, alles dasjenige zu leisten, was ich von einem Lehrmeister gefordert habe.</p>
                    <p rend="zenoPLm4n0" xml:id="tg11.3.27">Bedächten wir aber auch, daß sich von unsern Kindern nur diejenigen den Studien widmen sollten, denen die Natur die Fähigkeit dazu verliehen hat, so würden wir sehen, daß es sehr leicht ist, die Jugend nach derjenigen Art zu unterweisen, welche mir die vernünftigste zu sein geschienen hat.</p>
                    <closer>
                        <lb xml:id="tg11.3.28"/>
                        <seg type="closer" rend="zenoPLm4n0" xml:id="tg11.3.29">
                            <seg rend="zenoTXFontsize80" xml:id="tg11.3.29.1">
                                <hi rend="spaced" xml:id="tg11.3.29.1.1">Anmerkung</hi>. Ein würdiges Seitenstück zur Erziehung des männlichen Geschlechts, wie Rabener sie hier und an anderem Orte schildert, ist die Darlegung, welche er über die <hi rend="spaced" xml:id="tg11.3.29.1.2">Erziehung</hi>
                                <hi rend="spaced" xml:id="tg11.3.29.1.3">der Töchter</hi> in der »Trauerrede eines Witwers auf den Tod seiner Frau« uns zum besten giebt:</seg>
                        </seg>
                        <seg type="closer" rend="zenoPLm4n0" xml:id="tg11.3.30">
                            <seg rend="zenoTXFontsize80" xml:id="tg11.3.30.1">Sie wissen, meine Herren, daß ich der Vater einer Tochter bin ... Meine Frau hat diese Tochter zur Welt gebracht und also alles verrichtet, was man von einer Mutter fordern kann. Der Wohlstand nötigte sie, eine Amme zu wählen, welche die Pflichten der Ernährung über sich nähme. Schon im zweiten Jahre zeigte das Kind zum un</seg>
                            <seg rend="zenoTXFontsize80" xml:id="tg11.3.30.2">aussprechlichen Vergnügen seiner wertesten Mama die deutlichsten Proben eines durchdringenden Verstandes, da es mit der größten Heftigkeit dasjenige verlangte, was ihm einfiel, und mit Händen und Füßen seinen Unwillen bezeugte, wenn jemand so unbedachtsam war und ihm widersprach ... Man war sehr sorgfältig, meine Tochter zu unterweisen. Das erste, was sie von ihrer Muttersprache lernte, war dieses: sie sei ein artiges Kind, und wenn sie fromm wäre, so sollte sie auch einen hübschen Mann bekommen ... Im 4. Jahre verstand sie die Wirkung des Spiegels; im 5. erlangte sie einen Geschmack von schönen Kleidern; im 6. war sie vermögend, über ihre Gespielinnen zu spotten; im siebenten saßte sie die Regeln des L'hombres und andern Zeitvertreibs; im 8. unterwies man sie in der Kunst, zärtlich zu blicken und artig zu seufzen; und nunmehr war meine Frau eben im Begriff, ihr eine kleine Kenntnis von dem beizubringen, was der gemeine Mann Christentum und Wirtschaft nennt, als eine unverhoffte Krankheit diese sorgfältige Mutter von ihrer hoffnungsvollen Tochter trennte.</seg>
                        </seg>
                    </closer>
                </div>
            </div>
        </body>
    </text>
</TEI>