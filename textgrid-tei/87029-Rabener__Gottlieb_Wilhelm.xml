<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg20" n="/Literatur/M/Rabener, Gottlieb Wilhelm/Satire/Sammlung satirischer Schriften/2. Satirische Briefe/Vorbericht">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Vorbericht</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Rabener, Gottlieb Wilhelm: Element 00025 [2011/07/11 at 20:31:42]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Rabeners Werke. Auswahl von August Holder. Ein Beitrag zur Kulturgeschichte und Pädagogik des achtzehnten Jahrhunderts, Halle a.d.S.: Otto Hendel, [1888].</title>
                        <author key="pnd:118743368">Rabener, Gottlieb Wilhelm</author>
                    </titleStmt>
                    <extent>98-</extent>
                    <publicationStmt>
                        <date when="1888"/>
                        <pubPlace>Halle a.d.S.</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1714" notAfter="1771"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg20.3">
                <div type="h4">
                    <head type="h4" xml:id="tg20.3.1">Vorbericht.</head>
                    <p xml:id="tg20.3.2">– – Es ist vielen unter unsern Deutschen sehr gewöhnlich, daß ihr Witz langsam und spät erwacht. Erwacht er aber einmal, so sind sie bis zum Ekel witzig. Der Beifall, den einige anakreontische Oden verdienten, machte das halbe Land anakreontisch. Man sang von Wein und Liebe, man tändelte mit Wein und Liebe, und die Leser gähnten bei Wein und Liebe. Ein Heldengedicht, dessen Vorzüge vielleicht erst in hundert Jahren den verdienten Beifall allgemein haben werden, macht zwei Dritteile des Volks episch. Aus allen Winkeln, wo ein Autor schwitzt, kriechen epische Hochzeitwünsche, epische Totenflüche, epische Wiegenlieder hervor, und der kleinste Geist flattert, so weit er kann, in die Höhe, um über geschwärzten Wolken hoch daherrauschend zu donnern. Mit den<hi rend="spaced" xml:id="tg20.3.2.1"> Briefen</hi> geht es uns ebenso, und wir sind in Gefahr, bei dieser Art des Witzes noch mehr auszustehen, je gewisser ein jeder glaubt, daß es sehr leicht sei, Briefe zu schreiben – und je leichter es ist, aus allem, was man geschrieben hat, einen Brief zu ma chen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg20.3.3">Mit Erlaubnis dieser meiner Herren Kollegen will ich hier die Kunst ihres Handwerks ein wenig verraten. Sie haben gelesen, daß man einen Brief so schreiben solle, wie man rede. Aber weiter haben sie nicht gelesen, sonst würden sie gefunden haben, daß man im stande sein müsse, <hi rend="spaced" xml:id="tg20.3.3.1">vernünftig</hi> zu reden und zu denken, wenn man es wagen wolle, vernünftige Briefe zu schreiben ... sie schreiben sehr viele, Briefe, weil ihnen der Mangel des Verstands den Vorteil verschafft, daß sie mit großer Geschwindigkeit <hi rend="spaced" xml:id="tg20.3.3.2">wenig denken und viel plaudern</hi>...</p>
                    <p rend="zenoPLm4n0" xml:id="tg20.3.4">
                        <pb n="98" xml:id="tg20.3.4.1"/>
Der steife und strotzende Witz, den uns die Ausländer so oft vorwerfen, äußert sich besonders bei denen, welche fühlen, daß sie <hi rend="spaced" xml:id="tg20.3.4.2">gelehrt und belesen</hi> sind, auf eine andere Art. Sie machen sehr tiefsinnige Abhandlungen von uralten Wahrheiten, jagen solche durch alle Gassen der Schulberedsamkeit, machen dieses gotische Gewebe mit Sentenzen der Alten erbaulich und mit schönen Sinnbildern anmutig – und wenn sie endlich unter Mühe und Angst sechs Bogen zusammengepredigt haben, so setzen sie darüber: »Hochedelgeborner Herr, Hochzuverehrender Herr und Gönner!« – Den Augenblick wird dieses gelehrte Werk ein <hi rend="spaced" xml:id="tg20.3.4.3">Brief</hi>!</p>
                    <p rend="zenoPLm4n0" xml:id="tg20.3.5">Das ist das große Geheimnis und der wahre Kunstgriff, dessen sich ein arbeitsamer Deutscher bedienen kann, wenn er ein gelehrter Briefsteller von vier Quartbänden werden will. Durch dieses vortreffliche Mittel getraue ich mir, aus allen Folianten meines Vaterlandes Briefe zu machen ...</p>
                    <p rend="zenoPLm4n0" xml:id="tg20.3.6">Nun weiß man, wie man <hi rend="spaced" xml:id="tg20.3.6.1">artig, vertraut und geschwind,</hi> man weiß auch, wie man <hi rend="spaced" xml:id="tg20.3.6.2">gelehrt</hi> schreiben solle. In diese Klassen werden sich die meisten Briefe einschränken lassen. Allenfalls nehme ich diejenigen aus, welche man <hi rend="spaced" xml:id="tg20.3.6.3">Amts- und Berufsbriefe</hi> nennen könnte, und welchen der Kanzleistil eigen ist. Die Gewohnheit rechtfertigt diese Schreibart und macht sie unentbehrlich. Wer diesen Kanzleistil zur Unzeit unterläßt, ist ebenso wohl ein lächerlicher Pedant, als derjenige, der ihn zur Unzeit braucht.</p>
                    <p rend="zenoPLm4n0" xml:id="tg20.3.7">Über die <hi rend="spaced" xml:id="tg20.3.7.1">Titulatur</hi> muß ich noch etwas bemerken. Es ist uns Deutschen nicht zuzumuten, daß wir unser gezwungenes und buntes Wortgepränge auf einmal verlassen sollten, mit dem wir die Eingänge unserer Briefe prächtig machen. Am wenigsten wollte ich, daß die witzigen Köpfe die ersten wären, diese Gewohnheit lächerlich, das »Mein Herr« oder »Madame« allgemein zu machen ... Diejenigen, welche durch die Gewohnheit ein Recht haben, weitläufige und prächtige Titel zu fordern, haben auch allein das Recht, sich davon loszusagen. Es wäre zu wünschen, daß sie es nach und nach thäten und dadurch unsre deutschen Ehrenbezeugungen biegsamer und natürlicher machten ... In <hi rend="spaced" xml:id="tg20.3.7.2">erdichten Briefen</hi> und bei unsern Freunden können wir das vertraute <hi rend="spaced" xml:id="tg20.3.7.3">Mein Herr</hi> ohne Gefahr brauchen, und wir thun wohl, wenn wir es in dergleichen Fällen allgemein machen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg20.3.8">Ich wollte wünschen, daß sich jemand die Mühe gäbe, eine <hi rend="spaced" xml:id="tg20.3.8.1">chronologische Geschichte der Komplimente und Titulaturen</hi> zu schreiben. Ich habe angemerkt, daß das <pb n="99" xml:id="tg20.3.8.2"/>
Lächerliche der Titulaturen in eben dem Grade gestiegen, in welchem der gute Gehalt der Münzen gefallen ist ...</p>
                    <p rend="zenoPLm4n0" xml:id="tg20.3.9">Da ich so viel Nachteiliges von den Briefen, von ihren Verfassern und von andern dabei vorfallenden äußerlichen Umständen sage, so werden meine Leser vermuten, daß ich mich dieses Augenblicks bediene, desto vorteilhafter von mir und meinen Briefen zu sprechen, um auch für mich das angemaßte Recht der Autoren zu behaupten, die gemeiniglich nicht eher zu ihrem Lobe schreiten, als wenn sie zehn andere Schriftsteller der Welt verdächtig gemacht haben. Ich werde es nicht thun. Ich <hi rend="spaced" xml:id="tg20.3.9.1">will mich und meine Sammlung dem Urteil der Leser überlassen</hi>, ohne zu flehen und ohne zu trotzen ... Der Beifall der Kenner macht mich stolz. Der Beifall derer, die nicht Kenner sind, macht mir ein Vergnügen ...</p>
                    <p rend="zenoPLm4n0" xml:id="tg20.3.10">Die <hi rend="spaced" xml:id="tg20.3.10.1">Einrichtung</hi> meiner satirischen Briefe ist ungefähr diese. Ich habe gewisse Bemerkungen von dem Lächerlichen oder Lasterhaften gemacht. Diese Bemerkungen habe ich durch Briefe erläutert. Um meinen Lesern durch die Abwechslung die Sache angenehm zu machen, habe ich hin und wider diesen Briefen die Gestalt einer zusammenhängenden Geschichte gegeben. Da sie alle nur erdichtet sind, so habe ich besonders in Ansehung der Titulaturen nicht nötig gehabt, sorgsam zu sein. Es ist meine Absicht nicht gewesen, meinen Lesern durch diese Sammlung Formulare in die Hände zu geben, die sie bei andern Gelegenheiten brauchen könnten.</p>
                    <p rend="zenoPLm4n0" xml:id="tg20.3.11">Ich wollte es wohl wünschen, daß man in der Welt schriebe, wie man dächte. Auf diesen Fall würde meine Sammlung ungemein praktisch sein, und ich würde vor andern Briefstellern unendliche Vorzüge erlangen. Weil man aber gemeiniglich anders schreibt, als man denkt, so will ich zufrieden sein, wenn man durch meine Bemühung und durch mein gegebenes Beispiel nur so viel lernt, wie man einen Brief verstehen soll, in welchem der Verfasser anders gedacht hat, als er schreibt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg20.3.12">Das <hi rend="spaced" xml:id="tg20.3.12.1">Verzeichnis</hi> wird über die ganze Einrichtung des Werks und meine Absichten nähere Auskunft geben.</p>
                    <p rend="zenoPLm4n0" xml:id="tg20.3.13">Von der Behutsamkeit, die ich gebraucht habe, auch in diesem Teile meiner Schriften weder den Wohlstand zu verletzen, noch jemand persönlich zu beleidigen, will ich weiter nichts sagen. Die gerechteste Sache wird verdächtig, wenn man sie zu oft und zu mühsam entschuldigt. Zugleich würde ich meine Leser beleidigen, wenn ich an ihrer Billigkeit und Einsicht bei jeder Gelegenheit zweifeln wollte. Das Einzige, <pb n="100" xml:id="tg20.3.13.1"/>
was ich hierbei thun kann, ist dieses, daß ich denen, welche mich und meine Schriften noch nicht kennen, das <hi rend="spaced" xml:id="tg20.3.13.2">Glauben</hi>s <hi rend="spaced" xml:id="tg20.3.13.3">bekenntnis</hi>
                        <hi rend="spaced" xml:id="tg20.3.13.4">meiner</hi>
                        <hi rend="spaced" xml:id="tg20.3.13.5"> Satire</hi> empfehle, welches ich in der Vorrede zum ersten Teile meiner Schriften abgelegt habe.</p>
                    <lb xml:id="tg20.3.14"/>
                    <p rend="zenoPLm4n0" xml:id="tg20.3.15">
                        <hi rend="spaced" xml:id="tg20.3.15.1">Leipziger Ostermesse</hi> 1752.</p>
                    <lb xml:id="tg20.3.16"/>
                    <closer>
                        <seg type="closer" rend="zenoPR" xml:id="tg20.3.17">G.W.R.</seg>
                    </closer>
                </div>
            </div>
        </body>
    </text>
</TEI>