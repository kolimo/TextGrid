<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg21" n="/Literatur/M/Rabener, Gottlieb Wilhelm/Satire/Sammlung satirischer Schriften/2. Satirische Briefe/Klagen wegen der Kinderzucht in vornehmen Häusern">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Klagen wegen der Kinderzucht in vornehmen Häusern</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Rabener, Gottlieb Wilhelm: Element 00026 [2011/07/11 at 20:31:42]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Rabeners Werke. Auswahl von August Holder. Ein Beitrag zur Kulturgeschichte und Pädagogik des achtzehnten Jahrhunderts, Halle a.d.S.: Otto Hendel, [1888].</title>
                        <author key="pnd:118743368">Rabener, Gottlieb Wilhelm</author>
                    </titleStmt>
                    <extent>101-</extent>
                    <publicationStmt>
                        <date when="1888"/>
                        <pubPlace>Halle a.d.S.</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1714" notAfter="1771"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg21.3">
                <div type="h4">
                    <head type="h4" xml:id="tg21.3.1">Klagen wegen der Kinderzucht in vornehmen Häusern</head>
                    <p xml:id="tg21.3.2">Die Klagen wegen der Kinderzucht sind so alt und so allgemein, daß ich nicht willens bin, mich gar zu lange dabei aufzuhalten. Diejenigen, welche Kinder haben, beschweren sich mit der größten Bitterkeit, daß es so viel Mühe koste, jemand zu finden, der den Willen und das Geschick habe, die Kinder redlich zu unterweisen und vernünftig anzuleiten. Ebenso unzufrieden und mißvergnügt sind auf der andern Seite diejenigen, welche sich unter dem Titel der Hofmeister und Informatoren der <hi rend="spaced" xml:id="tg21.3.2.1">Unterweisung</hi>
                        <hi rend="spaced" xml:id="tg21.3.2.2">der</hi>
                        <hi rend="spaced" xml:id="tg21.3.2.3"> Kinder</hi>
                        <hi rend="spaced" xml:id="tg21.3.2.4">in Familien</hi> unterziehen. Denn von dieser Art der Kinderzucht rede ich jetzt. Die Fehler der öffentlichen Schulen verdienten eine besondere Betrachtung. Ich glaube, man hat auf beiden Seiten Ursache, sich zu beschweren, und gemeiniglich sind beide schuld daran.</p>
                    <p rend="zenoPLm4n0" xml:id="tg21.3.3">Eltern, welche die Pflichten der Eltern nicht verstehen (und wie viele verstehen sie nicht!) – Eltern, welche in ihrer Jugend selbst keine Erziehung gehabt und nicht verlangen, daß ihre Kinder vernünftiger werden, als <hi rend="spaced" xml:id="tg21.3.3.1">sie</hi> sind, die vielmehr nur darauf sehen, daß sie mit einer sorgfältigen Ersparung alles Aufwands dieselben heranziehen mögen: solche Eltern verdienen das Glück kaum, einen geschickten Mann in ihr Haus zu bekommen, welcher es getreuer und redlicher mit ihnen meint, als sie es selbst mit ihnen meinen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg21.3.4">Kinder, und besonders Kinder vornehmer Eltern zu ziehen, ist die wichtigste, aber auch die schwerste Arbeit, die man sich vorstellen kann. Wird sich wohl ein Mann, der Gelehrsamkeit, Geschmack und gute Sitten besitzt, so leicht entschließen können, ein Amt über sich zu nehmen, bei dem so wenig Vorteil und oft noch weniger Ehre, allemal aber viel Verdruß und Arbeit ist?</p>
                    <p rend="zenoPLm4n0" xml:id="tg21.3.5">Ein Vater, welcher niemals gewohnt ist, vernünftig zu denken, ist auch nicht im stande, sich vernünftige Vorstellungen von der Verbindlichkeit zu machen, die er einem Manne schuldig ist, der das schwere Amt der Erziehung mit ihm teilt. Er sieht diesen Mann als einen seiner Bedienten, und wenn <pb n="101" xml:id="tg21.3.5.1"/>
er recht artig denkt, als den Vornehmsten seiner Bedienten an. Er wird ihm nicht mehr Achtung erweisen, als er einem seiner Bedienten erweist. Und kann er alsdann wohl verlangen, daß seine Kinder diesen ihren Hofmeister mehr ehren sollen? Wie viel unglückliche Folgen fließen aus dieser einzigen Quelle, wenn die Kinder sich durch das Beispiel der Eltern berechtigt halten, denjenigen zu verachten, der ihr Führer und Lehrer sein soll!</p>
                    <p rend="zenoPLm4n0" xml:id="tg21.3.6">Die Besoldung oder (wie es in vielen vornehmen Häusern genannt wird) der Lohn, den man dem Hofmeister giebt, ist so kümmerlich und gering, daß ein rechtschaffener Mann unmöglich Mut genug behalten kann, sein sklavisches Amt mit dem Eifer und der Munterkeit zu verwalten, die bei dieser Verrichtung so nötig sind.</p>
                    <p rend="zenoPLm4n0" xml:id="tg21.3.7">Und damit der Hofmeister sein Geld ja nicht mit Müßiggehen verdiene, so sind viele so sinnreich, daß sie von ihm alle Wissenschaften und alle mögliche Handdienste fordern und es gerne sähen, wenn er Hofmeister und Perückenmacher und Hausvogt und Kornschreiber zugleich wäre.</p>
                    <p rend="zenoPLm4n0" xml:id="tg21.3.8">Können dergleichen unbillige Eltern sich es wohl befremden lassen, wenn ihre Kinder schlecht und niederträchtig erzogen werden, da sie mit demjenigen, der sie erziehen soll, so, niederträchtig und eigennützig verfahren?</p>
                    <p rend="zenoPLm4n0" xml:id="tg21.3.9">Ich weiß, daß ich alle diejenigen auf meiner Seite habe, denen in adeligen Häusern etc. die Erziehung und Unterweisung der Jugend anvertraut ist. Sie werden so billig sein und mir in dem auch Beifall geben, was ich jetzt anführen will.</p>
                    <p rend="zenoPLm4n0" xml:id="tg21.3.10">Sie geben den Eltern ebenso oft und noch öfter Gelegenheit, unzufrieden mit ihnen zu sein. Viele sind verwegen genug, dieses Amt auf sich zu nehmen, welche bei ihrer tiefen Unwissenheit eine so schlechte Aufführung haben, daß sie selbst noch verdienten, unter der Hand eines Zuchtmeisters zu stehen. Die Sorgfalt, welche man wegen des äußerlichen Wohlstandes auch in den kleinsten Umständen beobachten muß, ist ihnen auf niedern und hohen Schulen so gleichgültig gewesen, daß sie es für brav gehalten haben, ungezogen zu sein. Nun kommen sie in ein Haus, wo rechtschaffene Eltern ebenso sorgfältig verlangen, daß ihre Kinder wohl gesittet erzogen, als daß sie in den Wissenschaften unterrichtet werden mögen. Wie empfindlich muß es ihnen sein, wenn sie diesem sich selbst gelassenen Hofmeister ihre <hi rend="spaced" xml:id="tg21.3.10.1">Kinder</hi> zur Aufsicht anvertrauen sollen, welche gar leicht das Unanständige an ihrem Lehrer wahrnehmen müssen, da sie dergleichen weder bei ihren Eltern <pb n="102" xml:id="tg21.3.10.2"/>
noch bei ihren Bedienten zu sehen gewohnt sind. Die Bedienten selbst finden ihn lächerlich, und er wird es endlich dem ganzen Hause, da er sich so wenig Mühe giebt, seine Fehler zu verbergen oder sich zu ändern. Und dennoch wird eben dieser ungesittete Mensch die bittersten Klagen führen, daß man ihn in diesem Hause nicht mit der Achtung und Ehrerbietung begegne, die er im Namen seines Amtes fordert.</p>
                    <p rend="zenoPLm4n0" xml:id="tg21.3.11">Es ist ein Unglück, daß gemeiniglich nur diejenigen sich dieser Lebensart widmen, welchen die Armut ihrer Eltern die Hoffnung benimmt, ihre Absichten auf etwas Höheres als die Erlangung einer Dorfpfarre zu richten. Es geschieht alsdann gar zu leicht, daß ihre Aufführung entweder zu schüchtern und kleinmütig ist, denn sie sind gewohnt, einsam und im Dunkeln zu leben; oder sie sind zu trotzig und zu stolz, weil sie zu wenig Gelegenheit gehabt haben, sich und die Welt kennen zu lernen. Beides ist ihnen bei der Unterweisung der Jugend nachteilig. Kommt endlich dieses noch dazu, daß ihre Absichten allzu eigennützig sind, daß sie die Beförderung in ein Amt je eher je lieber zu erlangen wünschen, so wird ihnen die übernommene Arbeit desto verdrießlicher und die geringste Verzögerung ihrer Hoffnung unerträglich fallen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg21.3.12">Aber darum getraue ich mir noch nicht zu behaupten, daß ein Mensch deswegen, weil er <hi rend="spaced" xml:id="tg21.3.12.1">nicht</hi> von niedriger Geburt ist, ... stets geschickt sei, die Jugend zu unterrichten und vernünftig zu erziehen. Nein ... die Erfahrung würde mir widersprechen. Man bemerkt es nur gar zu oft, daß diejenigen am meisten ungesittet sind, welche die beste Gelegenheit gehabt haben, wohl erzogen zu werden.</p>
                    <p rend="zenoPLm4n0" xml:id="tg21.3.13">Ich kann mir kein lebhafteres Vergnügen vorstellen, als wenn vernünftige Eltern, die keine Mühe und Kosten sparen, ihren Kindern eine anständige Erziehung zu verschaffen, einen Mann finden, der bei einer gesitteten Aufführung ein redliches Herz und die Geschicklichkeit besitzt, seinem Amte vollkommen vorzustehen: wenn sie die Früchte seiner redlichen Bemühung von Zeit zu Zeit wahrnehmen, und wenn sie alsdann eine Gelegenheit erlangen, das Glück dieses rechtschaffenen Mannes auf eine vollkommene Art zu befestigen ...</p>
                    <p rend="zenoPLm4n0" xml:id="tg21.3.14">»Die Erzieher der Jugend sollen entweder Kenntnisse besitzen, oder wenn diese ihnen fehlen, so sollen sie ihren Mangel an Gelehrsamkeit einsehen. Es giebt nichts Unerträglicheres, als wenn Leute, die kaum über die Anfangsgründe hinausgekommen sind, sich den Anstrich von Gelehrsamkeit geben. Denn sie geben teils den in der Erziehungskunst Erfahrenen<pb n="103" xml:id="tg21.3.14.1"/>
 nicht gerne nach, teils zeigen sie ihre Thorheit durch Machtsprüche, auf die sich diese Art von Leuten etwas zugute thut, ja zuweilen sogar durch Wut. Auch hat ihr Fehler den nachteiligsten Einfluß auf die Sittlichkeit.«</p>
                    <milestone unit="hi_start"/>
                    <p rend="zenoPR" xml:id="tg21.3.15">
                        <hi rend="spaced" xml:id="tg21.3.15.1">Quintilian</hi>.</p>
                    <milestone unit="hi_end"/>
                    <lb xml:id="tg21.3.16"/>
                    <p rend="zenoPLm4n0" xml:id="tg21.3.17">Ich will ein paar Briefe einrücken, welche dasjenige näher beweisen werden, was ich hier vielleicht ein wenig zu ernsthaft voraus erinnert habe.</p>
                    <lb xml:id="tg21.3.18"/>
                    <sp>
                        <speaker rend="zenoPC" xml:id="tg21.3.19">*</speaker>
                        <lb xml:id="tg21.3.20"/>
                        <lg>
                            <l rend="zenoPLm8n8" xml:id="tg21.3.21">Hochzuverehrender Herr Professor!</l>
                        </lg>
                        <lb xml:id="tg21.3.22"/>
                    </sp>
                    <p rend="zenoPLm4n0" xml:id="tg21.3.23">Meine Jungen wachsen heran, und es ist nun Zeit, daß ich ihnen einen gescheiten Hofmeister halte. Bisher habe ich den Schulmeister zu ihnen gehen lassen, aber er kann sie nicht mehr bändigen. Ich weiß, in welchem Ansehen Sie in Leipzig stehen, und daß Ihr Vorzimmer beständig von solchen krummgebückten Kreaturen voll ist, welche Hofmeisterstellen oder Informationen suchen. Lesen Sie mir einen hübschen, gesunden Kerl aus. Sie wissen es selbst, daß bei mir weder Menschen noch Vieh notleiden. Fritz, der älteste, ist ein durchtriebener Schelm. Er hat einen offenen Kopf und ist auf die Mägde, wie ein kleiner Teufel. Ich darf es den Buben nicht merken lassen, daß ich ihn lieb habe – der leichtfertige Schelm! Er ist noch nicht 14 Jahre alt und hat in <hi rend="italic" xml:id="tg21.3.23.1">humanioris</hi> gar seine <hi rend="italic" xml:id="tg21.3.23.2">principio.</hi> Ferdinand ist meiner Frau ihr Junge. Er ist immer kränklich, und das geringste Ärgernis kann ihm schaden. Das gute Kind will mit lauter Liebe gezogen sein, und meine Frau hat schon zwei Bediente weggejagt, die ihm unfreundlich begegnet haben. Das älteste Mädchen ist 12 Jahre. Sie soll noch ein bißchen Katechissen lernen und hernach will ich dem kleinen Nickel einen Mann geben, der mag sehen, wie er mit ihr zurechte kommt. Mit dem kleinen Mädchen hat der Hofmeister gar nichts zu thun, die, behält die Mamsell bei sich. Sehen Sie nun, Herr Professor, das ist die Arbeit alle. Ich werde Ihnen sehr verbunden sein, wenn Sie mir einen hübschen Menschen vorschlagen. Ich verlange weiter nichts von ihm, als daß er gut Latein versteht, sich in Wäsche und Kleidung reinlich und sauber hält, Französisch und Italienisch sprechen kann, eine schöne Hand schreibt, die Mathematik versteht, Verse macht, so viel man fürs Haus braucht, tanzen, fechten und reiten kann und womöglich ein wenig zeichnet. In der Historie muß er auch gut beschlagen sein, vor allen Dingen aber in der Wappenkunst. Ist er schon auf Reisen gewesen, desto besser. <pb n="104" xml:id="tg21.3.23.3"/>
Aber er muß sich's gefallen lassen, bei mir auf meinem Gute zu bleiben und sich wenigstens auf 6 Jahre bei mir zu vermieten. Dafür soll er bei meinen Kindern auf der Stube freie Wohnung haben, mit dem Kammerdiener essen und jährlich 50 Gulden bekommen. Zum heiligen Christ und zur Messe gebe ich nichts, dergleichen Betteleien kann ich nicht leiden. Sind die 6 Jahre um, so kann er in Gottes Namen hingehen, wo er will. Ich will ihn sodann an seinem Glück nicht hindern. Mich dünkt, die Vorschläge sind ganz billig. Hat der Mensch Lust zur Wirtschaft, so kann er meinem Verwalter mit an die Hand gehen. Es wird sein Schade nicht sein, denn er weiß doch nicht, wozu er's einmal brauchen kann. Ich werde für Ihre Bemühung erkenntlich sein und bin,</p>
                    <lb xml:id="tg21.3.24"/>
                    <p rend="zenoPLm4n0" xml:id="tg21.3.25">Hochzuverehrender Herr Professor,</p>
                    <lb xml:id="tg21.3.26"/>
                    <sp>
                        <speaker rend="zenoPC" xml:id="tg21.3.27">Ihr</speaker>
                        <lg>
                            <l rend="zenoPR" xml:id="tg21.3.28">dienstwilliger</l>
                            <l rend="zenoPR" xml:id="tg21.3.29">– –</l>
                        </lg>
                    </sp>
                </div>
            </div>
        </body>
    </text>
</TEI>