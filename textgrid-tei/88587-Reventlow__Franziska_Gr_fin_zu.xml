<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg74" n="/Literatur/M/Reventlow, Franziska Gräfin zu/Erzählungen/Eine Uniform">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Eine Uniform</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Reventlow, Franziska Gräfin zu: Element 00013 [2011/07/11 at 20:29:37]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <notesStmt>
                <note>
                              Erstdruck in: Husumer Nachrichten, 7. Januar 1893.
                           </note>
            </notesStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Franziska Gräfin zu Reventlow: Autobiographisches. Ellen Olestjerne. Novellen, Schriften, Selbstzeugnisse. Herausgegeben von Else Reventlow. Mit einem Nachwort von Wolfdietrich Rasch, München: Langen Müller, 1980.</title>
                        <author>Reventlow, Franziska Gräfin zu</author>
                    </titleStmt>
                    <extent>277-</extent>
                    <publicationStmt>
                        <date when="1980"/>
                        <pubPlace>München</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1871" notAfter="1918"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg74.3">
                <div type="h3">
                    <head type="h3" xml:id="tg74.3.1">Franziska Gräfin zu Reventlow</head>
                    <head type="h2" xml:id="tg74.3.2">Eine Uniform</head>
                    <p xml:id="tg74.3.3">
                        <hi rend="italic" xml:id="tg74.3.3.1">Lawntennis </hi>– auf dem grünen, schattenlosen Platz, abwärts von den hohen alten Bäumen, die in tiefem Schatten daliegen, mit dem weiten Blick auf Kornfelder und dahinter die blaue Ostsee. Heiß flimmert, flirrt und leuchtet die Sonne vom Sommerhimmel herunter, es ist nachmittags um drei, um die müde, heiße Stunde. Aber davon wissen die jungen Leute nichts, die hier <hi rend="italic" xml:id="tg74.3.3.2">Lawntennis</hi> spielen, und die Alten sitzen drüben unter der Buche und sehen nur zu, dem Einnicken nahe.</p>
                    <p rend="zenoPLm4n0" xml:id="tg74.3.4">Auf dem Tennisplatz wird eine heiße Schlacht geschlagen, die Bälle fliegen durcheinander, kreuz und quer, und die jugendlichen Gestalten biegen, bücken und recken sich fast wie im Zirkus, um sie in Bewegung zu halten. Alle Gesichter glühen, hier und da fliegt wohl ein kurzes Lachen, eine flüchtige Scherzrede hin und her, sonst ist alles ganz in den Eifer des Spielens vertieft.</p>
                    <p rend="zenoPLm4n0" xml:id="tg74.3.5">
                        <pb n="277" xml:id="tg74.3.5.1"/>
Ein Gang ist zu Ende, der Schauplatz wird ein anderer, neue Mitspieler treten ein, während die vorigen, zur Seite stehend, mit gespannter Aufmerksamkeit den Fortgang beobachten oder sich den kühlen Räumen des Schlosses zuwenden, um auszuruhen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg74.3.6">Ein junges Mädchen mit schwerem Blondhaar ging langsam und müde die breite, teppichbelegte Treppe hinauf. Das ganze Haus lag so still, sie waren alle draußen im Sonnenschein. Hier drinnen waren alle Läden geschlossen, daß kaum ein Strahl durchdringen konnte, alles schien zu schlafen. Die Tür zum Billardsaal war angelehnt, sie öffnete dieselbe leise und trat hinein, als sie den Raum leer fand. Auch hier waren die Rouleaux niedergelassen, die Staffeleien und Bücher standen umher, als ob sie sich wunderten, daß heute niemand sie anrührte, der Billardtisch sah so gelangweilt aus, und die weißen Kugeln lagen wie verirrt auf dem dunkelgrünen Tuch.</p>
                    <p rend="zenoPLm4n0" xml:id="tg74.3.7">Da auf dem Sofa lag eine Uniform, und das blonde Mädchen wußte, wem sie gehörte, es war seine Uniform, die er für das Spiel am heißen Nachmittag mit der Tropenjacke vertauscht hatte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg74.3.8">Unten wurde das Tamtam geschlagen, um alle zum Diner zusammenzurufen. Die dumpfen Schläge dröhnten bis in den Saal hinauf und in die Ohren des jungen Weibes, das vor dem Sofa auf den Knien lag, den schmerzenden Kopf in das dunkle, kühle Zeug der Uniform hineingewühlt, liebesschwere traurige Küsse auf dasselbe drückend, während ihr schwere, angstgepreßte Tränen aus den Augen rannen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg74.3.9">Und er wußte nichts davon.</p>
                    <pb n="278" xml:id="tg74.3.10"/>
                </div>
            </div>
        </body>
    </text>
</TEI>