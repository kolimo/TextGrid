<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg77" n="/Literatur/M/Reventlow, Franziska Gräfin zu/Erzählungen/Moment-Aufnahmen/Nachtarbeit">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Nachtarbeit</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Reventlow, Franziska Gräfin zu: Element 00017 [2011/07/11 at 20:29:37]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Franziska Gräfin zu Reventlow: Autobiographisches. Ellen Olestjerne. Novellen, Schriften, Selbstzeugnisse. Herausgegeben von Else Reventlow. Mit einem Nachwort von Wolfdietrich Rasch, München: Langen Müller, 1980.</title>
                        <author>Reventlow, Franziska Gräfin zu</author>
                    </titleStmt>
                    <extent>281-</extent>
                    <publicationStmt>
                        <date when="1980"/>
                        <pubPlace>München</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1871" notAfter="1918"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg77.2">
                <div type="h4">
                    <head type="h4" xml:id="tg77.2.1">Nachtarbeit</head>
                    <p xml:id="tg77.2.2">Unten an der Isar ging ich entlang, wo Tag und Nacht an den Kanalisationswerken gearbeitet wird.</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.2.3">Tag und Nacht.</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.2.4">In der Mitte der Straße eine tiefe, lang sich hinziehende Grube, unten tief die Arbeiter, die unermüdlich die Erde emporschaufeln. Man hört nur das Klirren der Spaten und das Hinabrollen der aufgeworfenen Steine.</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.2.5">Gegen Abend haben die Männer da unten noch bei der Arbeit gesungen, jetzt sind sie längst zu müde, aber die Arbeit geht immer weiter. Durch die scharfe Nachtluft rieselt empfindlicher Frostschnee auf alles herab, der beißt auf der Haut und dringt schneidend in die Kleidung ein.</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.2.6">Hier und da hängt eine Laterne mit unruhig flackerndem Licht an einem der hervorstehenden Balken.</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.2.7">Durch die Nacht klingt das Rauschen der Isar und das Ächzen der Dampfmaschine.</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.2.8">Schwarz, blank, kolossal steht sie da. Der mächtige Schlot atmet Rauchwolken aus, durch welche einzelne Funken blitzen und wie Sternschnuppen verschwinden. Hinter der Maschine steht der Heizer. Seine Gestalt ist in schwarzer Silhouette gegen die helle Wand der die Maschine umgebenden Bretterbude abgeschnitten.</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.2.9">Dann und wann fährt er sich mit der Hand über die müden, von Rauch und Hitze brennenden Augen. Nun reißt er die Ofentür auf, flackernder roter Feuerschein fährt über sein Gesicht. Dann rasselt die Schaufel durch die Kohlen und füllt den aufgerissenen Schlund mit neuer Nahrung.</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.2.10">
                        <pb n="281" xml:id="tg77.2.10.1"/>
Auf einer Bank im Bretterverschlag sitzt ein zweiter Mann, den Kopf herabgesunken. Er scheint zu schlafen. Der andere steht nach vollbrachter Heizarbeit wieder unbeweglich auf seinem Platz. Nur zuweilen fährt er sich über die Augen, während die Nacht mit unerbittlicher Langsamkeit vorrückt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.2.11">Über die Brücke hört man Studenten singen mit rohen berauschten Stimmen. Liebespaare drücken sich am Quai entlang.</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.2.12">Und drüben auf der anderen Seite, wo die neuerbauten hohen Häuser stehen, kommen die Theaterbesucher nach Hause, in Pelzen und hellen Abendmänteln. Einige von ihnen gähnen und reiben sich die Augen. Es war doch recht anstrengend, so lange dazusitzen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.2.13">Ein junger Mann und eine Dame unterhalten sich über Sozialismus und über die letzten großen Strikes.</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.2.14">»Sehen Sie, Fräulein, ein interessantes Motiv.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg77.2.15">Der müde Mann an der Maschine fährt sich über die Augen und schüttelt sich zwischen Nachtfrost und Kohlenhitze.</p>
                </div>
            </div>
        </body>
    </text>
</TEI>