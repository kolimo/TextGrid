<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg80" n="/Literatur/M/Reventlow, Franziska Gräfin zu/Erzählungen/Wahnsinn">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Wahnsinn</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Reventlow, Franziska Gräfin zu: Element 00020 [2011/07/11 at 20:29:37]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <notesStmt>
                <note>
                              Erstdruck in: Simplicissimus, München (Albert Langen) 1. Jg., Nr. 7, 16. Mai 1896.
                           </note>
            </notesStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Franziska Gräfin zu Reventlow: Autobiographisches. Ellen Olestjerne. Novellen, Schriften, Selbstzeugnisse. Herausgegeben von Else Reventlow. Mit einem Nachwort von Wolfdietrich Rasch, München: Langen Müller, 1980.</title>
                        <author>Reventlow, Franziska Gräfin zu</author>
                    </titleStmt>
                    <extent>286-</extent>
                    <publicationStmt>
                        <date when="1980"/>
                        <pubPlace>München</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1871" notAfter="1918"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg80.3">
                <div type="h3">
                    <head type="h3" xml:id="tg80.3.1">
                        <pb n="286" xml:id="tg80.3.1.1"/>
                        <pb type="start" n="322" xml:id="tg80.3.1.2"/>Franziska Gräfin zu Reventlow</head>
                    <head type="h2" xml:id="tg80.3.2">Wahnsinn</head>
                    <p xml:id="tg80.3.3">Geerdt Sievers war Bildhauer in München. Seine Heimat war an der Ostsee unter den dänischen Buchen. Er hatte eben sein Modell weggeschickt, weil die Dämmerung kam, und nun stand er vor dem Werk seines Tages. Es war ein lebensgroßer Akt – ein altes Motiv: Eva, das Weib.</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.4">Er wollte etwas ganz Neues, noch nicht Dagewesenes <pb n="322" xml:id="tg80.3.4.1"/>
schaffen, und eine seltsame Idee hatte sich aus diesem Wollen herausgeboren: das Weib vor dem Sündenfall mit vollen, noch unschuldigen Formen, die verlangend der Erkenntnis entgegenschwellen – das Vorspiel der Sünde in dem jugendlich reifenden Körper. Eva kniet und spielt mit der Schlange, die sich vor ihr im Grase ringelt. Der Gesichtsausdruck zeigt noch ahnungslose Neugier; über dem in geschwungener Linie vornüber gebeugten Rücken hängt ein Zweig mit den Äpfeln vom Baume der Erkenntnis. Sie hat die Frucht noch nicht gesehen; die Schlange hält sie für ein Spielzeug – aber der Augenblick ist nah, er muß bald kommen, der Augenblick, wo die Schlange zu sprechen beginnen wird, und wo sie die Frucht gewahren wird.</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.5">Sie kennt die Sünde noch nicht, aber sie wird erkennen und sie wird sündigen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.6">Lange hatte er nach dem geeigneten Modell gesucht und er hatte endlich gefunden was er suchte, ein noch sehr junges und unverdorbenes Mädchen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.7">Und nun war er an dem Kind zum Sünder geworden.</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.8">Es wurde dunkel. Der Künstler saß auf einer Ecke des Diwans und starrte auf seine Arbeit hin.</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.9">Es war nicht das geworden, was er gewollt hatte. Gerade das Gegenteil: in der Haltung seiner Eva lag etwas Gedrücktes und Schuldbewußtes. Aber gerade so hatte sie da vor ihm auf dem Podium gekniet, durch seine Schuld. Und auf dem Diwan da war sie gelegen, damals, als sie die Sünde erkannt hatte. Es war nichts, er mußte wieder ein anderes Modell suchen. Aber wo war zum zweitenmal ein solcher Körper, solche Jugend? – Und wenn auch, würde es nicht wieder dasselbe Ende sein? –</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.10">Er fühlte wohl: er ging nicht auf in seiner Kunst – wenn er auch danach lechzte in völliger Raserei und sich verzweifelnd mühte, sich ihr hinzugeben mit seinem ganzen <pb n="323" xml:id="tg80.3.10.1"/>
Sein. Er war nicht fähig dazu. Es war eine traurige Impotenz in ihm, der er unterlag.</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.11">Nun war es wieder so gekommen. Er hatte eine Idee gehabt, die ihn ganz erfüllte, und wie er sie fassen und wie er sie gestalten wollte, zerging sie vor dem brutalen Zugreifen seiner Hände. Seine Nerven zitterten, als er das Modell da vor sich knien sah in seiner jungen Schönheit und er arbeitete fanatisch. Aber dann sah er nur noch das Weib und wie er den Ton unter den Händen fühlte, war ihm, als sei es ihr Leib, der ihm verlangend entgegenbrannte, – und der Taumel kam – und es war wieder alles hin, seine Arbeit und ihre Unschuld.</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.12">So kam es immer. Die Gedanken, die so brennend seinen Kopf durchwühlten, konnte er nicht zu realer Gestaltung bringen, weil die Wirklichkeit allzu brutal zerstörend über ihn kam. Er hatte noch nie etwas Großes geschaffen und er würde es nie können, das wußte er. Hundertmal stellte er sich wieder vor die Feuerprobe und jedesmal unterlag er. Er trat vor seine Arbeit hin und riß den Ton herunter, bis nur noch das Gerüst wie ein einsam drohendes Gerippe seine Arme in die leere Luft streckte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.13">Dann ging er. –</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.14">Geerdt Sievers wurde irrsinnig.</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.15">Er hatte sich überarbeitet und dazu kam das tolle Leben; die Weiber und das alles. Eines Tages brach die Tobsucht bei ihm aus und er wurde in eine Heilanstalt geschafft.</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.16">Nach einem Jahr wurde er als geheilt entlassen und kam an einem Herbsttage wieder nach München zurück.</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.17">Nun wollte er wieder arbeiten, versuchen zu arbeiten – wenn in ihm noch etwas geblieben war.</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.18">Er suchte das Mädchen auf, das ihm zu seiner Eva Modell gestanden, es war herabgekommen und schlecht geworden wie die anderen. Das hatte ihn aufgeregt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.19">Um sich zu beruhigen, ging er ins Freie.</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.20">
                        <pb n="324" xml:id="tg80.3.20.1"/>
Es war zehn Uhr abends und der Mond schien hell in dieser Septembernacht.</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.21">Der Englische Garten lag in zauberhaftem Nebel da.</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.22">Das Auf- und Niederwogen der weißen Dunstgebilde verwirrte den Kopf des krank Gewesenen. Er fühlte, daß er doch noch recht schwach war.</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.23">Bald verließ er den Park und ging zur Isar hinab, über die Luitpoldbrücke, über den Platz, am Springbrunnen vorbei und auf die Terrasse hinauf. Er wollte heute seine Nerven auf die Probe stellen, und versuchte das Bild ruhig in sich aufzufassen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.24">Geradeaus eine Perspektive von tanzenden Lichtern, beginnend mit den Kandelabern der Luitpoldbrücke und dann sich in die Königsstraße hinein verlierend. Links, rechts, dunkle Häusermassen, Türme, Lichter.</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.25">Auf der Steinumrandung des Brunnens vor der Terrasse lag kalter Mondschein.</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.26">Mitten aus dem dunklen Wasser stieg eine weiße Springsäule auf und bewegte sich hin und her, wie eine Frauengestalt in langen Gewändern.</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.27">Geerdt schloß einen Augenblick lang die Augen und sah dann wieder hin. Die weiße Gestalt kam aus dem schwarzen Brunnen, dehnte sich empor, schüttelte das weiß sprühende Wasser von sich ab und sank zusammen – stieg wieder in die Höhe – schüttelte sich – sank zusammen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.28">Er starrte hin, mußte hinsehen. Es war als ob das Weib da unten sich mühte, Gestalt zu gewinnen. Es kam empor, warf die Wasserfunken nach allen Seiten von sich, und wenn es dann in seiner Schönheit emporsteigen wollte, floß es wieder in sich zusammen – still und lautlos – still – und lautlos-?</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.29">Nein, da schrie sie – laut und gellend.</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.30">Er schlug sich vor die Stirn. Er merkte plötzlich, daß er <pb n="325" xml:id="tg80.3.30.1"/>
selbst laut aufschrie, jedesmal, wenn die weiße Gestalt wieder zusammenfiel.</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.31">Dann kam es, als ob in seinem Schädel sich etwas wie ein Rad mit rasender Geschwindigkeit drehte. Das Weib reckte sich wieder in die Höhe und zerrann wieder in die Tiefe hinein.</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.32">Und es schrie wieder, laut und gellend.</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.33">Er wandte sich um. Der Vollmond stand dicht über ihm und er griff nach der glänzenden Kugel, sie war ja ganz nahe an seinem Kopf. Er griff danach, da bekam sie Gesichtszüge und grinste ihn an und stand dann auf einmal hoch am Himmel und war wieder der Mond.</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.34">Der Schrecken faßte ihn furchtbar an. Er stürzte in die Anlagen hinein.</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.35">Er rannte gegen verschiedene Liebespaare an, die in den dunklen Wegen gingen!</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.36">Er kam wieder aus den Anlagen heraus und rannte über die Maximiliansbrücke.</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.37">Lief etwas hinter ihm her?</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.38">Er fuhr herum und sah nach rückwärts. –</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.39">Schwarz hob sich das Maximilianeum gegen den Himmel ab. Durch einen der Galeriebogen lachte ihn wieder die grinsende Mondfratze an.</p>
                    <p rend="zenoPLm4n0" xml:id="tg80.3.40">Geerdt Sievers war wieder wahnsinnig geworden und diesmal unheilbar.</p>
                    <pb n="326" xml:id="tg80.3.41"/>
                </div>
            </div>
        </body>
    </text>
</TEI>