<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg85" n="/Literatur/M/Reventlow, Franziska Gräfin zu/Erzählungen/Das Jüngste Gericht">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Das Jüngste Gericht</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Reventlow, Franziska Gräfin zu: Element 00025 [2011/07/11 at 20:29:37]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <notesStmt>
                <note>
                              Erstdruck in: Simplicissimus, München (Albert Langen) 1. Jg., Nr. 41, 9. Januar 1897.
                           </note>
            </notesStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Franziska Gräfin zu Reventlow: Autobiographisches. Ellen Olestjerne. Novellen, Schriften, Selbstzeugnisse. Herausgegeben von Else Reventlow. Mit einem Nachwort von Wolfdietrich Rasch, München: Langen Müller, 1980.</title>
                        <author>Reventlow, Franziska Gräfin zu</author>
                    </titleStmt>
                    <extent>428-</extent>
                    <publicationStmt>
                        <date when="1980"/>
                        <pubPlace>München</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1871" notAfter="1918"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg85.3">
                <div type="h3">
                    <head type="h3" xml:id="tg85.3.1">Franziska Gräfin zu Reventlow</head>
                    <head type="h2" xml:id="tg85.3.2">Das Jüngste Gericht</head>
                    <p xml:id="tg85.3.3">Es war am Vorabend des Jüngsten Gerichts.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.4">Petrus und der liebe Gott pflogen Rat miteinander. Sie waren in einiger Verlegenheit, wie die Sache gehen sollte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.5">»Petrus, wie soll es nur werden?« seufzte der liebe Gott. »Wir sind gar zu sehr aus der Zeitanschauung herausgekommen. <pb n="428" xml:id="tg85.3.5.1"/>
Und da kommen nun alle diese modernen Menschen und sind eine ganz andere Art von Rechtsprechung gewöhnt und wir –«</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.6">Petrus hatte erst schweigend zugehört, dann seufzte er: »Gottvater, wir sollten einen Staatsanwalt zum Beistand haben. Siehst du, die Leutchen auf Erden können sich keine Gerichtsverhandlung ohne Staatsanwalt vorstellen. Und sie haben recht. Wer soll denn anklagen? Für dich schickt sich das nicht. Der oberste Richter kann doch nicht die Anklage führen. – Das Prinzip der Milde und Gerechtigkeit würde darunter leiden. Und ich? Man weiß ja, wie die Leute sind. Sie könnten mich für parteiisch oder am Ende gar für bestechlich halten. Und dann träfe es sich gerade gut. Gerade gestern haben wir einen Staatsanwalt in die Juristenabteilung des Fegefeuers bekommen. – Die Herren sollen ja heutzutage Kolossales in der Rechtsprechung leisten.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.7">Aber der liebe Gott wollte nicht recht dran. Er meinte, es müsse auch so gehen. Die Zuziehung eines Staatsanwalts sei ja doch im göttlichen Weltplan von Anbeginn nicht vorgesehen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.8">Sankt Petrus wagte nicht weiter zu widersprechen. Die Sache hatte ja auch immerhin ihre Bedenken.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.9">Der Jüngste Tag brach an. Auf Erden herrschte ein furchtbares Durcheinander, die Erde drehte sich nicht mehr, die Sonne hatte aufgehört zu scheinen. Die Gräber taten sich auf, und mit Entsetzen sah man seine guten Freunde wieder auferstehen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.10">Die recht gesinnten Menschen erfaßten sofort die Sachlage und fingen an zu beten und sich zu bekehren. Die Lasterhaften und Verstockten dagegen beeilten sich noch, die wüstesten Gelage und Orgien zu veranstalten, weil sie in ihrem sträflichen Materialismus meinten, es sei ja nun doch alles vorbei.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.11">
                        <pb n="429" xml:id="tg85.3.11.1"/>
Im Himmel war die Konfusion womöglich noch größer. Der liebe Gott und Petrus saßen vor ihren großen Büchern, suchten, zählten und verglichen um die Wette. Da waren die Geburts- und Todesregister, die Himmel-, Höllen- und Fegefeuerlisten. Da waren die Kontobücher – auf der einen Seite standen die Sünden, auf der anderen die guten und verdienstlichen Werke der Menschheit aufgezeichnet.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.12">Manches Mal konnte der liebe Gott sich nicht durchfinden, und Petrus schlug das Gewissen. Die Buchführung war seine schwache Seite, und er hatte es manchmal nicht ganz genau damit genommen. Gewöhnlich hatte er sich dabei beruhigt, daß es am Ende gar nicht zum Jüngsten Gericht kommen würde.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.13">Und nun war es da. – Kalter Angstschweiß trat dem armen Petrus auf die Stirn. Verstohlen blickte er zu Gottvater hinüber. Der legte gerade die Feder aus der Hand und sagte nach längerem Nachdenken: »Petrus, weißt du, ich glaube, die Idee mit dem Staatsanwalt wäre doch nicht ganz ohne. Wir finden uns da sonst nicht zurecht. Die Rechtsanschauungen haben sich gerade in letzter Zeit so sehr verschroben.« – Der liebe Gott versprach sich, er meinte verschoben – Petrus merkte es, aber wagte nicht, ihn zu verbessern.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.14">Voller Freude suchte er den Schlüssel zur Juristenabteilung und begab sich ins Fegefeuer. Als er zurückkam, erschien mit ihm ein Herr in der üblichen Büßertracht, der mit kalten, unbestechlichen Blicken um sich schaute. Er stellte sich dem lieben Gott als Staatsanwalt Donnerschlag vor. »Freut mich, klingt sehr vielversprechend«, sagte der liebe Gott, die Verbeugung erwidernd, »Petrus, du magst einstweilen die Leute versammeln und eine provisorische Scheidung vornehmen, du weißt ja, die Schafe zur Rechten, die Böcke zur Linken.« – – »Wir <pb n="430" xml:id="tg85.3.14.1"/>
pflegen uns hier nämlich bildlich auszudrücken, das ist so die Tradition«, fügte er zum Staatsanwalt gewendet hinzu.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.15">»Sie, Herr Donnerschlag, sind wohl so freundlich, einstweilen die Register durchzusehen und sich etwas zu orientieren. Petrus hat Ihnen wohl schon gesagt, was uns veranlaßt hat, Sie um Ihren fachmännischen Beistand anzugehen. – Ich habe jetzt noch alle Hände voll zu tun. Auf Wiedersehen!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.16">– Der Gerufene setzte sich vor die Bücher, zog eine Miniatur-Westentaschenausgabe des Strafgesetzbuches hervor, las, schlug auf und machte Notizen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.17">Endlich war alles so weit. Das Jüngste Gericht konnte beginnen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.18">Petrus hatte die provisorische Einteilung in Gerechte und Ungerechte sehr geschickt arrangiert.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.19">Die Gerechten begannen vorlaut ein Halleluja anzustimmen. Ein Teil der Verdammten betete und flehte um Gnade, andere tobten, ein paar alte Schiffskapitäne fluchten sogar. Als man es ihnen verwies, behaupteten sie, man gewöhne es sich im Fegefeuer an.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.20">Die Glocke des Präsidenten erscholl, und der liebe Gott hielt eine der Gelegenheit angemessene Thronrede, in welcher er sein Programm kundtat. Dann stellte er der Versammlung den Staatsanwalt Donnerschlag vor, was mit allgemeinem Gemurmel begrüßt wurde. Herr Donnerschlag ignorierte diese zweifelhafte Kundgebung vollkommen. Er glaubte die Beförderung zum himmlischen Justizminister schon ganz sicher in der Tasche zu haben. Nun intonierte das Orchester »Heulen und Zähneklappern« nach einem eigens für diesen Tag von einer einflußreichen Persönlichkeit komponierten Motiv.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.21">Petrus mußte das Buch des Lebens herbeibringen, und die Verhandlung konnte beginnen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.22">
                        <pb n="431" xml:id="tg85.3.22.1"/>
»Es geht nach dem Alphabet, Herr Staatsanwalt«, bemerkte der liebe Gott. – –</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.23">»Adam«, begann Herr Donnerschlag mit weithin vernehmlicher Stimme.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.24">Ein Gemurmel äußersten Unwillens erhob sich von allen Seiten. Im Hintergrunde wurde sogar gezischt. Sankt Petrus konnte sich eines Lächelns nicht erwehren. Da standen sie ja, die beiden Sünder, die so viel Unheil angerichtet hatten.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.25">Eva hatte die eine Hand, in der sie den berühmten Apfel hielt, auf dem Rücken versteckt und beide waren äußerst verlegen. Sie schienen sich in der großen Versammlung zu schämen, da sie so wenig anhatten. »Passons là-dessus«, warf der liebe Gott in jovialem Ton ein und sagte dann leise zum Juristen, als er dessen Brillengläser verwundert auf sich gerichtet sah: »Entschuldigen Sie, Herr Staatsanwalt, mit Adam und Eva ist das so eine besondere Sache. Sie verstehen – der göttliche Weltplan –.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.26">Herr Donnerschlag wollte etwas entgegnen, aber jetzt hatte Eva sich halb umgewandt und warf ihm über die Schulter einen Blick zu, der ihn entwaffnete. »Gut denn, lassen wir die Sache ruhen, handelt sich ja auch nur um leichte Delikte – – Mundraub.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.27">Er murmelte noch etwas vor sich hin und fuhr dann mit der Verlesung fort:</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.28">»Abel! – hat sich früh der himmlischen Gerechtigkeit entzogen, indem er vorgab, von seinem Bruder Kain, einem notorischen Mordbuben, umgebracht worden zu sein. – Ich beantrage die Vorführung.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.29">Petrus und der Herrgott wechselten einen ratlosen Blick, aber Petrus faßte sich schnell und erklärte kurz und bündig, die Sache habe längst eine andere Aufklärung gefunden. Man habe Abel seinerzeit einen Abonnementsplatz <pb n="432" xml:id="tg85.3.29.1"/>
im Himmel eingeräumt und er sei unbedingt unter die Gerechten zu klassifizieren, – was auch geschah.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.30">»Bebel«, las der Staatsanwalt mit einem tiefen Seufzer der Befriedigung – endlich fing er an, sich heimisch zu fühlen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.31">»Gehört doch nicht hierher«, rief der liebe Gott entrüstet, »bitte im Alten Testamente fortzufahren.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.32">Petrus bekam einen zornigen Blick, diese unordentliche Buchführung. Es war wirklich zu arg.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.33">Das eben aufgeflammte Feuer in den Augen des Staatsanwaltes erlosch. Enttäuscht und abgekühlt las er weiter: »Abraham«. –</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.34">Wieder wurde interpelliert. Der Himmelvater war hinter den Verlesenden getreten und raunte ihm zu:</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.35">»An Abraham dürfen wir nicht rühren. Bedenken Sie doch, der Stammvater des Volkes Israel. Sie begreifen, es sind die Prinzipien, die aufrechterhalten werden müssen.« –</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.36">Und die Musik spielte einen Tusch, während Abraham unangefochten zu den Gerechten passierte. Jakob und Esau machten einen Versuch, sich ihm anzuschließen, wurden aber zurückgehalten. Ihnen ahnte Schlimmes.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.37">Jetzt wurde eine Schar verwilderter Zuchthäusler durch einen Gendarmen vorgeführt. Sie hatten rote Kokarden an den Hüten und sangen die Marseillaise. Es waren die Kinder Korah.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.38">»Korah und Genossen«, begann Herr Donnerschlag, »mehrfach vorbestraft wegen Aufruhrs und Zusammenrottung.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.39">Es wurde beraten. Der Staatsanwalt beantragte lebenslängliche Höllenstrafe und Stellung unter Polizeiaufsicht wegen Gemeingefährlichkeit der in Frage kommenden Individuen. Auch seien denselben die bürgerlichen Ehrenrechte abzuerkennen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.40">
                        <pb n="433" xml:id="tg85.3.40.1"/>
Petrus warf sich zum Verteidiger auf. Er meinte, man müsse mit der noch besserungsfähigen Jugend nicht so hart verfahren. Aber Herr Donnerschlag hielt ihm entgegen, es könne der zunehmenden Entsittlichung und inneren Zerfetzung des Volkes durch dergleichen rot angehauchte Bestien nicht scharf genug entgegengearbeitet werden. Er wußte diese Anschauung so plausibel zu machen, daß einstimmig auf Vollstreckung des Urteils erkannt wurde.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.41">Man machte kurzen Prozeß mit der Rotte. Die Höllenversenkung wurde in Tätigkeit gesetzt, und die Kinder Korah verschwanden mit einem Hoch auf die Anarchie in der Tiefe.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.42">»Nun hat es doch endlich mal jeklappt«, ließ sich ein Eckensteher aus der Schar der Gerechten vernehmen. – Plötzlich entdeckte man, daß auch der Gendarm mitverschwunden war. Jemand wollte gesehen haben, wie ein Kind Korah ihn böswillig mit hinabgezogen habe.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.43">Der Staatsanwalt beantragte Wiederaufnahme des Verfahrens, aber Petrus erklärte ihm, daß das bis zur nächsten Höllenrevision warten müsse. Augenblicklich würden weder die Angeklagten noch die Opfer vernehmungsfähig sein.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.44">Herr Donnerschlag war just in seinem Element und begann Exempel zu statuieren, daß es eine Lust war. So wütete er mit verfassungsmäßiger Schneidigkeit durch die zwei Bücher Josua und das Buch der Richter hindurch, daß dem lieben Gott angst und bange wurde.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.45">Heimlich rief er Petrus beiseite, um Rücksprache mit ihm zu nehmen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.46">»Petrus, was sollen wir machen, der Mann wächst uns über den Kopf. Dein Rat war gut, aber ich wollte, du hättest ihn mir nicht gegeben. – Wo der Donner schlag nur all die Paragraphen hernimmt. Für jeden hat er einen, <pb n="434" xml:id="tg85.3.46.1"/>
der für ihn paßt. Was sollen wir denn anfangen, wenn er an die Könige kommt mit den fatalen Weibergeschichten. Das gibt noch den reinen Kolonialskandal.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.47">»Es ist recht peinlich«, sagte Petrus nachdenklich. »Im schlimmsten Falle müssen wir den Teufel durch Beelzebub austreiben und – –«</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.48">»Das geht nicht«, antwortete Gottvater entsetzt, »das wäre doch zu mittelalterlich possenhaft.« – »Ich meine es ja nur bildlich«, sagte Petrus mit Würde. – Die Unterhaltung wurde jäh unterbrochen durch ein wüstes Getöse, Musik, Lärmen, Durcheinanderrufen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.49">»Um Gottes willen«, schrie Petrus, »sie sind schon bei David und wir haben nicht aufgepaßt.« –</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.50">Es hatte seine Richtigkeit. Schon hatte Herr Donnerschlag seine Anklagen vorgebracht, die in schwerer Menge auf den König von Israel herabregneten. David ließ sich aber nicht einschüchtern, sondern tanzte mit seinen Hofschranzen vor der Bundeslade, während das Getöse der Harfen, Zymbeln und Pauken durch den Himmel erschallte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.51">Petrus hatte seine schwere Mühe, alles wieder zur Ruhe zu bringen. Mehrmals mußte die Glocke des Präsidenten ertönen, bis die Verhandlung weitergehen konnte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.52">Nun begann Sankt Peter mit Gewandtheit die Verteidigung aber es war ihm nicht möglich, den eklatanten Beweisen gegenüber durchzudringen, die Herr Donnerschlag auf den Tisch des Hauses niederlegte. Da war der Uriasbrief. Petrus und Gottvater konnten nur mit Mühe ihr Erstaunen darüber ausdrücken, wie der in die Hände der Staatsanwaltschaft gelangt sein konnte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.53">Der Staatsanwalt beantragte für den König David eine halbe Ewigkeit schwerer Höllenpein wegen Ehebruchs und Totschlag.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.54">Vergebens suchte Petrus darzutun, daß Ehebruch Privatsache <pb n="435" xml:id="tg85.3.54.1"/>
sei und nur auf Antrag des armen seligen Urias hätte verfolgt werden können, und daß man an dem Tode des letzteren doch nicht unbedingt den König beschuldigen könne. Derselbe habe nur angeordnet, daß Urias während der Schlacht an einem exponierten Platz Posten stehen solle. Das müsse jeder Soldat. Es könne hier also höchstens von einem<hi rend="italic" xml:id="tg85.3.54.2"> Dolus eventualis</hi> die Rede sein.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.55">»Der Petrus hat wirklich schon etwas profitiert«, dachte der liebe Gott, der ganz verwundert dem glänzenden Plädoyer zugehört hatte. Aber gegen Herrn Donnerschlag kämpfte selbst Petrus vergebens. Mit dem kostbaren Beweisstück, mit einem Hagel von Paragraphen und mit der festen Absicht, dem lasterhaften Judenkönig eins auszuwischen, trug der findige Mann des Rechts den Sieg davon, und das Unerhörte geschah. David mußte zur Hölle fahren. Für Sack und Asche war es zu spät. Nun hatten ihm alle seine Psalmen doch nichts geholfen. Verstimmt trat er die Fahrt an, mit seinen Zymbeln und Pauken zu einem letzten Tanz aufspielend.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.56">Der ganze Himmel war skandalisiert. Petrus und der liebe Gott schauten sich verzagt an. Jetzt sollte Hiob dran kommen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.57">»Gottvater«, flüsterte Petrus, »laß mich nur machen, ich werde Rat schaffen.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.58">»Mach', was du willst«, sagte der liebe Gott ganz apathisch, »nur rette mir meinen Knecht Hiob.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.59">Sankt Peter entfernte sich schleunigst.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.60">Während seiner Abwesenheit saß der liebe Gott wie auf Kohlen und mußte zuhören, wie der Staatsanwalt fortfuhr, ihm sein bestes Himmelsmaterial mit Hilfe dieses entsetzlichen Strafgesetzbuches zur Höllenware zu stempeln.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.61">Hiob, der hart mitgenommene alte Mann, dem er, der Himmelsvater, längst ein Rentengut zugesichert hatte, <pb n="436" xml:id="tg85.3.61.1"/>
wo er sich von den Strapazen seiner Heimsuchung erholen konnte, wurde der Majestätsbeleidigung geziehen. Und da sollte man noch ruhig sitzen und zuhören. Empört sprang der liebe Gott auf und übernahm dieses Mal selbst die Verteidigung:</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.62">Da hätte er denn doch noch ein Wort mitzureden. Die Sache sei überhaupt zwischen ihm und Hiob längst zur beiderseitigen Zufriedenheit beigelegt worden, da man sie nun aber noch einmal an die Öffentlichkeit gezerrt habe, so müsse er denn doch die Ansicht betonen, daß sich Hiob allerdings im Übermaß seiner Schmerzen einer Gotteslästerung schuldig gemacht, aber wenn er selbst, – Gottvater – – »Gotteslästerung mit Gefängnis bis zu drei Jahren bestraft, § 16,6«, warf der Staatsanwalt ein.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.63">Der liebe Gott konnte vor Zorn nicht weiterreden, und der Staatsanwalt setzte nun auseinander, wie hier nach seiner Auffassung unbedingt eine Majestätsbeleidigung zu konstruieren sei, insofern als für Hiob in diesem speziellen Fall eine Identität der himmlischen und irdischen Staatsgewalt vorgelegen habe. – –</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.64">Petrus war inzwischen wieder erschienen. In seiner Begleitung erblickte man einen allgemein bekannten Nervenarzt, der seinerzeit auf Erden durch seine tiefsinnigen Lehren über Suggestion und Gegensuggestion manchen Raubmörder den Händen der strafenden Gerechtigkeit entrissen hatte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.65">Ein alter Russe mit Heiligenschein und apostolischen Allüren machte die Umstehenden auf die ausgeprägte Schurkenphysiognomie des neuen Ankömmlings aufmerksam.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.66">Jetzt unterbrach Petrus den Staatsanwalt mitten im schönsten Reden, was dieser nicht gern sah, aber er hatte längst gemerkt, daß man es mit dem Alten nicht verderben dürfe. So ließ er resigniert die Zeugen abtreten. Es <pb n="437" xml:id="tg85.3.66.1"/>
waren die drei Freunde Hiobs, die mit ihren langen Reden und weisen Bemerkungen die Versammlung schon aufs höchste irritiert hatten.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.67">Es wurde eine Erholungspause gemacht und während derselben die landesüblichen Erfrischungen herumgereicht, welche aus Heuschrecken und wildem Honig bestanden.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.68">Dann ertönte wieder die Glocke, und diesmal ergriff Petrus zuerst das Wort. Er redete erst von der hohen Bedeutung des Tages, sprach dann über Gerichtswesen im allgemeinen und über das Wesen des Jüngsten Gerichtes im speziellen, erwähnte ferner, daß eine nicht zu verkennende Mißstimmung über einige der heute gefällten Urteile in der Versammlung herrsche. – Einige scheelsüchtige Böcke hätten zwar triumphiert, aber unter den Schafen herrsche große Trauer. –</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.69">Hier lächelte Herr Donnerschlag satanisch. – Er, Petrus, mache deshalb den Vorschlag, daß ein Schwurgericht zusammentreten solle, das jedesmal nach eingehender Beratung den endgültigen Urteilsspruch zu fällen habe.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.70">Ein beifälliges Gemurmel ging durch die Versammlung. Der liebe Gott atmete auf: Wenn man die Geschworenen richtig zusammenstellte, konnte Hiob noch gerettet werden. Und vielleicht waren auch noch Aussichten für David. Man mußte ihm sofort telephonieren, daß er Revision gegen das vorhin gefällte Urteil einlege.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.71">Der Staatsanwalt protestierte heftig. Aber der Redner war noch nicht zu Ende. Nachdem der Beifall sich gelegt, fuhr er fort: er habe außerdem die Absicht, eine anerkannte ärztliche Autorität als Sachverständigen heranzuziehen – hier machte der Arzt eine tiefe Verbeugung gegen die Anwesenden. – Als Sachverständiger, der vor und nach jedem Urteilsspruch den Geisteszustand aller Beteiligten aufs genaueste zu untersuchen habe. Es sei in <pb n="438" xml:id="tg85.3.71.1"/>
letzter Zeit so oft nachträglich die geistige Zurechnungsfähigkeit eines Verurteilten oder seines Richters angezweifelt worden. Hierzu komme noch, daß die Leute durch den Aufenthalt im Fegefeuer sehr oft ihre geistige Frische einbüßten. Man kann nicht nachsichtig genug vorgehen. –</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.72">Ein rasender Beifallssturm machte den Himmel erbeben und übertönte die Worte des Herrn Donnerschlag, der sich Gehör zu verschaffen suchte. Er war der erste, der sich der Untersuchung durch den skeptisch blickenden Arzt unterziehen mußte, während Petrus und der liebe Gott sich mit der Auswahl der Geschworenen beschäftigten. Die bewährte Einteilung in Böcke und Schafe wurde auch hier festgehalten.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.73">Als alles arrangiert war, wurde der Fall Hiob von neuem aufs Tapet gebracht. Der Arzt stellte zeitweilige geistige Störungen fest, an denen der alte Mann gelitten habe, und die Geschworenen verneinten die Schuldfrage.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.74">Das Urteil erregte allgemeine Befriedigung. Die Gerechten frohlockten, und manchem Verdammungskandidaten wurde leichter ums Herz. Der liebe Gott telephonierte an David, und der degradierte König beging in der Hölle einen Freudentanz.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.75">Herr Donnerschlag war außer sich. Seine Ausdrucksweise war fast unehrerbietig.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.76">»Petrus«, sagte der liebe Gott leise zu diesem, »wir müssen sehen, ihn loszuwerden. Er macht sich unmöglich und kompromittiert uns.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.77">»Nur abwarten«, meinte Petrus, »vielleicht können wir ihn unter Kuratel stellen lassen.« –</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.78">Er nahm den Arzt beiseite und besprach sich mit ihm. Aber sie wurden gleich wieder unterbrochen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.79">Ratlos stürzte der liebe Gott herbei: »Um Himmels willen, Petrus, wo bleibst du? Alles steht auf dem Kopf.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.80">
                        <pb n="439" xml:id="tg85.3.80.1"/>
Nun sieh selbst, was du angerichtet hast mit deinen Geschworenen. Eben haben sie die Jesabel freigesprochen, und jetzt ist Kain dran. Den bringen sie mir womöglich auch noch in den Himmel, deine Geschworenen!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.81">Dem armen Petrus wirbelte der Kopf. Heute mußte auch alles schiefgehen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.82">Allgemeine Panik hatte die Versammlung ergriffen. Jesabel wandelte mitten unter den Gerechten und zog drei Foxterriers an der Leine hinter sich her. Vor den Geschworenen stand Kain mit rohen Landstreichermienen und leugnete hartnäckig. Seine Manieren hatten einen unangenehm arbeitslosen Anstrich.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.83">Auch aus der Hölle ertönte wilder Aufruhr. David tanzte, und die Kinder Korah brachten ein dröhnendes Hoch auf das Proletariat der Zukunft aus. Nur der Geistesgegenwart Sankt Peters gelang es, die Situation zu retten. Er erklärte der gespannt aufhorchenden Versammlung, das Jüngste Gericht müsse unlösbarer Schwierigkeiten wegen einstweilen vertagt werden.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.84">Alles atmete auf. Nur Herr Donnerschlag stand wie angewurzelt da.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.85">Der liebe Gott begann die nötigen Befehle zu erteilen, und die himmlischen Heerscharen flatterten geschäftig hin und her.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.86">Tausende von weißbeschwingten Engeln stießen in ihre Posaunen. Petrus drückte auf einen Knopf, und das große Schwungrad der Dynamo, die das Weltsystem zu bewegen hatte, begann sich langsam zu drehen. Mit donnerartigem Getöse fingen die Planeten wieder an, ihre vorschriftsmäßigen Bahnen zu wandeln.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.87">Noch ein rasender Posaunenstoß, alles drehte sich – alles versank und verschwand. – – –</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.88">Mit einem lauten Ausruf des Schreckens fuhr der Rechtspraktikant Guido Kusbohrer aus dem Schlaf empor.</p>
                    <p rend="zenoPLm4n0" xml:id="tg85.3.89">
                        <pb n="440" xml:id="tg85.3.89.1"/>
Neben seinem Bett rasselte der Wecker. Es war höchste Zeit aufzustehen, wenn er nicht zu spät in den Gerichtssaal kommen wollte. Er kleidete sich rasch an. Der Kopf wirbelte ihm. Seine erste Verteidigung – – Himmelherrgott, ob er seinen Klienten wohl durchbringen würde?</p>
                </div>
            </div>
        </body>
    </text>
</TEI>