<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg89" n="/Literatur/M/Reventlow, Franziska Gräfin zu/Erzählungen/Wir Spione">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Wir Spione</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Reventlow, Franziska Gräfin zu: Element 00029 [2011/07/11 at 20:29:37]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <notesStmt>
                <note>
                              Erstdruck in: Simplicissimus, München (Albert Langen) 20. Jg., Nr. 32, 9. November 1915.
                           </note>
            </notesStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Franziska Gräfin zu Reventlow: Autobiographisches. Ellen Olestjerne. Novellen, Schriften, Selbstzeugnisse. Herausgegeben von Else Reventlow. Mit einem Nachwort von Wolfdietrich Rasch, München: Langen Müller, 1980.</title>
                        <author>Reventlow, Franziska Gräfin zu</author>
                    </titleStmt>
                    <extent>322-</extent>
                    <publicationStmt>
                        <date when="1980"/>
                        <pubPlace>München</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1871" notAfter="1918"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg89.3">
                <div type="h3">
                    <head type="h3" xml:id="tg89.3.1">
                        <pb n="322" xml:id="tg89.3.1.1"/>
                        <pb type="start" n="388" xml:id="tg89.3.1.2"/>Franziska Gräfin zu Reventlow</head>
                    <head type="h2" xml:id="tg89.3.2">Wir Spione</head>
                    <p xml:id="tg89.3.3">Wir verlebten das erste Kriegsjahr in einem neutralen Kurort, wo sich alle möglichen Nationalitäten zusammenfanden. In unserem näheren Bekanntenkreis war die Zusammenstellung folgende: drei Deutsche, zwei Wienerinnen, ein amerikanisches Ehepaar namens Strong und ein Italiener, welcher Ravelli hieß, ferner ein Pole und ein Herr, den man kurzerhand den »Balkan« nannte, denn er <pb n="388" xml:id="tg89.3.3.1"/>
stammte sicher irgendwo von dort drunten her, sprach sich aber nicht näher darüber aus.</p>
                    <p rend="zenoPLm4n0" xml:id="tg89.3.4">Da wir alle fern der Heimat waren, taten wir das einzige, was man in dieser Zeit im Ausland tun kann, wir verschlangen die Zeitungen und warteten auf Briefe. Dabei gaben wir uns alle Mühe, möglichst wenig über die Weltereignisse zu sprechen – das war so ausgemacht worden, weil wir bis zum Ausbruch des Krieges sozusagen befreundet waren. Es war gewissermaßen unser Ehrgeiz, zu beweisen, daß man unter gebildeten Menschen sich selbst in solchen Zeiten auf eine internationale Basis stellen könne.</p>
                    <p rend="zenoPLm4n0" xml:id="tg89.3.5">Natürlich war es nicht immer so einfach; zum Beispiel fiel der Unterseebootkrieg Mr. Strong des öftern auf die Nerven; seine Frau verhielt sich mehr passiv und zog es vor, mit dem Polen zu kokettieren. Der Pole war Revolutionär und schwur bei jeder Gelegenheit, er würde noch auf einer Kanone in Warschau einreiten; selbstverständlich hieß er Stanislaus. Wenn er sich in dieser Weise äußerte, pflegte der Balkan leise zu knurren und sah ihn scheel von der Seite an. Im übrigen schien sein politisches Interesse nicht besonders rege, dagegen war er leidenschaftlicher Spieler, sprach gerne von Ostende und Monte Carlo, und die Zukunft dieser beiden Orte erfüllte ihn mit großer Besorgnis. – Zwischen den Wienerinnen und Signor Ravelli spannen sich ebenfalls zarte Fäden; sie konnten stundenlang über der Karte von Südtirol sitzen und versuchten dann, sich freundschaftlich über die Berechtigung der Irredenta zu einigen. Wir Deutschen ärgerten uns manchmal, wenn die Damen bei diesen Verhandlungen zu entgegenkommend waren.</p>
                    <p rend="zenoPLm4n0" xml:id="tg89.3.6">Bis zum Frühjahr hatten wir so in gutem Einvernehmen gelebt. Die Bevölkerung des kleinen Kurorts sowie die anderen Fremden schienen sich darüber zu verwundern; <pb n="389" xml:id="tg89.3.6.1"/>
denn wo wir uns nur sehen ließen, in Restaurants, Cafés oder bei geselligen Veranstaltungen, wurden wir mit größtem Interesse angestarrt und beobachtet; sogar auf der Straße bemerkten wir, daß die Vorübergehenden sich gegenseitig auf uns aufmerksam machten.</p>
                    <p rend="zenoPLm4n0" xml:id="tg89.3.7">Allmählich aber geriet die internationale Basis ins Schwanken; vor allem begann man sich gegenseitig zu mißtrauen. Das Ehepaar Strong interessierte sich nach unserem Gefühl in übertriebener Weise für die Feldpostkarten, die wir von Bekannten oder Angehörigen erhielten, und uns stieg manchmal der Verdacht auf, sie möchten am Ende gar nicht von drüben, sondern verkappte Engländer sein; denn wenn wir irgendwelche ganz harmlose Fragen über englische Bräuche taten, konnte Strong manchmal in geradezu verletzender Weise antworten:</p>
                    <p rend="zenoPLm4n0" xml:id="tg89.3.8">»Uarum uollen Sie das uissen?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg89.3.9">Außerdem schickte er rätselhaft viele Telegramme ab. Signor Ravelli war überhaupt ungemein neugierig, und wir warnten seine Freundinnen wiederholt, ihm nicht soviel von ihren Alpenwanderungen zu erzählen. Was schließlich den Balkan betraf, so traute ihm in politischer Hinsicht wohl niemand über den Weg, weder wir noch die anderen. Aber entweder merkte er es nicht, oder es focht ihn nicht an. Er blieb immer derselbe, präokkupiert, aber heiter und liebenswürdig. Und wie es denn so kommt – unsere Wege trennten sich unter Mißtrauen und Übelwollen, und die Strongs zogen erbittert in eine entfernt gelegene Pension.</p>
                    <p rend="zenoPLm4n0" xml:id="tg89.3.10">Bald darauf brach der italienische Krieg aus; unser Freund Ravelli rückte zwar nicht ein (ob das Vaterland oder er selbst darauf verzichtete, haben wir nicht erfahren), war aber fortan sehr verstimmt, konnte sich nicht mehr mit seinen schönen Gegnerinnen über die Grenzfragen einigen und verschwand grollend aus unserem Gesichtskreis. <pb n="390" xml:id="tg89.3.10.1"/>
Der Balkan war der einzige, der uns treu blieb, denn Stanislaus war schon vorher unter mysteriösen Andeutungen abgereist – wir erhielten späterhin eine Postkarte von ihm aus Warschau, aus der jedoch nicht hervorging, ob er wirklich auf einer Kanone oder auf normalem Wege dahingekommen war.</p>
                    <p rend="zenoPLm4n0" xml:id="tg89.3.11">Neue Bekanntschaften ergaben sich nicht, und das Dasein war recht eintönig geworden, man mußte sich Mühe geben, die Zeit nur einigermaßen totzuschlagen. So kam uns eines Tages aus reiner Langeweile die Idee, unseren Balkon mit einem Zeltdach zu versehen, weil die sich immer gleichbleibende Neugier der Bevölkerung uns auf die Länge lästig fiel. Es wurde also Stoff gekauft, beratschlagt, konstruiert, und als alles so gut wie fertig war, meinte der Balkan, der plötzlich ungeahnte technische Fähigkeiten entwickelte, man solle doch innerhalb des Zeltes eine elektrische Lampe anbringen, um abends in aller Gemütlichkeit »meine Tante, deine Tante« spielen zu können. Gesagt, getan – bald war alles fertig; wir hofften nun die Früchte unserer Arbeit ungestört zu genießen und vor allem von der Neugier der Passanten befreit zu sein. Aber als wir zum erstenmal unter unserem Zeltdach »meine Tante, deine Tante« spielten, gab es geradezu einen Volksauflauf. Der Balkan wurde nervös und trat an die Balustrade, um das Volk zu beruhigen. In diesem Moment teilte sich die Menge, um zwei Polizisten durchzulassen, welche uns für verhaftet erklärten. Was wir getan hatten, war uns völlig unklar, aber wir folgten ihnen ohne weiteres, nur der Balkan verfärbte sich. Am nächsten Morgen wurden wir dem Kommissär vorgeführt, und unsere Verwunderung stieg, als gleich darauf auch unsere alten Freunde Strong und Ravelli in das Bureau traten. Ein Gerichtsdiener begleitete sie und rief dem Kommissär zu: »Spionage – Register 6.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg89.3.12">
                        <pb n="391" xml:id="tg89.3.12.1"/>
»Also doch«, sagte die eine Wienerin halblaut, aber Mr. Strong hatte es gehört und fuhr wie ein Berserker auf sie los: »Und Sie? Uir haben Sie immer für Spion gehalten mit Ihre deutsche Freunde und das Balkan – –«</p>
                    <p rend="zenoPLm4n0" xml:id="tg89.3.13">»Balkan?« brüllte der Balkan, »was soll das heißen?« Dies war die erste politische Äußerung, die wir von ihm hörten, aber niemand antwortete. Der Kommissär rief zur Ordnung, aber die allgemeine Aufregung war nicht mehr zu beschwichtigen. Ravelli wandte sich wild an den Beamten und erklärte, die Damen hätten tatsächlich eine merkwürdige Kenntnis der Tiroler Grenzen und die Herrschaften aus Deutschland – – – nun wallte auch in uns das Mißtrauen wieder auf – das Interesse für Feldpostkarten – die Telegramme – kurz und gut, es entbrannte ein Kreuzfeuer von gegenseitigen Beschuldigungen. Der Kommissär wartete geduldig, bis etwas Stille eingetreten war, nahm dann die Personalien auf und eröffnete uns milde, daß wir samt und sonders unter Spionageverdacht ständen. Wir Deutsche und Österreicher waren Register 5. – Der Balkan wurde gesondert behandelt. Man nahm einen Fingerabdruck von ihm, wir hatten diese Prozedur noch nie gesehen und verfolgten sie neugierig; er aber benahm sich wie bei allen technischen Dingen sehr sachverständig. Dann begann das Verhör. Register 6 wurde zuerst vorgerufen – sie hätten Bomben fabriziert und besäßen zusammenlegbare Flugzeuge, wir dagegen wurden beschuldigt, Lichtsignale gegeben zu haben – – Lichtsignale – – wir hatten doch seit drei Wochen keinen Abend zu Hause verbracht, ausgenommen den gestrigen, der so unliebsam unterbrochen wurde; aber hier musterte der Kommissär uns der Reihe nach mit einem durchbohrenden Blick und erklärte, für Spione habe man uns von Anfang an gehalten, uns dauernd beobachtet und nur auf Beweismaterial gewartet.</p>
                    <p rend="zenoPLm4n0" xml:id="tg89.3.14">
                        <pb n="392" xml:id="tg89.3.14.1"/>
»Und das Beweismaterial?« fragten wir. – Nun, eben die Lampe – – wir erfuhren erst jetzt, daß Lichtsignale zu spionistischen Zwecken verwandt werden können. Und unser Erstaunen war so aufrichtig, daß er sich endlich überzeugen ließ, die Lampe sei tatsächlich nur zu Beleuchtungszwecken angebracht worden. Sodann kam wieder Register 6 an die Reihe. Mr. Strong antwortete auf die Fragen des Kommissärs einigermaßen erbittert:</p>
                    <p rend="zenoPLm4n0" xml:id="tg89.3.15">» <hi rend="italic" xml:id="tg89.3.15.1">Yes,</hi> Herr Kommissär, uir haben nicht nur Bomben und zusammenlegbare Äroplane, sondern auch eine Flotte in unserer Pension.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg89.3.16">Der Kommissär wurde nun ernstlich nervös und sagte, gleich würde die Gerichtskommission da sein, welche inzwischen die Haussuchung vornehme.</p>
                    <p rend="zenoPLm4n0" xml:id="tg89.3.17">Man wartete, nach einer guten Weile erschien die Kommission und brachte drei Fußbälle von verschiedener Größe und einen vielfach zusammengeklappten Gegenstand. – – Mr. Strong klappte ihn mit größter Seelenruhe auseinander, und er erwies sich als ein amerikanischer Liegestuhl mit Lesepult und unendlichen Finessen; dann bemerkte er, der Kommissär möge doch mit diesem Flugzeug eine Probefahrt machen. Er selbst würde inzwischen versuchen, mit seinen Bomben die Polizei in die Luft zu sprengen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg89.3.18">Der Kommissär war so enttäuscht, daß wir wirklich Mitgefühl hatten. Es blieb ihm nichts anderes übrig, als beide Register zu entlassen und sich obendrein noch zu entschuldigen. Nur der Balkan wurde zurückbehalten; er hatte sich zwar nie politisch betätigt, sondern war einfach ein vielgesuchter internationaler Hoteldieb.</p>
                    <p rend="zenoPLm4n0" xml:id="tg89.3.19">Wir anderen verließen gemeinsam das Gerichtsgebäude und versöhnten uns unterwegs, fortan war alles gegenseitige Mißtrauen geschwunden.</p>
                    <p rend="zenoPLm4n0" xml:id="tg89.3.20">Noch manchen Abend saßen wir auf dem Balkon und <pb n="393" xml:id="tg89.3.20.1"/>
spielten Karten. Wir glaubten uns rehabilitiert und wurden auch nicht wieder verhaftet, aber die Vorübergehenden blieben immer noch stehen und hielten uns nach wie vor für Spione.</p>
                </div>
            </div>
        </body>
    </text>
</TEI>