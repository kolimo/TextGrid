<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg98" n="/Literatur/M/Reventlow, Franziska Gräfin zu/Essays/Erinnerungen an Theodor Storm">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Erinnerungen an Theodor Storm</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Reventlow, Franziska Gräfin zu: Element 00038 [2011/07/11 at 20:29:37]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <notesStmt>
                <note>
                              Erstdruck in: Frankfurter Zeitung, Abendblatt, Frankfurt am Main, Nr. 71, 12. März 1897.
                           </note>
            </notesStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Franziska Gräfin zu Reventlow: Autobiographisches. Ellen Olestjerne. Novellen, Schriften, Selbstzeugnisse. Herausgegeben von Else Reventlow. Mit einem Nachwort von Wolfdietrich Rasch, München: Langen Müller, 1980.</title>
                        <author>Reventlow, Franziska Gräfin zu</author>
                    </titleStmt>
                    <extent>286-</extent>
                    <publicationStmt>
                        <date when="1980"/>
                        <pubPlace>München</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1871" notAfter="1918"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg98.3">
                <milestone unit="sigel" n="Reventlow-Autobiogr." xml:id="tg98.3.1"/>
                <head type="h3" xml:id="tg98.3.3">Franziska Gräfin zu Reventlow</head>
                <head type="h2" xml:id="tg98.3.4">Erinnerungen an Theodor Storm<ref type="noteAnchor" target="#tg98.3.59">
                        <anchor xml:id="tg98.3.4.1" n="/Literatur/M/Reventlow, Franziska Gräfin zu/Essays/Erinnerungen an Theodor Storm#Fußnote_1"/>
                        <ptr cRef="/Literatur/M/Reventlow, Franziska Gräfin zu/Essays/Erinnerungen an Theodor Storm#Fußnoten_1"/>
                        <hi rend="superscript" xml:id="tg98.3.4.2.1">1</hi>
                    </ref>
                </head>
                <milestone unit="sigel" n="Reventlow-Autobiogr." xml:id="tg98.3.5"/>
                <pb type="start" n="286" xml:id="tg98.3.6"/>
                <p xml:id="tg98.3.7">Am grauen Strand, am grauen Meer</p>
                <p xml:id="tg98.3.8">Und seitab liegt die Stadt,</p>
                <p xml:id="tg98.3.9">Der Nebel drückt die Dächer schwer</p>
                <p xml:id="tg98.3.10">Und durch die Stille rauscht das Meer</p>
                <p xml:id="tg98.3.11">Eintönig um die Stadt.</p>
                <p xml:id="tg98.3.12">Doch hängt mein ganzes Herz an Dir,</p>
                <p xml:id="tg98.3.13">Du graue Stadt am Meer,</p>
                <p xml:id="tg98.3.14">Der Jugend Zauber für und für</p>
                <p xml:id="tg98.3.15">Ruht lächelnd doch auf Dir, auf Dir</p>
                <p xml:id="tg98.3.16">Du graue Stadt am Meer.</p>
                <lb xml:id="tg98.3.17"/>
                <p xml:id="tg98.3.18">In Husum, der kleinen, grauen Stadt am Nordseestrand, zieht sich dicht am Hafen eine enge stille Straße hin, genannt die »Wasserreihe«. Dort steht ein schmuckloses Haus, umgeben von einem schwarzen Bretterzaun, über <pb n="286" xml:id="tg98.3.18.1"/>
dem uralte Kastanienbäume ihr dunkles Laubdach emporwölben. In diesem Haus wohnte Husums Dichter Theodor Storm lange Jahre seines Lebens hindurch, hier hat er jene Novellen geschrieben, die auf dem Boden seiner Heimat spielen, auf dem Boden dieses abgelegenen, in grauen Nordseenebeln verborgenen Erdenwinkels, dessen intime Reize keiner so wie er zu belauschen und so unvergleichlich wiederzugeben wußte.</p>
                <p rend="zenoPLm4n0" xml:id="tg98.3.19">Seinem bürgerlichen Beruf nach war Storm, solange er in Husum lebte, Amtsrichter. Die Husumer pflegten, in der Liebe und Verehrung für ihren Sänger, seinen Titel stets zu ignorieren und nannten ihn zum Unterschied von zahlreichen Namensvettern nie anders wie »Dichter Storm«. Er selbst verabscheute alles, was einer Beweihräucherung ähnlich sehen konnte.</p>
                <p rend="zenoPLm4n0" xml:id="tg98.3.20">Als ihm einmal in einer Abendgesellschaft ein besonders begeisterter Verehrer in etwas aufdringlicher Weise zu huldigen bestrebt war, indem er stets aufs Neue sein Glas emporhob und, Storm zutrinkend, ausrief: »Dichter! – Dichter!«, da wandte Storm sich schließlich ärgerlich mit einem ziemlich laut gemurmelten »Schafskopf« ab und würdigte den armen X. keines Blickes mehr. Er wollte eben wie jeder wahre Künstler nur ein Mensch unter Menschen sein.</p>
                <p rend="zenoPLm4n0" xml:id="tg98.3.21">Storm hat nie zu denen gehört, die Unrast des Genies auf unruhig verschlungenen Wegen durch die Welt umtreibt. Ihn hat der kleine Kreis, in dem sein Leben verlief, nie im freien künstlerischen Schaffen eingeengt. Er hat sich die seltene Gabe der Lebensfreude am Kleinen und Kleinsten bis ins späteste Alter hinein bewahrt, obgleich das Leben auch ihm manches schwere Herzeleid zugefügt hat. Seine erste Frau, die von seltener Schönheit gewesen sein soll, starb bei der Geburt des 7. Kindes. Ihr hat er die ergreifenden Verse nachgedichtet:</p>
                <lb xml:id="tg98.3.22"/>
                <pb n="287" xml:id="tg98.3.23"/>
                <p xml:id="tg98.3.24">Das aber kann ich nicht ertragen,</p>
                <p xml:id="tg98.3.25">Daß so wie sonst die Sonne lacht,</p>
                <p xml:id="tg98.3.26">Daß wie in deinen Lebenstagen</p>
                <p xml:id="tg98.3.27">Die Uhren gehen, Glocken schlagen,</p>
                <p xml:id="tg98.3.28">Einförmig wechseln Tag und Nacht;</p>
                <lb xml:id="tg98.3.29"/>
                <p xml:id="tg98.3.30">Daß, wenn des Tages Lichter schwanden,</p>
                <p xml:id="tg98.3.31">Wie sonst der Abend uns vereint;</p>
                <p xml:id="tg98.3.32">Und daß, wo sonst dein Stuhl gestanden,</p>
                <p xml:id="tg98.3.33">Schon andre ihre Plätze fanden</p>
                <p xml:id="tg98.3.34">Und nichts dich zu vermissen scheint.</p>
                <lb xml:id="tg98.3.35"/>
                <p xml:id="tg98.3.36">Indessen von den Gitterstäben</p>
                <p xml:id="tg98.3.37">Die Mondesstreifen schmal und karg</p>
                <p xml:id="tg98.3.38">In deine Gruft hinunterweben</p>
                <p xml:id="tg98.3.39">Und mit gespenstig trübem Leben</p>
                <p xml:id="tg98.3.40">hinwandeln über deinen Sarg.</p>
                <lb xml:id="tg98.3.41"/>
                <p xml:id="tg98.3.42">Storm heiratete später noch einmal. Sein Familienleben war auch in zweiter Ehe das denkbar glücklichste. Seine Gattin wußte mit liebevollem Verständnis das Heim des schaffenden Mannes zu einer wohltuenden Häuslichkeit zu gestalten, und die Kinder hingen mit fast schwärmerischer Verehrung an ihm, dessen heiter jugendfrisches Gemüt Verständnis für alles hatte, was jung und frei emporwuchs. Das Storm'sche Haus war eine wirkliche Idylle, man mochte kommen, wann man wollte, an Winterabenden, wenn die zahlreiche Familie beim warmen Kaminfeuer beisammen saß und der Dichter mit seiner klangvollen, etwas leisen Stimme vorlas, manchmal seine eigenen Werke – oder an Sommertagen in dem lauschigen Garten, den er selbst mit liebevoller Sorgfalt pflegte. Storms äußere Erscheinung hatte etwas von einer Märchengestalt an sich, der kleine, etwas gebeugte Mann mit dem langen, schlohweißen Bart und den milden hellblauen <pb n="288" xml:id="tg98.3.42.1"/>
Augen, der in seinem schwarzen Beamtenrock so still und unauffällig einherging. So sah man ihn Tag für Tag, im Sommer mit einem breitkrempigen, weißen Strohhut, winters mit brauner Pelzmütze und dickem, weißem Shawl um den Hals, durch die winkeligen Gassen der kleinen Stadt gehen, um seinen Amtsgeschäften obzuliegen oder seinen Spaziergang zu machen.</p>
                <p rend="zenoPLm4n0" xml:id="tg98.3.43">Storms Lieblingsweg war der Seedeich, der, hart an der Stadt beginnend, sich meilenweit in die grüne Marsch hineinschlängelt. Gegen Westen blickt man auf das Meer mit den vorgelagerten, meist »wie Träume im Nebel« liegenden Inseln und landeinwärts auf weite grüne Wiesenflächen, die in unabsehbarer Ferne mit dem Horizont verschwimmen. Mit der Heimatliebe aller eingebornen Küstenbewohner, die selbst in der schönsten Gebirgs- oder Waldgegend die andern oft unverständliche Sehnsucht nach diesem unendlich weiten Horizont ihres Flachlandes nicht los werden, hing Storm an seiner Heimatgegend. Stundenlang konnte er an Sonntagen dem Anschlagen der Wellen gegen den Strand und dem einförmigen Schrei der Seevögel lauschen oder in die rotblühende Heide, die sich auf der andern Seite der Stadt hindehnt, hineinwandern.</p>
                <p rend="zenoPLm4n0" xml:id="tg98.3.44">In religiösen Sachen war Storm völliger Freidenker und pflegte auch mit seiner Meinung nicht hinter dem Berge zu halten. Das war vielleicht das Einzige, wodurch er die strenge an dem guten alten Brauch des sonntägigen Kirchganges festhaltenden Mitbürger hier und da vor den Kopf stieß, ausgenommen noch, daß einige besonders charakterfeste ältere Damen der Gemeinde manchesmal an den »zu freien« Stellen seiner Werke Ärgernis nahmen. Aber im Ganzen war man doch milde und tolerant in der kleinen Stadt und »vergab« dem Menschen, was der Dichter etwa »fehlen« mochte, und Storms liebenswürdige <pb n="289" xml:id="tg98.3.44.1"/>
Persönlichkeit trug über alle Bedenken gegen seine Ansichten in diesen oder jenen Lebenssachen stets den Sieg davon.</p>
                <p rend="zenoPLm4n0" xml:id="tg98.3.45">Eines eigentümlichen Zuges möchte ich hier noch Erwähnung tun. Storm glaubte trotz seiner rationalistischen Lebensauffassung an alle möglichen Geister. Es war so eine Art Märchenglauben in ihm. Er verkehrte viel in der Familie des Landrats<ref type="noteAnchor" target="#tg98.3.61">
                        <anchor xml:id="tg98.3.45.1" n="/Literatur/M/Reventlow, Franziska Gräfin zu/Essays/Erinnerungen an Theodor Storm#Fußnote_2"/>
                        <ptr cRef="/Literatur/M/Reventlow, Franziska Gräfin zu/Essays/Erinnerungen an Theodor Storm#Fußnoten_2"/>
                        <hi rend="superscript" xml:id="tg98.3.45.2.1">2</hi>
                    </ref>, dem das alte, malerisch von Ulmen umkränzte Schloß Husums mit seinen weiten Räumen, großen Sälen, Wendeltreppen und unheimlich düsteren Gängen zur Amtswohnung diente. Nachdem Storm Husum schon verlassen, kehrte er alljährlich zu längerem Besuch im Schlosse ein und war dann durch keine Macht der Welt zu bewegen, sein Quartier in einem der ziemlich zahlreichen Zimmer aufzuschlagen, in denen es »spuken« sollte. Abends vermochten wir Kinder ihn öfters zum Erzählen von Geister- und Spukgeschichten, dann konnte ihn selbst das Gruseln so heftig ankommen, daß er stets eines von uns als Begleitung mitnahm, wenn er sich nach den entlegenen Gastzimmern, die er bewohnte, begeben wollte.</p>
                <p rend="zenoPLm4n0" xml:id="tg98.3.46">Damit habe ich schon vorgegriffen. Im Jahre 1880 verließ Storm Husum, um frei von Amt und Bürden seinen Lebensabend in dem anmutigen holsteinischen Dörfchen Hademarschen zu beschließen. Er baute sich dort ein eigenes Haus und lebte die Jahre, die ihm noch vergönnt waren, nur seinem Schaffen und seiner Häuslichkeit. Jedes Jahr kam er auf längere Zeit wieder nach Husum. Der Abschied von der alten Heimat wurde ihm stets aufs Neue schwer und er sprach in den letzten Jahren sogar davon, sich wieder ganz dort niederzulassen.</p>
                <p rend="zenoPLm4n0" xml:id="tg98.3.47">Aber es kam nicht mehr dazu. Theodor Storm starb im <pb n="290" xml:id="tg98.3.47.1"/>
Juni 1888. Er war schon lange schwer leidend gewesen, aber vom Sterben wollte er nie etwas wissen. Ihm, der zeitlebens ein Priester des Schönen gewesen, erschien der Tod als etwas Häßliches, Grauenvolles und er sprach oft in bezug auf sein Alter davon, wie schön auch das Abendrot noch sei, wenn die Sonne niedergegangen.</p>
                <p rend="zenoPLm4n0" xml:id="tg98.3.48">In seiner grauen Stadt am Meer liegt er begraben, auf dem kleinen, lindenbeschatteten, alten Kirchhof. Die Husumer haben ihren Dichter nicht vergessen und legen ihm noch manchen roten Heidekranz auf die schmucklosen, grauen Steinplatten nieder, welche die Storm'sche Familiengruft decken.</p>
                <p rend="zenoPLm4n0" xml:id="tg98.3.49">In seinem Testament hatte Storm ausdrücklich verlangt, ohne Geistlichen und ohne Glockenklang begraben zu werden. Kurz vor seinem Tode hatte er noch einmal darauf hingewiesen, daß er sein letztes Bekenntnis in folgenden Worten seines Gedichtes »Ein Sterbender« niedergelegt habe:</p>
                <lb xml:id="tg98.3.50"/>
                <p xml:id="tg98.3.51">»Auch bleib' der Priester meinem Grabe fern,</p>
                <p xml:id="tg98.3.52">Denn nicht geziemt sich's, daß an meinem Sarge</p>
                <p xml:id="tg98.3.53">Protest gepredigt werde dem, was ich gewesen,</p>
                <p xml:id="tg98.3.54">Indeß ich ruh' im Bann des ew'gen Schweigens.«</p>
                <lb xml:id="tg98.3.55"/>
                <milestone unit="sigel" n="Reventlow-Autobiogr." xml:id="tg98.3.57"/>
                <div type="footnotes">
                    <head type="h4" xml:id="tg98.3.58">Fußnoten</head>
                    <note xml:id="tg98.3.59.note" target="#tg98.3.4.1">
                        <p xml:id="tg98.3.59">
                            <anchor xml:id="tg98.3.59.1" n="/Literatur/M/Reventlow, Franziska Gräfin zu/Essays/Erinnerungen an Theodor Storm#Fußnoten_1"/>
                            <ref cRef="/Literatur/M/Reventlow, Franziska Gräfin zu/Essays/Erinnerungen an Theodor Storm#Fußnote_1" xml:id="tg98.3.59.2">1</ref> [Fußnote der Redaktion:] Soeben erläßt ein Komité, dessen geschäftsführenden Ausschuß die Herren Commerzienrat E. Paetel, Dr. Julius Rodenberg und Prof. Erich Schmidt in Berlin bilden, einen Aufruf zur Errichtung eines Denkmals für Theodor Storm, das dem Dichter in seiner Heimatstadt Husum errichtet werden soll. Beiträge sind an die Verlagsbuchhandlung von Gebrüder Paetel in Berlin W, Lützowstraße 7, zu richten. Vielleicht tragen obige »Erinnerungen« dazu bei, dieses schöne Vorhaben zu fördern.</p>
                    </note>
                    <note xml:id="tg98.3.61.note" target="#tg98.3.45.1">
                        <p xml:id="tg98.3.61">
                            <anchor xml:id="tg98.3.61.1" n="/Literatur/M/Reventlow, Franziska Gräfin zu/Essays/Erinnerungen an Theodor Storm#Fußnoten_2"/>
                            <ref cRef="/Literatur/M/Reventlow, Franziska Gräfin zu/Essays/Erinnerungen an Theodor Storm#Fußnote_2" xml:id="tg98.3.61.2">2</ref> Verfasserin ist ein Kind dieser Familie.</p>
                    </note>
                </div>
            </div>
        </body>
    </text>
</TEI>