<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg403" n="/Literatur/M/Rilke, Rainer Maria/Dichtungen in Prosa/[Prosagedichte]/Die Auslage des Fischhändlers">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Die Auslage des Fischhändlers</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Rilke, Rainer Maria: Element 00435 [2011/07/11 at 20:24:40]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <notesStmt>
                <note>
                                 Entstanden 1907 und 1925, Erstdruck: Aus Rainer Maria Rilkes Nachlaß, Frankfurt/M. 1950.
                              </note>
            </notesStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Rainer Maria Rilke: Sämtliche Werke. Herausgegeben vom Rilke-Archiv in Verbindung mit Ruth Sieber-Rilke, besorgt von Ernst Zinn, Band 1–6, Wiesbaden und Frankfurt a.M.: Insel, 1955–1966.</title>
                        <author key="pnd:118601024">Rilke, Rainer Maria</author>
                    </titleStmt>
                    <extent>1133-</extent>
                    <publicationStmt>
                        <date notBefore="1955" notAfter="1966"/>
                        <pubPlace>Frankfurt a.M.</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1907" notAfter="1925"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg403.3">
                <milestone unit="sigel" n="Rilke-SW Bd. 6" xml:id="tg403.3.1"/>
                <div type="h4">
                    <head type="h4" xml:id="tg403.3.2">
                        <pb type="start" n="1133" xml:id="tg403.3.2.1"/>
Die Auslage des Fischhändlers</head>
                    <milestone unit="head_start"/>
                    <p rend="zenoPC" xml:id="tg403.3.3">
                        <hi rend="italic" xml:id="tg403.3.3.1">(Neapel)</hi>
                    </p>
                    <milestone unit="head_end"/>
                    <lb xml:id="tg403.3.4"/>
                    <p xml:id="tg403.3.5">Auf leicht geneigter Marmorplatte liegen sie in Gruppen, manche auf dem feuchten Stein, mit ein wenig schwärzlichem Moos unterlegt, andre in von Nässe dunkelgewordenen flachen Spankörben. Silbern beschuppte, darunter einer, rund nach oben gebogen, wie ein Schwertarm in einem Wappen, so daß das Silber an ihm sich spannt und schimmert. Silbern beschuppte, die quer – über liegen, wie aus altem Silber, schwärzlich beschlagen, und drüber einer, der das Maul voran, zurückzukommen scheint, entsetzt, aus dem Haufen hinter ihm. Hat man erst einmal sein Maul gemerkt, so sieht man, da und da, noch eines, ein anderes, rasch hergewendet, klagend. (Was man »klagend« nennen möchte, entsteht wohl, weil hier die Stelle, von der Stimme ausgeht, sofort Stummheit bedeutet, ein Bild des ) Und nun sucht man, infolge einer Überlegung vielleicht, die Augen. Alle diese flachen, seitlich hingelegten, wie mit Uhrgläsern überdeckten Augen, an die die im Wasser schwimmenden Bilder herangetrieben sind, solange sie schauten. Nicht anders waren sie damals, ebenso blicklos gleichgültig: denn Blicke trüge das Wasser nicht. Ebenso seicht und untief, leer herausgewendet, wie Wagenlaternen bei Tag. Aber hingetragen durch Widerstand und Bewegung jener dichteren Welt, warfen sie, leicht und sicher, Zeichnung um Zeichnung, Wink und <pb n="1133" xml:id="tg403.3.5.1"/>
Wendung einwärts in ein uns unbekanntes Bewußtsein. Still und sicher trieben sie her, vor dem glatten Entschluß, ohne ihn zu verraten; still und sicher standen sie tagelang der Strömung entgegen, überzogen von ihr, von Schattenfluchten verdunkelt. Nun aber sind sie ausgelöst aus den langen Strähnen ihres Schauens, flach hingelegt, ohne daß es deshalb möglich wäre, in sie einzudringen. Die Pupille wie mit schwarzem Stoff bezogen, der Umkreis um sie aufgelegt, wie dünnstes Blattgold. Mit einem Schrecken, ähnlich dem, den man beim Beißen auf etwas Hartes erfährt, entdeckt man die Undurchdringlichkeit dieser Augen –, und plötzlich meint man, vor lauter Stein und Metall zu stehen, wie man über (den) Tisch hinsieht. Alles Gebogene ist hart anzusehen, und der Haufen stahlglänzender, pfriemenförmiger Fische liegt kalt und schwer wie ein Haufen Werkzeuge da, mit denen andere, die das Aussehn von Steinen haben, geschliffen worden sind. Denn da nebenan liegen sie: runde glatte Achate, von braunen, blassen und goldenen Adern durchzogen, Streifen von rötlich-weißem Marmor, Jadestücke von vorsichtig gewölbtem Schliff, teilweise bearbeitete Topase, Bergkristall mit Spitzen von Amethyst, Opale aus Quallen. Und eine ganz dünne Schicht verweilenden Wassers ist noch über ihnen allen und trennt sie von diesem Licht, in dem sie fremd sind, verschlossen, Behälter, die man vergebens zu öffnen versucht hat.</p>
                    <pb n="1134" xml:id="tg403.3.6"/>
                </div>
            </div>
        </body>
    </text>
</TEI>