<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg435" n="/Literatur/M/Rilke, Rainer Maria/Erzählungen und Skizzen/Generationen">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Generationen</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Rilke, Rainer Maria: Element 00454 [2011/07/11 at 20:24:40]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <notesStmt>
                <note>
                              Entstanden 1898, Erstdruck in: Die Zukunft (Berlin), 7. Jg., Nr. 9, 1898.
                           </note>
            </notesStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Rainer Maria Rilke: Sämtliche Werke. Herausgegeben vom Rilke-Archiv in Verbindung mit Ruth Sieber-Rilke, besorgt von Ernst Zinn, Band 1–6, Wiesbaden und Frankfurt a.M.: Insel, 1955–1966.</title>
                        <author key="pnd:118601024">Rilke, Rainer Maria</author>
                    </titleStmt>
                    <extent>509-</extent>
                    <publicationStmt>
                        <date notBefore="1955" notAfter="1966"/>
                        <pubPlace>Frankfurt a.M.</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date when="1898"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg435.3">
                <milestone unit="sigel" n="Rilke-SW Bd. 4" xml:id="tg435.3.1"/>
                <div type="h3">
                    <head type="h3" xml:id="tg435.3.2">Rainer Maria Rilke</head>
                    <head type="h2" xml:id="tg435.3.3">Generationen</head>
                    <p xml:id="tg435.3.4">
                        <pb type="start" n="509" xml:id="tg435.3.4.1"/>
In unseren Stuben riecht es am Donnerstag nach Tomaten, am Sonntag nach Gänsebraten, und jeden Montag ist Wäsche. So sind die Tage: der rote, der fette, der seifige. Außerdem giebt es noch die Tage hinter der Glastür; oder eigentlich einen einzigen Tag aus Kühle, Seide und Sandelholz. Das Licht darin ist gesiebt, fein, silbern, still; Ruß, Sturm, Lärm und Fliegen kommen nicht mit herein wie in alle anderen Stuben. Und doch ist nur die Glastür dazwischen; aber sie ist wie zwanzig eherne Tore, oder wie eine Brücke, die nicht enden will, oder wie ein Fluß mit einer unsicheren Fähre von Ufer zu Ufer.</p>
                    <p rend="zenoPLm4n0" xml:id="tg435.3.5">Selten kommt jemand hinüber und erkennt nach und nach, tief in der Dämmerung: über dem Sofa, groß, in Goldrahmen, der Großvater, die Großmutter. Es sind enge, ovale Brustbilder, aber beide haben ihre Hände hineingehoben, so mühsam das gewesen sein mag. Es wären keine Porträts geworden ohne diese Hände, hinter denen sie leise und bescheiden hingelebt haben, alle Tage lang. Diese Hände hatten das Leben gehabt und die Arbeit, die Sehnsucht und die Sorge, waren mutig und jung gewesen und sind müde und alt geworden, während sie selbst nur fromme, ehrfürchtige Zuschauer dieser Geschicke waren. Ihre Mienen blieben müßig irgendwo weit vom Leben und hatten nichts zu tun, als einander langsam ähnlich zu werden. Und in den Goldrahmen über dem Sofa sehen sie wie Geschwister aus. Aber dann stehen mit einem Male <pb n="509" xml:id="tg435.3.5.1"/>
ihre Hände vor den schwarzen Sonntagskleidern und verraten sie.</p>
                    <p rend="zenoPLm4n0" xml:id="tg435.3.6">Die eine, hart, krampfig, rücksichtslos, sagt: So ist das Leben. Die andere, blaß, bang, voll Zärtlichkeit, sagt: Sieben Kinder – oh! Und einmal ist der blonde Enkel dabei, hört die Hände und denkt: diese Hand ist wie der Vater, und meint die harte, narbige damit. Und vor der bleichen Hand fühlt er: wie die Mutter ist sie. Die Ähnlichkeit ist groß; und der Knabe weiß, daß die Eltern sich nicht gern so sehen mögen; deshalb kommen sie selten in den Salon. Sie passen in die Stuben, die voll sind von lautem Licht, und in den Wechsel der Tage, die bald rot von Tomaten, bald dumpf von Soda sind. Denn Das ist das Leben. Und es bleibt Alles in ihren Zügen hängen wie einst an den Händen der Großeltern. Ein paar Hände sind sie und nichts dahinter.</p>
                    <p rend="zenoPLm4n0" xml:id="tg435.3.7">Hinter der Glastür sind seltsame Gedanken. Die hohen, halbblinden Spiegel wiederholen immerfort, als müßten sie's auswendig lernen: der Großvater, die Großmutter. Und die Albums auf der gehäkelten Tischdecke sind voll davon: Großvater, Großmutter, Großvater, Großmutter. Natürlich stehen die steilen Stühle ehrfurchtsvoll herum: als ob sie einander eben erst vorgestellt wären und gerade die ersten Phrasen tauschten: »Sehr angenehm«, oder: »Sie gedenken, lange hier zu bleiben?« oder so etwas Höfliches. Und dann verstummen sie ganz, sagen gleichsam: »Bitte«, wenn die Spieluhr beginnt: »Tingilligin...« Und sie singt mit ihrer welken, winzigen Stimme ein Menuett. Das Lied bleibt eine Weile über den Dingen und sickert dann in die <pb n="510" xml:id="tg435.3.7.1"/>
vielen dunklen Spiegel hinein und ruht in ihnen wie Silber in Seen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg435.3.8">In einer Ecke steht der Enkel und ist wie von van Dyck. Er möchte so heißen, daß man seinen Namen zur Spieluhr singen könnte, denn er hat plötzlich das Gefühl: Kampf und Krankheit sind es nicht, auch nicht die Sorgen und das tägliche Brot und der Wäschetag und alles andere, was mit uns draußen in den engen Stuben wohnt. Das wirkliche Leben ist wie dieses »Tingilligin«... Es kann nehmen und schenken, kann dich Bettler rufen oder König und tief oder traurig machen je nachdem, – aber es kann nicht das Gesicht bang oder zornig verzerren und es kann auch – verzeih, Großpapa – es kann auch die Hände nicht hart und häßlich machen wie deine.</p>
                    <p rend="zenoPLm4n0" xml:id="tg435.3.9">Das war nur so ein breites, dunkles Gefühl in dem blonden Knaben. Wie ein Hintergrund, vor dem andere kleine Kindergedanken standen wie Bleisoldaten. Aber er empfand es doch, und vielleicht lebt ers einmal.</p>
                    <pb n="511" xml:id="tg435.3.10"/>
                </div>
            </div>
        </body>
    </text>
</TEI>