<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg441" n="/Literatur/M/Rilke, Rainer Maria/Erzählungen und Skizzen/Geschichten vom lieben Gott/Warum der liebe Gott will, daß es arme Leute giebt">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Warum der liebe Gott will, daß es arme Leute giebt</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Rilke, Rainer Maria: Element 00461 [2011/07/11 at 20:24:40]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Rainer Maria Rilke: Sämtliche Werke. Herausgegeben vom Rilke-Archiv in Verbindung mit Ruth Sieber-Rilke, besorgt von Ernst Zinn, Band 1–6, Wiesbaden und Frankfurt a.M.: Insel, 1955–1966.</title>
                        <author key="pnd:118601024">Rilke, Rainer Maria</author>
                    </titleStmt>
                    <extent>302-</extent>
                    <publicationStmt>
                        <date notBefore="1955" notAfter="1966"/>
                        <pubPlace>Frankfurt a.M.</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1875" notAfter="1926"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg441.2">
                <div type="h4">
                    <head type="h4" xml:id="tg441.2.1">Warum der liebe Gott will,</head>
                    <head type="h4" xml:id="tg441.2.2">daß es arme Leute giebt</head>
                    <p xml:id="tg441.2.3">Die vorangehende Geschichte hat sich so verbreitet, daß der Herr Lehrer mit sehr gekränktem Gesicht auf<pb n="302" xml:id="tg441.2.3.1"/>
 der Gasse herumgeht. Ich kann das begreifen. Es ist immer schlimm für einen Lehrer, wenn die Kinder plötzlich etwas wissen, was er ihnen nicht erzählt hat. Der Lehrer muß sozusagen das einzige Loch in der Planke sein, durch welches man in den Obstgarten sieht; sind noch andere Löcher da, so drängen sich die Kinder jeden Tag vor einem anderen und werden bald des Ausblicks überhaupt müde. Ich hätte diesen Vergleich nicht hier aufgezeichnet, denn nicht jeder Lehrer ist vielleicht damit einverstanden, ein Loch zu sein; aber der Lehrer, von dem ich rede, mein Nachbar, hat den Vergleich zuerst von mir vernommen und ihn sogar als äußerst treffend bezeichnet. Und sollte auch jemand anderer Meinung sein, die Autorität meines Nachbars ist mir maßgebend.</p>
                    <p rend="zenoPLm4n0" xml:id="tg441.2.4">Er stand vor mir, rückte beständig an seiner Brille und sagte: »Ich weiß nicht, wer den Kindern diese Geschichte erzählt hat, aber es ist jedenfalls unrecht, ihre Phantasie mit solchen ungewöhnlichen Vorstellungen zu überladen und anzuspannen. Es handelt sich um eine Art Märchen –« »Ich habe es zufällig er zählen hören«, unterbrach ich ihn. (Dabei log ich nicht, denn seit jenem Abend ist es mir wirklich schon von meiner Frau Nachbarin wiederberichtet worden.) »So«, machte der Lehrer; er fand das leicht erklärlich. »Nun, was sagen Sie dazu?« Ich zögerte, auch fuhr er sehr schnell fort: »Zunächst finde ich es unrecht, religiöse, besonders biblische Stoffe frei und eigenmächtig zu gebrauchen. Es ist das alles im Katechismus jedenfalls so ausgedrückt, daß es besser nicht gesagt werden kann...« Ich wollte <pb n="303" xml:id="tg441.2.4.1"/>
etwas bemerken, erinnerte mich aber im letzten Augenblick, daß der Herr Lehrer »zunächst« gebraucht hatte, daß also jetzt nach der Grammatik und um der Gesundheit des Satzes willen ein »dann« und vielleicht sogar ein »und endlich« folgen mußte, ehe ich mir erlauben durfte, etwas anzufügen. So geschah es auch. Ich will, da der Herr Lehrer diesen selben Satz, dessen tadelloser Bau jedem Kenner Freude bereiten wird, auch anderen übermittelt hat, die ihn ebensowenig wie ich vergessen dürften, hier nur noch das aufzeichnen, was hinter dem schönen, vorbereitenden Worte: »Und endlich« wie das Finale einer Ouverture kam. »Und endlich . . . (die sehr phantastische Auffassung hingehen lassend) erscheint mir der Stoff gar nicht einmal genügend durchdrungen und nach allen Seiten hin berücksichtigt zu sein. Wenn ich Zeit hätte, Geschichten zu schreiben –« »Sie vermissen etwas in der bewußten Erzählung?« konnte ich mich nicht enthalten ihn zu unterbrechen. »Ja, ich vermisse manches. Vom literarisch-kritischen Standpunkt gewissermaßen. Wenn ich zu Ihnen als Kollege sprechen darf –« Ich verstand nicht, was er meinte, und sagte bescheiden: »Sie sind zu gütig, aber ich habe nie eine Lehrertätigkeit...« Plötzlich fiel mir etwas ein, ich brach ab, und er fuhr etwas kühl fort: »Um nur eins zu nennen: es ist nicht anzunehmen, daß Gott (wenn man schon auf den Sinn der Geschichte soweit eingehen will), daß Gott, also – sage ich – daß Gott keinen weiteren Versuch gemacht haben sollte, einen Menschen zu sehen, wie er ist, ich meine –« Jetzt glaubte ich den Herrn Lehrer wieder <pb n="304" xml:id="tg441.2.4.2"/>
versöhnen zu müssen. Ich verneigte mich ein wenig und begann: »Es ist allgemein bekannt, daß Sie sich eingehend (und, wenn man so sagen darf, nicht ohne Gegenliebe zu finden) der sozialen Frage genähert haben.« Der Herr Lehrer lächelte. »Nun, dann darf ich annehmen, daß, was ich Ihnen im folgenden mitzuteilen gedenke, Ihrem Interesse nicht ganz ferne steht, zumal ich ja auch an Ihre letzte, sehr scharfsinnige Bemerkung anknüpfen kann.« Er sah mich erstaunt an: »Sollte Gott etwa . . .« »In der Tat,« bestätigte ich, »Gott ist eben dabei, einen neuen Versuch zu machen.« »Wirklich?,« fuhr mich der Lehrer an, »ist das an maßgebender Stelle bekannt geworden?« »Darüber kann ich Ihnen nichts Genaues sagen –« bedauerte ich – »ich bin nicht in Beziehung mit jenen Kreisen, aber wenn Sie dennoch meine kleine Geschichte hören wollen?« »Sie würden mir einen großen Gefallen erweisen.« Der Lehrer nahm seine Brille ab und putzte sorgfältig die Gläser, während seine nackten Augen sich schämten.</p>
                    <p rend="zenoPLm4n0" xml:id="tg441.2.5">Ich begann: »Einmal sah der liebe Gott in eine große Stadt. Als ihm von dem vielen Durcheinander die Augen ermüdeten (dazu trugen die Netze mit den elektrischen Drähten nicht wenig bei), beschloß er, seine Blicke auf ein einziges hohes Mietshaus für eine Weile zu beschränken, weil dieses weit weniger anstrengend war. Gleichzeitig erinnerte er sich seines alten Wunsches, einmal einen lebenden Menschen zu sehen, und zu diesem Zwecke tauchten seine Blicke ansteigend in die Fenster der einzelnen Stockwerke. Die Leute im ersten <pb n="305" xml:id="tg441.2.5.1"/>
Stockwerke (es war ein reicher Kaufmann mit Familie) waren fast nur Kleider. Nicht nur, daß alle Teile ihres Körpers mit kostbaren Stoffen bedeckt waren, die äußeren Umrisse dieser Kleidung zeigten an vielen Stellen eine solche Form, daß man sah, es konnte kein Körper mehr darunter sein. Im zweiten Stock war es nicht viel besser. Die Leute, welche drei Treppen wohnten, hatten zwar schon bedeutend weniger an, waren aber so schmutzig, daß der liebe Gott nur graue Furchen erkannte und in seiner Güte schon bereit war, zu befehlen, sie möchten fruchtbar werden. Endlich unter dem Dach, in einem schrägen Kämmerchen, fand der liebe Gott einen Mann in einem schlechten Rock, der sich damit beschäftigte, Lehm zu kneten. ›Oho, woher hast du das?‹ rief er ihn an. Der Mann nahm seine Pfeife gar nicht aus dem Munde und brummte: ›Der Teufel weiß woher. Ich wollte, ich wär Schuster geworden. Da sitzt man und plagt sich...‹ Und was der liebe Gott auch fragen mochte, der Mann war schlechter Laune und gab keine Antwort mehr. – Bis er eines Tages einen großen Brief vom Bürgermeister dieser Stadt bekam. Da erzählte er dem lieben Gott, ungefragt, alles. Er hatte so lange keinen Auftrag bekommen. Jetzt, plötzlich, sollte er eine Statue für den Stadtpark machen, und sie sollte heißen: die Wahrheit. Der Künstler arbeitete Tag und Nacht in einem entfernten Atelier, und dem lieben Gott kamen verschiedene alte Erinnerungen, wie er das so sah. Wenn er seinen Händen nicht immer noch böse gewesen wäre, er hätte wohl auch wieder irgendwas begonnen. – Als aber der Tag kam, <pb n="306" xml:id="tg441.2.5.2"/>
da die Bildsäule, welche die Wahrheit hieß, hinausgetragen werden sollte, auf ihren Platz in den Garten, wo auch Gott sie hätte sehen können in ihrer Vollendung, da entstand ein großer Skandal, denn eine Kommission von Stadtvätern, Lehrern und anderen einflußreichen Persönlichkeiten hatte verlangt, die Figur müsse erst teilweise bekleidet werden, ehe das Publikum sie zu Gesicht bekäme. Der liebe Gott verstand nicht weshalb, so laut fluchte der Künstler. Stadtväter, Lehrer und die anderen haben ihn in diese Sünde gebracht, und der liebe Gott wird gewiß an denen – Aber Sie husten ja fürchterlich!« »Es geht schon vorüber –« sagte mein Lehrer mit vollkommen klarer Stimme. »Nun, ich habe nur noch ein weniges zu berichten. Der liebe Gott ließ das Mietshaus und den Stadtpark los und wollte seinen Blick schon ganz zurückziehen, wie man eine Angelrute aus dem Wasser zieht, mit einem Schwung, um zu sehen, ob nicht etwas angebissen hat. In diesem Falle hing wirklich etwas daran. Ein ganz kleines Häuschen mit mehreren Menschen drinnen, die alle sehr wenig anhatten, denn sie waren sehr arm. ›Das also ist es –,‹ dachte der liebe Gott, ›arm müssen die Menschen sein. Diese hier sind, glaub ich, schon recht arm, aber ich will sie so arm machen, daß sie nicht einmal ein Hemd zum Anziehen haben.‹ So nahm sich der liebe Gott vor.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg441.2.6">Hier machte ich beim Sprechen einen Punkt, um anzudeuten, daß ich am Ende sei. Der Herr Lehrer war damit nicht zufrieden; er fand diese Geschichte ebensowenig abgeschlossen und gerundet, wie die vorhergehende. <pb n="307" xml:id="tg441.2.6.1"/>
»Ja« – entschuldigte ich mich – »da müßte eben ein Dichter kommen, der zu dieser Geschichte irgend einen phantastischen Schluß erfindet, denn tatsächlich hat sie noch kein Ende.« »Wieso?« machte der Herr Lehrer und schaute mich gespannt an. »Aber, lieber Herr Lehrer,« erinnerte ich, »wie vergeßlich Sie sind ! Sie sind doch selbst im Vorstand des hiesigen Armenvereins...« »Ja, seit etwa zehn Jahren bin ich das und –?« »Das ist es eben; Sie und Ihr Verein verhindern den lieben Gott die längste Zeit, sein Ziel zu erreichen. Sie kleiden die Leute –« »Aber ich bitte Sie,« sagte der Lehrer bescheiden, »das ist einfach Nächstenliebe. Das ist doch Gott im höchsten Grade wohlgefällig.« »Ach, davon ist man maßgebenden Orts wohl überzeugt?« fragte ich arglos. »Natürlich ist man das. Ich habe gerade in meiner Eigenschaft als Vorstandsmitglied des Armenvereins manches Lobende zu hören bekommen. Vertraulich gesagt, man will auch bei der nächsten Beförderung meine Tätigkeit in dieser Weise – – – Sie verstehen?« Der Herr Lehrer errötete schamhaft. »Ich wünsche Ihnen das Beste«, entgegnete ich. Wir reichten uns die Hände, und der Herr Lehrer ging mit so stolzen, gemessenen Schritten fort, daß ich überzeugt bin: er ist zu spät in die Schule gekommen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg441.2.7">Wie ich später vernahm, ist ein Teil dieser Geschichte (soweit sie für Kinder paßt) den Kindern doch bekannt geworden. Sollte der Herr Lehrer sie zu Ende gedichtet haben?</p>
                    <pb n="308" xml:id="tg441.2.8"/>
                </div>
            </div>
        </body>
    </text>
</TEI>