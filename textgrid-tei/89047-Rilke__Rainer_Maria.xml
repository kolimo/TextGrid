<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg442" n="/Literatur/M/Rilke, Rainer Maria/Erzählungen und Skizzen/Geschichten vom lieben Gott/Wie der Verrat nach Rußland kam">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Wie der Verrat nach Rußland kam</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Rilke, Rainer Maria: Element 00462 [2011/07/11 at 20:24:40]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Rainer Maria Rilke: Sämtliche Werke. Herausgegeben vom Rilke-Archiv in Verbindung mit Ruth Sieber-Rilke, besorgt von Ernst Zinn, Band 1–6, Wiesbaden und Frankfurt a.M.: Insel, 1955–1966.</title>
                        <author key="pnd:118601024">Rilke, Rainer Maria</author>
                    </titleStmt>
                    <extent>309-</extent>
                    <publicationStmt>
                        <date notBefore="1955" notAfter="1966"/>
                        <pubPlace>Frankfurt a.M.</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1875" notAfter="1926"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg442.2">
                <div type="h4">
                    <head type="h4" xml:id="tg442.2.1">Wie der Verrat nach Rußland kam</head>
                    <p xml:id="tg442.2.2">Ich habe noch einen Freund hier in der Nachbarschaft. Das ist ein blonder, lahmer Mann, der seinen Stuhl, winters wie sommers, hart am Fenster hat. Er kann sehr jung aussehen, ja in seinem lauschenden Gesicht ist manchmal etwas Knabenhaftes. Aber es giebt auch Tage, da er altert, die Minuten gehen wie Jahre über ihn, und plötzlich ist er ein Greis, dessen matte Augen das Leben fast schon losgelassen haben. Wir kennen uns lang. Erst haben wir uns immer angesehen, später lächelten wir unwillkürlich, ein Jahr lang grüßten wir einander, und seit Gott weiß wann erzählen wir uns das Eine und das Andere, wahllos, wie es eben passiert. »Guten Tag,« rief er, als ich vorüberkam und sein Fenster war noch offen in den reichen und stillen Herbst hinaus. »Ich habe Sie lange nicht gesehen.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg442.2.3">»Guten Tag, Ewald –.« Ich trat an sein Fenster, wie ich immer zu tun pflegte, im Vorübergehen. »Ich war verreist.« »Wo waren Sie?« fragte er mit ungeduldigen Augen. »In Rußland.« »Oh so weit –« er lehnte sich zurück, und dann: »Was ist das für ein Land, Rußland? Ein sehr großes, nicht wahr?« »Ja,« sagte ich, »groß ist es und außerdem –« »Habe ich dumm gefragt?« lächelte Ewald und wurde rot. »Nein, Ewald, im Gegenteil. Da Sie fragen: was ist das für ein Land? wird mir verschiedenes klar. Zum Beispiel woran Rußland grenzt.« »Im Osten?« warf mein Freund ein. Ich dachte nach: »Nein.« »Im Norden?« forschte der Lahme. »Sehen Sie,« fiel mir ein, »das Ablesen von <pb n="309" xml:id="tg442.2.3.1"/>
der Landkarte hat die Leute verdorben. Dort ist alles plan und eben, und wenn sie die vier Weltgegenden bezeichnet haben, scheint ihnen alles getan. Ein Land ist doch aber kein Atlas. Es hat Berge und Abgründe. Es muß doch auch oben und unten an etwas stoßen.« »Hm –« überlegte mein Freund, »Sie haben recht. Woran könnte Rußland an diesen beiden Seiten grenzen?« Plötzlich sah der Kranke wie ein Knabe aus. »Sie wissen es,« rief ich. »Vielleicht an Gott?« »Ja,« bestätigte ich, »an Gott.« »So« – nickte mein Freund ganz verständnisvoll. Erst dann kamen ihm einzelne Zweifel: »Ist denn Gott ein Land?« »Ich glaube nicht,« erwiderte ich, »aber in den primitiven Sprachen haben viele Dinge denselben Namen. Es ist da wohl ein Reich, das heißt Gott, und der es beherrscht, heißt auch Gott. Einfache Völker können ihr Land und ihren Kaiser oft nicht unterscheiden; beide sind groß und gütig, furchtbar und groß.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg442.2.4">»Ich verstehe«, sagte langsam der Mann am Fenster. »Und merkt man in Rußland diese Nachbarschaft?« »Man merkt sie bei allen Gelegenheiten. Der Einfluß Gottes ist sehr mächtig. Wie viel man auch aus Europa bringen mag, die Dinge aus dem Westen sind Steine, sobald sie über die Grenze sind. Mitunter kostbare Steine, aber eben nur für die Reichen, die sogenannten ›Gebildeten‹, während von drüben aus dem anderen Reich das Brot kommt, wovon das Volk lebt.« »Das hat das Volk wohl in Überfluß?« Ich zögerte: »Nein, das ist nicht der Fall, die Einfuhr aus Gott ist durch gewisse Umstände erschwert –« Ich suchte ihn von diesem Gedanken <pb n="310" xml:id="tg442.2.4.1"/>
abzubringen. »Aber man hat vieles aus den Gebräuchen jener breiten Nachbarschaft angenommen. Das ganze Zeremoniell beispielsweise. Man spricht zu dem Zaren ähnlich wie zu Gott.« »So, man sagt also nicht: Majestät?« »Nein, man nennt beide Väterchen.« »Und man kniet vor beiden?« »Man wirft sich vor beiden nieder, fühlt mit der Stirn den Boden und weint und sagt: ›Ich bin sündig, verzeih mir, Väterchen.‹ Die Deutschen, welche das sehen, behaupten: eine ganz unwürdige Sklaverei. Ich denke anders darüber. Was soll das Knien bedeuten? Es hat den Sinn zu erklären: Ich habe Ehrfurcht. Dazu genügt es auch, das Haupt zu entblößen, meint der Deutsche. Nun ja, der Gruß, die Verbeugung, gewissermaßen sind auch sie Ausdrücke dafür, Abkürzungen, die entstanden sind in den Ländern, wo nicht soviel Raum war, daß jeder sich hätte niederlegen können auf der Erde. Aber Abkürzungen gebraucht man bald mechanisch und ohne sich ihres Sinnes mehr bewußt zu werden. Deshalb ist es gut, wo noch Raum und Zeit dafür ist, die Gebärde auszuschreiben, das ganze schöne und wichtige Wort: Ehrfurcht.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg442.2.5">»Ja, wenn ich könnte, würde ich auch niederknien –«, träumte der Lahme. »Aber es kommt« – fuhr ich nach einer Pause fort – »in Rußland auch vieles andere von Gott. Man hat das Gefühl, jedes Neue wird von ihm eingeführt, jedes Kleid, jede Speise, jede Tugend und sogar jede Sünde muß erst von ihm bewilligt werden, ehe sie in Gebrauch kommt.« Der Kranke sah mich fast erschrocken an. »Es ist nur ein Märchen, auf welches <pb n="311" xml:id="tg442.2.5.1"/>
ich mich berufe,« eilte ich ihn zu beruhigen, »eine sogenannte Bylina, ein Gewesenes zu deutsch. Ich will Ihnen kurz den Inhalt erzählen. Der Titel ist: ›Wie der Verrat nach Rußland kam‹. « Ich lehnte mich ans Fenster, und der Gelähmte schloß die Augen, wie er gerne tat, wenn irgendwo eine Geschichte begann.</p>
                    <p rend="zenoPLm4n0" xml:id="tg442.2.6">»Der schreckliche Zar Iwan wollte den benachbarten Fürsten Tribut auferlegen und drohte ihnen mit einem großen Krieg, falls sie nicht Gold nach Moskau, in die weiße Stadt, schicken würden. Die Fürsten sagten, nachdem sie Rat gepflogen hatten, wie ein Mann: Wir geben dir drei Rätselfragen auf. Komm an dem Tage, den wir dir bestimmen, in den Orient, zu dem weißen Stein, wo wir versammelt sein werden, und sage uns die drei Lösungen. Sobald sie richtig sind, geben wir dir die zwölf Tonnen Goldes, die du von uns verlangst. Zuerst dachte der Zar Iwan Wassiljewitsch nach, aber es störten ihn die vielen Glocken seiner weißen Stadt Moskau. Da rief er seine Gelehrten und Räte vor sich, und jeden, der die Fragen nicht beantworten konnte, ließ er auf den großen, roten Platz führen, wo gerade die Kirche für Wassilij, den Nackten, gebaut wurde, und einfach köpfen. Bei einer solchen Beschäftigung verging ihm die Zeit so rasch, daß er sich plötzlich auf der Reise fand nach dem Orient, zu dem weißen Stein, bei welchem die Fürsten warteten. Er wußte auf keine der drei Fragen etwas zu erwidern, aber der Ritt war lang, und es war immer noch die Möglichkeit, einem Weisen zu begegnen; denn damals waren viele Weise unterwegs auf der Flucht, da alle Könige die Gewohnheit <pb n="312" xml:id="tg442.2.6.1"/>
hatten, ihnen den Kopf abschneiden zu lassen, wenn sie ihnen nicht weise genug schienen. Ein solcher kam ihm nun allerdings nicht zu Gesicht, aber an einem Morgen sah er einen alten, bärtigen Bauer, welcher an einer Kirche baute. Er war schon dabei angelangt, den Dachstuhl zu zimmern und die kleinen Latten darüberzulegen. Da war es nun recht verwunderlich, daß der alte Bauer immer wieder von der Kirche herunterstieg, um von den schmalen Latten, welche unten aufgeschichtet waren, jede einzeln zu holen, statt viele auf einmal in seinem langen Kaftan mitzunehmen. Er mußte so beständig auf- und niederklettern, und es war garnicht abzusehen, daß er auf diese Weise überhaupt jemals alle vielhundert Latten an ihren Ort bringen würde. Der Zar wurde deshalb ungeduldig: ›Dummkopf‹, schrie er (so nennt man in Rußland meistens die Bauern), ›du solltest dich tüchtig beladen mit deinem Holz und dann auf die Kirche kriechen, das wäre bei weitem einfacher.‹ Der Bauer, der gerade unten war, blieb stehen, hielt die Hand über die Augen und antwortete: ›Das mußt du schon mir überlassen, Zar Iwan Wassiljewitsch, jeder versteht sein Handwerk am besten; indessen, weil du schon hier vorüberreitest, will ich dir die Lösung der drei Rätsel sagen, welche du am weißen Stein im Orient, gar nicht weit von hier, wirst wissen müssen.‹ Und er schärfte ihm die drei Antworten der Reihe nach ein. Der Zar konnte vor Erstaunen kaum dazu kommen, zu danken. ›Was soll ich dir geben zum Lohne?‹ fragte er endlich. ›Nichts‹ , machte der Bauer, holte eine Latte und wollte <pb n="313" xml:id="tg442.2.6.2"/>
auf die Leiter steigen. ›Halt,‹ befahl der Zar, ›das geht nicht an, du mußt dir etwas wünschen.‹ ›Nun, Väterchen, wenn du befiehlst, gieb mir eine von den zwölf Tonnen Goldes, welche du von den Fürsten im Orient erhalten wirst.‹ ›Gut –,‹ nickte der Zar. ›Ich gebe dir eine Tonne Goldes.‹ Dann ritt er eilends davon, um die Lösungen nicht wieder zu vergessen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg442.2.7">Später, als der Zar mit den zwölf Tonnen zurückgekommen war aus dem Orient, schloß er sich in Moskau in seinen Palast, mitten im fünftorigen Kreml ein und schüttete eine Tonne nach der anderen auf die glänzenden Dielen des Saales aus, so daß ein wahrer Berg aus Gold entstand, der einen großen schwarzen Schatten über den Boden warf. In Vergeßlichkeit hatte der Zar auch die zwölfte Tonne ausgeleert. Er wollte sie wieder füllen, aber es tat ihm leid, soviel Gold von dem herrlichen Haufen wieder fortnehmen zu müssen. In der Nacht ging er in den Hof hinunter, schöpfte feinen Sand in die Tonne, bis sie zu drei Vierteilen voll war, kehrte leise in seinen Palast zurück, legte Gold über den Sand und schickte die Tonne mit dem nächsten Morgen durch einen Boten in die Gegend des weiten Rußland, wo der alte Bauer seine Kirche baute. Als dieser den Boten kommen sah, stieg er von dem Dach, welches noch lange nicht fertig war, und rief: ›Du mußt nicht näher kommen, mein Freund, reise zurück samt deiner Tonne, welche drei Vierteile Sand und ein knappes Viertel Gold enthält; ich brauche sie nicht. Sage deinem Herrn, bisher hat es keinen Verrat in Rußland gegeben. Er aber ist selbst daran schuld, wenn <pb n="314" xml:id="tg442.2.7.1"/>
er bemerken sollte, daß er sich auf keinen Menschen verlassen kann; denn er hat nunmehr gezeigt, wie man verrät, und von Jahrhundert zu Jahrhundert wird sein Beispiel in ganz Rußland viele Nachahmer finden. Ich brauche nicht das Gold, ich kann ohne Gold leben. Ich erwartete nicht Gold von ihm, sondern Wahrheit und Rechtlichkeit. Er aber hat mich getäuscht. Sage das deinem Herrn, dem schrecklichen Zaren Iwan Wassiljewitsch, der in seiner weißen Stadt Moskau sitzt mit seinem bösen Gewissen und in einem goldenen Kleid.‹</p>
                    <p rend="zenoPLm4n0" xml:id="tg442.2.8">Nach einer Weile Reitens wandte sich der Bote nochmals um: der Bauer und seine Kirche waren verschwunden. Und auch die aufgeschichteten Latten lagen nicht mehr da, es war alles leeres, flaches Land. Da jagte der Mann entsetzt zurück nach Moskau, stand atemlos vor dem Zaren und erzählte ihm ziemlich unverständlich, was sich begeben hatte, und daß der vermeintliche Bauer niemand anderes gewesen sei, als Gott selbst.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg442.2.9">»Ob er wohl recht gehabt hat damit?« meinte mein Freund leise, nachdem meine Geschichte verklungen war.</p>
                    <p rend="zenoPLm4n0" xml:id="tg442.2.10">»Vielleicht –,« entgegnete ich, »aber, wissen Sie, das Volk ist – abergläubisch – indessen, ich muß jetzt gehen, Ewald.« »Schade,« sagte der Lahme aufrichtig. »Wollen Sie mir nicht bald wieder eine Geschichte erzählen?« »Gerne –, aber unter einer Bedingung.« Ich trat noch einmal ans Fenster heran. »Nämlich?« staunte Ewald. »Sie müssen alles gelegentlich den Kindern in der Nachbarschaft weitererzählen«, bat ich. »Oh, die <pb n="315" xml:id="tg442.2.10.1"/>
Kinder kommen jetzt so selten zu mir.« Ich vertröstete ihn: »Sie werden schon kommen. Offenbar haben Sie in der letzten Zeit nicht Lust gehabt, ihnen etwas zu erzählen, und vielleicht auch keinen Stoff, oder zu viel Stoffe. Aber wenn einer eine wirkliche Geschichte weiß, glauben Sie, das kann verborgen bleiben? Bewahre, das spricht sich herum, besonders unter den Kindern!« »Auf Wiedersehen.« Damit ging ich.</p>
                    <p rend="zenoPLm4n0" xml:id="tg442.2.11">Und die Kinder haben die Geschichte noch an demselben Tage gehört.</p>
                </div>
            </div>
        </body>
    </text>
</TEI>