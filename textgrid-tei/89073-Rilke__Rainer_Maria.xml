<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg479" n="/Literatur/M/Rilke, Rainer Maria/Theoretische Schriften/[Aufsätze und Rezensionen]/Friedrich Huch, Peter Michel [Zweite Besprechung]">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Friedrich Huch, Peter Michel [Zweite Besprechung]</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Rilke, Rainer Maria: Element 00490 [2011/07/11 at 20:24:40]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <notesStmt>
                <note>
                                 Erstdruck in: Die Zukunft (Berlin), 10. Jg., 1902.
                              </note>
            </notesStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Rainer Maria Rilke: Sämtliche Werke. Herausgegeben vom Rilke-Archiv in Verbindung mit Ruth Sieber-Rilke, besorgt von Ernst Zinn, Band 1–6, Wiesbaden und Frankfurt a.M.: Insel, 1955–1966.</title>
                        <author key="pnd:118601024">Rilke, Rainer Maria</author>
                    </titleStmt>
                    <extent>469-</extent>
                    <publicationStmt>
                        <date notBefore="1955" notAfter="1966"/>
                        <pubPlace>Frankfurt a.M.</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1875" notAfter="1926"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg479.3">
                <div type="h4">
                    <head type="h4" xml:id="tg479.3.1">
                        <pb n="469" xml:id="tg479.3.1.1"/>
                        <pb type="start" n="509" xml:id="tg479.3.1.2"/>Friedrich Huch, Peter Michel</head>
                    <milestone unit="head_start"/>
                    <p rend="zenoPC" xml:id="tg479.3.2">
                        <hi rend="italic" xml:id="tg479.3.2.1">[Zweite Besprechung]</hi>
                    </p>
                    <milestone unit="head_end"/>
                    <lb xml:id="tg479.3.3"/>
                    <p xml:id="tg479.3.4">Wenn man die Romane, die in den letzten drei oder vier Jahren erschienen sind, heute vorurteilslos betrachtet, so erscheinen die besten unter ihnen als Vorläufer und Verkünder irgend eines kommenden Werkes. Sie sind alle einseitig, sowohl die realistischen wie die romantischen und diejenigen, welche man die psychologischen genannt hat, und gerade diese Einseitigkeit macht sie interessant und lesenswert, diese bewußte, mehr oder minder geistvolle Übertreibung nach einer Seite hin, nach einer bestimmten neuen Seite hin, von der man jetzt mehr zu wissen glaubte oder mehr wußte als früher, in der Zeit größerer Dichter. Zu einem einheitlichen, zusammenfassenden Kunstwerk schien alles zu fehlen: die Kraft, die Zeit und die Unbefangenheit. Und nun ist dieses Kunstwerk, dessen Erscheinen auch die Optimisten unter den ernsteren Kritikern in unbestimmte Zukunft verlegten, da, ist unter uns, und je der kann es befühlen und sehen, daß es wirklich und am nächsten Morgen nicht verschwunden ist, sondern an dem Platze liegt, wo er es verließ, als er sich in tiefster Nacht schwer und in seltsamer Erregung davon trennte. Ich glaube nicht, daß dieses Buch an einem von denen, die es in die Hand nehmen, spurlos vorüber geht. Es redet jeden an, obwohl es sich an keinen wendet, und läßt keinen mehr los, obwohl es ihn gleichsam nur mit dem kleinen Finger halt, mit irgend einem einfachen Satz, mit irgend einer Unaussprechlichkeit, die <pb n="509" xml:id="tg479.3.4.1"/>
ausgesprochen ist, mit irgend einer Überraschung, die so selbstverständlich vor sich geht wie alles in diesem Buche, in dem nur Selbstverständliches geschieht. Wie Zufälle stehen die Ereignisse neben einander und die Menschen gehen durch sie durch, selbst wie Zufälle, von einander getrennt, wie eben ein Zufall vom anderen getrennt ist, allein, wie Kinder allein sind unter Erwachsenen, traurig wie Träumer und empfindlich wie Schlaflose, – und das Leben, das Leben rinnt ihnen durch die Finger wie Sand und wächst wie ein Sandberg vor ihnen auf, immer höher und höher, bis sie schließlich dahinter verloren gehen. Von solcher Art ist die Tragik dieses Buches, die mir mehr zu sein scheint als die Tragik einer bestimmten Zeit, während das viele Komische, von dem das Buch erfüllt ist, an der Zeit zu hängen und aus ihren Kleinheiten aufzuwachsen scheint. Denn es ist viel Anlaß zum Lachen und viel Grund zum Weinen und zum Nachdenken in diesem Buch, wie im Leben zu alledem täglich Anlässe sind; nur werden sie uns durch diesen Roman so gebieterisch auferlegt, daß wir sie ausnützen müssen, während sie im Leben an unserer Trägheit oder Zerstreutheit so oft vorübergehen. Das Buch heißt »Peter Michael«. In seinen ersten elf Kapiteln erfahren wir die Geschichte Peters von seiner Kindheit bis zu seiner Verheiratung. Das zwölfte und letzte Kapitel zeigt uns Peter zu einer Zeit, wo er von sich selbst, von dem Peter der elf Kapitel, nur sehr wenig mehr weiß: er hat zwei Kinder und Ernestine Treuthaler ist eine brave Hausfrau. Der Sandberg vor ihm ist ganz groß geworden, <pb n="510" xml:id="tg479.3.4.2"/>
so groß, daß er nicht mehr darüber weg sehen kann; aber vorher, in dem größeren Teil des Buches, sehen wir diesen Zufall Peter als die Ursache von glücklichen und unglücklichen Stunden, als einen Anlaß zu manchen Veränderungen sich auf dem kleinen Stück Welt bewegen, das er in Aufregung bringt und beschwichtigt und das auf ihn zurückwirkt, wie Masse auf Masse wirkt, mit seinen tausend Gesetzen und Zufälligkeiten und mit seinen Menschen, die alt werden und eingehen und sich bescheiden. Und obwohl allen Gestalten dieses Buches gemeinsam ist, daß sie alt werden und eingehen und sich bescheiden, ist doch gar nichts Einseitiges in diesem Buch; im Gegenteil: wollte man das Bezeichnende seiner Art in Kürze feststellen, so müßte man sich entschließen, zu sagen, daß Alles in diesem Buch ist von der Katastrophe bis zum Aperçu und von der breiten Komik, die absichtlich banal und derb wirkt, bis zu jenen feinsten und leisesten Ereignissen, Freuden und Enttäuschungen, Entfremdenden und Harmonien, bei deren Eintreten die Sprache machtlos bleibt und der Zeiger der Worte keinen Ausschlagswinkel mehr aufweist. Ich habe nie für möglich gehalten, daß Dinge, Stimmungen. Übergänge, wie dieses Buch sie in reicher Menge enthält, ausdrückbar sind, es sei denn, daß man das schwer ausdrückbare Motiv zur Hauptsache macht, eine Skizze, eine Novelle, ein Gedicht dafür schreibt, also einen ganzen Apparat von Hilfsmitteln in Bewegung setzt, um ihm beizukommen. Davon ist aber hier gar nicht die Rede. Als ob es das Allereinfachste wäre, spricht dieses Buch von ganz leisen <pb n="511" xml:id="tg479.3.4.3"/>
Vorgängen, Zusammenhängen und Anklängen in seinen kurzen Sätzen, die lauter Tatsachen zu enthalten scheinen. Auf allem ruht die gleiche Betonung; mit Recht: denn alles ist wichtig in diesem Buch und, trotzdem alles zufällig scheint, voll Gesetzmäßigkeit. Eins hält das andere im Gleichgewicht und die Erregung seiner bewegten Momente scheint über dem Ganzen wirksam zu sein, ebenso wie die Wehmut seiner traurigen Stellen über alle dreihundertfünfzig Seiten sich wie Mondlicht auszugießen scheint. Und da drängt sich denn ungestüm die Frage nach dem Künstler auf, nach dem Zusammenfasser und Ordner und Gesetzgeber. Ich weiß nichts von ihm. Er heißt: Friedrich Huch.</p>
                </div>
            </div>
        </body>
    </text>
</TEI>