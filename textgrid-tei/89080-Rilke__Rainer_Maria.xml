<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg486" n="/Literatur/M/Rilke, Rainer Maria/Theoretische Schriften/[Aufsätze und Rezensionen]/Samskola">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Samskola</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Rilke, Rainer Maria: Element 00497 [2011/07/11 at 20:24:40]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <notesStmt>
                <note>
                                 Erstdruck in: Die Zukunft (Berlin), 13. Jg., 1905.
                              </note>
            </notesStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Rainer Maria Rilke: Sämtliche Werke. Herausgegeben vom Rilke-Archiv in Verbindung mit Ruth Sieber-Rilke, besorgt von Ernst Zinn, Band 1–6, Wiesbaden und Frankfurt a.M.: Insel, 1955–1966.</title>
                        <author key="pnd:118601024">Rilke, Rainer Maria</author>
                    </titleStmt>
                    <extent>636-</extent>
                    <publicationStmt>
                        <date notBefore="1955" notAfter="1966"/>
                        <pubPlace>Frankfurt a.M.</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1875" notAfter="1926"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg486.3">
                <div type="h4">
                    <head type="h4" xml:id="tg486.3.1">
                        <pb n="636" xml:id="tg486.3.1.1"/>
                        <pb type="start" n="672" xml:id="tg486.3.1.2"/>Samskola</head>
                    <p xml:id="tg486.3.2">Ich werde erzählen, was sich neulich in Gothenburg begeben hat. Es ist merkwürdig genug. Es geschah in dieser Stadt, daß mehrere Kinder zu ihren Eltern kamen und erklärten, sie wollten auch nachmittags in der Schule bleiben, auch wenn kein Unterricht ist, immer. Immer? Ja, so viel wie möglich. In welcher Schule?</p>
                    <p rend="zenoPLm4n0" xml:id="tg486.3.3">Ich werde von dieser Schule erzählen. Es ist eine ungewöhnliche, eine völlig unimperativische Schule; eine Schule, die nachgiebt, eine Schule, die sich nicht für fertig halt, sondern für etwas Werdendes, daran die Kinder selbst, umformend und bestimmend, arbeiten sollen. Die Kinder, in enger und freundlicher Beziehung mit einigen aufmerksamen, lernenden, vorsichtigen Erwachsenen, Menschen, Lehrern, wenn man will. Die Kinder sind in dieser Schule die Hauptsache. Man begreift, daß damit verschiedene Einrichtungen fortfallen, die an anderen Schulen üblich sind. Zum Beispiel: jene hochnotpeinlichen Untersuchungen und Verhöre, die man Prüfungen genannt hat, und die damit zusammenhängenden Zeugnisse. Sie waren ganz und gar eine Erfindung der Großen. Und man fühlt gleich, wenn man die Schule betritt, den Unterschied. Man ist in einer Schule, in der es nicht nach Staub, Tinte und Angst riecht, sondern nach Sonne, blondem Holz und Kindheit.</p>
                    <p rend="zenoPLm4n0" xml:id="tg486.3.4">Man wird sagen, daß eine solche Schule sich nicht halten kann. Nein, natürlich. Aber die Kinder halten sie. <pb n="672" xml:id="tg486.3.4.1"/>
Sie besteht nun im vierten Jahre und man zählt in diesem Semester zweihundertfünfzehn Schüler, Mädchen und Knaben aus allen Altern. Denn es ist eine richtige Schule, die beim Anfang anfängt und bis ans Ende reicht. Freilich: dieses Ende liegt noch nicht ganz in ihrer Hand. An diesem Ausgang der Achtzehnjährigen steht, gespenstisch wie ein Revenant, die Reifeprüfung. Und sie treten, aus der Zukunft, in der sie schon waren, in eine andere Zeit zurück. In die Zeit ihrer Zeitgenossen. Aber sie sind doch, sozusagen, im Kommenden erzogen; werden sie das ganz verleugnen? Wird man es später an ihrem Leben merken?</p>
                    <p rend="zenoPLm4n0" xml:id="tg486.3.5">Für alle, die jetzt und in den nächsten Jahren die Schule verlassen, trifft das noch nicht ganz zu; denn sie sind (da die Schule erst ihr viertes Jahr beginnt) nicht von Anfang an ihre Schüler gewesen. Sie sind eines Tages übergetreten, mit Schulerfahrungen und – konventionen behaftet und ganz voll von den Bazillen alter, verschleppter Schulseuchen. Wäre der junge Körper dieser neuen Schule nicht so durch und durch gesund, so hatten sie leicht eine Gefahr für ihn werden können. So aber gehen sie, ohne Schaden zu stiften, durch seinen Organismus durch; ihre schlechten Gebräuche und Schülerheimlichkeiten, die sie fortsetzen, bekommen, inmitten des weiten, offenen Vertrauens, inmitten dieser lebensgroßen Menschlichkeit, die weit über die Wände einer Schulstunde hinausreicht, einen Anschein von trauriger, harmloser Lächerlichkeit; sie werden so überflüssig wie die umwickelten Gebärden eines Freigelassenen, der fortfahrt, in der Zeichen-<pb n="673" xml:id="tg486.3.5.1"/>
 und Klopfsprache des Gefängnisses sich auszudrücken. Aber wenn diese einmal scheu Gemachten auch nicht fähig sind, sich in der Sonne der neuen Schule ganz arglos auszubreiten, so merkt man doch, wie sie sich erholen, wie sie sich aufrichten und, bei aller Frühreife ihrer trüben Erfahrung, reine, kindhaft lichte Triebe ansetzen und da und dort zum Blühen kommen. Aber man muß vorsichtig mit ihnen sein; denn die Freiheit ist eine Gefahr für sie.</p>
                    <p rend="zenoPLm4n0" xml:id="tg486.3.6">Das Wort Freiheit ist genannt. Es scheint mir, als ob wir, die Erwachsenen, in einer Welt lebten, in der keine Freiheit ist. Freiheit ist bewegtes, steigendes, mit der Menschenseele sich wandelndes, wachsendes Gesetz. Unsere Gesetze sind nicht mehr die unserigen. Sie sind zurückgeblieben, während das Leben lief. Man hat sie zurückgehalten, aus Geiz, aus Habgier, aus Eigennutz; aber vor allem: aus Angst. Man wollte sie nicht mit auf den Wellen haben in Sturm und Schiffbruch; sie sollten in Sicherheit sein. Und da man sie so, gerettet aus aller Gefahr, auf dem Strande zurückließ, sind sie erstarrt. Und das ist unsere Not: daß wir Gesetze haben aus Stein. Gesetze, die nicht immer mit uns waren, fremde, unverwandte Gesetze. Keine von den tausend neuen Bewegungen unseres Blutes pflanzt sich in ihnen fort; unser Leben besteht nicht für sie; und die Wärme aller Herzen reicht nicht aus, einen Schimmer von Grün auf ihren kalten Oberflächen hervorzurufen. Wir schreien nach dem neuen Gesetz. Nach einem Gesetz, das Tag und Nacht bei uns bleibt und das wir erkannt und befruchtet haben wie ein Weib.</p>
                    <p rend="zenoPLm4n0" xml:id="tg486.3.7">
                        <pb n="674" xml:id="tg486.3.7.1"/>
Aber es kommt keiner, der solches Gesetz uns geben kann; es ist über die Kraft.</p>
                    <p rend="zenoPLm4n0" xml:id="tg486.3.8">Aber denkt niemand daran, daß das neue Gesetz, das wir nicht zu schaffen vermögen, täglich anfangen kann mit denen, die wieder ein Anfang sind? Sind sie nicht wieder das Ganze, Schöpfung und Welt, wachsen nicht in ihnen alle Kräfte heran, wenn wir nur Raum geben? Wenn wir nicht aufdringlich, mit dem Recht des Stärkeren, den Kindern all das Fertige in den Weg stellen, das für unser Leben gilt, wenn sie nichts vorfinden, wenn sie alles machen müssen: werden sie nicht alles machen? Wenn wir uns hüten, den alten Riß zwischen Pflicht und Freude (Schule und Leben), Gesetz und Freiheit in sie hinein zu vergrößern: ist es nicht möglich, daß die Welt heil in ihnen heranwächst? Nicht in einer Generation freilich, nicht in der nächsten und übernächsten, aber langsam, von Kindheit zu Kindheit heilend?</p>
                    <p rend="zenoPLm4n0" xml:id="tg486.3.9">Ich weiß nicht, ob man zu dem Ursprung der Schule auch durch diese Gedanken gegangen ist; es ist eine Welt von Gedanken gedacht worden. Aber nun ist sie da. Ihre einfache Heiterkeit spielt vor einem Hintergrunde dunkelsten Ernstes. Sie ist nicht in ein Programm eingeschlossen, sie ist nach allen Seiten offen. Und es ist gar nicht vom »Erziehen« die Rede. Es handelt sich gar nicht darum. Denn wer kann erziehen? Wo ist der unter uns, der erziehen dürfte?</p>
                    <p rend="zenoPLm4n0" xml:id="tg486.3.10">Was diese Schule versucht, ist dieses: nichts zu stören. Aber indem sie dies auf ihre tätige und hingebende Weise versucht, indem sie Hemmungen entfernt, Fragen <pb n="675" xml:id="tg486.3.10.1"/>
anregt, horcht, beobachtet, lernt und vorsichtig liebt, – tut sie alles, was Erwachsene an denen tun können, die nach ihnen kommen sollen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg486.3.11">Das fünfteilige hölzerne Gebäude eines früheren Hospitals. An Kranke denkt man nicht mehr; nur etwas wie die Freude von vielen Genesenden ist darin geblieben.</p>
                    <p rend="zenoPLm4n0" xml:id="tg486.3.12">Die Zimmer sind wie die Zimmer in einem Landhaus. Mittelgroß, mit klaren, einfarbigen Wänden und geräumigen Fenstern, in denen viele Blumen stehen. Die niedrigen, gelben, harzhellen Tische lassen sich, wenn es nötig ist, in der Art von Schulbanken anreihen; meist aber sind sie in der Mitte zu einem einzigen großen Tisch zusammengeschoben, wie in einer Wohnstube. Und die kleinen, behaglichen Sessel stehen rund herum. Natürlich ist alles da, was in ein richtiges Schulzimmer gehört: ein (übrigens nicht erhöhter) Lehrertisch, eine Tafel und alles andere. Aber diese Dinge repräsentieren nicht; sie ordnen sich ein. An der Wand, dem Fenster gegenüber, ist eine Karte von Schweden, blau, grün und rot: ein frohes, buntes Kinderland. Sonst sind Abbildungen von guten Gemälden da, in glatten, einfachen Holzrahmen. Des Velazquez kleiner reitender Infant. Daneben aber, ganz ebenso anerkannt, hängt das rote Haus, das der kleine Bengt oder Nils oder Ebbe gemalt hat, mit dem ernstesten Gesicht. Die lichten Gänge führen zu den Sälen hin, die für viele Beschäftigungen eingerichtet sind. Da ist ein weiter, luftiger Raum für die Handarbeiten der Kleinsten; in einem anderen werden Bürsten hergestellt <pb n="676" xml:id="tg486.3.12.1"/>
und Bücher gebunden; eine Werkstatt ist da für Tischlerarbeiten und Mechanik, eine Druckerei und ein stilles, heiteres Musikzimmer.</p>
                    <p rend="zenoPLm4n0" xml:id="tg486.3.13">Man hat das Gefühl: hier kann man etwas werden. Diese Schule ist nicht etwas Vorläufiges; da ist schon die Wirklichkeit. Da fängt das Leben schon an. Das Leben hat sich klein gemacht für die Kleinen. Aber es ist da, mit allen seinen Möglichkeiten und mit vielen Gefahren. Da hängen in den Werkstätten, wo die Zwölfjährigen arbeiten, all die scharfen Messer und Ahlen und Stahle, die man sonst ängstlich vor den Kindern verbirgt. Hier legt man sie ihnen vorsichtig und ernst und richtig in die Hand und sie denken gar nicht daran, damit zu »spielen«. Sie beschäftigen sich so intensiv; und fast alle ihre Arbeiten sind gut und genau und brauchbar; des Handwerks tiefer Ernst kommt über sie.</p>
                    <p rend="zenoPLm4n0" xml:id="tg486.3.14">Im Saal für Mechanik wurde ein Knabe gerufen, der einen Motor erfunden und im Modell ausgeführt hatte. Er sollte ihn erklären. Er war schon mit einer anderen Arbeit beschäftigt, von der er bereitwillig, aber doch ungern gestört, herüberkam. Sein Gesicht war noch ganz von der verlassenen Arbeit erfüllt. Aber dann nahm er sich zusammen und gab sachlich kurz die gewünschten Aufklärungen. Der Ton seiner Worte, die geschickten Gebärden, womit er sie begleitete, selbst die offene, sichere Art seiner Freundlichkeit zeigte den Arbeiter, der in seiner Arbeit lebt. Und wie bei diesem Knaben, so war bei allen Kindern Offenheit und Sicherheit zu finden; sie waren alle beschäftigt und froh <pb n="677" xml:id="tg486.3.14.1"/>
und dadurch allen Tätigen nah; mochten es nun Erwachsene oder Kinder sein; in der ernsthaften und freudigen Beschäftigung war eine Gemeinsamkeit gegeben, auf der sich verkehren ließ; aller Grund zur Verlegenheit war fortgefallen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg486.3.15">Die Freudigkeit, die Neigung, womit in dieser Schule alles geschieht, prägt alle Dinge. Wie schön sind die von den Kindern gedruckten und gebundenen Bücher, wie rührend ausdrucksvoll sind ihre kleinen Modellierversuche; und ihre Blumenzeichnungen nach der Natur sind so richtig und liebevoll und gewissenhaft, daß sie, wo gewisse Voraussetzungen da sind, jeden Augenblick Kunst werden können. Es tut so gut, zu fühlen, daß in diesen Kindern nichts verkümmern kann. Jede, auch die leiseste Anlage muß nach und nach zum Blühen kommen. Keins von diesen Kindern muß sich dauernd zurückgesetzt glauben. Der Möglichkeiten sind so viele. Für ein jedes muß der Tag kommen, da es sein Können entdeckt, irgendeine Fähigkeit, eine Geschicklichkeit, eine Lust zu irgend etwas, die ihm in dieser kleinen Welt seinen Platz, seine Berechtigung giebt. Und was das wichtigste ist: diese kleine Welt ist im Grunde nichts anderes als die große Welt auch; was man in ihr ist, kann man überall sein; diese Schule ist nicht ein Gegensatz des Heims. Sie ist dasselbe. Sie ist nur zu jedem » Zuhause« hinzugekommen, sie ist an alle Hauser angebaut und will mit ihnen in Verbindung sein. Sie ist nicht das andere. Die Eltern gehen in ihr eben so ein und aus wie ihre Kinder. Es steht ihnen frei, dann und wann einer Unterrichtsstunde <pb n="678" xml:id="tg486.3.15.1"/>
beizuwohnen; sie kennen die Raume des Schulhauses und finden sich darin zurecht. Und auch im Verhältnis zum Leben will diese Schule nicht das andere sein. Deshalb kann sie keine Lehrer brauchen, die diesen Beruf ergreifen; die an ihr lehren, müssen von ihrem Beruf ergriffen sein. Es genügt nicht, daß sie einen Gegenstand beherrschen; dieser Gegenstand muß gewissermaßen unter freiem Himmel stehen; er darf nicht isoliert, nicht abgeschnitten, nicht aus allen Zusammenhängen gehoben sein. Er muß sich verwandeln, und wenn sich etwas rührt in der Welt, muß er zittern und tönen; man muß es an ihm merken können. Immer soll, unter dem Vorwande der verschiedenen Fächer, vom Leben die Rede sein. Wie schön war es, als einmal ein Bergmann kam, ein gewöhnlicher Bergmann, der schlicht und schwer von seinen schwarzen Tagen erzählte; und wie für ihn, so steht der Lehrersessel für jeden da, der etwas erfahren hat: für den Reisenden, der von fremden Gegenden erzählt, für den Mann, der Maschinen baut, und vor allem für den Schlichtesten unter den Wissenden, den Handwerker mit den klugen, vorsichtigen Händen. Denk, wenn einmal ein Zimmermann käme! Oder ein Uhrmacher oder gar ein Orgelbauer! Und sie können jeden Augenblick kommen. Denn ganz leise nur, ohne Last, liegt das Netz des Stundenplanes über den Tagen. Es wird oft verschoben. Die Wochen gehen einem nicht mit der monotonen Eile eines Rosenkrazes durch die Finger. Jeder lag fangt an als etwas Neues und bringt unerwartete und erwartete und völlig überraschende <pb n="679" xml:id="tg486.3.15.2"/>
Dinge. Und für alles ist Zeit. Die Frühstückspause ist so lang, daß man den Tisch abräumen und ihn mit hellem Wachstuch decken kann. Blumen werden in der Mitte daraufgestellt, Butterbrotteller und Gläser und Becher mit Milch; und dann sitzt es rund herum und ißt und träumt, lacht und erzählt und sieht wie eine Geburtstagsgesellschaft aus.</p>
                    <p rend="zenoPLm4n0" xml:id="tg486.3.16">Es ist Zeit und Raum in dieser Schule. Um jedes dieser kleinen blonden Geschöpfe ist Raum. Wie ein Haus mit Garten ist jedes. Es ist nicht eingerammt zwischen seine Nachbarn. Es hat etwas um sich herum, etwas Lichtes, Freies, Blühendes. Es soll auch nicht gerade so wie seine Nachbarn aussehen; im Gegenteil: es soll so von Herzen verschieden sein, so aufrichtig anders, so wahr wie nur irgend möglich.</p>
                    <p rend="zenoPLm4n0" xml:id="tg486.3.17">Es war konsequent und mutig, diesen Kindern keinen Religionsunterricht im herkömmlichen Sinn aufzuerlegen. Eine autoritative Beeinflussung an dieser empfindlichsten Stelle inneren Eigenlebens hätte alles Gerechte und Menschliche, das hier versucht worden ist, wieder aufgewogen. Man hat sich entschlossen, die biblischen Stoffe nach den reinsten, absichtslosesten Quellen als Historie vorzutragen, und man will nach und nach dazu kommen, Religion nicht ein- oder zweimal in der Woche zu geben, nicht heute von neun bis zehn, sondern immer, täglich, mit jedem Gegenstande, in jeder Stunde. Die Menschen, die diese Schule am meisten lieben, haben nach Tagen und nach Nächten, im ganzen Bewußtsein ihrer Verantwortung, diesen Beschluß gefaßt. Nun muß man Vertrauen zu ihnen haben. <pb n="680" xml:id="tg486.3.17.1"/>
Kinder und Eltern. Denn diese Bedeutung scheint mir leise in dem Namen Samskola mitzuklingen: Gemeinschule, Schule für Knaben und Mädchen, aber auch: Schule für Kinder und Eltern und Lehrer. Da ist keiner über dem anderen; alle sind gleich und alle Anfänger. Und was gemeinsam gelernt werden soll, ist: die Zukunft.</p>
                    <p rend="zenoPLm4n0" xml:id="tg486.3.18">Nur mit Einem reicht die Vergangenheit herein. Mit dem Aberglauben von den großen Kathedralen. Menschenleben sind unter den Grundsteinen verschwunden und der Mörtel ist auch bei diesem Bauwerk mit Herzblut gemischt.</p>
                    <pb n="681" xml:id="tg486.3.19"/>
                </div>
            </div>
        </body>
    </text>
</TEI>