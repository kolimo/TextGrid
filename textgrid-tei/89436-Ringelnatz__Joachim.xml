<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg349" n="/Literatur/M/Ringelnatz, Joachim/Erzählprosa/Ein Jeder lebt's/Sie steht doch still">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Sie steht doch still</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Ringelnatz, Joachim: Element 00358 [2011/07/11 at 20:32:19]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <notesStmt>
                <note>
                                 Erstdruck in: »Münchener Illustrierte Zeitung«, 3, Nr. 45, 1910.
                              </note>
            </notesStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Joachim Ringelnatz: Das Gesamtwerk in sieben Bänden. Herausgegeben von Walter Pape, Band 1: Gedichte, Band 2: Gedichte, Band 3: Dramen, Band 4: Erzählungen, Band 5: Vermischte Prosa, Band 6: Mein Leben bis zum Kriege, Band 7: Als Mariner im Krieg, Zürich: Diogenes, 1994.</title>
                        <author key="pnd:118601121">Ringelnatz, Joachim</author>
                    </titleStmt>
                    <extent>32-</extent>
                    <publicationStmt>
                        <date when="1994"/>
                        <pubPlace>Zürich</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1883" notAfter="1934"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg349.3">
                <div type="h4">
                    <head type="h4" xml:id="tg349.3.1">Sie steht doch still</head>
                    <p xml:id="tg349.3.2">Ein großer Dampfer schiebt sich durch den Ozean. Auf dem Gitterwerk über dem Maschinenraum liegt ein kranker Mann, das schmutzige Gesicht auf die heißen Stangen gepreßt, nicht schlafend, nicht wachend. Schwüle Dämpfe steigen von unten herauf und hängen Perlen an seine Stirne. Wenn er die Augen öffnet, sieht er Räder, Kessel und Stangen. Er sieht sie jetzt auch mit geschlossenen Augen. Die Maschine stampft, schlägt, braust, dröhnt, wie sie Tag und Nacht tut. Heute hört er es. Dabei wartet er mit Angst auf ein Glockenzeichen. Es muß gleich kommen. Vergeblich versucht er zu schlafen, nichts zu denken. Er sieht Räder, Kessel, Stangen, er denkt an die Glocke und hört das Dröhnen der Maschine. »Sie steht nicht still«, flüstert er vor sich hin. Plötzlich liegt Udo neben ihm.</p>
                    <p rend="zenoPLm4n0" xml:id="tg349.3.3">Udo ist schon zwei Wochen tot. Er ist verrückt geworden und über Bord gesprungen, weil – – sie nicht stillstand.</p>
                    <p rend="zenoPLm4n0" xml:id="tg349.3.4">»Udo, glast es bald?« fragt der kranke Mann.</p>
                    <p rend="zenoPLm4n0" xml:id="tg349.3.5">»Noch eine Minute«, gibt der andere zurück.</p>
                    <p rend="zenoPLm4n0" xml:id="tg349.3.6">»Udo, – – ich kann nicht mehr.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg349.3.7">Udo grinst blöde und schweigt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg349.3.8">»Nicht wahr, sie steht nicht still?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg349.3.9">»Nein, sie steht nicht still.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg349.3.10">»Aber wenn wir alle nicht mehr wollen?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg349.3.11">»Alle?« – Udo lacht hart. »Ihr müßt und ihr wollt.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg349.3.12">»Udo, ist die Minute bald um?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg349.3.13">Niemand antwortet. Der kranke Heizer ist allein.</p>
                    <p rend="zenoPLm4n0" xml:id="tg349.3.14">Acht Glockenschläge gellen häßlich durch den gleichmäßigen Lärm. Es klingt wie das »Hü, Hü« eines Kutschers.</p>
                    <p rend="zenoPLm4n0" xml:id="tg349.3.15">Der Mann erhebt sich matt und steigt mit klappernden Holzpantoffeln die schwarze Wendeltreppe hinab. Hansen, der abgelöste Heizer, übergibt ihm eine Feile und spricht dabei etwas. Er versteht es nicht. »Sie steht nicht still«, murmelt er, ohne aufzusehen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg349.3.16">
                        <pb n="32" xml:id="tg349.3.16.1"/>
»Wer steht nicht still?« fragt Hansen verwundert.</p>
                    <p rend="zenoPLm4n0" xml:id="tg349.3.17">»Ach – Udo hat's gesagt.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg349.3.18">Hansen dreht sich ärgerlich um und steigt mit einer höhnischen Bemerkung an Deck. Der Mann unten legt die Feile fort, nimmt eine Kanne und beginnt zu ölen. Der Maschinist tritt aus dem Heizraum ein. Er gibt irgendeine Anweisung. Der Heizer tritt an das große Rad, um das Ölbassin aufzufüllen. »Sie steht nicht still«, stöhnt er ganz leise und schauert dabei zusammen, als ob er fröre. Auf einmal ist der Heizer nicht mehr da. Es klingt schrecklich, wenn ein menschlicher Körper zermahlen wird, noch viel schrecklicher als das kurze Todesgekreisch eines, der verunglückt. Der Maschinist hält sich die Ohren zu und starrt zitternd, bleich, mit verzerrten Augen auf die roten Fetzen an dem rotierenden Rad. – – Im Vorderschiff unter Deck trinkt Hansen Kaffee mit anderen Heizern. Mitten im lustigen Gespräch setzt er seine Tasse nieder und wendet horchend sein Gesicht zu Boden. Dann sagt er tiefernst: »Sie steht doch still!«</p>
                </div>
            </div>
        </body>
    </text>
</TEI>