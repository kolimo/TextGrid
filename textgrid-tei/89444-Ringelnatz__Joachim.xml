<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg371" n="/Literatur/M/Ringelnatz, Joachim/Erzählprosa/Die Woge/Die Blockadebrecher">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Die Blockadebrecher</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Ringelnatz, Joachim: Element 00367 [2011/07/11 at 20:32:19]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <notesStmt>
                <note>
                                 Erstdruck in: »Simplicissimus«, 20, Nr. 34, 1915.
                              </note>
            </notesStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Joachim Ringelnatz: Das Gesamtwerk in sieben Bänden. Herausgegeben von Walter Pape, Band 1: Gedichte, Band 2: Gedichte, Band 3: Dramen, Band 4: Erzählungen, Band 5: Vermischte Prosa, Band 6: Mein Leben bis zum Kriege, Band 7: Als Mariner im Krieg, Zürich: Diogenes, 1994.</title>
                        <author key="pnd:118601121">Ringelnatz, Joachim</author>
                    </titleStmt>
                    <extent>103-</extent>
                    <publicationStmt>
                        <date when="1994"/>
                        <pubPlace>Zürich</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1883" notAfter="1934"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg371.3">
                <div type="h4">
                    <head type="h4" xml:id="tg371.3.1">
                        <pb type="start" n="103" xml:id="tg371.3.1.1"/>
Die Blockadebrecher</head>
                    <p xml:id="tg371.3.2">Ein drittes Mehlfaß rollte der Steward zurück, wodurch in dem Stapel von Proviantkisten ein Hohlraum geöffnet wurde. Dann drängte er flüsternd den langen, bartlosen Mann, der in der Haltung eines hilfsbereiten Ratlosen ihm zugeschaut hatte: »Schnell! Es ist schon einer drin.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.3">Der Lange warf sich ungeachtet seiner gediegenen Kleidung stracks zu Boden und kroch kopfan in das Loch. Das mußte eben nicht viel Platz bieten, denn als er sich zur Hälfte darin befand, blieb er stecken. Der Steward hörte, wie im Innern der Höhle eine zweistimmige Begrüßung in deutscher Sprache stattfand; und da er kein Verständnis für dies Idiom, außerdem Eile hatte, deutete er solches mit der Stiefelspitze auf dem noch sichtbaren Hinterteil des Liegenden an. Ruckweis zogen sich nun auch des Langen Beine in die Öffnung hinein, welche der Schiffskellner unter letzten Ermahnungen wieder mit den schweren Fässern verrammelte. Die Ankerlaterne vom Boden aufhebend, leuchtete er noch einmal das Proviantlager und dessen fensterlose Eisenwände und Schotten ab, fand nichts Verräterisches und begab sich schmunzelnd eine Leiter empor, durch eine Luke an Deck des norwegischen Dampfers, der am nächsten Tage Barcelona verlassen sollte, und über dem jetzt die feuchte Abendluft des 24. Januars 1915 taute.</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.4">In der Dunkelheit unter einer doppelten Kistenschicht hockte nun der lange Seemann, die Knie bis ans Kinn eingezogen und Schulter an Schulter mit einem Fremden, der ebenfalls seine Beine nicht auszustrecken vermochte, der sich ebenfalls schlechtweg als deutscher Matrose vorgestellt und auch die Absicht hatte, sich nach Genua zu schmuggeln. Dieser Mann redete anfangs nur auf Befragen, dann knapp sachlich und ziemlich ungemütlich, was sich aber möglicherweise dem Umstand zuschrieb, daß die Unterhaltung im Flüstertone bleiben mußte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.5">»Wurden auch Sie vom Zollbeamten bemerkt?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.6">»Ja, aber den wird der Steward bestochen haben.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.7">»Morgen Mittag soll es in See gehen. Wieviel wird er laufen?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.8">»Sechs, sieben Meilen. Mehr schaffen diese lütten Fischklepper nicht.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.9">
                        <pb n="103" xml:id="tg371.3.9.1"/>
»<hi rend="italic" xml:id="tg371.3.9.2">Bueno,</hi> Kamerad, dann können wir schon nächste Woche deutsche Soldaten sein.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.10">Der fremde Matrose erwiderte nichts.</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.11">»Wo, meinen Sie, daß man uns hinsteckt? Ich wünsche mich auf ein U-Boot, irgendwohin, wo es aufs Ganze geht. O, Deutschland wird siegen! Wissen Sie, wofür ich verdammt zehn Jahre meines Lebens hingeben wollte? Einmal als Sieger über den Trafalgar Square zu bummeln. Glauben Sie nicht auch, daß wir siegen werden?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.12">»Ich weiß nicht.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.13">Ein nüchterner Mensch! dachte der lange Matrose, und er stellte sich danach ein. »Teufel, das stinkt hier wie tausend Rattenkadaver! Kommt das aus der Bilsch?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.14">»Klippfisch«, brummte der andere wegwerfend.</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.15">»Der Kasten scheint voll zu sein; die Luken waren dicht. Am Ende nimmt er noch Deckslast. Wenn sie nur nicht morgen das Schiff noch einmal überholen. Ich bin schon zweimal von diesen vermaledeiten französischen Geheimspionen verscheucht.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.16">Der Stumme gähnte langatmig und dehnte sich in die Breite, wobei er dem Langen versehentlich mit dem Ellbogen in die Zähne schlug. Aber er sagte nichts.</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.17">»Halten Sie sich schon lange in Barcelona auf?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.18">»Sechs Wochen.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.19">»Sie musterten hier ab?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.20">»Nein, in Lissabon.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.21">»Lloyd?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.22">»Hapag.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.23">»Von der Westküste ...?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.24">»Südamerika. Ja.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.25">»Und reisten per Bahn?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.26">»Erst nach Madrid, dann nach Bilbao und dann nach hier.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.27">»Warum nicht gleich direkt?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.28">»Es waren zuviel Deutsche dort; man ließ uns nicht hinein. Erst mit der Zeit in kleinen Trupps schob man uns nach, wenn wieder andere fort waren.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.29">»Ja, ja, sie strömen alle herbei, für die Heimat zu kämpfen. – Wann weilten Sie zuletzt in Deutschland?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.30">»Vor drei Jahren.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.31">»Drei Jahren? Denken Sie: ich bin seit sieben Jahren fort. – Ob uns die Franzosen unterwegs anhalten werden?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.32">
                        <pb n="104" xml:id="tg371.3.32.1"/>
»– weiß nicht.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.33">»Indolent!« stieß der Lange geärgert hervor; doch war er überzeugt, daß der andere das Wort nicht verstünde. Er beschloß, fortan gleichfalls stumm zu sein.</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.34">So erstarben die Worte in dem geheimen Gelaß, und dafür lebten mancherlei traumwebende Geräusche der Ruhe auf: der ohnmächtig zornige Wellenschlag an der Bordwand, das Nagen einer Maus, zwei schnaufende Atemzüge nebeneinander, zuweilen ein Seufzer, auch ein Kleiderrauschen und Füßescharren, wenn der eine oder andere von den Matrosen seine Lage zu verändern trachtete.</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.35">Da füllte sich das auf die Knie gepreßte Ohr des Langen mit einem feinen Klingen, dem Summen eines Moskitos oder jenem Tone ähnlich, der entsteht, wenn man mit feuchtem Finger auf dem geschliffenen Rande eines wenig gefüllten Weinglases kreist.</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.36">Mein Ohr klingt, konstatierte der Lange in Gedanken, es denkt jemand an mich. Wahrscheinlich sogar mehrere ... alle ... selbst Vater. Und da das Klingen weiter währte, lauschte der Lange ihm aufmerksamst, zu ergründen, wo es wohl herrührte und was es für eine Bedeutung hätte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.37">Klingt es nicht wie ein vielfach gedämpfter Schrei? Ernst und gleichsam warnend? Zei ... eit! – Nein, doch irgendein Ruf mit i muß es sein, der ausdrückt: Besinne dich! Die Zeit eilt und kehrt nie – nie ... ie – wieder.</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.38">Die sieben Jahre kehren nie wieder. Arm und einsam verrannen sie, überreich an Glück und Liebe konnten sie sein. Er ist doch etwas ungemein Vornehmes an uns, dieser Trotz auf das Ausgesprochene. – Vielleicht klingt in dieser Minute auch Vaters Ohr. – – Unser Trotz wird nicht gebrochen sein, wenn wir uns wieder in die Arme fallen; er wird von beiden Seiten zurückgezogen, um einer höheren Aufgabe willen. – – Eilen sie doch alle, für ihr Blut, für ihr Vaterland einzustehen, auch die, welche die Heimat vergessen wollten oder sie hassen lernten. – – Auch ich werde für alle kämpfen, auch für die Brüder und ebenso für meinen Vater, sogar um die Erde, die Muttern deckt. – – Mein Gott, sich vorzustellen, daß Vater von reuiger Rückkehr sprechen oder an Vorwand – Krieg denken konnte – – nein! Ein deutscher Edelmann und Kaiser und Reich in Gefahr – –! Ich will meinen Schuldteil tilgen, ihn mit Blut abwaschen, und es spreche keiner von romantischer Wahnidee, von falscher Sentimentalität, Phrase oder Pose. – O <pb n="105" xml:id="tg371.3.38.1"/>
großes Jahr, da in der Welt das Theatralische zur alltäglichen Wirklichkeit geworden ist! – Zwischen Feuer und Wasser will ich kämpfen, immer dort, wo die Gefahr gipfelt, allen voran, und nur mit dem Kreuz das geliebte Haus wieder betreten. – Gott gebe, daß sie mich nicht für dienstuntauglich erklären. – –</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.39">Die blinden Passagiere stöhnten und gähnten immer häufiger, und da sie doch zu sehr aufeinander angewiesen waren, um sich gegenseitig zu ignorieren, so lehnte schließlich der Lange seine rechte Wange gegen die Brust des andern und ruhte ein wenig in dieser Abwechslung aus, bis ihn die linke Hüfte schmerzte. Dann wechselten sie die Rollen. Nicht Licht noch Dämmerung kündeten nach einer Ewigkeit den Morgen, sondern ein Konzert aus Poltern, Rufen und schweren Tritten, das durch Eisen- und Holzwände geschwächt wurde, den erfahrenen, angestrengt lauschenden Seeleuten jedoch wichtige, vertraute Vorgänge verriet.</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.40">Der Lange klagte: »Ich habe Hunger wie ein Seeteufel. Wissen Sie, was ich jetzt tue? Ich ziehe mein Messer und untersuche der Reihe nach alle diese Kisten nach Freßbarem.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.41">»Nein, mußt nicht!« wehrte der andere. »Wir wollen dem Steward keine Schweinereien machen; das scheint ein anständiger Kerl. – Gib mal deine Flosse her – so! Da liegt ein Paket mit Brot und Wurst. Und hier, in dieser Fuge – vorsichtig! Fühlst du's? – Darauf mußt achtgeben; es ist eine Rose darin, die darf nicht geknickt werden.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.42">Dem Langen gefiel es herzlich und versöhnte ihn, daß jener bezüglich der Kisten so gewissenhaft dachte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.43">»Was für eine Rose?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.44">»Eine Rose aus Papageienfedern; ich kaufte sie in Brasilien vom Bumbootsmann für meine Braut.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.45">»Haben Sie keine Eltern mehr?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.46">»Nein.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.47">»Geschwister?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.48">»Keine. Mein einziger Bruder ist kürzlich mit einem Minensucher in die Luft gegangen.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.49">»Hm! Traurig und doch schön. Ich habe zwei Brüder im Felde, Dragoner – – –«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.50">»Pst! Still!« zischte Klein. »Hörst du? Draußen ist schlimmes Wetter.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.51">»Immerzu! Sagen Sie mal: Warum wollen – warum willst du eigentlich nach Deutschland?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.52">
                        <pb n="106" xml:id="tg371.3.52.1"/>
»Was soll ich denn hier? Ich habe meine ganze Heuer, über vierhundertfünfzig Mark, aufgebraucht; die letzten sechzig Peseten gab ich dem Steward. Nun will ich erst mal zu meinem Mädchen nach Ostpreußen, na und dann werden sie mich einkleiden. Seewehr zwo.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.53">»Freust du dich gar nicht darauf, Soldat zu werden?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.54">Der andere lachte wie über eine törichte Frage. »Man muß doch. Es dienen jetzt doch alle.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.55">»Gewiß, gewiß!« Und der Lange tastete die Kisten ab nach dem Paket, das er entfaltete. Er tastete den Umfang des Brotes und der Wurst ab und begann in derben Bissen zu schlingen. Dabei war es ihm nicht einmal möglich, seinen Oberkörper vollständig aufzurichten.</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.56">Der norwegische Dampfer tutete. Ein Sirenenheulen wand sich empor. Das Quirlen der Schraube setzte ein und erschütterte den schwimmenden Eisenbau. »Hurra!« jauchzte der Lange, indem er den Nachbar knuffte. »Jetzt sind wir frei.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.57">»Was hast du dem Steward für die Überfahrt gezahlt?« fragte dieser.</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.58">»Hundert Peseten.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.59">»Ja so, du bist von feinen Eltern.« Das war ohne Spott gesagt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.60">»Ich besitze nur selbstverdientes Geld, aber ziemlich reichlich, und kann dir daher die weitere Heimreise mit Vergnügen bezahlen.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.61">»Dazu gibt mir der Konsul in Genua Geld. Auch hier bekam ich täglich Unterstützung. Warst du denn nicht beim deutschen Konsul?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.62">»Nein.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.63">»Also hast du wohl auch keinen Paß?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.64">»Nee.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.65">»Das ist windig. Ich komme mit meinem Paß durch, aber du darfst dich beim Landen nicht kitschen lassen, sonst schicken sie dich gleich wieder zurück. Wenn wir also morgens einlaufen, dann laß uns ruhig noch bis abends in diesem Loch bleiben, bis die Schauerleute ausscheiden; und dann sehen wir zu, wie wir uns am Wachtmann vorbeikreuzen.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.66">»Wir«, wiederholte der Lange gerührt; »Landsmann, nichts für ungut; ich habe dich anfangs für unliebenswürdig und gefühllos gehalten, weil du so schweigsam –«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.67">»Ja, ich kann nicht so reden wie du«, fiel ihm der andere ins Wort.</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.68">
                        <pb n="107" xml:id="tg371.3.68.1"/>
»Du bist ein braver Kerl; laß uns gute Freundschaft halten.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.69">»Schön!« sagte der Fremde und drückte mit ehernen Fingern die Hand, die nach der seinen tastete. Aus seiner Antwort war zu entnehmen, daß er amüsiert lächelte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.70">»Wie heißt du eigentlich? – Wie? – Heinrich Klein? – – Ich? Ach, ich! – Ich heiße – mein Name ist Tilger, Rein – Reuhard Tilger.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.71">In diesem Augenblick legte sich das Schiff stark nach Steuerbord über, so daß sie beide mit den Köpfen gegen die vordere Kistenwand schlugen. Damit fing es an, aufs heftigste zu stampfen und zu rollen. Sie mußten sich mit Rücken, Armen und Zehen feststemmen, um nicht hin und her geworfen zu werden. An Schlaf war vollends nicht mehr zu denken. So kämpften sie stundenlang mit erlahmender Muskelkraft gegen den zunehmenden Seegang an, stöhnten, fluchten, gähnten und schwiegen oder schwatzten eine Weile in schleppenden Sätzen über ihre Seefahrten, ihre nächste und fernere Zukunft, über Kleins ländliche Braut, über Krieg, Engländer und den lieben Gott. Dazwischen rechneten sie und taxierten, wie spät es ungefähr sei, und schwiegen wieder oder jammerten, hin und her rutschend, leise vor sich hin.</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.72">Plötzlich brachen sie ein Gespräch ab.</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.73">»Nun?« fragte der Lange.</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.74">»Die Maschine stoppt?« fragte Klein.</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.75">Im ersten Moment hat es für den Dampfermatrosen stets etwas Beängstigendes, wenn der permanente Rhythmus der Maschine unversehens aussetzt, wenn ihr Atem stockt. Für die Deutschen war besonderer Grund vorhanden, besorgt zu sein. Reuhard Tilger sprach es aus: »Die Franzosen!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.76">»Kriegszone?« meinte Klein unsicher. »Nein, unmöglich.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.77">Sie horchten verhaltenen Atems, ohne Bestimmteres zu ergründen. »Vielleicht Maschinenschaden.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.78">»Mann über Bord?« So rieten sie hin und her, bis die Maschine wieder ansprang.</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.79">»Lotse!« triumphierte Tilger.</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.80">»Nein«, sagte Klein bestimmt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.81">Wieder stieg und stürzte der Boden unter ihnen und mit ihnen. Schwere Brecher prallten gegen die Bordwand. Irgendwo rollte donnernd ein Balken vorwärts und rückwärts über Eisen. Da verursachte ein Rasseln neue Spannung. In die Finsternis der Höhle Schossen zwei Strahlen gebrochenen Taglichtes, ein feiner und ein <pb n="108" xml:id="tg371.3.81.1"/>
stärkerer. Der feine brach seitlich zwischen den Mehlfässern herein, der stärkere fiel aus einem Spalt von oben und traf Heinrich Klein mitten ins Gesicht.</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.82">Auf dieses Gesicht warf Reuhard unverzüglich einen raschen, unbescheidenen Blick, dann einen zweiten auf seine Taschenuhr, während er mit kalter Angst vernahm, wie jemand die Leiter herabklomm. Ein Gegenstand wurde durch den oberen Spalt herabgeschoben; eine Stimme rief leise auf Englisch:</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.83">»Ein Bissen Speck! He, da unten! Alles klar?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.84">
                        <hi rend="italic" xml:id="tg371.3.84.1">»Allright«,</hi> gab Klein zurück, »warum stoppen wir?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.85">»Maschine heiß gelaufen. Bleibt und schweigt!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.86">Damit entfernte sich der Steward wieder, und indem er, an Deck angelangt, die Luke zuschlug, tötete er die zwei Strahlen himmlischen Lichtes.</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.87">»Klein, du bist nun bald achtundvierzig Stunden in dieser Box«, sagte Tilger; dann dachte er über das grobe, knochige Gesicht nach. Es hatte durch eine platte Nase, eine senkrechte Stirn, sowie durch struppiges Kinn- und Barthaar etwas Pinscherhaftes. Aber unter den ausgewucherten Brauen blinkten aus weit entblößter Augenweiße blaue Siegel der Ehrlichkeit.</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.88">Klein knitterte mit Papier. »Speck und Schokolade!« sagte er trocken. Sie teilten und aßen, während sie immer einen Arm gebrauchten, sich festzuklemmen. Ihre Hände, ihre Haut, ihre Kleider klebten von Schmutz und Schweiß. Ihre Nasenlöcher waren von Staub verstopft. Sie empfanden Schmerzen im Leib, im Genick, im Gesäß, und Klein lamentierte über Wadenkrämpfe. Die Luft in dem Loche verschlechterte sich unerträglich.</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.89">Einmal täglich brachte der Steward etwas Nahrung. Am zweiten Tage ließ er eine Tüte voll Wasser durch den Spalt, ihr Inhalt ging jedoch zum größten Teil verloren.</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.90">Noch immer schüttelte das Unwetter die Flüchtlinge wie Käfer in einer Schachtel herum; sie leisteten nur mehr schwachen Widerstand.</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.91">Auch stoppte die Maschine abermals eines Schadens wegen. Es kletterten zwei Heizer in den Proviantraum und machten sich dort – Klein beobachtete es durch den seitlichen Spalt – mit Schlosserwerkzeug in einem Winkel zu schaffen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.92">»Wenn das Schiff absäuft«, sagte der Lange nachdenklich, nachdem die ahnungslosen Heizer den Raum wieder verlassen hatten, »würde kein Mensch je erfahren, wo wir abgeblieben sind.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.93">
                        <pb n="109" xml:id="tg371.3.93.1"/>
Der Dampfer nahm seine Fahrt von neuem auf. Ermattet schwiegen die Deutschen. Die Gedanken verschwammen ihnen. Immer gleichgültiger überließen sie sich dem Schiff und dem Schicksal. Mehrmals überfiel sie eine schlafähnliche Schwäche, in der sie für kurze Dauer ihre Schmerzen und ihre Sorgen vergaßen. Allmählich mäßigten sich die Schwankungen des Schiffes.</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.94">Es geschah am dritten Tage, daß wiederum die Maschine verstummte und Klein und Tilger aus ihrer Lethargie jäh aufschraken. Ein quietschendes Rollen, dann hohles Aufschlagen an der Bordwand bewies ihnen, daß ein Boot zu Wasser gelassen wurde. Sie rafften alle Energie zusammen, rückten sich so bequem als möglich zurecht; denn nun sollte es gelten, sich nicht um Haaresbreite zu rühren. Ihre Spannung entdeckte und verfolgte rege Schritte, welche kamen und gingen und wieder kamen. Das Kettenschloß an der Luke rasselte, und lebhaftes Sprechen in französischer Sprache drang wie ein Sturmwind gleichzeitig mit blendender Lampenhelle in den Vorratsraum. Klein sah erbebend ein Stück von einem französischen Soldaten auf der Leiter, der ein blitzendes Eisen schwang. Klein drückte seine Lippen an Tilgers Ohr und raunte dem zu: »Lange Dolche! Wen's trifft, bleibt still!« Im Nu hatte er seinen Filzhut wurstförmig zusammengedreht und stieß ihn nun mit gewaltigem Kraftaufwand in die obere Spalte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.95">Es mußten zwei Soldaten in Begleitung des norwegischen Kapitäns und des Stewards sein, welche das Lager rundum absuchten. Überall stocherten sie mit den Eisenstäben zwischen den Waren herum, und sie näherten sich mehr und mehr dem Versteck. Jetzt schurrten ihre Tritte auf der Kistenschicht über den Köpfen der Deutschen. Die hielten den Atem zurück. Unwillkürlich hatten sie sich gegenseitig gepackt, und jeder fühlte die Knie des andern zittern. Lärmvoll fuhr ein Dolch den oberen Spalt herab, stieß auf den Hut auf, wurde zurückgezogen, wieder herabgestoßen. Diesmal gab der Hut um Zentimeterlänge nach. Aber er fiel nicht heraus. Und die feindliche Patrouille schritt weiter, verließ den Proviantraum, später das Schiff. Endlich fuhr das Schiff. –</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.96">Reuhard drückte Heinrichen die Hand. Der Steward erschien, rollte die Mehlfässer ab und ließ seine Schützlinge heraus, damit sie sich an einer duftenden Suppe stärken, sich einmal für eine Viertelstunde strecken möchten.</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.97">Sie sahen aschfahl aus. Ihr Anzug war verknüllt. Sie blinzelten <pb n="110" xml:id="tg371.3.97.1"/>
mit den Augen, schnitten beim Strecken der Beine schmerzliche Gesichter und konnten zunächst nicht ungestützt stehen. Klein war von untersetzter Gestalt. Er klopfte sich den Mehlstaub von der Kleidung mit seinem entrollten Hute, welcher fünf Löcher von Dolchstichen aufwies. »Schade um die nagelneue <hi rend="italic" xml:id="tg371.3.97.2">cloth«,</hi> sagte er, »sie hat mich neunzig Peseten gekostet.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.98">Es bedurfte strenger Überwindung, sich nochmals in die bisherige Marterlage einzuzwängen. Noch einen halben Tag durchlitten sie dort, bis das Schiff im italienischen Hafen festlag und der Steward sie zu erlösen kam. Der Lange stemmte sich in jubelnder Ungeduld von innen gegen die Mehlfässer. »Au! Au!« schrie er.</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.99">»Halt doch das Maul!« zischte Klein. »Willst du zuletzt noch alles verderben?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.100">Aber Tilger lachte übermütig laut. »Dies verfluchte Faß hat mir einen meterlangen Nagel ins Genick gepiekt.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.101">Ganz wie Klein angenommen, waren sie morgens in Genua eingetroffen. Nun blieben sie noch volle zwölf Stunden verborgen; es war eine böse, böse Zeit. Immerhin durften sie jetzt wenigstens innerhalb des Proviantraumes frei einherspazieren, und Tilger bestand darauf, daß der Steward Sekt herbeischaffte. Tilger redete unaufhörlich in höchster Begeisterung. Er gestand seinem Kameraden, daß er gar nicht Tilger hieße, und gelobte und bat Heinrichen, daß die Freundschaft zwischen ihnen, die in der Glut der Vaterlandsliebe geschmiedet, in Gefahr gehärtet und schließlich mit Champagner besiegelt wäre, ihr Leben lang bestehen sollte. Dann spann er schillernde Träume aus von ruhmreichen Kampfestaten. Heinrich Klein war sein wortkarger, ungeduldiger und doch gutmütig aufmerkender Zuhörer. Nicht ohne mancherlei Schwierigkeiten stahlen sie sich abends an dem Wachtmann vorbei von Bord.</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.102">Sie speisten, becherten und übernachteten an Land in einem geringen deutschen Gasthaus; da gab es einen Schlaf in Betten. Bei nächster Frühe trennten sich die beiden Blockadebrecher in feierlicher Schlichtheit. Der Lange lag noch im Bett. Er wollte sich neue Kleidung beschaffen, bevor er weiterreiste; überdies war sein Hals infolge des Nagelstiches geschwollen. Klein aber war durchaus nicht zu längerem Aufenthalt zu bewegen. Er eilte aufs deutsche Konsulat, wo man ihn mit einer Fahrkarte bis Ala nebst entsprechender Wegzehrung versorgte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.103">
                        <pb n="111" xml:id="tg371.3.103.1"/>
Die Schilderung seiner Flucht machte auf den Bezirksfeldwebel, bei dem er sich in der nächsten deutschen Stadt meldete, wenig Eindruck. Dort trafen täglich viele Blockadebrecher ein. Man befahl Klein, sich unverzüglich nach Kiel zu begeben. Um seine Braut wiederzusehen, möge er später ein Urlaubsgesuch einreichen. Er war sehr aufgebracht darob. Und er fuhr nicht gleich nach Kiel, sondern zunächst nach Ostpreußen. Unterwegs, irgendwo auf einem Bahnhof, begegnete er zufällig einem Musketier, der aus seinem Heimatsdorfe stammte. Sie tauschten wiedersehensfroh ihre Kriegserlebnisse aus, in knappem Umfang. Dann erkundigte sich Klein nach seiner Braut. »Was«, rief der Musketier, »weißt du's noch gar nicht?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.104">»Was soll ich denn wissen?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.105">»Mischka ist tot.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.106">»Tot? Du bist ja verrückt«, sagte Klein ungläubig; aber er wurde blaß.</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.107">»Gott verdamme mich! Weißt du gar nicht, wie die Russen bei uns gehaust haben?«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.108">»Die Russen? Die Russen hätten Mischka tot – –« Klein räusperte sich heiser.</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.109">»Was ich dir sage«, erwiderte der Musketier, »sie ist tot.« Und etwas leiser fügte er hinzu: »Sie hat sich selbst erhängt – aus Scham – – –«</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.110">Wenige Tage, nachdem Klein ihn verlassen hatte, war Tilger in Genua an Blutvergiftung gestorben.</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.111">– – – – – – – – – – – – – – – – – – – – – – – – – –</p>
                    <p rend="zenoPLm4n0" xml:id="tg371.3.112">Mit anderen Marinern durch ein feindliches Dorf marschierend, warf Heinrich Klein eines Tages die brasilianische Rose einem flandrischen Mädchen zu.</p>
                </div>
            </div>
        </body>
    </text>
</TEI>