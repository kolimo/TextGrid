<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg136" n="/Literatur/M/Rubiner, Ludwig/Schriften/Mitmensch">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Mitmensch</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Rubiner, Ludwig: Element 00048 [2011/07/11 at 20:30:52]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <notesStmt>
                <note>
                              Erstdruck in: Zeitecho (München), 3. Jg., 1917.
                           </note>
            </notesStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Ludwig Rubiner: Der Dichter greift in die Politik. Herausgegeben und mit einem Nachwort von Klaus Schuhmann, Leipzig: Reclam, 1976.</title>
                        <author key="pnd:118603590">Rubiner, Ludwig</author>
                    </titleStmt>
                    <extent>306-</extent>
                    <publicationStmt>
                        <date when="1976"/>
                        <pubPlace>Leipzig</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1881" notAfter="1920"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg136.3">
                <div type="h3">
                    <head type="h3" xml:id="tg136.3.2">Ludwig Rubiner</head>
                    <head type="h2" xml:id="tg136.3.3">Mitmensch</head>
                    <p xml:id="tg136.3.5">
                        <pb type="start" n="306" xml:id="tg136.3.5.1"/>
Und da nun endlich überall in der Welt vom Geist und von den Geistigen die Rede ist, so wollen wir uns gewiß nicht bei dem bloßen Worte beruhigen. Es geschieht noch nichts für den Geist, wenn ein Musiker Sinfonien vor Musikfreunden klingen läßt, die nach seiner Hoffnung das Fühlen der Menschen umheben; wenn ein Maler Bilder vor Eingeweihten ausstellt, die nach seiner Überzeugung einen neuen Begriff vom Zusammenhalt der Dinge geben; wenn ein Dichter Gesellschaftsromane schreibt, die seiner Meinung nach aus demokratischen Ideen kommen; wenn ein Essayist alle diese Fragen zur Erörterung stellt. Es geschieht nichts. Es wird nur viel schlimmer. Denn eine Schicht von Menschen, deren Tatneigung ohnehin nicht groß ist, glaubt, schon aus dem bloßen Mitmachen dieser Werke etwas Geistiges getan zu haben.</p>
                    <p rend="zenoPLm4n0" xml:id="tg136.3.6">Es ist aber heute nicht mehr zweifelhaft, was eine geistige Tat ist. Diese Frage kann gar nicht mehr ehrlich gestellt werden. Sie ist uns allein durch die Tat eines Volkes in ungeheuerster Weise beantwortet worden. Vor unsern Augen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg136.3.7">Was dort geschah, ist kein »Ereignis«. Nicht ein Vorfall, der durch einen neueren Vorfall, durch ein anderes Ereignis verdrängt werden könnte. Es ist eine wirkliche Tat. Eine Tat ohne Symbolik, ohne Nebenbedeutung. Tat, vorbereitet, gezeugt und geschaffen aus der tiefsten, langschwingendsten, intensivsten Erhebung für den Geist. Und darum nicht mehr aus der Welt zu räumen. Kein Mißerfolg, kein Verrat, kein Rückschlag mehr, so Bitterstes auch noch eintreffen mag, kann die größte Entscheidung, die ein Jahrhundert gesehen hat, wieder ungeschehen machen. Die Entscheidung gegen die Macht, die Gewalt, die Unterdrückung: zum Geist, zur Freiheit und zum Menschenrecht nach dem Plane der Idee.</p>
                    <p rend="zenoPLm4n0" xml:id="tg136.3.8">Daß dies alles nicht Bluff, nicht Schiebung oder äußerlich auferlegte Beeinflussung war, ersieht man aus der unglaublich <pb n="306" xml:id="tg136.3.8.1"/>
vielfachen und fruchtbar um sich wirkenden Lebendigkeit des neuen Organismus: Von dem, was im Ostlande geschah, erwartet jeder etwas. Der Politiker erwartet eine Beeinflussung der Weltteile, der Marxist erwartet einen Aufschwung des Klassenkampfs, der Militär erwartet eine Veränderung der Kriegslage, der Pazifist erwartet Frieden, der Agent erwartet neuen Zwischenhandel, der Sozialist erwartet neue Gemeinschaft, der Reporter erwartet neue Sensationen, der Genosse der Finsternis erwartet sehr Böses für sich.</p>
                    <p rend="zenoPLm4n0" xml:id="tg136.3.9">Jeder, der in den Handlungen der Menschen nicht nur Nützliches, sondern vor allem Menschliches sieht, wird wild vor der Unbescheidenheit, vor der aufdringlichen Unverschämtheit höchsten Grades, wenn er sieht, wie Unbeteiligte mühelos der russischen Revolution lyrisch oder wohlwollend auf die Schulter klopfen. Es ist nicht die Zeit, weise Worte über die Gemeinschaftstat eines Volkes zu sagen; es ist nicht die Zeit, Dynastenhistorie zu erzählen, es ist nicht die Zeit, den Leser oder den Hörer mit der Feststellung von Erfolgen zu unterhalten, zu denen er nichts selbst getan und gegeben hat!</p>
                    <p rend="zenoPLm4n0" xml:id="tg136.3.10">Denn da drüben geschah nichts unerwartet Plötzliches. Aber auch nichts von dem Narrenchaos, das bequeme hämische Propheten ansagten. Um die neue, höhere Menschheitsordnung im Osten hat das edelste Blut dieses Landes mehr als ein halbes Jahrhundert lang gekämpft. Durch Jahre hindurch haben Hunderttausende ihr Haus, ihre Familie, ihr Vermögen, ihre Bequemlichkeit, ihre Genüsse, ihre Sicherheit, ihr Leben für nichts erachtet, um der Hingabe willen an ein Gebild aus dem Geiste. Diese wahrhaften Freiwilligen, die ihr Gesetz diktiert fanden von der Bruderliebe, schufen an der Verwirklichung einer Idee. Diese wahrhaft Tapferen waren nicht gedeckt durch Befehl und Massenzwang, sondern jeder wußte sich in jedem Augenblicke des Lebens ungeschützt, preisgegeben. Sie waren gestützt nur auf das Vertrauen zu ihrem Gewissen, auf ihren festen Willen zur Unbedingtheit, auf ihren Glauben an die dereinstige Leibwerdung ihrer Idee, auf ihren Glauben an die neue Auferstehung des Geistes auf Erden. Sie waren gestützt auf ihren Glauben an die Heiligkeit des Mitmenschen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg136.3.11">
                        <pb n="307" xml:id="tg136.3.11.1"/>
Derweile war in den andern Ländern der Erde Entmutigung, Skepsis, Erörterung und Erwerb von bloßen Methoden, denen der Antrieb des Geistes längst in Vergessenheit geraten war.</p>
                    <p rend="zenoPLm4n0" xml:id="tg136.3.12">Aber der Puls dieses neuen Leibes der Menschheit aus dem Geiste klopft so laut, daß ihn die ganze Erde hört. Einer, der einst Umstürzler hieß, jetzt zur Hilfe am Aufbau seines Landes gebeten: Krapotkin, zeigt in seinem Buch <hi rend="italic" xml:id="tg136.3.12.1">Gegenseitige Hilfe</hi> (Abschnitt <hi rend="italic" xml:id="tg136.3.12.2">Gegenseitige Hilfe im Mittelalter</hi>), wie im zwölften Jahrhundert eine ungeheure Freiheitswelle über Europa stürmte; die Schöpfung von Bünden, Gesellschaften, Gilden, freien Städten, Kommunen. Diese Geistwanderung über die Völker war jahrhundertelang vergessen, und nur die Dome und die großen Bauten der Städte sind uns als erstarrte Zeichen der Geistesglut zur Gemeinschaft geblieben. –</p>
                    <p rend="zenoPLm4n0" xml:id="tg136.3.13">Es kann Zeiten geben, in denen die Besinnung des Menschen zum Geistigen schläft; unser Wesentlichstes, unser Brudertum und unsere Gemeinschaft kann verfressen und vergessen werden. Aber der Geist kann nicht vernichtet werden. Immer wieder richtet er sich weithin sichtbar auf in Einzelnen und in kleinen Gruppen, den Frühen, den inspirierten Eingeweihten der menschlichen Freiheit, jenen, die nie Vergleiche mit der Macht des Ungeistes schlossen und die, märtyrerhaft geschmäht und verfolgt, noch im qualvollsten Tode verkündeten, daß sie Söhne der Idee waren.</p>
                    <p rend="zenoPLm4n0" xml:id="tg136.3.14">Es gibt nichts, das heute allen Menschen der Erde, allen, so klargeworden ist, als die Idee der Freiheit, der Bruderschaft und des Mitmenschentums. Dieser Erde ist es so unglaublich schlecht gegangen, sie hat so nichts mehr zu verlieren, sie ist so verschwistert mit der Verzweiflung, daß endlich auch der Träge und Böswillige als Heilung erkennt, was früher nur erhabene Seelen, unter aller Gefährdung ihrer Sicherheit, in eine stählern feindliche Welt zu künden wagten.</p>
                    <p rend="zenoPLm4n0" xml:id="tg136.3.15">Wen schaudert der Rückblick nicht? zu sehen, daß durch Jahrhunderte unsere Brüder gehetzt, gemartert und als tolle Störer der Gesellschaft ums Leben gebracht wurden, Menschen, deren Gemeinschaftspläne (um nur einen zu nennen: Thomas Morus) wir längst als selbstverständlichste <pb n="308" xml:id="tg136.3.15.1"/>
Voraussetzung nehmen, während das Chaos ihrer Zeit uns als irrsinnig erscheint.</p>
                    <p rend="zenoPLm4n0" xml:id="tg136.3.16">Auf einem neuen Erdreich werden diesen die Heldenmale gerichtet werden, und das Bruderhaus der Menschheit wird eine Gedenkschrift haben: »Zum Gedächtnis der Wiedertäufer, hingerichtet für ihren Kampf um eine bewußte Menschengemeinschaft aus dem Geiste.« –</p>
                    <p rend="zenoPLm4n0" xml:id="tg136.3.17">Es ist nicht Zeit, diese Revolution im Osten zu betrachten. Betrachtet nur erst euch selber, ihr geübten Betrachter! Betrachtet eure Voreltern, Eltern, Verwandten, Frauen, Bräute, Freunde – ob sie so gläubig mutvoll hingegebene Kameraden einer Idee waren, wie die dort drüben lange Jahre, erbittert und langmütig weiten Herzens! Betrachtet euch, ob ihr, ohne Rentabilitätsversprechen, ob ihr geistig das gewagt hättet! Es ist nicht mehr Zeit, zu <hi rend="italic" xml:id="tg136.3.17.1">betrachten.</hi>
                    </p>
                    <p rend="zenoPLm4n0" xml:id="tg136.3.18">Es ist Zeit, vor dieser Neuordnung des Ungeordneten sich auf sein Gewissen zu besinnen. Auf die eigenen sittlichen Fähigkeiten. Auf den eigenen Trieb zur unbedingten Hingabe an das Geistige. Auf die eigene Kraft zur Entfaltung des Willens für die Idee vom Mitmenschen. Für die Idee allein? Für die Tatsache vom Mitmenschen! Und welche Tatsache kennt die Welt heute besser, als daß jeder Mensch das Recht auf Existenz, Platz, Leben hat, nicht anders als du selbst. So weit sind wir endlich. Selbst in den Regierungshäusern des Erdballs.</p>
                    <p rend="zenoPLm4n0" xml:id="tg136.3.19">Es ist Zeit, das Wort vom Mitmenschen laut auszusprechen. Aber wer es spricht, der rechne erst ganz mit sich ab, der sei gleichgültig bereit, alles zu verlieren, erst dann wird sein Wort eine Schwingung und seine Idee der Gewinn eines Wagnisses.</p>
                    <p rend="zenoPLm4n0" xml:id="tg136.3.20">Wir alle, zu unsern Lebzeiten, werden noch das Ungeheuerste sehen. Die höchste Not der Menschheit wird ihre Gegenwaage haben in der höchsten Verwirklichung von Paradiesträumen. Niemand von uns braucht mehr entmutigt zu sein.</p>
                    <p rend="zenoPLm4n0" xml:id="tg136.3.21">Es gibt für den Menschen keine innere Leere mehr. Jeder hat sich mit seinem Gewissen unter das Auge der Ewigkeit zu stellen und die Sekunde jedes Lebensmoments hinzugeben an die Verwirklichung der Mitmenschenfreiheit.</p>
                    <p rend="zenoPLm4n0" xml:id="tg136.3.22">Diese Idee ist wahrhaft und wirklich schon so stark in die Welt gedrungen, daß keine Kriegsmaschine ihr mehr ein <pb n="309" xml:id="tg136.3.22.1"/>
tatsächlicher Gegner ist. Immer, wenn die Welt zwischen Dunkel und Licht schwebt, gibt es noch Wirrnis, und die Gewalt dringt im letzten Zucken vor, aber sieglos, nur im Todeskrampf. Es gibt keine Siege mehr. Vor dem Geist dieser neuen Menschheit werden die Heere zersplittern.</p>
                </div>
            </div>
        </body>
    </text>
</TEI>