<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg1507" n="/Literatur/M/Schönwerth, Franz/Sagen/Aus der Oberpfalz/Zweyter Theil/Eilftes Buch/Erde/6. Anhang/70. Ein verlassenes Dorf">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>70. Ein verlassenes Dorf</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Schönwerth, Franz: Element 01659 [2011/07/11 at 20:24:42]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Franz Schönwerth: Aus der Oberpfalz. Sitten und Sagen 1–3, Band 2, Augsburg: Rieger, 1857/58/59.</title>
                        <author key="pnd:118610236">Schönwerth, Franz</author>
                    </titleStmt>
                    <extent>449-</extent>
                    <publicationStmt>
                        <date>1857/58</date>
                        <pubPlace>Augsburg</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1810" notAfter="1886"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg1507.2">
                <div type="h4">
                    <head type="h4" xml:id="tg1507.2.1">§. 70. Ein verlassenes Dorf.</head>
                    <p xml:id="tg1507.2.2">Auf einer Hochebene zwischen Bleystein und Neukirchen, rings von üppigem Fichtenwalde beschlossen, liegen in weiter Ausdehnung die Spuren eines Dorfes dem forschenden Wanderer vor Augen. Stiller Friede der Natur lagert über der Fläche, wo einst reges Leben sich entfaltete; Rinder weiden, wo in früheren Zeiten der Mensch seine Feuerstätte aufgerichtet hatte, und der Pflug zieht lange Furchen quer durch die ehemaligen Häuserreihen. Wehmut überkommt den Wanderer, wenn sein Fuß auf die Reste der Wohnungen tritt, welche wohl eine glückliche Bevölkerung in sich aufgenommen und zu einer zahlreichen Gemeinde versammelt haben. Von diesem Wohlstande zeugen die weitgestreckten Hochäcker, aus denen nun kräftige Fichten emporgewachsen, und auf hohes Alter weisen die Feldmasen oder Beet-Breiten, welche zu einer Weite von fünf bis sechs Schuh sich ausdehnen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1507.2.3">Und mit einem Male ist das Unglück hereingebrochen, welches das Dorf von der Erde hinwegnahm, und die Erinnerung daran so sehr verwischte, daß selbst der Name des Ortes ungewiß ist. Die Einen nennen ihn Hagendorf, die Andern von seiner hohen Lage Hochdorf. Nach der Sage waren es die Hussiten, welche <pb n="449" xml:id="tg1507.2.3.1"/>
vor 430 Jahren, von Rötz her ziehend, soeben das Gotteshaus auf dem nahen Fahrenberge verwüstet hatten und nun ihre sengenden und brennenden Banden in die Umgegend aussendeten. Ein Zug berührte Hagendorf und brannte es nieder. Die Bewohner, schon früher von der Ankunft der Slaven unterrichtet, waren entflohen und hatten Alles zurücklassen müssen. Daher findet man in den Ruinen keine Menschengebeine, wohl aber verbrannte Thierknochen und Hausgeräthe aller Art. Ein Theil der Bevölkerung zog sich an den untern Fahrenberg hinüber und gründete dort neue Wohnsitze, meist in Einödhöfen, wie Bibershof, Pfifferlingstiel, Radwaschen, Birkenbühl. Das Kloster Waldsassen, die Grundherrschaft, hatte den Boden gegen Ueberlassung der alten Gründe abgetreten und ließ diese nun aufforsten. Nach anderer Meynung erbauten sich die Unglücklichen ein neues Dorf, auf welches sie den Namen des alten übertrugen, das heutige Hagendorf, etwa eine Stunde in südlicher Richtung gegen Waidhaus zu. Vom alten Dorfe bilden die einzigen Reste zwey Einödhöfe in einer südwestlich abfallenden Schlucht, Steinbach und Ramelsleiten. Vielleicht gehörte auch die heutige Hagenmühle unten an der Zott hieher.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1507.2.4">Es war den Barbaren leichte Mühe, in etlichen Stunden zu zerstören, was Jahrhunderte langem Fleisse, Entbehren und Mühsal aller Art sein Entstehen verdankte; denn die Häuser waren wie jetzt noch häufig in der Gegend von Holz erbaut und ruhten auf einer erhöhten Grundlage von Granitsteinen, welche noch jetzt <pb n="450" xml:id="tg1507.2.4.1"/>
die Brandmale vom verzehrenden Feuer tragen. In zwey langen Reihen, ziemlich dicht an einander, ziehen noch sichtbar die Brandhügel sich hin, welche die Häuser zurückliessen; unzweifelhaft ist ein Theil derselben schon zu Feld und Wiese gemacht und vom Pfluge eingeebnet worden. Ich vermute, daß diesen beyden Reihen wieder andere, mehr gegen Süden hin gelegen, entsprachen, um das Kirchlein, wovon sogleich unten, nicht offen stehen zu lassen. Jedes Haus hat neben sich den Backofen, aus Lehm geschlagen, und eine kleine Cisterne, das Vieh zu tränken.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1507.2.5">Am östlichen Ende der sichtbaren Reihe sind die Hügel besonders gut erhalten. Dort muß auch der Hafner gehaust haben; man findet an seiner Stätte viele Scherben ungebrannten Geschirres, Töpfe mit kreisrundem Boden, am Rande sich zu einem Vierecke erweiternd, an der Aussenseite schienenartig gefurcht; selbst Vorräthe zubereiteten Töpferthones kommen zu Tage. Am westlichen Ende, in einer Schlucht am Bache, wohnte der Müller; man stieß auf Bruchstücke behauener Mühlsteine. Aber nirgend eine Spur von Ziegeln. – Auffallend ist die Menge ausgegrabener Schafscheeren, welche auf starke Schafzucht deuten, und kleiner Hufeisen, hier Jasl-oder Eseleisen genannt. Man schreibt diese gewöhnlich den Schweden zu, darf aber mit mehr Recht annehmen, daß sie einem kleinen Schlage von Pferden angehörten, welche sich für das Hügelland gar wohl eigneten und daher einst gezogen wurden. – Nach den gefundenen Resten hatten die Räder keine eisernen Reife, <pb n="451" xml:id="tg1507.2.5.1"/>
sondern nur Schienen. Nicht selten stößt man auch auf Pfeilspitzen und Sporen mit grossen Rädern. Vor der ersten Häuserreihe, welche fast in gerader Linie von Osten nach Westen läuft, steht auf offenem Platze erhaben der steinerne Grundbau des Kirchleins, welches dem heiligen Johann dem Täufer geweiht gewesen seyn soll und groß genug war für eine Gemeinde von 400 bis 500 Seelen. Das Presbyterium ist gegen Osten gerichtet, die Thüre gegen Norden. Letztere war ganz von Eisen und befand sich später in dem Pflegamtsgebäude zu Bleystein, wo sie in dem grossen Brande verzehrt wurde. Nach ihrer Zeichnung möchte ich auf byzantinischen Bau des Kirchleins schliessen. Das Kirchenpflaster bestand aus kleinen Bruchsteinen, in schlechten Mörtel gelegt: denn Kalk ist hier theuer; gleichwohl ist es so hart wie Stein geworden. – Die Mauersteine sind und werden von den Leuten hinweggeführt: noch zu Anfang des Jahrhunderts war ein ansehnlicher Theil davon erhalten; doch ist auch jetzt noch ihre Führung gut kenntlich, und junge Fichten, welche auf den Mauerresten angeflogen sind, bilden eine natürliche Wand zum Schutze des ehemaligen Heiligtumes.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1507.2.6">Begreiflicher Weise meldet das Volk von hier vergrabenen Schätzen. Ein Ehepaar ging von der Gevatterschaft heim um Mitternacht, und sah hier eine Kufe, darauf eine Monstranze zwischen zwey brennenden Lichtern. Wieder grub Einer nach Schätzen: da brachte der Geist einen Sarg, und der Schatzgräber lief davon. Noch nicht lange ist es, daß eine vielberühmte Wahrsagerin <pb n="452" xml:id="tg1507.2.6.1"/>
der Gegend, die Stangenbäuerin, das Vorhandenseyn der Truhe bestättigte, das Erheben der Schätze aber von der Anwesenheit des Grundeigentümers abhängig machte. Dieser läßt sich aber nicht bewegen, beym Ausgraben gegenwärtig zu seyn.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1507.2.7">Der Kirchthüre gegenüber zeigen sich in der Häuserreihe die Spuren eines grössern viereckigen Gebäudes, wohl das Priesterhaus. Den Gottesdienst sollen Mönche aus Waldsassen, von ihrem Hospiz auf dem Fahrenberge aus, abgehalten haben. In diesem Vierecke ist ein verschütteter Brunnen, in welchem die Glocken des Kirchleins liegen sollen; um sie vor den Hussiten zu retten, wurden sie hineingeworfen. Nun hört man sie zeitweise unterirdisch läuten, gar wehmütig, daß sie nicht mehr die fromme Gemeinde von der Höhe des Thurmes zum Dienste des Herrn rufen können.</p>
                    <p rend="zenoPLm4n0" xml:id="tg1507.2.8">Noch eine Sage geht hier: in der Nähe floß ein Gnadenquell, eine Salzquelle; der Berg, wo er entsprang, heißt noch der Sulzberg, und die kupfernen Röhren sollen aus der dortigen Salzquelle bis nach Pfrentsch gelegt gewesen seyn. Da hütete einmal ein armes altes Weib am Berge ihre einzige Gais, und diese trank vom salzigen Brunnen, und das Salz tödtete das Thier. Voll Zornes lief die Alte in's Dorf und holte einen Napf Hirse, und warf ihn in den Salzquell mit der Verwünschung: »So viele Jahre, als Körnlein Hirse sind, sollst du versiegt und trocken seyn.« Und seitdem läuft der Fluß nicht mehr.</p>
                </div>
            </div>
        </body>
    </text>
</TEI>