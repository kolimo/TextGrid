<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg2033" n="/Literatur/M/Schönwerth, Franz/Sagen/Aus der Oberpfalz/Dritter Theil/Fünfzehntes Buch/Ende der Welt/4. Der kalte Baum">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>4. Der kalte Baum</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Schönwerth, Franz: Element 02257 [2011/07/11 at 20:24:42]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Franz Schönwerth: Aus der Oberpfalz. Sitten und Sagen 1–3, Band 3, Augsburg: Rieger, 1857/58/59.</title>
                        <author key="pnd:118610236">Schönwerth, Franz</author>
                    </titleStmt>
                    <extent>339-</extent>
                    <publicationStmt>
                        <date>1857/58</date>
                        <pubPlace>Augsburg</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1810" notAfter="1886"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg2033.2">
                <div type="h4">
                    <head type="h4" xml:id="tg2033.2.1">§. 4. Der kalte Baum.</head>
                    <p xml:id="tg2033.2.2">Wenn der Wanderer auf der Heerstrasse von Vohenstrauß nach Wernberg, in der Richtung von Ost nach West zieht, befindet er sich auf dem Grad eines langgestreckten Bergrückens, der zu beyden Seiten ziemlich steil abfällt, und unten rechts das liebliche Lärauthal, links das wildromantische Thal der schauerlichen Pfreimd bilden hilft. Sind diese Wasser jetzt auch nicht mehr <pb n="339" xml:id="tg2033.2.2.1"/>
bedeutend, so waren sie in der Vorzeit um so gewaltiger, da sie tiefe Schluchten in den harten Felsen zu graben vermochten. – Hebt sich das Auge, so sieht es sich bald in die Vergangenheit zurückversetzt beym Anblicke der trauernden Trümmer einst herrlicher Burgen, mit denen ringsum die Berghöhen gekrönt sind; vor Allem leuchten ihm die stolzen Mauern und Thürme der alten Veste der weyland durchlauchtigsten Landgrafen von Leuchtenberg entgegen, da wo der Grad gegen die Naab hin sich abdacht; und neben ihm läuft die Spur der alten Handelsstrasse, auf welcher ehedem in der Zeit regeren Verkehres die Landgrafen den Kaufleuten das Geleite gaben. Da nun, hart an der Strasse, zu linker Hand, steht ein einsamer Baum, eine Steinlinde, vor sich einen kleinen Teich, vielmehr Pfuhl, im Rücken einen Einödhof; hier weht der Wind Tag und Nacht, Sommer und Winter, in kalten Strömen, oft in der Stimme des heulenden Sturmes oder des grollenden Donners, und ewig bewegt sich das Laubdach des Baumes und theilt den Schauer des frierenden Wanderers. Darum heißt es hier: <hi rend="spaced" xml:id="tg2033.2.2.2">beym kalten Baum.</hi> Dieser steigt an 80 Fuß empor und beugt seine Krone dankbar über das Wasser, das ihn nährt und tränkt. Er war ein Doppelbaum und steht nur mehr zur Hälfte. In dem Stamme ist eine Nische ausgefault, groß genug um mehrere Menschen aufzunehmen. Sibylla Weis hat ihn gepflanzt, den Baum, <hi rend="spaced" xml:id="tg2033.2.2.3">den Niemand kennt,</hi> und gleich einer Vala von ihm ausgesagt, daß, wenn einst sein Ast stark genug seyn wird, um einen geharnischten <pb n="340" xml:id="tg2033.2.2.4"/>
Reiter mit sammt dem Rosse zu tragen, die Feinde aus Ost und West in zahllosen Heersäulen hier zusammen treffen werden. Dann werden sie sich eine Schlacht liefern und bis zur Mitternachtsstunde soll das Würgen währen, wovon so arges Blutvergiessen gegen Norden hin entsteht, daß es die Mühle im Thale bey Lind treibt. Davon heißt der Baum auch <hi rend="spaced" xml:id="tg2033.2.2.5">Schlachtenbaum.</hi> Die Rosse der Türken aber werden den Boden bedecken, so weit das Auge reicht, und den Gräuel einer Pest verbreiten, wie sie die Welt noch nicht gesehen. Alles Volk und Vieh fällt ihr zum Opfer. Zuletzt wird ein Hirt heranziehen aus weiter Ferne und in dem Baume Wohnung nehmen, seine zahlreiche Nachkommenschaft aber das öde Land auf's neue bevölkern und fortan in seligem Frieden und Wohlstande besitzen. Neuenhammer. Der Baum, den Niemand nennen kann, muß bleiben, bis Alles zu Grunde geht. Erbendorf.</p>
                    <p rend="zenoPLm4n0" xml:id="tg2033.2.3">Diese Sage ist voll mythischen Hauches: sie deutet auf den letzten Kampf der lichten Asengötter gegen die Feuerkinder, Locki und seinen Anhang, auf dem Schlachtfelde Wigrid, in der Nähe des Weltbaumes, der Weltesche, welche, den Weltenbrand überdauernd, ein einziges Menschenpaar, Lif und Lifthrasir, in sich verborgen hält, damit sie ein neues glücklicheres Geschlecht gründen, gleichwie der Hirt im kalten Baum, den ebenso <hi rend="spaced" xml:id="tg2033.2.3.1">die Feinde nicht vernichten können,</hi> sein Obdach findet und Vater eines neuen seligen Volkes wird. Um den Verband mit der Edda noch enger zu ziehen, ist an seiner Wurzel der Quellenteich, wie an der Weltesche <pb n="341" xml:id="tg2033.2.3.2"/>
der Brunnen der Urd, der Vergangenheits-Norne, hier der Sibylle.</p>
                    <p rend="zenoPLm4n0" xml:id="tg2033.2.4">Daß es die Ungläubigen sind, die Türken, welche am kalten Baum die furchtbare Niederlage erleiden, steht in allgemeiner Geltung. Dabey wird noch näher ausgeführt, daß der letzte derselben von einem Weibe mit der Schürgabel oder einem Scheite Holz erschlagen werden soll. Tännesberg. Türschenreut. Gegenstück hievon ist der letzte Schwede, der gleichfalls von des Weibes Hand fiel. Denn diese hatte von der allgemeinen Plünderung noch etwas Mehl gerettet. Sie wollte davon das letzte Brod backen und war eben mit dem Ausnehmen des Taiges beschäftiget, als der Schwede herantrat, um es ihr zu nehmen. Darüber gerieth die Arme in rasende Wuth und schlug den Räuber mit der Backschüssel tod. Waldkirch. Dieser Sage muß irgend eine Mythe zu Grunde liegen. Sie kommt unten noch einmal vor.</p>
                    <p rend="zenoPLm4n0" xml:id="tg2033.2.5">Ferner heißt es, die Schlacht werde vorfallen, wenn um den kalten Baum drey Höfe entstehen. Erst unlängst siedelte sich ein zweyter Bauer an. Tännesberg.</p>
                    <p rend="zenoPLm4n0" xml:id="tg2033.2.6">Ueber die Entstehung des kalten Baumes läuft aber noch eine andere Sage, welche theilweise an jene über die Gräfin von Orlamünde erinnert. Eine Landgräfin von Leuchtenberg nämlich, Wittwe mit zwey Kindern, aber noch jung und schön, hatte zu dem benachbarten Grafen von Sulzberg, der eben von einer Fahrt wider die Ungläubigen zurückgekehrt war, leidenschaftliche Neigung gefaßt, und ließ ihm durch einen Vertrauten <pb n="342" xml:id="tg2033.2.6.1"/>
hievon Kunde geben. Der Graf wies aber die Zumutung unwillig mit dem Bedeuten zurück: »Soll ich Kinder aufziehen, müssen sie meines Blutes seyn!« Da ließ die verblendete Mutter den beyden Kindern Nesteln in das Hemd knüpfen und sie starben. Darnach beschied sie den Grafen zu einer Unterredung. Auf der Höhe zwischen Sulzberg und Leuchtenberg kamen sie zusammen und der Graf beschwor das Weib, ihm die Wahrheit zu sagen auf seine Frage, ob ihre Kinder natürlichen Todes verblichen. Um die Höhe ihrer leidenschaftlichen Liebe kund zu geben, erwiderte die Gräfin: »Deinethalben mußten sie sterben!« Da entbrannte der edle Mann vor Zorn und stieß ihr mit den Worten: »So stirb du deiner Kinder wegen!« das Schwert in das Herz. Zur selben Stelle ließ er die unnatürliche Mutter begraben. Dabey fiel ihm aber ein Samenkorn, das er aus dem heiligen Lande mitgebracht, unversehens in das Grab und aus dem kalten Herzen entwuchs der kalte Baum. Als Geist wandert die Gräfin um ihr Grab und um den Baum: daher der stete Wind, der hier geht. Und so lange hat sie nicht Ruhe auf des Grafen Fluch, bis nicht der deutsche Kaiser, der aus der Oberpfalz aufstehen wird, die Schlacht schlägt gegen die Türken, in welcher das Blut bis an die unteren Zweige des Baumes steigen muß. Darum hat der Baum nicht seines Gleichen im Lande und keinen Namen, weil er aus fremder Ferne stammt. Waidhaus.</p>
                </div>
            </div>
        </body>
    </text>
</TEI>