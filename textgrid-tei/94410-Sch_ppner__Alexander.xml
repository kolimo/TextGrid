<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg783" n="/Literatur/M/Schöppner, Alexander/Sagen/Sagenbuch der Bayerischen Lande/Zweiter Band/769. Die Zwerge im Joßgrund">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>769. Die Zwerge im Joßgrund</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Schöppner, Alexander: Element 00782 [2011/07/11 at 20:29:18]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Alexander Schöppner: Sagenbuch der Bayer. Lande 1–3. München: Rieger 1852–1853.</title>
                        <author key="pnd:11915403X">Schöppner, Alexander</author>
                    </titleStmt>
                    <extent>283-</extent>
                    <publicationStmt>
                        <date notBefore="1852" notAfter="1853"/>
                        <pubPlace>München</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1820" notAfter="1860"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg783.2">
                <div type="h4">
                    <head type="h4" xml:id="tg783.2.1">
                        <pb n="283" xml:id="tg783.2.1.1"/>
769. Die Zwerge im Joßgrund.</head>
                    <milestone unit="head_start"/>
                    <p rend="zenoPC" xml:id="tg783.2.2">
                        <seg rend="zenoTXFontsize80" xml:id="tg783.2.2.1">A. v. <hi rend="spaced" xml:id="tg783.2.2.1.1">Herrlein</hi> die Sagen Spessarts S. 101.</seg>
                    </p>
                    <milestone unit="head_end"/>
                    <lb xml:id="tg783.2.3"/>
                    <p xml:id="tg783.2.4">Die fleißigen Zwerge waren auf der Wanderschaft. Sie hatten den Menschen redliche Dienste geleistet, hatten für sie geschafft, wie Leibeigene, hatten gegraben, gesäet, geärntet, aufgebaut und niedergerissen, wie man es gewünscht; aber sie hatten nichts davon gehabt, als die traurige Ueberzeugung, daß das uralte Sprichwort: »Undank ist der Welt Lohn,« leider nur zu wahr sei. Darum waren sie auf der Wanderschaft, sie wollten den Undank, den sie überall geärntet, nicht länger ertragen, sondern lieber in ein fernes, unbewohntes Land ziehen und allen Umgang mit den Menschen aufgeben, so schmerzlich sie ihn auch vermissen würden; denn die Zwerge haben die Menschen sehr lieb und wohnten unter ihnen, so lange es nur immer geht.</p>
                    <p rend="zenoPLm4n0" xml:id="tg783.2.5">Auf ihrer Wanderschaft waren die Zwerge in den Spessart und endlich in den Joßgrund gekommen. Damals war der Spessart nicht so bevölkert, wie jetzt, und tagelang waren die Zwerge gezogen, ohne auf eine menschliche Wohnung zu stoßen; die geringen Mundvorräthe, die sie mitgenommen hatten, waren bald aufgezehrt, der tausendjährige dichte Eichwald ließ in seinem Schatten weder genießbare Wurzeln noch Früchte wachsen und die Zwerge litten den bittersten Mangel. Sie schleppten sich weiter, so lange sie die matten Beine tragen konnten; als es nicht mehr ging, lagerten sie sich in das hohe Heidekraut und sahen ergeben ihrem Tode entgegen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg783.2.6">Da kam ein Bauersmann des Wegs. Er hatte sich ein Bund Holz im Walde geholt und kehrte eben heim. Er mußte durch die Heide, sein Fuß strauchelte über einen der kleinen Leute, den er beinahe zertreten hätte, denn obwohl sie längst ihre Nebelkappen abgeworfen hatten und deßhalb sichtbar waren, verbarg sie doch das Heidekraut seinen Augen. Erschrocken prallte er zurück, dann aber füllte tiefes Mitleid seine Seele, als er den erbärmlichen Zustand der Zwerge sah; er brauchte nicht zu fragen, was ihnen fehle: der helle Hunger schaute aus ihnen. Er forderte sie auf, ihre letzte Kraft zusammen zu nehmen und ihm zu folgen; er sei zwar ein armer Mann, mehr mit Kindern, als mit Glücksgütern gesegnet, aber ein Stückchen Brod werde sich doch noch für sie finden – und in <pb n="284" xml:id="tg783.2.6.1"/>
seinem Keller sei mehr Platz, als ihm lieb. Die Zwerge wurden durch die Hoffnung ihrer Rettung neu belebt und folgten dem Manne zu seiner Hütte, die zum Glücke nicht sehr entfernt war. Dort quartierten sie sich ein in den leeren Keller; sie erhielten von dem Bauersmanne, was seine Armuth vermochte, und in einigen Tagen hatten sie sich wieder erholt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg783.2.7">Als die Zwerge aus dem Keller hervorkamen und sahen, wie sich der Mann abmühte, um auf einem Stückchen steinigen Landes ein paar Getreidehalme zu erzielen; wie er sich plagte, in dem Walde oder von dem sumpfigen Grunde eine Handvoll Gras für die mageren Kühe, deren Milch seinen zahlreichen Kindern die karge Nahrung gab, zusammen zu bringen, hatten sie alle Unbilden vergessen, die ihnen die Menschen angethan. Sie hatten mit einem einfachen »Vergelt's Gott!« – und das war mehr, als ihnen je die Menschen gegeben – scheiden wollen, aber nun sprachen sie zu dem Manne: »Du hast uns beherbergt und gespeist mit dem, was du dir und deinen Kindern entziehen mußtest, das werden wir dir vergelten. Wir sind nicht so schwach, wie du uns ansiehst; in uns lebt nur ein Wille und darum sind wir zusammen stark, wie Riesen. Wir werden dich in deiner Landwirthschaft unterstützen und du wirst mit uns zufrieden sein, aber bleibe du auch freundlich gegen uns, wie du es bisher warst!« Der Bauersmann hatte zwar kein großes Vertrauen auf die Riesenkraft der kleinen Bürschchen, aber er dachte: »wenn's auch nicht viel nützt, kann's doch nicht schaden,« und ließ sie nach ihrem Gutdünken schalten und walten.</p>
                    <p rend="zenoPLm4n0" xml:id="tg783.2.8">Am Tage blieben die Zwerge in ihrem Keller; aber sobald es Nacht geworden, wurde es dort lebendig, wie in einem Ameisenhaufen – und wenn der Bauersmann Morgens aus seiner Hütte trat, fand er bald einen berghohen Haufen des besten Grases vor seiner Thüre liegen, bald eine Arke Holz; bald sah er einen großen Sumpf mit Abzugsgräben versehen, und zu den schönsten Wiesen angelegt, bald ein großes Stück Wald gerodet, und von den Baumwurzeln und dem Steingerölle gereinigt, daß er es nur einzusäen brauchte, um einer guten Ernte gewiß zu sein. Bei seinen nunmehr ausgedehnten Besitzungen fand er leicht die Mittel, sich einen größeren Viehstand anzuschaffen; die Zwerge bauten ihm die Stallungen und das Vieh gedieh bei dem ausgesuchten Futter wunderbar – und als die Zeit der Ernte kam, fiel sie so reichlich aus, daß sie zehn so kleine Scheuern, wie der Bauer eine besaß, gefüllt hätte. Auch da halfen die Zwerge; sie bauten ihm eine schöne große Scheuer, wie er<pb n="285" xml:id="tg783.2.8.1"/>
 noch keine im Traume gesehen. Jetzt war dem Manne auch die Wohnung zu klein; er durfte nur den Wunsch äußern und die Zwerge bauten ihm zwei Häuser, die Pallästen glichen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg783.2.9">Der Mann war nun der reichste Bauer im Spessart. Er nahm eine Menge Knechte und Mägde, obgleich er sie der Zwerge wegen nicht gebraucht hätte, und lebte wie ein Fürst. Die Zwerge aber wohnten nach wie vor in dem Keller der Hütte und begnügten sich mit der früheren einfachen Kost.</p>
                    <p rend="zenoPLm4n0" xml:id="tg783.2.10">So vergingen einige Jahre. Als die Zwerge nichts mehr für den Mann zu thun hatten, kamen sie zu ihm, und baten, er möge ihnen gestatten, daß sie auf seinem Eigenthum ein Haus für sich selbst erbauten; der Keller, in dem sie wohnten, sei doch gar zu dumpfig und unfreundlich. Mit dem Reichthume war der Mann hart geworden, wie die Felsen des Spessarts. Er fuhr die Zwerge zornig an: »was sie mit dem Hause thun wollten? Das nehme ihm zu viel Platz weg. Hätten sie bisher im Keller gewohnt, so könnten sie auch ferner da wohnen. Ein neues Haus für sie sei reine Verschwendung; so kleines Volk brauche gar kein großes Haus – und wenn ihnen ihre jetzige Wohnung nicht recht sei, so könnten sie sich weiter packen; er habe sie ohnehin lange genug gefüttert.« Die Zwerge waren erst überrascht von einer Antwort, die sie nicht im Entferntesten erwartet hatten, aber bald weckte der neuerliche Undank ihren alten Groll gegen die Menschen. Sie verließen den Keller und zogen sich in eine benachbarte Mühle. Von dort aus kamen sie nächtlicher Weile und holten das Getreide aus der Scheuer des Bauern, denn die liebe Gottesgabe wollten sie nicht verderben. Sie mahlten es auf der Mühle und verschenkten das Mehl an arme Leute. Dann zündeten sie die Scheuer und die Wohnhäuser und die Stallungen des Bauern an und die Flammen verzehrten sein Hab und Gut; die Felder, die sie selbst gerodet, bewarfen sie mit Steinen, daß der Bauersmann drei Menschenalter gebraucht hätte, um sie wieder wegzuschaffen; die Abzugsgräben der Wiesen verstopften sie, daß der alte Sumpf wieder entstand – der Bauer ward der arme Mann, der er vormals gewesen und der nichts besaß, als seine alte Hütte, sein Stückchen Feld und ein paar magere Kühe und sein kümmerliches Brod aß und Wasser trank bis an sein Ende. Die Zwerge aber wanderten weiter; wo sie hingekommen, ist nicht bekannt worden.</p>
                </div>
            </div>
        </body>
    </text>
</TEI>