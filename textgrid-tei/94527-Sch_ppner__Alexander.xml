<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg900" n="/Literatur/M/Schöppner, Alexander/Sagen/Sagenbuch der Bayerischen Lande/Zweiter Band/886. Das Pestmännlein">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>886. Das Pestmännlein</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Schöppner, Alexander: Element 00899 [2011/07/11 at 20:29:18]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Alexander Schöppner: Sagenbuch der Bayer. Lande 1–3. München: Rieger 1852–1853.</title>
                        <author key="pnd:11915403X">Schöppner, Alexander</author>
                    </titleStmt>
                    <extent>421-</extent>
                    <publicationStmt>
                        <date notBefore="1852" notAfter="1853"/>
                        <pubPlace>München</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1820" notAfter="1860"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>other</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg900.2">
                <div type="h4">
                    <head type="h4" xml:id="tg900.2.1">886. Das Pestmännlein.</head>
                    <milestone unit="head_start"/>
                    <p rend="zenoPC" xml:id="tg900.2.2">
                        <seg rend="zenoTXFontsize80" xml:id="tg900.2.2.1">Nach J. F. <hi rend="spaced" xml:id="tg900.2.2.1.1">Lentner,</hi> Geschichten aus den Bergen S. 104.</seg>
                    </p>
                    <milestone unit="head_end"/>
                    <lb xml:id="tg900.2.3"/>
                    <p xml:id="tg900.2.4">Vor Jahren hatte das Stift zu Rottenbuch sein eigen Recht und Land und ließ seine Leute vor dem eigenen Stuhle richten, selbst auf Leben und Tod. Sie hielten sich dazu einen eigenen Richter, und da war denn auch einmal ein gar schlimmer und scharfer, der für die Chorherrn Schwert und Wage handhabte, aber weit lieber mit dem einen darein schlug, als auf das richtige Zeigen des Züngleins an der andern wartete.</p>
                    <p rend="zenoPLm4n0" xml:id="tg900.2.5">Einmal, nach einer großen Tafelei, es war gerade des Herrn Prälaten Namenstag, lag der Richter in seiner Behausung wie ein Stückfaß auf dem Lotterbett und schnaubte und athmete, als wollte er zur Stunde ersticken, denn er hatte sich das Bankett zu wohl behagen lassen. In seinem Taumel hatte er lange nicht bemerkt, daß ein Mensch vor ihm stand, ihm zusah in seinen Nöthen und dabei lachte, so gut es sein saures<pb n="421" xml:id="tg900.2.5.1"/>
 Gesicht erlaubte. Der Mensch war ein schmutziger Bauer, mit nußbrauner Haut und einer Igelperücke, knochigen Leibs und kaum in ein paar Lederfetzen gewickelt. Man hieß den wilden Gesellen den »Filzdraken,« weil er wie ein Drache im öden Forst am Filze<ref type="noteAnchor" target="#tg900.2.13">
                            <anchor xml:id="tg900.2.5.2" n="/Literatur/M/Schöppner, Alexander/Sagen/Sagenbuch der Bayerischen Lande/Zweiter Band/886. Das Pestmännlein#Fußnote_1"/>
                            <ptr cRef="/Literatur/M/Schöppner, Alexander/Sagen/Sagenbuch der Bayerischen Lande/Zweiter Band/886. Das Pestmännlein#Fußnoten_1"/>
                            <hi rend="superscript" xml:id="tg900.2.5.3.1">1</hi>
                        </ref> sich eine Lehmhütte gebaut und dorten in Noth und Elend hauste mit Weib und Kind.</p>
                    <p rend="zenoPLm4n0" xml:id="tg900.2.6">Der Drak also brummte ein paarmal etwas in den Bart und machte so seine Gegenwart kund, daß darob der Klosterrichter aus seinem schweren Schlaf erwachte. Wie er nun den Bauern vor sich sah, erschrak er sichtlich, denn er hatte den Mann, einen Zinspflichtigen des Stifts, kürzlich im Uebermuth und um schlechten Vorwand hart gebüßt und ihm die einzige Kuh aus dem Stall getrieben. Schnell aber nahm er sich wieder zusammen und wie das die Gerichtsherrn im Brauch haben, wenn ihnen ein Untergebener vorkommt, dem sie Unrecht gethan, ward er grob und begann den Filzdraken zu inquiriren, was er hier in der Stube suche, wie er gleich Diebsgesindel hereingekommen, weßhalb er ihn böswillig erschrecke, und mehr dergleichen, wobei er ihm schließlich mit Keuche und Ruthenstreichen drohte. Der Filzdrak ließ sich aber des Gestrengen Zorn nicht irren, stellte sich steif vor ihn hin und hielt ihm mit einer Feuerzange einen alten, zerdrückten Bauernhut hin, ohne ein Wort zu sagen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg900.2.7">»Was sollen die Narretheidungen!« grollte da der Vogt, »weßhalb bringt Er den Hut in der Zange? Was treibt Er für Gespötte mit mir?« Dabei riß er dem Bauern den Hut weg, zerdrückte ihn mit beiden Fäusten und trat dann mit den Füßen darauf herum. Der Filzdrak aber öffnete sein breites Maul und begann faul und eintönig: »Ich habe Ew. Gestrengen nur berichten wollen, wie es sich mit dem Hute verhält, weil das eine besondere Sache ist und mir viel zu bedeuten deucht. Mein Bub der Jürgenatz (Georg Ignatz), hütet die letzten zwei Gaisen, die Ew. Gnaden mir noch übrig gelassen, müßt Ihr wissen, und die trieb er heute in das Wäldlein gegen die Wildsteig hin. Da saß der Bube und weinte, weil ihn hungerte, und er glaubte, man müsse dann essen. Er ist noch dumm und weiß nicht, daß Ew. Gestrengen es nicht leiden mögen, wenn wir Bauern satt sind, und uns darum das Tischtuch kürzen. Wie er so heulte, kam mit einem Male ein wunderliches Männlein, schier nackend, mit einem Laubgürtel um die Lenden und ein Hütlein auf, aus dem Wald gelaufen, und ehe mein Bub vor Angst und Furcht entlaufen konnte, <pb n="422" xml:id="tg900.2.7.1"/>
hatte es ihn erwischt und hielt ihn am Kittel fest. Das Männlein, sagt der Bube, war käsebleich und gelb, zottig von Haaren, sah darein mit gläsernen Augen und krächzte ein Kauderwelsch mit weinerlicher Stimme. Meinem Jürgenatz ward todtenübel, er riß sich mit Gewalt los und rannte heimwärts; das Männlein aber sprang ihm nach eine gute Weile und schrie dazu: ›Wehe und aber wehe!‹ daß es wiederhallte im Holz. Wie mir der Bube die Mähre vorgekeucht, laufe ich waidlich hinaus, das Männlein zu sehen. Es war aber verkommen, und sein Hut lag am Boden, den es meinem Buben hatte schenken wollen. Worauf mir schnell beifiel, ich habe einmal gehört, wie damals, als der große Sterb gewüthet im Land, ebenfalls ein nackend Weib zu einem Hirtenmädel aufs Feld gekommen und ihm ein Paar Strümpfe geschenkt habe; wie dann also gleich die Dirn an der Pestilenz verstorben und mit ihr viel tausend Menschen, die allein im wilden Freithof liegen oder bei Sanct Ruperts Münster, das nun zusammengefallen ist. Da sprang ich in meiner Einfalt zu Ew. Gnaden und wollte vermelden, was geschehen, und wie ich fest glaube, daß dieß Wesen das Männlein ist vom selbigen Weiblein, und alsbald ein großer Sterb und Todfall anheben wird. In dem Hute hat es uns die Pest gebracht, und darum habe ich ihn auch nur mit der alten Feuerzange angefaßt, sintemalen ich gar gut weiß, daß man sie erbt, wenn man auch nur mit der Fingerspitze ein verpestet Ding berührt.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg900.2.8">Kaum hatte der Filzdrak dieß Wort gesprochen, so hättet Ihr sehen sollen, wie der Vogt von Neuem erblaßte und hinsank in die Kissen. Er hatte ja den Hut des Pestmännleins mit beiden Händen erfaßt; er wußte, daß es wahr sei, was der Bauer von dem Pestweiblein erzählt, denn er hatte es in einer Chronika gelesen, die im Kloster lag. Er fühlte sich mit einem Male todtkrank und elend; er hatte die Pest. Der Filzdrak, als er des gestrengen Herrn Uebelbefinden vermerkte, lachte boshaft; jener aber griff nach der silbernen Pfeife auf dem Tischlein und wollte den Fronknecht rufen, damit er den Boten des Todes fasse. Der Filzdrak aber spürte, wo das hinaus sollte, schlug ihm die Pfeife aus der Hand mit der Eisenzange, lupfte seine Lederkappe und ging von dannen, indem er noch als B'hüt Gott zur Thür hineinrief: »Ich wünsch Euch wohl zu sterben, gestrenger Herr!«</p>
                    <p rend="zenoPLm4n0" xml:id="tg900.2.9">Dem Vogt aber ward noch erbärmlicher zu Muth, und er legte sich den Abend noch hin und starb unter unsäglichen Martern gerade um zwölf Uhr Nachts. Er konnte bald nicht mehr reden, nicht beichten, noch<pb n="423" xml:id="tg900.2.9.1"/>
 beten, sondern fuhr hin in seinen Sünden, voll Grimm und Wuth auf seinem rothen stolzen Gesicht. Bei seinem Tode entstand ein großes Geschrei, und allgemein ward die Furcht, daß die Pestilenz wieder losbreche. Man begrub darum den bösen Pfleger von Rottenbuch in dem wilden Freithöfte, ohne Segen und Weihbrunnen, ohne Licht und Leucht. Der Schinder mußte ihn verscharren.</p>
                    <p rend="zenoPLm4n0" xml:id="tg900.2.10">Darauf erzählte man überall von dem »Pestmännlein.« Mancher hatte es gesehen, wenn er durch einen Wald ging; die Hirten am Feld schreckte es, und wild schreiend lief es Einzelnen nach.</p>
                    <p rend="zenoPLm4n0" xml:id="tg900.2.11">Bald war es da, bald dort. Einer hatte es am Berg<ref type="noteAnchor" target="#tg900.2.15">
                            <anchor xml:id="tg900.2.11.1" n="/Literatur/M/Schöppner, Alexander/Sagen/Sagenbuch der Bayerischen Lande/Zweiter Band/886. Das Pestmännlein#Fußnote_2"/>
                            <ptr cRef="/Literatur/M/Schöppner, Alexander/Sagen/Sagenbuch der Bayerischen Lande/Zweiter Band/886. Das Pestmännlein#Fußnoten_2"/>
                            <hi rend="superscript" xml:id="tg900.2.11.2.1">2</hi>
                        </ref> gesehen, der Andere traf es am Tastwalde am Lech. Da bekehrten sich die Menschen in ihrer Todesangst, die Herren wurden barmherziger gegen die Bauern, und allzusammen beteten um Abwendung der großen Noth zu Gott und Sanct Sebastian. – Es kam auch Niemand mehr um, denn einzig der gewaltthätige Vogt, den der Herrgott getroffen hatte mit seinem starken Arm. Gar viel Leut meinen auch, seine Seele habe keinen Frieden, und er geiste mit dem neidigen Schaffner, der den Armen das Brod zu klein gab, unten in den steinernen Stuben am Strausberg in der Amperleite. Das Pestmännlein aber verschwand, und seitdem hat man's nicht wieder gesehen.</p>
                </div>
                <div type="footnotes">
                    <head type="h4" xml:id="tg900.2.12">Fußnoten</head>
                    <note xml:id="tg900.2.13.note" target="#tg900.2.5.2">
                        <p xml:id="tg900.2.13">
                            <anchor xml:id="tg900.2.13.1" n="/Literatur/M/Schöppner, Alexander/Sagen/Sagenbuch der Bayerischen Lande/Zweiter Band/886. Das Pestmännlein#Fußnoten_1"/>
                            <ref cRef="/Literatur/M/Schöppner, Alexander/Sagen/Sagenbuch der Bayerischen Lande/Zweiter Band/886. Das Pestmännlein#Fußnote_1" xml:id="tg900.2.13.2">1</ref> Moorland mit niederem Buschwerk.</p>
                    </note>
                    <note xml:id="tg900.2.15.note" target="#tg900.2.11.1">
                        <p xml:id="tg900.2.15">
                            <anchor xml:id="tg900.2.15.1" n="/Literatur/M/Schöppner, Alexander/Sagen/Sagenbuch der Bayerischen Lande/Zweiter Band/886. Das Pestmännlein#Fußnoten_2"/>
                            <ref cRef="/Literatur/M/Schöppner, Alexander/Sagen/Sagenbuch der Bayerischen Lande/Zweiter Band/886. Das Pestmännlein#Fußnote_2" xml:id="tg900.2.15.2">2</ref> Den Berg nennt man in Lechrain vorzugsweise den Peissenberg mit seiner herrlichen Aussicht.</p>
                    </note>
                </div>
            </div>
        </body>
    </text>
</TEI>