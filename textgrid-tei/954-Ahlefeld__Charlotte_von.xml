<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns="http://www.tei-c.org/ns/1.0" xml:id="tg178" n="/Literatur/M/Ahlefeld, Charlotte von/Erzählungen/Gesammelte Erzählungen/Zweiter Band/Die Königstochter aus der Fremde">
    <teiHeader xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:a="http://www.textgrid.info/namespace/digibib/authors" xmlns:fn="http://www.w3.org/2005/xpath-functions">
        <fileDesc>
            <titleStmt>
                <title>Die Königstochter aus der Fremde</title>
            </titleStmt>
            <publicationStmt>
                <idno type="FileCreationTime">Ahlefeld, Charlotte von: Element 00092 [2011/07/11 at 20:27:40]</idno>
                <availability>
                    <p> Der annotierte Datenbestand der Digitalen Bibliothek inklusive Metadaten sowie
                                   davon einzeln zugängliche Teile sind eine Abwandlung des Datenbestandes von
                                   www.editura.de durch TextGrid und werden unter der Lizenz Creative Commons
                                   Namensnennung 3.0 Deutschland Lizenz (by-Nennung TextGrid, www.editura.de) veröffentlicht. Die
                                   Lizenz bezieht sich nicht auf die der Annotation zu Grunde liegenden
                                   allgemeinfreien Texte (Siehe auch Punkt 2 der Lizenzbestimmungen). </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/legalcode">Lizenzvertrag</ref>
                    </p>
                    <p>
                        <ref target="http://creativecommons.org/licenses/by/3.0/de/"> Eine vereinfachte
                                   Zusammenfassung des rechtsverbindlichen Lizenzvertrages in
                                   allgemeinverständlicher Sprache </ref>
                    </p>
                    <p>
                        <ref target="http://www.textgrid.de/Digitale-Bibliothek">Hinweise zur Lizenz und zur Digitalen Bibliothek</ref>
                    </p>
                </availability>
            </publicationStmt>
            <sourceDesc>
                <biblFull>
                    <titleStmt>
                        <title>Charlotte von Ahlefeld: Gesammelte Erzählungen, 2 Bände, Band 2, Schleswig: Königl. Taubstummen-Institut, 1822.</title>
                        <author key="pnd:118962590">Ahlefeld, Charlotte von</author>
                    </titleStmt>
                    <extent>207-</extent>
                    <publicationStmt>
                        <date when="1822"/>
                        <pubPlace>Schleswig</pubPlace>
                    </publicationStmt>
                </biblFull>
            </sourceDesc>
        </fileDesc>
        <profileDesc>
            <creation>
                <date notBefore="1781" notAfter="1849"/>
            </creation>
            <textClass>
                <keywords scheme="http://textgrid.info/namespaces/metadata/core/2010#genre">
                    <term>prose</term>
                </keywords>
            </textClass>
        </profileDesc>
    </teiHeader>
    <text>
        <body>
            <div type="text" xml:id="tg178.3">
                <div type="h4">
                    <head type="h4" xml:id="tg178.3.1">Die Königstochter aus der Fremde</head>
                    <head type="h4" xml:id="tg178.3.2">Legende.</head>
                    <p xml:id="tg178.3.3">Zu der Zeit, als der fromme Bischof Basilius in Rom auf dem päbstlichen Stuhl saß, verbreitete sich in alle Länder das Gerücht seines heiligen Wandels, und seiner nimmer befleckten Keuschheit und Tugend.</p>
                    <p rend="zenoPLm4n0" xml:id="tg178.3.4">In stillen Unterhaltungen mit Gott und seinen Heiligen floß gleich einem anhaltenden Gebet sein Leben dahin, und wenn weltliche Geschäfte ihn zuweilen aufriefen aus dem Feuer seiner Andacht, so kehrte er seine Gedanken nur auf das irdische wüste Treiben der Welt, um die Menschen durch Wohlthaten oder Trost zu beglücken. Daher durfte frei die Armuth ihm nahen, und Leiden des Körpers und der Seele fanden Linderung bei seiner unbegränzten Barmherzigkeit.</p>
                    <p rend="zenoPLm4n0" xml:id="tg178.3.5">
                        <pb n="207" xml:id="tg178.3.5.1"/>Als er auch einst in seiner verschwiegenen Kammer vor Gott sich demüthigte in stillem Gebet, meldete man ihm, daß eine Jungfrau auf der Schwelle des Pallastes kniee, und bei ihm zu beichten begehre.</p>
                    <p rend="zenoPLm4n0" xml:id="tg178.3.6">Der fromme Basilius, der nicht Zeit hatte, den oft sträflichen Heimlichkeiten der Beichte sein Ohr zu leihen, ließ sie deshalb an die Priester verweisen, denen das Amt oblag, die sündhaften Gewissen der Menschen von ihren Schmerzen zu entbinden, allein die Jungfrau erwiederte: daß sie weit aus der Ferne hergekommen sey, um dem hochverehrten Bischof Basilius die Tiefe ihres Busens aufzuschließen, und daß sie nur ihm allein ihre Geheimnisse zu vertrauen und nur aus seinem Munde Beruhigung zu erlangen vermöge.</p>
                    <p rend="zenoPLm4n0" xml:id="tg178.3.7">Hierauf willfahrte der Bischof ihrem Begehren, und erhob sich von seinen Knieen, die Jungfrau zu empfangen, die sittsam und verschämt herein trat, im dunklen Pilgerkleide, den mit Muscheln geschmückten Hut tief in das rosige Antliz gedrückt, und den Stab der Wallfahrt in ihren bebenden Händen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg178.3.8">Sie warf sich nieder vor dem frommen Bischof, und küßte, von ihren Thränen benetzt, seine ihr dargebotene Rechte.</p>
                    <p rend="zenoPLm4n0" xml:id="tg178.3.9">Wer bist du, meine Tochter? fragte Basilius, sie liebreich aufhebend, und welche Bürde belastet bei so jungen Jahren schon dein Gewissen, daß du meinst, nur <hi rend="spaced" xml:id="tg178.3.9.1">ich selbst</hi> könne sie dir vergeben?</p>
                    <p rend="zenoPLm4n0" xml:id="tg178.3.10">Heiliger Vater, versetzte die Fremde, drückt denn allein die Sünde den Geist des Menschen nieder, oder vermag nicht auch der Widerwärtigkeiten Last ihn zu<pb n="208" xml:id="tg178.3.10.1"/>beugen? Kein sträflicher Wandel, der Buße heischt, führt mich zu Euren Füßen, sondern nur das Verlangen, daß Ihr mich schützen möget gegen jegliche Unbill, und der Wunsch, daß meine fromme, aber zaghafte Gesinnung sich stärken und befestigen mag in Eurer heiligen Nähe. Denn wisset, ich bin eines Königs Tochter, und weit aus der Fremde her hab ich an diesem Stabe einen beschwerlichen Weg durchpilgert, Euer hochverehrtes Angesicht zu schauen. O nicht zum Erstenmahl erblicken Euch heute meine Augen.<hi rend="spaced" xml:id="tg178.3.10.2"> So</hi> dachte ich mir Eure milden Züge, wenn ich nachsann, welche Gestalt der Himmel wohl gewählt haben möchte, um sie als Hülle der edelsten Seele zu ehren.<hi rend="spaced" xml:id="tg178.3.10.3"> So</hi> strahlten gleich Sternen in dunkler Nacht, Eure Blicke Muth und Hoffnung in mein umwölktes Gemüth, ehe ich ihnen noch in der Wirklichkeit begegnete – <hi rend="spaced" xml:id="tg178.3.10.4">so</hi>, heiliger Vater, <hi rend="spaced" xml:id="tg178.3.10.5">so</hi> und nicht anders, wie ich Euch vor mir sehe, seyd Ihr mir lange schon erschienen, wachend oder im Traume, wenn ich allein war, und schon damals ging mein ganzes Herz vor Euch auf, als Ihr mir nur nahe waret in meinen Gedanken, die sich stets mit Euch beschäftigten.</p>
                    <p rend="zenoPLm4n0" xml:id="tg178.3.11">Wenn es mir, dem demüthigen Knecht Gottes, vergönnt seyn soll, das Gute in Dir vor Wanken zu bewahren, so weiß ich es Deiner Einbildungskraft Dank, die dich schon früh so wunderbar mit meinem Anblick vertraut hat, entgegnete der Bischoff. Aber laß hören, welche Unbill Du denn befürchtest auf Erden, und worin ich Deine zaghafte Gesinnung zu befestigen vermag.</p>
                    <p rend="zenoPLm4n0" xml:id="tg178.3.12">
                        <pb n="209" xml:id="tg178.3.12.1"/>Edler Herr, sprach die Pilgerin, ich habe Euch schon gesagt, daß ich aus fürstlichem Geblüt entsprossen bin. Auserzogen in allen Wollüsten des Throns strebten meine königlichen Eltern, von der Welt verblendet, meinen Sinn nur auf die vergängliche Krone zu richten, durch die man herrschet, dieweil man lebet. Aber zur Demuth neigte sich stets mein Gemüth, und frühe regte sich schon Ekel an irdischem Thun und Lassen in meinem Inneren. Als ich nun heranblühte zu jungfräulichen Jahren gedachte der König, mein Vater, mich zu vermählen. Gewohnt, mich seinen Wünschen mit kindlichem Gehorsam, obschon oft mit geheimen Widerstreben zu fügen, wagt' ich keine Weigerung, wiewohl ich lieber in klösterlichen Mauern meine Tage der Einsamkeit gewidmet hätte. Als daher der königliche Prinz, dem ich bestimmt war, an meines Vaters Hof kam, um mich zu werben, nahm ich zitternd das Ringlein von seiner Hand, und ging dann in den dunkelen Wald, zu weinen, und um ein nachgebendes Herz zu beten.</p>
                    <p rend="zenoPLm4n0" xml:id="tg178.3.13">Als ich nun knieete unter einer Eiche, die ihre knorrigen Aeste weit umher verbreitete, rauscht es plötzlich in ihrem Wipfel, und wie ein grüner Regen säuselten unzählige Blätter auf mich hernieder, mich zu wecken aus dem seligen Rausch meiner Andacht. Da blickt' ich empor, und sah ein Dunstbild, gleich dem heiligen Andreas gestaltet, oben in den Lüften zwischen den Zweigen des Baumes schweben. Das rief mit ernster Stimme zu mir herab: »Siehe wohl zu, wem Du Dich vermählst mit Leib und Seele, denn Fluch lastet auf der Ehe, die nicht Liebe knüpft, <pb n="210" xml:id="tg178.3.13.1"/>und<hi rend="spaced" xml:id="tg178.3.13.2"> dieser</hi> ists allein auf Erden, den Du lieben kannst.«</p>
                    <p rend="zenoPLm4n0" xml:id="tg178.3.14">Da schaut' ich wie in einem Spiegel Euer Antlitz, heiliger Vater, das freundlich, gleich der Morgensonne, mir lächelte, und plötzlich umgeschaffen dünkte mir das Innerste des Busens, denn das Sehnen nach Abgeschiedenheit und Stille hatte sich in glühendes Verlangen nach Eurem Wiedersehn, und nach der Süßigkeit Eurer Liebe verwandelt.</p>
                    <p rend="zenoPLm4n0" xml:id="tg178.3.15">Du thust nicht wohl, meine Tochter, erwiederte Basilius ernst, daß du so unziemend die strafbaren Wünsche deines Herzens in Worte kleidest; doch bist du gekommen, der weltlichen Eitelkeit Ade zu sagen, und das Aergerniß eines solchen Geständnisses durch Reue und Buße zu versöhnen, so sieh mich bereit, Dir beizustehen in Allem, was den Schiffbruch Deiner Seele verhindern kann. Denn es ist nicht genug, den Leib vor schnöder Entweihung zu hüten; auch das Gemüth muß sich rein erhalten in Worten und Gedanken, vor Gott und vor sich selbst.</p>
                    <p rend="zenoPLm4n0" xml:id="tg178.3.16">Als der Bischoff so sprach, entfiel der Pilgerin der leicht befestigte Hut, und unverhüllt stand sie da in rosiger Jugendschönheit, edle Perlen der Rührung im funkelnden Auge, und wie von einer Glorie von goldenen Locken umflossen. Der Bischoff schauderte fast, ob ihrer unaussprechlichen Anmuth. <hi rend="spaced" xml:id="tg178.3.16.1">So</hi> hatte nimmer Weiberschönheit noch die fromme Ruhe seines Herzens in stürmische Bewegung gebracht – doch er wandte den Blick strenge und enthaltsam von ihr ab, und fuhr fort, wiewohl mit sanfterem Ton der Stimme: »Du hast noch nicht <pb n="211" xml:id="tg178.3.16.2"/>geendet, edle Jungfrau! Sage mir, was ferner noch geschah, und wie Du dem Werben Deines Verlobten, und dem Gebote Deiner Eltern Dich entzogest?</p>
                    <p rend="zenoPLm4n0" xml:id="tg178.3.17">Als ich zurückkehrte in meines Vaters Schloß, sprach die Königstochter, erklärte ich den Meinigen, daß ich nimmer meine Hand dem Prinzen geben könne, weil mein Herz einen Bräutigam erkohren habe, dem ich allein auf Erden anzugehören wünsche.</p>
                    <p rend="zenoPLm4n0" xml:id="tg178.3.18">Da ergrimmte mein Herr Vater sehr, und fuhr mich an mit rauhen Worten, und als ich auf meinem Entschluß beharrete, schlug er mich sogar, daß mein Blut unter seinen Streichen zur Erde floß. Doch mir dünkte seine strafende Hand leicht, denn ich litt ja um den, dessen Bildniß liebreich in meinem Busen schimmerte. Wie nun weder Drohungen noch Züchtigungen, noch selbst das zärtliche Flehen meiner vielgeliebten Mutter meinen Sinn zu erweichen vermochte, sperrte man mich in ein düsteres Gefängniß, auf daß die Zeit meinen störrischen Sinn zu brechen versuche. Doch ich war nicht <hi rend="spaced" xml:id="tg178.3.18.1">allein</hi> in meinem öden Kerker. Oft verklärten sich die finsteren Wände mir mit himmlischem Glanze, und die Strahlen eines überirdischen Lichts, die zu mir eindrangen, bildeten Eure Gestalt, und Euere Züge, und Trost brachte mir Eure beglückende Nähe selbst in der trostlosen Beschränkung einer von allen Menschen geschiedenen Einsamkeit.</p>
                    <p rend="zenoPLm4n0" xml:id="tg178.3.19">So waren zwei Jahre langsam an mir vorübergegangen – doch während mein Leib gefangen <pb n="212" xml:id="tg178.3.19.1"/>war, schwebte frei die Seele in glückseligeren Regionen einher, und genoß in Träumen, was die Wirklichkeit ihr versagte. Schon wähnte ich, mein Kerker werde mir einst auch zum Grabe dienen, da ließ der Wächter, der meiner hütete, vergeßlicherweise die Thüre meines Gefängnisses offen, und mir war plötzlich, als winke die frische Luft der Freiheit mich hinaus aus diesen dumpfen Mauern, dorthin, wo sie weht – und ich floh eiligst, irrete lang im Elend umher, und endlich hab ich in Pilgertracht das Ziel erreicht, nach dem ich schmachtete.</p>
                    <p rend="zenoPLm4n0" xml:id="tg178.3.20">Hier zu Euren Füßen, heiliger Vater, seht mich knieen. Ist es eine Sünde, für Euch in den Flammen unauslöschlicher Liebe entbrannt zu seyn, so lehrt mich sie büßen, aber begehrt nicht, mit rauher Hand sie zu verlöschen, denn Ihr würdet meinen inneren Himmel dadurch zerstören. Mit Fasten und Gebet will ich mein Fleisch kreuzigen – nur verdammt mich nicht zur Reue. Denn heilig wie Ihr selbst ist mir die Leidenschaft, die gleich einer Eingebung von Oben mich durch Wälder und Wüsten, durch Berge und Thäler zu Euch führte, zu Euch, dessen wirklicher Anblick sie auf ewig befestigt hat.</p>
                    <p rend="zenoPLm4n0" xml:id="tg178.3.21">Der fromme Bischoff hatte sich noch nie in einer schwierigeren Lage befunden. Er räusperte sich, und wußte kaum, was er vor Verlegenheit beginnen sollte. Es dünkte ihm rauh und lieblos, mit strengen Worten so treu ergebene Gesinnungen nieder zu schlagen, und sündlich schien es ihm wieder, sie zu nähren durch Freundlichkeit und Güte. Er wollte einen finsteren Blick auf die Königstochter werfen, <pb n="213" xml:id="tg178.3.21.1"/>doch ihr Auge strahlte ihn an, wie eine warme Sonne, und das erkünstelte Eis seiner Mienen fing plötzlich an zu schmelzen vor dem Zauber ihrer unvergleichlichen Schönheit.</p>
                    <p rend="zenoPLm4n0" xml:id="tg178.3.22">Viel und mancherlei, stammelte er verwirrt, sind der Anfechtungen auf Erden, aber schön ists, sie zu bekämpfen, wenn es uns auch schwer fällt. Gehe daher in ein Kloster, meine Tochter, und ziehe Deine Gedanken ab von irdischen Wünschen, auf daß die Sünde nicht länger in dir triumphire.</p>
                    <p rend="zenoPLm4n0" xml:id="tg178.3.23">Was Ihr mir gebietet will ich ohne Murren thun, versetzte die Jungfrau mit einer Stimme, die immer melodischer und seelenvoller gleich einem Syrenengesang in den Busen des Bischoffs drang. Aber entlaßt mich nicht so gleichgültig, als hätte mein Geständniß nur Eure herbe Verachtung erregt. Gebt mir nur Euren Segen mit in die ewige Abgeschiedenheit, der ich mein junges Leben opfere, und als das Zeichen der Weihe gönnt mir die Gunst, das Eure Lippen nur einmahl die meinigen berühren, auf daß doch <hi rend="spaced" xml:id="tg178.3.23.1">ein</hi> Augenblick in meinem Daseyn sey, an den ich zurück denken darf, dankbar für das hohe, unüberschwengliche Glück, das er enthält.</p>
                    <p rend="zenoPLm4n0" xml:id="tg178.3.24">Was begehrst Du, Versucherin? sprach Basilius erbebend, doch die verbotene Lust glühte in ihm gleich einer sengenden Flamme und verzehrte jede Kraft, die Bitte der Königstochter zu verweigern. Wunderbare Wallungen wirbelten sein Blut in wildem Kreislauf durch alle Pulse – vor seinen Augen tanzten im Regenbogenschimmer die Gegenstände umher, und unwillkührlich streckten sich zitternd seine Arme <pb n="214" xml:id="tg178.3.24.1"/>aus, die blühende Jungfrau zu umfassen, um den begehrten Kuß auf ihre Lippen zu drücken.</p>
                    <p rend="zenoPLm4n0" xml:id="tg178.3.25">Da säuselte leise, wie ein kühler Hauch des Mittags Schwüle erfrischt, der Ton eines Seufzers von dem hohen Marienbild herab, das in einer Vertiefung der Wand stehend, so oft der Gegenstand der inbrünstigsten Andacht des Bischoffs gewesen war, und als er erschrocken hinaufblickte zu der Hochgebenedeiten, sah er ihre göttlichen Augen von dem Thau einer irdischen Thräne verdunkelt, und die immer brennenden Kerzen, die er ihr zu Ehren auf dem Altar zu ihren Füßen gestiftet hatte, waren erloschen, und füllten das Gemach mit bläulichem Dampfe.</p>
                    <p rend="zenoPLm4n0" xml:id="tg178.3.26">Nimm Dein Opfer zurück, Basilius, sprach sie mit himmlischer aber trauriger Stimme, denn als du mit reinem Gemüth diese Kerzen mir weihtest, waren sie mir eine wohlgefällige Spende Deiner Frömmigkeit – jetzt aber verletzen sie, gleich Irrlichtern, auf Sümpfen erzeugt, flackernd mein verklärtes Auge, und sie sollen nicht Deinen Fall beleuchten.</p>
                    <p rend="zenoPLm4n0" xml:id="tg178.3.27">Da schlug Basilius in sich. Wie ein Nebel erhob es sich vor seinen Sinnen, und als er schaute was aus der Königstochter geworden sey, lächelte sie zwar noch immer in übermenschlicher Schönheit ihn an, doch mit Entsetzen sah er den Pferdefuß unter ihrem faltenreichen Gewand hervorragen, und es wurde ihm klar, daß der Böse nur die Gestalt einer Jungfrau angenommen hatte, ihn zu versuchen, und ihn um die Ehre rühmlich erkämpfter Heiligkeit zu bringen.</p>
                    <p rend="zenoPLm4n0" xml:id="tg178.3.28">
                        <pb n="215" xml:id="tg178.3.28.1"/>Hebe Dich weg von mir, Satan! rief er mit zerknirschtem Herzen, und des Mägdleins lockende Gestalt verwandelte sich plötzlich in eine hohe, zischende Schwefelflamme, die prasselnd in die Erde sank, als er ein Kreuz schlug, sich vor ihr zu verwahren.</p>
                    <p rend="zenoPLm4n0" xml:id="tg178.3.29">Da stürzte der Bischoff nieder auf seine Kniee, und zerfloß fast in Thränen der Reue, doch als er nach langem, schmerzlichen Gebet sein Auge schüchtern wieder erhob zu dem Antlitz der Mutter aller Gnaden, lächelte ihre Miene ihn wieder an, und ohne menschliche Hülfe hatten sich von neuem die Kerzen auf ihrem Altar entzündet. Das verehrt' er als ein Zeichen, daß seine Schwäche ihm vergeben sey, und alle seine noch übrigen Lebenstage brachte er zu, um durch rüstigen Kampf mit der Sünde sich mit der Gottheit und sich selber zu versöhnen.</p>
                </div>
                <div type="footnotes">
                    <head type="h4" xml:id="tg178.3.32">Fußnoten</head>
                    <note xml:id="tg178.3.33.note" target="#tg161.3.31.1">
                        <p xml:id="tg178.3.33">
                            <anchor xml:id="tg178.3.33.1" n="/Literatur/M/Ahlefeld, Charlotte von/Erzählungen/Gesammelte Erzählungen/Zweiter Band/Die Königstochter aus der Fremde#Fußnoten_1"/>
                            <ref cRef="/Literatur/M/Ahlefeld, Charlotte von/Erzählungen/Gesammelte Erzählungen/Erster Band/Das Liebhaber-Theater#Fußnote_1" xml:id="tg178.3.33.2">1</ref>
                            <hi rend="spaced" xml:id="tg178.3.33.3">Wallensteins Tod</hi>. Dritter Aufzug, erste Scene, Seite 55.</p>
                    </note>
                </div>
            </div>
        </body>
    </text>
</TEI>