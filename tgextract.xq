xquery version "3.0";

import module namespace console="http://exist-db.org/xquery/console" at "java:org.exist.console.xquery.ConsoleModule";
declare namespace tei="http://www.tei-c.org/ns/1.0";

for $doc at $pos in collection('/db/data/textgrid')//tei:TEI
where  ( try { exists( $doc//tei:text/tei:body//text()[not(ancestor::tei:head)] ) }
        catch * { console:log( 'where: '||$doc/base-uri() ) }
        )
    and 
      (  if($doc//tei:textClass/tei:keywords[@scheme="http://textgrid.info/namespaces/metadata/core/2010#genre"]/tei:term/string(.) != 'verse')
        then true() else false() )
return
    try {   xmldb:store('/db/data/textgrid-tei/', $pos||'.xml' ,$doc) }
    catch * { console:log($doc/@n) }
